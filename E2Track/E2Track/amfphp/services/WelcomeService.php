<?php


class WelcomeService
{
    // get last updated for sparkline indices
    function getLastUpdate(){
        $sql = "SELECT date_format(max(LastUpdated), '%a, %b %e %Y, %h:%i %p EDT') as LastUpdated from sparkindices";
        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsLastUpdated = mysql_query($sql, $mce);
        $row_rsLastUpdated = mysql_fetch_assoc($rsLastUpdated);
        $lastUpdated = $row_rsLastUpdated['LastUpdated'];        
        return $lastUpdated;
        
    }

    // get all sparkline indices
    function getSparklineSummary(){
        $sql = "SELECT * FROM sparkindices  ORDER BY DisplayOrder";

        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsIndices = mysql_query($sql, $mce);
        return $rsIndices;

    }



    function getPicks($rowLimit){
        $sql = "SELECT ID, date_format(PublishDate,'%Y/%m/%d') as PublishDate, Title, URL, Body, Source, date_format(LastUpdated, '%Y/%m/%d %H:%i:%s') as LastUpdated, Username FROM marketupdates order by PublishDate DESC, LastUpdated DESC";
        if ($rowLimit){
            $sql .= " LIMIT $rowLimit";
        }

        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsRows = mysql_query($sql, $mce);
        return $rsRows;
    }

}


?>