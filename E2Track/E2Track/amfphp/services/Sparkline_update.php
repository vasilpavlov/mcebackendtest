<?php
require_once('../../sparkline/lib/Sparkline_Line.php');


class Sparkline_update
{


    // login the user - they must have securitylevel of 99 to run the app
    function loginWebsiteTools($username, $password){
       $sql = "SELECT SecurityLevel FROM users where Username = '$username' AND Password = '$password'";
        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsResult = mysql_query($sql, $mce);
        $row_rsResult = mysql_fetch_assoc($rsResult);
        if ($row_rsResult == FALSE){
            return 0;
        }
        else{
            return $row_rsResult['SecurityLevel'];
        }
    }



    // make a single sparkline and update index, by name
	function makeSparkline($sourcename, $Username){
	    $sql = "SELECT Price, LastUpdated from mcene1.sparkdata WHERE SparkIndexName = '$sourcename' AND pricedate > date_sub(now(), INTERVAL 1 YEAR) ORDER  BY pricedate DESC";

        require('mce.php');
		mysql_select_db($database_mce, $mce);
		$rsPrices = mysql_query($sql, $mce);

        $tmpArray = array();
        $counter = 0;

        $row_rsPrices = mysql_fetch_assoc($rsPrices);
        $lastUpdated = $row_rsPrices['LastUpdated'];
        do{
            $tmpArray[$counter] = round($row_rsPrices['Price'], 2);
            $counter++;
        }while ($row_rsPrices = mysql_fetch_assoc($rsPrices));
        mysql_free_result($rsPrices);

        $lastPrice = $tmpArray[0];
        $previousPrice = $tmpArray[1];
        $pctChange = round(((($lastPrice - $previousPrice) / $lastPrice) *100), 2);

        // reverse the array
        $tmpArrayR = array_reverse($tmpArray);

        $sparkline = new Sparkline_Line();
        $sparkline->SetDebugLevel(DEBUG_ALL);

        $counter = 0;


        for($counter = 0; $counter < count($tmpArrayR); $counter++){
            $sparkline->SetData($counter, $tmpArrayR[$counter]);
        }

        $sparkline->SetLineSize(1);
        $sparkline->RenderResampled(83, 22);
        $retval = $sparkline->Output('../../sparkline/images/' . $sourcename . '.png');
        unset ($sparkline);


        // update the database with last price, pct changed
        $sql = "update sparkindices set LastPrice = '$lastPrice', PctChange = $pctChange, LastUpdated = '$lastUpdated', Username = '$Username' where SparkIndexName = '$sourcename'";

        $result = mysql_query($sql, $mce);
        mysql_free_result($result);

	}

    // Generate sparklines and update numbers for all indices
    function makeAllSparklines($Username){
        $this->makeSparkline('mass-hub', $Username);
        $this->makeSparkline('nymex-crude', $Username);
        $this->makeSparkline('nymex-gas', $Username);
        $this->makeSparkline('nymex-heating', $Username);
        $this->makeSparkline('pseg-average', $Username);
        $this->makeSparkline('zone-j', $Username);
        return 'done';
    }

    // get all sparkline indices
    function getSparklineSummary(){
        $sql = "select si.*, max(PriceDate) as PriceDate from sparkindices si join sparkdata on si.SparkIndexName = sparkdata.SparkIndexName group by si.SparkIndexName order by LastUpdated DESC";

        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsIndices = mysql_query($sql, $mce);
        return $rsIndices;

    }

    function getPricesForIndex($SparkIndexName){
        $sql = "select date_format(PriceDate,'%Y/%m/%d') as PriceDate , Price, date_format(LastUpdated,'%Y/%m/%d') as LastUpdated, Username from sparkdata where SparkIndexName = '$SparkIndexName' order by PriceDate desc";

        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsPrices = mysql_query($sql, $mce);

        return $rsPrices;

    }

    function addPriceForIndex($SparkIndexName, $PriceDate, $Price, $Username){
        $sql = "insert sparkdata (SparkIndexName, PriceDate, Price, LastUpdated, Username) values ('$SparkIndexName', '$PriceDate', $Price, now(), '$Username')";
        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsPrices = mysql_query($sql, $mce);

        if ($rsPrices == false){
            $retval = mysql_error();
        }
        else{
            $retval = "Record saved";
        }
        $this->makeSparkline($SparkIndexName, $Username);
        return $retval;
    }

    function editPriceForIndex($SparkIndexName, $PriceDate, $Price, $Username){
        $sql = "UPDATE sparkdata SET Price = $Price, LastUpdated = now(), Username = '$Username' WHERE SparkIndexName = '$SparkIndexName' AND PriceDate = '$PriceDate'";
        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsPrices = mysql_query($sql, $mce);

        if ($rsPrices == false){
            $retval = mysql_error();
        }
        else{
            $retval = "Record saved";
        }
        $this->makeSparkline($SparkIndexName, $Username);
        return $retval;

    }

    function getPicks($rowLimit){
        $sql = "SELECT ID, date_format(PublishDate,'%Y/%m/%d') as PublishDate, Title, URL, Body, Source, date_format(LastUpdated, '%Y/%m/%d %H:%i:%s') as LastUpdated, Username FROM marketupdates order by PublishDate DESC, LastUpdated DESC";
        if ($rowLimit){
            $sql .= " LIMIT $rowLimit";
        }

        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsRows = mysql_query($sql, $mce);
        return $rsRows;
    }

     function editPick($ID, $Title, $URL, $Body, $Source, $PublishDate, $Username){

        $sql = "UPDATE marketupdates SET Title = '$Title', URL='$URL', Body='$Body', Source='$Source', PublishDate='$PublishDate', LastUpdated='" . date('Y-m-d H:i:s')  . "', Username = '$Username' WHERE ID = $ID";

        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsRows = mysql_query($sql, $mce);
        return $rsRows;
    }
    function addPick($Title, $URL, $Body, $Source, $PublishDate, $Username){

        $sql = "INSERT marketupdates (Title, URL, Body, Source, PublishDate, LastUpdated, Username) ";
        $sql .= "VALUES('$Title', '$URL', '$Body', '$Source', '$PublishDate', '" . date('Y-m-d H:i:s') . "', '$Username')";
        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $rsRows = mysql_query($sql, $mce);
        return $rsRows;
    }

    function deletePick($ID){

        $sql = "DELETE FROM marketupdates WHERE ID = $ID";
        require('mce.php');
        mysql_select_db($database_mce, $mce);
        $retval = mysql_query($sql, $mce);
        return $retval;
    }

}


?>