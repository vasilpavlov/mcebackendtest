<!--- This file includes by testingTools.cfm --->

<cfparam name="form.alert_subscription_uid" type="string" default="">
<cfparam name="form.numberOfRecords" type="string" default="2">

<cfif isDefined("form.doTouchRecords")>
	<cfquery datasource="#datasource#" result="touched">
		update EnergyUsage
		set consider_touched_date = getDate()
		where energy_usage_uid IN (
			select top #form.numberOfRecords# energy_usage_uid from EnergyUsageView euv
			where euv.energy_account_uid IN
				(select energy_account_uid FROM AlertSubscriptions_EnergyAccounts
				 where alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#form.alert_subscription_uid#" list="true">))
			and is_active = 1
			and data_is_reportable = 1
			order by period_end desc, consider_touched_date desc
		)
	</cfquery>

	<cfoutput><font color="red">#touched.recordCount# records touched.</font></cfoutput>
</cfif>


<cfform method="post">
	<h3>Touch Energy Usage Records</h3>
	Subscription UID:
	<cfinput name="alert_subscription_uid" type="text" value="#alert_subscription_uid#" required="true" size="40"><br/>

	Number of Records to Touch (most recent first):
	<cfinput name="numberOfRecords" type="text" size="5" validate="integer" value="#numberOfRecords#"><br/>

	<cfinput name="doTouchRecords" type="submit" value="Touch Records Now">
</cfform>