<!--- This file includes by testingTools.cfm --->

<cfparam name="form.lastGeneratedMinutesBack" type="numeric" default="10">

<cfif isDefined("form.doResetLastGenerated")>
	<cfquery datasource="#datasource#" result="touched">
		update AlertSubscriptions_EnergyAccounts
		set
			last_generated = '1/1/1970'
		where
			last_generated > DATEADD(n, -#lastGeneratedMinutesBack#, getDate())
			and alert_subscription_uid IN
				(SELECT alert_subscription_uid FROM AlertSubscriptions WHERE is_active = 1)
	</cfquery>

	<cfoutput><font color="red">#touched.recordCount# records updated.</font></cfoutput>
</cfif>


<cfform method="post">
	<h3>Reset Last-Generated Date (at Subscription-to-Account level)</h3>
	<p>
	This clears the "last generated" flag for recently-polled alert subscriptions.
	For some subscription types, this will cause the system to be willing to send an alert again.
	</p>

	Number of minutes back to look for recently-polled subscriptions:
	<cfinput name="lastGeneratedMinutesBack" size="5" type="text" validate="integer" value="#lastGeneratedMinutesBack#"><br/>

	<cfinput name="doResetLastGenerated" type="submit" value="Reset Last-Generated">
</cfform>