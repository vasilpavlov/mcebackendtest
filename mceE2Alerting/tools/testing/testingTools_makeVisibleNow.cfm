<!--- This file includes by testingTools.cfm --->

<cfif isDefined("doMakeVisibleNow")>
	<cfquery datasource="#datasource#" result="touched">
		update alerts
		set alert_visibility_date = alert_generated_date
		where alert_visibility_date > getDate()
	</cfquery>

	<cfoutput><font color="red">#touched.recordCount# records updated.</font></cfoutput>
</cfif>


<cfform method="post">
	<h3>Make Visible Now (at Alert level)</h3>
	<p>
	This updates the visibility date for any recently-generated alerts that are currently "hidden" because their
	visibility date is still in the future. This is helpful for testing purposes, so you can see new alerts in the UIs without waiting.
	If you want to change or eliminate the *production* visibility delay for a given alert type, do that in code in the alert renderer.
	</p>
	<cfinput name="doMakeVisibleNow" type="submit" value="Make Visible Now">
</cfform>