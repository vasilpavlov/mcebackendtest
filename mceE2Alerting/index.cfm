<!--- Chanage these settings to control which alert processes are executed --->
<cfparam name="doAlertGeneration" type="boolean" default="false">
<cfparam name="doAlertSending" type="boolean" default="false">
<cfparam name="showResultDumps" type="boolean" default="true">
<cfparam name="respectVisibilityDate" type="boolean" default="true">
<cfparam name="digestCodeForAlertSending" type="string" default="daily">

<cfinclude template="tools/toolsHeader.cfm">

<!--- ALERT GENERATION --->
<cfif doAlertGeneration>
	<cfoutput><h2>Generating Alerts</h2></cfoutput>
	<cfset result = createObject("component", "remote.RemoteAlertScheduleService").pollAlerts()>
	<cfoutput>Alerts Generated: #ArrayLen(result)#</cfoutput>
	<p>Newly-generated alerts are subject to the visibility delay specific to that alert type.
	You can clear the visibility delay for current alerts via the "Make Visible Now" button in the Testing Tools page,
	or by clicking <a href="tools/testing/testingTools.cfm?doMakeVisibleNow=true">Make Visible Now</a>.</p>

	<!--- Dump out info --->
	<cfif showResultDumps and ArrayLen(result) gt 0>
		<cfdump var="#result#" label="Quick Summary of Generated Alerts">
	</cfif>
</cfif>


<!--- ALERT SENDING --->
<cfif doAlertSending>
	<cfoutput><h2>Sending Alerts</h2></cfoutput>
		<cfset result = createObject("component", "remote.RemoteAlertScheduleService").sendAlerts(digestCodeForAlertSending, respectVisibilityDate)>
	<cfoutput>Alerts Sent: #result#</cfoutput>
</cfif>