
<!---

  Template Name:  Alert
     Base Table:  Alerts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, July 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, July 16, 2009 - Template Created.

--->

<cfcomponent	displayname="AlertSummary"
				hint="This CFC sumarizes all information for an Alert, and is used to summarize data for read-only access that appears in multiple tables."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.alerting.vo.AlertSummary"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="alert_uid" required="true" type="string"/>
	<cfproperty name="alert_subscription_uid" required="true" type="string"/>
	<cfproperty name="client_company_uid" required="true" type="string"/>
	<cfproperty name="property_uid" required="true" type="string"/>
	<cfproperty name="alert_type_code" required="true" type="string"/>
	<cfproperty name="alert_status_code" required="true" type="string"/>
	<cfproperty name="recipient_address" required="true" type="string"/>
	<cfproperty name="alert_content" required="true" type="string"/>
	<cfproperty name="alert_due_date" required="true" type="date"/>
	<cfproperty name="alert_visibility_date" required="true" type="date"/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>
	<cfproperty name="client_company_friendly_name" required="true" type="string" hint="Name of the client company"/>
	<cfproperty name="property_friendly_name" required="true" type="string" hint="Name of the property"/>
	<cfproperty name="alert_type_friendly_name" required="true" type="string" hint="Name of the alert type"/>
	<cfproperty name="alert_status_friendly_name" required="true" type="string" hint="Name of the alert status"/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="alert_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<cfproperty name="alertMeta" type="array">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "alert_uid"/>
	<cfset this.alert_uid = ""/>

</cfcomponent>
