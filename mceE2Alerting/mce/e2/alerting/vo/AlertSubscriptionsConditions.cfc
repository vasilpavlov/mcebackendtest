<cfcomponent output="false"
			extends="BaseValueObject"
			alias="mce.e2.alerting.vo.AlertSubscriptionsConditions">
	
	<cfproperty name="relationship_id" required="true" type="string"/>		
	<cfproperty name="alert_subscription_uid" required="true" type="string"/>
	<cfproperty name="alert_condition_code" required="true" type="string"/>
	<cfproperty name="condition_value" required="true" type="string"/>
	
	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="relationship_id" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "relationship_id"/>
	
</cfcomponent>