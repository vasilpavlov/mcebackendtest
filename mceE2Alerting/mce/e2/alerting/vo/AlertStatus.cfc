
<!---

  Template Name:  AlertStatus
     Base Table:  AlertStatuses	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, July 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, July 16, 2009 - Template Created.		

--->
		
<cfcomponent	displayname="AlertStatus"
				hint="This CFC manages all object/bean/value object instances for the AlertStatuses table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.alerting.vo.AlertStatus"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="alert_status_code" required="true" type="string"/>
	<cfproperty name="friendly_name" required="true" type="string"/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="alert_status_code" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "alert_status_code"/>
	<cfset this.alert_status_code = ""/>

</cfcomponent>
