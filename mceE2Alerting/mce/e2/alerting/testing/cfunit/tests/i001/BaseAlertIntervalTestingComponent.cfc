<cfcomponent name="Base Alert Interval Method Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the alert interval delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeAlertIntervalTestData"
				access="private"
				hint="This method is used to purge / delete any alertInterval data related to the test data being used when alertInterval unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="alertIntervals" type="query" hint="This argument is used to identify the alertInterval data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertInterval data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertIntervals
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newAlertIntervals.friendly_name)#">

					)
		
			delete
			from	dbo.alertIntervals
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedAlertIntervals.friendly_name)#">

					)		
		
		</cfquery>
			
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the alertInterval service and delegate tests.">
		
		<cfscript>
		
			//Build out the alertInterval test data (for create / modify scenarios)
			createAlertIntervalTestData();

		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createAlertIntervalTestData"
				access="private"
				hint="This method is used to create the test data for testing alertInterval records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing alertInterval data to be tested with (the query should contain all of the columns of the base table)
			this.newAlertIntervals = queryNew('alert_interval_code,friendly_name,is_enabled,queue_priority,polling_interval,last_polled,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newAlertIntervals,4);
		
			// Set the cell values for this instance of a alertInterval
			querySetCell(this.newAlertIntervals,'alert_interval_code', 'AlertInterval001', 1);
			querySetCell(this.newAlertIntervals,'friendly_name', 'AlertInterval 001', 1);
			querySetCell(this.newAlertIntervals,'is_enabled', 1, 1);
			querySetCell(this.newAlertIntervals,'queue_priority', 1, 1);
			querySetCell(this.newAlertIntervals,'polling_interval', 32000, 1);
			querySetCell(this.newAlertIntervals,'last_polled', '2000-01-01 00:00:00.0', 1);
			querySetCell(this.newAlertIntervals,'is_active', 1, 1);
			querySetCell(this.newAlertIntervals,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertIntervals,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newAlertIntervals,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertIntervals,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newAlertIntervals,'alert_interval_code', 'AlertInterval002', 2);
			querySetCell(this.newAlertIntervals,'friendly_name', 'AlertInterval 002', 2);
			querySetCell(this.newAlertIntervals,'is_enabled', 1, 2);
			querySetCell(this.newAlertIntervals,'queue_priority', 1, 2);
			querySetCell(this.newAlertIntervals,'polling_interval', 32000, 2);
			querySetCell(this.newAlertIntervals,'last_polled', '2000-01-01 00:00:00.0', 2);
			querySetCell(this.newAlertIntervals,'is_active', 1, 2);
			querySetCell(this.newAlertIntervals,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertIntervals,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newAlertIntervals,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertIntervals,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newAlertIntervals,'alert_interval_code', 'AlertInterval003', 3);
			querySetCell(this.newAlertIntervals,'friendly_name', 'AlertInterval 003', 3);
			querySetCell(this.newAlertIntervals,'is_enabled', 1, 3);
			querySetCell(this.newAlertIntervals,'queue_priority', 1, 3);
			querySetCell(this.newAlertIntervals,'polling_interval', 32000, 3);
			querySetCell(this.newAlertIntervals,'last_polled', '2000-01-01 00:00:00.0', 3);
			querySetCell(this.newAlertIntervals,'is_active', 1, 3);
			querySetCell(this.newAlertIntervals,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertIntervals,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newAlertIntervals,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertIntervals,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newAlertIntervals,'alert_interval_code', 'AlertInterval004', 4);
			querySetCell(this.newAlertIntervals,'friendly_name', 'AlertInterval 004', 4);
			querySetCell(this.newAlertIntervals,'is_enabled', 1, 4);
			querySetCell(this.newAlertIntervals,'queue_priority', 1, 4);
			querySetCell(this.newAlertIntervals,'polling_interval', 32000, 4);
			querySetCell(this.newAlertIntervals,'last_polled', '2000-01-01 00:00:00.0', 4);
			querySetCell(this.newAlertIntervals,'is_active', 1, 4);
			querySetCell(this.newAlertIntervals,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertIntervals,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newAlertIntervals,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertIntervals,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of alertIntervals without primary keys
			this.newAlertIntervalsWithoutPks = duplicate(this.newAlertIntervals);
			
			// Remote the primary keys from the alertInterval test data 
			querySetCell(this.newAlertIntervalsWithoutPks,'alert_interval_code', '', 1);
			querySetCell(this.newAlertIntervalsWithoutPks,'alert_interval_code', '', 2);
			querySetCell(this.newAlertIntervalsWithoutPks,'alert_interval_code', '', 3);
			querySetCell(this.newAlertIntervalsWithoutPks,'alert_interval_code', '', 4);
					
			// Duplicate the new alertIntervals to create the baseline modified alertIntervals
			this.modifiedAlertIntervals = duplicate(this.newAlertIntervals);

			// Update the friendly names for the modified alertIntervals
			querySetCell(this.modifiedAlertIntervals,'friendly_name', 'AlertInterval M 001', 1);
			querySetCell(this.modifiedAlertIntervals,'friendly_name', 'AlertInterval M 002', 2);
			querySetCell(this.modifiedAlertIntervals,'friendly_name', 'AlertInterval M 003', 3);			
			querySetCell(this.modifiedAlertIntervals,'friendly_name', 'AlertInterval M 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedAlertIntervals,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedAlertIntervals,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedAlertIntervals,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedAlertIntervals,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareAlertIntervalProperties"
				access="private"
				hint="This method is used to compare the properties for a given alertInterval vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of alertInterval vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the alertInterval from the application database --->
				<cfset local.retrievedAlertInterval = this.delegate.getAlertIntervalAsComponent(arguments.sourceObject[local.arrayIndex].alert_interval_code)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the alertInterval from the application database --->
				<cfset local.retrievedAlertInterval = this.service.getAlertInterval(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared alertInterval VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedAlertInterval, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved alertInterval VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedAlertInterval[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedAlertInterval[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>