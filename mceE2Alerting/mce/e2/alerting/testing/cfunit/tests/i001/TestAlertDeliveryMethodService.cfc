<cfcomponent name="Test Alert Delivery Method Service"
			 extends="BaseAlertDeliveryMethodTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the alertDeliveryMethodService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')>
		<cfset init()>
	</cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the alertDeliveryMethod service
			this.service = request.beanFactory.getBean("AlertDeliveryMethodService");
			this.delegate = request.beanFactory.getBean("AlertDeliveryMethodDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyAlertDeliveryMethodComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty alertDeliveryMethod component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertDeliveryMethod delegate --->
		<cfset local.alertDeliveryMethod = this.service.getEmptyAlertDeliveryMethodVo()>

		<!--- Assert that the alertDeliveryMethod object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.alertDeliveryMethod, "mce.e2.alerting.vo.alertDeliveryMethod"), true, "local.alertDeliveryMethod objectType is not of type mce.e2.alerting.vo.AlertDeliveryMethod.")>
			
	</cffunction>

	<cffunction name="getAlertDeliveryMethods"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all alertDeliveryMethods from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alert delegate --->
		<cfset local.alertDeliveryMethodCollection = this.service.getAlertDeliveryMethods()>
				
		<!--- Assert that the alertDeliveryMethod array collection is an array --->
		<cfset assertTrue(isArray(local.alertDeliveryMethodCollection), "The value returned by this.service.getAlertDeliveryMethods() was not an array.")>

		<!--- Compare the alertDeliveryMethod properties and excersize retrieving single instances of alertDeliveryMethods --->
		<cfset comparealertDeliveryMethodProperties(local.alertDeliveryMethodCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getAlertDeliveryMethod"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single alertDeliveryMethod from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertDeliveryMethod delegate --->
		<cfset local.alertDeliveryMethodCollection = this.service.getAlertDeliveryMethods()>
				
		<!--- Loop through the alertDeliveryMethod collection, and retrieve each alertDeliveryMethod individually --->
		<cfloop from="1" to="#arraylen(local.alertDeliveryMethodCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the alertDeliveryMethod to test --->
			<cfset local.alertDeliveryMethod = local.alertDeliveryMethodCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a alertDeliveryMethod from the service --->
				<cfset local.testAlertDeliveryMethod = this.service.getalertDeliveryMethod(local.alertDeliveryMethod)>

			<!--- Assert that the alertDeliveryMethod array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testAlertDeliveryMethod, "mce.e2.alerting.vo.AlertDeliveryMethod"), true, "local.testAlertDeliveryMethod objectType is not of type mce.e2.alerting.vo.AlertDeliveryMethod.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of alertDeliveryMethod data --->
	<cffunction name="saveAndValidateNewAlertDeliveryMethods"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveNewAlertDeliveryMethod() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertDeliveryMethods = this.delegate.queryToVoArray(this.newAlertDeliveryMethods, 'mce.e2.alerting.vo.AlertDeliveryMethod')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the alertDeliveryMethod test data --->	
		<cfset purgeAlertDeliveryMethodTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertDeliveryMethods)#" index="local.arrayIndex">	

			<!--- Save the alertDeliveryMethod to the application database --->
			<cfset local.testAlertDeliveryMethodPrimaryKey = this.service.saveNewAlertDeliveryMethod(local.newAlertDeliveryMethods[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<!---
			Value is a code, not a uid
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertDeliveryMethodPrimaryKey), "The value returned by this.service.saveNewAlertDeliveryMethod() was not a unique identifier.")/>
			--->
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertDeliveryMethods), "The total number of records processed does not match the total number of records iterated over when creating alertDeliveryMethod records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertDeliveryMethodProperties(sourceObject=local.newAlertDeliveryMethods, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertDeliveryMethodTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewAlertDeliveryMethods"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveAndReturnNewAlertDeliveryMethod() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test alertDeliveryMethods array --->			
		<cfset local.testAlertDeliveryMethods = arrayNew(1)>			
						
		<!--- Build out the array of alertDeliveryMethod objects --->
		<cfset local.newAlertDeliveryMethods = this.delegate.queryToVoArray(this.newAlertDeliveryMethods, 'mce.e2.alerting.vo.AlertDeliveryMethod')/>
		
		<!--- Purge / remote the alertDeliveryMethod test data --->	
		<cfset purgeAlertDeliveryMethodTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertDeliveryMethods)#" index="local.arrayIndex">	

			<!--- Save the alertDeliveryMethod to the application database --->
			<cfset local.testAlertDeliveryMethod = this.service.saveAndReturnNewAlertDeliveryMethod(local.newAlertDeliveryMethods[local.arrayIndex])/>
			
			<!--- Assert that the alertDeliveryMethod object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testAlertDeliveryMethod, "mce.e2.alerting.vo.AlertDeliveryMethod"), true, "local.alertDeliveryMethod objectType is not of type mce.e2.alerting.vo.AlertDeliveryMethod.")>
						
			<!--- Append the alertDeliveryMethod to the test array --->			
			<cfset arrayAppend(local.testAlertDeliveryMethods, local.testAlertDeliveryMethod)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testAlertDeliveryMethods), arrayLen(local.newAlertDeliveryMethods), "The total number of records processed does not match the total number of records iterated over when creating alertDeliveryMethod records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertDeliveryMethodProperties(sourceObject=local.testAlertDeliveryMethods, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertDeliveryMethodTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingAlertDeliveryMethods"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of alertDeliveryMethods.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alertDeliveryMethod objects --->
		<cfset local.newAlertDeliveryMethods = this.delegate.queryToVoArray(this.newAlertDeliveryMethods, 'mce.e2.alerting.vo.AlertDeliveryMethod')/>
		<cfset local.modifiedAlertDeliveryMethods = this.delegate.queryToVoArray(this.modifiedAlertDeliveryMethods, 'mce.e2.alerting.vo.AlertDeliveryMethod')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the alertDeliveryMethod test data --->	
		<cfset purgeAlertDeliveryMethodTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertDeliveryMethods)#" index="local.arrayIndex">	

			<!--- Save the alertDeliveryMethod to the application database --->
			<cfset this.service.saveNewAlertDeliveryMethod(local.newAlertDeliveryMethods[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertDeliveryMethods), "The total number of records processed does not match the total number of records iterated over when creating alertDeliveryMethod records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertDeliveryMethodProperties(sourceObject=local.newAlertDeliveryMethods, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedAlertDeliveryMethods)#" index="local.arrayIndex">	

			<!--- Save the alertDeliveryMethod to the application database --->
			<cfset this.service.saveExistingAlertDeliveryMethod(local.modifiedAlertDeliveryMethods[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedAlertDeliveryMethods), "The total number of records processed does not match the total number of records iterated over when modifying alertDeliveryMethod records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertDeliveryMethodProperties(sourceObject=local.modifiedAlertDeliveryMethods, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertDeliveryMethodTestData()> 			
							
	</cffunction>	

</cfcomponent>