<cfcomponent name="Test Alert Summary Service"
			 extends="BaseAlertSummaryTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the alertSummaryService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')>
		<cfset init()>
	</cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the alert service
			this.alertService = request.beanFactory.getBean("alertService");
			this.alertDelegate = request.beanFactory.getBean("alertDelegate");
			this.alertSummaryService = request.beanFactory.getBean("alertSummaryService");
			this.alertSummaryDelegate = request.beanFactory.getBean("alertSummaryDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyAlertSummaryComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty alertSummary component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertSummary delegate --->
		<cfset local.alertSummary = this.alertSummaryService.getEmptyAlertSummaryVo()>

		<!--- Assert that the alertSummary object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.alertSummary, "mce.e2.alerting.vo.alertSummary"), true, "local.alert objectType is not of type mce.e2.alerting.vo.alertSummary.")>
			
	</cffunction>

	<cffunction name="getAlertSummaries"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all alert summaries from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertSummary delegate --->
		<cfset local.alertSummaryCollection = this.alertSummaryService.getAlertSummaries()>
				
		<!--- Assert that the alertSummary array collection is an array --->
		<cfset assertTrue(isArray(local.alertSummaryCollection), "The value returned by this.service.getAlertSummaries() was not an array.")>

		<!--- Compare the alertSummary properties and excersize retrieving single instances of alertSummaries --->
		<cfset compareAlertSummaryProperties(local.alertSummaryCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getAlertSummary"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single alertSummary from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertSummary delegate --->
		<cfset local.alertSummaryCollection = this.alertSummaryService.getAlertSummaries()>
				
		<!--- Loop through the alertSummary collection, and retrieve each alertSummary individually --->
		<cfloop from="1" to="#arraylen(local.alertSummaryCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the alertSummary to test --->
			<cfset local.alertSummary = local.alertSummaryCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a alertSummary from the service --->
				<cfset local.testAlertSummary = this.alertSummaryService.getalertSummary(local.alertSummary)>

			<!--- Assert that the alertSummary array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testAlertSummary, "mce.e2.alerting.vo.alertSummary"), true, "local.testAlertSummary objectType is not of type mce.e2.alerting.vo.alertSummary.")>
	
		</cfloop>
				
	</cffunction>

</cfcomponent>