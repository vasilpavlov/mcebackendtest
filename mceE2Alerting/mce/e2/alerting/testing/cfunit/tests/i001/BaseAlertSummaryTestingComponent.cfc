<cfcomponent name="Base Alert Summary Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the alertSummary delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeAlertTestData"
				access="private"
				hint="This method is used to purge / delete any alert data related to the test data being used when alert unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="alerts" type="query" hint="This argument is used to identify the alert data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alert data --->
		<cfquery name="local.purgeData" datasource="#this.alertDelegate.datasource#">

			delete
			from	dbo.alerts
			where	recipient_address in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newAlerts.recipient_address)#">

					)
		
			delete
			from	dbo.alerts
			where	recipient_address in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedAlerts.recipient_address)#">

					)		
		
		</cfquery>
			
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the alert service and delegate tests.">
		
		<cfscript>
		
			//Build out the alert test data (for create / modify scenarios)
			createAlertTestData();

		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createAlertTestData"
				access="private"
				hint="This method is used to create the test data for testing alert records (create and update actions).">
	
		<cfscript>			
			// Initialize the foreign key values
			this.testData.alertSubscriptions.client_company_uid = "2355C5B2-EF6B-4E19-B666-7ADFAA6B081C";
			this.testData.alertSubscriptions.property_uid = "5A384FD7-34AA-4BE9-B2B2-000B41E87043";
			this.testData.alertSubscriptions.alert_type_code = "CompletedUsage";
			this.testData.alertSubscriptions.alert_status_code = "Pending";
			
			// Initialize a query containing alert data to be tested with (the query should contain all of the columns of the base table)
			this.newAlerts = queryNew('alert_uid,client_company_uid,property_uid,alert_type_code,alert_status_code,recipient_address,alert_content,alert_generated_date,alert_due_date,alert_visibility_date,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newAlerts,4);
		
			// Set the cell values for this instance of a alert
			querySetCell(this.newAlerts,'alert_uid', this.alertDelegate.createUniqueIdentifier(), 1);
			querySetCell(this.newAlerts,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 1);
			querySetCell(this.newAlerts,'property_uid', this.testData.alertSubscriptions.property_uid, 1);
			querySetCell(this.newAlerts,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 1);
			querySetCell(this.newAlerts,'alert_status_code', this.testData.alertSubscriptions.alert_status_code, 1);
			querySetCell(this.newAlerts,'recipient_address', 'test@test.com', 1);
			querySetCell(this.newAlerts,'alert_content', 'Alert Content 1', 1);
			querySetCell(this.newAlerts,'alert_generated_date', '2000-01-01 00:00:00.0', 1);
			querySetCell(this.newAlerts,'alert_due_date', '2000-01-01 00:00:00.0', 1);
			querySetCell(this.newAlerts,'alert_visibility_date', '2000-01-01 00:00:00.0', 1);
			querySetCell(this.newAlerts,'is_active', 1, 1);
			querySetCell(this.newAlerts,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlerts,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newAlerts,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlerts,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newAlerts,'alert_uid', this.alertDelegate.createUniqueIdentifier(), 2);
			querySetCell(this.newAlerts,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 2);
			querySetCell(this.newAlerts,'property_uid', this.testData.alertSubscriptions.property_uid, 2);
			querySetCell(this.newAlerts,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 2);
			querySetCell(this.newAlerts,'alert_status_code', this.testData.alertSubscriptions.alert_status_code, 2);
			querySetCell(this.newAlerts,'recipient_address', 'test@test.com', 2);
			querySetCell(this.newAlerts,'alert_content', 'Alert Content 2', 2);
			querySetCell(this.newAlerts,'alert_generated_date', '2000-01-01 00:00:00.0', 2);
			querySetCell(this.newAlerts,'alert_due_date', '2000-01-01 00:00:00.0', 2);
			querySetCell(this.newAlerts,'alert_visibility_date', '2000-01-01 00:00:00.0', 2);
			querySetCell(this.newAlerts,'is_active', 1, 2);
			querySetCell(this.newAlerts,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlerts,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newAlerts,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlerts,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newAlerts,'alert_uid', this.alertDelegate.createUniqueIdentifier(), 3);
			querySetCell(this.newAlerts,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 3);
			querySetCell(this.newAlerts,'property_uid', this.testData.alertSubscriptions.property_uid, 3);
			querySetCell(this.newAlerts,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 3);
			querySetCell(this.newAlerts,'alert_status_code', this.testData.alertSubscriptions.alert_status_code, 3);
			querySetCell(this.newAlerts,'recipient_address', 'test@test.com', 3);
			querySetCell(this.newAlerts,'alert_content', 'Alert Content 3', 3);
			querySetCell(this.newAlerts,'alert_generated_date', '2000-01-01 00:00:00.0', 3);
			querySetCell(this.newAlerts,'alert_due_date', '2000-01-01 00:00:00.0', 3);
			querySetCell(this.newAlerts,'alert_visibility_date', '2000-01-01 00:00:00.0', 3);
			querySetCell(this.newAlerts,'is_active', 1, 3);
			querySetCell(this.newAlerts,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlerts,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newAlerts,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlerts,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newAlerts,'alert_uid', this.alertDelegate.createUniqueIdentifier(), 4);
			querySetCell(this.newAlerts,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 4);
			querySetCell(this.newAlerts,'property_uid', this.testData.alertSubscriptions.property_uid, 4);
			querySetCell(this.newAlerts,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 4);
			querySetCell(this.newAlerts,'alert_status_code', this.testData.alertSubscriptions.alert_status_code, 4);
			querySetCell(this.newAlerts,'recipient_address', 'test@test.com', 4);
			querySetCell(this.newAlerts,'alert_content', 'Alert Content 4', 4);
			querySetCell(this.newAlerts,'alert_generated_date', '2000-01-01 00:00:00.0', 4);
			querySetCell(this.newAlerts,'alert_due_date', '2000-01-01 00:00:00.0', 4);
			querySetCell(this.newAlerts,'alert_visibility_date', '2000-01-01 00:00:00.0', 4);
			querySetCell(this.newAlerts,'is_active', 1, 4);
			querySetCell(this.newAlerts,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlerts,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newAlerts,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlerts,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of alerts without primary keys
			this.newAlertsWithoutPks = duplicate(this.newAlerts);
			
			// Remote the primary keys from the alert test data 
			querySetCell(this.newAlertsWithoutPks,'alert_uid', '', 1);
			querySetCell(this.newAlertsWithoutPks,'alert_uid', '', 2);
			querySetCell(this.newAlertsWithoutPks,'alert_uid', '', 3);
			querySetCell(this.newAlertsWithoutPks,'alert_uid', '', 4);
					
			// Duplicate the new alerts to create the baseline modified alerts
			this.modifiedAlerts = duplicate(this.newAlerts);

			// Update the friendly names for the modified alerts
			querySetCell(this.modifiedAlerts,'recipient_address', 'modified@test.com', 1);
			querySetCell(this.modifiedAlerts,'recipient_address', 'modified@test.com', 2);
			querySetCell(this.modifiedAlerts,'recipient_address', 'modified@test.com', 3);			
			querySetCell(this.modifiedAlerts,'recipient_address', 'modified@test.com', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedAlerts,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedAlerts,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedAlerts,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedAlerts,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareAlertSummaryProperties"
				access="private"
				hint="This method is used to compare the properties for a given alert vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of alert vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the alert from the application database --->
				<cfset local.retrievedAlertSummary = this.alertDelegate.getAlertSummaryAsComponent(arguments.sourceObject[local.arrayIndex].alert_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the alert from the application database --->
				<cfset local.retrievedAlertSummary = this.alertSummaryService.getAlertSummary(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared alertSummary VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedAlertSummary, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved alertSummary VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.alertDelegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedAlert[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedAlert[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>