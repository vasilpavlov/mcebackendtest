<cfcomponent name="Base Alert Status Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the alert Status delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeAlertStatusTestData"
				access="private"
				hint="This method is used to purge / delete any alertStatus data related to the test data being used when alertStatus unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="alertStatuses" type="query" hint="This argument is used to identify the alertStatus data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertStatus data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertStatuses
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newAlertStatuses.friendly_name)#">

					)
		
			delete
			from	dbo.alertStatuses
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedAlertStatuses.friendly_name)#">

					)		
		
		</cfquery>
			
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the alertStatus service and delegate tests.">
		
		<cfscript>
		
			//Build out the alertStatus test data (for create / modify scenarios)
			createAlertStatusTestData();

		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createAlertStatusTestData"
				access="private"
				hint="This method is used to create the test data for testing alertStatus records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing alertStatus data to be tested with (the query should contain all of the columns of the base table)
			this.newAlertStatuses = queryNew('alert_status_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newAlertStatuses,4);
		
			// Set the cell values for this instance of a alertStatus
			querySetCell(this.newAlertStatuses,'alert_status_code', 'AlertStatus001', 1);
			querySetCell(this.newAlertStatuses,'friendly_name', 'AlertStatus 001', 1);
			querySetCell(this.newAlertStatuses,'is_active', 1, 1);
			querySetCell(this.newAlertStatuses,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertStatuses,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newAlertStatuses,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertStatuses,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newAlertStatuses,'alert_status_code', 'AlertStatus002', 2);
			querySetCell(this.newAlertStatuses,'friendly_name', 'AlertStatus 002', 2);
			querySetCell(this.newAlertStatuses,'is_active', 1, 2);
			querySetCell(this.newAlertStatuses,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertStatuses,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newAlertStatuses,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertStatuses,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newAlertStatuses,'alert_status_code', 'AlertStatus003', 3);
			querySetCell(this.newAlertStatuses,'friendly_name', 'AlertStatus 003', 3);
			querySetCell(this.newAlertStatuses,'is_active', 1, 3);
			querySetCell(this.newAlertStatuses,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertStatuses,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newAlertStatuses,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertStatuses,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newAlertStatuses,'alert_status_code', 'AlertStatus004', 4);
			querySetCell(this.newAlertStatuses,'friendly_name', 'AlertStatus 004', 4);
			querySetCell(this.newAlertStatuses,'is_active', 1, 4);
			querySetCell(this.newAlertStatuses,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertStatuses,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newAlertStatuses,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertStatuses,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of alertStatuses without primary keys
			this.newAlertStatusesWithoutPks = duplicate(this.newAlertStatuses);
			
			// Remote the primary keys from the alertStatus test data 
			querySetCell(this.newAlertStatusesWithoutPks,'alert_status_code', '', 1);
			querySetCell(this.newAlertStatusesWithoutPks,'alert_status_code', '', 2);
			querySetCell(this.newAlertStatusesWithoutPks,'alert_status_code', '', 3);
			querySetCell(this.newAlertStatusesWithoutPks,'alert_status_code', '', 4);
					
			// Duplicate the new alertStatuses to create the baseline modified alertStatuses
			this.modifiedAlertStatuses = duplicate(this.newAlertStatuses);

			// Update the friendly names for the modified alertStatuses
			querySetCell(this.modifiedAlertStatuses,'friendly_name', 'AlertStatus M 001', 1);
			querySetCell(this.modifiedAlertStatuses,'friendly_name', 'AlertStatus M 002', 2);
			querySetCell(this.modifiedAlertStatuses,'friendly_name', 'AlertStatus M 003', 3);			
			querySetCell(this.modifiedAlertStatuses,'friendly_name', 'AlertStatus M 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedAlertStatuses,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedAlertStatuses,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedAlertStatuses,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedAlertStatuses,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareAlertStatusProperties"
				access="private"
				hint="This method is used to compare the properties for a given alertStatus vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of alertStatus vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the alertStatus from the application database --->
				<cfset local.retrievedAlertStatus = this.delegate.getAlertStatusAsComponent(arguments.sourceObject[local.arrayIndex].alert_status_code)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the alertStatus from the application database --->
				<cfset local.retrievedAlertStatus = this.service.getAlertStatus(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared alertStatus VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedAlertStatus, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved alertStatus VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedAlertStatus[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedAlertStatus[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>