<cfcomponent name="Test Alert Interval Service"
			 extends="BaseAlertIntervalTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the alertIntervalService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')>
		<cfset init()>
	</cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the alertInterval service
			this.service = request.beanFactory.getBean("AlertIntervalService");
			this.delegate = request.beanFactory.getBean("AlertIntervalDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyAlertIntervalComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty alertInterval component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertInterval delegate --->
		<cfset local.alertInterval = this.service.getEmptyAlertIntervalVo()>

		<!--- Assert that the alertInterval object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.alertInterval, "mce.e2.alerting.vo.alertInterval"), true, "local.alertInterval objectType is not of type mce.e2.alerting.vo.AlertInterval.")>
			
	</cffunction>

	<cffunction name="getAlertIntervals"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all alertIntervals from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alert delegate --->
		<cfset local.alertIntervalCollection = this.service.getAlertIntervals()>
				
		<!--- Assert that the alertInterval array collection is an array --->
		<cfset assertTrue(isArray(local.alertIntervalCollection), "The value returned by this.service.getAlertIntervals() was not an array.")>

		<!--- Compare the alertInterval properties and excersize retrieving single instances of alertIntervals --->
		<cfset comparealertIntervalProperties(local.alertIntervalCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getAlertInterval"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single alertInterval from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertInterval delegate --->
		<cfset local.alertIntervalCollection = this.service.getAlertIntervals()>
				
		<!--- Loop through the alertInterval collection, and retrieve each alertInterval individually --->
		<cfloop from="1" to="#arraylen(local.alertIntervalCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the alertInterval to test --->
			<cfset local.alertInterval = local.alertIntervalCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a alertInterval from the service --->
				<cfset local.testAlertInterval = this.service.getalertInterval(local.alertInterval)>

			<!--- Assert that the alertInterval array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testAlertInterval, "mce.e2.alerting.vo.AlertInterval"), true, "local.testAlertInterval objectType is not of type mce.e2.alerting.vo.AlertInterval.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of alertInterval data --->
	<cffunction name="saveAndValidateNewAlertIntervals"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveNewAlertInterval() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertIntervals = this.delegate.queryToVoArray(this.newAlertIntervals, 'mce.e2.alerting.vo.AlertInterval')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the alertInterval test data --->	
		<cfset purgeAlertIntervalTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertIntervals)#" index="local.arrayIndex">	

			<!--- Save the alertInterval to the application database --->
			<cfset local.testAlertIntervalPrimaryKey = this.service.saveNewAlertInterval(local.newAlertIntervals[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<!---
			Value is a code, not a uid
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertIntervalPrimaryKey), "The value returned by this.service.saveNewAlertInterval() was not a unique identifier.")/>
			--->
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertIntervals), "The total number of records processed does not match the total number of records iterated over when creating alertInterval records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertIntervalProperties(sourceObject=local.newAlertIntervals, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertIntervalTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewAlertIntervals"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveAndReturnNewAlertInterval() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test alertIntervals array --->			
		<cfset local.testAlertIntervals = arrayNew(1)>			
						
		<!--- Build out the array of alertInterval objects --->
		<cfset local.newAlertIntervals = this.delegate.queryToVoArray(this.newAlertIntervals, 'mce.e2.alerting.vo.AlertInterval')/>
		
		<!--- Purge / remote the alertInterval test data --->	
		<cfset purgeAlertIntervalTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertIntervals)#" index="local.arrayIndex">	

			<!--- Save the alertInterval to the application database --->
			<cfset local.testAlertInterval = this.service.saveAndReturnNewAlertInterval(local.newAlertIntervals[local.arrayIndex])/>
			
			<!--- Assert that the alertInterval object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testAlertInterval, "mce.e2.alerting.vo.AlertInterval"), true, "local.alertInterval objectType is not of type mce.e2.alerting.vo.AlertInterval.")>
						
			<!--- Append the alertInterval to the test array --->			
			<cfset arrayAppend(local.testAlertIntervals, local.testAlertInterval)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testAlertIntervals), arrayLen(local.newAlertIntervals), "The total number of records processed does not match the total number of records iterated over when creating alertInterval records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertIntervalProperties(sourceObject=local.testAlertIntervals, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertIntervalTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingAlertIntervals"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of alertIntervals.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alertInterval objects --->
		<cfset local.newAlertIntervals = this.delegate.queryToVoArray(this.newAlertIntervals, 'mce.e2.alerting.vo.AlertInterval')/>
		<cfset local.modifiedAlertIntervals = this.delegate.queryToVoArray(this.modifiedAlertIntervals, 'mce.e2.alerting.vo.AlertInterval')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the alertInterval test data --->	
		<cfset purgeAlertIntervalTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertIntervals)#" index="local.arrayIndex">	

			<!--- Save the alertInterval to the application database --->
			<cfset this.service.saveNewAlertInterval(local.newAlertIntervals[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertIntervals), "The total number of records processed does not match the total number of records iterated over when creating alertInterval records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertIntervalProperties(sourceObject=local.newAlertIntervals, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedAlertIntervals)#" index="local.arrayIndex">	

			<!--- Save the alertInterval to the application database --->
			<cfset this.service.saveExistingAlertInterval(local.modifiedAlertIntervals[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedAlertIntervals), "The total number of records processed does not match the total number of records iterated over when modifying alertInterval records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertIntervalProperties(sourceObject=local.modifiedAlertIntervals, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertIntervalTestData()> 			
							
	</cffunction>	

</cfcomponent>