<cfcomponent name="Test Alert Status Service"
			 extends="BaseAlertStatusTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the alertStatusService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')>
		<cfset init()>
	</cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the alertStatus service
			this.service = request.beanFactory.getBean("AlertStatusService");
			this.delegate = request.beanFactory.getBean("AlertStatusDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyAlertStatusComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty alertStatus component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertStatus delegate --->
		<cfset local.alertStatus = this.service.getEmptyAlertStatusVo()>

		<!--- Assert that the alertStatus object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.alertStatus, "mce.e2.alerting.vo.alertStatus"), true, "local.alertStatus objectType is not of type mce.e2.alerting.vo.AlertStatus.")>
			
	</cffunction>

	<cffunction name="getAlertStatuses"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all alertStatuss from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alert delegate --->
		<cfset local.alertStatusCollection = this.service.getAlertStatuses()>
				
		<!--- Assert that the alertStatus array collection is an array --->
		<cfset assertTrue(isArray(local.alertStatusCollection), "The value returned by this.service.getAlertStatuss() was not an array.")>

		<!--- Compare the alertStatus properties and excersize retrieving single instances of alertStatuss --->
		<cfset comparealertStatusProperties(local.alertStatusCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getAlertStatus"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single alertStatus from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertStatus delegate --->
		<cfset local.alertStatusCollection = this.service.getAlertStatuses()>
				
		<!--- Loop through the alertStatus collection, and retrieve each alertStatus individually --->
		<cfloop from="1" to="#arraylen(local.alertStatusCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the alertStatus to test --->
			<cfset local.alertStatus = local.alertStatusCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a alertStatus from the service --->
				<cfset local.testAlertStatus = this.service.getalertStatus(local.alertStatus)>

			<!--- Assert that the alertStatus array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testAlertStatus, "mce.e2.alerting.vo.AlertStatus"), true, "local.testAlertStatus objectType is not of type mce.e2.alerting.vo.AlertStatus.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of alertStatus data --->
	<cffunction name="saveAndValidateNewAlertStatuss"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveNewAlertStatus() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertStatuses = this.delegate.queryToVoArray(this.newAlertStatuses, 'mce.e2.alerting.vo.AlertStatus')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the alertStatus test data --->	
		<cfset purgeAlertStatusTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertStatuses)#" index="local.arrayIndex">	

			<!--- Save the alertStatus to the application database --->
			<cfset local.testAlertStatusPrimaryKey = this.service.saveNewAlertStatus(local.newAlertStatuses[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<!---
			Value is a code, not a uid
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertStatusPrimaryKey), "The value returned by this.service.saveNewAlertStatus() was not a unique identifier.")/>
			--->
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertStatuses), "The total number of records processed does not match the total number of records iterated over when creating alertStatus records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertStatusProperties(sourceObject=local.newAlertStatuses, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertStatusTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewAlertStatuses"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveAndReturnNewAlertStatus() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test alertStatuses array --->			
		<cfset local.testAlertStatuses = arrayNew(1)>			
						
		<!--- Build out the array of alertStatus objects --->
		<cfset local.newAlertStatuses = this.delegate.queryToVoArray(this.newAlertStatuses, 'mce.e2.alerting.vo.AlertStatus')/>
		
		<!--- Purge / remote the alertStatus test data --->	
		<cfset purgeAlertStatusTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertStatuses)#" index="local.arrayIndex">	

			<!--- Save the alertStatus to the application database --->
			<cfset local.testAlertStatus = this.service.saveAndReturnNewAlertStatus(local.newAlertStatuses[local.arrayIndex])/>
			
			<!--- Assert that the alertStatus object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testAlertStatus, "mce.e2.alerting.vo.AlertStatus"), true, "local.alertStatus objectType is not of type mce.e2.alerting.vo.AlertStatus.")>
						
			<!--- Append the alertStatus to the test array --->			
			<cfset arrayAppend(local.testAlertStatuses, local.testAlertStatus)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testAlertStatuses), arrayLen(local.newAlertStatuses), "The total number of records processed does not match the total number of records iterated over when creating alertStatus records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertStatusProperties(sourceObject=local.testAlertStatuses, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertStatusTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingAlertStatuses"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of alertStatuses.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alertStatus objects --->
		<cfset local.newAlertStatuses = this.delegate.queryToVoArray(this.newAlertStatuses, 'mce.e2.alerting.vo.AlertStatus')/>
		<cfset local.modifiedAlertStatuses = this.delegate.queryToVoArray(this.modifiedAlertStatuses, 'mce.e2.alerting.vo.AlertStatus')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the alertStatus test data --->	
		<cfset purgeAlertStatusTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertStatuses)#" index="local.arrayIndex">	

			<!--- Save the alertStatus to the application database --->
			<cfset this.service.saveNewAlertStatus(local.newAlertStatuses[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertStatuses), "The total number of records processed does not match the total number of records iterated over when creating alertStatus records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertStatusProperties(sourceObject=local.newAlertStatuses, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedAlertStatuses)#" index="local.arrayIndex">	

			<!--- Save the alertStatus to the application database --->
			<cfset this.service.saveExistingAlertStatus(local.modifiedAlertStatuses[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedAlertStatuses), "The total number of records processed does not match the total number of records iterated over when modifying alertStatus records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertStatusProperties(sourceObject=local.modifiedAlertStatuses, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertStatusTestData()> 			
							
	</cffunction>	

</cfcomponent>