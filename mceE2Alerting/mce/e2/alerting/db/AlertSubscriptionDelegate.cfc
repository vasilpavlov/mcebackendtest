
<!---

  Template Name:  AlertSubscriptionDelegate
     Base Table:  AlertSubscriptions

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, July 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, July 16, 2009 - Template Created.

--->

<cfcomponent	displayname="AlertSubscriptionDelegate"
				hint="This CFC manages all data access interactions for the AlertSubscriptions table including date creation, modification, and association activities."
				output="true"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction	name="getEmptyAlertSubscriptionComponent"
				hint="This method is used to retrieve a Alert Subscription object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.alerting.vo.AlertSubscription"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.alerting.vo.AlertSubscription")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction	name="getAlertSubscriptionsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert Subscription objects for all the Alert Subscriptions in the application."
				output="true"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given alert."/>
		<cfargument name="alert_type_code" required="false" type="string" hint="Describes the type code / internal identifier for a given alert."/>
		<cfargument name="alert_uid" required="false" type="string" hint="A list of alert UID(s) for which to get subscriptions."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alert Subscriptions and build out the array of components --->
		<cfset local.qResult = getAlertSubscription(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.AlertSubscription")/>

		<cfloop array="#local.returnResult#" index="local.item">
			<cfset decorateAlertSubscriptionWithRelatedData(local.item)>
		</cfloop>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>



	<cffunction	name="getAlertSubscriptionAsComponent"
				hint="This method is used to retrieve a single instance of a / an Alert Subscription value object representing a single Alert Subscription record."
				output="false"
				returnType="mce.e2.alerting.vo.AlertSubscription"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_subscription_uid" required="true" type="string"/>
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given alert."/>
		<cfargument name="alert_type_code" required="false" type="string" hint="Describes the type code / internal identifier for a given alert."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert Subscription and build out the component --->
		<cfset local.qResult = getAlertSubscription(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.alerting.vo.AlertSubscription")/>

		<!--- Related information (conditions, users, energy accounts...) --->
		<cfset decorateAlertSubscriptionWithRelatedData(local.returnResult)>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>


	<cffunction name="decorateAlertSubscriptionWithRelatedData" returntype="void" output="false">
		<cfargument name="alertSubscription" type="mce.e2.alerting.vo.AlertSubscription" required="true">

		<cfset alertSubscription.conditions = getConditionsForAlertSubscription(alertSubscription.alert_subscription_uid)>
		<cfset alertSubscription.users = getUsersForAlertSubscription(alertSubscription.alert_subscription_uid)>
		<cfset alertSubscription.accounts = getEnergyAccountsForAlertSubscription(alertSubscription.alert_subscription_uid)>
		<cfset alertSubscription.lastGeneratedDates = getLastGeneratedDates(alertSubscription.alert_subscription_uid)>
	</cffunction>


	<cffunction	name="getAlertSubscriptionsByIntervals"
				hint="This method is used to retrieve single / multiple records from the AlertSubscriptions table based on the alert_interval_code."
				output="true"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_interval_codes" required="true" type="string"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve Alert Subscription record(s) --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
			select	asub.alert_subscription_uid
			from	dbo.AlertSubscriptions asub (nolock),
					dbo.AlertTypes atype (nolock),
					dbo.AlertTypes_AlertIntervals atai (nolock)
			where	1=1
			and atai.alert_interval_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_interval_codes#" null="false" list="true"> )
			and atype.alert_type_code = atai.alert_type_code
			and atype.alert_type_code = asub.alert_type_code
			and asub.is_active = 1
		</cfquery>

		<!--- Convert the results to components --->
		<cfif local.qResult.recordCount gt 0>
			<cfset local.aResult = getAlertSubscriptionsAsArrayOfComponents(alert_subscription_uid=valueList(local.qResult.alert_subscription_uid))>
		<cfelse>
			<cfset local.aResult = ArrayNew(1)>
		</cfif>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.aResult>

	</cffunction>

	<cffunction	name="getAlertSubscription"
				hint="This method is used to retrieve single / multiple records from the AlertSubscriptions table."
				output="true"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="alert_delivery_method_code" required="false" type="string"/>
		<cfargument name="user_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given alert."/>
		<cfargument name="alert_type_code" required="false" type="string" hint="Describes the type code / internal identifier for a given alert."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name of a given alert."/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>
		<cfargument name="alert_uid" required="false" type="string" hint="A list of alert UID(s) for which to get subscriptions."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structNew()>

		<!--- Retrieve a single / multiple Alert Subscription records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the AlertSubscriptions table matching the where clause
			select	tbl.alert_subscription_uid,
					tbl.property_uid,
					tbl.alert_type_code,
					tbl.alert_digest_code,
					tbl.friendly_name,
					tbl.profile_lookup_code,
					tbl.is_active,
					tbl.last_polled,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					types.cfc_name as alertTypeCfcName,
					p.friendly_name as propertyFriendlyName

			from	dbo.AlertSubscriptions tbl (nolock) JOIN
					dbo.AlertTypes types (nolock) ON tbl.alert_type_code = types.alert_type_code JOIN
					dbo.Properties p (nolock) ON p.property_uid = tbl.property_uid
			where	1=1

					<cfif structKeyExists(arguments, 'alert_subscription_uid')>
					and tbl.alert_subscription_uid in ( select list.alert_subscription_uid from dbo.AlertSubscriptions (nolock) list where list.alert_subscription_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" null="false" list="true"> ))
					</cfif>

					<cfif structKeyExists(arguments, 'alert_delivery_method_code')>
					and tbl.alert_subscription_uid in
						(
							select	alert_subscription_uid
							from	AlertSubscriptions_AlertDeliveryMethods
							where	alert_delivery_method_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_delivery_method_code#" list="true" null="false">
						)
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_type_code')>
					and tbl.alert_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_type_code#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_uid')>
					and tbl.alert_subscription_uid IN
						(SELECT aa.alert_subscription_uid FROM Alerts
						WHERE alert_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_uid#" list="true">)
						)
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" list="true" null="false"> )
					<cfelse>
					and tbl.is_active in ( 1 )
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="true"> )
					</cfif>

		</cfquery>


		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>


	<cffunction name="getConditionsForAlertSubscription" returntype="array" output="false">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var local = StructNew()>

		<!--- Get a value for each condition type; the specific numeric value from subscription if available; otherwise default from alert-type level --->
		<cfquery datasource="#this.datasource#" name="local.qResult">
			SELECT
				ctypes.alert_condition_code,
				ISNULL(
					(SELECT cond.numeric_value FROM AlertSubscriptions_Conditions cond (nolock)
					 WHERE cond.alert_condition_code = ctypes.alert_condition_code
					 AND cond.alert_subscription_uid = subs.alert_subscription_uid),
				ctypes.default_value) as numeric_value
			FROM
				dbo.AlertSubscriptions subs (nolock) JOIN
				dbo.AlertConditionTypes ctypes (nolock) ON ctypes.alert_type_code = subs.alert_type_code
			WHERE
				subs.alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#">
			ORDER BY
				ctypes.alert_condition_code
		</cfquery>

		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.AlertConditionsType")/>

		<cfreturn local.returnResult>
	</cffunction>


	<cffunction name="getUsersForAlertSubscription" returntype="array" output="false">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var local = StructNew()>

		<cfquery datasource="#this.datasource#" name="local.qResult">
			SELECT
				u.*
			FROM
				AlertSubscriptions_Users asu (nolock) JOIN
				Users u (nolock) ON asu.user_uid = u.user_uid
			WHERE
				asu.alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#">
			ORDER BY
				u.username
		</cfquery>

		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.User")/>

		<cfreturn local.returnResult>
	</cffunction>


	<cffunction name="getEnergyAccountsForAlertSubscription" returntype="array" output="false">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var local = StructNew()>

		<cfquery datasource="#this.datasource#" name="local.qResult">
			SELECT
				ea.energy_account_uid
				,ea.friendly_name
				,ea.property_uid
				,ea.energy_type_uid
				,ea.meta_group_uid
				,ea.energy_unit_uid
				,ea.factor_alias_uid_override
				,ea.is_active
				,ea.created_by
				,ea.created_date
				,ea.modified_by, ea.modified_date
				,ea.master_energy_account_uid
				,ea.account_type_uid
				,ea.notes_text
				,ea.currentEnergyProviderUid
			FROM
				AlertSubscriptions_EnergyAccounts asa (nolock) JOIN
				EnergyAccounts ea (nolock) ON asa.energy_account_uid = ea.energy_account_uid
			WHERE
				asa.alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#">
			ORDER BY
				ea.friendly_name
		</cfquery>

		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.EnergyAccount")/>

		<cfreturn local.returnResult>
	</cffunction>


	<!---Describes the methods used to persist / modify Alert Subscription data. --->
	<cffunction	name="saveNewAlertSubscription"
				hint="This method is used to persist a new Alert Subscription record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription" hint="Describes the VO containing the Alert Subscription record that will be persisted."/>

		<!--- Persist the Alert Subscription data to the application database --->
		<cfset local.subscription_id = this.dataMgr.insertRecord("AlertSubscriptions", arguments.AlertSubscription)/>

		<!--- loop through users and insert --->
		<cfset commitUsers(local.subscription_id, arguments.AlertSubscription.users) />
		<!--- loop through accounts and insert --->
		<cfset commitAccounts( local.subscription_id, arguments.AlertSubscription.accounts ) />
		<!--- loop through conditions and insert --->
		<cfset commitConditions( local.subscription_id, arguments.AlertSubscription.conditions ) />
	</cffunction>


	<cffunction name="commitUsers" access="private" returntype="any">
		<cfargument name="subscription_id" type="string" required="true" />
		<cfargument name="users" type="array" required="true" />
		<cfset var local = structNew() />

		<cfquery name="local.qDelete" datasource="#this.datasource#">
			delete from AlertSubscription_User
			where alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscription_id#">
		</cfquery>

		<cfloop from="1" to="#arrayLen(arguments.users)#" index="local.i">
			<cfquery name="local.qInsertUser" datasource="#this.datasource#">
				insert AlertSubscription_User
				(
					alert_subscription_uid,
					user_uid
				)
				values
				(
					<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscription_id#">,
					<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.users[local.i].user_uid#">
				)
			</cfquery>
		</cfloop>
	</cffunction>

	<cffunction name="commitAccounts" access="private" returntype="any">
		<cfargument name="subscription_id" type="string" required="true" />
		<cfargument name="accounts" type="array" required="true" />

		<cfset var local = structNew() />
		<cfquery name="local.qDelete" datasource="#this.datasource#">
			delete from AlertSubscription_EnergyAccounts
			where
				alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscription_id#">
		</cfquery>

		<cfloop from="1" to="#arrayLen(arguments.accounts)#" index="local.i">

			<cfquery name="local.qInsertAccount" datasource="#this.datasource#">
				insert AlertSubscription_EnergyAccounts
				(
					alert_subscription_uid,
					energy_account_uid
				)
				values
				(
					<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscription_id#">,
					<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.accounts[local.i].energy_account_uid#">
				)
			</cfquery>
		</cfloop>
	</cffunction>

	<cffunction name="commitConditions" access="private" returntype="any">
		<cfargument name="subscription_id" type="string" required="true" />
		<cfargument name="conditions" type="array" required="true" />
		<cfset var local = structNew() />
		<cfquery name="local.qDelete" datasource="#this.datasource#">
			delete from AlertSubscriptions_Conditions
			where
				alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscription_id#">
		</cfquery>

		<cfloop from="1" to="#arrayLen(arguments.conditions)#" index="local.i">
			<cfquery name="local.qInsertConditions" datasource="#this.datasource#">
				insert AlertSubscriptions_Conditions
				(
					alert_subscription_uid,
					alert_condition_code,
					condition_value
				)
				values
				(
					<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscription_id#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.conditions[local.i].alert_condition_code#">,
					<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.conditions[local.i].condition_value#">
				)
			</cfquery>
		</cfloop>
	</cffunction>

	<cffunction	name="saveExistingAlertSubscription"
				hint="This method is used to persist an existing Alert Subscription record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription" hint="Describes the VO containing the Alert Subscription record that will be persisted."/>

		<!--- Persist the Alert Subscription data to the application database --->
		<cfset this.dataMgr.updateRecord("AlertSubscriptions", arguments.AlertSubscription)/>

	</cffunction>

	<cffunction	name="saveNewAlertSubscriptionDeliveryMethods"
				hint="This method is used to persist new Alert Subscription Delivery Method records to the application database."
				output="false"
				returnType="boolean"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription"/>
		<cfargument name="alert_delivery_method_code" required="true" type="string" hint="Comma-delimeted string of alert_delivery_method_codes to be added to the application database."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Persist the Alert Subscription Delivery Method data to the application database --->
		<cfloop list="#arguments.alert_delivery_method_code#" index="local.i">
			<cfset local.insertParams.alert_subscription_uid = arguments.AlertSubscription.alert_subscription_uid>
			<cfset local.insertParams.alert_delivery_method_code = local.i>
			<cfset this.dataMgr.insertRecord("AlertSubscriptions_AlertDeliveryMethods", local.insertParams)/>
		</cfloop>

		<cfreturn true>
	</cffunction>


	<!---Update the last-polled date for the interval --->
	<cffunction	name="updateLastPolledDate"
				hint="This method is used to update the last-polled date of the subscription (to 'now')."
				output="false"
				returnType="boolean"
				access="public">

		<cfargument name="alert_subscription_uid" required="true" type="string"/>

		<cfset var sqlResult = "">

		<cfquery result="sqlResult" datasource="#this.datasource#">
			UPDATE dbo.AlertSubscriptions
			SET last_polled = getDate()
			WHERE alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
		</cfquery>

		<cfreturn sqlResult.recordCount eq 1>
	</cffunction>



	<cffunction	name="deleteAlertSubscriptionDeliveryMethods"
				hint="This method is used to delete an Alert Subscription Delivery Method record from the application database."
				output="false"
				returnType="boolean"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription"/>
		<cfargument name="alert_delivery_method_code" required="true" type="string" hint="Comma-delimeted string of alert_delivery_method_codes to be deleted from the application database."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single / multiple Alert Subscription records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- delete records from the AlertSubscriptions_AlertDeliveryMethods table matching the where clause
			delete	dbo.AlertSubscriptions_AlertDeliveryMethods
			where	alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.AlertSubscription.alert_subscription_uid#" null="false" list="false">
			and		alert_delivery_method_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_delivery_method_code#" null="false" list="true"> )

		</cfquery>

		<cfreturn true>
	</cffunction>


	<!---Update the last-polled date for the interval --->
	<cffunction	name="getLastGeneratedDates"
				hint="Retrieves the last-generated date for each account for a subscription, and returns as a structure (keyed by the account id)."
				output="false"
				returnType="struct"
				access="public">

		<cfargument name="alert_subscription_uid" required="true" type="string"/>

		<cfset var qResult = "">
		<cfset var result = StructNew()>

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				asa.energy_account_uid,
				asa.last_generated
			FROM AlertSubscriptions_EnergyAccounts asa
			WHERE asa.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
			ORDER BY asa.energy_account_uid
		</cfquery>

		<cfloop query="qResult">
			<cfset result[qResult.energy_account_uid] = qResult.last_generated>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<!---Update the last-polled date for the interval --->
	<cffunction	name="updateLastGeneratedDate"
				hint="This method is used to update the last-generated date of a subscription-to-account record (to 'now')."
				output="false"
				returnType="boolean"
				access="public">

		<cfargument name="alert_subscription_uid" required="true" type="string"/>
		<cfargument name="energy_account_uid" required="true" type="string"/>

		<cfset var sqlResult = "">

		<cfquery result="sqlResult" datasource="#this.datasource#">
			UPDATE dbo.AlertSubscriptions_EnergyAccounts
			SET last_generated = getDate()
			WHERE alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
			AND energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" list="true">)
		</cfquery>

		<cfreturn sqlResult.recordCount eq 1>
	</cffunction>



</cfcomponent>
