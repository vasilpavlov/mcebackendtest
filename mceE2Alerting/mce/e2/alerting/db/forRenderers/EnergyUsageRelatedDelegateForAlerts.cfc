<cfcomponent extends="mce.e2.alerting.db.BaseDatabaseDelegate" output="false">


	<cffunction name="getSubcriptionsThatHaveChangedEnergyUsageRecordsSinceSubscriptionWasLastPolled">
		<cfargument name="energy_subscription_uid_list" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			select
				DISTINCT sub.alert_subscription_uid
			FROM
				AlertSubscriptions sub (nolock)
				INNER JOIN
				AlertSubscriptions_EnergyAccounts subea (nolock) ON sub.alert_subscription_uid = subea.alert_subscription_uid
				INNER JOIN
				/* EnergyUsageView euv (nolock)
				ON euv.energy_account_uid = subea.energy_account_uid
					AND euv.consider_touched_date > ISNULL(sub.last_polled, '1/1/1970') */
				EnergyUsage eu (nolock)
					ON eu.energy_account_uid = subea.energy_account_uid
					AND eu.consider_touched_date > ISNULL(sub.last_polled, '1/1/1970')
			WHERE
				/* euv.data_is_reportable = 1
				AND euv.is_active = 1 */
				usage_status_code in(select usage_status_code from EnergyUsageStatuses eus where eus.data_is_reportable = 1)
				AND eu.is_active = 1
				AND sub.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_subscription_uid_list#" list="true">)
			ORDER BY
				sub.alert_subscription_uid ASC
		</cfquery>

		<cfreturn ValueList(qResult.alert_subscription_uid)>
	</cffunction>

	<cffunction name="getEnergyUsageDataForUsageAlerts">
		<cfargument name="energy_account_uid_list" type="string" required="true">
		<cfargument name="fromDate" type="date" required="false">
		<cfargument name="thruDate" type="date" required="false">
		<cfargument name="profile_lookup_code" type="string" required="true">
		<cfargument name="usage_type_code" type="string" required="false" default="actual">
		<cfargument name="include_budgeted_cost" type="boolean" required="false" default="false">
		<cfargument name="select_reportable_records_only" type="boolean" required="false" default="true">

		<cfset var qResult = "">
		<cfset arguments.energy_account_uid_list = removeDuplicates(arguments.energy_account_uid_list)>
		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				SUM(euv.total_cost) as total_cost,
				SUM(euv.recorded_value) as recorded_value,
				SUM(euv.recordedValueAsBtu) as recordedValueAsBtu,
				SUM(euv.recorded_value * dbo.getEmissionsFactorValueForEnergyAccount(energy_account_uid, '#arguments.profile_lookup_code#', DEFAULT, euv.energy_unit_uid)) AS emissionsImpact,

				<cfif include_budgeted_cost>
				SUM(dbo.getBudgetedEnergyUsageCost(euv.energy_usage_uid, default)) as budgeted_cost,
				</cfif>

				<!--- Property name, if possible -- if there is no single name, then the number of different properties --->
				CASE COUNT(DISTINCT euv.propertyFriendlyName)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(euv.propertyFriendlyName)
					ELSE '(Multiple Properties)'
				END AS propertyFriendlyName,
				<!--- Energy type name, if possible -- if there is no single name, then the number of different types --->
				CASE COUNT(DISTINCT euv.energyTypeFriendlyName)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(euv.energyTypeFriendlyName)
					ELSE '(Multiple Energy Types)'
				END AS energyTypeFriendlyName,
				<!--- Energy account name, if possible -- if there is no single name, then the number of different accounts --->
				CASE COUNT(DISTINCT euv.energyAccountFriendlyName)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(euv.energyAccountFriendlyName)
					ELSE CAST(COUNT(DISTINCT euv.energyAccountFriendlyName) as varchar(max)) + ' Accounts'
				END AS energyAccountFriendlyName,
				<!--- Energy unit name, if possible -- if there is no single name, then the number of different units --->
				CASE COUNT(DISTINCT euv.energyUnitFriendlyName)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(euv.energyUnitFriendlyName)
					ELSE NULL <!--- While it is normal to *have* a mix of units, it is strange to *display* a mix of units, so send back NULL so that renderer can degrade gracefully --->
				END AS energyUnitFriendlyName
			FROM
				dbo.EnergyUsageView euv (nolock)
			WHERE
				euv.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid_list#" list="true">)
				AND euv.is_active = 1
				AND ((CASE WHEN supplyRecordCheck = 'No Supply Record' THEN 0 ELSE 1 END) + (CASE WHEN dbo.getEnergyAccountSupplyAccount(euv.energy_account_uid) IS NULL THEN 0 ELSE 1 END)) <> 1
				<cfif select_reportable_records_only>
					AND euv.data_is_reportable = 1
				</cfif>
				AND euv.usage_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usage_type_code#">

				<!--- Note that the period_end column in EnergyUsageView is a calculated field that comes from the physical column "report_date" --->
				<!--- In other words, we are filtering on the report_date here --->
					<!--- 9/19/11 Changed to be based on actual billing end date,temp --->
				<cfif isDefined("arguments.fromDate")>
					AND euv.period_end >= #createODBCDate(arguments.fromDate)#
				</cfif>
				<!--- Note that the period_end column in EnergyUsageView is a calculated field that comes from the physical column "report_date" --->
				<!--- In other words, we are filtering on the report_date here --->
					<!--- 9/19/11 Changed to be based on actual billing end date,temp --->
				<cfif isDefined("arguments.thruDate")>
					AND euv.period_end <= #createODBCDate(arguments.thruDate)#
				</cfif>
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<cffunction name="getEnergyUsageRecordsForUsageEntryAlerts">
		<cfargument name="energy_account_uid_list" type="string" required="true">
		<cfargument name="usageTouchedDateFrom" type="date" required="false">
		<cfargument name="usageTouchedDateThru" type="date" required="false">
		<cfargument name="usage_status_code" type="string" required="false">
		<cfargument name="usage_type_code" type="string" required="false" default="actual">

		<cfset var qResult = "">
		<cfset arguments.energy_account_uid_list = removeDuplicates(arguments.energy_account_uid_list)>
		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT DISTINCT
			euv.energy_usage_uid,
			euv.energy_account_uid,
			euv.propertyFriendlyName,
			euv.energyAccountFriendlyName,
			euv.energyTypeFriendlyName,
			euv.total_cost,
			euv.recorded_value,
			euv.period_start,
			euv.period_end,
			euv.usage_status_date,
			euv.usageStatusFriendlyName,
			euv.energyUnitFriendlyName,
			euv.property_uid,
			euv.propertyFriendlyName,
			euv.usagePeriodStart,
			euv.usagePeriodEnd
			FROM
				EnergyUsageView euv (nolock)
			WHERE
				euv.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid_list#" list="true">)
				AND euv.is_active = 1
				AND euv.usage_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usage_type_code#">
				<cfif isDefined("arguments.usageTouchedDateFrom")>
					AND euv.consider_touched_date >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.usageTouchedDateFrom#">
				</cfif>
				<cfif isDefined("arguments.usageTouchedDateThru")>
					AND euv.consider_touched_date <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.usageTouchedDateThru#">
				</cfif>
				<cfif isDefined("arguments.usage_status_code")>
					AND euv.usage_status_code <= <cfqueryparam cfsqltype="usage_status_code" value="#arguments.usage_status_code#">
				</cfif>
			ORDER BY
				euv.energy_account_uid,
				euv.period_end DESC
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<cffunction name="getEnergyAccountsDueForDataEntry">
		<cfargument name="energy_account_uid_list" type="string" required="true">
		<cfargument name="elapsed_days" type="numeric" required="true">

		<cfset var qResult = "">
		<cfset arguments.energy_account_uid_list = removeDuplicates(arguments.energy_account_uid_list)>
		<!--- Initialize the local structure for all variables --->

		<cfstoredproc datasource="#this.datasource#" procedure="alertDataEntryDue">
			<cfprocparam value="#arguments.energy_account_uid_list#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.elapsed_days#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<!--- Return the result set based on the query parameters --->
		<cfreturn qResult>
	</cffunction>

	<cffunction name="getEnergyAccountsDueForDataEntryE2">
		<cfargument name="energy_account_uid_list" type="string" required="true">
		<cfargument name="elapsed_days" type="numeric" required="true">

		<cfset var qResult = "">
		<cfset arguments.energy_account_uid_list = removeDuplicates(arguments.energy_account_uid_list)>
		<!--- Initialize the local structure for all variables --->

		<cfstoredproc datasource="#this.datasource#" procedure="alertDataEntryDueE2Track">
			<cfprocparam value="#arguments.energy_account_uid_list#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.elapsed_days#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		<!--- Return the result set based on the query parameters --->
		<cfreturn qResult>
	</cffunction>
		
	<cffunction name="getHighLevelAccountInfo" access="public" output="false" returntype="query">
		<cfargument name="energy_account_uid_list" type="string" required="true">

		<cfset var qResult = "">
		<cfset arguments.energy_account_uid_list = removeDuplicates(arguments.energy_account_uid_list)>

		<cfquery name="qResult" datasource="#this.datasource#">
			SET NOCOUNT ON;
			SELECT
				COUNT(DISTINCT p.property_uid) AS propertyCount,
				COUNT(DISTINCT ea.energy_account_uid) AS energyAccountCount,
				COUNT(DISTINCT ep.energy_provider_uid) AS energyProviderCount,
				COUNT(DISTINCT et.energy_type_uid) AS energyTypeCount,
				COUNT(DISTINCT eu.friendly_name) AS energyUnitCount,

				CASE COUNT(DISTINCT p.property_uid)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(p.friendly_name)
					ELSE '(Multiple Properties)'
				END AS propertyFriendlyName,

				CASE COUNT(DISTINCT et.energy_type_uid)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(et.friendly_name)
					ELSE '(Mutiple Energy Types)'
				END AS energyTypeFriendlyName,

				CASE COUNT(DISTINCT ep.energy_provider_uid)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(ep.friendly_name)
					ELSE '(Multiple Providers)'
				END AS energyProviderFriendlyName,

				CASE COUNT(DISTINCT eu.friendly_name)
					WHEN 0 THEN NULL
					WHEN 1 THEN MIN(eu.friendly_name)
					ELSE '(Multiple Units)'
				END AS energyUnitFriendlyName

			FROM
				EnergyAccounts ea INNER JOIN
				Properties p ON p.property_uid = ea.property_uid INNER JOIN
				EnergyTypes et ON et.energy_type_uid = ea.energy_type_uid INNER JOIN
				EnergyProviders ep ON ep.energy_provider_uid = ea.currentEnergyProviderUid INNER JOIN
				EnergyUnits eu ON eu.energy_unit_uid = ea.energy_unit_uid
			WHERE
				ea.energy_account_uid IN (
					<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid_list#" list="true" null="#yesNoFormat(arguments.energy_account_uid_list eq '')#">
				)
				SET NOCOUNT OFF;
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<cffunction name="removeDuplicates" returntype="string">
  <cfargument name="list" type="string" required="true">

  <cfset var map = StructNew()>
  <cfset var result = ArrayNew(1)>
  <cfset var item = "">

  <cfloop list="#list#" index="item">
   <cfif not StructKeyExists(map, item)>
    <cfset arrayAppend(result, item)>
    <cfset map[item] = true>
   </cfif>
  </cfloop>

  <cfreturn ArrayToList(result)>
 </cffunction>
</cfcomponent>