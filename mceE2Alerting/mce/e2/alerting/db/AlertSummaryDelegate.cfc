
<!---

  Template Name:  AlertDelegate
     Base Table:  Alerts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, July 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, July 16, 2009 - Template Created.

--->

<cfcomponent	displayname="AlertSummaryDelegate"
				hint="This CFC is a read-only summarization of alert fields pulled from multiple tables."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction	name="getEmptyAlertSummaryComponent"
				hint="This method is used to retrieve a AlertSummary object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.alerting.vo.AlertSummary"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.alerting.vo.AlertSummary")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction	name="getAlertSummariesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of AlertSummary objects for all the Alerts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alerts and build out the array of components --->
		<cfset local.qResult = getAlertSummary(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.AlertSummary")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertSummaryAsComponent"
				hint="This method is used to retrieve a single instance of a / an AlertSummary value object representing a single Alert record."
				output="false"
				returnType="mce.e2.alerting.vo.AlertSummary"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="true" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert and build out the component --->
		<cfset local.qResult = getAlert(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.alerting.vo.AlertSummary")/>
		<cfset local.returnResult.alertMeta = getAlertMea(arguments.alert_uid)>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertMeta" output="false" returnType="query" access="public">
		<cfargument name="alert_uid" required="false" type="string"/>

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT * FROM AlertsMeta
			WHERE alert_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#">
			ORDER BY meta_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<cffunction	name="getAlertSummary"
				hint="This method is used to retrieve single / multiple records from the Alerts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>
		<cfargument name="recipient_address" required="false" type="string"/>
		<cfargument name="alert_content" required="false" type="string"/>
		<cfargument name="alert_due_date" required="false" type="date"/>
		<cfargument name="alert_visibility_date" required="false" type="date"/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the Alerts table matching the where clause
			select	tbl.alert_uid,
					tbl.alert_subscription_uid,
					tbl.client_company_uid,
					tbl.property_uid,
					tbl.alert_type_code,
					tbl.alert_status_code,
					tbl.recipient_address,
					tbl.alert_content,
					tbl.alert_due_date,
					tbl.alert_visibility_date,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,

					cc.friendly_name as 'client_company_friendly_name',
					prop.friendly_name as 'property_friendly_name',
					type.friendly_name as 'alert_type_friendly_name',
					status.friendly_name as 'alert_status_friendly_name'

			from	dbo.Alerts tbl (nolock),
					dbo.ClientCompanies cc (nolock),
					dbo.Properties prop (nolock),
					dbo.AlertTypes type (nolock),
					dbo.AlertStatuses status (nolock)
			where	tbl.client_company_uid = cc.client_company_uid
			and		tbl.property_uid = prop.property_uid
			and		tbl.alert_type_code = type.alert_type_code
			and		tbl.alert_status_code = status.alert_status_code

					<cfif structKeyExists(arguments, 'alert_uid')>
					and tbl.alert_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_subscription_uid')>
					and tbl.alert_subscription_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and tbl.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_type_code')>
					and tbl.alert_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_type_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_status_code')>
					and tbl.alert_status_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_status_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'recipient_address')>
					and tbl.recipient_address in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.recipient_address#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_content')>
					and tbl.alert_content in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_content#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_due_date')>
					and tbl.alert_due_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.alert_due_date#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_visibility_date')>
					and tbl.alert_visibility_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.alert_visibility_date#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" list="true" null="false"> )
					<cfelse>
					and tbl.is_active in ( 1 )
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="true"> )
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

</cfcomponent>
