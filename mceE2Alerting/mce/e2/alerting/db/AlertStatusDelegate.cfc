
<!---

  Template Name:  AlertStatusDelegate
     Base Table:  AlertStatuses	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, July 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, July 16, 2009 - Template Created.		

--->
		
<cfcomponent	displayname="AlertStatusDelegate"
				hint="This CFC manages all data access interactions for the AlertStatuses table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction	name="getEmptyAlertStatusComponent"
				hint="This method is used to retrieve a Alert Status object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.alerting.vo.AlertStatus"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.alerting.vo.AlertStatus")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction	name="getAlertStatusesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert Status objects for all the Alert Statuses in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_status_code" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alert Statuses and build out the array of components --->
		<cfset local.qResult = getAlertStatus(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.AlertStatus")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertStatusAsComponent"
				hint="This method is used to retrieve a single instance of a / an Alert Status value object representing a single Alert Status record."
				output="false"
				returnType="mce.e2.alerting.vo.AlertStatus"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_status_code" required="true" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert Status and build out the component --->
		<cfset local.qResult = getAlertStatus(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.alerting.vo.AlertStatus")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertStatus"
				hint="This method is used to retrieve single / multiple records from the AlertStatuses table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_status_code" required="false" type="string"/>
		<cfargument name="friendly_name" required="false" type="string"/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert Status records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the AlertStatuses table matching the where clause
			select	tbl.alert_status_code,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.AlertStatuses tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'alert_status_code')>
					and tbl.alert_status_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_status_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" null="false" list="true"> ) 
					<cfelse>
					and tbl.is_active in ( 1 )
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="true"> ) 
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Alert Status data. --->	
	<cffunction	name="saveNewAlertStatus"
				hint="This method is used to persist a new Alert Status record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertStatus" required="true" type="mce.e2.alerting.vo.AlertStatus" hint="Describes the VO containing the Alert Status record that will be persisted."/>

		<!--- Persist the Alert Status data to the application database --->
		<cfset this.dataMgr.insertRecord("AlertStatuses", arguments.AlertStatus)/>

	</cffunction>

	<cffunction	name="saveExistingAlertStatus"
				hint="This method is used to persist an existing Alert Status record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertStatus" required="true" type="mce.e2.alerting.vo.AlertStatus" hint="Describes the VO containing the Alert Status record that will be persisted."/>

		<!--- Persist the Alert Status data to the application database --->
		<cfset this.dataMgr.updateRecord("AlertStatuses", arguments.AlertStatus)/>

	</cffunction>

</cfcomponent>
