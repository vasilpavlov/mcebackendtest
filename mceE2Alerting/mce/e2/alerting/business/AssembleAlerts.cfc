<cfcomponent displayname="Alert Assembler"
			 extends="BaseBusiness" output="true"
			 hint="The component is used to manage all business logic tied / associated with assembling Alerts.">

	<!--- Constructor / dependencies --->
	<cffunction name="init"
				access="public" returntype="any"
				hint="Constructor.">

		<cfset this.AlertService = application.beanFactory.getBean("AlertService")>
		<cfset this.AlertIntervalDelegate = application.beanFactory.getBean("AlertIntervalDelegate")>
		<cfset this.AlertSubscriptionDelegate = application.beanFactory.getBean("AlertSubscriptionDelegate")>
		<cfset this.AlertDelegate = application.beanFactory.getBean("AlertDelegate")>
		<cfset this.RendererFactory = createObject("component", "mce.e2.alerting.renderer.RendererFactory").init()>

		<!--- This number can be adjusted to keep this process from taking over the CPU --->
		<cfset this.maxNumberOfAlertsToGenerateAtOnce = 50>

		<cfreturn this>
	</cffunction>


	<!--- Assemble the alerts based on intervals and subscriptions --->
	<cffunction name="assemble"
				access="public" returntype="array"
				hint="Assemble the set of alerts that will be executed based on their subscription and interval schedule.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset local.alertsGeneratedSummaries = ArrayNew(1)>
		<cfset local.alert_renderers = arrayNew(1)>

		<!--- Get the intervals --->
		<cfset local.alert_interval_codes = getScheduledIntervals()>

		<!--- Get the subscriptions --->
		<cfset local.alert_subscriptions = getScheduledAlertSubscriptions(alert_interval_codes=local.alert_interval_codes)>

		<!--- Might as well bail out early if there are no subscriptions to process at this time --->
		<cfif ArrayLen(local.alert_subscriptions) eq 0>
			<cfreturn ArrayNew(1)>
		</cfif>

		<!--- Create alert renderers from subscriptions --->
		<cfloop from="1" to="#arrayLen(local.alert_subscriptions)#" index="local.i">
			<cfset arrayAppend(local.alert_renderers, this.RendererFactory.getRenderer(local.alert_subscriptions[local.i]))>
		</cfloop>

		<cftrace category="Alerting" text="Created renderers for #ArrayLen(local.alert_subscriptions)# subscriptions">

		<!--- Execute renderers to generate alert instances - alerts are not being sent yet, just created and queued --->
		<cfloop array="#local.alert_renderers#" index="local.renderer">

			<!--- Ask the renderer if its conditions have been met (ask it if it should "fire" now) --->
			<cfif local.renderer.isAlertConditionsMet()>
				<cftrace category="Alerting" text="Alert conditions met for '#local.renderer.AlertSubscription.alert_type_code#' subscription #local.renderer.AlertSubscription.alert_subscription_uid#">

				<!--- Ask the renderer to create whatever number of new alert instances are appropriate --->
				<cfset local.alerts = local.renderer.renderAlerts()>

				<cfloop array="#local.alerts#" index="local.thisAlert">
					<!--- As a courtesy to the renderer, clean any "excessive whitespace" from the content/body that the renderer provided --->
					<cfset local.thisAlert.alert_content = super.cleanWhitespace(local.thisAlert.alert_content)>
					<cfset local.thisAlert.alert_body = super.cleanWhitespace(local.thisAlert.alert_body)>

					<!--- Set the alert visibility and generated date --->
					<cfset local.thisAlert.alert_visibility_date = DateAdd("n", local.renderer.getAlertVisibilityDelay(), Now())>
					<cfset local.thisAlert.alert_generated_date = Now()>

					<!--- Persist the new alert --->
					<cfset this.AlertService.saveNewAlert(local.thisAlert)>

					<cfif isDefined("local.thisAlert.energy_account_uid") and local.thisAlert.energy_account_uid neq "">
						<cfset this.AlertSubscriptionDelegate.updateLastGeneratedDate(local.renderer.AlertSubscription.alert_subscription_uid, local.thisAlert.energy_account_uid)>
					</cfif>

					<!--- Append to the array of feedback we'll return at the end --->
					<cfset local.thisAlertSummary = {alert_content=local.thisAlert.alert_content, alert_body=local.thisAlert.alert_body, alert_subscription_uid=local.thisAlert.alert_subscription_uid, alert_type_code=local.thisAlert.alert_type_code, alert_visibility_date=local.thisAlert.alert_visibility_date}>
					<cfset ArrayAppend(local.alertsGeneratedSummaries, local.thisAlertSummary)>

					<!--- Let's pause for a moment so we don't race the CPU too much --->
					<cfthread action="sleep" duration="250"/>
				</cfloop>

				<!--- We're done (until next time) if we now have more than our "max rows" limitation --->
				<!--- Note that we may go slightly over this limit if the last subscription that was processed generated multiple alert instances --->
				<cfif ArrayLen(local.alertsGeneratedSummaries) gte this.maxNumberOfAlertsToGenerateAtOnce>
					<cfbreak>
				</cfif>

				<!--- Remember the last-polled date for this subscription (so that we don't check it again for a while) --->
				<cfset this.AlertSubscriptionDelegate.updateLastPolledDate(local.renderer.AlertSubscription.alert_subscription_uid)>
			<cfelse>
				<cftrace category="Alerting" text="Alert conditions not met for '#local.renderer.AlertSubscription.alert_type_code#' subscription #local.renderer.AlertSubscription.alert_subscription_uid#">
			</cfif>

			<!--- Let's pause for a moment so we don't race the CPU too much --->
			<cfthread action="sleep" duration="250"/>
		</cfloop>

		<!--- Update the last-polled date on the interval record(s) --->
		<cfset this.AlertIntervalDelegate.updateLastPolledDate(local.alert_interval_codes)>

		<cfreturn local.alertsGeneratedSummaries>
	</cffunction>



	<!--- Assemble the alerts based on intervals and subscriptions --->
	<cffunction name="checkRelevancy"
				access="public" returntype="array">

		<cfargument name="alert_uid" type="string" required="true" hint="One or more alert instance uids, as a list">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset var processedMap = {}>
		<cfset var thisAlert = "">
		<cfset local.alert_renderers = arrayNew(1)>
		<cfset local.noLongerRelevant = arrayNew(1)>

		<cfset arguments.alert_uid = removeDuplicates(arguments.alert_uid)>
		<cfset local.qAlerts = this.AlertDelegate.getAlert(alert_uid=arguments.alert_uid)> <!--- Could eliminate this query --->

		<cfif local.qAlerts.recordCount gt 0>
			<cfset local.alert_subscription_uid_list = removeDuplicates(ValueList(local.qAlerts.alert_subscription_uid))>
			<cfset local.alert_subscriptions = this.AlertSubscriptionDelegate.getAlertSubscriptionsAsArrayOfComponents(alert_subscription_uid=local.alert_subscription_uid_list)>

			<!--- Create alert renderers from subscriptions --->
			<cfloop from="1" to="#arrayLen(local.alert_subscriptions)#" index="local.i">
				<cfset arrayAppend(local.alert_renderers, this.RendererFactory.getRenderer(local.alert_subscriptions[local.i]))>
			</cfloop>

			<!--- Make a map of the renderer instances, keyed by subscription uid --->
			<cfset local.rendererMap = StructNew()>
			<cfloop from="1" to="#arrayLen(local.alert_renderers)#" index="local.i">
				<cfset local.renderer = local.alert_renderers[local.i]>
				<cfset local.rendererMap[local.renderer.AlertSubscription.alert_subscription_uid] = local.renderer>
			</cfloop>

			<!--- Now, for each alert that we've been asked to scan --->
			<cfloop query="local.qAlerts">
				<cfset local.arThisAlert = this.AlertDelegate.queryToVoArray(local.qAlerts, "mce.e2.alerting.vo.Alert", 1, local.qAlerts.currentRow)>
				<cfif arrayLen(local.arThisAlert) gt 0>
					<cfset thisAlert = local.arThisAlert[1]>

					<cfif not structKeyExists(processedMap, thisAlert.alert_uid)>

						<!--- This alert's subscription should normally be in the renderer map, but perhaps might not be if the subscription has since been made inactive, etc --->
						<cfif structKeyExists(local.rendererMap, thisAlert.alert_subscription_uid)>

							<!--- This gives us an instance of the appropriate alert renderer CFC --->
							<cfset local.renderer = local.rendererMap[thisAlert.alert_subscription_uid]>

							<!--- Ask the renderer CFC if this alert is still relevant --->
							<cfset local.isStillRelevant = local.renderer.isAlertStillRelevant(thisAlert)>

							<!--- If it's no longer relevant we simply add it to the array that we'll return at the end --->
							<cfif not local.isStillRelevant>
								<cfset ArrayAppend(local.noLongerRelevant, thisAlert.alert_uid)>
							</cfif>

						</cfif>
						<cfset processedMap[thisAlert.alert_uid] = true>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn local.noLongerRelevant>
	</cffunction>


	<!--- Assemble appropriate alert intervals --->
	<cffunction name="getScheduledIntervals"
				access="private" returntype="string"
				hint="Assemble the set of alert intervals that will be executed based on their interval schedule.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get intervals --->
		<cfset local.qIntervals = this.AlertIntervalDelegate.getAlertInterval()>

		<!--- Determine intervals to run by comparing polling_interval and last_polled --->
		<cfset local.aIntervals = arrayNew(1)>
		<cfloop query="local.qIntervals">
			<cfset local.nextIntervalPoll = dateAdd("s", local.qIntervals.polling_interval, local.qIntervals.last_polled)>
			<!--- Append to array of intervals to be polled --->
			<cfif local.nextIntervalPoll lte now()>
				<cfset arrayAppend(local.aIntervals, local.qIntervals.alert_interval_code)>
			</cfif>
		</cfloop>

		<cfreturn arrayToList(local.aIntervals)>
	</cffunction>



	<!--- Assemble appropriate alert subscriptions by intervals --->
	<cffunction name="getScheduledAlertSubscriptions"
				access="private" returntype="array"
				hint="Assemble the set of alerts that will be executed based on their interval schedule.">

		<cfargument name="alert_interval_codes" type="string" required="true">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get subscriptions --->
		<cfset local.subscriptions = this.AlertSubscriptionDelegate.getAlertSubscriptionsByIntervals(alert_interval_codes=arguments.alert_interval_codes)>

		<!--- Filter out any alert subscriptions that their respective renderers would like to "triage" out right away --->
		<cftrace category="Alerting" text="Number of subscriptions before pre-filtering: #ArrayLen(local.subscriptions)#">
		<cfset local.subscriptions = preFilterAlertSubscriptions(local.subscriptions)>
		<cftrace category="Alerting" text="Number of subscriptions after pre-filtering: #ArrayLen(local.subscriptions)#">

		<cfreturn local.subscriptions>
	</cffunction>


	<cffunction name="preFilterAlertSubscriptions" access="private" returntype="array">
		<cfargument name="subscriptions" type="array" required="true">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset local.subscriptions = arguments.subscriptions>
		<cfset local.types = structNew()>

		<!--- Loop 1: Create a structure of alert types.  --->
		<cfloop array="#local.subscriptions#" index="local.subscription">
			<cfif not StructKeyExists(local.types, local.subscription.alert_type_code)>
				<cfset local.types[local.subscription.alert_type_code] = StructNew()>
				<cfset local.types[local.subscription.alert_type_code].alertTypeCfcName = local.subscription.alertTypeCfcName>
				<cfset local.types[local.subscription.alert_type_code].alert_subscription_uids = ArrayNew(1)>
			</cfif>
			<cfset ArrayAppend(local.types[local.subscription.alert_type_code].alert_subscription_uids, local.subscription.alert_subscription_uid)>
		</cfloop>

		<!--- Loop 2: Ask each renderer class for a cleaned version of the subscription uid list --->
		<cfset filteredSubscriptionUids = "">
		<cfloop collection="#local.types#" item="local.type">
			<cfset local.rendererClass = this.RendererFactory.getRendererClass(local.types[local.type].alertTypeCfcName)>
			<cfset filteredSubscriptionUids = ListAppend(filteredSubscriptionUids, local.rendererClass.preFilterAlertSubscriptionList(ArrayToList(local.types[local.type].alert_subscription_uids)))>
		</cfloop>

		<!--- Loop 3: Using the cleaned subscription UID list, filter the array of subscription VOs that we were handed --->
		<cfloop from="#ArrayLen(local.subscriptions)#" to="1" step="-1" index="local.i">
			<cfif not ListFind(filteredSubscriptionUids, local.subscriptions[local.i].alert_subscription_uid)>
				<cfset ArrayDeleteAt(local.subscriptions, local.i)>
			</cfif>
		</cfloop>

		<!--- Return the cleaned array of subscription VOs --->
		<cfreturn local.subscriptions>
	</cffunction>
 <cffunction name="removeDuplicates" returntype="string">
  <cfargument name="list" type="string" required="true">

  <cfset var map = StructNew()>
  <cfset var result = ArrayNew(1)>
  <cfset var item = "">

  <cfloop list="#list#" index="item">
   <cfif not StructKeyExists(map, item)>
    <cfset arrayAppend(result, item)>
    <cfset map[item] = true>
   </cfif>
  </cfloop>

  <cfreturn ArrayToList(result)>
 </cffunction>
</cfcomponent>