<cfcomponent displayname="Alert Sender"
			 extends="BaseBusiness" output="false"
			 hint="The component is used to manage all business logic tied / associated with sending Alerts.">

	<!--- Constructor / dependencies --->
	<cffunction name="init"
				access="public" returntype="any"
				hint="Constructor.">

		<cfset this.AlertDelegate = application.beanFactory.getBean("AlertDelegate")>
		<cfset this.AlertDeliveryMethodDelegate = application.beanFactory.getBean("AlertDeliveryMethodDelegate")>
		<cfset this.AlertSubscriptionDelegate = application.beanFactory.getBean("AlertSubscriptionDelegate")>
		<cfset this.DeliveryFactory = createObject("component", "mce.e2.alerting.delivery.DeliveryFactory").init()>
		<cfset this.maxNumberOfAlertsToSendAtOnce = 50>
		<cfset this.maxNumberOfAlertsPerUserAtOnce = 100>

		<cfreturn this>
	</cffunction>


	<!--- Send the alerts based on status --->
	<cffunction name="send"
				access="public" returntype="numeric"
				hint="Send the set of alerts that will be executed based on their status.">

		<!--- We send alerts for a given digest --->
		<cfargument name="alert_digest_code" type="string" required="true">
		<cfargument name="respectVisibilityDate" type="boolean" required="true">
		<cfargument name="eligible_alert_delivery_method_codes" type="string" required="false" default="email">

		<cfset var thisUser = "">
		<cfset var thisDeliveryMethod = "">
		<cfset var thisDeliveryAlerts = "">
		<cfset var thisDeliveryMethodComponent = "">
		<cfset var alertUidList = "">
		<cfset var alerts = "">
		<cfset var userStruct = "">
		<cfset var numMessagesSent = 0>
		<cfset var numAlertsSent = 0>

		<!--- These are the arguments we'll use to retrieve the list of (already generated) alerts to send --->
		<cfset var criteriaArgs = {
			alert_status_code="Pending",
			alert_digest_code=arguments.alert_digest_code,
			alert_delivery_method_code=arguments.eligible_alert_delivery_method_codes,
			order_by="user-then-visibility-date"
		}>
		<!--- Assuming that we are meant to respect the "alert_visibility_date" (normal operation mode) --->
		<cfif arguments.respectVisibilityDate>
			<cfset criteriaArgs.alert_visibility_date = now()>
		</cfif>

		<!--- Get the alerts --->
		<cfset alerts = this.AlertDelegate.getAlertsAsArrayOfComponents(argumentCollection=criteriaArgs)>

		<!--- Contruct a structure that has an entry for each user; within is an "alerts" structure that has alerts for each delivery method --->
		<cfset userStruct = buildUserStruct(alerts)>

		<cftrace category="Alerting" text="Found #ArrayLen(alerts)# alerts for digest '#arguments.alert_digest_code#'">

		<!--- For each user... --->
		<cfloop collection="#userStruct#" item="thisUser">
			<!--- For each delivery method... --->
			<cfloop collection="#userStruct[thisUser].alertsByMethod#" item="thisDeliveryMethod">
				<cfset thisDeliveryAlerts = userStruct[thisUser].alertsByMethod[thisDeliveryMethod]>

				<!--- Get instance of the CFC that is able to send/deliver based on the "delivery method" --->
				<cfset thisDeliveryMethodComponent = this.DeliveryFactory.getDeliveryMethod(
					thisDeliveryMethod,
					thisDeliveryAlerts,
					userStruct[thisUser].alert_email
				)>

				<!--- Tell the delivery CFC to deliver these alerts --->
				<cfset thisDeliveryMethodComponent.deliverAlerts()>

				<!--- Update the alert status so we know not to send the same alert again later --->
				<cfset this.AlertDelegate.updateAlertStatus(thisDeliveryAlerts,	"sent")>
				<cfset numAlertsSent = numAlertsSent + ArrayLen(thisDeliveryAlerts)>

				<!--- Let's pause for a moment so we don't race the CPU too much --->
				<cfthread action="sleep" duration="250"/>
			</cfloop>

			<!--- Gate ourselves to a reasonable number of generated emails (or whatever) per execution --->
			<cfset numMessagesSent = numMessagesSent + 1>
			<cfif numMessagesSent gte this.maxNumberOfAlertsToSendAtOnce>
				<cfbreak>
			</cfif>
		</cfloop>

		<!--- return number alert alerts processed --->
		<cfreturn numAlertsSent>

	</cffunction>




	<!--- Utility function --->
	<cffunction name="buildUserStruct" returntype="struct" access="private">
		<cfargument name="userAlerts" type="array" required="true">

		<cfset var s = StructNew()>
		<cfset var alertItem = "">
		<cfset var userItem = "">
		<cfset var qUsers = "">
		<cfset var qMethods = "">

		<!--- For each alert --->
		<cfloop array="#userAlerts#" index="alertItem">
			<!--- Get the users that this alert is addressed to --->
			<cfset qUsers = this.AlertDelegate.getUsersForAlert(alertItem.alert_uid)>

			<!--- Get the delivery methods for this alert --->
			<cfset qMethods = this.AlertDeliveryMethodDelegate.getAlertDeliveryMethod(alert_subscription_uid=alertItem.alert_subscription_uid)>

			<!--- For each of the users --->
			<cfloop query="qUsers">

				<!--- If this is the first time encountering this user, make a nested structure for the user --->
				<cfif not StructKeyExists(s, qUsers.user_uid)>
					<cfset s[qUsers.user_uid] = StructNew()>
					<cfset s[qUsers.user_uid].alert_email = qUsers.alert_email>
					<cfset s[qUsers.user_uid].alertsByMethod = StructNew()>
				</cfif>

				<!--- For each delivery method --->
				<cfloop query="qMethods">
					<!--- If this is the first time encountering this delivery method for this user --->
					<cfif not StructKeyExists(s[qUsers.user_uid].alertsByMethod, qMethods.alert_delivery_method_code)>
						<cfset s[qUsers.user_uid].alertsByMethod[qMethods.alert_delivery_method_code] = ArrayNew(1)>
					</cfif>

					<!--- Append this alert to the array of alerts for this user, for this delivery method --->
					<cfif ArrayLen(s[qUsers.user_uid].alertsByMethod[qMethods.alert_delivery_method_code]) lt this.maxNumberOfAlertsPerUserAtOnce>
						<cfset ArrayAppend(s[qUsers.user_uid].alertsByMethod[qMethods.alert_delivery_method_code], alertItem)>
					</cfif>
				</cfloop>

			</cfloop>
		</cfloop>

		<cfreturn s>
	</cffunction>



</cfcomponent>