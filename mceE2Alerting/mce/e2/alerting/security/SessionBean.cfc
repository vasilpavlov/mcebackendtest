<cfcomponent displayName="Session Bean"
			 hint="This component represents a session bean, and carries all important properties of a given session.">

	<!--- Define the properties for this component --->	
	<cfproperty name="sessionKey" type="string" hint="Describes the primary key / unique identifier for a given session.">
	<cfproperty name="isLoggedIn" type="boolean" hint="Describes whether the user attached to a given session is already logged into the application (true = the user is logged in; false = the user is not logged in).">
	<cfproperty name="user_uid" type="string" hint="Describes the primary key / unique identifier of the user to which the session is associated to.">
	<cfproperty name="client_company_uid" type="string" hint="Describes the primary key / unique identifier of the company to which the user owning the current session is associated to.">

	<!--- Define the security / association lists --->
	<cfproperty name="rolesList" type="string" hint="Describes a comma-delimited list of the security role lookup codes that a user has been given rights / access to.">
	<cfproperty name="permissionsList" type="string" hint="Describes a comma-delimited list of the security permissions that a user has been given rights / access to based on their group / role associations.">
	<cfproperty name="propertyCollectionsList" type="string" hint="Describes a comma-delimited list of the properyCollection primary keys that a user have been given rights / access to.">

	<!--- Define the audit properties --->
	<cfproperty name="auditUserLabel" type="string" hint="Describes the user name / audit label for a given user that is attached to a given session, if any.">

	<cffunction name="init"
				hint="This method is the constructor for a given session bean.">

		<!--- Set / initialize the unidentified user label --->
		<cfset variables.unidentifiedUserLabel = "[Unidentified User]">

		<!--- Does a given session already exist while the bean is being instantiated? --->
		<cfif isDefined("session.mce.e2.alerting.session")>
			
			<!--- If so, then place the session properties in the bean --->
			<cfset this.sessionKey = session.mce.e2.alerting.session.sessionKey>
			<cfset this.isLoggedIn = session.mce.e2.alerting.session.isLoggedIn>
			<cfset this.rolesList = "">
			<cfset this.propertyCollectionsList = "">
			<cfset this.user_uid = "">
			<cfset this.client_company_uid = "">
			
			<!--- ToDo: [ADL] Add logic to populate the login /auditUserLabel fields so that
						no database interactions need to be made to determine who the user
						is for a given session. --->	
											
			<!--- Added the audit trail / system facing user identification label --->
			<cfset this.auditUserLabel = "">
			
		<cfelse>
		
			<!--- Otherwise, initialize the session bean properties --->	
			<cfset this.sessionKey = "">
			<cfset this.isLoggedIn = false>
			<cfset this.rolesList = "">
			<cfset this.PropertyCollectionsList = "">
			<cfset this.user_uid = "">
			<cfset this.client_company_uid = "">

			<!--- ToDo: [ADL] Add logic to populate the login /auditUserLabel fields. --->
			<!--- Added the audit trail / system facing user identification label --->
			<cfset this.auditUserLabel = getUnidentifiedUserLabel()>
		
		</cfif>
		
		<cfreturn this>

	</cffunction>

	<cffunction name="getUnidentifiedUserLabel"
				access="public" returnType="string"
				hint="This method is used to return / define the unidentified user label that will be attached to a given session.">
		<cfreturn variables.unidentifiedUserLabel>			
	</cffunction>

</cfcomponent>