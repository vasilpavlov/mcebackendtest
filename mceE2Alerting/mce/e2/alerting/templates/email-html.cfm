<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>#emailSubject#</title>

		<style type="text/css">
			<!--
			body  {
				font: 100% Verdana, Arial, Helvetica, sans-serif;
				background: #666666;
				margin: 0; /* it's good practice to zero the margin and padding of the body element to account for differing browser defaults */
				padding: 0;
				text-align: center; /* this centers the container in IE 5* browsers. The text is then set to the left aligned default in the #container selector */
				color: #000000;
			}
			.twoColLiqLtHdr #container {
				width: 80%;  /* this will create a container 80% of the browser width */
				background: #FFFFFF;
				margin: 0 auto; /* the auto margins (in conjunction with a width) center the page */
				border: 1px solid #000000;
				text-align: left; /* this overrides the text-align: center on the body element. */
			}
			.twoColLiqLtHdr #header {
				background: #DDDDDD;
				padding: 0 10px;  /* this padding matches the left alignment of the elements in the divs that appear beneath it. If an image is used in the #header instead of text, you may want to remove the padding. */
			}
			.twoColLiqLtHdr #header h1 {
				margin: 0; /* zeroing the margin of the last element in the #header div will avoid margin collapse - an unexplainable space between divs. If the div has a border around it, this is not necessary as that also avoids the margin collapse */
				padding: 10px 0; /* using padding instead of margin will allow you to keep the element away from the edges of the div */
			}
			.twoColLiqLtHdr #sidebar1 {
				float: left;
				width: 24%; /* since this element is floated, a width must be given */
				background: #EBEBEB; /* the background color will be displayed for the length of the content in the column, but no further */
				padding: 15px 0; /* top and bottom padding create visual space within this div  */
			}
			.twoColLiqLtHdr #sidebar1 h3, .twoColLiqLtHdr #sidebar1 p {
				margin-left: 10px; /* the left and right margin should be given to every element that will be placed in the side columns */
				margin-right: 10px;
			}
			.twoColLiqLtHdr #mainContent {
				margin: 0 20px 0 26%; /* the right margin can be given in percentages or pixels. It creates the space down the right side of the page. */
			}
			.twoColLiqLtHdr #footer {
				padding: 0 10px; /* this padding matches the left alignment of the elements in the divs that appear above it. */
				background:#DDDDDD;
			}
			.twoColLiqLtHdr #footer p {
				margin: 0; /* zeroing the margins of the first element in the footer will avoid the possibility of margin collapse - a space between divs */
				padding: 10px 0; /* padding on this element will create space, just as the the margin would have, without the margin collapse issue */
			}
			.fltrt { /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
				float: right;
				margin-left: 8px;
			}
			.fltlft { /* this class can be used to float an element left in your page */
				float: left;
				margin-right: 8px;
			}
			.clearfloat { /* this class should be placed on a div or break element and should be the final element before the close of a container that should fully contain a float */
				clear:both;
				height:0;
				font-size: 1px;
				line-height: 0px;
			}
			-->
		</style>
		<!--[if IE]>
			<style type="text/css">
				.twoColLiqLtHdr #sidebar1 { padding-top: 30px; }
				.twoColLiqLtHdr #mainContent { zoom: 1; padding-top: 15px; }
			</style>
		<![endif]-->
	</head>

	<body class="twoColLiqLtHdr">

		<div id="container">

			<div id="header">
				<cfoutput><img src="#this.imageLocation#/top_left_logo.png" alt="E2Track" border="0" /></cfoutput>
				<a href="http://beta.mcenergyinc.com">E2Track Login</a><br />
			</div>

			<div id="sidebar1">
				<h3>Alert Update</h3>
				<p></p>
			</div>

			<div id="mainContent">
			<dl>
				<!--- Begin looping over alerts to send... grouped by the "alert_location_notes" (generally property name) --->
				<cfoutput query="qAlerts" group="alert_location_notes">

					<!--- This part executes for each new "alert_location_notes" string --->
					<p><font size="+1"><b>#qAlerts.alert_location_notes#</b></font></p>

					<!--- Within each property, group by "alert_context_notes" string (generally the energy type) --->
					<cfoutput group="alert_context_notes">

						<!--- This part executes for each new "alert_context_notes" string --->
						<b>#qAlerts.alert_context_notes#</b><br />

						<!--- This innermost post executes for each individual alert --->
						<cfoutput>
							<dt style="padding-top:10px">#qAlerts.alertSubscriptionFriendlyName#</dt>
							<dd>
								<font size="-1">#DateFormat(qAlerts.alert_visibility_date)# #TimeFormat(qAlerts.alert_visibility_date)#</font><br />
								<b>#qAlerts.alert_content#</b><br />
								<i>#qAlerts.alert_body#</i>
							</dd>
						</cfoutput>
					</cfoutput>

					<hr size="1" noshade="true" color="silver">
				</cfoutput>

			</dl>
			</div>

			<br class="clearfloat" />

			<div id="footer">
				<p>
				<cfoutput><img src="#this.imageLocation#/powered_by_big.gif" alt="Powered by MCE" border="0" /></cfoutput><br />
				<a href="mailto:info@mcenergyinc.com">info@mcenergyinc.com</a><br />
				<a href="http://www.mcenergyinc.com">www.mcenergyinc.com</a><br />
				Phone: (914) 767-3100<br />
				</p>
			</div>

		</div>

	</body>
</html>
