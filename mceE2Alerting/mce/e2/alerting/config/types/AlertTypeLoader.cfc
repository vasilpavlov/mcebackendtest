<cfcomponent output="false">

	<!--- Constructor --->
	<cffunction name="init" hint="Constructor">
		<cfargument name="AlertTypeDelegate" type="mce.e2.alerting.db.AlertTypeDelegate" required="true" hint="Autowire">
		<cfargument name="AlertIntervalDelegate" type="mce.e2.alerting.db.AlertIntervalDelegate" required="true" hint="Autowire">

		<cfset this.AlertTypeDelegate = arguments.AlertTypeDelegate>
		<cfset this.AlertIntervalDelegate = arguments.AlertIntervalDelegate>

		<cfset this.xmlFile = ExpandPath("/mce/e2/alerting/config/types/known_alert_types.xml")>
		<cfset this.load()>

		<cfreturn this>
	</cffunction>


	<cffunction name="load">
		<cfset loadAlertIntervals()>
 		<cfset loadAlertTypes()>
		<cfset loadAlertConditions()>
	</cffunction>


	<cffunction name="loadAlertIntervals" access="private">
		<cfset var xmlDoc = "">
		<cfset var nodes = "">
		<cfset var node = "">
		<cfset var record = "">
		<cfset var recordExists = false>
		<cfset var new_uid ="">

		<!--- Parse the xml document and find all of the relevant nodes --->
		<cffile action="read" file="#this.xmlFile#" variable="xmlContent">
		<cfset xmlDoc = XmlParse(xmlContent)>
		<cfset nodes = XmlSearch(xmlDoc, "config/alert-interval")>

		<!--- For each node --->
		<cfloop array="#nodes#" index="node">
			<cfset recordExists = ArrayLen(this.AlertIntervalDelegate.getAlertIntervalsAsArrayOfComponents(alert_interval_code=node.XmlAttributes.code)) gt 0>
			<cftrace var="recordExists" text="Checking existence of Alert Interval '#node.XmlAttributes.code#'">

			<!---
				Note that we intentionally do not do "updates" here, only "inserts".
				The idea is that if this alert type is already in the DB, its fields
				may have been altered there and we shouldn't clobber it.
			 --->
			<!--- If a corresponding record is not yet in the DB --->
			<cfif not recordExists>
				<!--- Create new vo instance for the alert type --->
				<cfset record = CreateObject("component", "mce.e2.alerting.vo.AlertInterval")>

				<!--- Populate properties of the new alert type from info in xml --->
				<cfset record.alert_interval_code = node.XmlAttributes.code>
				<cfset record.friendly_name = node.XmlAttributes.name>
				<cfset record.is_enabled = node.XmlAttributes.enabled>
				<cfset record.queue_priority = node.XmlAttributes.priority>
				<cfset record.polling_interval = node.XmlAttributes.interval>
 				<cfset record.last_polled = CreateDate(1970, 1, 1)>

				<!--- Persist new alert type --->
				<cfset record.created_by = "AlertTypeLoader">
				<cfset new_uid = this.AlertIntervalDelegate.saveNewAlertInterval(record)>
			</cfif>
		</cfloop>
	</cffunction>



	<cffunction name="loadAlertTypes" access="private">
		<cfset var xmlDoc = "">
		<cfset var nodes = "">
		<cfset var node = "">
		<cfset var record = "">
		<cfset var recordExists = false>
		<cfset var new_uid ="">
		<cfset var alertTypeVos = "">

		<!--- Parse the xml document and find all of the relevant nodes --->
		<cffile action="read" file="#this.xmlFile#" variable="xmlContent">
		<cfset xmlDoc = XmlParse(xmlContent)>
		<cfset nodes = XmlSearch(xmlDoc, "config/alert-type")>

		<!--- For each node --->
		<cfloop array="#nodes#" index="node">
			<cfset alertTypeVos = this.AlertTypeDelegate.getAlertTypesAsArrayOfComponents(alert_type_code=node.XmlAttributes.code)>
			<cfset recordExists = ArrayLen(alertTypeVos) gt 0>

			<cftrace var="recordExists" text="Checking existence of Alert Type '#node.XmlAttributes.code#'">

			<!---
				Note that we intentionally do not do "updates" here, only "inserts".
				The idea is that if this alert type is already in the DB, its fields
				may have been altered there and we shouldn't clobber it.
			 --->
			<!--- If a corresponding record is not yet in the DB --->
			<cfif not recordExists>
				<!--- Create new vo instance for the alert type --->
				<cfset record = CreateObject("component", "mce.e2.alerting.vo.AlertType")>

				<!--- Populate properties of the new alert type from info in xml --->
				<cfset record.alert_type_code = node.XmlAttributes.code>
				<cfset record.friendly_name = node.XmlAttributes.name>
				<cfset record.description = node.XmlAttributes.description>
				<cfset record.cfc_name = node.XmlAttributes.cfc>
				<cfset record.ui_priority = node.XmlAttributes.priority>
				<cfset record.ui_category = node.XmlAttributes.category>

				<!--- Persist new alert type --->
				<cfset record.created_by = "AlertTypeLoader">
				<cfset new_uid = this.AlertTypeDelegate.saveNewAlertType(record)>

				<!--- Associate new record with alert interval --->
				<cfset record.alert_type_uid = new_uid>
			</cfif>

			<!--- Ensure alert type is associated with alert interval --->
			<cfset this.AlertTypeDelegate.saveNewAlertTypeIntervals(node.XmlAttributes.code, node.XmlAttributes.intervals, node.XmlAttributes.enabled)>

		</cfloop>
	</cffunction>



	<cffunction name="loadAlertConditions" access="private">
		<cfset var xmlDoc = "">
		<cfset var nodes = "">
		<cfset var node = "">
		<cfset var record = "">
		<cfset var recordExists = false>
		<cfset var alert_type_code ="">
		<cfset var qCurrentConditions = "">
		<cfset var currentConditionCodes = "">

		<!--- Parse the xml document and find all of the relevant nodes --->
		<cffile action="read" file="#this.xmlFile#" variable="xmlContent">
		<cfset xmlDoc = XmlParse(xmlContent)>
		<cfset nodes = XmlSearch(xmlDoc, "config/alert-condition")>

		<!--- For each node --->
		<cfloop array="#nodes#" index="node">
			<cfloop list="#node.XmlAttributes.alertTypes#" index="alert_type_code">
				<cfset qCurrentConditions = this.AlertTypeDelegate.getAlertTypeConditions(alert_type_code)>
				<cfset currentConditionCodes = ValueList(qCurrentConditions.alert_condition_code)>
				<cfset recordExists = ListFindNoCase(currentConditionCodes, node.XmlAttributes.code) gt 0>

				<cftrace var="recordExists" text="Checking existence of Alert Condition '#node.XmlAttributes.code#' for #alert_type_code#">

				<cfif not recordExists>
					<cfset record = CreateObject("component", "mce.e2.alerting.vo.AlertConditionsType")>

					<cfset record.alert_type_code = alert_type_code>
					<cfset record.alert_condition_code = node.XmlAttributes.code>
					<cfset record.friendly_name = node.XmlAttributes.name>
					<cfset record.default_value = node.XmlAttributes.default>

					<!--- Persist new condition type record --->
					<cfset record.created_by = "AlertTypeLoader">
					<cfset this.AlertTypeDelegate.saveNewAlertConditionType(record)>

				</cfif>
			</cfloop>
		</cfloop>
	</cffunction>




</cfcomponent>