<cfinterface>

	<!--- Create the *.cfc constructor --->
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the base renderer component.">
		<cfargument name="alertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription">
	</cffunction>

	<!--- Give the renderer an opportunity to skip processing of subscriptions that it knows could not be relevant right now --->
	<cffunction name="preFilterAlertSubscriptionList"
				access="public" returntype="string"
			 	hint="Given a list of subscription uids, filters the list to only include the subscriptions that may be relevant at this time.">
		<cfargument name="alert_subscription_uid_list" type="string" required="true">
	</cffunction>

	<!--- Determine whether an alert should be spawned, based on the conditions in the subscription --->
	<cffunction name="isAlertConditionsMet"
				access="public" returntype="boolean"
			 	hint="Returns whether the alert subscription's conditions have been met; if false, then the alert should not be spawned at this time."/>

	<!--- Render the alert with specific information based on the alert type --->
	<cffunction name="renderAlerts"
				access="public" returntype="array"
			 	hint="Renders the alert to be sent and persisted."/>

	<!--- Return a number of minutes to delay the visibility of this alert --->
	<cffunction name="getAlertVisibilityDelay"
				access="public" returntype="numeric"
			 	hint="The date when the alert can be considered to be visible to the outside world"/>

	<!--- Return whether the alert is still relevant --->
	<!--- Specific alert renderers can override to return true/false based on what makes sense --->
	<cffunction name="isAlertStillRelevant"
				access="public" returntype="boolean"
			 	hint="Whether this alert is still relevant, for instance if the alert is a data entry task, it may not be relevant anymore if the task has been completed via some other path.">
		<cfargument name="alert" type="mce.e2.alerting.vo.Alert" required="true">
	</cffunction>

</cfinterface>