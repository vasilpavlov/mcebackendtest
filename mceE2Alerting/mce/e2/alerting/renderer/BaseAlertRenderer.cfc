<cfcomponent name="Base AlertRenderer Component"
			 output="true"
			 extends="mce.e2.alerting.common.BaseComponent"
			 implements="IAlertRenderer"
			 hint="This abstract component is the base alert renderer component for all alert renderer *.cfc's - do not implement this component directly.">

	<!--- Create the *.cfc constructor --->
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the base alert renderer component.">

		<cfargument name="alertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription">

		<cfset this.AlertDelegate = application.beanFactory.getBean("AlertDelegate")>
		<cfset this.AlertSubscriptionDelegate = application.beanFactory.getBean("AlertSubscriptionDelegate")>

		<!--- Create the alert instance and subscription reference --->
		<cfset this.AlertSubscription = arguments.alertSubscription>

		<!--- Return an instance of the component --->
		<cfreturn this>
	</cffunction>


	<cffunction name="createAlertInstance"
				access="package" returntype="mce.e2.alerting.vo.Alert"
			 	hint="Configures a new alert to be sent and persisted.">

		<!--- Configure the alert --->
		<cfset var alert = this.AlertDelegate.getEmptyAlertComponent()>
		<cfset alert.alert_subscription_uid = this.AlertSubscription.alert_subscription_uid>
		<cfset alert.alert_type_code = this.AlertSubscription.alert_type_code>
		<cfset alert.alert_digest_code = this.AlertSubscription.alert_digest_code>
		<cfset alert.alert_location_notes = this.AlertSubscription.propertyFriendlyName>
		<cfset alert.alert_context_notes = "">
		<cfset alert.alert_subject_notes = "">
		<!--- TODO:[KO] Get alert_status_code from config --->
		<cfset alert.alert_status_code = "Pending">
		<!--- TODO:[KO] Replace with 1 to many solution
		<cfset this.Alert.property_uid = "5A384FD7-34AA-4BE9-B2B2-000B41E87043">--->
		<cfset alert.property_uid = this.AlertSubscription.property_uid>

		<cfset alert.alertUsers = this.AlertSubscriptionDelegate.getUsersForAlertSubscription(this.AlertSubscription.alert_subscription_uid)>

		<cfreturn alert>
	</cffunction>


	<!--- Adds a name/value pair to the meta-data on the alert instance --->
	<cffunction name="addAlertMetaData" access="package" returntype="void" output="false">
		<cfargument name="Alert" type="mce.e2.alerting.vo.Alert" required="true">
		<cfargument name="meta_name" type="string" required="true">
		<cfargument name="meta_value" type="string" required="true">

		<cfset var pair = {meta_name=arguments.meta_name, meta_value=arguments.meta_value}>

		<cfif not IsDefined("arguments.Alert.alertMeta")>
			<cfset arguments.Alert.alertMeta = ArrayNew(1)>
		</cfif>

		<cfset ArrayAppend(arguments.Alert.alertMeta, pair)>
	</cffunction>

	<!--- Implement IAlertRenderer interface --->
	<!--- Return whether the alert is still relevant --->
	<cffunction name="isAlertStillRelevant"
				access="public" returntype="boolean"
			 	hint="Whether this alert is still relevant, for instance if the alert is a data entry task, it may not be relevant anymore if the task has been completed via some other path.">
		<cfargument name="alert" type="mce.e2.alerting.vo.Alert" required="true">

		<!--- This base implementation always answers "true" --->
		<!--- Specific subclasses can override to return true/false based on what makes sense --->
		<cfreturn true>
	</cffunction>


	<!--- *********** ABSTRACT METHODS *********** --->
	<!--- ****** SUBCLASSES MUST OVERRIDE ******* --->

	<!--- Renders the alert with specific information based on the alert type --->
	<cffunction name="renderAlerts"
				access="public" returntype="array"
			 	hint="Renders the alert to be sent and persisted.">

		<!--- This method should be implemented in each alert renderer with specific logic per the alert type --->
		<cfset abstractErrorMessage()>
	</cffunction>


	<!--- Give the renderer an opportunity to skip processing of subscriptions that it knows could not be relevant right now --->
	<cffunction name="preFilterAlertSubscriptionList"
				access="public" returntype="string"
			 	hint="Given a list of subscription uids, filters the list to only include the subscriptions that may be relevant at this time.">
		<cfargument name="alert_subscription_uid_list" type="string" required="true">

		<!--- This base class does the simplest possible thing, which is to return the list unmodified --->
		<!--- Subclasses can perform a query to "triage" out any subscriptions that couldn't possibly want to send a subscription now --->
		<!--- (For instance, for an alert type that is based on energy records, all subscriptions that were last polled after any of their accounts were touched could be filtered out) --->
		<cfreturn arguments.alert_subscription_uid_list>
	</cffunction>

	<!--- Return a number of minutes to delay the visibility of this alert --->
	<cffunction name="getAlertVisibilityDelay"
				access="public" returntype="numeric"
			 	hint="The date when the alert can be considered to be visible to the outside world">

		<cfset abstractErrorMessage()>
	</cffunction>

	<!--- Determine whether an alert should be spawned, based on the conditions in the subscription --->
	<cffunction name="isAlertConditionsMet"
				access="public" returntype="boolean"
			 	hint="Returns whether the alert subscription's conditions have been met; if false, then the alert should not be spawned at this time.">
		<cfset abstractErrorMessage()>
	</cffunction>


	<cffunction name="getConditionsAsStruct" access="package">
		<cfreturn super.arrayToMap(this.AlertSubscription.conditions, "alert_condition_code")>
	</cffunction>

	<cffunction name="getConditionValue" access="package" returntype="numeric">
		<cfargument name="alert_condition_code" type="string" required="true">
		<cfargument name="default_value" type="string" required="false">

		<cfset var conditions = getConditionsAsStruct()>

		<cfif not StructKeyExists(conditions, arguments.alert_condition_code)>
			<cfif isDefined("arguments.default_value")>
				<cfreturn default_value>
			<cfelse>
				<cfthrow
					message="No value for condition '#alert_condition_code#'">
			</cfif>
		</cfif>

		<cfreturn Val(conditions[arguments.alert_condition_code].numeric_value)>
	</cffunction>



	<cffunction name="getAccountsByLastGenerated" returntype="string">
		<cfargument name="comparisonDate" type="date" required="true">
		<cfargument name="comparison" type="string" required="false" default="before">

		<cfset var accounts = "">
		<cfset var energy_account_uid = "">
		<cfset var match = "">

		<cfloop collection="#this.AlertSubscription.lastGeneratedDates#" item="energy_account_uid">
			<cfset match = false>

			<cfswitch expression="#arguments.comparison#">
				<cfcase value="before">
					<cfif this.AlertSubscription.lastGeneratedDates[energy_account_uid] lt arguments.comparisonDate>
						<cfset match = true>
					</cfif>
				</cfcase>
				<cfcase value="after">
					<cfif this.AlertSubscription.lastGeneratedDates[energy_account_uid] gt arguments.comparisonDate>
						<cfset match = true>
					</cfif>
				</cfcase>
				<cfcase value="on-or-before">
					<cfif this.AlertSubscription.lastGeneratedDates[energy_account_uid] lte arguments.comparisonDate>
						<cfset match = true>
					</cfif>
				</cfcase>
				<cfcase value="on-or-after">
					<cfif this.AlertSubscription.lastGeneratedDates[energy_account_uid] gte arguments.comparisonDate>
						<cfset match = true>
					</cfif>
				</cfcase>

				<cfdefaultcase>
					<cfthrow message="Unknown comparison argument '#comparison#'">
				</cfdefaultcase>
			</cfswitch>

			<cfif match>
				<cfset accounts = ListAppend(accounts, energy_account_uid)>
			</cfif>
		</cfloop>

		<cfreturn accounts>
	</cffunction>


	<cffunction name="getHighLevelAccountInfo" access="package" returntype="query">
		<cfset var qResult = "">
		<cfset var energyAccountUidList = getFieldListFromVoArray(this.AlertSubscription.accounts, "energy_account_uid")>

		<cfinvoke
			component="#this.lookupDelegate#"
			method="getHighLevelAccountInfo"
			energy_account_uid_list="#energyAccountUidList#"
			returnVariable="qResult">

		<cfreturn qResult>
	</cffunction>


	<cffunction name="formatDatePair" access="package" returntype="string">
		<cfargument name="date1" type="date" required="true">
		<cfargument name="date2" type="date" required="true">
		<cfargument name="formatMask" type="string" required="false" default="mmm yyyy">

		<cfset var str1 = DateFormat(date1, formatMask)>
		<cfset var str2 = DateFormat(date2, formatMask)>

		<!--- If the start and end dates are the same (at least after formatting), just return the first so it doesn't look like awkward repetition --->
		<cfif str1 eq str2>
			<cfreturn str1>
		<!--- Otherwise return both dates separated by a hyphen --->
		<cfelse>
			<cfreturn "#str1# to #str2#">
		</cfif>
	</cffunction>


	<!--- *********** PRIVATE METHODS *********** --->
	<cffunction access="package" name="abstractErrorMessage">
		<cfthrow
			message="Illegal call of an abstract method"
			detail="This method is meant to be considered 'abstract' and should be implemented in the specific renderer class for this alert type.">
	</cffunction>
</cfcomponent>