<cfcomponent
	extends="BasePeriodOverPeriodAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	output="false">

	<!--- The "column of interest" for calculations; see this renderer's superclass for details --->
	<cfset this.conceptDataField = "total_cost">


	<!--- Renders the alert with specific information based on the alert type --->
	<cffunction name="renderAlerts"
				access="public" returntype="array"
			 	hint="Renders the alert(s) to be sent and persisted.">

		<cfset var result = ArrayNew(1)>
		<cfset var thisAlert = createAlertInstance()>

		<!--- Percentage change could be positive or negative; let's express in human-readable format depending --->
		<cfset var verbString = IIF(this.data.percentDifference gte 0, "'increased'", "'decreased'")>
		<cfset var percentChange = Abs(this.data.percentDifference)>

		<!--- Render alert "content" --->
		<cfsavecontent variable="thisAlert.alert_content">
			<cfoutput>Cost has #verbString# by #NumberFormat(percentChange, '9.9')#%</cfoutput>
		</cfsavecontent>

		<!--- Render alert "body" --->
		<cfsavecontent variable="thisAlert.alert_body">
			<cfoutput>
				The newly-entered cost (for #formatDatePair(this.data.startOfRecentPeriod, this.data.endOfRecentPeriod)#) is #LSCurrencyFormat(this.data.recentValue)#.
				The previous cost (from #formatDatePair(this.data.startOfPriorPeriod, this.data.endOfPriorPeriod)#) was #LSCurrencyFormat(this.data.priorValue)#.
			</cfoutput>
		</cfsavecontent>

		<!--- Return the "rendered" alert --->
		<cfset ArrayAppend(result, thisAlert)>
		<cfreturn result>
	</cffunction>

</cfcomponent>