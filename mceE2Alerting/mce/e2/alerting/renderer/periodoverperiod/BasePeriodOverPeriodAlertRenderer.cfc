<cfcomponent
	extends="mce.e2.alerting.renderer.BaseEnergyUsageComparisonAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	output="false">


	<!--- Determine whether an alert should be spawned, based on the conditions in the subscription --->
	<cffunction name="isAlertConditionsMet"
				access="public" returntype="boolean"
			 	hint="Returns whether the alert subscription's conditions have been met; if false, then the alert should not be spawned at this time.">

		<!--- This type of alert does not have any "conditions" conceptually, so always answer "true" --->
		<cfset var conditions = getConditionsAsStruct()>

		<!--- Create "this.data" structure... because it is on "this", the "render" method in the subclasses can access these values without re-querying --->
		<cfset this.data = structNew()>
		<cfset this.data.percentDifference = 0>

		<!--- Alert condition: what percent over/under the prior period's value should trigger an alert? --->
		<cfset this.data.percentThreshold = getConditionValue("changeThresholdPercentage")>
		<!--- Alert condition: how many months *previous* should the prior period start? (typically 12, to compare to same period in prior year) --->
		<cfset this.data.priorPeriodOffsetMonths = getConditionValue("priorPeriodOffsetMonths")>
		<!--- Alert condition: how many months *long* are the two periods? (1 for a month, 3 for a quarter, etc)  --->
		<cfset this.data.periodLengthMonths = getConditionValue("periodLengthMonths")>

		<!--- Dates that we are interested in --->
		<cfset this.data.startOfRecentPeriod = firstOfMonth(now())>
		<cfset this.data.endOfRecentPeriod 	 = lastOfMonth(now())>
		<cfif this.data.periodLengthMonths gt 1>
			<cfset this.data.startOfRecentPeriod = DateAdd("m", -(this.data.periodLengthMonths-1), this.data.startOfRecentPeriod)>
		</cfif>
		<cfset this.data.startOfPriorPeriod  = DateAdd("m", -this.data.priorPeriodOffsetMonths, this.data.startOfRecentPeriod)>
		<cfset this.data.endOfPriorPeriod    = DateAdd("m", -this.data.priorPeriodOffsetMonths, this.data.endOfRecentPeriod)>

		<!--- Data that we need --->
		<cfset this.data.recentData = getEnergyUsageData(this.data.startOfRecentPeriod, this.data.endOfRecentPeriod, "actual")>
		<cfset this.data.priorData  = getEnergyUsageData(this.data.startOfPriorPeriod, this.data.endOfPriorPeriod, "actual")>

		<!--- Stop here if there is no recent data --->
		<cfif this.data.recentData.recordCount eq 0>
			<cfreturn false>
		</cfif>

		<!--- Do some simple calculations --->
		<cfset this.data.recentValue = super.queryQuickSum(this.data.recentData, this.conceptDataField)>
		<cfset this.data.priorValue = super.queryQuickSum(this.data.priorData, this.conceptDataField)>

		<!--- Stop here if there is no comparison value (logically it doesn't make sense to continue, and also we'd have divide-by-zero problems) --->
		<cfif this.data.priorValue eq 0>
			<cfreturn false>
		</cfif>

		<!--- Practically speaking, it is probably not sensible to consider sending an alert if the recentValue is zero either (probably an accident or something) --->
		<cfif this.data.recentValue eq 0>
			<cfreturn false>
		</cfif>

		<!--- Now we know enough to calculate the difference between the two values --->
		<cfset this.data.percentDifference = ((this.data.recentValue - this.data.priorValue) / this.data.priorValue) * 100>

		<!--- Now we can decide if the conditions were met --->
		<cftrace category="Alerting" text="Comparing actual percent difference #this.data.percentDifference# to condition threshold #this.data.percentThreshold# for period #dateFormat(this.data.startOfRecentPeriod)# to #dateFormat(this.data.endOfRecentPeriod)# (#this.data.recentValue#), vs #dateFormat(this.data.startOfPriorPeriod)# to #dateFormat(this.data.endOfPriorPeriod)# (#this.data.priorValue#)">
		<cfif Abs(this.data.percentDifference) gte this.data.percentThreshold>
			<!--- Return "true" to indicate that conditions have been met and we want to move to the rendering phase --->
			<cfreturn true>
		</cfif>

		<!--- If we get here, the conditions must not have been met --->
		<cfreturn false>
	</cffunction>



</cfcomponent>