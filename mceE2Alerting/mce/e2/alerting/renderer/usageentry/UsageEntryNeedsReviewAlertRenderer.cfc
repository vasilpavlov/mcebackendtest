<cfcomponent name="CompletedUsage AlertRenderer Component"
	extends="mce.e2.alerting.renderer.usageentry.BaseUsageEntryAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	hint="This component is the CompletedUsage alert renderer component.">


	<!--- Renders the alert with specific information based on the alert type --->
	<cffunction name="renderAlerts"
				access="public" returntype="array"
			 	hint="Renders the alert to be sent and persisted.">

		<cfset var result = ArrayNew(1)>
		<cfset var thisAlert = "">

		<!--- Get energy usage records... we'll get only those records with the appropriate status --->
		<cfset var energyUsageRecords = super.getAppropriateEnergyUsageRecordsSinceLastTimeSubscriptionWasPolled(usage_status_code='needs_review')>

		
		<!--- For each energy usage record found --->
		<cfloop query="energyUsageRecords">
			<!--- Create new alert --->
			<cfset thisAlert = createAlertInstance()>

			<!--- Populate the alert's textual content --->
			<cfset thisAlert.alert_content = "#DateFormat(period_end, 'MMM YY')# Energy usage entry is ready for review for #energyUsageRecords.energyAccountFriendlyName# at #energyUsageRecords.propertyFriendlyName#">
			<cfset thisAlert.alert_body = "Review is needed for the entry for the period #DateFormat(energyUsageRecords.usagePeriodStart)# to #DateFormat(energyUsageRecords.usagePeriodEnd)#. The total consumption was #NumberFormat(energyUsageRecords.recorded_value)# #energyUsageRecords.energyUnitFriendlyName#, and the total cost was #LSCurrencyFormat(energyUsageRecords.total_cost)#.">
			<cfset thisAlert.alert_context_notes = energyUsageRecords.energyTypeFriendlyName>
			<cfset thisAlert.alert_location_notes = energyUsageRecords.propertyFriendlyName>
			<cfset thisAlert.energy_account_uid = energyUsageRecords.energy_account_uid>
			
			<!--- Populate the alert's meta data --->
			<cfset addAlertMetaData(thisAlert, "energy_usage_uid", energyUsageRecords.energy_usage_uid)>
			<cfset addAlertMetaData(thisAlert, "energy_account_uid", energyUsageRecords.energy_account_uid)>
			<cfset addAlertMetaData(thisAlert, "energy_account_friendly_name", energyUsageRecords.energyAccountFriendlyName)>
			
			<!--- Add the new alert to the list we are returning --->
			<cfset arrayAppend(result, thisAlert)>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<!--- Return a number of minutes to delay the visibility of this alert --->
	<cffunction name="getAlertVisibilityDelay" access="public" returntype="numeric"	hint="The date when the alert can be considered to be visible to the outside world">
		<!--- Zero is good for demos, but in practice you probably want a bit of a delay so that alerts don't go out the second the user hits save, etc --->
		<cfreturn 30>
	</cffunction>

</cfcomponent>