<cfcomponent name="EnterUsage AlertRenderer Component"
	extends="mce.e2.alerting.renderer.usageentry.BaseUsageEntryAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	hint="This component is the EnterUsage alert renderer component.">


	<!---
		Because this type of alert is conceptually about records that have not changed,
		we are specifically overriding here to negate the logic from our ancestors
	 --->
	<cffunction name="preFilterAlertSubscriptionList"
				access="public" returntype="string"
			 	hint="Given a list of subscription uids, filters the list to only include the subscriptions that may be relevant at this time.">
		<cfargument name="alert_subscription_uid_list" type="string" required="true">

		<!--- We are returning the input unchanged (no pre-filtering) --->
		<cfreturn arguments.alert_subscription_uid_list>
	</cffunction>


	<!--- Implement IAlertRenderer interface --->
	<!--- Return whether the alert is still relevant --->
	<!--- Specific alert renderers can override to return true/false based on what makes sense --->
	<cffunction name="isAlertStillRelevant"
				access="public" returntype="boolean"
			 	hint="Whether this alert is still relevant, for instance if the alert is a data entry task, it may not be relevant anymore if the task has been completed via some other path.">
		<cfargument name="alert" type="mce.e2.alerting.vo.Alert" required="true">

		<cfset var elapsedDays = getConditionValue("elapsedDays")>
		<cfset var qAccountData = super.getEnergyAccountsDueForDataEntryE2(elapsedDays, alert.energy_account_uid)>

		<cfreturn qAccountData.recordCount gt 0>
	</cffunction>

	<!--- Renders the alert with specific information based on the alert type --->
	<cffunction name="renderAlerts"
				access="public" returntype="array"
			 	hint="Renders the alert(s) to be sent and persisted.">

		<cfset var result = ArrayNew(1)>
		<cfset var thisAlert = "">
		<cfset var elapsedDays = getConditionValue("elapsedDays")>

		<!--- Get energy account information, with last dates... --->
		<cfset var energyAccountInfoRecords = super.getEnergyAccountsDueForDataEntryE2(elapsedDays)>

		<!--- For each energy usage record found --->
		<cfloop query="energyAccountInfoRecords">
			<!--- Create new alert --->
			<cfset thisAlert = createAlertInstance()>
			<cfset thisAlert.energy_account_uid = energyAccountInfoRecords.energy_account_uid>
			<cfset thisAlert.alert_location_notes = energyAccountInfoRecords.propertyFriendlyName>
			<cfset thisAlert.alert_subject_notes = energyAccountInfoRecords.energyProviderFriendlyName>
			<cfset thisAlert.alert_context_notes = energyAccountInfoRecords.energyTypeFriendlyName>

			<!--- Populate the alert's textual content --->
			<cfsavecontent variable="thisAlert.alert_content">
				<cfoutput>
					Data entry is due for #energyAccountInfoRecords.friendly_name#
				</cfoutput>
			</cfsavecontent>
			<cfsavecontent variable="thisAlert.alert_body">
				<cfoutput>
					It has been #DateDiff('d', energyAccountInfoRecords.mostRecentEnergyUsageDate, now())#
					days since data entry was done for this account
					(last entry was for period ending #DateFormat(energyAccountInfoRecords.mostRecentEnergyUsageDate)#).
				</cfoutput>
			</cfsavecontent>

			<!--- Populate the alert's meta data --->

			<cfset addAlertMetaData(thisAlert, "energy_account_friendly_name", energyAccountInfoRecords.friendly_name)>
			<cfset addAlertMetaData(thisAlert, "energy_account_uid", energyAccountInfoRecords.energy_account_uid)>
			<cfset addAlertMetaData(thisAlert, "last_period_end", energyAccountInfoRecords.mostRecentEnergyUsageDate)>

			<!--- Add the new aler to the list we are returning --->
			<cfset arrayAppend(result, thisAlert)>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<!--- Return a number of minutes to delay the visibility of this alert --->
	<cffunction name="getAlertVisibilityDelay" access="public" returntype="numeric"	hint="The date when the alert can be considered to be visible to the outside world">
		<cfreturn 0>
	</cffunction>

</cfcomponent>