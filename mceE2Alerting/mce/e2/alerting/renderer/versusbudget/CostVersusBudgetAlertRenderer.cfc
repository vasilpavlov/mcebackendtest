<cfcomponent
	extends="BaseVersusBudgetAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	output="false">

	<!--- The "column of interest" for calculations; see this renderer's superclass for details --->
	<cfset this.conceptDataField = "total_cost">
	<cfset this.budgetedDataField = "budgeted_cost">


	<!--- Renders the alert with specific information based on the alert type --->
	<cffunction name="renderAlerts"
				access="public" returntype="array"
			 	hint="Renders the alert(s) to be sent and persisted.">

		<cfset var result = ArrayNew(1)>
		<cfset var thisAlert = createAlertInstance()>

		<!--- Percentage change could be positive or negative; let's express in human-readable format depending --->
		<cfset var verbString = IIF(this.data.percentDifference gte 0, "'over'", "'under'")>
		<cfset var percentChange = Abs(this.data.percentDifference)>

		<!--- Render alert "content" --->
		<cfsavecontent variable="thisAlert.alert_content">
			<cfoutput>Cost is #verbString# budget by #NumberFormat(percentChange, '9.9')#%</cfoutput>
		</cfsavecontent>

		<!--- Render alert "body" --->
		<cfsavecontent variable="thisAlert.alert_body">
			<cfoutput>
				For period #formatDatePair(this.data.startOfPeriod, this.data.endOfPeriod)#,
				the actual cost was #LSCurrencyFormat(this.data.actualValue)#.
				The budgeted cost was #LSCurrencyFormat(this.data.budgetedValue)#.
			</cfoutput>
		</cfsavecontent>

		<!--- Return the "rendered" alert --->
		<cfset ArrayAppend(result, thisAlert)>
		<cfreturn result>
	</cffunction>

</cfcomponent>