<cfcomponent name="AlertRenderer Factory Component"
			 output="true"
			 extends="mce.e2.alerting.common.BaseComponent"
			 hint="This component is the alert renderer factory component for all alert renderer *.cfc's.">

	<!--- Create the *.cfc constructor --->
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the alert renderer factory component.">

		<!--- Return an instance of the component --->
		<cfreturn this>

	</cffunction>

	<!--- Return the appropriate renderer cfc based on the alert type --->
	<cffunction name="getRenderer"
				access="public" returntype="mce.e2.alerting.renderer.IAlertRenderer"
			 	hint="Returns the proper alert renderer, note that the alert renderer is not populated yet, just constructed.">

		<cfargument name="alertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cftry>
			<cfset local.renderer = getRendererClass(arguments.alertSubscription.alertTypeCfcName).init(arguments.alertSubscription)>

			<!--- If there was a problem initializing the component throw an exception - we're relying on the alert component to be properly implemented --->
			<cfcatch >
				<cfthrow
					message="The RendererFactory component could not INITIALIZE the appropriate renderer for #arguments.alertSubscription.alert_type_code#.  Please verify the renderer exists and is created properly."
					detail="#cfcatch.Message# #cfcatch.Detail#">
			</cfcatch>
		</cftry>

		<cfreturn local.renderer>
	</cffunction>


	<!--- Return the appropriate renderer cfc based on the alert type --->
	<cffunction name="getRendererClass"
				access="public" returntype="mce.e2.alerting.renderer.IAlertRenderer"
			 	hint="Returns the proper alert renderer, note that the alert renderer is not populated yet, just constructed.">

		<cfargument name="alertTypeCfcName" required="true" type="string">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cftry>
			<cfset local.renderer = createObject("component", arguments.alertTypeCfcName)>

			<!--- If there was a problem initializing the component throw an exception - we're relying on the alert component to be properly implemented --->
			<cfcatch>
				<cfrethrow>
				<cfthrow
					message="The RendererFactory component could not CREATE the appropriate renderer via #arguments.alertTypeCfcName#.  Please verify the renderer exists and is created properly."
					detail="#cfcatch.Message# #cfcatch.Detail# #cfcatch.ExtendedInfo#">
			</cfcatch>
		</cftry>

		<cfreturn local.renderer>
	</cffunction>


</cfcomponent>