<cfcomponent displayname="Base Component"
			 output="false"
			 hint="This component is the base component for all other *.cfc's">

	<cffunction name="createUniqueIdentifier"
				hint="This method is used to create a new Microsoft UUID/GUID value."
				returntype="string"
				access="public"
				output="false">

		<!--- Return the guid value --->
		<cfreturn insert("-", CreateUUID(), 23)>

	</cffunction>

	<cffunction name="isUniqueIdentifier"
				hint="This method is used validate that a guid/unique identifier is Microsoft friendly."
				returntype="boolean"
				access="public"
				output="false">

		<!--- Define the guid argument - the unique identifier to be validated --->
		<cfargument name="guid" type="string" required="true"
					hint="defines the unique identifier to be validated as Microsoft friendly.">

		<!--- Initialize the local scope for all variables within this method --->
		<cfset var local = structnew()>

		<cfscript>

			// Initialize the validationResult variable (used to store the validation results)
			local.validationResult = true;

			// Is the length of the GUID 36 Characters?
			if( len(arguments.guid) neq 36 ){

				// No; this is not a Microsoft GUID.  Exit the function as validation failed.
				local.validationResult = false;

			// Validate that the specified GUID only contains numbers from 0-9, letters from
			// A-F, and that the GUID format required is followed.  All GUID values must follow
			// the Microsoft GUID format of 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'.

			// Were any invalid characters found in the specified guid value?
			} else if( refindnocase("^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$", arguments.guid) eq 0 ){

				// Yes; there were values in the specified GUID that were not present in the RegEx
				// used to validate Microsoft compliant GUID values.  Exit the function as validation
				// failed.

				local.validationResult = false;

			}

		</cfscript>

		<!--- Return the validation result --->
		<cfreturn local.validationResult>

	</cffunction>

	<cffunction name="queryRowToStruct"
				hint="This method is used to convert a query row to a structure."
				returntype="any"
				access="public"
				output="false">

		<!--- Default the query argument (identifies the query to transform ) --->
		<cfargument	name="query"
					hint="defines the query to transform"
					required="true"
					type="query">

		<!--- Default the row number (identifies the query row to transform ) --->
		<cfargument	name="row"
					hint="defines the query row to transform"
					required="true"
					type="numeric"
					default="1">

		<!--- Initialize the local scope for all variables within this method --->
		<cfset var local = structnew()>

		<cfscript>

			// Default the looping variable
			local.rowIndex = 1;

			// Place each of the query column in a list
			local.queryColumns = listtoArray(arguments.query.columnList);

			// Default the looplength variable
			local.loopLength = arraylen(local.queryColumns);

			// Initialize the return object
			local.queryStruct = structnew();

			// Loop over each of the columns in the query and build the structure
			for( local.rowIndex = 1; local.rowIndex lte local.loopLength; local.rowIndex = local.rowIndex + 1 ){

				// Build the structure using each column as a key/value pair
				local.queryStruct[local.queryColumns[local.rowIndex]] = arguments.query[local.queryColumns[local.rowIndex]][arguments.row];

			}

		</cfscript>

		<!--- Return the structure --->
		<cfreturn local.queryStruct>

	</cffunction>

	<cffunction name="removeMilisecondsFromDate"
				returnType="string"
				access="public"
				hint="This method is used to remove the miliseconds from a given ODBC date.">

		<!--- Define the method arguments --->
		<cfargument name="date" type="date" required="true" hint="Describes the date from which miliseconds will be removed.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Reformat the date; remove miliseconds --->
		<cfset local.newDate = dateFormat(arguments.date, 'YYYY-MM-DD') & ' ' & timeFormat(arguments.date, 'HH:MM:SS')>

		<!--- Return the date / time --->
		<cfreturn createOdbcDateTime(local.newDate)>

	</cffunction>

	<!--- Utility function, returns the date that "floors" the passed-in date to the first date in that month --->
	<!--- So, pass in "March 18 2009", and you get back "March 1 2009" --->
	<cffunction name="firstOfMonth" returntype="date">
		<cfargument name="d" type="date" required="true">
		<cfreturn CreateDate(Year(d), Month(d), 1)>
	</cffunction>

	<cffunction name="lastOfMonth" returntype="date">
		<cfargument name="d" type="date" required="true">
		<cfreturn CreateDate(Year(d), Month(d), DaysInMonth(d))>
	</cffunction>

	<!--- Utility function, removes "excessive" whitespace from a string --->
	<cffunction name="cleanWhitespace" returntype="string">
		<cfargument name="str" type="string" required="true">
		<!--- Reduce any whitespace character to a space --->
		<cfset str = REReplace(Trim(str), "[[:space:]]+", " ", "all")>
		<!--- Reduce any two-or-more-whitespace-characters-together (will be only spaces due to above) into one --->
		<cfset str = REReplace(Trim(str), "\s{2,}", " ", "all")>
		<cfreturn str>
	</cffunction>

	<!--- Utility function, returns the sum of a query column (column must be numeric) --->
	<cffunction name="queryQuickSum" returntype="numeric">
		<cfargument name="qry" type="query" required="true">
		<cfargument name="columnName" type="string" required="true">

		<cfset var qResult = "">

		<cfquery dbtype="query" name="qResult">
			SELECT SUM(#columnName#) as quickSum
			FROM qry
		</cfquery>

		<cfreturn Val(qResult.quickSum)>
	</cffunction>

	<!--- Utility function, returns an array as a "map" structure, keyed by some single property of the items in an array --->
	<!--- Useful when you have an array, but want to be able to "find" items within by name or code, etc --->
	<cffunction name="arrayToMap" returntype="struct" output="false">
		<cfargument name="ar" type="array" required="true">
		<cfargument name="fieldName" type="variableName" required="true">

		<cfset var map = StructNew()>
		<cfset var item = "">

		<cfloop array="#ar#" index="item">
			<cfset map[item[fieldName]] = item>
		</cfloop>

		<cfreturn map>
	</cffunction>

	<!--- Utility function, can be used to get a simple list (comma-separated) of ID's, codes, names, or any other single field from an array of VOs--->
	<cffunction name="getFieldListFromVoArray" output="false">
		<cfargument name="ar" type="array" required="true">
		<cfargument name="fieldName" type="string" required="true">

		<cfset var result = "">
		<cfset var item = "">

		<cfloop array="#ar#" index="item">
			<cfset result = ListAppend(result, item[fieldName])>
		</cfloop>

		<cfreturn result>
	</cffunction>
</cfcomponent>