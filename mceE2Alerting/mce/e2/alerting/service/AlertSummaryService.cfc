<cfcomponent displayname="Alert Summary Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to display Alert related information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAlertSummaryDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage AlertSummary data (dependency injection).">
		<cfargument name="bean" type="mce.e2.alerting.db.AlertSummaryDelegate" hint="Describes the *.cfc used to manage database interactions related to Alert information.">
		<cfset this.AlertSummaryDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAlertSummaryVo"
				access="public" returntype="mce.e2.alerting.vo.AlertSummary"
				hint="This method is used to retrieve / return an empty Alert information value object.">
		<cfreturn this.AlertSummaryDelegate.getEmptyAlertSummaryComponent()>
	</cffunction>

	<cffunction name="getAlertSummaries"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of AlertSummaries.">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="true" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>

		<cfreturn this.AlertSummaryDelegate.getAlertSummariesAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getAlertSummary"
				access="public" returntype="mce.e2.alerting.vo.AlertSummary"
				hint="This method is used to return a populated AlertSummary vo (value object).">
		<cfargument name="AlertSummary" type="mce.e2.alerting.vo.AlertSummary" required="true" hint="Describes the AlertSummary VO object containing the properties of the Alert to be retrieved." />
		<cfreturn this.AlertSummaryDelegate.getAlertSummaryAsComponent(alert_uid=arguments.AlertSummary.alert_uid)>
	</cffunction>

</cfcomponent>