<cfcomponent displayname="Alert Interval Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Alert Interval information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAlertIntervalDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage Alert Interval data (dependency injection).">
		<cfargument name="bean" type="mce.e2.alerting.db.AlertIntervalDelegate" hint="Describes the *.cfc used to manage database interactions related to Alert Interval information.">

		<cfset this.AlertIntervalDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAlertIntervalVo"
				access="public" returntype="mce.e2.alerting.vo.AlertInterval"
				hint="This method is used to retrieve / return an empty Alert Interval information value object.">
		<cfreturn this.AlertIntervalDelegate.getEmptyAlertIntervalComponent()>
	</cffunction>

	<cffunction name="getAlertIntervals"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Alert Intervals.">
		<cfreturn this.AlertIntervalDelegate.getAlertIntervalsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAlertInterval"
				access="public" returntype="mce.e2.alerting.vo.AlertInterval"
				hint="This method is used to return a populated Alert Interval vo (value object).">
		<cfargument name="AlertInterval" type="mce.e2.alerting.vo.AlertInterval" required="true" hint="Describes the Alert Interval VO object containing the properties of the Alert Interval to be retrieved." />
		<cfreturn this.AlertIntervalDelegate.getAlertIntervalAsComponent(alert_interval_code=arguments.AlertInterval.alert_interval_code)>
	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewAlertInterval"
				access="public" returntype="string"
				hint="This method is used to persist a new Alert Interval record to the application database.">
		<cfargument name="AlertInterval" type="mce.e2.alerting.vo.AlertInterval" required="true" hint="Describes the Alert Interval VO object containing the details of the Alert Interval to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.AlertInterval = setAuditProperties(arguments.AlertInterval, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<!---<cfset local.AlertInterval = setPrimaryKey(local.AlertInterval)>--->

		<!--- Save the modified Alert --->
		<cfset this.AlertIntervalDelegate.saveNewAlertInterval(AlertInterval=local.AlertInterval)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.AlertInterval.alert_interval_code>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewAlertInterval"
				access="public" returntype="mce.e2.alerting.vo.AlertInterval"
				hint="This method is used to persist a new Alert Interval record to the application database.">
		<cfargument name="AlertInterval" type="mce.e2.alerting.vo.AlertInterval" required="true" hint="Describes the Alert Interval VO object containing the details of the Alert Interval to be saved and returned." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewAlertInterval(AlertInterval=arguments.AlertInterval)>

		<!--- Set the primary key --->
		<cfset arguments.AlertInterval.alert_interval_code = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.AlertInterval>

	</cffunction>

	<cffunction name="saveExistingAlertInterval"
				access="public" returntype="mce.e2.alerting.vo.AlertInterval"
				hint="This method is used to save an existing Alert Interval information to the application database.">
		<cfargument name="AlertInterval" type="mce.e2.alerting.vo.AlertInterval" required="true" hint="Describes the Alert Interval VO object containing the details of the Alert Interval to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.AlertInterval = setAuditProperties(arguments.AlertInterval, "modify")>

		<!--- Save the modified AlertInterval --->
		<cfset this.AlertIntervalDelegate.saveExistingAlertInterval(AlertInterval=local.AlertInterval)/>

		<!--- Return the modified AlertInterval --->
		<cfreturn local.AlertInterval>

	</cffunction>

</cfcomponent>