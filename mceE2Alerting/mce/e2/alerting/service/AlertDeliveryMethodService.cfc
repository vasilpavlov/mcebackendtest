<cfcomponent displayname="Alert Delivery Method Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Alert Delivery Method information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAlertDeliveryMethodDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Alert Delivery Method data (dependency injection).">
		<cfargument name="bean" type="mce.e2.alerting.db.AlertDeliveryMethodDelegate" hint="Describes the *.cfc used to manage database interactions related to Alert Delivery Method information.">
		<cfset this.AlertDeliveryMethodDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAlertDeliveryMethodVo" 
				access="public" returntype="mce.e2.alerting.vo.AlertDeliveryMethod"
				hint="This method is used to retrieve / return an empty Alert Delivery Method information value object.">
		<cfreturn this.AlertDeliveryMethodDelegate.getEmptyAlertDeliveryMethodComponent()>
	</cffunction>

	<cffunction name="getAlertDeliveryMethods" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Alert Delivery Methods.">
		<cfreturn this.AlertDeliveryMethodDelegate.getAlertDeliveryMethodsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAlertDeliveryMethod" 
				access="public" returntype="mce.e2.alerting.vo.AlertDeliveryMethod" 
				hint="This method is used to return a populated Alert Delivery Method vo (value object)."> 
		<cfargument name="AlertDeliveryMethod" type="mce.e2.alerting.vo.AlertDeliveryMethod" required="true" hint="Describes the Alert Delivery Method VO object containing the properties of the Alert DeliveryMethod to be retrieved." /> 
		<cfreturn this.AlertDeliveryMethodDelegate.getAlertDeliveryMethodAsComponent(alert_delivery_method_code=arguments.AlertDeliveryMethod.alert_delivery_method_code)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewAlertDeliveryMethod" 
				access="public" returntype="string" 
				hint="This method is used to persist a new Alert Delivery Method record to the application database."> 
		<cfargument name="AlertDeliveryMethod" type="mce.e2.alerting.vo.AlertDeliveryMethod" required="true" hint="Describes the Alert Delivery Method VO object containing the details of the Alert Delivery Methodto be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.AlertDeliveryMethod = setAuditProperties(arguments.AlertDeliveryMethod, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<!---<cfset local.AlertDeliveryMethod = setPrimaryKey(local.AlertDeliveryMethod)>--->
		
		<!--- Save the modified AlertDeliveryMethod --->
		<cfset this.AlertDeliveryMethodDelegate.saveNewAlertDeliveryMethod(AlertDeliveryMethod=local.AlertDeliveryMethod)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.AlertDeliveryMethod.alert_delivery_method_code>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewAlertDeliveryMethod" 
				access="public" returntype="mce.e2.alerting.vo.AlertDeliveryMethod" 
				hint="This method is used to persist a new Alert Delivery Method record to the application database."> 
		<cfargument name="AlertDeliveryMethod" type="mce.e2.alerting.vo.AlertDeliveryMethod" required="true" hint="Describes the Alert Delivery Method VO object containing the details of the Alert Delivery Method to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewAlertDeliveryMethod(AlertDeliveryMethod=arguments.AlertDeliveryMethod)>

		<!--- Set the primary key --->
		<cfset arguments.AlertDeliveryMethod.alert_delivery_method_code = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.AlertDeliveryMethod>

	</cffunction>

	<cffunction name="saveExistingAlertDeliveryMethod" 
				access="public" returntype="mce.e2.alerting.vo.AlertDeliveryMethod" 
				hint="This method is used to save an existing Alert Delivery Method information to the application database."> 
		<cfargument name="AlertDeliveryMethod" type="mce.e2.alerting.vo.AlertDeliveryMethod" required="true" hint="Describes the Alert Delivery Method VO object containing the details of the Alert Delivery Method to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.AlertDeliveryMethod = setAuditProperties(arguments.AlertDeliveryMethod, "modify")>
		
		<!--- Save the modified AlertDeliveryMethod --->
		<cfset this.AlertDeliveryMethodDelegate.saveExistingAlertDeliveryMethod(AlertDeliveryMethod=local.AlertDeliveryMethod)/>

		<!--- Return the modified AlertDeliveryMethod --->
		<cfreturn local.AlertDeliveryMethod>	

	</cffunction>
	
</cfcomponent>