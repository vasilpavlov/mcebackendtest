<cfcomponent displayname="Alert Status Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Alert Status information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAlertStatusDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Alert Status data (dependency injection).">
		<cfargument name="bean" type="mce.e2.alerting.db.AlertStatusDelegate" hint="Describes the *.cfc used to manage database interactions related to Alert Status information.">
		<cfset this.AlertStatusDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAlertStatusVo" 
				access="public" returntype="mce.e2.alerting.vo.AlertStatus"
				hint="This method is used to retrieve / return an empty Alert Status information value object.">
		<cfreturn this.AlertStatusDelegate.getEmptyAlertStatusComponent()>
	</cffunction>

	<cffunction name="getAlertStatuses" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Alert Statuss.">
		<cfreturn this.AlertStatusDelegate.getAlertStatusesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAlertStatus" 
				access="public" returntype="mce.e2.alerting.vo.AlertStatus" 
				hint="This method is used to return a populated Alert Status vo (value object)."> 
		<cfargument name="AlertStatus" type="mce.e2.alerting.vo.AlertStatus" required="true" hint="Describes the Alert Status VO object containing the properties of the Alert Status to be retrieved." /> 
		<cfreturn this.AlertStatusDelegate.getAlertStatusAsComponent(alert_status_code=arguments.AlertStatus.alert_status_code)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewAlertStatus" 
				access="public" returntype="string" 
				hint="This method is used to persist a new Alert Status record to the application database."> 
		<cfargument name="AlertStatus" type="mce.e2.alerting.vo.AlertStatus" required="true" hint="Describes the Alert Status VO object containing the details of the Alert Statusto be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.AlertStatus = setAuditProperties(arguments.AlertStatus, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<!---<cfset local.AlertStatus = setPrimaryKey(local.AlertStatus)>--->
		
		<!--- Save the modified AlertStatus --->
		<cfset this.AlertStatusDelegate.saveNewAlertStatus(AlertStatus=local.AlertStatus)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.AlertStatus.alert_status_code>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewAlertStatus" 
				access="public" returntype="mce.e2.alerting.vo.AlertStatus" 
				hint="This method is used to persist a new Alert Status record to the application database."> 
		<cfargument name="AlertStatus" type="mce.e2.alerting.vo.AlertStatus" required="true" hint="Describes the Alert Status VO object containing the details of the Alert Status to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewAlertStatus(AlertStatus=arguments.AlertStatus)>

		<!--- Set the primary key --->
		<cfset arguments.AlertStatus.alert_status_code = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.AlertStatus>

	</cffunction>

	<cffunction name="saveExistingAlertStatus" 
				access="public" returntype="mce.e2.alerting.vo.AlertStatus" 
				hint="This method is used to save an existing Alert Status information to the application database."> 
		<cfargument name="AlertStatus" type="mce.e2.alerting.vo.AlertStatus" required="true" hint="Describes the Alert Status VO object containing the details of the Alert Status to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.AlertStatus = setAuditProperties(arguments.AlertStatus, "modify")>
		
		<!--- Save the modified AlertStatus --->
		<cfset this.AlertStatusDelegate.saveExistingAlertStatus(AlertStatus=local.AlertStatus)/>

		<!--- Return the modified AlertStatus --->
		<cfreturn local.AlertStatus>	

	</cffunction>
	
</cfcomponent>