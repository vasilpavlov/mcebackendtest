<cfcomponent hint="Utility functions to make it easier to perform certain Coldspring operations" output="false">

	<cffunction name="configureColdspringRemoteBeans" access="public" returntype="void" hint="Utility function to configure a factory with a config file of remote bean proxy entries.">
		<cfargument name="factory" type="coldspring.beans.DefaultXmlBeanFactory" required="true" hint="A Coldspring bean factory instance">
		<cfargument name="remoteBeanConfigFile" type="string" required="true" hint="Relative path to the Coldspring-style xml file with the remote bean proxy entries">

		<cfset var xmlFileContents = "">
		<cfset var configXml = "">
		<cfset var configArray = "">
		<cfset var i = "">

		<cffile
			action="read"
			file="#ExpandPath(remoteBeanConfigFile)#"
			variable="xmlFileContents">

		<cfset configXml = XmlParse(xmlFileContents)>
<!--- 		<cfset configArray = XmlSearch(configXml, "beans/bean/property[@name='serviceName']")> --->
		<cfset configArray = XmlSearch(configXml, "beans/bean")>


		<cfloop from="1" to="#ArrayLen(configArray)#" index="i">
			<cfset serviceBeanName = XmlSearch(configArray[i], "property[@name='target']/ref")>
			<cfset serviceBeanName = serviceBeanName[1].XmlAttributes.bean>
			<cfset remoteBeanName = XmlSearch(configArray[i], "property[@name='serviceName']/value")>
			<cfset remoteBeanName = remoteBeanName[1].XmlText>
			<cfset remoteBeanPath = XmlSearch(configArray[i], "property[@name='relativePath']/value")>
			<cfset remoteBeanPath = remoteBeanPath[1].XmlText>
			<cfset remoteBeanPath = ExpandPath("#remoteBeanPath#/#remoteBeanName#.cfc")>

			<cfset serviceBeanDef = factory.getBeanDefinition(serviceBeanName)>
			<cfset serviceBeanPath = getComponentMetaData(serviceBeanDef.getBeanClass()).path>

				<cfset x= factory.getBean(serviceBeanName)>
				<cfset x= factory.getBeanDefinition("#remoteBeanName#")>

			<cfif (not fileExists(remoteBeanPath)) or (getFileInfo(remoteBeanPath).lastModified lt getFileInfo(serviceBeanPath).lastModified)>
<!--- 				<cfset factory.getBean(remoteBeanName)>
 --->

				<cfset factory.getBean(remoteBeanName)>
			</cfif>


<!--- 			<cfset x= factory.getBean("&#configArray[i].XmlText#")>
			<cfif not x.isConstructed()>
				<cfset x.createRemoteProxy()>
			</cfif> --->
		</cfloop>
	</cffunction>

</cfcomponent>