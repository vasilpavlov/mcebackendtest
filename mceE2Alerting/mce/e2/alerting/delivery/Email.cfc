<cfcomponent name="Email AlertDelivery Component"
			 output="true"
			 extends="BaseDelivery"
			 implements="IDelivery"
			 hint="This component is the Email alert delivery component.">

	<cfset this.alertSkinName = "mce-us">
	<cfset this.baseAlertSkinUrl = "http://beta.mcenergyinc.com">
	<cfset this.alertEmailFrom = "admin@mcenergyinc.com">
	<cfset this.templateFilename = "email-html.cfm">

	<!--- Uncomment/adjust the following line to use local images and assets (when debugging etc) --->
	<!--- <cfset this.baseAlertSkinUrl = "http://localhost:8500/mceE2Alerting"> --->

	<!--- This variable is available to the email template --->
	<cfset this.imageLocation = "#this.baseAlertSkinUrl#/alert-skin/#this.alertSkinName#/images">


	<!--- Delivers the alert with specific information based on the alert delivery method --->
	<cffunction name="deliverAlerts"
				access="public" returntype="any"
			 	hint="Delivers the alerts to their intended destinations.">

		<!--- Initialize the local scope --->
		<cfset var qAlerts = super.alertArrayToQuery(this.alerts)>
		<cfset var local = structNew()>
		<cfset var emailSubject = "Your MCEnergy Update for #dateFormat(now(), 'm/d/yy')#">

		<!--- Sanity-check the email address --->
		<cfif not this.alert_email contains "@">
			<cfthrow
				message="Invalid email">
		</cfif>

		<!--- Sort the alerts we're going to send out now --->
		<!--- We can group by these within a cfoutput block --->
		<cfif qAlerts.recordCount gt 1> <!--- (Can skip sorting if there is only 1 record) --->
			<cfquery dbtype="query" name="qAlerts">
				SELECT DISTINCT *
				FROM
					qAlerts
				ORDER BY
					alert_location_notes,
					alert_context_notes,
					alert_subject_notes,
					alert_visibility_date desc
			</cfquery>
		</cfif>

		<!--- Send the email --->
		<cfmail
			to="#this.alert_email#"
			from="#this.alertEmailFrom#"
			subject="#emailSubject#"
			type="html">
			<!--- Include the template, which does the business of composing the specific text, formatting, of the email --->
			<cfinclude template="../templates/#this.templateFilename#">
		</cfmail>

			<!--- If there was a problem sending the email throw an exception --->
<!--- 			<cfcatch >
				<cfthrow message="The Email component could not send the email.">
			</cfcatch>
		</cftry> --->

	</cffunction>

</cfcomponent>