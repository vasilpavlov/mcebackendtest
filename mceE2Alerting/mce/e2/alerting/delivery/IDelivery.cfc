<cfinterface>

	<!--- Create the *.cfc constructor --->
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the base delivery component.">
		<!--- <cfargument name="alertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription"> --->
		<cfargument name="alerts" required="true" type="array">
		<cfargument name="alert_email" required="true" type="string">
	</cffunction>

	<!--- Delivers the alert with specific information based on the alert delivery method --->
	<cffunction name="deliverAlerts"
				access="public" returntype="any"
			 	hint="Delivers the alerts to their intended destinations."/>

</cfinterface>