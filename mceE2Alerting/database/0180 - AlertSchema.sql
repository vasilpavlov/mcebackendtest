------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Alerting Tables
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table AlertDeliveryMethods
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertDeli__is_ac__4B180DA3]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertDeliveryMethods] DROP CONSTRAINT [DF__AlertDeli__is_ac__4B180DA3]ENDsGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertDeli__creat__4C0C31DC]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertDeliveryMethods] DROP CONSTRAINT [DF__AlertDeli__creat__4C0C31DC]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertDeli__modif__4D005615]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertDeliveryMethods] DROP CONSTRAINT [DF__AlertDeli__modif__4D005615]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertDeliveryMethods]    Script Date: 07/20/2009 14:52:41 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertDeliveryMethods]') AND type in (N'U'))DROP TABLE [dbo].[AlertDeliveryMethods]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertDeliveryMethods]    Script Date: 07/20/2009 14:52:41 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[AlertDeliveryMethods](	[alert_delivery_method_code] [dbo].[lookupcode] NOT NULL,	[friendly_name] [dbo].[friendlyname] NOT NULL,	[is_active] [bit] NOT NULL,	[created_by] [nvarchar](50) NULL,	[created_date] [datetime] NOT NULL,	[modified_by] [nvarchar](50) NULL,	[modified_date] [datetime] NOT NULL, CONSTRAINT [AlertDeliveryMethods_PK] PRIMARY KEY CLUSTERED (	[alert_delivery_method_code] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the active status of a given record (1 = active, 0 = inactive).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods', @level2type=N'COLUMN',@level2name=N'is_active'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that created a  given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods', @level2type=N'COLUMN',@level2name=N'created_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was first created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods', @level2type=N'COLUMN',@level2name=N'created_date'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that last modified a given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods', @level2type=N'COLUMN',@level2name=N'modified_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was last modified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods', @level2type=N'COLUMN',@level2name=N'modified_date'GOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertDeliveryMethodDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to manage the ways an alert can be delivered electronically' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'AlertDeliveryMethods' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alert Delivery Methods' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert Delivery Method' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertDeliveryMethods'GOALTER TABLE [dbo].[AlertDeliveryMethods] ADD  CONSTRAINT [DF__AlertDeli__is_ac__4B180DA3]  DEFAULT ((1)) FOR [is_active]GOALTER TABLE [dbo].[AlertDeliveryMethods] ADD  CONSTRAINT [DF__AlertDeli__creat__4C0C31DC]  DEFAULT (getdate()) FOR [created_date]GOALTER TABLE [dbo].[AlertDeliveryMethods] ADD  CONSTRAINT [DF__AlertDeli__modif__4D005615]  DEFAULT (getdate()) FOR [modified_date]GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table AlertIntervals
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertQueu__is_ac__50D0E6F9]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertIntervals] DROP CONSTRAINT [DF__AlertQueu__is_ac__50D0E6F9]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertQueu__creat__51C50B32]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertIntervals] DROP CONSTRAINT [DF__AlertQueu__creat__51C50B32]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertQueu__modif__52B92F6B]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertIntervals] DROP CONSTRAINT [DF__AlertQueu__modif__52B92F6B]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertIntervals]    Script Date: 07/20/2009 14:55:24 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertIntervals]') AND type in (N'U'))DROP TABLE [dbo].[AlertIntervals]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertIntervals]    Script Date: 07/20/2009 14:55:24 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[AlertIntervals](	[alert_interval_code] [dbo].[lookupcode] NOT NULL,	[friendly_name] [dbo].[friendlyname] NOT NULL,	[is_enabled] [bit] NOT NULL,	[queue_priority] [tinyint] NOT NULL,	[polling_interval] [int] NOT NULL,	[last_polled] [datetime] NULL,	[is_active] [bit] NOT NULL,	[created_by] [nvarchar](50) NULL,	[created_date] [datetime] NOT NULL,	[modified_by] [nvarchar](50) NULL,	[modified_date] [datetime] NOT NULL, CONSTRAINT [PK_AlertIntervals] PRIMARY KEY CLUSTERED (	[alert_interval_code] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the active status of a given record (1 = active, 0 = inactive).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals', @level2type=N'COLUMN',@level2name=N'is_active'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that created a  given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals', @level2type=N'COLUMN',@level2name=N'created_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was first created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals', @level2type=N'COLUMN',@level2name=N'created_date'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that last modified a given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals', @level2type=N'COLUMN',@level2name=N'modified_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was last modified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals', @level2type=N'COLUMN',@level2name=N'modified_date'GOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertIntervalDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to manage the various frequencies possible to trigger alerts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'AlertIntervals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alert Intervals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert Interval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertIntervals'GOALTER TABLE [dbo].[AlertIntervals] ADD  CONSTRAINT [DF__AlertQueu__is_ac__50D0E6F9]  DEFAULT ((1)) FOR [is_active]GOALTER TABLE [dbo].[AlertIntervals] ADD  CONSTRAINT [DF__AlertQueu__creat__51C50B32]  DEFAULT (getdate()) FOR [created_date]GOALTER TABLE [dbo].[AlertIntervals] ADD  CONSTRAINT [DF__AlertQueu__modif__52B92F6B]  DEFAULT (getdate()) FOR [modified_date]GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table AlertStatuses
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertStat__is_ac__5689C04F]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertStatuses] DROP CONSTRAINT [DF__AlertStat__is_ac__5689C04F]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertStat__creat__577DE488]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertStatuses] DROP CONSTRAINT [DF__AlertStat__creat__577DE488]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertStat__modif__587208C1]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertStatuses] DROP CONSTRAINT [DF__AlertStat__modif__587208C1]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertStatuses]    Script Date: 07/20/2009 14:57:03 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertStatuses]') AND type in (N'U'))DROP TABLE [dbo].[AlertStatuses]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertStatuses]    Script Date: 07/20/2009 14:57:04 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[AlertStatuses](	[alert_status_code] [dbo].[lookupcode] NOT NULL,	[friendly_name] [dbo].[friendlyname] NOT NULL,	[is_active] [bit] NOT NULL,	[created_by] [nvarchar](50) NULL,	[created_date] [datetime] NOT NULL,	[modified_by] [nvarchar](50) NULL,	[modified_date] [datetime] NOT NULL, CONSTRAINT [AlertStatuses_PK] PRIMARY KEY CLUSTERED (	[alert_status_code] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the active status of a given record (1 = active, 0 = inactive).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses', @level2type=N'COLUMN',@level2name=N'is_active'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that created a  given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses', @level2type=N'COLUMN',@level2name=N'created_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was first created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses', @level2type=N'COLUMN',@level2name=N'created_date'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that last modified a given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses', @level2type=N'COLUMN',@level2name=N'modified_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was last modified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses', @level2type=N'COLUMN',@level2name=N'modified_date'GOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.AlertStatus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'AlertStatus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'AlertStatus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'AlertStatus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertStatusDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to manage the stages of an alert before, during, and after execution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'AlertStatuses' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'AlertStatus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alert Statuses' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertStatuses'GOALTER TABLE [dbo].[AlertStatuses] ADD  CONSTRAINT [DF__AlertStat__is_ac__5689C04F]  DEFAULT ((1)) FOR [is_active]GOALTER TABLE [dbo].[AlertStatuses] ADD  CONSTRAINT [DF__AlertStat__creat__577DE488]  DEFAULT (getdate()) FOR [created_date]GOALTER TABLE [dbo].[AlertStatuses] ADD  CONSTRAINT [DF__AlertStat__modif__587208C1]  DEFAULT (getdate()) FOR [modified_date]GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table AlertTypes
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertType__is_ac__59662CFA]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertTypes] DROP CONSTRAINT [DF__AlertType__is_ac__59662CFA]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertType__creat__5A5A5133]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertTypes] DROP CONSTRAINT [DF__AlertType__creat__5A5A5133]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertType__modif__5B4E756C]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertTypes] DROP CONSTRAINT [DF__AlertType__modif__5B4E756C]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertTypes]    Script Date: 07/20/2009 14:58:21 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertTypes]') AND type in (N'U'))DROP TABLE [dbo].[AlertTypes]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertTypes]    Script Date: 07/20/2009 14:58:21 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[AlertTypes](	[alert_type_code] [dbo].[lookupcode] NOT NULL,	[friendly_name] [dbo].[friendlyname] NOT NULL,	[description] [nvarchar](500) NULL,	[cfc_name] [varchar](max) NULL,	[ui_priority] [friendlyname] NOT NULL,
	[ui_category] [friendlyname] NOT NULL,
	[is_active] [bit] NOT NULL,	[created_by] [nvarchar](50) NULL,	[created_date] [datetime] NOT NULL,	[modified_by] [nvarchar](50) NULL,	[modified_date] [datetime] NOT NULL, CONSTRAINT [AlertTypes_PK] PRIMARY KEY CLUSTERED (	[alert_type_code] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the active status of a given record (1 = active, 0 = inactive).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes', @level2type=N'COLUMN',@level2name=N'is_active'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that created a  given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes', @level2type=N'COLUMN',@level2name=N'created_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was first created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes', @level2type=N'COLUMN',@level2name=N'created_date'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that last modified a given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes', @level2type=N'COLUMN',@level2name=N'modified_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was last modified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes', @level2type=N'COLUMN',@level2name=N'modified_date'GOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.AlertType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'AlertType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'AlertType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'AlertType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertTypeDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to manage the types of alert notifications that the system can act on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'AlertTypes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'AlertType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alert Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes'GOALTER TABLE [dbo].[AlertTypes] ADD  CONSTRAINT [DF__AlertType__is_ac__59662CFA]  DEFAULT ((1)) FOR [is_active]GOALTER TABLE [dbo].[AlertTypes] ADD  CONSTRAINT [DF__AlertType__creat__5A5A5133]  DEFAULT (getdate()) FOR [created_date]GOALTER TABLE [dbo].[AlertTypes] ADD  CONSTRAINT [DF__AlertType__modif__5B4E756C]  DEFAULT (getdate()) FOR [modified_date]GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table Alerts
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Alerts_AlertStatuses]') AND parent_object_id = OBJECT_ID(N'[dbo].[Alerts]'))ALTER TABLE [dbo].[Alerts] DROP CONSTRAINT [FK_Alerts_AlertStatuses]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Alerts_AlertTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Alerts]'))ALTER TABLE [dbo].[Alerts] DROP CONSTRAINT [FK_Alerts_AlertTypes]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Alerts_ClientCompanies]') AND parent_object_id = OBJECT_ID(N'[dbo].[Alerts]'))ALTER TABLE [dbo].[Alerts] DROP CONSTRAINT [FK_Alerts_ClientCompanies]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Alerts_Properties]') AND parent_object_id = OBJECT_ID(N'[dbo].[Alerts]'))ALTER TABLE [dbo].[Alerts] DROP CONSTRAINT [FK_Alerts_Properties]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertQueu__is_ac__4DF47A4E]') AND type = 'D')BEGINALTER TABLE [dbo].[Alerts] DROP CONSTRAINT [DF__AlertQueu__is_ac__4DF47A4E]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertQueu__creat__4EE89E87]') AND type = 'D')BEGINALTER TABLE [dbo].[Alerts] DROP CONSTRAINT [DF__AlertQueu__creat__4EE89E87]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertQueu__modif__4FDCC2C0]') AND type = 'D')BEGINALTER TABLE [dbo].[Alerts] DROP CONSTRAINT [DF__AlertQueu__modif__4FDCC2C0]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[Alerts]    Script Date: 07/20/2009 14:56:18 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Alerts]') AND type in (N'U'))DROP TABLE [dbo].[Alerts]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[Alerts]    Script Date: 07/20/2009 14:56:18 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[Alerts](	[alert_uid] [uniqueidentifier] NOT NULL,	[client_company_uid] [uniqueidentifier] NOT NULL,	[property_uid] [uniqueidentifier] NOT NULL,	[alert_type_code] [dbo].[lookupcode] NOT NULL,	[alert_status_code] [dbo].[lookupcode] NOT NULL,	[recipient_address] [varchar](max) NOT NULL,	[alert_content] [varchar](max) NOT NULL,	[alert_generated_date] [datetime] NOT NULL,	[alert_due_date] [datetime] NULL,	[alert_visibility_date] [datetime] NOT NULL,	[is_active] [bit] NOT NULL,	[created_by] [nvarchar](50) NULL,	[created_date] [datetime] NOT NULL,	[modified_by] [nvarchar](50) NULL,	[modified_date] [datetime] NOT NULL, CONSTRAINT [AlertQueueEntries_PK] PRIMARY KEY CLUSTERED (	[alert_uid] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the active status of a given record (1 = active, 0 = inactive).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts', @level2type=N'COLUMN',@level2name=N'is_active'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that created a  given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts', @level2type=N'COLUMN',@level2name=N'created_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was first created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts', @level2type=N'COLUMN',@level2name=N'created_date'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that last modified a given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts', @level2type=N'COLUMN',@level2name=N'modified_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was last modified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts', @level2type=N'COLUMN',@level2name=N'modified_date'GOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.Alert' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'Alert' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'Alert' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'Alert' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to manage the alerts that are generated by alert subscriptions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'Alerts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'Alert' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alerts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alerts'GOALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_AlertStatuses] FOREIGN KEY([alert_status_code])REFERENCES [dbo].[AlertStatuses] ([alert_status_code])GOALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_AlertStatuses]GOALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_AlertTypes] FOREIGN KEY([alert_type_code])REFERENCES [dbo].[AlertTypes] ([alert_type_code])GOALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_AlertTypes]GOALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_ClientCompanies] FOREIGN KEY([client_company_uid])REFERENCES [dbo].[ClientCompanies] ([client_company_uid])GOALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_ClientCompanies]GOALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_Properties] FOREIGN KEY([property_uid])REFERENCES [dbo].[Properties] ([property_uid])GOALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_Properties]GOALTER TABLE [dbo].[Alerts] ADD  CONSTRAINT [DF__AlertQueu__is_ac__4DF47A4E]  DEFAULT ((1)) FOR [is_active]GOALTER TABLE [dbo].[Alerts] ADD  CONSTRAINT [DF__AlertQueu__creat__4EE89E87]  DEFAULT (getdate()) FOR [created_date]GOALTER TABLE [dbo].[Alerts] ADD  CONSTRAINT [DF__AlertQueu__modif__4FDCC2C0]  DEFAULT (getdate()) FOR [modified_date]GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table AlertSubscriptions
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AlertSubscriptions_AlertTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertSubscriptions]'))ALTER TABLE [dbo].[AlertSubscriptions] DROP CONSTRAINT [FK_AlertSubscriptions_AlertTypes]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AlertSubscriptions_ClientCompanies]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertSubscriptions]'))ALTER TABLE [dbo].[AlertSubscriptions] DROP CONSTRAINT [FK_AlertSubscriptions_ClientCompanies]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AlertSubscriptions_Properties]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertSubscriptions]'))ALTER TABLE [dbo].[AlertSubscriptions] DROP CONSTRAINT [FK_AlertSubscriptions_Properties]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AlertSubscriptions_alert_subscription_uid]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertSubscriptions] DROP CONSTRAINT [DF_AlertSubscriptions_alert_subscription_uid]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Alerts__is_activ__06C2E356]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertSubscriptions] DROP CONSTRAINT [DF__Alerts__is_activ__06C2E356]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Alerts__created___07B7078F]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertSubscriptions] DROP CONSTRAINT [DF__Alerts__created___07B7078F]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Alerts__modified__08AB2BC8]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertSubscriptions] DROP CONSTRAINT [DF__Alerts__modified__08AB2BC8]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertSubscriptions]    Script Date: 07/20/2009 14:57:28 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertSubscriptions]') AND type in (N'U'))DROP TABLE [dbo].[AlertSubscriptions]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertSubscriptions]    Script Date: 07/20/2009 14:57:28 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[AlertSubscriptions](	[alert_subscription_uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,	[client_company_uid] [uniqueidentifier] NOT NULL,	[property_uid] [uniqueidentifier] NULL,	[alert_type_code] [dbo].[lookupcode] NOT NULL,	[friendly_name] [dbo].[friendlyname] NOT NULL,	[is_active] [bit] NOT NULL,	[created_by] [nvarchar](50) NULL,	[created_date] [datetime] NOT NULL,	[modified_by] [nvarchar](50) NULL,	[modified_date] [datetime] NOT NULL, CONSTRAINT [PK_AlertSubscriptions] PRIMARY KEY CLUSTERED (	[alert_subscription_uid] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the primary key of the property associated to a given alert.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'property_uid'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the type code / internal identifier for a given alert.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'alert_type_code'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the external / customer facing name of a given alert.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'friendly_name'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the active status of a given record (1 = active, 0 = inactive).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'is_active'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that created a  given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'created_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was first created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'created_date'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that last modified a given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'modified_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was last modified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions', @level2type=N'COLUMN',@level2name=N'modified_date'GOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.AlertSubscription' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'AlertSubscription' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'AlertSubscription' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'AlertSubscription' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertSubscriptionDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to register the client to alerts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'AlertSubscriptions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'AlertSubscription' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alert Subscriptions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert Subscription' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions'GOALTER TABLE [dbo].[AlertSubscriptions]  WITH CHECK ADD  CONSTRAINT [FK_AlertSubscriptions_AlertTypes] FOREIGN KEY([alert_type_code])REFERENCES [dbo].[AlertTypes] ([alert_type_code])GOALTER TABLE [dbo].[AlertSubscriptions] CHECK CONSTRAINT [FK_AlertSubscriptions_AlertTypes]GOALTER TABLE [dbo].[AlertSubscriptions]  WITH CHECK ADD  CONSTRAINT [FK_AlertSubscriptions_ClientCompanies] FOREIGN KEY([client_company_uid])REFERENCES [dbo].[ClientCompanies] ([client_company_uid])GOALTER TABLE [dbo].[AlertSubscriptions] CHECK CONSTRAINT [FK_AlertSubscriptions_ClientCompanies]GOALTER TABLE [dbo].[AlertSubscriptions]  WITH CHECK ADD  CONSTRAINT [FK_AlertSubscriptions_Properties] FOREIGN KEY([property_uid])REFERENCES [dbo].[Properties] ([property_uid])GOALTER TABLE [dbo].[AlertSubscriptions] CHECK CONSTRAINT [FK_AlertSubscriptions_Properties]GOALTER TABLE [dbo].[AlertSubscriptions] ADD  CONSTRAINT [DF_AlertSubscriptions_alert_subscription_uid]  DEFAULT (newid()) FOR [alert_subscription_uid]GOALTER TABLE [dbo].[AlertSubscriptions] ADD  CONSTRAINT [DF__Alerts__is_activ__06C2E356]  DEFAULT ((1)) FOR [is_active]GOALTER TABLE [dbo].[AlertSubscriptions] ADD  CONSTRAINT [DF__Alerts__created___07B7078F]  DEFAULT (getdate()) FOR [created_date]GOALTER TABLE [dbo].[AlertSubscriptions] ADD  CONSTRAINT [DF__Alerts__modified__08AB2BC8]  DEFAULT (getdate()) FOR [modified_date]GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table AlertSubscriptions_AlertDeliveryMethods
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AlertSubscriptions_AlertDeliveryMethods_AlertDeliveryMethods]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertSubscriptions_AlertDeliveryMethods]'))ALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods] DROP CONSTRAINT [FK_AlertSubscriptions_AlertDeliveryMethods_AlertDeliveryMethods]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AlertSubscriptions_AlertDeliveryMethods_AlertSubscriptions]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertSubscriptions_AlertDeliveryMethods]'))ALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods] DROP CONSTRAINT [FK_AlertSubscriptions_AlertDeliveryMethods_AlertSubscriptions]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AlertSubscriptions_AlertDeliveryMethods_relationship_id]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods] DROP CONSTRAINT [DF_AlertSubscriptions_AlertDeliveryMethods_relationship_id]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertSubscriptions_AlertDeliveryMethods]    Script Date: 07/20/2009 14:57:53 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertSubscriptions_AlertDeliveryMethods]') AND type in (N'U'))DROP TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertSubscriptions_AlertDeliveryMethods]    Script Date: 07/20/2009 14:57:53 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods](	[relationship_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,	[alert_subscription_uid] [uniqueidentifier] NOT NULL,	[alert_delivery_method_code] [dbo].[lookupcode] NOT NULL, CONSTRAINT [PK_AlertSubscriptions_AlertDeliveryMethods] PRIMARY KEY CLUSTERED (	[relationship_id] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.AlertSubscription_AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'AlertSubscription_AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'AlertSubscription_AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'AlertSubscription_AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertSubscription_AlertDeliveryMethodDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to manage the relationship between client alert subscriptions and what electronic means it will be sent as' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'AlertSubscriptions_AlertDeliveryMethods' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'AlertSubscription_AlertDeliveryMethod' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alert Subscriptions to Alert Delivery Methods' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert Subscription to Alert Delivery Method' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertSubscriptions_AlertDeliveryMethods'GOALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods]  WITH CHECK ADD  CONSTRAINT [FK_AlertSubscriptions_AlertDeliveryMethods_AlertDeliveryMethods] FOREIGN KEY([alert_delivery_method_code])REFERENCES [dbo].[AlertDeliveryMethods] ([alert_delivery_method_code])GOALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods] CHECK CONSTRAINT [FK_AlertSubscriptions_AlertDeliveryMethods_AlertDeliveryMethods]GOALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods]  WITH CHECK ADD  CONSTRAINT [FK_AlertSubscriptions_AlertDeliveryMethods_AlertSubscriptions] FOREIGN KEY([alert_subscription_uid])REFERENCES [dbo].[AlertSubscriptions] ([alert_subscription_uid])GOALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods] CHECK CONSTRAINT [FK_AlertSubscriptions_AlertDeliveryMethods_AlertSubscriptions]GOALTER TABLE [dbo].[AlertSubscriptions_AlertDeliveryMethods] ADD  CONSTRAINT [DF_AlertSubscriptions_AlertDeliveryMethods_relationship_id]  DEFAULT (newid()) FOR [relationship_id]GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Table AlertTypes_AlertIntervals
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE [MCEnergyDBV2]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AlertTypes_AlertIntervals_AlertIntervals]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertTypes_AlertIntervals]'))ALTER TABLE [dbo].[AlertTypes_AlertIntervals] DROP CONSTRAINT [FK_AlertTypes_AlertIntervals_AlertIntervals]GOIF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AlertTypes_AlertIntervals_AlertTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertTypes_AlertIntervals]'))ALTER TABLE [dbo].[AlertTypes_AlertIntervals] DROP CONSTRAINT [FK_AlertTypes_AlertIntervals_AlertTypes]GOIF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AlertTypes_AlertQueues_StartBeforeEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[AlertTypes_AlertIntervals]'))ALTER TABLE [dbo].[AlertTypes_AlertIntervals] DROP CONSTRAINT [CK_AlertTypes_AlertQueues_StartBeforeEnd]GOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AlertTypes_AlertIntervals_relationship_id]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertTypes_AlertIntervals] DROP CONSTRAINT [DF_AlertTypes_AlertIntervals_relationship_id]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertType__is_ac__5C4299A5]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertTypes_AlertIntervals] DROP CONSTRAINT [DF__AlertType__is_ac__5C4299A5]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertType__creat__5D36BDDE]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertTypes_AlertIntervals] DROP CONSTRAINT [DF__AlertType__creat__5D36BDDE]ENDGOIF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AlertType__modif__5E2AE217]') AND type = 'D')BEGINALTER TABLE [dbo].[AlertTypes_AlertIntervals] DROP CONSTRAINT [DF__AlertType__modif__5E2AE217]ENDGOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertTypes_AlertIntervals]    Script Date: 07/20/2009 14:58:43 ******/IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertTypes_AlertIntervals]') AND type in (N'U'))DROP TABLE [dbo].[AlertTypes_AlertIntervals]GOUSE [MCEnergyDBV2]GO/****** Object:  Table [dbo].[AlertTypes_AlertIntervals]    Script Date: 07/20/2009 14:58:43 ******/SET ANSI_NULLS ONGOSET QUOTED_IDENTIFIER ONGOSET ANSI_PADDING ONGOCREATE TABLE [dbo].[AlertTypes_AlertIntervals](	[relationship_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,	[alert_type_code] [dbo].[lookupcode] NOT NULL,	[alert_interval_code] [dbo].[lookupcode] NOT NULL,	[is_sending_enabled] [bit] NOT NULL,	[relationship_start] [datetime] NOT NULL,	[relationship_end] [datetime] NULL,	[is_active] [bit] NOT NULL,	[created_by] [nvarchar](50) NULL,	[created_date] [datetime] NOT NULL,	[modified_by] [nvarchar](50) NULL,	[modified_date] [datetime] NOT NULL, CONSTRAINT [PK_AlertTypes_AlertIntervals] PRIMARY KEY CLUSTERED (	[relationship_id] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]GOSET ANSI_PADDING OFFGOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the active status of a given record (1 = active, 0 = inactive).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals', @level2type=N'COLUMN',@level2name=N'is_active'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that created a  given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals', @level2type=N'COLUMN',@level2name=N'created_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was first created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals', @level2type=N'COLUMN',@level2name=N'created_date'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the system user that last modified a given record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals', @level2type=N'COLUMN',@level2name=N'modified_by'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes the date that a given record was last modified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals', @level2type=N'COLUMN',@level2name=N'modified_date'GOEXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=N'mce.e2.alerting.vo.AlertType_AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=N'AlertType_AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'cfcName', @value=N'AlertType_AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'componentName', @value=N'AlertType_AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=N'AlertType_AlertIntervalDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table is used to manage the relationship mapping alert types to alert intervals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'pluralName', @value=N'AlertTypes_AlertIntervals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'singularName', @value=N'AlertType_AlertInterval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=N'Alert Types to Alert Intervals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOEXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=N'Alert Type to Alert Interval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AlertTypes_AlertIntervals'GOALTER TABLE [dbo].[AlertTypes_AlertIntervals]  WITH CHECK ADD  CONSTRAINT [FK_AlertTypes_AlertIntervals_AlertIntervals] FOREIGN KEY([alert_interval_code])REFERENCES [dbo].[AlertIntervals] ([alert_interval_code])GOALTER TABLE [dbo].[AlertTypes_AlertIntervals] CHECK CONSTRAINT [FK_AlertTypes_AlertIntervals_AlertIntervals]GOALTER TABLE [dbo].[AlertTypes_AlertIntervals]  WITH CHECK ADD  CONSTRAINT [FK_AlertTypes_AlertIntervals_AlertTypes] FOREIGN KEY([alert_type_code])REFERENCES [dbo].[AlertTypes] ([alert_type_code])GOALTER TABLE [dbo].[AlertTypes_AlertIntervals] CHECK CONSTRAINT [FK_AlertTypes_AlertIntervals_AlertTypes]GOALTER TABLE [dbo].[AlertTypes_AlertIntervals]  WITH CHECK ADD  CONSTRAINT [CK_AlertTypes_AlertQueues_StartBeforeEnd] CHECK  (([relationship_start]<[relationship_end]))GOALTER TABLE [dbo].[AlertTypes_AlertIntervals] CHECK CONSTRAINT [CK_AlertTypes_AlertQueues_StartBeforeEnd]GOALTER TABLE [dbo].[AlertTypes_AlertIntervals] ADD  CONSTRAINT [DF_AlertTypes_AlertIntervals_relationship_id]  DEFAULT (newid()) FOR [relationship_id]GOALTER TABLE [dbo].[AlertTypes_AlertIntervals] ADD  CONSTRAINT [DF__AlertType__is_ac__5C4299A5]  DEFAULT ((1)) FOR [is_active]GOALTER TABLE [dbo].[AlertTypes_AlertIntervals] ADD  CONSTRAINT [DF__AlertType__creat__5D36BDDE]  DEFAULT (getdate()) FOR [created_date]GOALTER TABLE [dbo].[AlertTypes_AlertIntervals] ADD  CONSTRAINT [DF__AlertType__modif__5E2AE217]  DEFAULT (getdate()) FOR [modified_date]GO






------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create Alert Extended Data
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data AlertDeliveryMethods
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertDeliveryMethods'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertDeliveryMethod'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the ways an alert can be delivered electronically'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertDeliveryMethods'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertDeliveryMethod'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Delivery Methods'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Delivery Method'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data AlertIntervals
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertIntervals'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertInterval'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the various frequencies possible to trigger alerts'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertIntervals'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertInterval'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Intervals'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Interval'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data Alerts
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'Alerts'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'Alert'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the alerts that are generated by alert subscriptions'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'Alerts'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'Alert'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alerts'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data AlertStatuses
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertStatuses'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertStatus'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the stages of an alert before, during, and after execution'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertStatuses'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertStatus'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Statuses'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Status'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data AlertSubscriptions_AlertDeliveryMethods
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertSubscriptions_AlertDeliveryMethods'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertSubscription_AlertDeliveryMethod'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the relationship between client alert subscriptions and what electronic means it will be sent as'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertSubscriptions_AlertDeliveryMethods'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertSubscription_AlertDeliveryMethod'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Subscriptions to Alert Delivery Methods'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Subscription to Alert Delivery Method'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data AlertSubscriptions
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertSubscriptions'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertSubscription'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to register the client to alerts'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertSubscriptions'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertSubscription'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Subscriptions'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Subscription'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data AlertIntervals
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertTypes_AlertIntervals'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertType_AlertInterval'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the relationship mapping alert types to alert intervals'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertTypes_AlertIntervals'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertTypes_AlertInterval'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Types to Alert Intervals'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Type to Alert Interval'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Extended Data AlertTypes
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertTypes'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertTypes'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the types of alert notifications that the system can act on'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertTypes'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertType'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Types'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Type'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName


