-- Describe local variables
declare @tableName nvarchar(500)
declare @cfcName nvarchar(500)
declare @xpValue nvarchar(500)

-- Define the name of the table for which the code generator will be used
-- Ex. "My_Table_Name"
set @tableName = N'AlertDeliveryMethods'

-- Define the base *.cfc name that will be used to render the vo / db delegate code
-- For this property, be sure to exclude any spaces and underscores from the table name; enforce camel case
-- Ex. "MyTableName"
set @cfcName = N'AlertDeliveryMethod'

-- Define the purpose of the table (will be added to *.cfc comment / code hint)
set @xpValue = N'This table is used to manage the ways an alert can be delivered electronically'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the *.cfc class path for the vo including the class name
set @xpValue = N'mce.e2.alerting.vo.' + @cfcName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanAlias' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanAlias', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanDisplayName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the extends property for the VO / db delegate (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanExtends', @value=N'BaseValueObject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the rendering style of the VO (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'beanStyle' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'beanStyle', @value=N'rpc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the base *.cfc name that will be rendered by the code generator
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'cfcName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'cfcName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'componentName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'componentName', @value=@cfcName , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate that will be created
set @xpValue = @cfcName + 'Delegate'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateDisplayName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateDisplayName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the name of the database delegate extends property (this will never change)
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'delegateExtends' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'delegateExtends', @value=N'BaseDatabaseDelegate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component plural name of the *.cfc being rendered; used to represent multiple components
-- ex. "MyTableNames"
set @xpValue = N'AlertDeliveryMethods'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'pluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'pluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component singular name of the *.cfc being rendered; used to represent one component
-- ex. "MyTableName"
set @xpValue = N'AlertDeliveryMethod'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'singularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'singularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose plural name of the *.cfc being rendered (regular English to represent multiple components in generated comments)
-- ex. "My Table Names"
set @xpValue = N'Alert Delivery Methods'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verbosePluralName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verbosePluralName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName

-- Define the component verbose singular name of the *.cfc being rendered (regular English to represent singular components in general comments)
-- ex. "My Table Name"
set @xpValue = N'Alert Delivery Method'

IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'verboseSingularName' , N'SCHEMA',N'dbo', N'TABLE',@tableName, NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'verboseSingularName', @value=@xpValue , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=@tableName
