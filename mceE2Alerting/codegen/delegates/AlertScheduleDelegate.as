package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertScheduleService.
	 */
	public class AlertScheduleDelegate extends BaseServiceDelegate
	{

		/** The alertScheduleService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertScheduleService")]
		public var alertScheduleService : RemoteObject;

		/** This method is used to check the Alert Intervals and poll for alerts that need to be sent.
		*/
		public function pollAlerts() : AsyncToken {
			return alertScheduleService.pollAlerts();
		}

		/** This method is used to send pending alerts.
		 *  @param alert_digest_code
		 *  @returns alert_digest_code:String A alert_digest_code:String value object populated with the properties used to modify the specified alert_digest_code:string instance.
		*/
		public function sendAlerts(alert_digest_code:String) : AsyncToken {
			return alertScheduleService.sendAlerts(alert_digest_code);
		}

	}
}
