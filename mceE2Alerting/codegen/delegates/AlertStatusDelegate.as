package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/** The AlertStatus object is used to define an instance of a given alertstatus (value object). */
	import vo.AlertStatus;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertStatusService.
	 */
	public class AlertStatusDelegate extends BaseServiceDelegate
	{

		/** The alertStatusService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertStatusService")]
		public var alertStatusService : RemoteObject;

		/** This method is used to set the database delegate used to manage Alert Status data (dependency injection).
		 *  @param bean Describes the *.cfc used to manage database interactions related to Alert Status information.
		 *  @returns bean:AlertStatusDelegate A bean:AlertStatusDelegate value object populated with the properties used to modify the specified bean:alertstatusdelegate instance.
		*/
		public function setAlertStatusDelegate(bean:AlertStatusDelegate) : AsyncToken {
			return alertStatusService.setAlertStatusDelegate(bean);
		}

		/** This method is used to retrieve / return an empty Alert Status information value object.
		*/
		public function getEmptyAlertStatusVo() : AsyncToken {
			return alertStatusService.getEmptyAlertStatusVo();
		}

		/** This method is used to retrieve / return a collection of Alert Statuss.
		*/
		public function getAlertStatuses() : AsyncToken {
			return alertStatusService.getAlertStatuses();
		}

		/** This method is used to return a populated Alert Status vo (value object).
		 *  @param AlertStatus Describes the Alert Status VO object containing the properties of the Alert Status to be retrieved.
		 *  @returns alertStatus:AlertStatus A alertStatus:AlertStatus value object populated with the properties used to modify the specified alertstatus:alertstatus instance.
		*/
		public function getAlertStatus(alertStatus:AlertStatus) : AsyncToken {
			return alertStatusService.getAlertStatus(alertStatus);
		}

		/** This method is used to persist a new Alert Status record to the application database.
		 *  @param AlertStatus Describes the Alert Status VO object containing the details of the Alert Statusto be saved.
		 *  @returns alertStatus:AlertStatus A alertStatus:AlertStatus value object populated with the properties used to modify the specified alertstatus:alertstatus instance.
		*/
		public function saveNewAlertStatus(alertStatus:AlertStatus) : AsyncToken {
			return alertStatusService.saveNewAlertStatus(alertStatus);
		}

		/** This method is used to persist a new Alert Status record to the application database.
		 *  @param AlertStatus Describes the Alert Status VO object containing the details of the Alert Status to be saved and returned.
		 *  @returns alertStatus:AlertStatus A alertStatus:AlertStatus value object populated with the properties used to modify the specified alertstatus:alertstatus instance.
		*/
		public function saveAndReturnNewAlertStatus(alertStatus:AlertStatus) : AsyncToken {
			return alertStatusService.saveAndReturnNewAlertStatus(alertStatus);
		}

		/** This method is used to save an existing Alert Status information to the application database.
		 *  @param AlertStatus Describes the Alert Status VO object containing the details of the Alert Status to be modified.
		 *  @returns alertStatus:AlertStatus A alertStatus:AlertStatus value object populated with the properties used to modify the specified alertstatus:alertstatus instance.
		*/
		public function saveExistingAlertStatus(alertStatus:AlertStatus) : AsyncToken {
			return alertStatusService.saveExistingAlertStatus(alertStatus);
		}

	}
}
