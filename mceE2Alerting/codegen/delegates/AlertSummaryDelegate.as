package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/** The AlertSummary object is used to define an instance of a given alertsummary (value object). */
	import vo.AlertSummary;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertSummaryService.
	 */
	public class AlertSummaryDelegate extends BaseServiceDelegate
	{

		/** The alertSummaryService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertSummaryService")]
		public var alertSummaryService : RemoteObject;

		/** This method is used to set the database delegate used to manage AlertSummary data (dependency injection).
		 *  @param bean Describes the *.cfc used to manage database interactions related to Alert information.
		 *  @returns bean:AlertSummaryDelegate A bean:AlertSummaryDelegate value object populated with the properties used to modify the specified bean:alertsummarydelegate instance.
		*/
		public function setAlertSummaryDelegate(bean:AlertSummaryDelegate) : AsyncToken {
			return alertSummaryService.setAlertSummaryDelegate(bean);
		}

		/** This method is used to retrieve / return an empty Alert information value object.
		*/
		public function getEmptyAlertSummaryVo() : AsyncToken {
			return alertSummaryService.getEmptyAlertSummaryVo();
		}

		/** This method is used to retrieve / return a collection of AlertSummaries.
		 *  @param alert_uid
		 *  @param alert_subscription_uid
		 *  @param client_company_uid
		 *  @param property_uid
		 *  @param alert_type_code
		 *  @param alert_status_code
		 *  @returns alert_uid:String,alert_subscription_uid:String,client_company_uid:String,property_uid:String,alert_type_code:String,alert_status_code:String A alert_uid:String,alert_subscription_uid:String,client_company_uid:String,property_uid:String,alert_type_code:String,alert_status_code:String value object populated with the properties used to modify the specified alert_uid:string,alert_subscription_uid:string,client_company_uid:string,property_uid:string,alert_type_code:string,alert_status_code:string instance.
		*/
		public function getAlertSummaries(alert_uid:String,alert_subscription_uid:String,client_company_uid:String,property_uid:String,alert_type_code:String,alert_status_code:String) : AsyncToken {
			return alertSummaryService.getAlertSummaries(alert_uid,alert_subscription_uid,client_company_uid,property_uid,alert_type_code,alert_status_code);
		}

		/** This method is used to return a populated AlertSummary vo (value object).
		 *  @param AlertSummary Describes the AlertSummary VO object containing the properties of the Alert to be retrieved.
		 *  @returns alertSummary:AlertSummary A alertSummary:AlertSummary value object populated with the properties used to modify the specified alertsummary:alertsummary instance.
		*/
		public function getAlertSummary(alertSummary:AlertSummary) : AsyncToken {
			return alertSummaryService.getAlertSummary(alertSummary);
		}

	}
}
