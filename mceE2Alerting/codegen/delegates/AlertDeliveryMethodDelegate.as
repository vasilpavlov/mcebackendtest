package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/** The AlertDeliveryMethod object is used to define an instance of a given alertdeliverymethod (value object). */
	import vo.AlertDeliveryMethod;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertDeliveryMethodService.
	 */
	public class AlertDeliveryMethodDelegate extends BaseServiceDelegate
	{

		/** The alertDeliveryMethodService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertDeliveryMethodService")]
		public var alertDeliveryMethodService : RemoteObject;

		/** This method is used to set the database delegate used to manage Alert Delivery Method data (dependency injection).
		 *  @param bean Describes the *.cfc used to manage database interactions related to Alert Delivery Method information.
		 *  @returns bean:AlertDeliveryMethodDelegate A bean:AlertDeliveryMethodDelegate value object populated with the properties used to modify the specified bean:alertdeliverymethoddelegate instance.
		*/
		public function setAlertDeliveryMethodDelegate(bean:AlertDeliveryMethodDelegate) : AsyncToken {
			return alertDeliveryMethodService.setAlertDeliveryMethodDelegate(bean);
		}

		/** This method is used to retrieve / return an empty Alert Delivery Method information value object.
		*/
		public function getEmptyAlertDeliveryMethodVo() : AsyncToken {
			return alertDeliveryMethodService.getEmptyAlertDeliveryMethodVo();
		}

		/** This method is used to retrieve / return a collection of Alert Delivery Methods.
		*/
		public function getAlertDeliveryMethods() : AsyncToken {
			return alertDeliveryMethodService.getAlertDeliveryMethods();
		}

		/** This method is used to return a populated Alert Delivery Method vo (value object).
		 *  @param AlertDeliveryMethod Describes the Alert Delivery Method VO object containing the properties of the Alert DeliveryMethod to be retrieved.
		 *  @returns alertDeliveryMethod:AlertDeliveryMethod A alertDeliveryMethod:AlertDeliveryMethod value object populated with the properties used to modify the specified alertdeliverymethod:alertdeliverymethod instance.
		*/
		public function getAlertDeliveryMethod(alertDeliveryMethod:AlertDeliveryMethod) : AsyncToken {
			return alertDeliveryMethodService.getAlertDeliveryMethod(alertDeliveryMethod);
		}

		/** This method is used to persist a new Alert Delivery Method record to the application database.
		 *  @param AlertDeliveryMethod Describes the Alert Delivery Method VO object containing the details of the Alert Delivery Methodto be saved.
		 *  @returns alertDeliveryMethod:AlertDeliveryMethod A alertDeliveryMethod:AlertDeliveryMethod value object populated with the properties used to modify the specified alertdeliverymethod:alertdeliverymethod instance.
		*/
		public function saveNewAlertDeliveryMethod(alertDeliveryMethod:AlertDeliveryMethod) : AsyncToken {
			return alertDeliveryMethodService.saveNewAlertDeliveryMethod(alertDeliveryMethod);
		}

		/** This method is used to persist a new Alert Delivery Method record to the application database.
		 *  @param AlertDeliveryMethod Describes the Alert Delivery Method VO object containing the details of the Alert Delivery Method to be saved and returned.
		 *  @returns alertDeliveryMethod:AlertDeliveryMethod A alertDeliveryMethod:AlertDeliveryMethod value object populated with the properties used to modify the specified alertdeliverymethod:alertdeliverymethod instance.
		*/
		public function saveAndReturnNewAlertDeliveryMethod(alertDeliveryMethod:AlertDeliveryMethod) : AsyncToken {
			return alertDeliveryMethodService.saveAndReturnNewAlertDeliveryMethod(alertDeliveryMethod);
		}

		/** This method is used to save an existing Alert Delivery Method information to the application database.
		 *  @param AlertDeliveryMethod Describes the Alert Delivery Method VO object containing the details of the Alert Delivery Method to be modified.
		 *  @returns alertDeliveryMethod:AlertDeliveryMethod A alertDeliveryMethod:AlertDeliveryMethod value object populated with the properties used to modify the specified alertdeliverymethod:alertdeliverymethod instance.
		*/
		public function saveExistingAlertDeliveryMethod(alertDeliveryMethod:AlertDeliveryMethod) : AsyncToken {
			return alertDeliveryMethodService.saveExistingAlertDeliveryMethod(alertDeliveryMethod);
		}

	}
}
