﻿package vo
{
	/**
	* mponent output
	*/

	[Bindable]
	[RemoteClass(alias="mce.e2.alerting.vo.AlertSubscriptionsConditions")]
	public class AlertSubscriptionsConditions extends BaseValueObject
	{

		public var relationship_id:String;

		public var alert_subscription_uid:String;

		public var alert_condition_code:String;

		public var condition_value:String;

		/** 	 Define the meta-data for a given value object. */

		/** Describes the primary key for the current object / table. */
		public var primaryKey:String;

		/** Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)." */
		public var isAssociated:Boolean;

		/** 	 Default any required properties. */

	}

}
