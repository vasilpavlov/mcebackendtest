﻿		/**  */

package vo
{
	/**
	* This CFC sumarizes all information for an Alert, and is used to summarize data for read-only access that appears in multiple tables
	*/

	[Bindable]
	[RemoteClass(alias="mce.e2.alerting.vo.AlertSummary")]
	public class AlertSummary extends BaseValueObject
	{

		/** 	 Declare and initialize the properties for a given component. */

		public var alert_uid:String;

		public var alert_subscription_uid:String;

		public var client_company_uid:String;

		public var property_uid:String;

		public var alert_type_code:String;

		public var alert_status_code:String;

		public var recipient_address:String;

		public var alert_content:String;

		public var alert_due_date:Date;

		public var alert_visibility_date:Date;

		/** Describes the active status of a given record (1 = active, 0 = inactive)." */
		public var is_active:Boolean;

		/** Describes the system user that created a  given record." */
		public var created_by:String;

		/** Describes the date that a given record was first created." */
		public var created_date:Date;

		/** Describes the system user that last modified a given record." */
		public var modified_by:String;

		/** Describes the date that a given record was last modified." */
		public var modified_date:Date;

		/** Name of the client company" */
		public var client_company_friendly_name:String;

		/** Name of the property" */
		public var property_friendly_name:String;

		/** Name of the alert type" */
		public var alert_type_friendly_name:String;

		/** Name of the alert status" */
		public var alert_status_friendly_name:String;

		/** 	 Define the meta-data for a given value object. */

		/** Describes the primary key for the current object / table. */
		public var primaryKey:String;

		/** Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)." */
		public var isAssociated:Boolean;

		public var alertMeta:Array;

		/** 	 Default any required properties. */

	}

}
