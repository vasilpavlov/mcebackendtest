﻿		/**  */

package vo
{
	/**
	* This CFC manages all object/bean/value object instances for the AlertStatuses table, and is used to pass object instances through the applicaiton service layer
	*/

	[Bindable]
	[RemoteClass(alias="mce.e2.alerting.vo.AlertStatus")]
	public class AlertStatus extends BaseValueObject
	{

		/** 	 Declare and initialize the properties for a given component. */

		public var alert_status_code:String;

		public var friendly_name:String;

		/** Describes the active status of a given record (1 = active, 0 = inactive)." */
		public var is_active:Boolean;

		/** Describes the system user that created a  given record." */
		public var created_by:String;

		/** Describes the date that a given record was first created." */
		public var created_date:Date;

		/** Describes the system user that last modified a given record." */
		public var modified_by:String;

		/** Describes the date that a given record was last modified." */
		public var modified_date:Date;

		/** 	 Define the meta-data for a given value object. */

		/** Describes the primary key for the current object / table. */
		public var primaryKey:String;

		/** Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)." */
		public var isAssociated:Boolean;

		/** 	 Default any required properties. */

	}

}
