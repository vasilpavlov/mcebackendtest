﻿package vo
{
	/**
	* This component is the base value component for all value objects
	*/

	[Bindable]
	[RemoteClass(alias="mce.e2.vo.BaseValueObject")]
	public class BaseValueObject extends BaseValueObject
	{

		/** 	 Define any methods leveraged by the value objects */

		/** 		 Create a new instance of this value object class, using simple runtime introspection */

		/** 		 For each column in the specified row of the query */

		/** 		 Return the new instance */

		/** 		 Define the arguments for this method */

		/** 		 Initialize the local scope */

		/** 		 Initialize the output variable */

		/** 		 Loop over the properties in the present component */

		/** 			 Was the property found in the target object? */

		/** 				 If not, set the output value and exit the function */

		/** 		 Return the output variable */

	}

}
