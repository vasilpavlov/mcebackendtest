﻿		/**  */

package vo
{
	/**
	* This CFC manages all object/bean/value object instances for the AlertSubscriptions table, and is used to pass object instances through the applicaiton service layer
	*/

	[Bindable]
	[RemoteClass(alias="mce.e2.alerting.vo.AlertSubscription")]
	public class AlertSubscription extends BaseValueObject
	{

		/** 	 Declare and initialize the properties for a given component. */

		public var alert_subscription_uid:String;

		/** Describes the type code / internal identifier for a given alert." */
		public var alert_type_code:String;

		/** Describes the external / customer facing name of a given alert." */
		public var friendly_name:String;

		/** Describes the type code / internal identifier for the associated emissions profile lookup code." */
		public var profile_lookup_code:String;

		/** Describes the active status of a given record (1 = active, 0 = inactive)." */
		public var is_active:Boolean;

		/** The last time the subscription was processed by the alerting system." */
		public var last_polled:Date;

		/** Describes the system user that created a  given record." */
		public var created_by:String;

		/** Describes the date that a given record was first created." */
		public var created_date:Date;

		/** Describes the system user that last modified a given record." */
		public var modified_by:String;

		/** Describes the date that a given record was last modified." */
		public var modified_date:Date;

		public var alertTypeCfcName:String;

		public var users:Array;

		/** Describes the primary key of the property associated to a given alert." */
		public var accounts:Array;

		public var conditions:Array;

		/** 	 Define the meta-data for a given value object. */

		/** Describes the primary key for the current object / table. */
		public var primaryKey:String;

		/** Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)." */
		public var isAssociated:Boolean;

		/** 	 Default any required properties. */

	}

}
