<!--- Some utilities that we use to make it easier to write these sorts of tests --->
<cfset utils = CreateObject("component", "RateModelTestUtils")>

<!--- ALTER THESE AS YOU WISH --->
<!--- You can also pass them in on the URL --->
<cfparam name="model" type="string" default="mce.sample.simple-prorating">
<cfparam name="accountName" type="string" default="494134716900022 (Con Edison)">
<cfparam name="periodStart" type="date" default="11/17/2008">
<cfparam name="periodEnd" type="date" default="12/18/2008">


<!--- Assemble example set of "arguments" to pass to the engine --->
<cfinvoke
	component="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments"
		modelLookupCode="#model#"
		accountIdentifier="#utils.getEnergyAccountUid(accountName)#"
		periodStart="#periodStart#"
		periodEnd="#periodEnd#"
	method="init"
	returnVariable="requestArgs">

<!--- Run the engine based on the arguments --->
<cfinvoke
	method="runModel"
	component="com.mce.rate.RateModelRunner"
	requestArgs="#requestArgs#"
	returnVariable="response">


<!--- If the engine encounters a problem... --->
<cfif response.request.status neq "ok">
	<strong>The rate model engine encountered a problem:</strong><br>
	<cfoutput>#response.request.exception.message#<br>
	#response.request.exception.detail#</cfoutput>


<!--- If the engine was able to calculate successfully --->
<cfelse>
	<cfoutput>
		<cfdump var="#response.result.getOutput()#" label="Calculator output">
		Calculator run time: #Val(response.request.timeCalculationFinished.getTime() - response.request.timeCalculationStarted.getTime())# ms
	</cfoutput>
</cfif>