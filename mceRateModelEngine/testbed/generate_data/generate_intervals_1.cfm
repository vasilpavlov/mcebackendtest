<cfparam name="energyAccountUid" default="0e5cc250-6a46-4bc4-a5c8-5d0d608267d3">
<cfparam name="startDate" default="9/1/2008">
<cfparam name="endDate" default="09/30/2008">
<cfparam name="isSubmittingForm" default="false">

<h2>Interval Data Generator</h2>

<cfform method="post">
	<cfinput type="hidden" name="isSubmittingForm" value="true">

	<p>Energy Account UID:<br/>
	<cfinput type="text" name="energyAccountUid" value="#energyAccountUid#" size="40">
	<p>Start Date:<br/>
	<cfinput type="text" name="startDate" value="#startDate#">
	<p>End Date:<br/>
	<cfinput type="text" name="endDate" value="#endDate#">

	<p><input type="submit" value="Generate">
</cfform>


<cfif isSubmittingForm>
<h3>Generating Interval Data...</h3>

<cfinvoke
	component="com.mce.testing.IntervalDataGenerator"
	method="generateIntervalDataForEnergyAccount"
	energyAccountUid="#energyAccountUid#"
	startDate="#startDate#"
	endDate="#endDate#"
	deleteExistingIntervals="true"
	showOutput="true">

Done generating interval data.
</cfif>