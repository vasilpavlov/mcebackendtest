<cfcomponent extends="BaseDatabaseDelegate">
	
	<cffunction name="getAllProperties">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				property_uid,
				friendly_name as property_friendly_name
			FROM
				Properties p
			WHERE
				p.friendly_name IS NOT NULL
			ORDER BY
				p.friendly_name
		</cfquery>

		<cfreturn result> 
	</cffunction>

	<cffunction name="getAccountsForProperty">

		<cfargument name="propertyUid" type="string">
		<cfset var result = "">
		
		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				ea.energy_account_uid,
				native_account_number as energy_account_friendly_name
			FROM
				EnergyAccounts ea join EnergyNativeAccounts ena
				ON ea.energy_account_uid = ena.energy_account_uid
			WHERE
				ena.is_active = 1 and
				ea.property_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#propertyUid#"> 
			ORDER BY
				native_account_number
		</cfquery>

		<cfreturn result> 
	</cffunction>
	
	<cffunction name="getContractType" access="public" returntype="string">
		<cfargument name="accountUid" type="string">
		<cfargument name="asOfDate" type="date">

		<cfset var result = "">
		<!--- The only way to get the contract type is be meta_group name, not good enough, but works--->
		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				(CASE WHEN CHARINDEX('Retail',mg.friendly_name) > 0 THEN 'retailaccess'
				ELSE 'fullservice' END) as contract_type
			FROM EnergyNativeAccounts na
			LEFT OUTER JOIN MetaTypeGroups mg ON mg.meta_group_uid = na.meta_group_uid
			WHERE na.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#accountUid#">
				AND na.relationship_start <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#asOfDate#">
				AND ISNULL(na.relationship_end, '2100-1-1 0:00') >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#asOfDate#">
		</cfquery>

		<cfreturn result.contract_type>
	</cffunction>
	
	<cffunction name="getTaxesForAccount"
				access="public" returntype="array"
				hint="This method returns sales taxes for energy entry.">

		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">

		<cfset var result = ArrayNew(1)>
		
		<cfquery name="query1" datasource="#this.datasource#" result="tmpResult">
			SELECT * 
			FROM dbo.getTaxesForAccount(<cfqueryparam cfsqltype="cf_sql_idstamp" value="#energy_account_uid#">, <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_end#">)
		</cfquery>
		
		<cfif tmpResult.RecordCount neq 1>
			<cfthrow
				type="com.mcenergyinc.RateModel.NoTaxesData"
				errorcode="NoTaxesData"
				message="There are no taxes in the database!"
				detail="The reason is that the property doesn't have a postal_code or there are no tax factors associated with this postal_code!">
		</cfif>
		
		<cfset arrayAppend(result, query1.commodity_tax)>
		<cfset arrayAppend(result, query1.delivery_tax)>
		<cfset arrayAppend(result, query1.sales_tax)>		
		
		<cfreturn result>
	</cffunction>
	
	<!-- not using this one >
	<cffunction name="getAllAccounts">
	
		<cfset var result = "">			
		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				energy_account_uid,
				ea.friendly_name as energy_account_friendly_name,
				p.friendly_name as property_friendly_name
			FROM
				EnergyAccounts ea join Properties p on
					ea.property_uid = p.property_uid
			WHERE
				p.friendly_name IS NOT NULL
			ORDER BY
				p.friendly_name, ea.friendly_name
		</cfquery>

		<cfreturn result> 
	</cffunction>
	-->
</cfcomponent>