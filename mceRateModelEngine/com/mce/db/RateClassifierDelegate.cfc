<cfcomponent extends="BaseDatabaseDelegate">

	<cffunction name="getClassifierRecord" access="public" returntype="query">
		<cfargument name="lookupCode" type="string">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				implementation_file
			FROM
				IntervalClassifiers
			WHERE
				(classifier_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#lookupCode#">)
		</cfquery>

		<cfif result.recordCount neq 1>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="Can't find component name for lookup code '#lookupCode#'."
				detail="Expected 1 record, found #result.recordCount#.">
		</cfif>

		<cfreturn result>
	</cffunction>
	
</cfcomponent>