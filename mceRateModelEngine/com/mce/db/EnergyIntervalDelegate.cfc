<cfcomponent extends="BaseDatabaseDelegate">

	<cffunction name="getEnergyIntervals" access="public" returntype="query" maxrows="10">
		<cfargument name="energyAccountUid" type="string">
		<cfargument name="intervalStart" type="date">
		<cfargument name="intervalEnd" type="date">
		<cfargument name="factorLookupCodes" type="struct" required="false">

		<cfset var result = "">
		<cfset var classifierFunction = "dbo.classifyConEdPeakOffPeak">
		<cfset var code = "">

		<cfquery name="result" datasource="#this.datasource#">
		WITH IntervalValues(classification_code, interval_value, interval_start, interval_end, interval_length, factor_lookup)
		AS (
			SELECT
				classification_code,
				interval_value,
				interval_start,
				interval_end,
				DATEDIFF(minute, interval_start, interval_end) AS interval_length,
				<cfif isDefined("factorLookupCodes")>
					CASE #classifierFunction#(interval_start)
					<cfloop collection="#factorLookupCodes#" item="code">
						WHEN '#code#' THEN '#factorLookupCodes[code]#'
					</cfloop>
					END AS factor_lookup
				<cfelse>
					null as factor_lookup
				</cfif>
			FROM
				EnergyUsageIntervals
			WHERE
				energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyAccountUid#"> AND
				interval_start >= <cfqueryparam cfsqltype="cf_sql_date" value="#intervalStart#"> AND
				interval_end <= <cfqueryparam cfsqltype="cf_sql_date" value="#intervalEnd#"> AND
				is_active = 1)
			SELECT *,
			dbo.getRateFactorValue(factor_lookup, interval_start) * interval_value as factor_times_value
			FROM IntervalValues
			ORDER BY interval_start
		</cfquery>

		<cfreturn result>
	</cffunction>

</cfcomponent>