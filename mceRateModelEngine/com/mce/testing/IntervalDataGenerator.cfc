<cfcomponent output="false"
			 displayName="Interval Data Generator"
			 hint="This component is used to generate interval data for a given energy account.">

	<cffunction name="generateIntervalDataForEnergyAccount" returntype="void" access="public"
				hint="This method is used to generate interval data for a given energy account.">
		<cfargument name="energyAccountUid" type="string" required="true" hint="Describes the primary key / unique identifier for a given energy account.">
		<cfargument name="startDate" type="date" required="true" hint="Describes the start date that will be used to generate interval data.">
		<cfargument name="endDate" type="date" required="true" hint="Describes the end date that will be used to generate interval data.">
		<cfargument name="deleteExistingIntervals" type="boolean" required="true" hint="Describes whether existing interals should be deleted from the specified energy account.">
		<cfargument name="showOutput" type="boolean" required="true" hint="Describes whether debugging output should be displayed / generated while interval data is being generated.">
		<cfargument name="intervalLength" type="numeric" required="false" default="15" hint="Describes the interval length that will be used to generate data for the specified energy account.">
		<cfargument name="valueRangeMin" type="numeric" required="true" default="5" hint="Describes the interval data minimum value range.">
		<cfargument name="valueRangeMax" type="numeric" required="true" default="20" hint="Describes the interval data maximum value range.">
		<cfargument name="decimalPlaceMultiplier" type="numeric" required="false" default="100" hint="Describes the decimal place multiplier used to generate the interval data for the specified energy account.">
		<cfargument name="energyUnitLookupCode" type="string" required="false" default="Kwh" hint="Describes the energy unit lookup code that will be used to generate the interval data.">
		<cfargument name="dsn" type="string" required="false" default="mceRateModel" hint="Describes the datasource used to access the rate model data.">

		<!--- Local vars --->
		<cfset var numDays = DateDiff("d", startDate, endDate)>
		<cfset var i = 0>
		<cfset var j = 0>
		<cfset var getUnit = "">
		<cfset var thisDate = "">
		<cfset var thisStartTime = "">
		<cfset var thisEndTime = "">
		<cfset var thisValue = "">

		<!--- Get the UID for the energy unit --->
		<cfquery name="getUnit" datasource="#dsn#">
			SELECT energy_unit_uid FROM EnergyUnits
			WHERE unit_lookup_code = '#energyUnitLookupCode#'
		</cfquery>

		<cfif getUnit.recordCount neq 1>
			<cfabort showerror="Could not get the energy unit uid">
		</cfif>

		<!--- Delete existing intervals --->
		<cfif deleteExistingIntervals>
			<cfquery datasource="#dsn#">
				DELETE FROM EnergyUsageIntervals
				WHERE
					energy_account_uid = '#energyAccountUid#'
					AND energy_unit_uid = '#getUnit.energy_unit_uid#'
					AND interval_start BETWEEN #parseDateTime(startDate)# AND #parseDateTime(endDate)#
			</cfquery>
		</cfif>

		<!--- For each day... --->
		<cfloop from="0" to="#(numDays - 1)#" index="i">
			<cfif showOutput>
				<cfset thisDate = DateAdd("d", i, startDate)>
				<cfoutput>Generating intervals for #DateFormat(thisDate)#<br/></cfoutput><cfflush>
			</cfif>

			<!--- Write a big query that inserts a record for each interval in this day --->
			<cfquery datasource="#dsn#">
				<cfloop from="0" to="#((24*60) - intervalLength)#" step="#intervalLength#" index="j">
					<cfset thisStartTime = DateAdd("n", j, thisDate)>
					<cfset thisEndTime = DateAdd("l", -1, DateAdd("n", intervalLength, thisStartTime))>
					<cfset thisValue = RandRange(valueRangeMin * decimalPlaceMultiplier, valueRangeMax * decimalPlaceMultiplier) / decimalPlaceMultiplier>

					INSERT INTO [EnergyUsageIntervals]
					           ([energy_account_uid]
					           ,[energy_unit_uid]
					           ,[interval_start]
					           ,[interval_end]
					           ,[interval_value])
					     VALUES
					           ('#energyAccountUid#'
					           ,'#getUnit.energy_unit_uid#'
					           ,#thisStartTime#
					           ,#thisEndTime#
					           ,#thisValue#)
				</cfloop>
			</cfquery>
		</cfloop>
	</cffunction>

</cfcomponent>