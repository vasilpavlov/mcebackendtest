<cfcomponent output="false"
			 displayName="Query Utilities"
			 hint="This component is used to store any query utility methods that are leveraged by the rate model engine.">

	<cffunction name="queryLimitRows" access="public" returntype="query"
				hint="This method is used to limit the number of rows for a given query. It trims a large resultset based on the maxRows argument.">
		<cfargument name="sourceQuery" type="query" required="true" hint="Describes the query whose rows will be trimmed / reduced.">
		<cfargument name="maxRows" type="string" required="true" hint="Describes the total number of rows to be retrieved for the specified query.">
		<cfset var result = "">

		<cfquery name="result" dbtype="query" maxrows="#maxRows#">
			SELECT * FROM sourceQuery
		</cfquery>

		<cfreturn result>
	</cffunction>

	<cffunction name="querySum" access="public" returntype="numeric"
				hint="This method is used to retrieve the sum of a query column (similar to the sum function in Excel).">
		<cfargument name="query" type="query" required="true" hint="Describes the query that will be used to perform the sum calculation.">
		<cfargument name="sumColumn" type="string" required="true" hint="Describes the column name that the sum calculation will be performed against.">
		<cfargument name="criteria" type="string" required="false" hint="Describes the calculation criteria in SQL format that will be used to perform the sum calculation.">

		<cfset var temp = "">

		<cfif query.RecordCount eq 0>
			<cfreturn 0>
		</cfif>

		<cfquery dbtype="query" name="temp">
			SELECT SUM(#sumColumn#) AS ComputedSum
			FROM query
			<cfif isDefined("criteria")>
				WHERE #PreserveSingleQuotes(criteria)#
			</cfif>
		</cfquery>

		<cfreturn val(temp.ComputedSum)>
	</cffunction>

	<cffunction name="queryMax" access="public" returntype="numeric"
				hint="This method is used retrieve the maximum value for a given query column (similar to the max function in Excel).">
		<cfargument name="query" type="query" required="true" hint="Describes the query that will be used to perform the max calculation.">
		<cfargument name="maxCoumn" type="string" required="true" hint="Describes the column name that the max calculation will be performed against.">
		<cfargument name="criteria" type="string" required="false" hint="Describes the calculation criteria in SQL format that will be used to perform the max calculation.">

		<cfset var temp = "">

		<cfif query.RecordCount eq 0>
			<cfreturn 0>
		</cfif>

		<cfquery dbtype="query" name="temp">
			SELECT MAX(#maxColumn#) AS ComputedMax
			FROM query
			<cfif isDefined("criteria")>
				WHERE #PreserveSingleQuotes(criteria)#
			</cfif>
		</cfquery>

		<cfreturn val(temp.ComputedMax)>
	</cffunction>

	<cffunction name="queryToValueStruct" access="public" returntype="struct"
				hint="This method is used to convert a pair of query columns to a structure.">
		<cfargument name="query" type="query" required="true" hint="Describes the query whose columns will be converted to a structure.">
		<cfargument name="keyColumn" type="string" required="true" hint="Describes the query column that will be used to map structure keys.">
		<cfargument name="valueColumn" type="string" required="true" hint="Describes the query column that will be used to map the corresponding structure key values.">

		<cfset var result = StructNew()>

		<cfloop query="query">
			<cfset result[query[keyColumn][query.currentRow]] = query[valueColumn][query.currentRow]>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<cffunction name="queryToObjectStruct" access="public" returntype="struct"
				hint="This method is used to convert a pair of query columns to a structure.">
		<cfargument name="query" type="query" required="true" hint="Describes the query whose columns will be converted to a structure.">
		<cfargument name="keyColumn" type="string" required="true" hint="Describes the query column that will be used to map structure keys.">
		<cfargument name="valueColumns" type="string" required="true" hint="Describes the query columns (as a comma-separated list) that will be used to map the corresponding structure key values.">

		<cfset var result = StructNew()>
		<cfset var col = "">

		<cfloop list="#valueColumns#" index="col">
			<cfloop query="query">
				<cfset result[query[keyColumn][query.currentRow]][col] = query[col][query.currentRow]>
			</cfloop>
		</cfloop>

		<cfreturn result>
	</cffunction>

</cfcomponent>