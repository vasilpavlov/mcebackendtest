<cfcomponent>

	<!--- Date equivalent of "Min" --->
	<cffunction name="earlierOf" returntype="date" access="public">
		<cfargument name="d1" type="date" required="true">
		<cfargument name="d2" type="date" required="true">

		<cfif d1 lt d2>
			<cfreturn d1>
		<cfelse>
			<cfreturn d2>
		</cfif>
	</cffunction>

	<!---  Date equivalent of "Max" --->
	<cffunction name="laterOf" returntype="date" access="public">
		<cfargument name="d1" type="date" required="true">
		<cfargument name="d2" type="date" required="true">

		<cfif d1 gt d2>
			<cfreturn d1>
		<cfelse>
			<cfreturn d2>
		</cfif>
	</cffunction>

	<!--- Convenience function for last day of the month --->
	<cffunction name="lastDayOfMonth" access="public">
		<cfargument name="d" type="date" required="true">
		<cfreturn CreateDate(Year(d), Month(d), DaysInMonth(d))>
	</cffunction>

</cfcomponent>