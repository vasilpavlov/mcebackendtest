<cfcomponent output="false"
			 displayname="Calculation Utilities"
			 hint="This component is used to house all calculation utility methods leveraged by the Rate Model Engine.">

	<cffunction name="aggregateEnergyUsage" returntype="struct" hint="I return a structure with these values: totalRecordedValue">
		<cfargument name="usageData" type="query" required="true" hint="Describes the query containing the usage data that will be aggregated.">

		<cfset var result = StructNew()>
		<cfset var aggregates = "">

		<!--- Re-query the interval data to get some basic information --->
		<cfquery name="aggregates" dbtype="query">
			SELECT
				SUM(recorded_value) as total_recorded_value
			FROM
				usageData
		</cfquery>

		<cfif aggregates.recordCount eq 0>
			<cfset result.totalRecordedValue = 0>
			<cfreturn result>
		</cfif>

		<!--- To return to the caller --->
		<cfset result.totalRecordedValue = aggregates.total_recorded_value>

		<cfreturn result>
	</cffunction>


	<cffunction name="aggregateIntervals" returntype="struct" hint="I return a structure with these values: totalIntervalValue, maxIntervalValue, totalIntervalCost">
		<cfargument name="intervalData" type="query" required="true" hint="Describes the query containing the interval data that will be aggregated.">
		<cfargument name="classificationCode" type="string" required="false" hint="Describes the classification code that will be used to drive the interval aggregation.">

		<cfset var result = StructNew()>
		<cfset var aggregates = "">

		<!--- Re-query the interval data to get some basic information --->
		<cfquery name="aggregates" dbtype="query">
			SELECT
				SUM(interval_value) as total_interval_value,
				MAX(interval_value) as max_interval_value,
				SUM(interval_length) as total_interval_length,
				SUM(factor_times_value) as total_interval_cost
			FROM
				intervalData
			<cfif isDefined("classificationCode")>
				WHERE
					classification_code = '#classificationCode#'
			</cfif>
		</cfquery>

		<cfif aggregates.recordCount eq 0>
			<cfset result.totalIntervalValue = 0>
			<cfset result.maxIntervalValue = 0>
			<cfset result.totalIntervalLength = 0>
			<cfset result.totalIntervalCost = 0>
			<cfreturn result>
		</cfif>

		<!--- To return to the caller --->
		<cfset result.totalIntervalValue = aggregates.total_interval_value>
		<cfset result.maxIntervalValue = aggregates.max_interval_value>
		<cfset result.totalIntervalLength = aggregates.total_interval_length>
		<cfset result.totalIntervalCost = val(aggregates.total_interval_cost)>

		<cfreturn result>
	</cffunction>

	<cffunction name="analyzeIntervals" returntype="struct"
				hint="This method is used to perform analysis on interval data, and returns a structure describing the simpleConsumptionInKwh, simpleDemandInKw, averageDemandInKw, and the simpleLoadFactor.">
		<cfargument name="intervalData" type="query" required="true" hint="Describes the query containing the interval data that will be analyzed.">
		<cfargument name="classificationCode" type="string" required="false" hint="Describes the classification code that will be used to drive the interval analysis.">

		<cfset var result = StructNew()>
		<cfset var aggregates = "">

		<!--- Re-query the interval data to get some basic information --->
		<cfquery name="aggregates" dbtype="query">
			SELECT
				SUM(interval_value) as total_value,
				MAX(interval_value) as highest_value,
				AVG(interval_value) as average_value,
				COUNT(interval_value) as num_values
			FROM
				intervalData
			<cfif isDefined("classificationCode")>
				WHERE
					classification_code = '#classificationCode#'
			</cfif>
		</cfquery>

		<cfif aggregates.recordCount eq 0>
			<cfset result.simpleConsumptionInKwh = 0>
			<cfset result.simpleDemandInKw = 0>
			<cfset result.averageDemandInKw = 0>
			<cfset result.simpleLoadFactor = 0>
			<cfreturn result>
		</cfif>

		<!--- Simple calculations to return to the caller --->
		<cfset result.simpleConsumptionInKwh = aggregates.total_value / (60 / determineIntervalLength(intervalData))>
		<cfset result.simpleDemandInKw = aggregates.highest_value>
		<cfset result.averageDemandInKw = aggregates.average_value>
		<cfset result.simpleLoadFactor = aggregates.average_value / aggregates.highest_value>

		<cfreturn result>
	</cffunction>

	<cffunction name="determineIntervalLength" returntype="numeric"
				hint="This method is used to determine the length of a given interval.">
		<cfargument name="intervalData" type="query" required="true" hint="Describes the query containing the interval data whose length willbe determined.">

		<cfset var aggregates = "">

		<cfquery name="aggregates" dbtype="query">
			SELECT
				interval_length,
				COUNT(interval_length) as occurrences
			FROM
				intervalData
			GROUP BY
				interval_length
		</cfquery>

		<!--- Sanity-check the interval length, to avoid division by zero and/or nonsense output --->
		<cfif aggregates.recordCount gt 1>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="Inconsistent interval lengths"
				detail="At this time, a known limitation of the system is that all intervals in a given batch must have the same length (in minutes). The batch being considered appears to include intervals of varying lengths. The lengths found (in minutes) were: #ValueList(aggregates.interval_length)#">
		</cfif>

		<!--- Sanity-check the interval length, to avoid division by zero and/or nonsense output --->
		<cfif aggregates.interval_length eq 0>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="Unsupported interval length"
				detail="The interval length can't be zero or less than zero (actual length appears to be #aggregates.interval_length#).">
		</cfif>

		<!--- Return the value we got from the first (and only) row of the query --->
		<cfreturn val(aggregates.interval_length)>
	</cffunction>

</cfcomponent>