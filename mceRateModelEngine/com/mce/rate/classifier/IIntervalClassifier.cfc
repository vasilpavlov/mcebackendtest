<cfinterface>
	<cffunction name="classifyData" access="public" output="false" returntype="void">
		<cfargument name="intervalData" type="query" required="true">
	</cffunction>
	<cffunction name="applyFactorValues" access="public" output="false" returntype="query">
		<cfargument name="intervalData" type="query" required="true">
	</cffunction>
</cfinterface>