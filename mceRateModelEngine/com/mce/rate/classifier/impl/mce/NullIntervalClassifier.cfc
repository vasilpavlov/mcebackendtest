<cfcomponent output="false"
	extends="com.mce.rate.classifier.impl.BaseIntervalClassifier"
	implements="com.mce.rate.classifier.IIntervalClassifier">

	<cffunction name="classifyData" access="public" output="false" returntype="void">
		<cfargument name="intervalData" type="query" required="true">
	</cffunction>
</cfcomponent>