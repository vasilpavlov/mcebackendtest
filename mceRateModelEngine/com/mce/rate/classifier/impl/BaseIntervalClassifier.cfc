<cfcomponent output="false">

	<cffunction name="classifyData" access="public" output="false" returntype="void">
		<cfargument name="intervalData" type="query" required="true">
	</cffunction>

	<!---
		Notes:
		This implementation could use some improvement... it is good in terms of abstraction but adds i/o that could be avoided.
		Different approach would be to move the classifiers to the database in the form of custom functions... will be less elegant but less i/o
		Note that this only is a problem when intervals need to be classified on the fly; already-classified intervals can be dealt with better
		- nmw 10/23/08
	--->
	<cffunction name="applyFactorValues" access="public" returntype="query" output="false">
		<cfargument name="intervalData" type="query" required="true">

		<cfset var rates = "">
		<cfset var result = "">
		<cfset var chunkSize = 1000>
		<cfset var lastRowThisChunk = "">
		<cfset var i = "">

		<cfquery name="rates" datasource="mceRateModel">
			DECLARE @intervalValues IntervalValueType

			<!--- Insert rows into the @intervalValues table-valued variable, in batches of 1000 (SQL Server restricts this form of INSERT statement to 1000 rows) --->
			<cfloop from="1" to="#intervalData.recordCount#" step="1000" index="i">
				<cfset lastRowThisChunk = Min((i + chunkSize - 1), intervalData.recordCount)>

				INSERT INTO
					@intervalValues(interval_start, factor_lookup_code)
				VALUES
					<cfloop query="intervalData" startrow="#i#" endrow="#lastRowThisChunk#">
						('#interval_start#', 'TestFactor1') <cfif currentRow neq lastRowThisChunk>,</cfif>
					</cfloop>
			</cfloop>

			<!--- Select the values back out, with currect factor values (rates) --->
			SELECT
				interval_start,
				dbo.getRateFactorValue(factor_lookup_code, interval_start) as factor_value
			FROM
				@intervalValues
		</cfquery>

		<!--- Join the values we just got with the query we started with --->
		<cfquery name="result" dbtype="query">
			SELECT
				intervalData.*,
				rates.factor_value,
				(rates.factor_value * intervalData.interval_value) as value_times_factor
			FROM
				rates,
				intervalData
			WHERE
				intervalData.interval_start = rates.interval_start
			ORDER BY
				intervalData.interval_start
		</cfquery>

		<!--- Return augmented query, which has all the original columns plus factor_value and value_times_factor --->
		<cfreturn result>
	</cffunction>


</cfcomponent>