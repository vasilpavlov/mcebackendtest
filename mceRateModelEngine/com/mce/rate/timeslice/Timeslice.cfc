<cfcomponent>

	<cfproperty name="periodStart" type="date">
	<cfproperty name="periodEnd" type="date">
	<cfproperty name="numberOfDays" type="numeric">
	<cfproperty name="usagePeriodStart" type="date">
	<cfproperty name="usagePeriodEnd" type="date">


	<cffunction name="init" access="public">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="periodEnd" type="date" required="true">
		<cfargument name="numberOfDays" type="numeric" required="true">

		<!--- Sanity check on the dates --->
		<cfif DateCompare(periodEnd, periodStart) eq -1>
			<cfthrow
				message="Nonsensical start/end dates supplied to Timeslice"
				detail="The start date (#periodStart#) is after the end date (#periodEnd#)">
		</cfif>

		<cfset this.periodStart = periodStart>
		<cfset this.periodEnd = periodEnd>
		<cfset this.numberOfDays = arguments.numberOfDays>

		<cfset this.usagePeriodStart = periodStart>
		<cfset this.usagePeriodEnd = periodEnd>

		<cfreturn this>
	</cffunction>


	<!--- Setter for incoming data - A subclass can override if it wants an opportunity to massage the data somehow --->
	<cffunction name="setInputValuesData" access="public">
		<cfargument name="data" type="struct" required="true">
		<cfset this.inputValues = data>
	</cffunction>

	<!--- Setter for incoming data - A subclass can override if it wants an opportunity to massage the data somehow --->
	<cffunction name="setEnergyUsageData" access="public">
		<cfargument name="data" type="query" required="true">
		<cfset this.energyUsage = data>
	</cffunction>

	<!--- Setter for incoming data - A subclass can override if it wants an opportunity to massage the data somehow --->
	<cffunction name="setEnergyUsageMetaData" access="public">
		<cfargument name="data" type="struct" required="true">
		<cfset this.energyUsageMeta = data>
	</cffunction>
    
     <!--- Setter for incoming data - A subclass can override if it wants an opportunity to massage the data somehow --->
	<cffunction name="setPropertyEnergyUsageData" access="public">
		<cfargument name="data" type="query" required="true">
		<cfset this.energyPropertyUsage = data>
	</cffunction>

	<!--- Setter for incoming data - A subclass can override if it wants an opportunity to massage the data somehow --->
	<cffunction name="setEnergyIntervalData" access="public">
		<cfargument name="data" type="query" required="true">
		<cfset this.energyIntervals = data>
	</cffunction>

	<!--- Setter for incoming data - A subclass can override if it wants an opportunity to massage the data somehow --->
	<cffunction name="setEnergyAccountInfoData" access="public">
		<cfargument name="data" type="struct" required="true">
		<cfset this.energyAccountInfo = data>
	</cffunction>


	<!--- Convenience "getter" for getting a meta value from energy usage, by name --->
	<cffunction name="getEnergyUsageMetaValue" access="public" returntype="Any">
		<cfargument name="name" type="string" required="true">
		<cfargument name="default" type="string" required="false">

		<cfif StructKeyExists(this.energyUsageMeta, name)>
			<cfreturn this.energyUsageMeta[name]>
		<cfelseif isDefined("arguments.default")>
			<cfreturn arguments.default>
		<cfelse>
<!---			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="The requested energy usage meta field ('#name#') is not available."
				detail="The available meta fields are #StructKeyList(this.energyUsageMeta)#">--->
			<cfreturn "0">
		</cfif>
	</cffunction>


	<!--- Convenience "getter" for getting a given input value, by name --->
	<cffunction name="getInputValue" access="public" returntype="numeric">
		<cfargument name="lookupCode" type="string" required="true">

		<cfreturn getInput(lookupCode).input_value>
	</cffunction>

	<!--- Convenience "getter" for getting a given input's name, by name --->
	<cffunction name="getInputName" access="public" returntype="string">
		<cfargument name="lookupCode" type="string" required="true">

		<cfreturn getInput(lookupCode).friendly_name>
	</cffunction>


	<!--- Convenience "getter" for getting a given input as a structure, which contains friendly_name and input_value --->
	<!--- The getInputValue() and getInputName() wrap around these --->
	<cffunction name="getInput" access="private" returntype="struct">
		<cfargument name="lookupCode" type="string" required="true">

		<cfif StructKeyExists(this.inputValues, lookupCode)>
			<cfif not isNumeric(this.inputValues[lookupCode].input_value)>
				<cfinvoke
					component="#Application.utils.Logger#"
					method="error"
					message="The requested input ('#lookupCode#') has a non-numeric value ('#this.inputValues[lookupCode].input_value#')."
					detail="This value is expected to be numeric. This may indicate that there is no appropriate record associated with the input code at the Input Set level, or that neither a input_value nor a factor_lookup_code has been provided at the input value level. Check that there is an entry for the dates in question. If the value points to a rate factor, check that there is a value for the rate factor for the dates in question as well. ">
			</cfif>

			<cfreturn this.inputValues[lookupCode]>
		<cfelse>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="The requested input ('#lookupCode#') does not have an associated value."
				detail="The available input fields are: #StructKeyList(this.inputValues)#">
		</cfif>
	</cffunction>


	<!--- Convenience "getter" for getting a total from our interval data --->
	<cffunction name="getIntervalDataAggregate" returntype="numeric">
		<cfargument name="aggregateFieldName" type="string" required="true">
		<cfargument name="errorIfZero" type="boolean" required="false" default="true">
		<cfargument name="errorIfNoIntervals" type="boolean" required="false" default="true">

		<cfif errorIfNoIntervals and this.energyIntervals.recordCount eq 0>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="No interval data was retrieved for the period in question (#Dateformat(this.params.intervalPeriodStart)# to #DateFormat(this.params.intervalPeriodEnd)#) for account #this.params.intervalAccountIdentifier#."
				detail="Expected to have interval data for the period in question, but did not.">
		</cfif>

		<cfif not isDefined("this.energyIntervalAggregates")>
			<cfset this.energyIntervalAggregates = Application.utils.CalcUtils.aggregateIntervals(this.energyIntervals)>
		</cfif>

		<cfif errorIfZero and this.energyIntervalAggregates[aggregateFieldName] eq 0>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="The value of '#aggregateFieldName#' from the energy usage interval data is zero."
				detail="This may mean that there is no energy usage interval data on hand for the period in question (#DateFormat(this.params.intervalPeriodStart)# to #DateFormat(this.params.intervalPeriodEnd)#) for account #this.params.intervalAccountIdentifier#.">
		</cfif>

		<cfif not StructKeyExists(this.energyIntervalAggregates, aggregateFieldName)>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="The value of '#aggregateFieldName#' does not exist in the aggregated interval data."
				detail="The available values are: #StructKeyList(this.energyIntervalAggregates)#">
		</cfif>

		<cfreturn this.energyIntervalAggregates[aggregateFieldName]>
	</cffunction>



</cfcomponent>