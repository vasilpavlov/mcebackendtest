<!--- Declare ourselves an implementation of ITimeslice adapter --->
<cfcomponent output="false" implements="ITimesliceAdapter" extends="BaseTimesliceAdapter">

	<!--- Required by interface --->
	<!--- Here we are simply calling what has been implemented in the base class with no modifications --->
	<cffunction name="getTimeslices" access="public" returntype="array">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">
		<cfargument name="timeslices" type="array" required="true">

		<cfreturn super.getTimeslices(argumentCollection=arguments)>
	</cffunction>

</cfcomponent>