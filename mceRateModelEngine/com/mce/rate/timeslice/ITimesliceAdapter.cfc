<cfinterface>
	<cffunction name="getTimeslices" access="public" returntype="array">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">
		<cfargument name="timeslices" type="array" required="true">
	</cffunction>
</cfinterface>