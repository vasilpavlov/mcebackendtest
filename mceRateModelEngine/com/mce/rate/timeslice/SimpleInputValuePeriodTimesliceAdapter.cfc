<!--- Declare ourselves an implementation of ITimeslice adapter --->
<cfcomponent output="false" implements="ITimesliceAdapter" extends="BaseTimesliceAdapter">

	<cfset this.databaseDelegate = CreateObject("component", "com.mce.db.RateModelInputValuesDelegate")>



	<!--- Required by interface --->
	<!--- Here we are simply calling what has been implemented in the base class with no modifications --->
	<cffunction name="getTimeslices" access="public" returntype="array">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">
		<cfargument name="timeslices" type="array" required="true">

		<cfset var result = ArrayNew(1)>
		<cfset var timeslice = "">
		<cfset var qDates = "">
		<cfset var sliceStart = "">
		<cfset var sliceEnd = "">
		<cfset var i = "">

		<!--- We want the "main slice" to be included in our new array --->
		<cfset ArrayAppend(result, arguments.timeslices[1])>

		<!--- Loop through the existing timeslices (not including the "main slice") --->
		<cfloop from="2" to="#ArrayLen(arguments.timeslices)#" index="i">
			<cfset timeslice = arguments.timeslices[i]>

			<!--- See if any rates changed during this existing slice's period of time --->
			<cfset qDates = this.databaseDelegate.getInputValueDates(
				model_lookup_code = requestArgs.modelLookupCode,
				period_start = timeslice.periodStart,
				period_end = timeslice.periodEnd)>

			<!--- If no rates changed during the slice's date period, we can just pass back the existing slice --->
			<cfif qDates.recordCount eq 0>
				<cfset ArrayAppend(result, timeslice)>

			<!--- If, indeed, any of the rates did change, we'll have a query row for each relationship_start --->
			<cfelse>
				<!--- We will make one "hard-coded" subslice before we start looping over the dates --->
				<!--- This is because, ultimately, we want one more subslice than we have rate-change dates (so that one rate-change generates two subslices, etc) --->
				<!--- Make a subslice that starts at the start of the existing slice and ends at (AND INCLUDES) the first rate-change date --->
				<cfset sliceStart = timeslice.periodStart>
				<cfset sliceEnd = qDates.relationship_start[1]>
				<!--- Add the subslice to the result --->
				<cfset ArrayAppend(result, makeTimeslice(sliceStart, sliceEnd, 1))>

				<!--- Now, for each row of the query (that is, for each rate-change date) --->
				<cfloop query="qDates">
					<!--- We want to make a subslice that starts JUST AFTER the rate-change date --->
					<!--- We are starting just after because the preceding slice will have included (ended with) the rate-change date itself --->
					<cfset sliceStart = DateAdd("d", 1, qDates.relationship_start[qDates.currentRow])>
					<!--- If there is another rate-change date after this one, end this subslice on that date --->
					<cfif qDates.currentRow lt qDates.recordCount>
						<cfset sliceEnd = qDates.relationship_start[qDates.currentRow + 1]>
						
					<!--- If this is the last (or only) rate-change date, end this subslice at the end of the existing slice --->
					<cfelse>
						<cfset sliceEnd = timeslice.periodEnd>												
					</cfif>
					
					<cfif sliceEnd lt sliceStart>
						<cfset sliceStart = sliceEnd>
					</cfif>
				</cfloop>

				<!--- Add the subslice to our result --->
				<cfset ArrayAppend(result, makeTimeslice(sliceStart, sliceEnd, 1))>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>

</cfcomponent>