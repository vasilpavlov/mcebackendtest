<cfcomponent output="no">
	<cfset this.basePackageName = "com.mce.rate.classifier.impl">
	<cfset this.rateClassifierDelegate = CreateObject("component", "com.mce.db.RateClassifierDelegate")>

	<cffunction name="getClassifierInstance" access="public" output="no" returntype="com.mce.rate.classifier.IIntervalClassifier">
		<cfargument name="lookupCode" type="string">
		<cfreturn getComponentInstanceForLookupCode(lookupCode)>
	</cffunction>
	
	<cffunction name="getComponentInstanceForLookupCode" access="private" output="no" returntype="com.mce.rate.classifier.IIntervalClassifier">
		<cfargument name="lookupCode" type="string">
		<cfset var name = getComponentNameForLookupCode(lookupCode)>
		<cfreturn CreateObject("component", name)>
	</cffunction>

	<cffunction name="getComponentNameForLookupCode" access="private" output="no" returntype="String">
		<cfargument name="lookupCode" type="string">
		<cfset var record = this.rateClassifierDelegate.getClassifierRecord(lookupCode)>
		<cfreturn "#this.basePackageName#.#record.implementation_file#">
	</cffunction>
</cfcomponent>