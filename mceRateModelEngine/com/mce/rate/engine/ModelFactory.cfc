<cfcomponent output="no">
	<cfset this.rateModelDelegate = CreateObject("component", "com.mce.db.RateModelDelegate")>
	<cfset this.basePackageName = "com.mce.rate.calculator.impl">

	<cffunction name="getModelInstance" access="public" output="no" returntype="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">
		<cfargument name="locatorString" type="string">
		<cfargument name="requestArgs" type="struct">
		<cfset var instance = getComponentInstanceForLocatorString(locatorString)>
		<cfreturn instance>
	</cffunction>

	<cffunction name="getComponentInstanceForLocatorString" access="private" output="no" returntype="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">
		<cfargument name="locatorString" type="string">
		<cfset var name = getComponentNameForLocatorString(locatorString)>
		<cfset var inst = CreateObject("component", Trim(name)).init(locatorString)>
		<cfreturn inst>
	</cffunction>

	<cffunction name="getComponentNameForLocatorString" access="private" output="no" returntype="String">
		<cfargument name="locatorString" type="string">
		<cfset var record = this.rateModelDelegate.getModelComponentRecord(locatorString)>
		<cfreturn "#this.basePackageName#.#record.implementation_file#">
	</cffunction>
</cfcomponent>