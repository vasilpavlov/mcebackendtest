<cfcomponent output="false">
	<cffunction name="init">
		<cfargument name="request" type="RateModelRequest" required="true">
		<cfset this.request = request>
		<cfset request.response = this>
		<cfreturn this>
	</cffunction>

	<cffunction name="setResult">
		<cfargument name="result" type="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfset this.result = result>
	</cffunction>

	<cffunction name="toSerializableForm">
		<cfargument name="intervalDataMaxRows" type="numeric" required="false" default="-1" hint="Optinally gates the amount of interval data returned. Does not affect internal calculations. The default value of -1 will result in all rows being returned.">

		<cfset var s = StructNew()>
		<cfset s.status = this.request.status>
		<cfif s.status eq "ok">
			<cfset s.result = this.result.toSerializableForm(argumentCollection=arguments)>
		<cfelse>
			<cfset s.message = this.request.exception.message>
			<cfset s.detail = this.request.exception.detail>
		</cfif>
		<cfreturn s>
	</cffunction>
</cfcomponent>