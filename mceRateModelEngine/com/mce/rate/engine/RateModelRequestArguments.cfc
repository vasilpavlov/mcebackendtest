<cfcomponent>

	<cffunction name="init" access="public" returntype="RateModelRequestArguments">

		<!--- Main arguments --->
		<cfargument name="modelLookupCode" type="string" required="true" hint="Describes the rate model that is being queried.">
		<cfargument name="accountIdentifier" type="string" required="true" hint="Describes the primary key /unique identifier for a given account.">
		<cfargument name="periodStart" type="date" required="true" hint="Describes the start date that will be used to query the specified rate mdoel.">
		<cfargument name="periodEnd" type="date" required="true" hint="Described the end datethat will be used to query the specified rate model.">

		<cfargument name="usageCriteria" type="string" required="false" default='{"usage_type_code":"actual"}'>
		<cfargument name="inputValueOverrides" type="struct" required="false" default="#StructNew()#">

		<!--- Optional: Allow for override of where usage data comes from --->
		<cfargument name="usageAccountIdentifier" type="string" required="false" default="#accountIdentifier#" hint="Describes the primary key / unique identifier for the usage account.">
		<cfargument name="usagePeriodStart" type="date" required="false" default="#periodStart#" hint="Describes the energy usage start date used to query the specified rate model.">
		<cfargument name="usagePeriodEnd" type="date" required="false" default="#periodEnd#" hint="Describes the energy usage end date used to query the specified rate model.">

		<!--- Optional: Allow for override of where interval data, if any, comes from --->
		<cfargument name="intervalAccountIdentifier" type="string" required="false" default="#accountIdentifier#" hint="Describes the primary key / unique identifier for the interval account.">
		<cfargument name="intervalPeriodStart" type="date" required="false" default="#periodStart#" hint="Describes the interval start date used to query the specified rate model.">
		<cfargument name="intervalPeriodEnd" type="date" required="false" default="#periodEnd#" hint="Describes the interval start date used to query the specified rate model.">

		<!--- Optional: Defaults for "accessor" codes --->
		<cfargument name="inputValuesAccessorCode" type="string" required="false" default="mce_internal" hint="Describes the accessor lookup code defining the input values used to query the specified rate model.">
		<cfargument name="energyUsageAccessorCode" type="string" required="false" default="mce_internal" hint="Describes the accessor lookup code for energy usage account information.">
		<cfargument name="energyIntervalAccessorCode" type="string" required="false" default="mce_internal" hint="Describes the accessor lookup code for energy interval account information.">
		<cfargument name="energyAccountInfoAccessorCode" type="string" required="false" default="mce_internal" hint="Describes the accessor lookup code the energy account information.">
        
       <cfargument name="property" type="string" required="false" default="property" hint="Property friendly name.">
        <cfargument name="usage" type="numeric" required="false" default="0" hint="Describes the accessor lookup code for energy interval account information.">
		<cfargument name="demand" type="numeric" required="false" default="0" hint="Describes the accessor lookup code the energy account information.">
        <cfargument name="tripCode" type="numeric" required="false" default="-1" hint="Describes the overrides for any input values that are processed.">
		
		<!--- For now let's keep this simple and retain each argument value as a property on this CFC instance --->
		<cfset var thisArg = "">
		<cfloop collection="#arguments#" item="thisArg">
			<cfset this[thisArg] = arguments[thisArg]>
		</cfloop>

		<cfset this.usageCriteria = DeserializeJson(arguments.usageCriteria)>
		
		<!--- By naming convention, "init" returns the instance itself --->
		<cfreturn this>
	</cffunction>


	<cffunction name="setTimeslice">
		<cfargument name="timeslice" type="struct" required="true">

		<cfset this.periodStart = timeslice.periodStart>
        <cfset this.periodEnd = timeslice.periodEnd>

	</cffunction>


	<cffunction name="toAnonymousStruct" access="public" returntype="struct">
		<cfset var result = {}>

		<cfset result.modelLookupCode = this.modelLookupCode>
		<cfset result.accountIdentifier = this.accountIdentifier>
		<cfset result.periodStart = DateFormat(this.periodStart, 'm/dd/yyyy')>
		<cfset result.periodEnd = DateFormat(this.periodEnd, 'm/dd/yyyy')>

		<cfreturn result>
	</cffunction>

</cfcomponent>