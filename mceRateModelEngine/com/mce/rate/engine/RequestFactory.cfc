<cfcomponent output="false">
	<cffunction name="getRequestInstance" returntype="RateModelRequest">
		<cfargument name="model" type="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">
		<cfargument name="requestArgs" required="true">

		<cfreturn CreateObject("component", "RateModelRequest").init(model, requestArgs)>
	</cffunction>
</cfcomponent>