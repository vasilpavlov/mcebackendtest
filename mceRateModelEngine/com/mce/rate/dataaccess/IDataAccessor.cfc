<cfinterface>
	<cffunction name="getData" returntype="query">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
	</cffunction>
</cfinterface>