<cfcomponent output="false"
	extends="com.mce.rate.dataaccess.impl.BaseDataAccessor"
	implements="com.mce.rate.dataaccess.IEnergyIntervalDataAccessor">

	<cfset this.databaseDelegate = CreateObject("component", "com.mce.db.EnergyIntervalDelegate")>

	<cffunction name="getData" access="public" returntype="query">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">

		<cfset var codeMap = {OP='mce.ny.electric.coned.day-ahead.zone-j', PK='mce.ny.electric.coned.day-ahead.zone-j'}>
		<cfreturn this.databaseDelegate.getEnergyIntervals(account_identifier, period_start, period_end, codeMap)>
	</cffunction>
</cfcomponent>