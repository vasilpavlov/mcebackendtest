<cfcomponent output="false"
	extends="com.mce.rate.dataaccess.impl.BaseDataAccessor"
	implements="com.mce.rate.dataaccess.IEnergyUsageDataAccessor">

	<cfset this.databaseDelegate = CreateObject("component", "com.mce.db.EnergyUsageDelegate")>

	<cffunction name="getEnergyUsageData" access="public" returntype="query">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		<cfargument name="usageCriteria" type="struct" required="true">

		<cfreturn this.databaseDelegate.getEnergyUsage(account_identifier, period_start, period_end, usageCriteria.usage_type_code)>
	</cffunction>

	<cffunction name="getEnergyUsageMetaData" access="public" returntype="query">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		<cfargument name="usageCriteria" type="struct" required="true">

		<cfreturn this.databaseDelegate.getEnergyUsageMeta(account_identifier, period_start, period_end, usageCriteria.usage_type_code)>
	</cffunction>
	
	<cffunction name="getEnergyUsageForPropertyElectricityAccounts" access="public" returntype="query">
		<cfargument name="property" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">

		<cfreturn this.databaseDelegate.getEnergyUsageForPropertyElectricityAccounts(property, period_start, period_end)>
	</cffunction>
	
</cfcomponent>