<cfinterface>
	<cffunction name="init" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">
		<cfargument name="modelLookupCode" type="string" required="true">
	</cffunction>

	<cffunction name="getTimesliceAdapters" access="public" returntype="array">
	</cffunction>

	<cffunction name="configure" access="public" returntype="void">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">
	</cffunction>

	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
	</cffunction>

	<cffunction name="loadData" access="public" returntype="void">
	</cffunction>
</cfinterface>