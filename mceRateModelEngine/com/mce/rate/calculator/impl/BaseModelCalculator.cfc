<cfcomponent output="false">

	<!--- Required by IModelCalculator interface --->
	<cffunction name="init" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">
		<cfargument name="modelLookupCode" type="string" required="true">
		<cfset this.modelLookupCode = modelLookupCode>
		<cfreturn this>
	</cffunction>

	<!--- Required by IModelCalculator interface --->
	<cffunction name="getTimesliceAdapters" access="public" returntype="array">
		<cfset var defaultAdapter = CreateObject("component", "mceRateModelEngine.com.mce.rate.timeslice.DefaultTimesliceAdapter")>
		<cfset var result = [defaultAdapter]>
		<cfreturn result>
	</cffunction>

	<!--- Required by IModelCalculator interface --->
	<cffunction name="configure" access="public" returntype="void">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">

		<cfset this.params = requestArgs>
		<cfset this.timeslices = getTimeslices()>
	</cffunction>

	<!--- Required by IModelCalculator interface --->
	<!--- Subclasses should overload this method... here we just return an empty result (total will be zero) --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfset var result = CreateObject("component", "CalculatorResult")>
		<cfreturn result>
	</cffunction>

	<!--- Required by IModelCalculator interface --->
	<!--- Subclasses probably won't override this method, but rather will override the methods called inside this method --->
	<cffunction name="loadData" access="public" returntype="void">
		<cfif getSlicesThatNeedInputValuesData() neq "none">
			<cfset loadInputValuesData()>
		</cfif>
		<cfif getSlicesThatNeedEnergyUsageData() neq "none">
			<cfset loadEnergyUsageData()>
		</cfif>
		<cfif getSlicesThatNeedIntervalData() neq "none">
			<cfset loadEnergyIntervalData()>
		</cfif>
		<cfif getSlicesThatNeedEnergyAccountInfoData() neq "none">
			<cfset loadEnergyAccountInfoData()>
		</cfif>
	</cffunction>


	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
		<cfreturn "">
	</cffunction>

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedInputValuesData" access="package" returntype="string">
		<!--- Most calculators want input values, and probably want it for each slice (if there are multiple slices) separately --->
		<cfreturn "all">
	</cffunction>

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedEnergyUsageData" access="package" returntype="string">
		<!--- Most calculators want the EnergyUsage record, but it typically wouldn't be different per timeslice, so use "first" by default --->
		<cfreturn "first">
	</cffunction>

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<!--- Many calculators don't need interval data; those that do will set this to "first" or "all" --->
		<cfreturn "none">
	</cffunction>

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedEnergyAccountInfoData" access="package" returntype="string">
		<!--- Most calculators probably want this data; let's assume that we don't care about these values changing per slice so we'll use "first" --->
		<!--- A particular calculator might possibly want to set this to "all", or "none" if it doesn't need this data at all --->
		<cfreturn "first">
	</cffunction>

	<!--- Used internally by BaseModelCalculator to determine what data classifier to use, if any; subclasses can answer differently as appopriate --->
	<cffunction name="getClassifierLookupCode" access="package" returntype="string">
		<!--- Value "none" means that no classifier class will be instantiated --->
		<cfreturn "none">
	</cffunction>

	<!---
		Loader methods for gettting database or external data
		Subclasses could override if they want an opportunity to tweak the logic for an invividual loader method
	--->
	<!--- Loader method; gets data (from internal database or other source) via accessor --->
	<cffunction name="loadInputValuesData" access="package">
		<!--- Local variables --->
		<cfset var local = {}>
		<cfset var i = 0>
		<cfset var timeslice = "">

		<cfset var accessor = Application.singletons.DataAccessorRegistry.getInputValuesDataAccessor(this.params.inputValuesAccessorCode)>
		<cfset var numberOfSlices = numberOfSlicesFor(getSlicesThatNeedInputValuesData())>

		<!--- For each slice (may be just first slice or all slices, depending on numberOfSlices) --->
		<cfloop from="1" to="#numberOfSlices#" index="i">
			<cfset timeslice = this.timeslices[i]>
			<cfset local.data = accessor.getData(this.modelLookupCode, getRequiredInputCodes(), timeslice.periodStart, timeslice.periodEnd)>
			<cfset local.dataAsStruct = Application.utils.QueryUtils.queryToObjectStruct(local.data, "input_lookup_code", "input_value,friendly_name")>
			<cfset local.mergedStruct = Application.utils.StructUtils.structMerge(this.params.inputValueOverrides, local.dataAsStruct)>

			<cfset timeslice.setInputValuesData(local.mergedStruct)>
		</cfloop>
	</cffunction>

	<!--- Loader method; gets data (from internal database or other source) via accessor --->
	<cffunction name="loadEnergyUsageData" access="package">
		<!--- Local variables --->
		<cfset var i = 0>
		<cfset var timeslice = "">
		<cfset var data = "">
		<cfset var meta = "">
		<cfset var metaAsStruct = "">

		<cfset var accessor = Application.singletons.DataAccessorRegistry.getEnergyUsageDataAccessor(this.params.energyUsageAccessorCode)>
		<cfset var numberOfSlices = numberOfSlicesFor(getSlicesThatNeedEnergyUsageData())>

		<!--- For each slice (may be just first slice or all slices, depending on numberOfSlices) --->
		<cfloop from="1" to="#numberOfSlices#" index="i">
			<cfset timeslice = this.timeslices[i]>

			<cfset data = accessor.getEnergyUsageData(this.params.usageAccountIdentifier, timeslice.usagePeriodStart, timeslice.usagePeriodEnd, this.params.usageCriteria)>
			<cfset meta = accessor.getEnergyUsageMetaData(this.params.usageAccountIdentifier, timeslice.usagePeriodStart, timeslice.usagePeriodEnd, this.params.usageCriteria)>
			<cfset propertyAccountsEnergyUsageData = accessor.getEnergyUsageForPropertyElectricityAccounts(this.params.property, timeslice.usagePeriodStart, timeslice.usagePeriodEnd)>
			<cfset metaAsStruct = Application.utils.QueryUtils.queryToValueStruct(meta, "type_lookup_code", "meta_value")>

			<cfset timeslice.setEnergyUsageData(data)>
			<cfset timeslice.setEnergyUsageMetaData(metaAsStruct)>
			<cfset timeslice.setPropertyEnergyUsageData(propertyAccountsEnergyUsageData)>
		</cfloop>
	</cffunction>

	<!--- Loader method; gets data (from internal database or other source) via accessor --->
	<cffunction name="loadEnergyAccountInfoData" access="package">
		<!--- Local variables --->
		<cfset var i = 0>
		<cfset var timeslice = "">
		<cfset var data = "">

		<cfset var accessor = Application.singletons.DataAccessorRegistry.getEnergyAccountInfoDataAccessor(this.params.energyAccountInfoAccessorCode)>
		<cfset var numberOfSlices = numberOfSlicesFor(getSlicesThatNeedEnergyAccountInfoData())>

		<!--- For each slice (may be just first slice or all slices, depending on numberOfSlices) --->
		<cfloop from="1" to="#numberOfSlices#" index="i">
			<cfset timeslice = this.timeslices[i]>
			<cfset data = accessor.getData(this.params.accountIdentifier, timeslice.periodStart, timeslice.periodEnd)>
			<cfset timeslice.setEnergyAccountInfoData(Application.utils.QueryUtils.queryToValueStruct(data, "type_lookup_code", "meta_value"))>
		</cfloop>
	</cffunction>

	<!--- Loader method; gets data (from internal database or other source) via accessor --->
	<cffunction name="loadEnergyIntervalData" access="package">
		<!--- Local variables --->
		<cfset var i = 0>
		<cfset var timeslice = "">
		<cfset var data = "">

		<cfset var accessor = Application.singletons.DataAccessorRegistry.getEnergyIntervalDataAccessor(this.params.energyIntervalAccessorCode)>
		<cfset var numberOfSlices = numberOfSlicesFor(getSlicesThatNeedIntervalData())>

		<!--- For each slice (may be just first slice or all slices, depending on numberOfSlices) --->
		<cfloop from="1" to="#numberOfSlices#" index="i">
			<cfset timeslice = "">
			<cfset data = accessor.getData(this.params.intervalAccountIdentifier, this.params.intervalPeriodStart, this.params.intervalPeriodEnd)>
			<cfset timeslice.setEnergyIntervalData(data)>
			<cfif this.getClassifierLookupCode() neq "none">
				<cfset classifyEnergyIntervalData()>
			</cfif>
		</cfloop>
	</cffunction>

	<!--- Loader method; gets data (from internal database or other source) via accessor --->
	<cffunction name="classifyEnergyIntervalData" access="package">
		<!--- Local variables --->
		<cfset var i = 0>
		<cfset var timeslice = "">

		<cfset var classifier = Application.singletons.ClassifierFactory.getClassifierInstance(this.getClassifierLookupCode())>
		<cfset var numberOfSlices = numberOfSlicesFor(getSlicesThatNeedIntervalData())>

		<!--- For each slice (may be just first slice or all slices, depending on numberOfSlices) --->
		<cfloop from="1" to="#numberOfSlices#" index="i">
			<cfset timeslice = "">
			<cfset classifier.classifyData(timeslice.energyIntervals)>
			<cfset timeslice.setEnergyIntervalData(classifier.applyFactorValues(timeslice.energyIntervals))>
		</cfloop>
	</cffunction>


	<!--- Internal convenience function to get slices from timeslice adapters --->
	<cffunction name="getTimeslices" access="private" returntype="array">
		<cfset var adapters = getTimesliceAdapters()>
		<cfset var adapter = "">
		<cfset var slices = ArrayNew(1)>

		<cfloop array="#adapters#" index="adapter">
			<cfset slices = adapter.getTimeslices(this.params, slices)>
		</cfloop>

		<cfreturn slices>
	</cffunction>

	<!--- Internal convenience method that returns a number based on the strings returned by getSlicesThatNeedInputValuesData() etc --->
	<cffunction name="numberOfSlicesFor" returntype="numeric" access="package">
		<cfargument name="str" type="string" required="true">

		<cfswitch expression="#str#">
			<cfcase value="first">
				<cfreturn 1>
			</cfcase>
			<cfcase value="all">
				<cfreturn ArrayLen(this.timeslices)>
			</cfcase>
			<cfdefaultcase>
				<cfreturn 0>
			</cfdefaultcase>>
		</cfswitch>
	</cffunction>



</cfcomponent>