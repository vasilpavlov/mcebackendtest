<cfcomponent
	extends="com.mce.rate.calculator.impl.BaseModelCalculator"
	implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
		<cfreturn "peakRatePerKwh,offPeakRatePerKwh,taxRatePerKwh,testx">
	</cffunction>

	<!--- Used internally by BaseModelCalculator to determine what data classifier to use, if any; subclasses can answer differently as appopriate --->
	<cffunction name="getClassifierLookupCode" access="package" returntype="string">
		<cfreturn "TestPeakOffPeak">
	</cffunction>

	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();

			// Use helper methods to perform common math calculations
			var analyzedPeak = Application.utils.CalcUtils.analyzeIntervals(this.data.energyIntervals, "PK");
			var analyzedOffPeak = Application.utils.CalcUtils.analyzeIntervals(this.data.energyIntervals, "OP");

			// Perform whatever custom calculations are appropriate
			var peakKwh = analyzedPeak.simpleConsumptionInKwh;
			var offPeakKwh = analyzedOffPeak.simpleConsumptionInKwh;
			var totalKwh = peakKwh + offPeakKwh;
			var peakCost = peakKwh * this.data.inputValues.peakRatePerKwh;
			var offPeakCost = offPeakKwh * this.data.inputValues.offPeakRatePerKwh;
			var largeBuildingSurcharge = IIF(this.data.energyAccountInfo.sqft > 1000, 10, 0);
			var isTaxExempt = this.data.energyAccountInfo.taxexempt == 1;
			var salesTax = 0;

			// Calculator can use whatever conditional logic is needed to make calculations
			if (not isTaxExempt) {
				salesTax = totalKwh * this.data.inputValues.taxRatePerKwh;
			}

			// Calculator's #1 responsibility is setting the total on the result object
			result.setTotal(peakCost + offPeakCost + salesTax + largeBuildingSurcharge);

			// Calculator may add output that will be helpful to customers or analysts, emulating line items on paper bill.
			// It can use addOutput() directly, or addEnergyOutput(), addMoneyOutput(), addNumberOutput(), which provide easy formatting.
			// While it is not required that all of the components in the total are outputted here, doing so increases transparency.
			// Note that the calculator is certainly free to add outputs that are *not* components of the total as well.
			result.addEnergyOutput("Usage", "Usage in kwh (Peak)", peakKwh);
			result.addEnergyOutput("Usage", "Usage in kwh (Off-Peak)", offPeakKwh);
			result.addEnergyOutput("Usage", "Total Usage in kwh", totalKwh);
			result.addMoneyOutput("Rates", "Rate per kwh (Peak)", this.data.inputValues.peakRatePerKwh);
			result.addMoneyOutput("Rates", "Rate per kwh (Off-Peak)", this.data.inputValues.offPeakRatePerKwh);
			result.addMoneyOutput("Cost", "Your Cost (Peak)", peakCost);
			result.addMoneyOutput("Cost", "Your Cost (Off-Peak)", offPeakCost);
			result.addEnergyOutput("Demand", "Demand in kw (Peak)", analyzedPeak.simpleDemandInKw);
			result.addEnergyOutput("Demand", "Demand in kw (Off-Peak)", analyzedOffPeak.simpleDemandInKw);
			result.addNumberOutput("Demand", "Load Factor", analyzedOffPeak.simpleLoadFactor, "0.00");
			result.addMoneyOutput("Rates", "Tax Rate", this.data.inputValues.taxRatePerKwh);

			// Some output can be included conditionally if appropriate
			if (largeBuildingSurcharge > 0) {
				result.addMoneyOutput("Misc Charges", "Large Building Surcharge", largeBuildingSurcharge);
			}
			if (not isTaxExempt) {
				result.addMoneyOutput("Misc Charges", "Sales Tax", salesTax);
			}
		</cfscript>
		<cfreturn result>
	</cffunction>

</cfcomponent>