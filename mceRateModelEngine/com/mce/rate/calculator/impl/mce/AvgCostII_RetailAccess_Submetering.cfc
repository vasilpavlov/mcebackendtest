<cfcomponent extends="com.mce.rate.calculator.impl.BaseModelCalculator"
			 implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
    <cfreturn "">
	</cffunction>
	
	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "none">
	</cffunction>


	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();
			var numslices = ArrayLen(this.timeslices); //includes full period slice
			var mainSlice = this.timeslices[1]; // 1 is the full billing period slice

		// Input & Calculations

			//Usage Info
			var period_start = mainSlice.periodStart;
			var period_end = mainSlice.periodEnd;
			var usage = this.params.usage; 
			var demand = this.params.demand;
			
			var consumption = mainSlice.energyPropertyUsage["Consumption"][mainSlice.energyPropertyUsage.currentRow];
			var demandSupplyCharge = mainSlice.energyPropertyUsage["DemandSupplycharge"][mainSlice.energyPropertyUsage.currentRow];
			var supplyGRTandOtherTaxCharges = mainSlice.energyPropertyUsage["GRTandOthertaxsurcharges_Supply"][mainSlice.energyPropertyUsage.currentRow];
			var supplyChargesWithGRT = mainSlice.energyPropertyUsage["Supplycharges_WithGRT"][mainSlice.energyPropertyUsage.currentRow];
			var demandDeliveryCharge = mainSlice.energyPropertyUsage["DemandDeliveryCharge"][mainSlice.energyPropertyUsage.currentRow];
			var billableReactivePowerDemand =  mainSlice.energyPropertyUsage["BillableReactivePowerDemand"][mainSlice.energyPropertyUsage.currentRow];
			var deliveryGRTandOtherTaxCharges = mainSlice.energyPropertyUsage["GRTandOthertaxsurcharges_Delivery"][mainSlice.energyPropertyUsage.currentRow];
			var deliveryChargesWithGRT = mainSlice.energyPropertyUsage["Deliverycharges_withGRT"][mainSlice.energyPropertyUsage.currentRow];
					
			//Total KW cost before sales tax including GRT
			//(Demand Delivery Charge $ + Billable Reactive Power Demand) * (1 + (Delivery GRT & Other tax charges $/(Delivery charges with GRT $ - Delivery GRT & Other tax charges $)))


			var deliveryDiv = deliveryChargesWithGRT - deliveryGRTandOtherTaxCharges;
						
			
			var totalKwCostBeforeSalesTaxIncludingGRT=0;
			var averageCostOfKw =0;
			var totalKwHCostBeforeSalesTaxIncludingGRT =0;
			var averageCostOfKwH =0;
			var totalBillingCharges	=0;
			var sec=0;
			var demandProperty=0;
			
				
			if(deliveryDiv eq 0)
			{
				sec = 0;
			}
			else
			{
				sec = deliveryGRTandOtherTaxCharges/deliveryDiv;
			}
			
			totalKwCostBeforeSalesTaxIncludingGRT = (demandDeliveryCharge + billableReactivePowerDemand) * (1 + sec);
			
			//Average cost of kw
			if(demandProperty eq 0)
			{
				averageCostOfKw = 0;
			}
			else
			{
				averageCostOfKw = totalKwCostBeforeSalesTaxIncludingGRT/demandProperty;
			}
			
			//Total KWH cost before sales tax including GRT
			//(Delivery charges with GRT $ + supply charges with GRT) - Total KW cost before sales tax including GRT
			totalKwHCostBeforeSalesTaxIncludingGRT = (deliveryChargesWithGRT + supplyChargesWithGRT) - totalKwCostBeforeSalesTaxIncludingGRT;
			
			//Average cost of kWh
			averageCostOfKwH = IIF(consumption EQUAL 0, 0, totalKwHCostBeforeSalesTaxIncludingGRT/consumption);
			
			//Total Cost
			totalBillingCharges	= averageCostOfKw * demand + averageCostOfKwH * usage;
			
		// Results Output
			
			result.setTotal(totalBillingCharges);
			
		</cfscript>


		<cffile 
   action = "append" 
    file = "D:\cfdata\mceRateModelEngine\Log.txt"
    output = "">
	
		<cfreturn result>
	</cffunction>

</cfcomponent>