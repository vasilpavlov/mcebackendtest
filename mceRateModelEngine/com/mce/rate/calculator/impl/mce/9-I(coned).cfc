<cfcomponent extends="com.mce.rate.calculator.impl.BaseSimpleThirtyDayProratingModelCalculator"
			 implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
				<cfreturn "ny.conedison.nyc.9-I.meteroth.billpayproc,ny.conedison.nyc.9-I.meteroth.billpayproc.riderM,ny.conedison.nyc.9-I.meteroth.metercharge,ny.conedison.nyc.9-I.meteroth.metercharge.riderM,ny.conedison.nyc.9-I.meteroth.meterdatacharge,ny.conedison.nyc.9-I.meteroth.meterdatacharge.riderM,ny.conedison.nyc.9-I.meteroth.metersvccharge,ny.conedison.nyc.9-I.meteroth.metersvccharge.riderM,ny.conedison.nyc.9-I.delivery.demand.100kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.5kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.5kw.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.5kw.trans.kw,ny.conedison.nyc.9-I.delivery.demand.895kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.895kw.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.895kw.trans.kw,ny.conedison.nyc.9-I.delivery.demand.900kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.900kw.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.900kw.trans.kw,ny.conedison.nyc.9-I.delivery.demand.95kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.first900kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.over900kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.trans.kw,ny.conedison.nyc.9-I.delivery.energy.dist.kwh,ny.conedison.nyc.9-I.delivery.energy.first15K.dist.kwh,ny.conedison.nyc.9-I.delivery.energy.mac.kwh,ny.conedison.nyc.9-I.delivery.energy.over15K.dist.kwh,ny.conedison.nyc.9-I.delivery.energy.trans.kwh,ny.conedison.nyc.9-I.sbcrps.rps.kwh,ny.conedison.nyc.9-I.sbcrps.sbc.kwh,ny.conedison.nyc.9-I.supply.demand.5kw.kw,ny.conedison.nyc.9-I.supply.demand.895kw.kw,ny.conedison.nyc.9-I.supply.demand.900kw.kw,ny.conedison.nyc.9-I.supply.energy.MFC.kwh,ny.conedison.nyc.9-I.supply.energy.MSC-I-adj.kwh,ny.conedison.nyc.9-I.supply.energy.MSC-II-adj.kwh,ny.conedison.nyc.9-I.supply.energy.MSC.kwh,ny.conedison.nyc.9-I.supply.energy.mscAdjDate.kwh,ny.conedison.nyc.9-I.macadj.kwh,ny.conedison.nyc.9-I.maccharges.delrevsur.kwh,ny.conedison.nyc.9-I.maccharges.psl18a.kwh,ny.conedison.nyc.9-I.maccharges.revdecmecadj.kwh,ny.conedison.nyc.9-I.delivery.grt,ny.conedison.nyc.9-I.fullservice.salestax,ny.conedison.nyc.9-I.retailaccess.salestax,ny.conedison.nyc.9-I.supply.grt">
	</cffunction>
	
	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "none">
	</cffunction>


	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();
			var numslices = ArrayLen(this.timeslices); //includes full period slice
			var mainSlice = this.timeslices[1]; // 1 is the full billing period slice

		// Input & Calculations
		
			//Usage Info
			var period_start = mainSlice.periodStart;
			var period_end = mainSlice.periodEnd;
			 var recorded_value = mainSlice.energyUsage.recorded_value; 
			var demand = mainSlice.getEnergyUsageMetaValue("Demand");
			var billingdays = mainSlice.numberOfDays;
			var hasSupplyContract = mainSlice.energyUsage.hasSupplyContract;
			var rdmadjustment = mainSlice.getInputValue("ny.conedison.nyc.9-I.maccharges.revdecmecadj.kwh");
			
			//Energy Charges
			var energyMarketsupply = IIF(hasSupplyContract eq True,0, recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.supply.energy.MSC.kwh")/100);
			var energyMonthlyadjustments = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyMonthlyadjustments")/100;
			var energyDeliverycharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.delivery.energy.dist.kwh")/100;
			var energySystembenefits = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.sbcrps.sbc.kwh")/100;
			var energyRenewableportfoliostandardprogram = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.sbcrps.rps.kwh")/100;
			var energyMerchantfunctioncharge = IIF(hasSupplyContract eq True,0, recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.supply.energy.MFC.kwh")/100);
			var energyRevenuedecouplingmechanismadjustment = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.maccharges.revdecmecadj.kwh")/100;
			var energyDeliveryrevenuesurcharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.maccharges.delrevsur.kwh")/100;
			var energySurchargePslSection18aAssessments = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.maccharges.psl18a.kwh")/100;

			//Adjustment Factors
			var adjMSCi = mainSlice.getInputValue("ny.conedison.nyc.9-I.supply.energy.MSC-I-adj.kwh");
			var adjMSCii = mainSlice.getInputValue("ny.conedison.nyc.9-I.supply.energy.MSC-II-adj.kwh");			
			var adjMSC = adjMSCi + adjMSCii;//mainSlice.getInputValue("adjMSC");

			var adjMACreconciliation = mainSlice.getInputValue("adjMACreconciliation");
			var adjMACuncollectiblebillexpense = mainSlice.getInputValue("adjMACuncollectiblebillexpense");
			var adjMACtransitionadjustment = mainSlice.getInputValue("adjMACtransitionadjustment");
			var adjRevenuedecouplingmechanismadjustment = prorateInputValueUsingPortionOfTotalDaysPolicy("energyRevenuedecouplingmechanismadjustment")/100;
			var adjDeliveryrevenuesurcharge = prorateInputValueUsingPortionOfTotalDaysPolicy("energyDeliveryrevenuesurcharge")/100;
			var adjPSL18a =  mainSlice.getInputValue("energySurchargePslSection18aAssessments");

			//adjMAC for 4-I & 9-I don't include adjPSL18a
			var adjMAC = adjMACreconciliation + adjMACuncollectiblebillexpense +  adjMACtransitionadjustment
			+ adjRevenuedecouplingmechanismadjustment + adjDeliveryrevenuesurcharge;
			
			//Energy Subtotal
			var subTotalEnergyChargeBeforeAdjustments = energyMarketsupply + energyMonthlyadjustments
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energyMerchantfunctioncharge + energyRevenuedecouplingmechanismadjustment + energyDeliveryrevenuesurcharge
			+ energySurchargePslSection18aAssessments;

			var adjustmentFactorMSC = IIF(hasSupplyContract eq True,0,recorded_value * adjMSC);
			var adjustmentFactorMAC = recorded_value * adjMAC;
			var adjustmentFactorTotal = adjustmentFactorMSC + adjustmentFactorMAC;

			var totalCommodityCharge = adjustmentFactorMSC + energyMarketsupply 
			+ energyMerchantfunctioncharge;
			
			var grtCommodityTaxCharge = (totalCommodityCharge * mainSlice.getInputValue("taxGRTCommodity"))/100;
			
			var energyCharges = energyMonthlyadjustments + (adjMAC * recorded_value) + (recorded_value * (prorateInputValueUsingPortionOfTotalDaysPolicy("energyDeliverycharge")/100));

			var totalEnergyDeliveryCharges  = 	energyMonthlyadjustments + energyDeliverycharge 
			+ adjustmentFactorMAC;
			var totalSBCRPSCharge = energySystembenefits + energyRenewableportfoliostandardprogram;
			
			//Meter Charges
			var energyMetercharges = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMetercharges"));
			var energyMeterservicecharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMeterservicecharge"));
			var energyMeterdatacharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMeterdatacharge"));
			var energyBillingandpaymentprocessingsingleservice = mainSlice.getInputValue("energyBillingandpaymentprocessingsingleservice");
			
			//Energy Totals Final
			var totalTDCharge = energyMonthlyadjustments
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energySurchargePslSection18aAssessments + adjustmentFactorMAC
			+ energyMetercharges + energyMeterservicecharge + energyMeterdatacharge + energyBillingandpaymentprocessingsingleservice;
			var grtTDTaxCharge = totalTDCharge * mainSlice.getInputValue("taxTD")/100;
			
			var totalEnergyFuelUsageBeforeTax = totalCommodityCharge + totalTDCharge;
			var subTotalEnergyCharge = totalEnergyFuelUsageBeforeTax + grtCommodityTaxCharge + grtTDTaxCharge;

			//Demand value in Tier
			var demand900 = IIF(demand < 900,demand,900);
			var demandOver900 = IIF(demand900 > 900 ,demand - 900,0);
			
			//Demand Market Supply Charges (Tier Pricing)
			var demandMarketSupply900 = IIF(hasSupplyContract eq True,0,demand900 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMarketSupply"));
			var demandMarketSupplyOver900 = IIF(hasSupplyContract eq True,0,demandOver900 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMarketSupply"));

			var demandMarketSupplyTotal = demandMarketSupply900 + demandMarketSupplyOver900;

			//Demand Monthly Adjustments (Tier Pricing)
			var demandMonthlyAdj900 = IIF(hasSupplyContract eq True,0,demand900 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMonthlyAdjustments"));
			var demandMonthlyAdjOver900 = IIF(hasSupplyContract eq True,0,demandOver900 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMonthlyAdjustments"));
			var demandMonthlyAdjustmentsTotal = demandMonthlyAdj900 + demandMonthlyAdjOver900;
	
			
			//Demand Delivery Charges (Tier Pricing)			
			var demandDeliveryCharge900 = demand900 * mainSlice.getInputValue("demandDeliveryCharge900");
			var demandDeliveryChargeOver900 = demandOver900 * mainSlice.getInputValue("demandDeliveryChargeOver900");

			var demandDeliveryChargeTotal = demandDeliveryCharge900 + demandDeliveryChargeOver900;
			
			//Demand Totals
			var subTotalLowTensionDemandChargeSupply = demandMarketSupplyTotal;
			var subTotalLowTensionDemandChargeDelivery = demandMonthlyAdjustmentsTotal + demandDeliveryChargeTotal;
			
			var subTotalLowTensionDemandCharge	= subTotalLowTensionDemandChargeSupply + subTotalLowTensionDemandChargeDelivery;		
			

			//Pro-Rated Demand Totals
			var proratedTotalDemandSupplyCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeSupply);
			var proratedTotalDemandDeliveryCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeDelivery);
			var proratedTotalDemandCharge = proratedTotalDemandDeliveryCharge + proratedTotalDemandSupplyCharge;
			
			var demandGRTCommodityTaxCharge = proratedTotalDemandSupplyCharge * mainSlice.getInputValue("taxGRTCommodity")/100 ;
			var demandGRTTDTaxCharge = proratedTotalDemandCharge * mainSlice.getInputValue("taxTD")/100;
			var totalDemandSupplyChargeWithGRTTax = proratedTotalDemandSupplyCharge + demandGRTCommodityTaxCharge;
			var totalDemandDeliveryChargeWithGRTTax = proratedTotalDemandDeliveryCharge + demandGRTTDTaxCharge;
			
			var subTotalDemandCharge = totalDemandSupplyChargeWithGRTTax + totalDemandDeliveryChargeWithGRTTax;
			
			var totalDeliveryCharges = totalTDCharge + proratedTotalDemandCharge;
			var grossReceiptsTaxDelivery = grtTDTaxCharge + demandGRTTDTaxCharge;
			var finalSubTotalDelivery = totalDeliveryCharges + grossReceiptsTaxDelivery;
			var salesTaxDelivery = mainSlice.getinputvalue("taxSales") * finalSubTotalDelivery/100;
			var grandTotalDelivery = finalSubTotalDelivery + salesTaxDelivery;

			var totalSupplyCharges = totalCommodityCharge + proratedTotalDemandSupplyCharge;
			var grossReceiptsTaxSupply = grtCommodityTaxCharge + demandGRTCommodityTaxCharge;
			var finalSubTotalSupply = totalSupplyCharges + grossReceiptsTaxSupply;
			var salesTaxSupply = mainSlice.getinputvalue("taxSales") * finalSubTotalSupply/100;
			var grandTotalSupply = finalSubTotalSupply + salesTaxSupply;
			
			var totalBillingCharges = grandTotalSupply + grandTotalDelivery;
		// Results Output
			//Simple Supply Check - not very scientific
			result.addOutput("Supply Check","Has Supply Contract ?",hasSupplyContract,"boolean");
			result.addMoneyOutput("Supply Check","Total Cost $",IIF(hasSupplyContract eq True,mainSlice.energyUsage.total_cost + mainSlice.energyUsage.supplychargesfromsubaccount, mainSlice.energyUsage.total_cost));
			result.addMoneyOutput("Supply Check","T&D Charges $",IIF(hasSupplyContract eq True,mainSlice.energyUsage.total_cost, mainSlice.getEnergyUsageMetaValue("tdcharges")));
			result.addMoneyOutput("Supply Check","Supply Charges $",IIF(hasSupplyContract eq True,mainSlice.energyUsage.supplychargesfromsubaccount, mainSlice.getEnergyUsageMetaValue("supplycharges")));

			//Usage Info
			result.addOutput("Usage Info","Billing Period",period_start & ' to ' & period_end,"date");
			result.addOutput("Usage Info","Days in Billing Period",billingdays,"numeric");
			result.addOutput("Usage Info","Number of Timeslices in Billing Period",numslices-1,"numeric");
			result.addEnergyOutput("Usage Info","Total Consumption",recorded_value);
			result.addEnergyOutput("Usage Info","Demand",demand);
			result.addEnergyOutput("Usage Info","Demand (Tier 900 kW)",demand900);
			result.addEnergyOutput("Usage Info","Demand (Tier 900+ kW)",demandOver900);
			
			//Rates
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ny.conedison.nyc.9-I.supply.energy.MSC.kwh"),prorateInputValueUsingPortionOfTotalDaysPolicy("energyMarketsupply")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("energyMonthlyadjustments"),prorateInputValueUsingPortionOfTotalDaysPolicy("energyMonthlyadjustments")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("energyDeliverycharge"),prorateInputValueUsingPortionOfTotalDaysPolicy("energyDeliverycharge")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("energySystembenefits"),prorateInputValueUsingPortionOfTotalDaysPolicy("energySystembenefits")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("energyRenewableportfoliostandardprogram"),prorateInputValueUsingPortionOfTotalDaysPolicy("energyRenewableportfoliostandardprogram")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("energyMerchantfunctioncharge"),prorateInputValueUsingPortionOfTotalDaysPolicy("energyMerchantfunctioncharge")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ny.conedison.nyc.9-I.maccharges.revdecmecadj.kwh"),prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.maccharges.revdecmecadj.kwh"));
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ny.conedison.nyc.9-I.maccharges.delrevsur.kwh"),prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.maccharges.delrevsur.kwh")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ny.conedison.nyc.9-I.maccharges.psl18a.kwh"),prorateInputValueUsingPortionOfTotalDaysPolicy("ny.conedison.nyc.9-I.maccharges.psl18a.kwh")/100);

			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMSC"),adjMSC);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMAC"),adjMAC);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ny.conedison.nyc.9-I.supply.energy.MSC-I-adj.kwh"),adjMSCi);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ny.conedison.nyc.9-I.supply.energy.MSC-II-adj.kwh"),adjMSCii);

			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMACreconciliation"),adjMACreconciliation);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMACuncollectiblebillexpense"),adjMACuncollectiblebillexpense);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMACtransitionadjustment"),adjMACtransitionadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjRevenuedecouplingmechanismadjustment"),adjRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjDeliveryrevenuesurcharge"),adjDeliveryrevenuesurcharge);
						
			//Charges



			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyMarketsupply"),energyMarketsupply);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyMonthlyadjustments"),energyMonthlyadjustments);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyDeliverycharge"),energyDeliverycharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energySystembenefits"),energySystembenefits);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyRenewableportfoliostandardprogram"),energyRenewableportfoliostandardprogram);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyMerchantfunctioncharge"),energyMerchantfunctioncharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyRevenuedecouplingmechanismadjustment"),energyRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyDeliveryrevenuesurcharge"),energyDeliveryrevenuesurcharge);							
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energySurchargePslSection18aAssessments"),energySurchargePslSection18aAssessments);

			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Before Adjustments)",subTotalEnergyChargeBeforeAdjustments);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MSC)",adjustmentFactorMSC);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MAC)",adjustmentFactorMAC);
			result.addMoneyOutput("Energy Totals","Adjustment Factors (MSC + MAC)",adjustmentFactorTotal);
			result.addMoneyOutput("Energy Totals","Total Commodity Charge (Market Supply + MSC Adjustment + Merchant Function Charge)",totalCommodityCharge);
			result.addMoneyOutput("Energy Totals","GRT Commodity Tax Charge",grtCommodityTaxCharge);
			result.addMoneyOutput("Energy Totals","Energy Charges (Monthly Adjustments + Adjustment Factor (MAC) + Delivery Charge)",energyCharges);
			result.addMoneyOutput("Energy Totals","Total Delivery (Energy Only + MAC Adjustment)",totalEnergyDeliveryCharges);
			result.addMoneyOutput("Energy Totals","Total SBC/RPS Charge",totalSBCRPSCharge);
			result.addMoneyOutput("Energy Totals","Total T&D Charge (Delivery Charges + MAC Adjustment)",totalTDCharge);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyMetercharges"),energyMetercharges);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyMeterservicecharge"),energyMeterservicecharge);					
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyMeterdatacharge"),energyMeterdatacharge);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyBillingandpaymentprocessingsingleservice"),energyBillingandpaymentprocessingsingleservice);
			result.addMoneyOutput("Energy Totals","GRT T&D Tax Charge",grtTDTaxCharge);
			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Supply + Delivery w/Adjustments and Tax)",subTotalEnergyCharge);

			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryCharge900"), demandDeliveryCharge900);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryChargeOver900"), demandDeliveryChargeOver900);
			
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Supply)",subTotalLowTensionDemandChargeSupply);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Delivery)",subTotalLowTensionDemandChargeDelivery);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Charge",subTotalLowTensionDemandCharge);

			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Delivery Charge",proratedTotalDemandDeliveryCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Supply Charge",proratedTotalDemandSupplyCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Charge",proratedTotalDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT Commodity Tax Charge (Demand)",demandGRTCommodityTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT T&D Tax Charge (Demand)",demandGRTTDTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Supply Charge with GRT Tax",totalDemandSupplyChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Delivery Charge with GRT Tax",totalDemandDeliveryChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Sub-Total Demand Charge (Supply + Delivery w/Tax)",subTotalDemandCharge);
	
			result.addMoneyOutput("Delivery Totals","Total Delivery Charges",totalDeliveryCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Delivery)",grossReceiptsTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Delivery)",finalSubTotalDelivery);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Delivery)",salesTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Grand Total (Delivery)",grandTotalDelivery);
			
			result.addMoneyOutput("Delivery Totals","Total Supply Charges",totalSupplyCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Supply)",grossReceiptsTaxSupply);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Supply)",finalSubTotalSupply);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Supply)",salesTaxSupply);
			result.addMoneyOutput("Delivery Totals","Grand Total (Supply)",grandTotalSupply);
			
			result.addMoneyOutput("Grand Totals","Grand Total",totalBillingCharges);
			
			//Set Calculator Grand Total
			result.setTotal(totalBillingCharges);
			/*
			result.addOutput("Usage Info","Number of Slices",numslices,"string");
			result.addOutput("Usage Info","Consumption",recorded_value,"string");
			result.addOutput("Usage Info","Slice1 Billing Period",this.timeslices[1].periodStart & ' to ' & this.timeslices[1].periodEnd,"date");
			result.addOutput("Usage Info","Slice2 Billing Period",this.timeslices[2].periodStart & ' to ' & this.timeslices[2].periodEnd,"date");
			result.addOutput("Usage Info","Slice3 Billing Period",this.timeslices[3].periodStart & ' to ' & this.timeslices[3].periodEnd,"date");
			result.addOutput("Usage Info","energyUsage UID",mainSlice.energyUsage.energy_usage_uid,"string");
			result.setTotal(0); */
		</cfscript>

		<cfreturn result>
	</cffunction>

</cfcomponent>