<cfcomponent extends="com.mce.rate.calculator.impl.BaseSimpleThirtyDayProratingModelCalculator"
			 implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
				<!---cfreturn "adjDeliveryrevenuesurcharge,adjMAC,adjMACreconciliation,adjMACtransitionadjustment,adjMACuncollectiblebillexpense,adjMSC,adjMSCi,adjMSCii,adjPSL18a,adjRevenuedecouplingmechanismadjustment,demandGandTOnPeak,demandMarketsupplyOffPeak,demandMarketsupplyOnPeak,demandMonthlyadjustmentsOffOnPeak,demandPrimaryOnPeak,demandSecondaryAll,energyBillingandpaymentprocessingsingleservice,energyDeliverycharge,energyDeliveryrevenuesurcharge,energyMarketsupplyOffPeak,energyMarketsupplyOnPeak,energyMerchantfunctioncharge,energyMetercharges,energyMeterdatacharge,energyMeterservicecharge,energyMonthlyadjustmentsOffPeak,energyMonthlyadjustmentsOnPeak,energyRenewableportfoliostandardprogram,energyRevenuedecouplingmechanismadjustment,energySurchargePslSection18aAssessments,energySystembenefits,taxGRTCommodity,taxSales,taxTD"--->
				<!---TODO: see the real values for these elements and then remove them from here--->
				<!---cfreturn "demandMarketsupplyOnPeak,demandMarketsupplyOffPeak,demandMonthlyadjustmentsOffOnPeak,demandGandTOnPeak,demandPrimaryOnPeak,demandSecondaryAll,energyMarketsupplyOnPeak,energyMarketsupplyOffPeak,energyMonthlyadjustmentsOnPeak,energyMonthlyadjustmentsOffPeak"--->
				<cfreturn "">
	</cffunction>
	
	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "none">
	</cffunction>


	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();
			var numslices = ArrayLen(this.timeslices); //includes full period slice
			var mainSlice = this.timeslices[1]; // 1 is the full billing period slice

		// Input & Calculations
		
			//Usage Info
			var period_start = mainSlice.periodStart;
			var period_end = mainSlice.periodEnd;
			
			//var recorded_value = mainSlice.energyUsage.recorded_value;
			//var demand = mainSlice.getEnergyUsageMetaValue("demand");
			//var onPeakUsage = mainSlice.getEnergyUsageMetaValue("onpeak");
			
			//shows if we have a data structure from the UI
			var energyUsageSaved = IIF(StructKeyExists(this.params.inputValueOverrides,"mainSliceData"), False, True);
			
			//TODO: see how to get the data from UI only if it is there and if not to use the values from DB
			//The stupid coldfusion throws exception if there is no this.params.inputValueOverrides.mainSliceData even I don't need it,
			//maybe I should use something different than IIF but I can't use simple if because the great guys from Adobe need all the vars on the top of cfscript
			//so for now we always need to pass the values in this.params.inputValueOverrides.mainSliceData
						
			var recorded_value = this.params.inputValueOverrides.mainSliceData.usage;//IIF(energyUsageSaved, mainSlice.energyUsage.recorded_value, this.params.inputValueOverrides.mainSliceData.usage);//mainSlice.energyUsage.recorded_value; 
			var demand = this.params.inputValueOverrides.mainSliceData.demand;//IIF(energyUsageSaved, mainSlice.getEnergyUsageMetaValue("Demand"), this.params.inputValueOverrides.mainSliceData.demand);
			
			//var hasSupplyContract = IIF(energyUsageSaved, mainSlice.energyUsage.hasSupplyContract, IIF(this.params.inputValueOverrides.mainSliceData.fullService eq 1, True, False));
			var onPeakUsage = this.params.inputValueOverrides.mainSliceData.onpeak;
			
			var offPeakUsage = recorded_value - onPeakUsage; // mainSlice.getEnergyUsageMetaValue("OffPeak");
			var billingdays = mainSlice.numberOfDays;
			var hasSupplyContract = IIF(this.params.inputValueOverrides.mainSliceData.fullService eq 1, True, False);//False;//mainSlice.energyUsage.hasSupplyContract; //IIF(mainSlice.getEnergyUsageMetaValue("tdcharges") eq mainSlice.energyUsage.total_cost,True,False);
			
			var tripCode = this.params.inputValueOverrides.mainSliceData.tripCode;
			var energyMarketSupplyInputValue = "ConEd.NYC.EL9-II.Market.Supply.Energy";// & tripCode;
			
			var billableKVar = this.params.inputValueOverrides.mainSliceData.bkvar;
			
			//taxes
			var taxes = getTaxes(this.params.accountidentifier, period_start, period_end);
			
			var grtDelivery = taxes[1]/100;//mainSlice.getInputValue("ConEd.NYC.EL9-II.GRT.Delivery.Tax")/100;
			var grtCommodity = taxes[2]/100;//mainSlice.getInputValue("ConEd.NYC.EL9-II.GRT.Commodity.Tax")/100;
			var grtSales = taxes[3]/100;//mainSlice.getinputvalue("ConEd.NYC.EL9-II.Sales.Tax")/100;
			
			//Energy Charges
			var energyMarketsupplyOnPeak = IIF(hasSupplyContract eq False,0,onPeakUsage * prorateInputValueUsingPortionOfTotalDaysPolicy(energyMarketSupplyInputValue/*"energyMarketsupplyOnPeak"*/)/100);
			var energyMarketsupplyOffPeak = IIF(hasSupplyContract eq False,0,offPeakUsage * prorateInputValueUsingPortionOfTotalDaysPolicy(energyMarketSupplyInputValue/*"energyMarketsupplyOffPeak"*/)/100);
			var energyMonthlyadjustmentsOnPeak = onPeakUsage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Monthly.Adjustments"/*"energyMonthlyadjustmentsOnPeak"*/)/100;
			var energyMonthlyadjustmentsOffPeak = offPeakUsage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Monthly.Adjustments"/*"energyMonthlyadjustmentsOffPeak"*/)/100;
			var energyDeliverycharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Charge.Energy")/100;
			var energySystembenefits = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.System.Benefits")/100;
			var energyRenewableportfoliostandardprogram = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Renewable.Portfolio.Standard.Prog")/100;
			var energyMerchantfunctioncharge = IIF(hasSupplyContract eq False,0,recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Merchant.Function.Charge")/100);
			var energyRevenuedecouplingmechanismadjustment = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Revenue.Decoupling.Adj")/100;
			var energyDeliveryrevenuesurcharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Revenue.Surcharge")/100;
			var energySurchargePslSection18aAssessments = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.PSL.Sec18a")/100;

			var billableReactivePower = billableKVar * billingdays/30;
			var billableReactivePowerCharges = billableReactivePower * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Billable.Reactive.Power.Demand");
						
			//Adjustment Factors
			var adjMSCi = mainSlice.getInputValue("ConEd.NYC-EL9-II.MSC-I.Adjustment")/100;
			var adjMSCii = mainSlice.getInputValue("ConEd.NYC-EL9-II.MSC-II.Adjustment")/100;			
			var adjMSC = adjMSCi + adjMSCii;//mainSlice.getInputValue("adjMSC");

			var adjMACreconciliation = /*0.002694;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-II.MAC.Reconciliation.Adjustment")/100;
			var adjMACuncollectiblebillexpense = /*0.000076;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-II.MAC.Uncollectable.Bill.Expense")/100;
			var adjMACtransitionadjustment = /*-0.000064;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-II.MAC.Transition.Adjustment")/100;
			var adjRevenuedecouplingmechanismadjustment = /*-0.005471063;//vas*/ prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Revenue.Decoupling.Adj")/100;
			var adjDeliveryrevenuesurcharge = /*0;//vas*/ prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Revenue.Surcharge")/100;
			var adjPSL18a = /*0.003945;//vas*/ prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.PSL.Sec18a")/100;
			var adjMAC = adjMACreconciliation + adjMACuncollectiblebillexpense +  adjMACtransitionadjustment + adjRevenuedecouplingmechanismadjustment + adjDeliveryrevenuesurcharge;//vas?? doesn't add in the excel + adjPSL18a;//mainSlice.getInputValue("adjMAC");
			
			//Energy Subtotal
			var subTotalEnergyChargeBeforeAdjustments = energyMarketsupplyOnPeak + energyMarketsupplyOffPeak + energyMonthlyadjustmentsOnPeak
			+ energyMonthlyadjustmentsOffPeak + energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energyMerchantfunctioncharge + energyRevenuedecouplingmechanismadjustment + energyDeliveryrevenuesurcharge
			+ energySurchargePslSection18aAssessments;

			var adjustmentFactorMSC = IIF(hasSupplyContract eq False,0,recorded_value * adjMSC);
			var adjustmentFactorMAC = recorded_value * adjMAC;
			var adjustmentFactorTotal = adjustmentFactorMSC + adjustmentFactorMAC;

			var totalCommodityCharge = adjustmentFactorMSC + energyMarketsupplyOnPeak + energyMarketsupplyOffPeak 
			+ energyMerchantfunctioncharge;
			
			var grtCommodityTaxCharge = totalCommodityCharge * grtCommodity;
			
			var energyOnPeakCharges = energyMonthlyadjustmentsOnPeak + (adjMAC * onPeakUsage) + (onPeakUsage * (prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Charge.Energy")/100));
			var energyOffPeakCharges = energyMonthlyadjustmentsOffPeak + (adjMAC * offPeakUsage) + (offPeakUsage * (prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Charge.Energy")/100));
			var totalEnergyDeliveryCharges  = 	energyMonthlyadjustmentsOnPeak + energyMonthlyadjustmentsOffPeak + energyDeliverycharge 
			+ adjustmentFactorMAC;
			var totalSBCRPSCharge = energySystembenefits + energyRenewableportfoliostandardprogram;
			
			//Meter Charges
			var energyMetercharges = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("ConEd.NYC.EL9-II.Meter.Ownership.Charge"));
			var energyMeterservicecharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("ConEd.NYC.EL9-II.Meter.Service.Provider.Charge"));
			var energyMeterdatacharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("ConEd.NYC.EL9-II.Meter.Data.Charge"));//was only Data
			var energyBillingandpaymentprocessingsingleservice = mainSlice.getInputValue("ConEd.WST.EL9-I.Billing.Payment.Processing");
			
			//Energy Totals Final
			var totalTDCharge = energyMonthlyadjustmentsOnPeak + energyMonthlyadjustmentsOffPeak 
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energySurchargePslSection18aAssessments + billableReactivePowerCharges + adjustmentFactorMAC
			+ energyMetercharges + energyMeterservicecharge + energyMeterdatacharge + energyBillingandpaymentprocessingsingleservice;
			var grtTDTaxCharge = totalTDCharge * grtDelivery;
			
			var totalEnergyFuelUsageBeforeTax = totalCommodityCharge + totalTDCharge;
			var subTotalEnergyCharge = totalEnergyFuelUsageBeforeTax + grtCommodityTaxCharge + grtTDTaxCharge;

			//Demand Charges
			var demandMarketsupplyOnPeak = IIF(hasSupplyContract eq False,0,prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Market.Supply.Dmd"/*"demandMarketsupplyOnPeak"*/) * demand);
			var demandMarketsupplyOffPeak = IIF(hasSupplyContract eq False,0,prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Market.Supply.Dmd"/*"demandMarketsupplyOffPeak"*/) * demand);
			var demandMonthlyadjustmentsOffOnPeak = IIF(hasSupplyContract eq False,0, 0 /*prorateInputValueUsingPortionOfTotalDaysPolicy("demandMonthlyadjustmentsOffOnPeak")*/ * demand); //Demand monthly adjustments not used anymore
			var demandGandTOnPeak = prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Demand.8am.6pm") * demand;
			var demandPrimaryOnPeak = prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Demand.8am.10pm") * demand;
			var demandSecondaryAll = prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Delivery.Demand.All.Hours") * demand;
					
			//Demand Totals
			var subTotalLowTensionDemandChargeSupply = demandMarketsupplyOnPeak + demandMarketsupplyOffPeak;
			var subTotalLowTensionDemandChargeDelivery = demandMonthlyadjustmentsOffOnPeak + demandGandTOnPeak
			+ demandPrimaryOnPeak + demandSecondaryAll;
			var subTotalLowTensionDemandCharge	= subTotalLowTensionDemandChargeSupply + subTotalLowTensionDemandChargeDelivery;		
			
			var proratedGTDemandCharge = prorateUsingThirtyDayPolicy(demandGandTOnPeak);
			var proratedPrimaryDemandCharge = prorateUsingThirtyDayPolicy(demandPrimaryOnPeak);
			var proratedSecondaryDemandCharge = prorateUsingThirtyDayPolicy(demandSecondaryAll);
			var proratedTotalDemandDeliveryCharge = proratedGTDemandCharge + proratedPrimaryDemandCharge + proratedSecondaryDemandCharge;
			
			var proratedTotalDemandSupplyCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeSupply);
			var proratedTotalDemandCharge = proratedTotalDemandDeliveryCharge + proratedTotalDemandSupplyCharge;
			
			var demandGRTCommodityTaxCharge = proratedTotalDemandSupplyCharge * grtCommodity ;
			var demandGRTTDTaxCharge = proratedTotalDemandCharge * grtDelivery/100;
			var totalDemandSupplyChargeWithGRTTax = proratedTotalDemandSupplyCharge + demandGRTCommodityTaxCharge;
			var totalDemandDeliveryChargeWithGRTTax = proratedTotalDemandDeliveryCharge + demandGRTTDTaxCharge;
			
			var subTotalDemandCharge = totalDemandSupplyChargeWithGRTTax + totalDemandDeliveryChargeWithGRTTax;
			
			var totalDeliveryCharges = totalTDCharge + proratedTotalDemandDeliveryCharge;
			var grossReceiptsTaxDelivery = grtTDTaxCharge + demandGRTTDTaxCharge;
			var finalSubTotalDelivery = totalDeliveryCharges + grossReceiptsTaxDelivery;
			var salesTaxDelivery = /*4.5*finalSubTotalDelivery/100;*/grtSales * finalSubTotalDelivery;
			var grandTotalDelivery = finalSubTotalDelivery + salesTaxDelivery;

			var totalSupplyCharges = totalCommodityCharge + proratedTotalDemandSupplyCharge;
			var grossReceiptsTaxSupply = grtCommodityTaxCharge + demandGRTCommodityTaxCharge;
			var finalSubTotalSupply = totalSupplyCharges + grossReceiptsTaxSupply;
			var salesTaxSupply = grtSales * finalSubTotalSupply;
			var grandTotalSupply = finalSubTotalSupply + salesTaxSupply;
			
			var totalBillingCharges = grandTotalSupply + grandTotalDelivery;
		// Results Output
			/*
			//Energy charge calculations
			result.addEnergyOutput("A Test", "Market Supply on Peak", energyMarketsupplyOnPeak);
			result.addEnergyOutput("A Test", "Market Supply off Peak", energyMarketsupplyOffPeak);
			result.addEnergyOutput("A Test", "Monthly Adjustments on Peak", energyMonthlyadjustmentsOnPeak);
			result.addEnergyOutput("A Test", "Monthly Adjustments off Peak", energyMonthlyadjustmentsOffPeak);
			result.addEnergyOutput("A Test", "Delivery Charge", energyDeliverycharge);
			result.addEnergyOutput("A Test", "System Benefits", energySystembenefits);
			result.addEnergyOutput("A Test", "Renewable portfolio", energyRenewableportfoliostandardprogram);
			result.addEnergyOutput("A Test", "Merchant function charge", energyMerchantfunctioncharge);
			result.addEnergyOutput("A Test", "Revenue Decoupling", energyRevenuedecouplingmechanismadjustment);
			result.addEnergyOutput("A Test", "Delivery Revenue", energyDeliveryrevenuesurcharge);
			result.addEnergyOutput("A Test", "Surcharge PSL 18", energySurchargePslSection18aAssessments);
			result.addEnergyOutput("A Test", "Billable Reactive power", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Billable.Reactive.Power.Demand"));
			result.addEnergyOutput("A Test", "Billable Reactive power charges", billableReactivePowerCharges);
			
			result.addEnergyOutput("A Test", "Monthly adjustment rate", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Monthly.Adjustments")/100);
			result.addEnergyOutput("A Test", "MFC rate", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-II.Merchant.Function.Charge")/100);
			
			//result.addEnergyOutput("A Test", "adjMSCi", adjMSCi);
			//result.addEnergyOutput("A Test", "adjMSCii", adjMSCii);
			//result.addEnergyOutput("A Test", "adjMSC", adjMSC);
			
			//result.addEnergyOutput("A Test", "adjMACreconciliation", adjMACreconciliation);
			//result.addEnergyOutput("A Test", "adjMACuncollectiblebillexpense", adjMACuncollectiblebillexpense);
			//result.addEnergyOutput("A Test", "adjMACtransitionadjustment", adjMACtransitionadjustment);
			//result.addEnergyOutput("A Test", "adjRevenuedecouplingmechanismadjustment", adjRevenuedecouplingmechanismadjustment);
			//result.addEnergyOutput("A Test", "adjDeliveryrevenuesurcharge", adjDeliveryrevenuesurcharge);
			//result.addEnergyOutput("A Test", "adjPSL18a", adjPSL18a);
			result.addEnergyOutput("A Test", "adjMAC", adjMAC);
			result.addEnergyOutput("A Test", "Adjustment factor MSC", adjustmentFactorMSC);
			result.addEnergyOutput("A Test", "Adjustment factor MAC", adjustmentFactorMAC);
			result.addEnergyOutput("A Test", "Adjustment factor Total", adjustmentFactorTotal);
			result.addEnergyOutput("A Test", "totalCommodityCharge", totalCommodityCharge);
			result.addEnergyOutput("A Test", "grtCommodityTaxCharge", grtCommodityTaxCharge);
						
			result.addEnergyOutput("A Test", "energyOnPeakCharges", energyOnPeakCharges);
			result.addEnergyOutput("A Test", "energyOffPeakCharges", energyOffPeakCharges);
			
			result.addEnergyOutput("A Test", "totalEnergyDeliveryCharges", totalEnergyDeliveryCharges);
			result.addEnergyOutput("A Test", "ttotalSBCRPSCharge", totalSBCRPSCharge);
			
			result.addEnergyOutput("A Test", "energyMetercharges", energyMetercharges);
			result.addEnergyOutput("A Test", "energyMeterservicecharge", energyMeterservicecharge);
			result.addEnergyOutput("A Test", "energyMeterdatacharge", energyMeterdatacharge);
			result.addEnergyOutput("A Test", "energyBillingandpaymentprocessingsingleservice", energyBillingandpaymentprocessingsingleservice);
			
			result.addEnergyOutput("A Test", "totalTDCharge", totalTDCharge);
			result.addEnergyOutput("A Test", "totalEnergyFuelUsageBeforeTax", totalEnergyFuelUsageBeforeTax);
						
			result.addEnergyOutput("A Test", "subTotalEnergyCharge", subTotalEnergyCharge);
			*/
		/*		
			//LOW TENSION PRIMARY DEMAND
			result.addEnergyOutput("A Test", "demandMarketsupplyOnPeak", demandMarketsupplyOnPeak);
			result.addEnergyOutput("A Test", "demandMarketsupplyOffPeak", demandMarketsupplyOffPeak);
			result.addEnergyOutput("A Test", "demandMonthlyadjustmentsOffOnPeak", demandMonthlyadjustmentsOffOnPeak);
			result.addEnergyOutput("A Test", "demandGandTOnPeak", demandGandTOnPeak);
			
			result.addEnergyOutput("A Test", "demandPrimaryOnPeak", demandPrimaryOnPeak);
			result.addEnergyOutput("A Test", "demandSecondaryAll", demandSecondaryAll);
			
			result.addEnergyOutput("A Test", "subTotalLowTensionDemandChargeSupply", subTotalLowTensionDemandChargeSupply);
			result.addEnergyOutput("A Test", "subTotalLowTensionDemandChargeDelivery", subTotalLowTensionDemandChargeDelivery);
			result.addEnergyOutput("A Test", "subTotalLowTensionDemandCharge", subTotalLowTensionDemandCharge);
			
			result.addEnergyOutput("A Test", "proratedGTDemandCharge", proratedGTDemandCharge);
			result.addEnergyOutput("A Test", "proratedPrimaryDemandCharge", proratedPrimaryDemandCharge);
			result.addEnergyOutput("A Test", "proratedSecondaryDemandCharge", proratedSecondaryDemandCharge);
			result.addEnergyOutput("A Test", "proratedTotalDemandDeliveryCharge", proratedTotalDemandDeliveryCharge);
			
			result.addEnergyOutput("A Test", "proratedTotalDemandSupplyCharge", proratedTotalDemandSupplyCharge);
			result.addEnergyOutput("A Test", "proratedTotalDemandCharge", proratedTotalDemandCharge);
			
			result.addEnergyOutput("A Test", "demandGRTCommodityTaxCharge", demandGRTCommodityTaxCharge);
			result.addEnergyOutput("A Test", "demandGRTTDTaxCharge", demandGRTTDTaxCharge);
			
			result.addEnergyOutput("A Test", "totalDemandSupplyChargeWithGRTTax", totalDemandSupplyChargeWithGRTTax);
			result.addEnergyOutput("A Test", "totalDemandDeliveryChargeWithGRTTax", totalDemandDeliveryChargeWithGRTTax);
			
			result.addEnergyOutput("A Test", "subTotalDemandCharge", subTotalDemandCharge);
		*/
			result.addEnergyOutput("A Test", "ConEd.NYC.EL9-II.GRT.Commodity.Tax", grtCommodity);
			result.addEnergyOutput("A Test", "ConEd.NYC.EL9-II.GRT.Delivery.Tax", grtDelivery);		
			
			result.addEnergyOutput("A Test", "totalDeliveryCharges", totalDeliveryCharges);
			result.addEnergyOutput("A Test", "grossReceiptsTaxDelivery", grossReceiptsTaxDelivery);
			result.addEnergyOutput("A Test", "finalSubTotalDelivery", finalSubTotalDelivery);
			result.addEnergyOutput("A Test", "salesTaxDelivery", salesTaxDelivery);
			result.addEnergyOutput("A Test", "grandTotalDelivery", grandTotalDelivery);
			
			result.addEnergyOutput("A Test", "totalSupplyCharges", totalSupplyCharges);
			result.addEnergyOutput("A Test", "grossReceiptsTaxSupply", grossReceiptsTaxSupply);
			result.addEnergyOutput("A Test", "finalSubTotalSupply", finalSubTotalSupply);
			result.addEnergyOutput("A Test", "salesTaxSupply", salesTaxSupply);
			result.addEnergyOutput("A Test", "grandTotalSupply", grandTotalSupply);
			
			result.addEnergyOutput("A Test", "Sales Tax", grtSales);			
							
			//Simple Supply Check - not very scientific
			result.addOutput("Supply Check","Has Supply Contract ?",hasSupplyContract,"boolean");
			result.addMoneyOutput("Supply Check","Total Cost $",totalBillingCharges);//mainSlice.energyUsage.total_cost);
			result.addMoneyOutput("Supply Check","T&D Charges $",grandTotalDelivery);//mainSlice.getEnergyUsageMetaValue("tdcharges"));
			result.addMoneyOutput("Supply Check","Supply Charges $",grandTotalSupply);//mainSlice.getEnergyUsageMetaValue("supplycharges"));
			
			//Usage Info
			result.addOutput("Usage Info","Billing Period",period_start & ' to ' & period_end,"date");
			result.addOutput("Usage Info","Days in Billing Period",billingdays,"numeric");
			result.addOutput("Usage Info","Number of Timeslices in Billing Period",numslices-1,"numeric");
			result.addEnergyOutput("Usage Info","Total Consumption",recorded_value);
			result.addEnergyOutput("Usage Info","On-Peak",onPeakUsage);
			result.addEnergyOutput("Usage Info","Off-Peak",offPeakUsage);
			result.addEnergyOutput("Usage Info","Demand",demand);

			//Rates
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-II.Monthly.Adjustments"/*"energyMonthlyadjustmentsOffPeak"*/),mainSlice.getInputValue("ConEd.NYC.EL9-II.Monthly.Adjustments"/*"energyMonthlyadjustmentsOffPeak"*/));

			result.addMoneyOutput("Rate Info","adjMSC",adjMSC);
			result.addMoneyOutput("Rate Info","adjMAC",adjMAC);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC-EL9-II.MSC-I.Adjustment"),adjMSCi);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC-EL9-II.MSC-II.Adjustment"),adjMSCii);

			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-II.MAC.Reconciliation.Adjustment"),adjMACreconciliation);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-II.MAC.Uncollectable.Bill.Expense"),adjMACuncollectiblebillexpense);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-II.MAC.Transition.Adjustment"),adjMACtransitionadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-II.Revenue.Decoupling.Adj"),adjRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-II.Delivery.Revenue.Surcharge"),adjDeliveryrevenuesurcharge);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-II.PSL.Sec18a"),adjPSL18a);
						
			//Charges
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Meter.Service.Provider.Charge"),energyMeterservicecharge);					
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Meter.Data.Charge"),energyMetercharges);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Meter.Data.Charge"),energyMeterdatacharge);//was only data
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.WST.EL9-I.Billing.Payment.Processing"),energyBillingandpaymentprocessingsingleservice);
			
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Market.Supply.Demand"/*"demandMarketsupplyOnPeak"*/), demandMarketsupplyOnPeak);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Market.Supply.Demand"/*"demandMarketsupplyOffPeak"*/), demandMarketsupplyOffPeak);
			result.addMoneyOutput("Demand Charges",/*mainSlice.getInputName("demandMonthlyadjustmentsOffOnPeak")*/"demandMonthlyadjustmentsOffOnPeak", demandMonthlyadjustmentsOffOnPeak);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Delivery.Demand.8am.6pm"), demandGandTOnPeak);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Delivery.Demand.8am.10pm"), demandPrimaryOnPeak);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Delivery.Demand.All.Hours"), demandSecondaryAll);

			result.addMoneyOutput("Energy Charges",mainSlice.getInputName(energyMarketSupplyInputValue/*"energyMarketsupplyOnPeak"*/),energyMarketsupplyOnPeak);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName(energyMarketSupplyInputValue/*"energyMarketsupplyOffPeak"*/),energyMarketsupplyOffPeak);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Monthly.Adjustments"/*"energyMonthlyadjustmentsOnPeak"*/),energyMonthlyadjustmentsOnPeak);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Monthly.Adjustments"/*"energyMonthlyadjustmentsOffPeak"*/),energyMonthlyadjustmentsOffPeak);			
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Delivery.Charge.Energy"),energyDeliverycharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.System.Benefits"),energySystembenefits);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Renewable.Portfolio.Standard.Prog"),energyRenewableportfoliostandardprogram);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Merchant.Function.Charge"),energyMerchantfunctioncharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Revenue.Decoupling.Adj"),energyRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.Delivery.Revenue.Surcharge"),energyDeliveryrevenuesurcharge);							
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-II.PSL.Sec18a"),energySurchargePslSection18aAssessments);
			
			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Before Adjustments)",subTotalEnergyChargeBeforeAdjustments);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MSC)",adjustmentFactorMSC);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MAC)",adjustmentFactorMAC);
			result.addMoneyOutput("Energy Totals","Adjustment Factors (MSC + MAC)",adjustmentFactorTotal);
			result.addMoneyOutput("Energy Totals","Total Commodity Charge (Market Supply + MSC Adjustment + Merchant Function Charge)",totalCommodityCharge);
			result.addMoneyOutput("Energy Totals","GRT Commodity Tax Charge",grtCommodityTaxCharge);
			result.addMoneyOutput("Energy Totals","Energy On Peak Charges (Monthly Adjustments + Adjustment Factor (MAC) + Delivery Charge)",energyOnPeakCharges);
			result.addMoneyOutput("Energy Totals","Energy Off Peak Charges (Monthly Adjustments + Adjustment Factor (MAC) + Delivery Charge)",energyOffPeakCharges);
			result.addMoneyOutput("Energy Totals","Total Delivery (Energy Only + MAC Adjustment)",totalEnergyDeliveryCharges);
			result.addMoneyOutput("Energy Totals","Total SBC/RPS Charge",totalSBCRPSCharge);
			result.addMoneyOutput("Energy Totals","Total T&D Charge (Delivery Charges + MAC Adjustment)",totalTDCharge);
			result.addMoneyOutput("Energy Totals","GRT T&D Tax Charge",grtTDTaxCharge);
			result.addMoneyOutput("Energy Totals","Total Energy and Fuel Usage (Before Taxes)",totalEnergyFuelUsageBeforeTax);
			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Supply + Delivery w/Adjustments and Tax)",subTotalEnergyCharge);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Supply)",subTotalLowTensionDemandChargeSupply);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Delivery)",subTotalLowTensionDemandChargeDelivery);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Charge",subTotalLowTensionDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated G&T Demand Charge",proratedGTDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Primary Demand Charge",proratedPrimaryDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Secondary Demand Charge",proratedSecondaryDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Delivery Charge",proratedTotalDemandDeliveryCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Supply Charge",proratedTotalDemandSupplyCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Charge",proratedTotalDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT Commodity Tax Charge (Demand)",demandGRTCommodityTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT T&D Tax Charge (Demand)",demandGRTTDTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Supply Charge with GRT Tax",totalDemandSupplyChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Delivery Charge with GRT Tax",totalDemandDeliveryChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Sub-Total Demand Charge (Supply + Delivery w/Tax)",subTotalDemandCharge);
	
			result.addMoneyOutput("Delivery Totals","Total Delivery Charges",totalDeliveryCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Delivery)",grossReceiptsTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Delivery)",finalSubTotalDelivery);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Delivery)",salesTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Grand Total (Delivery)",grandTotalDelivery);
			
			result.addMoneyOutput("Delivery Totals","Total Supply Charges",totalSupplyCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Supply)",grossReceiptsTaxSupply);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Supply)",finalSubTotalSupply);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Supply)",salesTaxSupply);
			result.addMoneyOutput("Delivery Totals","Grand Total (Supply)",grandTotalSupply);
			
			result.addMoneyOutput("Grand Totals","Grand Total",totalBillingCharges);
			
			//Set Calculator Grand Total
			result.setTotal(totalBillingCharges);
		</cfscript>

		<cfreturn result>
	</cffunction>

</cfcomponent>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            