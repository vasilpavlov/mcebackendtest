<cfcomponent extends="com.mce.rate.calculator.impl.BaseSimpleThirtyDayProratingModelCalculator"
			 implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
				<!---cfreturn "ny.conedison.nyc.9-I.meteroth.billpayproc,ny.conedison.nyc.9-I.meteroth.billpayproc.riderM,ny.conedison.nyc.9-I.meteroth.metercharge,ny.conedison.nyc.9-I.meteroth.metercharge.riderM,ny.conedison.nyc.9-I.meteroth.meterdatacharge,ny.conedison.nyc.9-I.meteroth.meterdatacharge.riderM,ny.conedison.nyc.9-I.meteroth.metersvccharge,ny.conedison.nyc.9-I.meteroth.metersvccharge.riderM,ny.conedison.nyc.9-I.delivery.demand.100kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.5kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.5kw.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.5kw.trans.kw,ny.conedison.nyc.9-I.delivery.demand.895kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.895kw.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.895kw.trans.kw,ny.conedison.nyc.9-I.delivery.demand.900kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.900kw.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.900kw.trans.kw,ny.conedison.nyc.9-I.delivery.demand.95kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.first900kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.monthadj.kw,ny.conedison.nyc.9-I.delivery.demand.over900kw.dist.kw,ny.conedison.nyc.9-I.delivery.demand.trans.kw,ny.conedison.nyc.9-I.delivery.energy.dist.kwh,ny.conedison.nyc.9-I.delivery.energy.first15K.dist.kwh,ny.conedison.nyc.9-I.delivery.energy.mac.kwh,ny.conedison.nyc.9-I.delivery.energy.over15K.dist.kwh,ny.conedison.nyc.9-I.delivery.energy.trans.kwh,ny.conedison.nyc.9-I.sbcrps.rps.kwh,ny.conedison.nyc.9-I.sbcrps.sbc.kwh,ny.conedison.nyc.9-I.supply.demand.5kw.kw,ny.conedison.nyc.9-I.supply.demand.895kw.kw,ny.conedison.nyc.9-I.supply.demand.900kw.kw,ny.conedison.nyc.9-I.supply.energy.MFC.kwh,ny.conedison.nyc.9-I.supply.energy.MSC-I-adj.kwh,ny.conedison.nyc.9-I.supply.energy.MSC-II-adj.kwh,ny.conedison.nyc.9-I.supply.energy.MSC.kwh,ny.conedison.nyc.9-I.supply.energy.mscAdjDate.kwh,ny.conedison.nyc.9-I.macadj.kwh,ny.conedison.nyc.9-I.maccharges.delrevsur.kwh,ny.conedison.nyc.9-I.maccharges.psl18a.kwh,ny.conedison.nyc.9-I.maccharges.revdecmecadj.kwh,ny.conedison.nyc.9-I.delivery.grt,ny.conedison.nyc.9-I.fullservice.salestax,ny.conedison.nyc.9-I.retailaccess.salestax,ny.conedison.nyc.9-I.supply.grt"--->
				<cfreturn "">
	</cffunction>
	
	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "none">
	</cffunction>
	
	<!--- function for summer and non-summer calc --->
	<cffunction name="isSummerTime" access="private" returntype="boolean">
		<cfargument name="interval_start" type="date" required="true">
		<cfreturn ((month(interval_start) gte 6) and (day(interval_start) gte 1) and (month(interval_start) lte 9) and (day(interval_start) lte 30))>
	</cffunction>
	
	<cffunction name="myDump" access="package">
		<cfargument name="dumpVar" required="true" type="any">
		<cfdump 
    var = "#dumpVar#"
    expand = "yes" 
    format = "text"
    label = "text"
    metainfo = "yes"
    output = "C:\ColdFusion8\logs\vas_log9i.log">
	</cffunction>
	
	<cffunction name="throw" access="package">
		<cfargument name="msg" required="true" type="any">
		<cfthrow
				type="com.mcenergyinc.BackOffice.Property_no_trip_code"
				errorcode="PropertyNoTripCode"
				message="#msg#"
				detail="#msg#">
	</cffunction>

	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();
			var numslices = ArrayLen(this.timeslices); //includes full period slice
			var mainSlice = this.timeslices[1]; // 1 is the full billing period slice

		// Input & Calculations
		
			//Usage Info
			var period_start = mainSlice.periodStart;
			var period_end = mainSlice.periodEnd;
			
			var isSummer = IIF(isSummerTime(period_start) eq true, 1, 0);
			var notSummer = 1 - isSummer;
			
			//shows if we have a data structure from the UI
			var energyUsageSaved = IIF(StructKeyExists(this.params.inputValueOverrides,"mainSliceData"), False, True);
			
			//TODO: see how to get the data from UI only if it is there and if not to use the values from DB
			//The stupid coldfusion throws exception if there is no this.params.inputValueOverrides.mainSliceData even I don't need it,
			//maybe I should use something different than IIF but I can't use simple if because the great guys from Adobe need all the vars on the top of cfscript
			//so for now we always need to pass the values in this.params.inputValueOverrides.mainSliceData
			
			var recorded_value = this.params.inputValueOverrides.mainSliceData.usage;//IIF(energyUsageSaved, mainSlice.energyUsage.recorded_value, this.params.inputValueOverrides.mainSliceData.usage);//mainSlice.energyUsage.recorded_value; 
			//??
			var demand = IIF(this.params.inputValueOverrides.mainSliceData.demand < 5, 5, this.params.inputValueOverrides.mainSliceData.demand);//IIF(energyUsageSaved, mainSlice.getEnergyUsageMetaValue("Demand"), this.params.inputValueOverrides.mainSliceData.demand);
			
			var hasSupplyContract = IIF(this.params.inputValueOverrides.mainSliceData.fullService eq 1, True, False);//mainSlice.energyUsage.hasSupplyContract;//IIF(energyUsageSaved, mainSlice.energyUsage.hasSupplyContract, IIF(this.params.inputValueOverrides.mainSliceData.fullService eq 1, True, False));
			
			var tripCode = this.params.inputValueOverrides.mainSliceData.tripCode;
			var energyMarketSupplyInputValue = "ConEd.NYC.EL9-I.Market.Supply.Energy" & tripCode;
			
			var billingdays = mainSlice.numberOfDays;
			
			var billableKVar = this.params.inputValueOverrides.mainSliceData.bkvar;	
						
			//Energy Charges
			var energyMarketsupply = IIF(hasSupplyContract eq False,0, recorded_value */* 0.063201);//vas*/ prorateInputValueUsingPortionOfTotalDaysPolicy(energyMarketSupplyInputValue));
			var energyMonthlyadjustments = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments(MAC)")/100;
			var energyDeliverycharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Charge.Energy")/100;
			var energySystembenefits = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.System.Benefits")/100;
			var energyRenewableportfoliostandardprogram = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Renewable.Portfolio.Standard.Prog")/100;
			var energyMerchantfunctioncharge = IIF(hasSupplyContract eq False,0, recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Merchant.Function.Charge(MFC)")/100);
			var energyRevenuedecouplingmechanismadjustment = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Revenue.Decoupling.Adj")/100;
			var energyDeliveryrevenuesurcharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Revenue.Surcharge")/100;
			var energySurchargePslSection18aAssessments = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.PSL.Sec18")/100;

			var billableReactivePower = billableKVar * billingdays/30;
			var energyBillableReactivePowerChargesDemand = billableReactivePower * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Billable.Reactive.Power.Demand")/100;
			
			//Adjustment Factors
			var adjMSCi = /*-0.015359;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-I.MSC-I.Adjustment")/100;
			var adjMSCii = /*0.006525;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-I.MSC-II.Adjustment")/100;			
			var adjMSC = /*-0.000017;//vas_new*/ adjMSCi + adjMSCii;

			var adjMACreconciliation = /*-0.000505;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-I.MAC.Reconciliation.Adjustment")/100;
			var adjMACuncollectiblebillexpense = /*0.000087;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-I.MAC.Uncollectable.Bill.Expense")/100;
			var adjMACtransitionadjustment = /*-0.000064;//vas*/ mainSlice.getInputValue("ConEd.NYC.EL9-I.MAC.Transition.Adjustment")/100;
			var adjRevenuedecouplingmechanismadjustment = /*-0.003486;//vas*/ prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Revenue.Decoupling.Adj")/100;
			var adjDeliveryrevenuesurcharge = /*0;//vas*/ prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Revenue.Surcharge")/100;
			var adjPSL18a =  mainSlice.getInputValue("ConEd.NYC.EL9-I.PSL.Sec18");

			//adjMAC for 4-I & 9-I don't include adjPSL18a
			var adjMAC = /*0.002539;//vas_new*/ adjMACreconciliation + adjMACuncollectiblebillexpense +  adjMACtransitionadjustment + adjRevenuedecouplingmechanismadjustment + adjDeliveryrevenuesurcharge;
			
			//taxes
			var taxes = getTaxes(this.params.accountidentifier, period_start, period_end);
			
			var grtDelivery = taxes[1]/100;//5.1326/100;//vas_new mainSlice.getInputValue("ConEd.NYC.EL9-I.GRT.Delivery")/100;
			var grtCommodity = taxes[2]/100;//mainSlice.getInputValue("ConEd.NYC.EL9-I.GRT.Commodity")/100;
			var grtSales = taxes[3]/100;//mainSlice.getinputvalue("ConEd.NYC.EL9-I.Sales.Tax")/100;
			
			//Energy Subtotal
			var subTotalEnergyChargeBeforeAdjustments = energyMarketsupply + energyMonthlyadjustments
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energyMerchantfunctioncharge + energyRevenuedecouplingmechanismadjustment + energyDeliveryrevenuesurcharge
			+ energySurchargePslSection18aAssessments;

			var adjustmentFactorMSC = IIF(hasSupplyContract eq False,0,recorded_value * adjMSC);
			var adjustmentFactorMAC = recorded_value * adjMAC;
			var adjustmentFactorTotal = adjustmentFactorMSC + adjustmentFactorMAC;

			var totalCommodityCharge = adjustmentFactorMSC + energyMarketsupply 
			+ energyMerchantfunctioncharge;
			
			var grtCommodityTaxCharge = (totalCommodityCharge * grtCommodity);
			
			var energyCharges = energyMonthlyadjustments + (adjMAC * recorded_value) + (recorded_value * (prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Charge.Energy")/100));

			var totalEnergyDeliveryCharges  = 	energyMonthlyadjustments + energyDeliverycharge 
			+ adjustmentFactorMAC;
			var totalSBCRPSCharge = energySystembenefits + energyRenewableportfoliostandardprogram;
			
			//Meter Charges
			var energyMetercharges = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("ConEd.NYC.EL9-I.Meter.Ownership"/*"ConEd.NYC.EL9-I.Meter.Data.Charge(Non Rider M)"*/));
			var energyMeterservicecharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("ConEd.NYC.EL9-I.Meter.Service.Provider"));
			var energyMeterdatacharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("ConEd.NYC.EL9-I.Meter.Data"));
			var energyBillingandpaymentprocessingsingleservice = mainSlice.getInputValue("ConEd.NYC.EL9-I.Billing.Payment.Processing");
			
			//Energy Totals Final
			var totalTDCharge = energyMonthlyadjustments + energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energySurchargePslSection18aAssessments + adjustmentFactorMAC
			+ energyMetercharges + energyMeterservicecharge + energyMeterdatacharge + energyBillingandpaymentprocessingsingleservice
			+ energyBillableReactivePowerChargesDemand + energyRevenuedecouplingmechanismadjustment;
			var grtTDTaxCharge = totalTDCharge * grtDelivery;
			
			var totalEnergyFuelUsageBeforeTax = totalCommodityCharge + totalTDCharge;
			var subTotalEnergyCharge = totalEnergyFuelUsageBeforeTax + grtCommodityTaxCharge + grtTDTaxCharge;

			//Demand value in Tier
			//var demand900 = IIF(demand < 900,demand,900);
			//var demandOver900 = IIF(demand900 > 900 ,demand - 900,0);
			
			//Demand Market Supply Charges (Tier Pricing)
			//var demandMarketSupply900 = IIF(hasSupplyContract eq False,0,demand900 * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Market.Supply.Demand"));
			//var demandMarketSupplyOver900 = IIF(hasSupplyContract eq False,0,demandOver900 * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Market.Supply.Demand"));

			//var demandMarketSupplyTotal = demandMarketSupply900 + demandMarketSupplyOver900;

			//Demand Monthly Adjustments (Tier Pricing)
			//var demandMonthlyAdj900 = IIF(hasSupplyContract eq False,0,demand900 * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments"));
			//var demandMonthlyAdjOver900 = IIF(hasSupplyContract eq False,0,demandOver900 * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments"));
			//var demandMonthlyAdjustmentsTotal = demandMonthlyAdj900 + demandMonthlyAdjOver900;
	
			
			//Demand Delivery Charges (Tier Pricing)			
			//var demandDeliveryCharge900 = demand900 * mainSlice.getInputValue("demandDeliveryCharge900");
			//var demandDeliveryChargeOver900 = demandOver900 * mainSlice.getInputValue("demandDeliveryChargeOver900");

			//var demandDeliveryChargeTotal = demandDeliveryCharge900 + demandDeliveryChargeOver900;
			
			//Demand value in Tier
			var demand5kW = IIF(demand < 5,demand, 5);
			var demandNext95kW = IIF(demand5kW >= 5, IIF(demand < 100, demand - 5, 95), 0);
			var demandOver100kW = IIF(demandNext95kW >= 95, demand - 100, 0);
			
			//Demand Market Supply Charges (Tier Pricing)
			var demandMarketSupply5kW = IIF(hasSupplyContract eq False,0,demand5kW */* 10.60);//vas*/ prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Market.Supply.Dmd"));
			var demandMarketSupplyNext95kW = IIF(hasSupplyContract eq False,0,demandNext95kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Market.Supply.Dmd"));
			var demandMarketSupplyOver100kW = IIF(hasSupplyContract eq False,0,demandOver100kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Market.Supply.Dmd"));

			var demandMarketSupplyTotal = demandMarketSupply5kW + demandMarketSupplyNext95kW + demandMarketSupplyOver100kW;

			//Demand Monthly Adjustments (Tier Pricing) - not used anymore
			var demandMonthlyAdj5kW = IIF(hasSupplyContract eq False,0,demand5kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments.Demand"));
			var demandMonthlyAdjNext95kW = IIF(hasSupplyContract eq False,0,demandNext95kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments.Demand"));
			var demandMonthlyAdjOver100kW = IIF(hasSupplyContract eq False,0,demandOver100kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments.Demand"));
			
			var demandMonthlyAdjustmentsTotal = demandMonthlyAdj5kW + demandMonthlyAdjNext95kW + demandMonthlyAdjOver100kW;
				
			//Demand Delivery Charges (Tier Pricing)			
			var demandDeliveryCharge5kW = /*demand5kW **/ (isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delv.Dmd.First.5kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Del.Dmd.First.5kw.Non-Summer"));
			var demandDeliveryChargeNext95kW = demandNext95kW * (isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delv.Dmd.Nxt.95kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delv.Dmd.Nxt.95kw.Non-Summer"));
			var demandDeliveryChargeOver100kW = demandOver100kW * (isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delv.Dmd.Over.100kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delv.Dmd.Over.100kw.Non-Summer"));
			//var demandDeliveryCharge5kW = demand5kW * (isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Chrg.Dmd.1st.5.Kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Deliv.Chrg.Dmd.1st.5kw.Non-Summer"));
			//var demandDeliveryChargeNext95kW = demandNext95kW * (isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Chrg.Dmd.next95.Kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Deliv.Chrg.Dmd.nxt.95kw.Non-Summer"));
			//var demandDeliveryChargeOver100kW = demandOver100kW * (isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Chrg.Dmd.over100kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Deliv.Chrg.Dmd.over100kw.Non-Summe"));

			var demandDeliveryChargeTotal = demandDeliveryCharge5kW + demandDeliveryChargeNext95kW + demandDeliveryChargeOver100kW;
			
			//Demand Totals
			var subTotalLowTensionDemandChargeSupply = demandMarketSupplyTotal;
			var subTotalLowTensionDemandChargeDelivery = demandMonthlyAdjustmentsTotal + demandDeliveryChargeTotal;
			
			var subTotalLowTensionDemandCharge	= subTotalLowTensionDemandChargeSupply + subTotalLowTensionDemandChargeDelivery;		
			

			//Pro-Rated Demand Totals
			var proratedTotalDemandSupplyCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeSupply);
			var proratedTotalDemandDeliveryCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeDelivery);
			var proratedTotalDemandCharge = proratedTotalDemandDeliveryCharge + proratedTotalDemandSupplyCharge;
			
			var demandGRTCommodityTaxCharge = proratedTotalDemandSupplyCharge * grtCommodity;
			var demandGRTTDTaxCharge = proratedTotalDemandDeliveryCharge * grtDelivery;
			var totalDemandSupplyChargeWithGRTTax = proratedTotalDemandSupplyCharge + demandGRTCommodityTaxCharge;
			var totalDemandDeliveryChargeWithGRTTax = proratedTotalDemandDeliveryCharge + demandGRTTDTaxCharge;
			
			var subTotalDemandCharge = totalDemandSupplyChargeWithGRTTax + totalDemandDeliveryChargeWithGRTTax;
			
			var totalDeliveryCharges = totalTDCharge + proratedTotalDemandDeliveryCharge;
			var grossReceiptsTaxDelivery = grtTDTaxCharge + demandGRTTDTaxCharge;
			var finalSubTotalDelivery = totalDeliveryCharges + grossReceiptsTaxDelivery;
			var salesTaxDelivery = finalSubTotalDelivery * grtSales;
			var grandTotalDelivery = finalSubTotalDelivery + salesTaxDelivery;

			var totalSupplyCharges = totalCommodityCharge + proratedTotalDemandSupplyCharge;
			var grossReceiptsTaxSupply = grtCommodityTaxCharge + demandGRTCommodityTaxCharge;
			var finalSubTotalSupply = totalSupplyCharges + grossReceiptsTaxSupply;
			var salesTaxSupply = finalSubTotalSupply * grtSales;
			var grandTotalSupply = finalSubTotalSupply + salesTaxSupply;
			
			var totalBillingCharges = grandTotalSupply + grandTotalDelivery;
			
			if(tripCode <= 0)
				throw("The selected property doesn't have a TripCode!");
			
			//myDump(this.params.accountidentifier);
			
		// Results Output
		//Energy charge calculations
		/*
			result.addEnergyOutput("A Test", "Market Supply", energyMarketsupply);
			result.addEnergyOutput("A Test", "Monthly Adjustments", energyMonthlyadjustments);
			result.addEnergyOutput("A Test", "Delivery Charge", energyDeliverycharge);
			result.addEnergyOutput("A Test", "System Benefits", energySystembenefits);
			result.addEnergyOutput("A Test", "Renewable portfolio", energyRenewableportfoliostandardprogram);
			result.addEnergyOutput("A Test", "Merchant function charge", energyMerchantfunctioncharge);
			result.addEnergyOutput("A Test", "Revenue Decoupling", energyRevenuedecouplingmechanismadjustment);
			result.addEnergyOutput("A Test", "Delivery Revenue", energyDeliveryrevenuesurcharge);
			result.addEnergyOutput("A Test", "Surcharge PSL 18", energySurchargePslSection18aAssessments);
			result.addEnergyOutput("A Test", "Billable Reactive power", energyBillableReactivePowerChargesDemand);
			
			result.addEnergyOutput("A Test", "Monthly adjustment rate", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments(MAC)")/100);
			result.addEnergyOutput("A Test", "MFC rate", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Merchant.Function.Charge(MFC)")/100);
			
			result.addEnergyOutput("A Test", "adjMSCi", adjMSCi);
			result.addEnergyOutput("A Test", "adjMSCii", adjMSCii);
			result.addEnergyOutput("A Test", "adjMSC", adjMSC);
			
			result.addEnergyOutput("A Test", "adjMACreconciliation", adjMACreconciliation);
			result.addEnergyOutput("A Test", "adjMACuncollectiblebillexpense", adjMACuncollectiblebillexpense);
			result.addEnergyOutput("A Test", "adjMACtransitionadjustment", adjMACtransitionadjustment);
			result.addEnergyOutput("A Test", "adjRevenuedecouplingmechanismadjustment", adjRevenuedecouplingmechanismadjustment);
			result.addEnergyOutput("A Test", "adjDeliveryrevenuesurcharge", adjDeliveryrevenuesurcharge);
			result.addEnergyOutput("A Test", "adjPSL18a", adjPSL18a);
			
			result.addEnergyOutput("A Test", "adjMAC", adjMAC);
			result.addEnergyOutput("A Test", "Adjustment factor MSC", adjustmentFactorMSC);
			result.addEnergyOutput("A Test", "Adjustment factor MAC", adjustmentFactorMAC);
			result.addEnergyOutput("A Test", "Adjustment factor Total", adjustmentFactorTotal);
			result.addEnergyOutput("A Test", "totalCommodityCharge", totalCommodityCharge);
			result.addEnergyOutput("A Test", "grtCommodityTaxCharge", grtCommodityTaxCharge);
			result.addEnergyOutput("A Test", "subTotalEnergyCharge", subTotalEnergyCharge);
		*/	
		/*	
			result.addEnergyOutput("A Test", "total delivery charges (Energy Only + MAC)", energyMonthlyadjustments + energyDeliverycharge + adjustmentFactorMAC);
			result.addEnergyOutput("A Test", "Total SBC/RPS", energySystembenefits + energyRenewableportfoliostandardprogram);
			
			result.addEnergyOutput("A Test", "energyMetercharges", energyMetercharges);
			result.addEnergyOutput("A Test", "energyMeterservicecharge", energyMeterservicecharge);
			result.addEnergyOutput("A Test", "energyMeterdatacharge", energyMeterdatacharge);
			result.addEnergyOutput("A Test", "energyBillingandpaymentprocessingsingleservice", energyBillingandpaymentprocessingsingleservice);
			
			
			result.addEnergyOutput("A Test", "totalTDCharge", totalTDCharge);
			result.addEnergyOutput("A Test", "GRT.Delivery", grtDelivery);
			result.addEnergyOutput("A Test", "subTotalEnergyCharge", subTotalEnergyCharge);		
		*/	
		/*
			//LOW TENSION PRIMARY DEMAND
			result.addEnergyOutput("A Test", "demandMarketSupply5kW", demandMarketSupply5kW);
			result.addEnergyOutput("A Test", "demandMarketSupplyNext95kW", demandMarketSupplyNext95kW);
			result.addEnergyOutput("A Test", "demandMarketSupplyOver100kW", demandMarketSupplyOver100kW);
			result.addEnergyOutput("A Test", "demandMonthlyAdjustmentsTotal", demandMonthlyAdjustmentsTotal);
			
			result.addEnergyOutput("A Test", "demandDeliveryCharge5kW", demandDeliveryCharge5kW);
			result.addEnergyOutput("A Test", "demandDeliveryChargeNext95kW", demandDeliveryChargeNext95kW);
			result.addEnergyOutput("A Test", "demandDeliveryChargeOver100kW", demandDeliveryChargeOver100kW);
			
			result.addEnergyOutput("A Test", "demandDeliveryChargeTotal", demandDeliveryChargeTotal);
			result.addEnergyOutput("A Test", "subTotalLowTensionDemandCharge", subTotalLowTensionDemandCharge);
			
			result.addEnergyOutput("A Test", "totalDemandSupplyChargeWithGRTTax", totalDemandSupplyChargeWithGRTTax);
			result.addEnergyOutput("A Test", "totalDemandDeliveryChargeWithGRTTax", totalDemandDeliveryChargeWithGRTTax);
			result.addEnergyOutput("A Test", "subTotalDemandCharge", subTotalDemandCharge);
			
			result.addEnergyOutput("A Test", "proratedTotalDemandDeliveryCharge", proratedTotalDemandDeliveryCharge);
			result.addEnergyOutput("A Test", "demandGRTTDTaxCharge", demandGRTTDTaxCharge);
		*/	
			//result.addEnergyOutput("A Test", "proratedTotalDemandDeliveryCharge", proratedTotalDemandDeliveryCharge);
			//result.addEnergyOutput("A Test", "GRT.Delivery", grtDelivery);
		
			result.addEnergyOutput("A Test", "totalDeliveryCharges", totalDeliveryCharges);
			result.addEnergyOutput("A Test", "grossReceiptsTaxDelivery", grossReceiptsTaxDelivery);
			result.addEnergyOutput("A Test", "finalSubTotalDelivery", finalSubTotalDelivery);
			result.addEnergyOutput("A Test", "salesTaxDelivery", salesTaxDelivery);
			result.addEnergyOutput("A Test", "grandTotalDelivery", grandTotalDelivery);
			
			result.addEnergyOutput("A Test", "totalSupplyCharges", totalSupplyCharges);
			result.addEnergyOutput("A Test", "grossReceiptsTaxSupply", grossReceiptsTaxSupply);
			result.addEnergyOutput("A Test", "finalSubTotalSupply", finalSubTotalSupply);
			result.addEnergyOutput("A Test", "salesTaxSupply", salesTaxSupply);
			result.addEnergyOutput("A Test", "grandTotalSupply", grandTotalSupply);
					
			
			/*		
			result.addEnergyOutput("A Test", "energyMarketsupply", energyMarketsupply);
			result.addEnergyOutput("A Test", "demandMarketSupplyTotal", demandMarketSupplyTotal);
			result.addEnergyOutput("A Test", "energyDeliverycharge", energyDeliverycharge);
			result.addEnergyOutput("A Test", "demandDeliveryChargeTotal", demandDeliveryChargeTotal);
			result.addEnergyOutput("A Test", "totalSBCRPSCharge", totalSBCRPSCharge);
			result.addEnergyOutput("A Test", "meterCharges", energyMetercharges + energyMeterservicecharge + energyMeterdatacharge + energyBillingandpaymentprocessingsingleservice);
			result.addEnergyOutput("A Test", "otherCharges", energyMonthlyadjustments + energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram + energySurchargePslSection18aAssessments + adjustmentFactorMAC);
		
			result.addEnergyOutput("A Test", "GRTTaxesCharges", grossReceiptsTaxDelivery);
			result.addEnergyOutput("A Test", "finalSubTotalDelivery", finalSubTotalDelivery);
			result.addEnergyOutput("A Test", "salesTaxDelivery", salesTaxDelivery);
			*/
			//Simple Supply Check - not very scientific
			result.addOutput("Supply Check","Has Supply Contract ?",hasSupplyContract,"boolean");
			result.addMoneyOutput("Supply Check","Total Cost $",totalBillingCharges);//IIF(hasSupplyContract eq False,mainSlice.energyUsage.total_cost + mainSlice.energyUsage.supplychargesfromsubaccount, mainSlice.energyUsage.total_cost));
			result.addMoneyOutput("Supply Check","T&D Charges $",grandTotalDelivery);//IIF(hasSupplyContract eq False,mainSlice.energyUsage.total_cost, mainSlice.getEnergyUsageMetaValue("tdcharges")));
			result.addMoneyOutput("Supply Check","Supply Charges $",grandTotalSupply);//IIF(hasSupplyContract eq False,mainSlice.energyUsage.supplychargesfromsubaccount, mainSlice.getEnergyUsageMetaValue("supplycharges")));

			//Usage Info
			result.addOutput("Usage Info","Billing Period",period_start & ' to ' & period_end,"date");
			result.addOutput("Usage Info","Days in Billing Period",billingdays,"numeric");
			result.addOutput("Usage Info","Number of Timeslices in Billing Period",numslices-1,"numeric");
			result.addEnergyOutput("Usage Info","Total Consumption",recorded_value);
			result.addEnergyOutput("Usage Info","Demand",demand);
			result.addEnergyOutput("Usage Info","Demand (Tier 5 kW)",demand5kW);
			result.addEnergyOutput("Usage Info","Demand (Tier next 95 kW)",demandNext95kW);
			
			//Rates
			result.addMoneyOutput("Rate Info",mainSlice.getInputName(energyMarketSupplyInputValue),prorateInputValueUsingPortionOfTotalDaysPolicy(energyMarketSupplyInputValue));
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.Monthly.Adjustments(MAC)"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Monthly.Adjustments(MAC)")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Charge.Energy"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Charge.Energy")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.System.Benefits"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.System.Benefits")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.Renewable.Portfolio.Standard.Prog"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Renewable.Portfolio.Standard.Prog")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.Merchant.Function.Charge(MFC)"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Merchant.Function.Charge(MFC)")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.MAC.Reconciliation.Adjustment"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.MAC.Reconciliation.Adjustment"));
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Revenue.Surcharge"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.Delivery.Revenue.Surcharge")/100);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.PSL.Sec18"),prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.NYC.EL9-I.PSL.Sec18")/100);

			result.addMoneyOutput("Rate Info","adjMSC",adjMSC);
			result.addMoneyOutput("Rate Info","adjMAC",adjMAC);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.MSC-I.Adjustment"),adjMSCi);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.MSC-II.Adjustment"),adjMSCii);

			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.MAC.Reconciliation.Adjustment"),adjMACreconciliation);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.MAC.Uncollectable.Bill.Expense"),adjMACuncollectiblebillexpense);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.MAC.Transition.Adjustment"),adjMACtransitionadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.Revenue.Decoupling.Adj"),adjRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Revenue.Surcharge"),adjDeliveryrevenuesurcharge);
						
			//Charges
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName(energyMarketSupplyInputValue),energyMarketsupply);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Monthly.Adjustments(MAC)"),energyMonthlyadjustments);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Charge.Energy"),energyDeliverycharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.System.Benefits"),energySystembenefits);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Renewable.Portfolio.Standard.Prog"),energyRenewableportfoliostandardprogram);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Merchant.Function.Charge(MFC)"),energyMerchantfunctioncharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Revenue.Decoupling.Adj"),energyRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Revenue.Surcharge"),energyDeliveryrevenuesurcharge);							
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.PSL.Sec18"),energySurchargePslSection18aAssessments);

			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Before Adjustments)",subTotalEnergyChargeBeforeAdjustments);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MSC)",adjustmentFactorMSC);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MAC)",adjustmentFactorMAC);
			result.addMoneyOutput("Energy Totals","Adjustment Factors (MSC + MAC)",adjustmentFactorTotal);
			result.addMoneyOutput("Energy Totals","Total Commodity Charge (Market Supply + MSC Adjustment + Merchant Function Charge)",totalCommodityCharge);
			result.addMoneyOutput("Energy Totals","GRT Commodity Tax Charge",grtCommodityTaxCharge);
			result.addMoneyOutput("Energy Totals","Energy Charges (Monthly Adjustments + Adjustment Factor (MAC) + Delivery Charge)",energyCharges);
			result.addMoneyOutput("Energy Totals","Total Delivery (Energy Only + MAC Adjustment)",totalEnergyDeliveryCharges);
			result.addMoneyOutput("Energy Totals","Total SBC/RPS Charge",totalSBCRPSCharge);
			result.addMoneyOutput("Energy Totals","Total T&D Charge (Delivery Charges + MAC Adjustment)",totalTDCharge);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Meter.Data.Charge(Non Rider M)"),energyMetercharges);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Meter.Service.Provider"),energyMeterservicecharge);					
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Meter.Data"),energyMeterdatacharge);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Billing.Payment.Processing"),energyBillingandpaymentprocessingsingleservice);
			result.addMoneyOutput("Energy Totals","GRT T&D Tax Charge",grtTDTaxCharge);
			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Supply + Delivery w/Adjustments and Tax)",subTotalEnergyCharge);

			//result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryCharge900"), demandDeliveryCharge900);
			//result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryChargeOver900"), demandDeliveryChargeOver900);
			
			result.addMoneyOutput("Demand Charges",IIF(isSummer eq 1,"'ConEd.NYC.EL9-I.Delv.Dmd.First.5kw.Summer'", "'ConEd.NYC.EL9-I.Del.Dmd.First.5kw.Non-Summer'"), demandDeliveryCharge5kW);
			result.addMoneyOutput("Demand Charges",IIF(isSummer eq 1, "'ConEd.NYC.EL9-I.Delv.Dmd.Nxt.95kw.Summer'", "'ConEd.NYC.EL9-I.Delv.Dmd.Nxt.95kw.Non-Summer'"), demandDeliveryChargeNext95kW);
			result.addMoneyOutput("Demand Charges",IIF(isSummer eq 1, "'ConEd.NYC.EL9-I.Delv.Dmd.Over.100kw.Summer'", "'ConEd.NYC.EL9-I.Delv.Dmd.Over.100kw.Non-Summer'"), demandDeliveryChargeOver100kW);
			//result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Chrg.Dmd.1st.5.Kw.Summer"), demandDeliveryCharge5kW);
			//result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Chrg.Dmd.next95.Kw.Summer"), demandDeliveryChargeNext95kW);
			//result.addMoneyOutput("Demand Charges",mainSlice.getInputName("ConEd.NYC.EL9-I.Delivery.Chrg.Dmd.over100kw.Summer"), demandDeliveryChargeOver100kW);
			
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Supply)",subTotalLowTensionDemandChargeSupply);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Delivery)",subTotalLowTensionDemandChargeDelivery);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Charge",subTotalLowTensionDemandCharge);

			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Delivery Charge",proratedTotalDemandDeliveryCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Supply Charge",proratedTotalDemandSupplyCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Charge",proratedTotalDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT Commodity Tax Charge (Demand)",demandGRTCommodityTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT T&D Tax Charge (Demand)",demandGRTTDTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Supply Charge with GRT Tax",totalDemandSupplyChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Delivery Charge with GRT Tax",totalDemandDeliveryChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Sub-Total Demand Charge (Supply + Delivery w/Tax)",subTotalDemandCharge);
	
			result.addMoneyOutput("Delivery Totals","Total Delivery Charges",totalDeliveryCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Delivery)",grossReceiptsTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Delivery)",finalSubTotalDelivery);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Delivery)",salesTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Grand Total (Delivery)",grandTotalDelivery);
			
			result.addMoneyOutput("Delivery Totals","Total Supply Charges",totalSupplyCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Supply)",grossReceiptsTaxSupply);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Supply)",finalSubTotalSupply);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Supply)",salesTaxSupply);
			result.addMoneyOutput("Delivery Totals","Grand Total (Supply)",grandTotalSupply);
			
			result.addMoneyOutput("Grand Totals","Grand Total",totalBillingCharges);
			
			//Set Calculator Grand Total
			result.setTotal(totalBillingCharges);
			/*
			result.addOutput("Usage Info","Number of Slices",numslices,"string");
			result.addOutput("Usage Info","Consumption",recorded_value,"string");
			result.addOutput("Usage Info","Slice1 Billing Period",this.timeslices[1].periodStart & ' to ' & this.timeslices[1].periodEnd,"date");
			result.addOutput("Usage Info","Slice2 Billing Period",this.timeslices[2].periodStart & ' to ' & this.timeslices[2].periodEnd,"date");
			result.addOutput("Usage Info","Slice3 Billing Period",this.timeslices[3].periodStart & ' to ' & this.timeslices[3].periodEnd,"date");
			result.addOutput("Usage Info","energyUsage UID",mainSlice.energyUsage.energy_usage_uid,"string");
			result.setTotal(0); */
		</cfscript>

		<cfreturn result>
	</cffunction>

</cfcomponent>