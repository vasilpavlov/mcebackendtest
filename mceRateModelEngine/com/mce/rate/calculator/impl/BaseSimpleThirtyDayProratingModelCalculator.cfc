<cfcomponent
	extends="com.mce.rate.calculator.impl.BaseModelCalculator"
	implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">


	<cffunction name="getTimesliceAdapters" access="public" returntype="array">
		<cfset var monthPortionAdapter = CreateObject("component", "mceRateModelEngine.com.mce.rate.timeslice.SimplePortionsOfMonthTimesliceAdapter")>
		<cfset var inputValueAdapter = CreateObject("component", "mceRateModelEngine.com.mce.rate.timeslice.SimpleInputValuePeriodTimesliceAdapter")>
		<cfset var result = [monthPortionAdapter,inputValueAdapter]>
		<cfreturn result>
	</cffunction>


	<cffunction name="prorateInputValueUsingPortionOfTotalDaysPolicy" returntype="numeric">
		<cfargument name="lookupCode" type="string" required="true">

		<cfset var i = "">
		<cfset var slice = "">
		<cfset var result = 0>
		<cfset var totalNumberOfDays = this.timeslices[1].numberOfDays>

		<!--- For however many timeslices there are, not including the "main" slice --->
		<!--- (we are starting at position 2 to skip that first slice which in CF is at position 1) --->
		<cfloop from="2" to="#ArrayLen(this.timeslices)#" index="i">

			<!--- Here is the value of the input as of this timeslice --->
			<cfset slice = this.timeslices[i]>

			<!--- This style of prorating always divides each partial calendar month's value by the total number of days --->
			<cfset result = result + (slice.getInputValue(lookupCode) * (slice.numberOfDays / totalNumberOfDays))>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="prorateUsingThirtyDayPolicy">
		<cfargument name="val" type="numeric" required="true">
		<cfreturn val * (this.timeslices[1].numberOfDays / 30)>
	</cffunction>
	
	<cffunction name="getTaxes" access="private" returntype="array">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		
		<cfset var accountDelegate = CreateObject("component", "com.mce.db.EnergyAccountDelegate")>

		<cfset var taxArr = accountDelegate.getTaxesForAccount(account_identifier, period_start, period_end)>
		
		<cfif taxArr[1] eq '' or taxArr[2] eq '' or taxArr[3] eq ''>
			<cfthrow
				type="com.mcenergyinc.BackOffice.NoTaxFactorsValues"
				errorcode="NoTaxFactorsValues"
				message="There is no Tax Factor Values for the selected property and period."
				detail="Please check if there is postal code associated with the property and tax factors for the postal code. Also if there are factor values for the selected  period.">
		</cfif>
		
		<cfreturn taxArr>
	</cffunction>

</cfcomponent>