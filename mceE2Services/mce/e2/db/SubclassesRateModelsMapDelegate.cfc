
<!---

  Template Name:  SubclassesRateModelsMap
     Base Table:  SubclassesRateModelsMaps	

abe@twintechs.com

       Author:  Ilian Mihaylov 
 Date Created:  Thu, May 30, 2013
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thu, May 30, 2013 - Template Created.		

--->
		
<cfcomponent displayname="SubclassesRateModelsMapDelegate"
				hint="This CFC manages all data access interactions for the SubclassesRateModelsMaps table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptySubclassesRateModelsMapComponent"
				hint="This method is used to retrieve a Energy Provider Rate Class object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.SubclassesRateModelsMap"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.SubclassesRateModelsMap")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getSubclassesRateModelsMapsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Provider Rate Class objects for all the Energy Provider Rate Classes in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_provider_uid" required="false" type="string" hint="Describes the primary key of the associated energy provider."/>
		<cfargument name="rate_class" required="false" type="string" hint="Describes the primary key of the associated energy provider."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Provider Rate Classes and build out the array of components --->
		<cfset local.qResult = getSubclassesRateModelsMap(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.SubclassesRateModelsMap")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getSubclassesRateModelsMapAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Provider Rate Class value object representing a single Energy Provider Rate Class record."
				output="false"
				returnType="mce.e2.vo.SubclassesRateModelsMap"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="rate_class_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given rate class."/>
		<cfargument name="energy_provider_uid" required="true" type="string" hint="Describes the primary key of the associated energy provider."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Provider Rate Class and build out the component --->
		<cfset local.qResult = getSubclassesRateModelsMap(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.SubclassesRateModelsMap")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getSubclassesRateModelsMap"
				hint="This method is used to retrieve single / multiple records from the SubclassesRateModelsMaps table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="rate_class" required="false" type="string" hint="Describes the primary key / unique identifier for a given rate class."/>
		<cfargument name="subclass" required="false" type="string" hint="Describes the primary key / unique identifier for a given rate class."/>
		<cfargument name="rate_model_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given rate class."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given rate class."/>
		<cfargument name="modelname" required="false" type="string" hint="Describes the external / customer facing name for a given rate class."/>
		<cfargument name="provider_type_uid" required="false" type="string" hint="Describes the primary key of the associated energy provider."/>
		<cfargument name="rate_class_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given rate class."/>
		<cfargument name="native_rate_class_code" required="false" type="string" hint="Describes the internal identifier describing the rate class code for a given rate class."/>
		<cfargument name="energy_provider_uid" required="false" type="string" hint="Describes the primary key of the associated energy provider."/>
		<cfargument name="distinct" required="false" type="string" hint="Describes the primary key / unique identifier for a given rate class."/>
		<cfargument name="id" required="false" type="numeric" hint="Describes the primary key "/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Provider Rate Class records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
			select	
				Distinct
				<cfif structKeyExists(arguments, 'rate_class')>
				tbl.class as rate_class,
				tbl.subclass,
				tbl.rate_model_uid,
				tbl.friendly_name,
				tbl.provider_type_uid,  
				tbl.modelname,
				tbl.rate_class_uid,
				tbl.native_rate_class_code,
				tbl.energy_provider_uid,
				tbl.id
				<cfelse>
					tbl.class as rate_class
				</cfif>
			from	dbo.SubclassesRateModelsMap tbl (nolock)
			where	1=1 and tbl.is_active = 1
				<cfif structKeyExists(arguments, 'id')>
				and tbl.id in ( <cfqueryparam cfsqltype="cf_sql_int" value="#arguments.id#" list="true" null="false"> ) 
				</cfif>
				
				<cfif structKeyExists(arguments, 'rate_class')>
				and tbl.class in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rate_class#" list="true" null="false"> ) 
				</cfif>
				
				<cfif structKeyExists(arguments, 'rate_class_uid')>
				and tbl.rate_class_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.rate_class_uid#" list="true" null="false"> ) 
				</cfif>

				<cfif structKeyExists(arguments, 'provider_type_uid')>
				and tbl.provider_type_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.provider_type_uid#" list="true" null="false"> ) 
				</cfif>


				<cfif structKeyExists(arguments, 'friendly_name')>
				and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
				</cfif>

				<cfif structKeyExists(arguments, 'energy_provider_uid')>
				and tbl.energy_provider_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_provider_uid#" null="false" list="true"> ) 
				</cfif>

				<cfif structKeyExists(arguments, 'native_rate_class_code')>
				and tbl.native_rate_class_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.native_rate_class_code#" null="false" list="true"> ) 
				</cfif>

			order by
				tbl.class
			<cfif structKeyExists(arguments, 'rate_class')>		
				, tbl.subclass 
	 		</cfif>

		</cfquery>
		
		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Provider Rate Class data. --->	
	<cffunction name="saveNewSubclassesRateModelsMap"
				hint="This method is used to persist a new Energy Provider Rate Class record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="SubclassesRateModelsMap" required="true" type="mce.e2.vo.SubclassesRateModelsMap" hint="Describes the VO containing the Energy Provider Rate Class record that will be persisted."/>

		<!--- Persist the Energy Provider Rate Class data to the application database --->
		<cfset this.dataMgr.insertRecord("SubclassesRateModelsMaps", arguments.SubclassesRateModelsMap)/>

	</cffunction>

	<cffunction name="saveExistingSubclassesRateModelsMap"
				hint="This method is used to persist an existing Energy Provider Rate Class record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="SubclassesRateModelsMap" required="true" type="mce.e2.vo.SubclassesRateModelsMap" hint="Describes the VO containing the Energy Provider Rate Class record that will be persisted."/>

		<!--- Persist the Energy Provider Rate Class data to the application database --->
		<cfset this.dataMgr.updateRecord("SubclassesRateModelsMaps", arguments.SubclassesRateModelsMap)/>

	</cffunction>

</cfcomponent>
