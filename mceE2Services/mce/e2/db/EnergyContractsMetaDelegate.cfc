<cfcomponent displayname="EnergyContractsMetaDelegate"
				hint="This CFC manages all data access interactions for the EnergyContractsMeta table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyContractsMetaComponent"
				hint="This method is used to retrieve a Energy Contracts Meta Data object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyContractsMeta"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyContractsMeta")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyContractsMetaDataAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Contracts Meta Data objects for all the Energy Contracts Metas Data in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_contract_meta_uid" required="false" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record."/>
		<cfargument name="energy_contract_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property related to a given meta data record."/>
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the meta type related to a given property / meta data record."/>
		<cfargument name="relationship_filter_date" required="true" type="date" default="#createOdbcDateTime(now())#" hint="Describes the date used to filter active records."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Contracts Metas Data and build out the array of components --->
		<cfset local.qResult = getEnergyContractsMetaData(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyContractsMeta")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="applyChoicesToEnergyContractsMeta" access="public">
		<cfargument name="metaFields" type="array" required="true">

		<cfset var qChoices = "">
		<cfset var thisMeta = "">

		<cfloop array="#metaFields#" index="thisMeta">
			<cfif thisMeta.numMetaChoices gt 0>
				<cfset qChoices = getEnergyContractsMetaChoices(thisMeta.meta_type_uid)>
				<cfset thisMeta.metaChoices = queryToVoArray(qChoices, "mce.e2.vo.EnergyContractsMetaChoice")>
			<cfelse>
				<cfset thisMeta.metaChoices = ArrayNew(1)>
			</cfif>
		</cfloop>

	</cffunction>

	<cffunction name="getEnergyContractsMetaChoices" returntype="query" access="public">
		<cfargument name="meta_type_uid" required="true" type="string">
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT
				*
			FROM
				EnergyContractsMetaChoices
			WHERE
				meta_type_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.meta_type_uid#" list="true">)
			ORDER BY
				display_order,
				friendly_name
		</cfquery>

		<cfreturn q>
	</cffunction>

	<cffunction name="getEnergyContractsMetaDataAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Contracts Meta Data value object representing a single Energy Contracts Meta Data record."
				output="false"
				returnType="mce.e2.vo.EnergyContractsMeta"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_contract_meta_uid" required="false" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record."/>
		<cfargument name="energy_contract_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property related to a given meta data record."/>
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the meta type related to a given property / meta data record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Contracts Meta Data and build out the component --->
		<cfset local.qResult = getEnergyContractsMetaData(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyContractsMeta")/>
		<cfset qChoices = getEnergyContractsMetaChoices(local.returnResult.meta_type_uid)>
		<cfset local.returnResult.metaChoices = queryToVoArray(qChoices, "mce.e2.vo.EnergyContractsMetaChoice")>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyContractsMetaData"
				hint="This method is used to retrieve single / multiple records from the EnergyContractsMeta table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_contract_meta_uid" required="false" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record."/>
		<cfargument name="energy_contract_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property related to a given meta data record."/>
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the meta type related to a given property / meta data record."/>
		<cfargument name="relationship_filter_date" required="true" type="date" default="#createOdbcDateTime(now())#" hint="Describes the date used to filter active records."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Contracts Meta Data records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyContractsMeta table matching the where clause
			select	tbl.energy_contract_meta_uid,
					tbl.energy_contract_uid,
					tbl.meta_type_uid,
					tbl.meta_value,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					mt.data_type,
					mt.friendly_name as friendly_name,
					ec.friendly_name as energyContractFriendlyName,
					(select COUNT(1) from dbo.EnergyContractsMetaChoices ecmc (nolock) where ecmc.meta_type_uid = tbl.meta_type_uid) as numMetaChoices					

			from	dbo.EnergyContractsMeta tbl (nolock)
					join dbo.EnergyContracts ec (nolock)
						on tbl.energy_contract_uid = ec.energy_contract_uid
					join dbo.EnergyContractsMetaTypes mt (nolock)
						on tbl.meta_type_uid = mt.meta_type_uid

			where	1=1

										and tbl.is_active = 1
					<cfif structKeyExists(arguments, 'energy_contract_meta_uid')>
					and tbl.energy_contract_meta_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_contract_meta_uid#" null="false" list="true"> ) 

					</cfif>

					<cfif structKeyExists(arguments, 'energy_contract_uid')>
					and ec.energy_contract_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_contract_uid#" list="true" null="false"> ) 
					and ec.is_active = 1
					</cfif>

					<cfif structKeyExists(arguments, 'meta_type_uid')>
					and tbl.meta_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_type_uid#" null="false" list="true"> ) 
					</cfif>

					and mt.is_active =1

			order
			by		mt.friendly_name,
					ec.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Contracts Meta Data data. --->	
	<cffunction name="saveNewEnergyContractsMeta"
				hint="This method is used to persist a new Energy Contracts Meta Data record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContractsMeta" required="true" type="mce.e2.vo.EnergyContractsMeta" hint="Describes the VO containing the Energy Contracts Meta Data record that will be persisted."/>

		<!--- Persist the Energy Contracts Meta Data data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyContractsMeta", arguments.EnergyContractsMeta)/>

	</cffunction>

	<cffunction name="saveExistingEnergyContractsMeta"
				hint="This method is used to persist an existing Energy Contracts Meta Data record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContractsMeta" required="true" type="mce.e2.vo.EnergyContractsMeta" hint="Describes the VO containing the Energy Contracts Meta Data record that will be persisted."/>

		<!--- Persist the Energy Contracts Meta Data data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyContractsMeta", arguments.EnergyContractsMeta)/>

	</cffunction>

</cfcomponent>
