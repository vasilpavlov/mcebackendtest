<cfcomponent displayname="Property Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving property data from the application database.">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyComponent"
				returntype="mce.e2.vo.Property"
				hint="This method is used to retrieve a property value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var propertyComponent = createObject("component", "mce.e2.vo.Property")/>

		<!--- Return the property component --->
		<cfreturn propertyComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertiesAsQuery"
				returntype="query"
				hint="This method is used to retrieve an query list of all the properties in the application.">

		<!--- Initialize the method arguments --->
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">
		<!--- added by JF 10/16/2009 - is this ok? --->
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="selectMethod" type="string" required="false" default="singular-inclusive" hint="Describes the type of select sql operation to perform ( inclusive = using [in]; exclusive = using [not in] ) when selecting property collections ).">
		<cfargument name="relationship_filter_date" type="string" required="false" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes a client company primary key to filter active records.">
		<cfargument name="metaValuesFor" type="string" required="false" default="">

		<!--- Retrieve all properties and return it as a simple query --->
		<cfreturn getProperty(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getPropertiesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of property value objects for all the properties in the application.">

		<!--- Initialize the method arguments --->
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">
		<!-- added by JF 10/16/2009 - is this ok? -->
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="selectMethod" type="string" required="false" default="singular-inclusive" hint="Describes the type of select sql operation to perform ( inclusive = using [in]; exclusive = using [not in] ) when selecting property collections ).">
		<cfargument name="relationship_filter_date" type="string" required="false" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes a client company primary key to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qProperties = getProperty(argumentCollection=arguments)>

		<cfset local.returnResult = queryToVoArray(local.qProperties, "mce.e2.vo.Property")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>
		<cfreturn local.qProperties>

	</cffunction>

	<cffunction name="getAssociatedPropertiesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of associated property value objects for all the properties in the application.">

		<!--- Initialize the method arguments --->
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create an instance of the security service --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>

		<!--- Create a reference to the user's current session --->
		<cfset local.session = local.securityService.getSession()>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qProperties = getProperty(
				property_collection_uid = local.session.propertyCollectionsList)>
		<cfset local.returnResult = queryToVoArray(local.qProperties, "mce.e2.vo.Property")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getPropertyAsComponent"
				returntype="mce.e2.vo.Property"
				hint="This method is used to retrieve a property record from the application database, and populate its properties in a property value object.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qProperty = getProperty(property_uid=arguments.property_uid)>
		<cfset local.returnResult = queryToVo(local.qProperty, "mce.e2.vo.Property")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Define the methods used to interact with property data in the application database --->
	<cffunction name="getProperty"
				returntype="query"
				hint="This method is used to retrieve any property record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="singular-inclusive" hint="Describes the type of select sql operation to perform ( inclusive = using [in]; exclusive = using [not in] ) when selecting property collections ).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes a client company primary key to filter active records.">
		<cfargument name="energy_account_uid" type="string" required="false" hint="Provides a way to get the property for an energy account">
		<cfargument name="metaValuesFor" type="string" required="false" default="">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>
		<cfset var item = "">

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	p.property_uid,
					p.friendly_name,
					p.image_filename,
					p.document_set_uid,
					p.is_active,
					p.created_by,
					p.created_date,
					p.modified_by,
					p.modified_date,
					p.currentClientUID as client_company_uid,
					
					<cfif arguments.metaValuesFor neq "">
						<cfloop list="#arguments.metaValuesFor#" index="item">
						dbo.getPropertyMetaValueAsString(p.property_uid, '#item#', DEFAULT) AS meta_#item#,
						</cfloop>
					</cfif>
					
					(select count(*) from dbo.energyAccounts ea where ea.is_active = 1 and ea.property_uid = p.property_uid) as energyAccountCount,
					p.pmBldgID,
					dbo.getPropertyCompany(p.property_uid,'Client') as clientCompanyFriendlyName,
					dbo.getPropertyAddressAsString(p.property_uid,'property') as propertyAddressString

			from	dbo.properties p (nolock)

			where	1=1

					-- Filter on active data only
					and p.is_active = 1

					<cfif structKeyExists(arguments, 'property_uid')>
					and p.property_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#">)
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and p.property_uid in(

						select	pcc.property_uid
						from	dbo.properties_clientcompanies pcc (nolock)
						where	1=1
								and pcc.is_active = 1
								and pcc.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
								and ( pcc.relationship_end is null or pcc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and (pcc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )

					)
					</cfif>

					<cfif structKeyExists(arguments, 'energy_account_uid')>
						and p.property_uid IN (SELECT property_uid FROM EnergyAccounts WHERE energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.energy_account_uid#">))
					</cfif>

					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and p.property_uid in(

						select	ppc.property_uid
						from	dbo.properties_propertyCollections ppc (nolock)
						where	1=1
								and ppc.is_active = 1
								and ppc.property_collection_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#"> )
								and ( ppc.relationship_end is null or ppc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and ppc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					)
					</cfif>

			order
			by		p.friendly_name

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>


<!--- --->
	<cffunction name="getPropertyMeters"
				returntype="query"
				hint="This method is used to retrieve any property record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	em.energy_meter_uid,
			p.property_uid
			from	dbo.properties p (nolock) inner join
					dbo.EnergyMeters em (nolock) on em.property_uid = p.property_uid
			where	1=1

					-- Filter on active data only
					and p.is_active = 1

					<cfif structKeyExists(arguments, 'property_uid')>
					and p.property_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#">)
					</cfif>

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>
	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewProperty"
				access="public" returntype="void"
				hint="This method is used to persist a new property record to the application database.">
		<cfargument name="property" type="mce.e2.vo.Property" hint="Describes the VO containing the property record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Persist the property data to the application database --->
		<cfset this.dataMgr.insertRecord("Properties", arguments.property)>

	</cffunction>

	<cffunction name="saveExistingProperty"
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing property record to the application database.">
		<cfargument name="property" type="mce.e2.vo.Property" hint="Describes the VO containing the property record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Persist the property data to the application database --->
		<cfset this.dataMgr.updateRecord("Properties", arguments.property)>

	</cffunction>

</cfcomponent>