
<!---

  Template Name:  Energy Unit
     Base Table:  EnergyUnits	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyUnitDelegate"
				hint="This CFC manages all data access interactions for the EnergyUnits table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyUnitComponent"
				hint="This method is used to retrieve a Energy Unit object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyUnit"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyUnit")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyUnitsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Unit objects for all the Energy Units in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key for an associated energy type record."/>
		<cfargument name="energy_unit_uid" required="false" type="string" hint="Describes the primary key for a given energy unit record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Units and build out the array of components --->
		<cfset local.qResult = getEnergyUnit(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyUnit")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyUnitAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Unit value object representing a single Energy Unit record."
				output="false"
				returnType="mce.e2.vo.EnergyUnit"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_unit_uid" required="true" type="string" hint="Describes the primary key for a given energy unit record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Unit and build out the component --->
		<cfset local.qResult = getEnergyUnit(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyUnit")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyUnit"
				hint="This method is used to retrieve single / multiple records from the EnergyUnits table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_unit_uid" required="false" type="string" hint="Describes the primary key for a given energy unit record."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key for an associated energy type record."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the customer facing / external name for a given energy unit."/>
		<cfargument name="unit_lookup_code" required="false" type="string" hint="Describes the internal lookup code for a given energy unit."/>
		<cfargument name="btu_conversion_factor" required="false" type="any" hint="Describes the btu conversion factor applied / associated to a given energy unit."/>
		<cfargument name="oldid" required="false" type="numeric" hint="Describes the legacy primary key value for a given energy unit record."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy unit record."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Unit records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyUnits table matching the where clause
			select	distinct
					tbl.energy_unit_uid,
					tbl.friendly_name,
					tbl.unit_lookup_code,
					tbl.btu_conversion_factor,
					tbl.oldid,
					tbl.meta_group_uid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.EnergyUnits tbl (nolock)
			
					<!--- Filter on energy types --->
					<cfif structKeyExists(arguments, 'energy_type_uid')>
					join dbo.EnergyTypes_EnergyUnits eteu (nolock)
						on tbl.energy_unit_uid = eteu.energy_unit_uid
					join dbo.EnergyTypes et (nolock)
						on eteu.energy_type_uid = et.energy_type_uid
					</cfif>
			
			where	1=1
					and tbl.is_active = 1
						
					<!--- Filter on energy types --->										
					<cfif structKeyExists(arguments, 'energy_type_uid')>
					and eteu.is_active = 1
					and et.is_active = 1
					and et.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'energy_unit_uid')>
					and tbl.energy_unit_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_unit_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'unit_lookup_code')>
					and tbl.unit_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.unit_lookup_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'btu_conversion_factor')>
					and tbl.btu_conversion_factor in ( <cfqueryparam cfsqltype="cf_sql_float" value="#arguments.btu_conversion_factor#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'oldid')>
					and tbl.oldid in ( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.oldid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'meta_group_uid')>
					and tbl.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_group_uid#" list="true" null="false"> ) 
					</cfif>
					
			order
			by		tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Unit data. --->	
	<cffunction name="saveNewEnergyUnit"
				hint="This method is used to persist a new Energy Unit record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyUnit" required="true" type="mce.e2.vo.EnergyUnit" hint="Describes the VO containing the Energy Unit record that will be persisted."/>

		<!--- Persist the Energy Unit data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyUnits", arguments.EnergyUnit)/>

	</cffunction>

	<cffunction name="saveExistingEnergyUnit"
				hint="This method is used to persist an existing Energy Unit record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyUnit" required="true" type="mce.e2.vo.EnergyUnit" hint="Describes the VO containing the Energy Unit record that will be persisted."/>

		<!--- Persist the Energy Unit data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyUnits", arguments.EnergyUnit)/>

	</cffunction>

</cfcomponent>
