
<!---

  Template Name:  EnergyBudgetType
     Base Table:  EnergyBudgetTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyBudgetTypeDelegate"
				hint="This CFC manages all data access interactions for the EnergyBudgetTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyBudgetTypeComponent"
				hint="This method is used to retrieve a Energy Budget Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyBudgetType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyBudgetType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyBudgetTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Budget Type objects for all the Energy Budget Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given Energy Budget Type."/>
		<cfargument name="default_energy_unit_uid" required="false" type="string" hint="Describes the default energy unit associated to a given Energy Budget Type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Budget Types and build out the array of components --->
		<cfset local.qResult = getEnergyBudgetType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyBudgetType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyBudgetTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Budget Type value object representing a single Energy Budget Type record."
				output="false"
				returnType="mce.e2.vo.EnergyBudgetType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="budget_type_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given Energy Budget Type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Budget Type and build out the component --->
		<cfset local.qResult = getEnergyBudgetType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyBudgetType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyBudgetType"
				hint="This method is used to retrieve single / multiple records from the EnergyBudgetTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="budget_type_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given Energy Budget Type."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given Energy Budget Type."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Budget Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyBudgetTypes table matching the where clause
			select	tbl.budget_type_code,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.EnergyBudgetTypes tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'budget_type_code')>
					and tbl.budget_type_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.budget_type_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'energy_type_hierarchy')>
					and tbl.energy_type_hierarchy in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energy_type_hierarchy#" null="false" list="true"> ) 
					</cfif>


		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Budget Type data. --->	
	<cffunction name="saveNewEnergyBudgetType"
				hint="This method is used to persist a new Energy Budget Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyBudgetType" required="true" type="mce.e2.vo.EnergyBudgetType" hint="Describes the VO containing the Energy Budget Type record that will be persisted."/>

		<!--- Persist the Energy Budget Type data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyBudgetTypes", arguments.EnergyBudgetType)/>

	</cffunction>

	<cffunction name="saveExistingEnergyBudgetType"
				hint="This method is used to persist an existing Energy Budget Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyBudgetType" required="true" type="mce.e2.vo.EnergyBudgetType" hint="Describes the VO containing the Energy Budget Type record that will be persisted."/>

		<!--- Persist the Energy Budget Type data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyBudgetTypes", arguments.EnergyBudgetType)/>

	</cffunction>

</cfcomponent>
