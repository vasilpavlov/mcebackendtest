<cfcomponent displayname="CompanyTypeDelegate"
				hint="This CFC manages all data access interactions for the CompanyTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyCompanyTypeComponent"
				hint="This method is used to retrieve a Company Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.CompanyType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.CompanyType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getCompanyTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Company Type objects for all the Company Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_type_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given company type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Company Types and build out the array of components --->
		<cfset local.qResult = getCompanyType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.CompanyType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getCompanyTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Company Type value object representing a single Company Type record."
				output="false"
				returnType="mce.e2.vo.CompanyType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_type_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given company type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Company Type and build out the component --->
		<cfset local.qResult = getCompanyType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.CompanyType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getCompanyType"
				hint="This method is used to retrieve single / multiple records from the CompanyTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_type_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given company type."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given company type."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Company Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the CompanyTypes table matching the where clause
			select	tbl.company_type_code,
					tbl.friendly_name,
					tbl.display_order,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.CompanyTypes tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'company_type_code')>
					and tbl.company_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.company_type_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>
					
			Order by tbl.display_order

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Company Type data. --->	
	<cffunction name="saveNewCompanyType"
				hint="This method is used to persist a new Company Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="CompanyType" required="true" type="mce.e2.vo.CompanyType" hint="Describes the VO containing the Company Type record that will be persisted."/>

		<!--- Persist the Company Type data to the application database --->
		<cfset this.dataMgr.insertRecord("CompanyTypes", arguments.CompanyType)/>

	</cffunction>

	<cffunction name="saveExistingCompanyType"
				hint="This method is used to persist an existing Company Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="CompanyType" required="true" type="mce.e2.vo.CompanyType" hint="Describes the VO containing the Company Type record that will be persisted."/>

		<!--- Persist the Company Type data to the application database --->
		<cfset this.dataMgr.updateRecord("CompanyTypes", arguments.CompanyType)/>

	</cffunction>

</cfcomponent>