
<!---

  Template Name:  AlertDelegate
     Base Table:  Alerts	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 16, 2009 - Template Created.		

--->
		
<cfcomponent displayname="AlertDelegate"
				hint="This CFC manages all data access interactions for the Alerts table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyAlertComponent"
				hint="This method is used to retrieve a Alert object (vo) / coldFusion component."
				output="false"
				returnType=""
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.Alert")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getAlertsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert objects for all the Alerts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given alert."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated property."/>
		<cfargument name="alert_type_code" required="false" type="string" hint="Describes the type code / internal identifier for a given alert."/>
		<cfargument name="type_group_code" required="false" type="string" hint="Describes the type group code / internal identifier for a given alert type."/>
		<cfargument name="alert_status_code" required="false" type="string" hint="Describes the internal identifier / status code for a given alert."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alerts and build out the array of components --->
		<cfset local.qResult = getAlert(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult,"mce.e2.vo.Alert")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getAlertAsComponent"
				hint="This method is used to retrieve a single instance of a / an Alert value object representing a single Alert record."
				output="false"
				returnType="mce.e2.vo.Alert"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given alert."/>
		<cfargument name="alert_type_code" required="true" type="string" hint="Describes the type code / internal identifier for a given alert."/>
		<cfargument name="alert_status_code" required="true" type="string" hint="Describes the internal identifier / status code for a given alert."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert and build out the component --->
		<cfset local.qResult = getAlert(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.Alert")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getAlert"
				hint="This method is used to retrieve single / multiple records from the Alerts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given alert."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated property."/>
		<cfargument name="alert_type_code" required="false" type="string" hint="Describes the type code / internal identifier for a given alert."/>
		<cfargument name="type_group_code" required="false" type="string" hint="Describes the type group code / internal identifier for a given alert type."/>
		<cfargument name="alert_status_code" required="false" type="string" hint="Describes the internal identifier / status code for a given alert."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the Alerts table matching the where clause
			select	tbl.alert_uid,
					tbl.property_uid,
					tbl.friendly_name,
					tbl.alert_type_code,
					tbl.alert_status_code,
					tbl.alert_generated_date,
					tbl.alert_due_date,
					tbl.alert_visibility_date,
					tbl.foreign_key_uid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					
					ats.friendly_name as alertTypeFriendlyName,
					atg.friendly_name as alertTypeGroupFriendlyName

			from	dbo.Alerts tbl (nolock)
					join dbo.alertTypes ats (nolock)
						on tbl.alert_type_code = ats.alert_type_code
					join dbo.alertTypeGroups atg (nolock)
						on ats.type_group_code = atg.type_group_code

			where	1=1
					
					and tbl.is_active = 1
					and ats.is_active = 1
					and atg.is_active = 1

					<cfif structKeyExists(arguments, 'alert_uid')>
					and tbl.alert_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'alert_type_code')>
					and tbl.alert_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_type_code#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'type_group_code')>
					and atg.type_group_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.type_group_code#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'alert_status_code')>
					and tbl.alert_status_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_status_code#" null="false" list="true"> ) 
					</cfif>

			order
			by		tbl.friendly_name,
					tbl.modified_date

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Alert data. --->	
	<cffunction name="saveNewAlert"
				hint="This method is used to persist a new Alert record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="Alert" required="true" type="mce.e2.vo.Alert" hint="Describes the VO containing the Alert record that will be persisted."/>

		<!--- Persist the Alert data to the application database --->
		<cfset this.dataMgr.insertRecord("Alerts", arguments.Alert)/>

	</cffunction>

	<cffunction name="saveExistingAlert"
				hint="This method is used to persist an existing Alert record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="Alert" required="true"  type="mce.e2.vo.Alert" hint="Describes the VO containing the Alert record that will be persisted."/>

		<!--- Persist the Alert data to the application database --->
		<cfset this.dataMgr.updateRecord("Alerts", arguments.Alert)/>

	</cffunction>

</cfcomponent>
