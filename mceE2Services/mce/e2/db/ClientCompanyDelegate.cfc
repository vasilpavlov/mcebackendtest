<cfcomponent displayname="Client Company Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving Client Company data from the application database.">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyClientCompanyComponent"
				returntype="mce.e2.vo.ClientCompany"
				hint="This method is used to retrieve a Client Company object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var ClientCompanyComponent = createObject("component", "mce.e2.vo.ClientCompany")/>

		<!--- Return the Client Company component --->
		<cfreturn ClientCompanyComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getClientCompaniesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of Client Company objects for all the Client Companies in the application.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="includeRoleInformation" type="boolean" required="true" default="true" hint="Whether to include information about company and property roles.">
		<cfargument name="getDescendants" type="boolean" required="true" default="false" hint="Whether to include companies in the hierarchy of the provided client company uid">
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Client Companies and build out the array of components --->
		<cfset local.qClientCompany = getClientCompany(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qClientCompany, "mce.e2.vo.ClientCompany")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getClientCompanyPropertiesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of Properties for a given Client Company object in the application.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Client Companies and build out the array of components --->
		<cfset local.qClientCompany = getClientCompanyProperties(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qClientCompany, "mce.e2.vo.Property")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getClientCompanyUsersAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of Users for a given Client Company object in the application.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Client Companies and build out the array of components --->
		<cfset local.qClientCompany = getClientCompanyUsers(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qClientCompany, "mce.e2.vo.User")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getClientCompanyAsComponent"
				returntype="mce.e2.vo.ClientCompany"
				hint="This method is used to retrieve a Client Company record from the application database, and populate its properties in a Client Company object.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given Client Company record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Client Companies and build out the array of components --->
		<cfset local.qClientCompany = getClientCompany(client_company_uid=arguments.client_company_uid)>
		<cfset local.returnResult = queryToVo(local.qClientCompany, "mce.e2.vo.ClientCompany")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>
	
	<cffunction name="getClientCompanyWithChildCompaniesList"
				returntype="String"
				hint="This method is used to retrieve a list of child companies that are part of a single client company.">
				
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given Client Company record.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="getDescendants" type="boolean" required="true" default="false" hint="Whether to include companies in the hierarchy of the provided client company uid">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Client Companies and build out the array of components --->
		<cfset local.qClientCompanies = getClientCompany(client_company_uid=arguments.client_company_uid,
											relationship_filter_date = arguments.relationship_filter_date,
											selectMethod = arguments.selectMethod,
											getDescendants = arguments.getDescendants
										)>
		<cfreturn valueList(local.qClientCompanies.client_company_uid)>
	</cffunction>

	<!--- Define the methods used to interact with Client Company data in the application database --->
	<cffunction name="getClientCompany"
				returntype="query"
				hint="This method is used to retrieve data for a given Client Company definition.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="includeRoleInformation" type="boolean" required="true" default="true" hint="Whether to include information about company and property roles. Note that this can result in multiple records being returned per company. If you are only interested in the company itself, set this to false.">
		<cfargument name="getDescendants" type="boolean" required="true" default="false" hint="Whether to include companies in the hierarchy of the provided client company uid">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Sanity check: Setting "includeRoleInformation" to true only makes sense if a property uid was provided --->
		<cfset arguments.includeRoleInformation = arguments.includeRoleInformation and structKeyExists(arguments, 'property_uid')>

		<!--- Build out the query to retrieve the Client Company's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			SELECT 	cc.client_company_uid
					,cc.friendly_name
					,cc.company_hierarchyid as sort_by_hierarchyid
					,cc.company_hierarchyid.ToString() as company_hierarchyid
					,cc.company_hierarchyid.ToString() as my_hier
					,cc.company_hierarchyid.GetAncestor(1).ToString() as parent_hier
					,cc.image_filename
					,cc.oldId
					,cc.is_active
					,cc.created_by
					,cc.created_date
					,cc.modified_by
					,cc.modified_date
					,cc.document_set_uid
					,cc.company_type_code
					,CONVERT(bit, null) as is_parent
					,ct.friendly_name as companyTypeFriendlyName
					
					<cfif arguments.includeRoleInformation>
					,cr.friendly_name as companyRoleFriendlyName
					,p.friendly_name as propertyFriendlyName
					,pcc.relationship_id
					,pcc.relationship_start
					,pcc.relationship_end
					</cfif>
			into	##list
			from 	dbo.ClientCompanies cc (nolock)
					join dbo.CompanyTypes ct (nolock)
						on cc.company_type_code = ct.company_type_code
					<cfif arguments.includeRoleInformation>
					<!--- join dbo.ClientCompanies_CompanyRoles cccr (nolock)
						on cc.client_company_uid = cccr.client_company_uid --->
					join dbo.Properties_ClientCompanies pcc (nolock)
						on cc.client_company_uid = pcc.client_company_uid
						
						<!--- and cccr.company_role_uid = pcc.company_role_uid --->
					join dbo.CompanyRoles cr (nolock)
						<!--- on cccr.company_role_uid = cr.company_role_uid --->
						<!--- and pcc.company_role_uid = cr.company_role_uid --->
						on pcc.company_role_uid = cr.company_role_uid
					join dbo.Properties p (nolock)
						on pcc.property_uid = p.property_uid
					</cfif>
			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					and cc.is_active = 1
					</cfif>

					<cfif arguments.includeRoleInformation>
						<cfif arguments.selectMethod eq 'current'>
						and p.is_active = 1
						and cr.is_active = 1
						and (

								-- Only retrieve active associations
								pcc.is_active = 1
								and ( pcc.relationship_end is null or pcc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and pcc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

							)
						</cfif>
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid') and len(arguments.client_company_uid)>
						and cc.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
						<cfif arguments.getDescendants eq true>
						or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)) = 1
						</cfif>
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid') and len(arguments.property_uid)>
						<cfif arguments.includeRoleInformation>
							and p.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#"> )
						<cfelse>
							and cc.client_company_uid IN (
								SELECT pcc.client_company_uid
								FROM dbo.Properties_ClientCompanies pcc (nolock)
								WHERE pcc.property_uid IN ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#"> )
								AND pcc.is_active = 1
								and ( pcc.relationship_end is null or pcc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and pcc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
							)
						</cfif>
					</cfif>

			update	##list
			set		is_parent = 1
			from	##list
			where	parent_hier is null or my_hier in (select parent_hier from ##list)

			select	*
			from	##list
			order
			by		friendly_name,
					sort_by_hierarchyid,
					modified_date
					
			drop table ##list

		</cfquery>

		<!--- Return the query / result set --->
		<cfreturn qResult>

	</cffunction>
	<cffunction name="getClientCompanyProperties"
				returntype="query"
				hint="This method is used to retrieve Properties data for a given Client Company definition.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the Client Company's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			SELECT DISTINCT	--cc.client_company_uid,
					p.property_uid
					,p.friendly_name
					,p.document_set_uid
					,p.currentClientUID as client_company_uid

			from 	dbo.ClientCompanies cc (nolock)
					join dbo.Properties_ClientCompanies pcc (nolock)
						on cc.client_company_uid = pcc.client_company_uid
						
					join dbo.Properties p (nolock)
					 	on pcc.property_uid = p.property_uid
			where	1=1
					and pcc.is_active=1
					<cfif arguments.selectMethod eq 'current'>
					and p.is_active = 1
					and cc.is_active = 1
					</cfif>

					and (cc.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
					or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)) = 1)

			order
			by		p.friendly_name

		</cfquery>

		<!--- Return the query / result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getChildCompaniesAndProperties"
				returntype="query"
				hint="This method is used to retrieve child companies and their properties for a given client.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the Client Company's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
			declare @client_company_uid uniqueidentifier = <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">
			
			select	DISTINCT cc.client_company_uid, null as property_uid, cc.friendly_name, cc.company_hierarchyid, 1 as sort_order, ct.friendly_name as company_type
			from	dbo.ClientCompanies cc
						join dbo.CompanyTypes ct on ct.company_type_code = cc.company_type_code
			where	1=1
					<cfif arguments.selectMethod eq 'current'>
						and cc.is_active = 1
					</cfif>
					and (cc.client_company_uid = @client_company_uid or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=@client_company_uid)) = 1)
			union
			select	DISTINCT cc.client_company_uid, p.property_uid, p.friendly_name,  cc.company_hierarchyid, 2 as sort_order, '' as company_type
			from	dbo.ClientCompanies cc
						join dbo.Properties_ClientCompanies pcc on pcc.client_company_uid = cc.client_company_uid
						join dbo.Properties p on p.property_uid = pcc.property_uid
			where	1=1
					and pcc.is_active = 1
					<cfif arguments.selectMethod eq 'current'>
						and cc.is_active = 1
						
						and p.is_active = 1
					</cfif>
					and (cc.client_company_uid = @client_company_uid/* or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=@client_company_uid)) = 1*/)
			order by
					--cc.company_hierarchyid,
					sort_order,
					friendly_name

		</cfquery>

		<!--- Return the query / result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getClientCompanyUsers"
				returntype="query"
				hint="This method is used to retrieve Properties data for a given Client Company definition.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="getDescendants" type="boolean" required="true" default="false" hint="Whether to include companies in the hierarchy of the provided client company uid">
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the Client Company's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			SELECT DISTINCT	cc.client_company_uid
					,u.user_uid
					,u.first_name
					,u.last_name
					,u.username
					,u.alert_email
					,u.is_active

			from 	dbo.ClientCompanies cc (nolock)
					join dbo.Users u (nolock)
						on cc.client_company_uid = u.client_company_uid
			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					and u.is_active = 1
					</cfif>

					and cc.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
					<cfif arguments.getDescendants eq true>
						or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)) = 1
					</cfif>
			order
			by		u.first_name

		</cfquery>

		<!--- Return the query / result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getNotesByContext"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of contacts associated to companies or properties.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="showReminders" type="boolean" required="true" default="true" hint="Describes whether or not reminders should be included.">
		<cfargument name="showNonReminders" type="boolean" required="true" default="false" hint="Describes whether or not non-reminders should be included.">
		<cfargument name="dueIn" type="string" required="true" default="7" hint="Describes which reminders to get.">
		<cfargument name="includeDescendants" type="boolean" required="true" default="true" hint="Describes whether or not to include children.">
		
		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Contact records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		declare @client_company_uid uniqueidentifier = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="false" null="#iif(isUniqueIdentifier(arguments.client_company_uid), 'false', 'true')#">
		declare @property_uid uniqueidentifier = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="false" null="#iif(isUniqueIdentifier(arguments.property_uid), 'false', 'true')#">
		declare @relationship_filter_date datetime = <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
		declare @dueIn varchar(20) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.dueIn#">
		
		select	*
		from	(
				select	distinct
						cc.company_hierarchyid,
						1 as display_order,
						cc.client_company_uid,
						null as property_uid,
						cc.client_company_uid as context_uid,
						cc.friendly_name as context_name,
						cn.client_note_uid, cn.friendly_name, cn.note_text, cn.reminder_date, cn.completed_date, cn.completed_user_uid, cn.is_active, cn.created_by, cn.created_date, cn.modified_by, cn.modified_date,
						u.first_name + ' ' + u.last_name as completed_by
				from	dbo.ClientCompanies cc
							left outer join dbo.ClientNotes cn 
								on cn.client_company_uid = cc.client_company_uid
								and cn.property_uid is null
								<cfif arguments.selectMethod eq 'current'>
									and cn.is_active = 1
								</cfif>
								<cfif arguments.showReminders and not arguments.showNonReminders>
									and cn.reminder_date is not null
								<cfelseif not arguments.showReminders and arguments.showNonReminders>
									and cn.reminder_date is null
								<cfelseif not arguments.showReminders and not arguments.showNonReminders>
									and 1 = 0
								</cfif>
								<cfif arguments.showReminders>
									<cfif listFind("7,14,30", arguments.dueIn)>
										and (cn.reminder_date <= DATEADD(DAY, convert(int, @dueIn), GETDATE()) or cn.reminder_date is null)
										and cn.completed_date is null
									<cfelseif arguments.dueIn eq "any">
										and cn.completed_date is null
									<cfelseif arguments.dueIn eq "complete">
										and cn.reminder_date is not null
										and cn.completed_date is not null
									</cfif>
								</cfif>
							left outer join dbo.Users u on u.user_uid = cn.completed_user_uid
				where	1 = 1
						and 1 = case when @client_company_uid is null then 0 else 1 end
						and cc.is_active = 1
						and (
							cc.client_company_uid = @client_company_uid
							<cfif includeDescendants>
							or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=@client_company_uid)) = 1
							</cfif>
						)

				union

				select	distinct
						cc.company_hierarchyid,
						2 as display_order,
						cc.client_company_uid, 
						p.property_uid,
						p.property_uid as context_uid,
						isnull(p.friendly_name,'Property') as context_name,
						cn.client_note_uid, cn.friendly_name, cn.note_text, cn.reminder_date, cn.completed_date, cn.completed_user_uid, cn.is_active, cn.created_by, cn.created_date, cn.modified_by, cn.modified_date,
						u.first_name + ' ' + u.last_name as completed_by
				from	dbo.ClientCompanies cc
							join dbo.Properties_ClientCompanies pcc 
								on pcc.client_company_uid = cc.client_company_uid
								
								and pcc.company_role_uid IN(SELECT company_role_uid FROM CompanyRoles WHERE company_type_code = 'client')
								and (
									-- Only retrieve active associations
									pcc.is_active = 1
									and ( pcc.relationship_end is null or pcc.relationship_end >= @relationship_filter_date )
									and pcc.relationship_start < @relationship_filter_date
								)						
							join dbo.Properties p 
								on p.property_uid = pcc.property_uid
								and p.is_active = 1
							left outer join dbo.ClientNotes cn 
								on cn.client_company_uid = cc.client_company_uid 
								and cn.property_uid = p.property_uid
								<cfif arguments.selectMethod eq 'current'>
									and cn.is_active = 1
								</cfif>
								<cfif arguments.showReminders and not arguments.showNonReminders>
									and cn.reminder_date is not null
								<cfelseif not arguments.showReminders and arguments.showNonReminders>
									and cn.reminder_date is null
								<cfelseif not arguments.showReminders and not arguments.showNonReminders>
									and 1 = 0
								</cfif>
								<cfif arguments.showReminders>
									<cfif listFind("7,14,30", arguments.dueIn)>
										and (cn.reminder_date <= DATEADD(DAY, convert(int, @dueIn), GETDATE()) or cn.reminder_date is null)
										and cn.completed_date is null
									<cfelseif arguments.dueIn eq "any">
										and cn.completed_date is null
									<cfelseif arguments.dueIn eq "complete">
										and cn.reminder_date is not null
										and cn.completed_date is not null
									</cfif>
								</cfif>
							left outer join dbo.Users u on u.user_uid = cn.completed_user_uid
				where	1 = 1
						and pcc.is_active = 1
						and cc.is_active = 1
						<cfif includeDescendants>
						and p.property_uid = isnull(@property_uid, p.property_uid)
						and (
							cc.client_company_uid = isnull(@client_company_uid, cc.client_company_uid)
							<cfif includeDescendants>
							or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=@client_company_uid)) = 1
							</cfif>
						)
						<cfelse>
						and p.property_uid = @property_uid
						</cfif>
				) der
		where	client_note_uid is not null 
				<cfif includeDescendants>
				or context_uid = coalesce(@property_uid, @client_company_uid)
				</cfif>

		order by
				company_hierarchyid,
				display_order,
				context_name,
				reminder_date desc,
				completed_date desc,
				friendly_name
		</cfquery>
		<cfreturn local.qResult>
	</cffunction>

	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewClientCompany"
				access="public" returntype="void"
				hint="This method is used to persist a new Client Company record to the application database.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" hint="Describes the VO containing the Client Company record that will be persisted.">

		<cfset var local = structnew()>
		
		<cfif not structKeyExists(arguments.ClientCompany, "parent_hier")>
			<cfset local.parent_uid = getInternalClientCompanyUid()>
		<cfelse>
			<cfset local.parent_uid = arguments.ClientCompany.parent_hier>
		</cfif>
				
		<!--- Persist the ClientCompany data to the application database --->
		<cfset this.dataMgr.insertRecord("ClientCompanies", arguments.ClientCompany)>
		
		<!--- Put this company is the right spot. --->
		<cfset changeCompanyFiling(parent_company_uid=local.parent_uid, child_company_uid=arguments.ClientCompany.client_company_uid, company_type_code=arguments.ClientCompany.company_type_code)>

	</cffunction>

	<cffunction name="saveExistingClientCompany"
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing Client Company record to the application database.">
		<cfargument name="ClientCompany" type="any" hint="Describes the VO containing the Client Company record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Persist the ClientCompany data to the application database --->
		<cfset local.newUid = this.dataMgr.updateRecord("ClientCompanies", arguments.ClientCompany)>

	</cffunction>
	<cffunction name="changeCompanyFiling"
				access="public" returntype="void"
				hint="This method is used to change the hierarchy for a given Client Company">
		<cfargument name="parent_company_uid" type="String" required="true" hint="Describes the Parent client company primary key/uid">
		<cfargument name="child_company_uid" type="String" required="true" hint="Describes the Child client company primary key/uid">
		<cfargument name="company_type_code" type="String" required="true" hint="Describes the company type code to file the child as">

		<cfstoredproc datasource="#this.datasource#" procedure="changeCompanyFiling">
			<cfprocparam value="#arguments.parent_company_uid#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.child_company_uid#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.company_type_code#" cfsqltype="CF_SQL_VARCHAR">
		</cfstoredproc>
	</cffunction>

	<cffunction name="changeCompanyFriendlyName"
				access="public" returntype="void"
				hint="This method is used to change the friendly name for a given Client Company">
	<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company value object">


		<cfstoredproc datasource="#this.datasource#" procedure="changeCompanyFriendlyName">
			<cfprocparam value="#arguments.clientCompany.client_company_uid#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.clientCompany.friendly_name#" cfsqltype="CF_SQL_VARCHAR">
		</cfstoredproc>
	</cffunction>


	<cffunction name="getInternalClientCompany" access="public" returntype="mce.e2.vo.ClientCompany">
		<cfset var internalClientCompanyUid = getInternalClientCompanyUid()>
		<cfreturn getClientCompanyAsComponent(internalClientCompanyUid)>
	</cffunction>
	
	<cffunction name="getRootClientForCompany" access="public" returntype="query">
			<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company value object">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the Client Company's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			SELECT dbo.getClientUID('#arguments.clientCompany.client_company_uid#') as client_company_uid
			,(SELECT friendly_name FROM ClientCompanies WHERE client_company_uid = dbo.getClientUID('#arguments.clientCompany.client_company_uid#')) as friendly_name

		</cfquery>

		<!--- Return the query / result set --->
		<cfreturn qResult>
	</cffunction>

	<cffunction name="getInternalClientCompanyUid" access="public" returntype="string">
		<cfset var qInternalClientCompanyLookup = "">

		<cfquery name="qInternalClientCompanyLookup" datasource="#this.datasource#">
			SELECT client_company_uid
			FROM ClientCompanies
			WHERE company_type_code = 'internal'
			AND is_active = 1
			AND company_hierarchyid = hierarchyid::GetRoot()
		</cfquery>

		<cfif qInternalClientCompanyLookup.recordCount neq 1>
			<cfthrow message="Missing or ambiguous internal client company; expected one but got #qInternalClientCompanyLookup.recordCount# identifiers.">
		</cfif>

		<cfreturn qInternalClientCompanyLookup.client_company_uid>
	</cffunction>

	<cffunction name="associateClientContactToClientCompany"
				access="public"
				returntype="void"
				hint="This method is used to add / associate a given client contact to a client company.">

		<!--- Define the arguments for this method --->
		<cfargument name="assocObject" type="any" required="true">
		
		<cfset this.dataMgr.insertRecord("ClientContacts_ClientCompanies", arguments.assocObject)>

	</cffunction>				

	<cffunction name="disAssociateClientContactFromClientCompany"
				access="public"
				returntype="void"
				hint="This method is used to remove / disassociate a given client contact from a client company.">

		<!--- Define the arguments for this method --->
		<cfargument name="assocObject" type="any" required="true">
		
		<cfset this.dataMgr.updateRecord("ClientContacts_ClientCompanies", arguments.assocObject)>

	</cffunction>				

	<cffunction name="saveNewClientNote"
				access="public" returntype="void"
				hint="This method is used to persist a new Client Note record to the application database.">
		<cfargument name="ClientNote" type="mce.e2.vo.ClientNote" hint="Describes the VO containing the Client Note record that will be persisted.">

		<!--- Persist the ClientNote data to the application database --->
		<cfset this.dataMgr.insertRecord("ClientNotes", arguments.ClientNote)>
		
	</cffunction>

	<cffunction name="saveExistingClientNote"
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing Client Company record to the application database.">
		<cfargument name="ClientNote" type="any" hint="Describes the VO containing the Client Note record that will be persisted.">

		<!--- Persist the ClientNote data to the application database --->
		<cfset this.dataMgr.updateRecord("ClientNotes", arguments.ClientNote)>

	</cffunction>

</cfcomponent>