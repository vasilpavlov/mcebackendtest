<cfcomponent displayname="Report Services Delegate"
			 output="false" extends="BaseDatabaseDelegate">

	<cffunction name="insertReportView" returntype="string"> 
		<cfargument name="report_lookup_code" type="string" required="true"> 
		<cfargument name="parameters" type="array" required="true"> 
		<cfargument name="user_session_uid" type="string" required="true"> 
			<cfset var local = structNew()>
			<!-- First use DataMgr to insert new UserReportViews record -->
			<cfset arguments.report_uid = this.getReportName(report_lookup_code).report_uid>
			<cfset local.report_view_uid = this.dataMgr.insertRecord("UserReportViews", arguments)> 
			<!-- You will need to get the report_name using the lookup code --> 

			<cfquery name="qResult" datasource="#this.datasource#">
	
				SELECT *
				FROM 	dbo.UserReports
				where	1=1
	
						<cfif structKeyExists(arguments, 'report_lookup_code')>
						and report_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.report_lookup_code#"> )
						</cfif>
			</cfquery>
			
			<!-- Now use DataMgr to insert new UserReportViewParams record --> 
			<cfloop from="1" to="#ArrayLen(parameters)#" index="i"> 
				<cfset parameters[i].report_view_uid = local.report_view_uid>
				<cfset this.dataMgr.insertRecord("UserReportViewParams", parameters[i])>
			</cfloop>
			<cfreturn local.report_view_uid>
	</cffunction> 
	<cffunction name="getReportView" returntype="struct"> 
		<cfargument name="report_view_uid" type="string" required="true"> 
		<cfargument name="user_session_uid" type="string" required="true"> 

		<cfset var local = structNew()>

<!-- first check db to make sure this is legit view/session combo --> 
		<cfset var xResult = ''>
		<cfset var qResult = ''>
		<cfquery name="xResult" datasource="#this.datasource#">

			SELECT *
			FROM 	dbo.UserReportViews JOIN
					dbo.UserReports on UserReports.report_uid = UserReportViews.report_uid			
			where	1=1

					<cfif structKeyExists(arguments, 'user_session_uid')>
					and user_session_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_session_uid#"> )
					</cfif>
					<cfif structKeyExists(arguments, 'report_view_uid')>
					and report_view_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.report_view_uid#"> )
					</cfif>
		</cfquery>
			<!-- throw hard error if you get no data --> 
			<cfif xResult.recordcount neq 1> 
			<cfthrow message="Not a valid UserReportView">
				<cfelse>

		<!-- next get parameters etc --> 
	
			<cfquery name="qResult" datasource="#this.datasource#">
	
				SELECT *
				FROM 	dbo.UserReportViewParams
				where	1=1
	
						<cfif structKeyExists(arguments, 'report_view_uid')>
						and report_view_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.report_view_uid#"> )
						</cfif>
			</cfquery>
				<cfset local.params = queryToVoArray(qResult, "mce.e2.vo.UserReportViewParams")>
			</cfif> 

		<cfset local.userrptview = queryToVo(xResult, "mce.e2.vo.UserReportViews")>

		<cfreturn local> 
	</cffunction>
	
	<cffunction name="getReportName" returntype="query"> 
		<cfargument name="report_lookup_code" type="string" required="true"> 
			<!-- You will need to get the report_name using the lookup code --> 
			<cfset var qResult = ''>
			<cfquery name="qResult" datasource="#this.datasource#">
	
				SELECT *
				FROM 	dbo.UserReports
				where	1=1
	
						<cfif structKeyExists(arguments, 'report_lookup_code')>
						and report_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.report_lookup_code#"> )
						</cfif>
			</cfquery>
		<cfreturn qResult>
	</cffunction>
	
	<cffunction name="getReports" returntype="query"
				hint="Returns a report list from dbo.UserReports"> 
			<cfset var qResult = ''>
			<cfquery name="qResult" datasource="#this.datasource#">
	
				SELECT *
				FROM 	dbo.UserReports
			</cfquery>
		<cfreturn qResult>
	</cffunction>
</cfcomponent>