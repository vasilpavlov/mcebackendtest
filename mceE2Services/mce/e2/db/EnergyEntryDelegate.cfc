<cfcomponent displayname="Energy Entry Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving energy usage data from the application database.">

	<!--- Constructor (Autowired by Coldspring IOC/DI framework) --->
	<cffunction name="init">
		<cfargument name="datasource" type="string" required="true" hint="A CF datasource name">
		<cfargument name="energyContractsDelegate" type="mce.e2.db.EnergyContractsDelegate" required="true">
		
		<cfset super.init(datasource)>
		<cfset this.energyContractsDelegate = arguments.energyContractsDelegate>
		
		<cfreturn this>
	</cffunction>


	<cffunction name="getEnergyUsageRecordAsComponent"
				returntype="mce.e2.vo.EnergyEntryInfo"
				hint="This method is used to retrieve an energy usage record as a coldFusion component.">
		<cfargument name="energy_usage_uid" type="string" required="true" hint="Describes the primary key / unique identifier of the energy usage record being retrieved.">
		<cfargument name="priorPeriods" type="array" required="false" default="#ArrayNew(1)#" hint="Describes the prior periods of energy usage information being added to the retrieved energy usage record.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="priorPeriodDefaultOffsets" type="string" required="true">
		<cfargument name="usageTypeCodeForDefaultPriorPeriods" type="string" required="true">
		<cfargument name="userPermissions" type="string" required="true">
		<cfargument name="includeEnergyContract" type="boolean" required="false" default="true">

		<!--- We get one EnergyUsage row from the DB to produce the main VO instance to return --->
		<cfset var qEnergyUsage = getEnergyUsageRecord(argumentCollection=arguments)>
		<cfset var entryInfo = queryToVo(qEnergyUsage, "mce.e2.vo.EnergyEntryInfo")>
		<cfset var energyContracts = "">

		<!--- Also get multiple EnergyUsageMeta rows from the DB to include as the VO's metaFields property --->
		<cfset var qEnergyUsageMeta = getEnergyUsageMeta(entryInfo.energy_usage_uid, entryInfo.meta_group_uid, userPermissions)>
		<cfset entryInfo.metaFields = queryToVoArray(qEnergyUsageMeta, "mce.e2.vo.EnergyEntryMeta")>
		<cfset applyChoicesToEnergyUsageMeta(entryInfo.metaFields)>

		<!--- Energy Contracts --->		
		<cfif arguments.includeEnergyContract>
			
		</cfif>

		<!--- If prior periods were not explicitly provided, use some defaults --->
		<cfif ArrayLen(priorPeriods) eq 0>
			<cfinvoke
				method="getPriorPeriodsFromDefaultOffsets"
				energy_account_uid="#qEnergyUsage.energy_account_uid#"
				report_date="#qEnergyUsage.report_date#"
				priorPeriodDefaultOffsets="#priorPeriodDefaultOffsets#"
				usageTypeCodeForDefaultPriorPeriods="#usageTypeCodeForDefaultPriorPeriods#"
				returnVariable="priorPeriods">
		</cfif>

		<!--- Merge in data from any prior/comparison periods that have been requested --->
		<cfset addPriorRecordsToEnergyEntryInfo(entryInfo, priorPeriods, userPermissions)>
		<!--- Merge in additional meta field objects to represent total_cost and recorded_value --->
		<cfset packTopLevelValuesIntoFakeMetaFields(entryInfo, priorPeriods)>

		<cfreturn entryInfo>
	</cffunction>


	<cffunction name="getEmptyEnergyUsageRecordAsComponent"
				returntype="mce.e2.vo.EnergyEntryInfo"
				hint="This method is used to retrieve an empty energy record as a coldFusion component.">
		<cfargument name="energy_account_uid" type="string" required="true" hint="Describes the primary key / unique identifier of the energy account used to create the empty energy usage record.">
		<cfargument name="period_start" type="string" required="true" hint="Describes the start period for the empty energy usage record.">
		<cfargument name="period_end" type="string" required="true" hint="Describes the end period for the empty energy usage record.">
		<cfargument name="priorPeriods" type="array" required="false" default="#ArrayNew(1)#" hint="Describes any prior enery usage peiods being applied / added to the empty energy usage record.">
		<cfargument name="priorPeriodDefaultOffsets" type="string" required="true">
		<cfargument name="usageTypeCodeForDefaultPriorPeriods" type="string" required="true">
		<cfargument name="userPermissions" type="string" required="true">
		<cfargument name="includeEnergyContract" type="boolean" required="false" default="true">

		<!--- We get one EnergyUsage row from the DB to produce the main VO instance to return --->
		<cfset var qEnergyUsage = getEmptyEnergyUsageRecord(energy_account_uid, period_start, period_end)>
		<cfset var entryInfo = queryToVo(qEnergyUsage, "mce.e2.vo.EnergyEntryInfo")>
		<cfset var energyContracts = "">

		<!--- Also get multiple EnergyUsageMeta rows from the DB to include as the VO's metaFields property --->
		<cfset var qEnergyUsageMeta = getEmptyEnergyUsageMeta(qEnergyUsage.meta_group_uid, userPermissions)>
		<cfset entryInfo.metaFields = queryToVoArray(qEnergyUsageMeta, "mce.e2.vo.EnergyEntryMeta")>
		<cfset applyChoicesToEnergyUsageMeta(entryInfo.metaFields)>
		
		<!--- Energy Contracts --->		
		<cfif arguments.includeEnergyContract>
			<cfset energyContracts = this.energyContractsDelegate.getEnergyContractsAsArrayOfComponentsUnique(energy_account_uid=qEnergyUsage.energy_account_uid, mostRecentFirst=true, rateModelInputLookupCodeForPrice="price")>
			<cfif ArrayLen(energyContracts) gt 0>
				<cfset entryInfo.energyContract = energyContracts[1]>
			</cfif>
		</cfif>

		<!--- If prior periods were not explicitly provided, use some defaults --->
		<cfif ArrayLen(priorPeriods) eq 0>
			<cfinvoke
				method="getPriorPeriodsFromDefaultOffsets"
				energy_account_uid="#qEnergyUsage.energy_account_uid#"
				report_date="#qEnergyUsage.report_date#"
				priorPeriodDefaultOffsets="#priorPeriodDefaultOffsets#"
				usageTypeCodeForDefaultPriorPeriods="#usageTypeCodeForDefaultPriorPeriods#"
				returnVariable="priorPeriods">
		</cfif>

		<!--- Merge in data from any prior/comparison periods that have been requested --->
		<cfset addPriorRecordsToEnergyEntryInfo(entryInfo, priorPeriods, userPermissions)>
		<cfset packTopLevelValuesIntoFakeMetaFields(entryInfo, priorPeriods)>

		<cfreturn entryInfo>
	</cffunction>





	<cffunction name="getEnergyUsageRecordsForBudget"
				returntype="array" access="public"
				hint="This method is used to retrieve all energy usage records from the application database for a given energy budget and also includes some additional columns (friendly names, etc).">

		<!--- Intialize the method arguments --->
		<cfargument name="energy_budget_uid" type="string" required="true" hint="Describes the bused to filter active records.">

		<!--- Initialize any local variables --->
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			select	distinct
					usage.energy_usage_uid,
					usage.period_start,
					usage.period_end,
					usage.classification_code,
					usage.recorded_value,
					usage.is_system_calculated,
					usage.usage_type_code,
					usage.usage_status_code,
					usage.usage_status_date,
					usage.report_date,
					ea.energy_account_uid,
					ea.energy_unit_uid,
					p.property_uid,
					nat.native_account_number,
					nat.meta_group_uid,
					ea.friendly_name as energyAccountFriendlyName,
					p.friendly_name as propertyFriendlyName,
					prov.friendly_name as providerFriendlyName,
					units.friendly_name as energyUnitFriendlyName,

					-- New properties added with UBS requirements
					usage.total_cost,
					usage.user_comments,
					mtg.has_total_cost,
					mtg.has_recorded_value,
					usage.is_active
					,dbo.getEnergyUsageMetaValueById(usage.energy_usage_uid,'tdcharges') as total_utility_cost
					,ISNULL(dbo.getEnergyUsageMetaUidById(usage.energy_usage_uid,'tdcharges'),0) as td_usage_meta_uid
					,dbo.getEnergyUsageMetaTypeUidById(nat.meta_group_uid,'tdcharges') as td_meta_type_uid
					,dbo.getEnergyUsageMetaValueById(usage.energy_usage_uid,'supplycharges') as total_supply_cost
					,ISNULL(dbo.getEnergyUsageMetaUidById(usage.energy_usage_uid,'supplycharges'),0) as supply_usage_meta_uid
					,dbo.getEnergyUsageMetaTypeUidById(nat.meta_group_uid,'supplycharges') as supply_meta_type_uid
			from	EnergyBudgetDetail ebd
					join dbo.EnergyUsage usage (nolock)
						on ebd.energy_usage_uid = usage.energy_usage_uid
					join dbo.EnergyAccounts ea  (nolock)
						ON ea.energy_account_uid = usage.energy_account_uid
					join dbo.Properties p (nolock)
						ON ea.property_uid = p.property_uid
					join dbo.EnergyNativeAccounts nat (nolock)
						ON ea.energy_account_uid = nat.energy_account_uid
					join dbo.EnergyProviders prov (nolock)
						ON prov.energy_provider_uid = nat.energy_provider_uid
					join dbo.EnergyUnits units (nolock)
						ON units.energy_unit_uid = ea.energy_unit_uid
					join dbo.MetaTypeGroups mtg (nolock)
						ON nat.meta_group_uid = mtg.meta_group_uid


			where	1=1
					and usage.is_active = 1
					and ea.is_active = 1
					and p.is_active = 1
					and prov.is_active = 1
					and units.is_active = 1
					and mtg.is_active = 1
					and ebd.energy_budget_uid = <cfqueryparam value="#arguments.energy_budget_uid#" cfsqltype="cf_sql_varchar">
					and ( nat.relationship_end is null or nat.relationship_end >= usage.report_date)
							--and nat.relationship_start <= usage.report_date
			order by
					ea.energy_account_uid,
					usage.period_end
		</cfquery>

		<!--- Return the result set --->
		<cfset q = queryToVoArray(q,"mce.e2.vo.EnergyEntryInfo")/>
		<cfreturn q>

	</cffunction>

	<cffunction name="getEnergyUsageRecord"
				returntype="query" access="public"
				hint="This method is used to retrieve one energy usage record from the application database and also includes some additional columns (friendly names, etc).">

		<!--- Intialize the method arguments --->
		<cfargument name="energy_usage_uid" type="string" required="true" hint="Describes the primary key / unique identifier of the energy usage record being retrieved.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize any local variables --->
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#" result="tmpResult">
			select	distinct
					usage.energy_usage_uid,
					usage.period_start,
					usage.period_end,
					<!--- calculate number of days --->
					dbo.getDaysInUsagePeriod(usage.period_start, usage.period_end, types.is_delivery_based) as numberOfDays,
					usage.classification_code,
					usage.recorded_value,
					usage.is_system_calculated,
					usage.usage_type_code,
					usage.usage_status_code,
					usage.usage_status_date,
					usage.report_date,
					ea.energy_account_uid,
					ea.energy_unit_uid,
					p.property_uid,
					nat.native_account_number,
					nat.meta_group_uid,
					ea.friendly_name as energyAccountFriendlyName,
					ea.notes_text as energyAccountNotes,
					p.friendly_name as propertyFriendlyName,
					prov.friendly_name as providerFriendlyName,
					types.friendly_name as energyTypeFriendlyName,
					units.friendly_name as energyUnitFriendlyName,
					sds.friendly_name as documentSetFriendlyName,
					types.is_delivery_based,

					usage.document_set_uid,
					(

						select	count(*)
						from	dbo.storedDocuments sd (nolock)
						where	sd.document_set_uid = usage.document_set_uid
								and sd.is_active = 1

					) as documentSetDocumentCount,

					-- New properties added with UBS requirements
					usage.total_cost,
					usage.user_comments,
					mtg.has_total_cost,
					mtg.has_recorded_value,
					usage.is_active,
					
					tblex.hasBill,
					tblex.hasScreen,
					tblex.created_rate_model_uid,
					tblex.created_meta_group_uid,
					mtgcr.friendly_name as created_meta_group_name,
					rmcr.friendly_name as created_rate_model_name,
					
					mtg.meta_group_uid as current_meta_group_uid,
					mtg.friendly_name as current_meta_group_name,
					
					rm.rate_model_uid as current_rate_model_uid,
					rm.friendly_name as current_rate_model_name,

					-- Retrieve the conversion information
					eu1.friendly_name as conversionEnergyUnitFriendlyName,
					isNull(euc.conversion_factor,1) as conversion_factor,

					-- Name of the ClientCompany that is associated to the property with the Client role, if any
					dbo.getPropertyCompany(p.property_uid,'Client') as propertyClientName

			from	dbo.EnergyUsage usage (nolock)
					join dbo.EnergyAccounts ea  (nolock)
						ON ea.energy_account_uid = usage.energy_account_uid
					
					left outer join EnergyUsage_Conedison tblex (nolock)
						on usage. energy_usage_uid = tblex.energy_usage_uid
					
					left outer join RateModels rmcr (nolock)
						on tblex.created_rate_model_uid = rmcr.rate_model_uid
					left outer join  dbo.MetaTypeGroups mtgcr (nolock)
						on tblex.created_meta_group_uid = mtgcr.meta_group_uid
						
					join dbo.Properties p (nolock)
						ON ea.property_uid = p.property_uid
					join dbo.EnergyNativeAccounts nat (nolock)
						ON ea.energy_account_uid = nat.energy_account_uid
					join dbo.EnergyProviders prov (nolock)
						ON prov.energy_provider_uid = ea.currentEnergyProviderUid
					join dbo.EnergyUnits units (nolock)
						ON units.energy_unit_uid = ea.energy_unit_uid
					join dbo.EnergyTypes types (nolock)
						ON types.energy_type_uid = ea.energy_type_uid
					join dbo.MetaTypeGroups mtg (nolock)
						ON nat.meta_group_uid = mtg.meta_group_uid
					join dbo.EnergyNativeAccounts_EnergyProviderRateClasses enaeprc
						ON enaeprc.native_account_uid = nat.native_account_uid
					
					join dbo.EnergyProviderRateClasses eprc (nolock)
						on enaeprc.rate_class_uid = eprc.rate_class_uid
					join dbo.EnergyProviderRateClasses_RateModels eprcrm (nolock)
						on eprc.rate_class_uid = eprcrm.rate_class_uid
					join dbo.RateModels rm (nolock)
						on eprcrm.rate_model_uid = rm.rate_model_uid
					
					-- Retrieve the document counts / information
					left outer join dbo.StoredDocumentSet sds (nolock)
						on usage.document_set_uid = sds.document_set_uid
						and sds.is_active = 1

					-- Retrieve the conversion information
					left outer join dbo.energyUnitConversions euc (nolock)
						on usage.energy_unit_uid = euc.from_energy_unit_uid
						and euc.normal_metric_conversion = 1
						and euc.is_active = 1
					left outer join dbo.energyUnits eu1 (nolock)
						on euc.to_energy_unit_uid = eu1.energy_unit_uid
						and eu1.is_active = 1

			where	1=1
					<cfif arguments.selectMethod eq 'current'>
					and usage.is_active = 1
					and ea.is_active = 1
					and p.is_active = 1
					and prov.is_active = 1
					and units.is_active = 1
					and mtg.is_active = 1

					and (

							-- Only retrieve active associations
							nat.is_active = 1

							--and ( enaeprc.relationship_end is null or enaeprc.relationship_end >= usage.period_start)
							--and enaeprc.relationship_start <= usage.period_end
							--and usage.usage_type_code = 'actual'
							--and nat.energy_provider_uid = ea.currentEnergyProviderUid
						)
						
					--added 2013-09-20 we need to get the meta_group_uid based on the Account History, so we add additional condition
					and (nat.relationship_start<=usage.period_start 
							or nat.relationship_start is NULL or cast(nat.relationship_start as time)<>'00:00:00' --needed because the relationship_start in EnergyNativeAccount is set to some CURRENT_DATE instead of NULL or 1970-01-01, not shure why and where
						)
					and (nat.relationship_end>=usage.period_end
							or nat.relationship_end is NULL
						)
						
					--added 2013-09-25 we need to get created data entry fields(meta group) and rate model
					and ( enaeprc.is_active = 1
							and enaeprc.relationship_end is null or enaeprc.relationship_end >= usage.period_start 
							and enaeprc.relationship_start <= usage.period_end
						)
					and (

							-- Only retrieve active associations
							enaeprc.is_active = 1
							and ( enaeprc.relationship_end is null or enaeprc.relationship_end >= usage.period_start )
							and enaeprc.relationship_start <= usage.period_end

						)
					and eprc.is_active = 1
					and (

							-- Only retrieve active associations
							eprcrm.is_active = 1
							and ( eprcrm.relationship_end is null or eprcrm.relationship_end >= usage.period_start )
							and eprcrm.relationship_start <= usage.period_end

					)
					and rm.is_active = 1
					</cfif>
					and usage.energy_usage_uid = <cfqueryparam value="#arguments.energy_usage_uid#" cfsqltype="cf_sql_varchar">
							and ( nat.relationship_end is null or nat.relationship_end >= usage.period_start)
							and enaeprc.relationship_start <= usage.period_end
							and usage.usage_type_code = 'actual'

and nat.energy_provider_uid IN(SELECT TOP 1 ep.energy_provider_uid 
	FROM 
		dbo.EnergyProviders ep (nolock), 
		dbo.EnergyNativeAccounts ena (nolock),
		dbo.EnergyNativeAccounts_EnergyProviderRateClasses enaeprc (nolock) /* added jmq - to bring the more recent entry first, experimental*/
	WHERE 
		ep.energy_provider_uid = ena.energy_provider_uid
		AND ena.native_account_uid = enaeprc.native_account_uid
		AND ena.energy_account_uid = nat.energy_account_uid
		AND (enaeprc.relationship_start <= usage.report_date AND isnull(enaeprc.relationship_end, '1/1/5000') >= usage.report_date) /*enaeprc was ena */
		AND ena.is_active = 1)
		</cfquery>
		
		<cfif tmpResult.RecordCount neq 1>
			<cfthrow
				type="com.mcenergyinc.BackOffice.Invalid_entry_period"
				errorcode="InvalidEntryPeriod"
				message="This energy usage entry is no longer valid!"
				detail="This energy usage entry is no longer valid! Either there is no valid Native Account Configuration for it or there are two different Native Account configurations for the period of the Energy Usage.">
		</cfif>
		
		<!--- Return the result set --->
		<cfreturn q>

	</cffunction>

	<cffunction name="getEmptyEnergyUsageRecord"
				returntype="query" access="public"
				hint="This method returns a dummy energy usage record, as if it were a record in the database, and also includes some additional columns (friendly names, etc).">
		<cfargument name="energy_account_uid" type="string" required="true" hint="Describes the primary key of the energy account used to create the empty energy usage record.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start period for the empty energy usage record.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end period for the empty energy usage record.">

		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#" result="tmpResult">

			select	distinct
					null as energy_usage_uid,
					<cfqueryparam value="#arguments.period_start#" cfsqltype="cf_sql_date"> as period_start,
					<cfqueryparam value="#arguments.period_end#" cfsqltype="cf_sql_date"> as period_end,
					<!--- calculate number of days --->
					dbo.getDaysInUsagePeriod(<cfqueryparam value="#arguments.period_start#" cfsqltype="cf_sql_date">, <cfqueryparam value="#arguments.period_end#" cfsqltype="cf_sql_date">, types.is_delivery_based) as numberOfDays,
					dbo.getDateLastInMonth(<cfqueryparam value="#arguments.period_end#" cfsqltype="cf_sql_date">) as report_date,
					null as classification_code,
					null as recorded_value,
					0 is_system_calculated,
					ea.energy_account_uid,
					ea.energy_unit_uid,
					p.property_uid,
					nat.meta_group_uid,
					nat.native_account_number,
					ea.friendly_name as energyAccountFriendlyName,
					ea.notes_text as energyAccountNotes,
					p.friendly_name as propertyFriendlyName,
					prov.friendly_name as providerFriendlyName,
					units.friendly_name as energyUnitFriendlyName,
					types.friendly_name as energyTypeFriendlyName,
					types.is_delivery_based,
					'' as documentSetFriendlyName,
					0 as documentSetDocumentCount,
					1 as is_active,

					-- New properties added with UBS requirements
					0 as total_cost,
					null as user_comments,
					mtg.has_total_cost,
					mtg.has_recorded_value,

					mtg.meta_group_uid as created_meta_group_uid,
					mtg.friendly_name as created_meta_group_name,
					
					rm.rate_model_uid as created_rate_model_uid,
					rm.friendly_name as created_rate_model_name,
					
					-- Retrieve the conversion information
					eu1.friendly_name as conversionEnergyUnitFriendlyName,
					isNull(euc.conversion_factor,1) as conversion_factor,
					
					-- Name of the ClientCompany that is associated to the property with the Client role, if any
					(select friendly_name from ClientCompanies cc1 where cc1.client_company_uid = dbo.getPropertyClient(p.property_uid)) as propertyClientName

			from	dbo.EnergyAccounts ea  (nolock)
					join dbo.Properties p (nolock)
						ON ea.property_uid = p.property_uid
					join dbo.EnergyNativeAccounts nat (nolock)
						ON ea.energy_account_uid = nat.energy_account_uid
					join dbo.EnergyProviders prov (nolock)
						ON prov.energy_provider_uid = nat.energy_provider_uid
					join dbo.EnergyTypes types (nolock)
						ON types.energy_type_uid = ea.energy_type_uid
					join dbo.EnergyUnits units (nolock)
						ON units.energy_unit_uid = ea.energy_unit_uid
					join dbo.MetaTypeGroups mtg (nolock)
						ON nat.meta_group_uid = mtg.meta_group_uid
						
					join dbo.EnergyNativeAccounts_EnergyProviderRateClasses enaeprc (nolock)
						on nat.native_account_uid = enaeprc.native_account_uid
					join dbo.EnergyProviderRateClasses eprc (nolock)
						on enaeprc.rate_class_uid = eprc.rate_class_uid
					join dbo.EnergyProviderRateClasses_RateModels eprcrm (nolock)
						on eprc.rate_class_uid = eprcrm.rate_class_uid
					join dbo.RateModels rm (nolock)
						on eprcrm.rate_model_uid = rm.rate_model_uid

					-- Retrieve the conversion information
					left outer join dbo.energyUnitConversions euc (nolock)
						on units.energy_unit_uid = euc.from_energy_unit_uid
						and euc.normal_metric_conversion = 1
						and euc.is_active = 1
					left outer join dbo.energyUnits eu1 (nolock)
						on euc.to_energy_unit_uid = eu1.energy_unit_uid
						and eu1.is_active = 1

			where	1=1
					and ea.is_active = 1
					and p.is_active = 1
					and prov.is_active = 1
					and units.is_active = 1
					and mtg.is_active = 1

					and ea.energy_account_uid = <cfqueryparam value="#arguments.energy_account_uid#" cfsqltype="cf_sql_idstamp">
					
					--added 2013-09-20 we need to get the meta_group_uid based on the Account History, so we add additional condition
					and nat.is_active = 1
					and (nat.relationship_start<=<cfqueryparam value="#arguments.period_start#" cfsqltype="cf_sql_date"> 
							or nat.relationship_start is NULL or cast(nat.relationship_start as time)<>'00:00:00' --needed because the relationship_start in EnergyNativeAccount is set to some CURRENT_DATE instead of NULL or 1970-01-01, not shure why and where
						)
					and (nat.relationship_end>=<cfqueryparam value="#arguments.period_end#" cfsqltype="cf_sql_date">
							or nat.relationship_end is NULL
						)
						
					--added 2013-09-25 we need to get created data entry fields(meta group) and rate model
					and ( enaeprc.is_active = 1
							and enaeprc.relationship_end is null or enaeprc.relationship_end >= <cfqueryparam value="#arguments.period_start#" cfsqltype="cf_sql_date"> 
							and enaeprc.relationship_start <= <cfqueryparam value="#arguments.period_end#" cfsqltype="cf_sql_date">
						)
					and (

							-- Only retrieve active associations
							enaeprc.is_active = 1
							and ( enaeprc.relationship_end is null or enaeprc.relationship_end >= <cfqueryparam value="#arguments.period_start#" cfsqltype="cf_sql_date"> )
							and enaeprc.relationship_start <= <cfqueryparam value="#arguments.period_end#" cfsqltype="cf_sql_date">

						)
					and eprc.is_active = 1
					and (

							-- Only retrieve active associations
							eprcrm.is_active = 1
							and ( eprcrm.relationship_end is null or eprcrm.relationship_end >= <cfqueryparam value="#arguments.period_start#" cfsqltype="cf_sql_date"> )
							and eprcrm.relationship_start <= <cfqueryparam value="#arguments.period_end#" cfsqltype="cf_sql_date">

					)
					and rm.is_active = 1

		</cfquery>
		
		<!---If the query returns 0 or more than 1 row then there is something wrong with the period or account history so we throw an exception, not sure if this should be here but
		this function is used only when the user creates a new EnergyUsageEntry so it seems appropriate for now--->

		<cfif tmpResult.RecordCount neq 1>
			<cfthrow
				type="com.mcenergyinc.BackOffice.Invalid_entry_period"
				errorcode="InvalidEntryPeriod"
				message="The provided period for the new energy usage entry is not correct!"
				detail="The provided period for the new energy usage entry is not correct! Please check the account history and ensure that each energy entry is completely in bounds of only one native energy account.">
		</cfif>
		
		<cfreturn q>

	</cffunction>

	<!--- Note: This was adapted from rate model project... should perhaps consolidate into single delegate --->
	<cffunction name="getEnergyUsageMetaForPeriod"
				access="public" returntype="query"
				hint="This method is used to retrieve n rows of EnergyUsageMeta records from the database.">

		<!--- Intialize the method arguments --->
		<cfargument name="energy_account_uid" type="string" required="true" hint="Describes the primary key of the energy account used to retrieve the meta data.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start period used to retrieve the meta data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end period used to retrieve the meta data.">
		<cfargument name="meta_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of the meta group from which meta data will be retrieved.">
		<cfargument name="userPermissions" required="false" type="string" hint="The permissions that the current user has.">

		<!--- Initialize the local scope --->
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT
				meta.energy_usage_uid,
				types.type_lookup_code,
				types.friendly_name,
				types.data_type,
				meta.meta_value,
				meta.meta_value as meta_value_before_edit,
				meta.meta_type_uid,
				meta.usage_meta_uid,
				rel1.consider_touched,
				rel1.variance_enabled,
				rel1.tolerance_warning,
				rel1.tolerance_error,
				usage.total_cost,
				usage.recorded_value,
				types.data_type,
				meta.is_active,

				meta.energy_unit_uid,
				eu1.friendly_name as energyUnitFriendlyName,

				-- Retrieve the conversion information
				rel1.unit_conversion_enabled,
				eu1.friendly_name as conversionEnergyUnitFriendlyName,
				isNull(euc.conversion_factor,1) as conversion_factor,

				types.is_cost_component,
				types.is_usage_component,
				types.is_system_calculated,
				rel1.is_required,

				<!--- Return a column that indicates whether user has permission to edit --->
				<cfif structKeyExists(arguments, 'userPermissions')>
					CASE WHEN permission_required_to_edit IN ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
					THEN 1 ELSE 0 END AS hasPermissionToEdit,
				</cfif>

				<!--- Return a column that indicates the number of related choices (if any) --->
				(SELECT COUNT(*) FROM EnergyUsageMetaChoices choices (nolock) WHERE choices.meta_type_uid = types.meta_type_uid)
				AS numMetaChoices

			FROM
				dbo.EnergyUsage usage (nolock)
				JOIN dbo.EnergyUsageMeta meta (nolock)
					ON usage.energy_usage_uid = meta.energy_usage_uid
				JOIN dbo.EnergyUsageMetaTypes types (nolock)
					ON meta.meta_type_uid = types.meta_type_uid
				JOIN dbo.MetaTypeGroups_MetaTypes rel1 (nolock)
					ON types.meta_type_uid = rel1.meta_type_uid

				-- Retrieve the conversion information
				left outer join dbo.energyUnitConversions euc (nolock)
					on meta.energy_unit_uid = euc.from_energy_unit_uid
					and euc.normal_metric_conversion = 1
					and euc.is_active = 1
				left outer join dbo.energyUnits eu1 (nolock)
					on euc.to_energy_unit_uid = eu1.energy_unit_uid
					and eu1.is_active = 1

			WHERE
				usage.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energy_account_uid#"> AND
				usage.period_start >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_start#"> AND
				usage.period_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_end#">

				<cfif structkeyexists(arguments, 'meta_group_uid')>
				and rel1.meta_group_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#meta_group_uid#" list="true">
				</cfif>

				<cfif structKeyExists(arguments, 'userPermissions')>
				and rel1.permission_required_to_view in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
				</cfif>

			ORDER BY
				rel1.display_order,
				types.friendly_name
		</cfquery>

		<!--- Return the result set--->
		<cfreturn q>

	</cffunction>

	<!--- Note: This was adapted from rate model project... should perhaps consolidate into single delegate --->
	<cffunction name="getEmptyEnergyUsageMeta"
				access="public" returntype="query"
				hint="This method is used to retrieve n rows of empty EnergyUsageMeta records from the database.">

		<!--- Intialize the method arguments --->
		<cfargument name="meta_group_uid" type="string" hint="Describes the primary key / unique identifier of the meta group from which meta data will be retrieved.">
		<cfargument name="userPermissions" required="false" type="string" hint="The permissions that the current user has.">
		<cfargument name="type_lookup_code" required="false" type="string" hint="">

		<!--- Initialize the local scope --->
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT
				rel1.display_order,
				'' as energy_usage_uid,
				types.type_lookup_code,
				types.friendly_name,
				types.data_type,
				'' as meta_value,
				'' as meta_value_before_edit,
				types.meta_type_uid,
				'' as usage_meta_uid,
				rel1.consider_touched,
				rel1.variance_enabled,
				rel1.tolerance_warning,
				rel1.tolerance_error,
				rel1.type_group_lookup_code,
				types.energy_unit_uid,
				eu2.friendly_name as energyUnitFriendlyName,

				-- Retrieve the conversion information
				rel1.unit_conversion_enabled,
				eu1.friendly_name as conversionEnergyUnitFriendlyName,
				isNull(euc.conversion_factor,1) as conversion_factor,

				types.is_cost_component,
				types.is_usage_component,
				types.is_system_calculated,
				types.data_type,
				rel1.is_required,
				1 as is_active,

				<!--- Return a column that indicates whether user has permission to edit --->
				<cfif structKeyExists(arguments, 'userPermissions')>
					CASE WHEN permission_required_to_edit IN ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
					THEN 1 ELSE 0 END AS hasPermissionToEdit,
				</cfif>

				<!--- Return a column that indicates the number of related choices (if any) --->
				(SELECT COUNT(*) FROM EnergyUsageMetaChoices choices (nolock) WHERE choices.meta_type_uid = types.meta_type_uid)
				AS numMetaChoices
			FROM
				dbo.EnergyUsageMetaTypes types
				JOIN dbo.MetaTypeGroups_MetaTypes rel1
					ON types.meta_type_uid = rel1.meta_type_uid

				-- Retrieve all meta type groups
				join dbo.metaTypeGroups mtg (nolock)
					on mtg.meta_group_uid = rel1.meta_group_uid

				-- Retrieve the conversion information
				left outer join dbo.energyUnitConversions euc (nolock)
					on types.energy_unit_uid = euc.from_energy_unit_uid
					and euc.normal_metric_conversion = 1
					and euc.is_active = 1

				left outer join dbo.energyUnits eu1 (nolock)
					on euc.to_energy_unit_uid = eu1.energy_unit_uid
					and eu1.is_active = 1

				left outer join dbo.energyUnits eu2 (nolock)
					on euc.from_energy_unit_uid = eu2.energy_unit_uid
					and eu2.is_active = 1
			WHERE

				1=1
				and types.is_active = 1
				and rel1.is_active = 1
				and mtg.is_active = 1

				<cfif structKeyExists(arguments, 'meta_group_uid')>
				and rel1.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.meta_group_uid#" list="true" null="false"> )
				</cfif>

				<cfif structKeyExists(arguments, 'userPermissions')>
				and rel1.permission_required_to_view in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
				</cfif>

				<cfif structKeyExists(arguments, 'type_lookup_code')>
				and types.type_lookup_code IN ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.type_lookup_code#" list="true" null="false"> )
				</cfif>

			ORDER BY
				rel1.display_order,
				types.friendly_name
		</cfquery>

		<!--- Return the result set--->
		<cfreturn q>
	</cffunction>


	<cffunction name="getEnergyUsageMeta"
				access="public" returntype="query"
				hint="This method is used to retrieve energy usage meta data from the application database.">

		<!--- Intialize the method arguments --->
		<cfargument name="energy_usage_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the energy usage record being retrieved.">
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the meta group from which meta data will be retrieved.">
		<cfargument name="userPermissions" required="false" type="string" hint="The permissions that the current user has.">

		<!--- Initialize the local scope --->
		<cfset var q = "">
		<cfset var qOtherMeta = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT

				distinct
				rel1.display_order,
				meta.energy_usage_uid,
				types.type_lookup_code,
				types.friendly_name,
				types.data_type,
				meta.meta_value,
				meta.meta_value as meta_value_before_edit,
				meta.meta_type_uid,
				meta.usage_meta_uid,
				rel1.consider_touched,
				rel1.variance_enabled,
				rel1.tolerance_warning,
				rel1.tolerance_error,
				rel1.type_group_lookup_code,
				meta.energy_unit_uid,
				eu2.friendly_name as energyUnitFriendlyName,

				-- Retrieve the conversion information
				rel1.unit_conversion_enabled,
				eu1.friendly_name as conversionEnergyUnitFriendlyName,
				isNull(euc.conversion_factor,1) as conversion_factor,

				types.is_cost_component,
				types.is_usage_component,
				types.is_system_calculated,
				types.data_type,
				rel1.is_required,
				meta.is_active,

				<!--- Return a column that indicates whether user has permission to edit --->
				<cfif structKeyExists(arguments, 'userPermissions')>
					CASE WHEN permission_required_to_edit IN ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
					THEN 1 ELSE 0 END AS hasPermissionToEdit,
				</cfif>

				<!--- Return a column that indicates the number of related choices (if any) --->
				(SELECT COUNT(*) FROM EnergyUsageMetaChoices choices (nolock) WHERE choices.meta_type_uid = types.meta_type_uid)
				AS numMetaChoices
			FROM
				dbo.EnergyUsage usage
				JOIN dbo.EnergyUsageMeta meta
					ON usage.energy_usage_uid = meta.energy_usage_uid
				JOIN dbo.EnergyUsageMetaTypes types
					ON meta.meta_type_uid = types.meta_type_uid
				JOIN dbo.MetaTypeGroups_MetaTypes rel1
					ON types.meta_type_uid = rel1.meta_type_uid

				-- Retrieve the associated meta group data
				join dbo.energyAccounts ea (nolock)
					on usage.energy_account_uid = ea.energy_account_uid
				join dbo.EnergyNativeAccounts ena (nolock)
					on ea.energy_account_uid = ena.energy_account_uid
				join dbo.metaTypeGroups mtg (nolock)
					on ena.meta_group_uid = mtg.meta_group_uid
					and mtg.meta_group_uid = rel1.meta_group_uid

				-- Retrieve the conversion information
				left outer join dbo.energyUnitConversions euc (nolock)
					on meta.energy_unit_uid = euc.from_energy_unit_uid
					and euc.normal_metric_conversion = 1
					and euc.is_active = 1
				left outer join dbo.energyUnits eu1 (nolock)
					on euc.to_energy_unit_uid = eu1.energy_unit_uid
					and eu1.is_active = 1
				left outer join dbo.energyUnits eu2 (nolock)
					on meta.energy_unit_uid = eu2.energy_unit_uid
					and eu2.is_active = 1

			WHERE	1=1

				and usage.is_active = 1
				and meta.is_active = 1
				and types.is_active = 1
				and rel1.is_active = 1
				and ea.is_active = 1
				and mtg.is_active = 1

				<cfif structKeyExists(arguments, 'energy_usage_uid')>
				and usage.energy_usage_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energy_usage_uid#" list="true" null="false"> )
				</cfif>

				<cfif structKeyExists(arguments, 'meta_group_uid')>
				and rel1.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.meta_group_uid#" list="true" null="false"> )
				</cfif>

				<cfif structKeyExists(arguments, 'userPermissions')>
				and rel1.permission_required_to_view in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
				</cfif>

			ORDER BY
				rel1.display_order,
				types.friendly_name
		</cfquery>

		<cfset qOtherMeta = getEmptyEnergyUsageMeta(arguments.meta_group_uid, arguments.userPermissions, "total_cost,recorded_value")>

		<cfquery name="q" dbtype="query">
			SELECT * FROM q
			UNION
			SELECT * FROM qOtherMeta
			ORDER BY display_order
		</cfquery>

		<!--- Return the result set--->
		<cfreturn q>

	</cffunction>


	<cffunction name="applyChoicesToEnergyUsageMeta" access="private">
		<cfargument name="metaFields" type="array" required="true">

		<cfset var qChoices = "">
		<cfset var thisMeta = "">

		<cfloop array="#metaFields#" index="thisMeta">
			<cfif thisMeta.numMetaChoices gt 0>
				<cfset qChoices = getEnergyUsageMetaChoices(thisMeta.meta_type_uid)>
				<cfset thisMeta.metaChoices = queryToVoArray(qChoices, "mce.e2.vo.EnergyEntryMetaChoice")>
			<cfelse>
				<cfset thisMeta.metaChoices = ArrayNew(1)>
			</cfif>
		</cfloop>

	</cffunction>



	<cffunction name="getEnergyUsageMetaChoices" returntype="query" access="public">
		<cfargument name="meta_type_uid" required="true" type="string">
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT
				*
			FROM
				EnergyUsageMetaChoices
			WHERE
				meta_type_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.meta_type_uid#" list="true">)
			ORDER BY
				display_order,
				friendly_name
		</cfquery>

		<cfreturn q>
	</cffunction>


	<cffunction name="isShouldBeConsideredTouched" returntype="boolean">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" required="true">

		<cfset var thisMeta = "">
		<cfset var qCheck = "">

		<!--- Check to see if any of the meta field values have changed --->
		<!--- This includes the "fake" meta fields for total_cost, recorded_value, etc., if present --->
		<cfloop array="#entryInfo.metaFields#" index="thisMeta">
			<cfif thisMeta.consider_touched and (thisMeta.meta_value neq thisMeta.meta_value_before_edit)>
				<cfreturn true>
			</cfif>
		</cfloop>

		<!--- If we get this far, the meta fields haven't changed; what we'll check now is whether the energy_usage_status --->
		<!--- has gone from a "reportable" status code to an "unreportable" one, or vice-versa --->
		<cfquery name="qCheck" datasource="#this.datasource#">
			SELECT
				eus.data_is_reportable AS data_is_reportable_old,
				eus2.data_is_reportable AS data_is_reportable_new
			FROM
				EnergyUsage eu INNER JOIN
				EnergyUsageStatuses eus ON eu.usage_status_code = eus.usage_status_code,
				EnergyUsageStatuses eus2
			WHERE
				energy_usage_uid = '#entryInfo.energy_usage_uid#' <!--- To get existing data_is_reportable in db now --->
				AND eus2.usage_status_code = '#entryInfo.usage_status_code#' <!--- To get data_is_reportable for new status --->
		</cfquery>

		<!--- Return true if the quick query above returns that the reportable-nesses of the statuses have changed --->
		<cfif qCheck.data_is_reportable_old neq qCheck.data_is_reportable_new>
			<cfreturn true>
		</cfif>

		<!--- If we get this far then we'll consider the energy usage record "not touched" in any meaningful way --->
		<cfreturn false>
	</cffunction>



	<cffunction name="isHistoryStatusChanged" access="private" returntype="boolean">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" required="true" hint="Describes the VO containing the energy usage record that will be persisted.">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<cfquery name="local.qResult" datasource="#this.datasource#">
			select	usage_status_code, user_comments
			from	EnergyUsage
			where	energy_usage_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.entryInfo.energy_usage_uid#" null="false" list="false">
		</cfquery>

		<cfreturn
			local.qResult.recordcount eq 0
			or (arguments.entryInfo.usage_status_code neq local.qResult.usage_status_code)
			or (trim(arguments.entryInfo.user_comments) neq trim(local.qResult.user_comments))
		>
	</cffunction>


	<cffunction name="saveEnergyUsageStatusHistory"
				hint="This method is used to persist an existing Energy Usage Status History record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyUsageStatusHistory" required="true" type="any" hint="Describes the VO containing the Energy Usage Status History record that will be persisted."/>

		<!--- Persist the Energy Usage Status History data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyUsageStatusHistory", arguments.EnergyUsageStatusHistory)/>

	</cffunction>

	<cffunction name="createNewEnergyUsageStatusHistory" access="private" returntype="mce.e2.vo.EnergyUsageStatusHistory">
		<cfargument name="entryInfo" required="true" type="any" hint="Describes the VO containing the Energy Entry record that will be persisted."/>
		<cfargument name="user_uid" required="false" type="string" default="true" hint="User uid to put on the history record"/>

		<cfset local.history = createObject("component", "mce.e2.vo.EnergyUsageStatusHistory")>
		<cfset local.history.energy_usage_uid = arguments.entryInfo.energy_usage_uid>
		<cfset local.history.user_uid = arguments.user_uid>
		<cfset local.history.usage_status_date = now()>
		<cfset local.history.usage_status_code = arguments.entryInfo.usage_status_code>
		<cfset local.history.user_comments = arguments.entryInfo.user_comments>
		<cfreturn local.history>

	</cffunction>

	<cffunction name="saveExistingEnergyUsageEntry"
				access="public" returntype="void"
				hint="This method is used to persist an energy usage record to the application database.">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" hint="Describes the VO containing the energy usage record that will be persisted.">
		<cfargument name="addHistory" required="false" type="boolean" default="true" hint="Allow internal users to change the status without affecting the history."/>
		<cfargument name="user_uid" required="false" type="string" default="true" hint="User uid to put on the history record"/>

		<cfset var meta = "">
		<cfset var i = 0>
		<cfset var historyStatusChanged = isHistoryStatusChanged(arguments.entryInfo)>
		<cfset var shouldConsiderTouched = isShouldBeConsideredTouched(arguments.entryInfo)>
		
		<cfset var local = structnew()>

		<!--- The "consider_touched_date" should be set to "now" if the new data is different in meaningful respects from what was in the db previously --->
		<cfif shouldConsiderTouched>
			<cfset entryInfo.consider_touched_date = now()>
		</cfif>

		<!--- If we sent "fake" meta fields down to the UI to represent total_cost, recorded_value, etc., they will be coming back to us at this point --->
		<cfset unpackTopLevelValuesFromFakeMetaFields(entryInfo)>

		<!--- This is a multi-step process (master and detail records), so open a transaction in case anything goes wrong --->
		<cftransaction action="begin">
			<!--- If addHistory, check usage_status_code --->
			<cfif arguments.addHistory>
				<cfif historyStatusChanged>
					<cfset local.history = createNewEnergyUsageStatusHistory(arguments.entryInfo, arguments.user_uid)>
					<cfset saveEnergyUsageStatusHistory(local.history)>
				</cfif>
			</cfif>

			<!--- First, update the main EnergyUsage record --->
			<cfset this.dataMgr.updateRecord("EnergyUsage", entryInfo)>
			
			<!---Updating the hasBill, hasScreen and is_system_calculated--->
			<cfset local.euExt = createObject("component", "mce.e2.vo.EnergyUsage")>
			<cfset local.euExt.energy_usage_uid = arguments.entryInfo.energy_usage_uid > 
			<cfset local.euExt.hasBill = arguments.entryInfo.hasBill > 
			<cfset local.euExt.hasScreen = arguments.entryInfo.hasScreen> 
			<cftry>
			<cfset this.dataMgr.updateRecord("EnergyUsage_Conedison", local.euExt)/>
			 <cfcatch type = "DataMgr">
				<cfset local.newId=this.dataMgr.insertRecord("EnergyUsage_Conedison", local.euExt)>
			 </cfcatch>
			</cftry>

			<!--- Now persist each related EnergeUsageMeta record  --->
			<cfloop from="1" to="#ArrayLen(entryInfo.metaFields)#" index="i">
				<cfset meta = entryInfo.metaFields[i]>
				<cfset this.dataMgr.updateRecord("EnergyUsageMeta", meta)>
			</cfloop>

			<!--- Now we can commit the transaction --->
			<cftransaction action="commit"/>
		</cftransaction>
	</cffunction>

	<cffunction name="saveNewEnergyUsageEntry"
				access="public" returntype="string"
				hint="This method is used to persist a new energy usage record to the application database.">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" hint="Describes the VO containing the energy usage record that will be persisted.">
		<cfargument name="user_uid" required="false" type="string" default="true" hint="User uid to put on the history record" >

		<cfset var newUId = "">
		<cfset var meta = "">
		<cfset var i = 0>
		
		<cfset var local = structnew()>

		<!--- The "consider_touched_date" should be "now" for new records --->
		<cfset entryInfo.consider_touched_date = now()>

		<!--- If we sent "fake" meta fields down to the UI to represent total_cost, recorded_value, etc., they will be coming back to us at this point --->
		<cfset unpackTopLevelValuesFromFakeMetaFields(entryInfo)>

		<!--- Just in case the UI does not provide --->
		<cfif not isDefined("arguments.entryInfo.usage_status_code")>
			<cfset arguments.entryInfo.usage_status_code= "OK">
		</cfif>

		<!--- This is a multi-step process (master and detail records), so open a transaction in case anything goes wrong --->
		<cftransaction action="begin">

			<!--- First, perist the main EnergyUsage record and get its ID --->
			<cfset newUid = this.dataMgr.insertRecord("EnergyUsage", entryInfo)>

			<cfset arguments.entryInfo.energy_usage_uid = newUid>
			<cfset local.history = createNewEnergyUsageStatusHistory(arguments.entryInfo, arguments.user_uid)>
			<cfset saveEnergyUsageStatusHistory(local.history)>
			
			<!---Updating the hasBill, hasScreen and is_system_calculated--->
			<cfset local.euExt = createObject("component", "mce.e2.vo.EnergyUsage")>
			<cfset local.euExt.energy_usage_uid = newUid > 
			<cfset local.euExt.hasBill = arguments.entryInfo.hasBill > 
			<cfset local.euExt.hasScreen = arguments.entryInfo.hasScreen> 
			<cfset local.euExt.created_rate_model_uid = arguments.entryInfo.created_rate_model_uid> 
			<cfset local.euExt.created_meta_group_uid = arguments.entryInfo.created_meta_group_uid> 
			<cfset local.newId=this.dataMgr.insertRecord("EnergyUsage_Conedison", local.euExt)>

			<!--- Now persist a related EnergeUsageMeta record for each meta field item --->
			<cfloop from="1" to="#ArrayLen(entryInfo.metaFields)#" index="i">
				<cfset meta = entryInfo.metaFields[i]>
				<cfset meta.energy_usage_uid = newUid>
				<cfset meta.usage_meta_uid = "">
				<cfset this.dataMgr.insertRecord("EnergyUsageMeta", meta)>
			</cfloop>

			<!--- Now we can commit the transaction --->
			<cftransaction action="commit"/>
		</cftransaction>

		<cfreturn newUid>
	</cffunction>

	<cffunction name="getPriorPeriodsFromDefaultOffsets"
				access="private" returntype="array"
				hint="This method is used internally to retrieve prior values based on default offsets.">

		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="priorPeriodDefaultOffsets" type="string" required="true">
		<cfargument name="usageTypeCodeForDefaultPriorPeriods" type="string" required="true" hint="The usage type to get prior records for">
		<cfargument name="report_date" type="string" required="true" hint="The report date to get prior records offset against">

		<cfset var qPrior = "">
		<cfset var result = ArrayNew(1)>
		<cfset var offsetInMonths = 0>
		<cfset var reportDateMonthForThisPriorPeriod = "">
		<cfset var newItem = "">

		<!--- Round the report_date to the start of the month --->
		<cfset report_date = CreateDate(Year(report_date), Month(report_date), 1)>

		<!--- The "priorPeriodDefaultOffsets" is probably a single number like -12 to indicate "same period from prior year" --->
		<!--- But it could be a comma-separated list of such numbers, so use a loop even though we anticipate it to usually loop just once --->
		<cfloop list="#arguments.priorPeriodDefaultOffsets#" index="offsetInMonths">
			<cfset reportDateMonthForThisPriorPeriod = DateAdd("m", -offsetInMonths, report_date)>

			<cfquery name="qPrior" datasource="#this.datasource#">
				SELECT TOP 1
					eu.energy_usage_uid,
					eu.period_start,
					eu.period_end,
					dbo.getDaysInUsagePeriod(eu.period_start, eu.period_end, 0) as numberOfDays
				FROM
					EnergyUsage eu (nolock)
				WHERE
					eu.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#"> AND
					eu.is_active = 1  AND
					eu.usage_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usageTypeCodeForDefaultPriorPeriods#"> AND
					YEAR(eu.report_date) = #Year(reportDateMonthForThisPriorPeriod)# AND
					MONTH(eu.report_date) = #Month(reportDateMonthForThisPriorPeriod)#
				ORDER BY
					eu.period_end DESC
			</cfquery>

			<cfif qPrior.recordCount gt 0>
				<cfset newItem = {period_start=qPrior.period_start, period_end=qPrior.period_end, energy_usage_uid=qPrior.energy_usage_uid, numberOfDays=qPrior.numberOfDays}>
				<cfset ArrayAppend(result, newItem)>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="addPriorRecordsToEnergyEntryInfo"
				access="private" returntype="void"
				hint="This method is used internally to retrieve prior values and add them to an EnergyEntryInfo object.">

		<!--- Define the arguments for this method --->
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" required="true" hint="Describes the energyEntryInfo object that prior periods of data will be added to.">
		<cfargument name="priorPeriods" type="array" required="true" hint="Describes the prior periods of data that will be added to the specified EnergyEntryInfo object.">
		<cfargument name="userPermissions" type="string" required="true">

		<!--- Initialize local variables --->
		<cfset var i = "">
		<cfset var qPriorEnergyUsageMeta = "">
		<cfset var entryMeta = "">
		<cfset var row = "">
		<cfset var priorValueIndex = "">

		<!--- Initialize the entry information prior period array --->
		<cfset entryInfo.priorPeriods = ArrayNew(1)>

		<!--- Loop over the prior periods ---->
		<cfloop from="1" to="#ArrayLen(priorPeriods)#" index="priorValueIndex">

			<!--- Append the prior period to the entry information array --->
			<cfset ArrayAppend(entryInfo.priorPeriods, priorPeriods[priorValueIndex])>

			<!--- Get the prior energy usage meta data --->
			<cfset qPriorEnergyUsageMeta = getEnergyUsageMetaForPeriod(
				energy_account_uid = entryInfo.energy_account_uid,
				period_start = priorPeriods[priorValueIndex].period_start,
				period_end = priorPeriods[priorValueIndex].period_end,
				userPermissions = userPermissions)>

			<!--- Loop over each of the meta fields --->
			<cfloop from="1" to="#ArrayLen(entryInfo.metaFields)#" index="i">

				<!--- Create a reference to the current field being processed --->
				<cfset entryMeta = entryInfo.metaFields[i]>

				<!--- Is this the first entry being processed? --->
				<cfif priorValueIndex eq 1>
					<cfset entryMeta.priorValues = ArrayNew(1)>
				</cfif>

				<!--- Calculate the row number being processed --->
				<cfset row = ListFind(ValueList(qPriorEnergyUsageMeta.meta_type_uid), entryMeta.meta_type_uid)>

				<!--- Is the row number valid? --->
				<cfif row gt 0>

					<!--- If so, then append the results --->
					<cfset ArrayAppend(entryMeta.priorValues, qPriorEnergyUsageMeta.meta_value[row])>

				</cfif>

			</cfloop>

		</cfloop>
	</cffunction>



	<!--- Related to "Fake" Meta Fields for use by the UI --->
	<cffunction name="packTopLevelValuesIntoFakeMetaFields">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" required="true">
		<cfargument name="priorPeriods" type="array" required="true">

		<cfset var priorTotalCost = "">
		<cfset var priorRecordedValue = "">
		<cfset var qPriorRecord = "">

		<cfif ArrayLen(priorPeriods) gt 0>
			<cfset qPriorRecord = getEnergyUsageRecord(priorPeriods[1].energy_usage_uid)>
			<Cfset priorTotalCost = qPriorRecord.total_cost>
			<Cfset priorRecordedValue = qPriorRecord.recorded_value>
		</cfif>

		<cfloop array="#entryInfo.metaFields#" index="metaField">
			<cfswitch expression="#metaField.type_lookup_code#">
				<cfcase value="total_cost">
					<cfset metaField.meta_value = entryInfo.total_cost>
					<cfset metaField.meta_value_before_edit = metaField.meta_value>
					<cfset metaField.priorValues = [priorTotalCost]>
				</cfcase>
				<cfcase value="recorded_value">
					<cfset metaField.meta_value = entryInfo.recorded_value>
					<cfset metaField.meta_value_before_edit = metaField.meta_value>
					<cfset metaField.priorValues = [priorRecordedValue]>
					<cfset metaField.energy_unit_uid = entryInfo.energy_unit_uid>
					<cfset metaField.energyUnitFriendlyName = entryInfo.energyUnitFriendlyName>
					<cfset metaField.unit_conversion_enabled = entryInfo.conversionEnergyUnitFriendlyName neq "">
					<cfset metaField.conversion_factor = entryInfo.conversion_factor>
					<cfset metaField.conversionEnergyUnitFriendlyName = entryInfo.conversionEnergyUnitFriendlyName>
				</cfcase>
			</cfswitch>
		</cfloop>
	</cffunction>

	<!--- Related to "Fake" Meta Fields for use by the UI --->
	<cffunction name="unpackTopLevelValuesFromFakeMetaFields">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" required="true">

		<cfset var i = 0>

		<cfloop from="#ArrayLen(entryInfo.metaFields)#" to="1" step="-1" index="i">
			<cfswitch expression="#entryInfo.metaFields[i].type_lookup_code#">
				<cfcase value="total_cost">
					<cfset entryInfo.total_cost = entryInfo.metaFields[i].meta_value>
					<cfset ArrayDeleteAt(entryInfo.metaFields, i)>
				</cfcase>
				<cfcase value="recorded_value">
					<cfset entryInfo.recorded_value = entryInfo.metaFields[i].meta_value>
					<cfset ArrayDeleteAt(entryInfo.metaFields, i)>
				</cfcase>
			</cfswitch>
		</cfloop>
	</cffunction>


	<cffunction name="getPotentialDuplicateRecords"
				access="public" returntype="query"
				hint="This method is used to determine whether an energy usage record already exists for a given time period.">
		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="period_start" type="date" required="false">
		<cfargument name="period_end" type="date" required="false">
		<cfargument name="usage_type_code" type="string" required="false" default="actual">
		<cfargument name="report_date" type="date" required="false">
		<cfargument name="not_energy_usage_uid" type="string" required="false" default="">

		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT
				energy_usage_uid,
				period_start,
				period_end,
				usage_type_code,
				usage_status_code
			FROM
				EnergyUsage tbl
			WHERE
				tbl.is_active = 1
				AND tbl.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energy_account_uid#">
				AND tbl.usage_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usage_type_code#">
				<!--- If report_date was passed, base our results on that --->
				<cfif isDefined("arguments.report_date")>
					AND tbl.report_date = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.report_date#">
				<!--- Otherwise, we can base results on period start/end dates --->
				<cfelseif isDefined("arguments.period_start") AND isDefined("arguments.period_end")>
					AND (
						(tbl.period_end > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.period_start#">
						 AND tbl.period_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.period_end#">)
						OR
						(tbl.period_start = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.period_start#">)
					)
				</cfif>
				<cfif arguments.not_energy_usage_uid neq "">
					AND tbl.energy_usage_uid <> <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.not_energy_usage_uid#">
				</cfif>
			ORDER BY
				period_end, period_start
		</cfquery>

		<cfreturn q>
	</cffunction>

	<cffunction name="deleteExistingEnergyUsageEntry"
				access="public" returntype="void"
				hint="This method is used to persist an energy usage record to the application database.">
		<cfargument name="energy_usage_uid" type="string" hint="Describes the primary key of the energy usage record that will be deleted.">
		<cfargument name="user_uid" required="false" type="string" default="true" hint="User uid to put on the history record"/>
		
		<cfset var qResult = ''>
		<cfstoredproc datasource="#this.datasource#" procedure="deleteEnergyUsageRecord">
			<cfprocparam value="#arguments.energy_usage_uid#" cfsqltype="CF_SQL_IDSTAMP">
			<cfprocparam value="#arguments.user_uid#" cfsqltype="CF_SQL_IDSTAMP">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
			<!--- Return the result set --->
			<!--- <cfreturn qResult> --->
	</cffunction>
	
	<cffunction name="getEnergyEntryTax"
				access="public" returntype="array"
				hint="This method returns sales taxes for energy entry.">

		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">

		<cfset var result = ArrayNew(1)>
		
		<cfquery name="query1" datasource="#this.datasource#" result="tmpResult">
			SELECT * 
			FROM dbo.getTaxesForAccount(<cfqueryparam cfsqltype="cf_sql_idstamp" value="#energy_account_uid#">, <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_end#">)
		</cfquery>
		
		<cfif tmpResult.RecordCount neq 1>
			<cfthrow
				type="com.mcenergyinc.RateModel.NoTaxesData"
				errorcode="NoTaxesData"
				message="There are no taxes in the database!"
				detail="The reason is that the property doesn't have a postal_code or there are no tax factors associated with this postal_code!">
		</cfif>
		
		<cfset arrayAppend(result, query1.commodity_tax)>
		<cfset arrayAppend(result, query1.delivery_tax)>
		<cfset arrayAppend(result, query1.sales_tax)>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getEnergyEntryTripCode" access="public" returntype="numeric">
		<cfargument name="propertyUid" type="string">
		<cfargument name="asOfDate" type="date">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			select
				ISNULL(dbo.getPropertyTripCode(<cfqueryparam cfsqltype="cf_sql_varchar" value="#propertyUid#">, <cfqueryparam cfsqltype="cf_sql_timestamp" value="#asOfDate#">), -1) as trip_code			
		</cfquery>		
		
		<cfreturn result.trip_code>
	</cffunction>
</cfcomponent>