
<!---

  Template Name:  PropertyMetaDelegate
     Base Table:  PropertyMeta

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Tuesday, March 31, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Tuesday, March 31, 2009 - Template Created.

--->

<cfcomponent displayname="PropertyMetaDelegate"
				hint="This CFC manages all data access interactions for the PropertyMeta table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<cffunction name="getRateFactorValueFromPropertyMeta" access="public" returntype="numeric">
		<cfargument name="property_uid" type="string" required="true">
		<cfargument name="type_lookup_code" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT dbo.getRateFactorValueFromPropertyMeta('#property_uid#', '#type_lookup_code#', DEFAULT) AS factorValue
		</cfquery>

		<cfreturn val(qResult.factorValue)>
	</cffunction>

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyMetaComponent"
				hint="This method is used to retrieve a Property Meta Data object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.PropertyMeta"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.PropertyMeta")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertyMetaDataAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Property Meta Data objects for all the Property Metas Data in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_meta_uid" required="false" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property related to a given meta data record."/>
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the meta type related to a given property / meta data record."/>
		<cfargument name="relationship_filter_date" required="true" type="date" default="#createOdbcDateTime(now())#" hint="Describes the date used to filter active records."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="is_public_facing" required="false" type="boolean" hint="Describes if a given property meta type is public (external / customer) or private (internal) facing."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Property Metas Data and build out the array of components --->
		<cfset local.qResult = getPropertyMetaData(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.PropertyMeta")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="applyChoicesToPropertyMeta" access="public">
		<cfargument name="metaFields" type="array" required="true">

		<cfset var qChoices = "">
		<cfset var thisMeta = "">

		<cfloop array="#metaFields#" index="thisMeta">
			<cfif thisMeta.numMetaChoices gt 0>
				<cfset qChoices = getPropertyMetaChoices(thisMeta.meta_type_uid)>
				<cfset thisMeta.metaChoices = queryToVoArray(qChoices, "mce.e2.vo.PropertyMetaChoice")>
			<cfelse>
				<cfset thisMeta.metaChoices = ArrayNew(1)>
			</cfif>
		</cfloop>

	</cffunction>

	<cffunction name="getPropertyMetaChoices" returntype="query" access="public">
		<cfargument name="meta_type_uid" required="true" type="string">
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT
				*
			FROM
				PropertyMetaChoices
			WHERE
				meta_type_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.meta_type_uid#" list="true">)
			ORDER BY
				display_order,
				friendly_name
		</cfquery>

		<cfreturn q>
	</cffunction>

	<cffunction name="getPropertyMetaDataAsComponent"
				hint="This method is used to retrieve a single instance of a / an Property Meta Data value object representing a single Property Meta Data record."
				output="false"
				returnType="mce.e2.vo.PropertyMeta"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_meta_uid" required="false" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property related to a given meta data record."/>
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the meta type related to a given property / meta data record."/>
		<cfargument name="is_public_facing" required="false" type="boolean" hint="Describes if a given property meta type is public (external / customer) or private (internal) facing."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_start" required="false" type="string" hint="Describes the start date for this record."/>
		<cfargument name="relationship_end" required="false" type="string" hint="Describes the end date for this record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Property Meta Data and build out the component --->
		<cfset local.qResult = getPropertyMetaData(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.PropertyMeta")/>
		<cfset qChoices = getPropertyMetaChoices(local.returnResult.meta_type_uid)>
		<cfset local.returnResult.metaChoices = queryToVoArray(qChoices, "mce.e2.vo.PropertyMetaChoice")>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getPropertyMetaData"
				hint="This method is used to retrieve single / multiple records from the PropertyMeta table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_meta_uid" required="false" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property related to a given meta data record."/>
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the meta type related to a given property / meta data record."/>
		<cfargument name="relationship_filter_date" required="true" type="date" default="#createOdbcDateTime(now())#" hint="Describes the date used to filter active records."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="is_public_facing" required="false" type="boolean" hint="Describes if a given property meta type is public (external / customer) or private (internal) facing."/>
		<cfargument name="relationship_start" required="false" type="string" hint="Describes the start date for this record."/>
		<cfargument name="relationship_end" required="false" type="string" hint="Describes the end date for this record."/>
		<cfargument name="relationshipConflictStart" required="false" type="string" hint="Describes the date when a record goes into effect"/>
		<cfargument name="relationshipConflictEnd" required="false" type="string" hint="Describes the date when a record is no longer in effect"/>
		<cfargument name="exclude_property_meta_uid" required="false" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record that shouldn't be retrieved."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Property Meta Data records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the PropertyMeta table matching the where clause
			select	pmtc.display_order as categoryDisplayOrder,
					types.display_order as metaTypeDisplayOrder,
					p.friendly_name as propertyFriendlyName,
					meta.property_meta_uid,
					types.type_lookup_code,
					types.friendly_name as metaTypeFriendlyName,
					types.data_type,
					meta.meta_value,
					meta.meta_value as meta_value_before_edit,
					meta.meta_type_uid,
					-- consider_touched,
					-- variance_enabled,
					-- tolerance_warning,
					-- tolerance_error,
					meta.property_uid,
					meta.relationship_start,
					meta.relationship_end,
					meta.is_active,
					meta.created_by,
					meta.created_date,
					meta.modified_by,
					meta.modified_date,

					<!--- Return a column that indicates whether user has permission to edit --->
					<cfif structKeyExists(arguments, 'userPermissions')>
						CASE WHEN types.permission_required_to_edit IN ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
						THEN 1 ELSE 0 END AS hasPermissionToEdit,
					</cfif>

					pmtc.friendly_name as categoryFriendlyName,
					(select COUNT(1) from dbo.PropertyMetaChoices pmc (nolock) where pmc.meta_type_uid = meta.meta_type_uid) as numMetaChoices

			from	dbo.Properties p (nolock)
						join dbo.PropertyMeta meta (nolock) on
						meta.property_uid = p.property_uid
					join dbo.PropertyMetaTypes types (nolock)
						on meta.meta_type_uid = types.meta_type_uid
					join dbo.PropertyMetaTypeCategories pmtc (nolock)
						on types.category_uid = pmtc.category_uid

			where	1=1
			
					-- Only get data for active properties.
					and p.is_active = 1

					-- Only active property meta types should ever be retrieved.
					and types.is_active = 1

					<cfif arguments.selectMethod eq 'current'>
					and ( meta.relationship_end is null or meta.relationship_end >= <cfqueryparam cfsqltype="cf_sql_date" list="false" value="#createOdbcDate(arguments.relationship_filter_date)#"> )
					and meta.relationship_start <= <cfqueryparam cfsqltype="cf_sql_date" list="false" value="#createOdbcDate(arguments.relationship_filter_date)#">
					</cfif>			
					
					<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active'>
					and meta.is_active = 1
					</cfif>

					<cfif structKeyExists(arguments, 'property_meta_uid')>
					and meta.property_meta_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_meta_uid#" null="false" list="true"> )
					</cfif>
					
					<cfif structKeyExists(arguments, 'exclude_property_meta_uid')>
					and meta.property_meta_uid not in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.exclude_property_meta_uid#" null="false" list="true"> )
					</cfif>
					
					<cfif structKeyExists(arguments, 'relationship_start')>
					and meta.relationship_start = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.relationship_start#" null="false">
					</cfif>

					<cfif structKeyExists(arguments, 'relationship_end') and len(arguments.relationship_end)>
					and meta.relationship_end = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.relationship_end#" null="false">
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and meta.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'meta_type_uid')>
					and meta.meta_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_type_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'is_public_facing')>
					and types.is_public_facing in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_public_facing#" null="false" list="true"> )
					</cfif>
					
					<cfif structKeyExists(arguments, 'userPermissions')>
					and types.permission_required_to_view in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userPermissions#" list="true" null="false"> )
					</cfif>

					<cfif structkeyexists(arguments, 'relationshipConflictStart') or structKeyExists(arguments, 'relationshipConflictEnd')>
					and (
						<cfif not len(arguments.relationshipConflictEnd)>
						(meta.relationship_start >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictStart)#">
						or meta.relationship_end is null)
						<cfelse>
						(
							(
								meta.relationship_start > <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictStart)#">
								and meta.relationship_end < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictEnd)#">
							)
							or
							(
								meta.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictStart)#">
								and meta.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictEnd)#">
							)
							or
							(
								meta.relationship_end > <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictStart)#">
								and meta.relationship_end < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictEnd)#">
							)
							or
							(
								meta.relationship_start > <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictStart)#">
								and meta.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictEnd)#">
							)
							or
							(
								meta.relationship_start = <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictStart)#">
								and meta.relationship_end = <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationshipConflictEnd)#">
							)
						)
						</cfif>
					)
					</cfif>

			order
			by		pmtc.display_order,
					types.display_order,
					meta.relationship_start

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Describes the methods used to persist / modify Property Meta Data data. --->
	<cffunction name="saveNewPropertyMeta"
				hint="This method is used to persist a new Property Meta Data record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyMeta" required="true" type="mce.e2.vo.PropertyMeta" hint="Describes the VO containing the Property Meta Data record that will be persisted."/>

		<!--- Persist the Property Meta Data data to the application database --->
		<cfset this.dataMgr.insertRecord("PropertyMeta", arguments.PropertyMeta)/>

	</cffunction>

	<cffunction name="saveExistingPropertyMeta"
				hint="This method is used to persist an existing Property Meta Data record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyMeta" required="true" type="mce.e2.vo.PropertyMeta" hint="Describes the VO containing the Property Meta Data record that will be persisted."/>

		<!--- Persist the Property Meta Data data to the application database --->
		<cfset this.dataMgr.updateRecord("PropertyMeta", arguments.PropertyMeta)/>

	</cffunction>

</cfcomponent>
