
<!---

  Template Name:  EmissionsFactorAlias
     Base Table:  EmissionsFactorAliases	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Sunday, March 22, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Sunday, March 22, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EmissionsFactorAliasDelegate"
				hint="This CFC manages all data access interactions for the EmissionsFactorAliases table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEmissionsFactorAliasComponent"
				hint="This method is used to retrieve a Emissions Factor Alias object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EmissionsFactorAlias"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EmissionsFactorAlias")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEmissionsFactorAliasesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Emissions Factor Alias objects for all the Emissions Factor Aliases in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="factor_alias_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given emission factor alias."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy type associated to a emission factor alias."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Emissions Factor Aliases and build out the array of components --->
		<cfset local.qResult = getEmissionsFactorAlias(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EmissionsFactorAlias")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEmissionsFactorAliasAsComponent"
				hint="This method is used to retrieve a single instance of a / an Emissions Factor Alias value object representing a single Emissions Factor Alias record."
				output="false"
				returnType="mce.e2.vo.EmissionsFactorAlias"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="factor_alias_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given emission factor alias."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Emissions Factor Alias and build out the component --->
		<cfset local.qResult = getEmissionsFactorAlias(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EmissionsFactorAlias")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>


	<cffunction name="getDefaultEmissionsProfile"
				hint="This method is used to retrieve the default Emissions Profile record from the application database."
				output="false"
				returnType="query"
				access="public">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve the defaultemissions profile record --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
			
			select	*
			from	dbo.emissionsProfiles ep (nolock)
			where	1=1 
					and ep.profile_lookup_code = 'default'

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<cffunction name="getEmissionsFactorAlias"
				hint="This method is used to retrieve single / multiple records from the EmissionsFactorAliases table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="factor_alias_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given emission factor alias."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given emissions factor alias."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy type associated to a emission factor alias."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>
		
		<!--- Retrieve a single / multiple Emissions Factor Alias records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
			
			-- select records from the EmissionsFactorAliases table matching the where clause
			select	tbl.factor_alias_uid,
					tbl.friendly_name,
					tbl.energy_type_uid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date
			
			from	dbo.EmissionsFactorAliases tbl (nolock)			

					<!--- Filter on the energy type --->
					<cfif structkeyexists(arguments, 'energy_type_uid')>
					join dbo.EmissionsProfiles_EmissionsFactors epef (nolock)
						on tbl.factor_alias_uid = epef.factor_alias_uid
					join dbo.EmissionsProfiles ep (nolock)
						on epef.emissions_profile_uid = ep.emissions_profile_uid
					join dbo.energyTypes et (nolock)
						on tbl.energy_type_uid = et.energy_type_uid
					</cfif>
															
			where	1=1
					and tbl.is_active = 1

					<!--- Filter on the energy type --->
					<cfif structkeyexists(arguments, 'energy_type_uid')>
					and epef.is_active = 1
					and ep.is_active = 1
					and et.is_active = 1
					and tbl.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" list="true" null="false"> ) 					
					</cfif>
										
					<cfif structKeyExists(arguments, 'factor_alias_uid')>
					and tbl.factor_alias_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.factor_alias_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> ) 
					</cfif>
					
			order
			by		tbl.friendly_name
			
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Emissions Factor Alias data. --->	
	<cffunction name="saveNewEmissionsFactorAlias"
				hint="This method is used to persist a new Emissions Factor Alias record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EmissionsFactorAlias" required="true" type="mce.e2.vo.EmissionsFactorAlias" hint="Describes the VO containing the Emissions Factor Alias record that will be persisted."/>

		<!--- Persist the Emissions Factor Alias data to the application database --->
		<cfset this.dataMgr.insertRecord("EmissionsFactorAliases", arguments.EmissionsFactorAlias)/>

	</cffunction>

	<cffunction name="saveExistingEmissionsFactorAlias"
				hint="This method is used to persist an existing Emissions Factor Alias record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EmissionsFactorAlias" required="true" type="mce.e2.vo.EmissionsFactorAlias" hint="Describes the VO containing the Emissions Factor Alias record that will be persisted."/>

		<!--- Persist the Emissions Factor Alias data to the application database --->
		<cfset this.dataMgr.updateRecord("EmissionsFactorAliases", arguments.EmissionsFactorAlias)/>

	</cffunction>

</cfcomponent>
