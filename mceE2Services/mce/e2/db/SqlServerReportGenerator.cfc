<cfcomponent implements="IReportGenerator">

	<cffunction name="setHostName">
		<cfargument name="value">
		<cfset this.hostname = value>
	</cffunction>
	<cffunction name="setHostPort">
		<cfargument name="value">
		<cfset this.hostport = value>
	</cffunction>
	<cffunction name="setServiceFolder">
		<cfargument name="value">
		<cfset this.servicefolder = value>
	</cffunction>
	<cffunction name="setReportFolder">
		<cfargument name="value">
		<cfset this.reportfolder = value>
	</cffunction>
	<cffunction name="setUserId">
		<cfargument name="value">
		<cfset this.userid = value>
	</cffunction>
	<cffunction name="setPwd">
		<cfargument name="value">
		<cfset this.pwd = value>
	</cffunction>
	<cffunction name="generateReportPdf">
		<cfargument name="report_name" type="string" required="true"> 
		<cfargument name="report_format" type="string" required="true" default="">
		<cfargument name="parameters" type="array" required="true">
		<cfargument name="hdroutput" type="string" required="true" default="inline">
		<cfset var myurl = "">
		<cfset myurl = "#constructSqlReportUrl(report_name,report_format, parameters)#">

	<cfhttp url="#myurl#"  username="#this.userid#" password="#this.pwd#" result="report" getasbinary="YES"/> 
		<cfif report_format eq "excel">
		<cfheader name="content-disposition" value="#hdroutput#; filename=#arguments.report_name#.xls">
		<cfcontent type="application/xls" variable ="#report.filecontent#">
		<cfelse>
		<cfheader name="content-disposition" value="#hdroutput#; filename=#arguments.report_name#.#arguments.report_format#">
		<cfcontent type="application/#arguments.report_format#" variable="#report.filecontent#">
		</cfif>
	</cffunction> 
	
	<cffunction name="constructSqlReportUrl" returntype="string" access="PRIVATE"> 
		<cfargument name="reportName" type="string" required="true">
		<cfargument name="report_format" type="string" required="true"> 
		<cfargument name="parameters" type="array" required="true">
		<cfset var qs = ""> 
		<cfset var myurl = "">
	
		<cfloop from="1" to="#arrayLen(parameters)#" index="i">
			<cfset qs = ListAppend(qs, "#parameters[i].param_name#=#urlencodedformat(parameters[i].param_value)#", "&")> 
		</cfloop>
	<cfif qs eq "">
	<cfset myurl= "http://#this.hostname#:#this.hostport#/#this.servicefolder#?/#this.reportfolder#/#arguments.reportName#&rs:Command=Render&rs:Format=#arguments.report_format#">
	<cfelse>
	<cfset myurl= "http://#this.hostname#:#this.hostport#/#this.servicefolder#?/#this.reportfolder#/#arguments.reportName#&rs:Command=Render&rs:Format=#arguments.report_format#&#qs#">
	</cfif>

<cfreturn myurl>
	</cffunction> 
</cfcomponent>