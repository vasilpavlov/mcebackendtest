<cfcomponent displayname="Security Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all security-related database interactions.">

	<cffunction name="attemptLogin"
				returntype="string" access="public"
				hint="This method is used to process login attempts.">
		<cfargument name="username" type="string" required="true" hint="Describes the username that is used to process a login attempt.">
		<cfargument name="password" type="string" required="true" hint="Describes the password that is used to process a login attempt.">
		<cfargument name="system_instance_uid" required="true" type="string" hint="Describes the system instance primary key /unique identifier that is used to process a login attempt.">
		<cfargument name="remote_addr" required="true" type="string" hint="Presumably the IP Address of the user's machine.">
		<cfargument name="http_user_agent_string" required="true" type="string" hint="Presumably the user agent of the user's machine.">

		<cfset var encryptedPassword = this.passwordEncrypter.encryptString(arguments.password)>
		<cfset var session_key = "">

		<cfstoredproc datasource="#this.datasource#" procedure="sessionAttemptLogin">
			<cfprocparam value="#arguments.username#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#encryptedPassword#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.system_instance_uid#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.remote_addr#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.http_user_agent_string#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out" variable="session_key" cfsqltype="CF_SQL_VARCHAR">
		</cfstoredproc>

		<cfreturn session_key>

	</cffunction>

	<cffunction name="getCurrentRoles"
				returntype="String" access="public"
				hint="This method is used to retrieve all the roles associated to a user tied to a given session key.">
		<cfargument name="session_key" type="string" required="false" hint="Describes the primary key / unique identifier of the session from which user roles will be retrieved.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of the user for which valid user roles will be retrieved.">

		<!--- Initialize the local scope --->
		<cfset var rolesQuery = "">

		<!--- Define the query used to retrieve roles associated to a given user --->
		<cfquery name="rolesQuery" datasource="#this.datasource#">

			select	role_lookup_code
			from	dbo.UserRoles (nolock)
			where	user_role_uid in (

					select	user_role_uid
					from	dbo.usergroups_userroles ugur (nolock) join
							dbo.usergroups ug (nolock) on ugur.user_group_uid = ug.user_group_uid join
							dbo.users_usergroups uug (nolock) on uug.user_group_uid = ug.user_group_uid
					where	1=1
							and uug.is_active = 1
							<cfif structKeyExists(arguments, 'session_key')>
							and uug.user_uid = <cfqueryparam value="#getUserUid(session_key)#" cfsqltype="cf_sql_idstamp">
							</cfif>

							<cfif structKeyExists(arguments, 'user_uid')>
							and uug.user_uid in (<cfqueryparam value="#arguments.user_uid#" cfsqltype="cf_sql_idstamp" list="true">)
							</cfif>

							and ugur.is_role_granted = 1

				)

		</cfquery>

		<!--- Return the lookup codes for the roles associated to the specified user --->
		<cfreturn ValueList(rolesQuery.role_lookup_code)>

	</cffunction>

	<cffunction name="getCurrentPermissions"
				returntype="String" access="public"
				hint="This method is used to retrieve all the permissions associated to a user tied to a given session key.">

		<!--- Define the arguments for this method --->
		<cfargument name="session_key" type="string" required="false" hint="Describes the primary key / unique identifier of the session from which user permissions that will be retrieved.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of the user for which valid user permissions that will be retrieved.">
		<cfargument name="role_lookup_code" type="string" required="false" hint="Describes the internal identifier of the role lookup code used to limit the scope of the valid user permissions that will be retrieved.">

		<!--- Initialize the local scope --->
		<cfset var permissionsQuery = "">

		<!--- Define the query used to retrieve permissions associated to a given user --->
		<cfquery name="permissionsQuery" datasource="#this.datasource#">

			-- Get the permission code
			select	up.user_permission_code
			from	dbo.UserPermissions up (nolock)
			where	up.is_active = 1

					-- Retrieve the active permissions that have been enabled
					and up.user_permission_code in (

						select	up.user_permission_code
						from	dbo.Users u (nolock)
								join dbo.Users_UserGroups uug (nolock)
									on u.user_uid = uug.user_uid
								join dbo.UserGroups ug (nolock)
									on ug.user_group_uid = uug.user_group_uid
								join dbo.UserGroups_UserRoles ugur (nolock)
									on ug.user_group_uid = ugur.user_group_uid
								join dbo.UserRoles_UserPermissions urup (nolock)
									on ugur.user_role_uid = urup.user_role_uid
								join dbo.UserRoles ur (nolock)
									on urup.user_role_uid = ur.user_role_uid
								join dbo.UserPermissions up (nolock)
									on up.user_permission_code like urup.user_permission_code_like

						where	1=1
								and u.is_active = 1
								and uug.is_active = 1
								and ug.is_active = 1
								and ugur.is_active = 1
								and urup.is_active = 1
								and ur.is_active = 1
								and up.is_active = 1
								and urup.is_granted = 1
								and ugur.is_role_granted = 1

								<cfif structKeyExists(arguments, 'user_uid')>
								and u.user_uid in (<cfqueryparam value="#arguments.user_uid#" cfsqltype="cf_sql_idstamp" list="true">)
								</cfif>

								<cfif structKeyExists(arguments, 'role_lookup_code')>
								and ur.role_lookup_code in (<cfqueryparam value="#arguments.role_lookup_code#" cfsqltype="cf_sql_varchar" list="true">)
								</cfif>

						)

					-- Retrieve the active permissions that have been disabled
					and up.user_permission_code not in (

						select	up.user_permission_code
						from	dbo.Users u (nolock)
								join dbo.Users_UserGroups uug (nolock)
									on u.user_uid = uug.user_uid
								join dbo.UserGroups ug (nolock)
									on ug.user_group_uid = uug.user_group_uid
								join dbo.UserGroups_UserRoles ugur (nolock)
									on ug.user_group_uid = ugur.user_group_uid
								join dbo.UserRoles_UserPermissions urup (nolock)
									on ugur.user_role_uid = urup.user_role_uid
								join dbo.UserRoles ur (nolock)
									on urup.user_role_uid = ur.user_role_uid
								join dbo.UserPermissions up (nolock)
									on up.user_permission_code like urup.user_permission_code_like

						where	1=1
								and u.is_active = 1
								and uug.is_active = 1
								and ug.is_active = 1
								and ugur.is_active = 1
								and urup.is_active = 1
								and ur.is_active = 1
								and up.is_active = 1
								and urup.is_granted = 0

								<cfif structKeyExists(arguments, 'user_uid')>
								and u.user_uid in (<cfqueryparam value="#arguments.user_uid#" cfsqltype="cf_sql_idstamp" list="true">)
								</cfif>

								<cfif structKeyExists(arguments, 'role_lookup_code')>
								and ur.role_lookup_code in (<cfqueryparam value="#arguments.role_lookup_code#" cfsqltype="cf_sql_varchar" list="true">)
								</cfif>

					)

			order
			by		up.user_permission_code

		</cfquery>

		<!--- Return the friendly names for the permissions associated to the specified user --->
		<cfreturn ValueList(permissionsQuery.user_permission_code)>

	</cffunction>

	<cffunction name="getValidPropertyCollections"
				returntype="String" access="public"
				hint="This method is used to retrieve all the property collections associated to a user tied to a given session key.">
		<cfargument name="session_key" type="string" required="false" hint="Describes the primary key / unique identifier of the session from which valid property collections will be retrieved.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of the user for which valid property collections will be retrieved.">

		<!--- Initialize the local scope --->
		<cfset var pcQuery = "">

		<!--- Create the query to pull together a list of property collection identifiers based on the current user's access rights --->
		<cfquery name="pcQuery" datasource="#this.datasource#">

			select	pc.property_collection_uid
			from	dbo.propertyCollections pc (nolock)
					join dbo.PropertyCollections_Users pcu (nolock)
						on pc.property_collection_uid = pcu.property_collection_uid
					join dbo.Users u (nolock)
						on pcu.user_uid = u.user_uid
						and pc.client_company_uid = u.client_company_uid

			where	1=1

					and pc.is_active = 1
					and pcu.is_active = 1
					and u.is_active = 1

					<cfif structKeyExists(arguments, 'session_key')>
					and u.user_uid = <cfqueryparam value="#getUserUid(session_key)#" cfsqltype="cf_sql_idstamp">
					</cfif>

					<cfif structKeyExists(arguments, 'user_uid')>
					and u.user_uid in (<cfqueryparam value="#arguments.user_uid#" cfsqltype="cf_sql_idstamp" list="true">)
					</cfif>
			union select distinct pc.property_collection_uid
			from PropertyCollections AS pc (nolock)
					join UserGroups_PropertyCollections AS ugpc (nolock)
						on pc.property_collection_uid = ugpc.property_collection_uid
					join Users_UserGroups AS uug (nolock)
						on ugpc.user_group_uid = uug.user_group_uid
			where 1=1
					and uug.is_active = 1
					<cfif structKeyExists(arguments, 'user_uid')>
					and uug.user_uid in (<cfqueryparam value="#arguments.user_uid#" cfsqltype="cf_sql_idstamp" list="true">)
					</cfif>
		</cfquery>

		<!--- Return the value list of property collection primary keys --->
		<cfreturn ValueList(pcQuery.property_collection_uid)>

	</cffunction>

	<cffunction name="getUserRecord"
				returntype="query" access="public"
				hint="This method is used to retrieve the complete user record tied to a given session key.">
		<cfargument name="session_key" type="string" required="true" hint="Describes the primary key / unique identifier of the session from which user information will be retrieved.">

		<!--- Initialize the local scope --->
		<cfset var userQuery = "">

		<!--- Retrieve the userId for the specified user session --->
		<cfquery name="userQuery" datasource="#this.datasource#">
			SELECT
					u.*,
					profiles.profile_lookup_code,
					profiles.carbon_eq_multiplier_label,
					ISNULL(u.playground_namespace_lookup_code_override, cc.default_playground_namespace_lookup_code) as namespace_lookup_code,
					cc.image_filename
			FROM	dbo.users (nolock) u
					JOIN dbo.userSessions (nolock) s
						ON u.user_uid = s.user_uid
					JOIN dbo.ClientCompanies  (nolock) cc
						ON u.client_company_uid = cc.client_company_uid
					JOIN dbo.EmissionsProfiles profiles
						ON profiles.profile_lookup_code = ISNULL(u.profile_lookup_code_override, cc.default_profile_lookup_code)
			WHERE
					user_session_uid = <cfqueryparam value="#session_key#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Return the user record for the session --->
		<cfreturn userQuery>

	</cffunction>

	<cffunction name="getUserUid"
				returntype="string" access="public"
				hint="This method is used to retrieve the userId for a user tied to a given session key.">
		<cfargument name="session_key" type="string" required="true" hint="Describes the primary key / unique identifier of the session from which user information will be retrieved.">

		<!--- Initialize the local scope --->
		<cfset var userQuery = "">

		<!--- Retrieve the userId for the specified user session --->
		<cfquery name="userQuery" datasource="#this.datasource#">

			select	user_uid
			from	dbo.userSessions (nolock)
			where	user_session_uid = <cfqueryparam value="#session_key#" cfsqltype="cf_sql_varchar">

		</cfquery>

		<!--- Return the userId for the session --->
		<cfreturn userQuery.user_uid>

	</cffunction>

	<cffunction name="setPasswordEncrypter"
				hint="This method is used to set the *.cfc that will be used to encrypt user passwords (dependency injection).">
		<cfargument name="passwordEncrypter" type="any" required="true" hint="Describes the *.cfc that will be used to encrypt passwords.">
		<cfset this.passwordEncrypter = arguments.passwordEncrypter>
	</cffunction>

</cfcomponent>