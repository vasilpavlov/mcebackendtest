
<!---

  Template Name:  EnergyAccount
     Base Table:  EnergyAccounts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Sunday, March 22, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Sunday, March 22, 2009 - Template Created.

--->

<cfcomponent displayname="EnergyAccountDelegate"
				hint="This CFC manages all data access interactions for the EnergyAccounts table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyAccountComponent"
				hint="This method is used to retrieve a Energy Account object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyAccount"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyAccount")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyAccountsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Account objects for all the Energy Accounts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="true" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="true" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
		<cfargument name="master_energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Accounts and build out the array of components --->
		<cfset local.qResult = getEnergyAccount(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyAccount")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyAccountsAndEnergyTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Account / Energy Type objects."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="true" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Accounts and build out the array of components --->
		<cfset local.qResult = getEnergyAccountAndEnergyType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyAccountEnergyType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyAccountSummariesForDataAnalysisAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Account summary objects for the purpose of data analysis."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
		<cfargument name="energy_unit_uid" required="false" type="string" hint="Describes the primary key of the energy unit associated to a given energy account."/>
		<cfargument name="master_energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Accounts and build out the array of components --->
		<cfset local.qResult = getEnergyAccountSummaryForDataAnalysis(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyAccountSummaryForDataAnalysis")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyAccountSummaryAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Account Summary objects for all the Energy Accounts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
		<cfargument name="master_energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Account Summary data and build out the array of components --->
		<cfset local.qResult = getEnergyAccountSummary(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyAccountSummary")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyAccountAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Account value object representing a single Energy Account record."
				output="false"
				returnType="mce.e2.vo.EnergyAccount"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
		<cfargument name="master_energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Account and build out the component --->
		<cfset local.qResult = getEnergyAccount(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyAccount")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyAccount"
				hint="This method is used to retrieve single / multiple records from the EnergyAccounts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the customer facing / external name for a given energy account."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
		<cfargument name="energy_unit_uid" required="false" type="string" hint="Describes the primary key of the energy unit associated to a given energy account."/>
		<cfargument name="factor_alias_uid_override" required="false" type="string" hint="Describes the primary key of the energy factor alias used to override a given energy account."/>
		<cfargument name="master_energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyAccounts table matching the where clause
			select	distinct
					tbl.energy_account_uid,
					tbl.property_uid,
					tbl.friendly_name,
					tbl.account_type_uid,
					tbl.energy_type_uid,
					ena.meta_group_uid,
					tbl.energy_unit_uid,
					tbl.factor_alias_uid_override,
					tbl.master_energy_account_uid,
					tbl.notes_text,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,

					-- Friendly Names
					tbl.friendly_name as friendlyEnergyAccountName,
					ea.friendly_name as friendlyMasterEnergyAccountName,
					p.friendly_name as friendlyPropertyName,
					eat.friendly_name as friendlyEnergyAccountTypeName,
					et.friendly_name as friendlyEnergyTypeName,
					mtg.friendly_name as friendlyMetaTypeGroupName,
					eu.friendly_name as friendlyEnergyUnitName,
					ep.friendly_name as friendlyEnergyProviderName,
					eprc.friendly_name as friendlyRateClassName,
					efa.friendly_name as friendlyFactorAliasName,
					(select friendly_name from dbo.rateModels where rate_model_uid = isNull(enaeprc.overridden_rate_model_uid, rm.rate_model_uid)) as friendlyRateModelName,

					(select model_lookup_code from dbo.rateModels where rate_model_uid = isNull(enaeprc.overridden_rate_model_uid, rm.rate_model_uid)) as model_lookup_code,

					-- Association / Relationship Id's
					enaeprc.relationship_id as energyNativeAccountEnergyProviderRateClass_relId,
					eprcrm.relationship_id as energyProviderRateClassRateModels_relId,

					ena.native_account_number,
					ena.native_account_uid,
					ena.energy_provider_uid,
					eprc.rate_class_uid,
					isNull(enaeprc.overridden_rate_model_uid, rm.rate_model_uid) as rate_model_uid,
					et.is_delivery_based,
					tbl.account_status,
					tbl.status_date

			from	dbo.EnergyAccounts tbl (nolock)
					join dbo.EnergyNativeAccounts ena (nolock)
						on tbl.energy_account_uid = ena.energy_account_uid
						and tbl.currentEnergyProviderUid = ena.energy_provider_uid
					join dbo.EnergyNativeAccounts_EnergyProviderRateClasses enaeprc (nolock)
						on ena.native_account_uid = enaeprc.native_account_uid
					join dbo.EnergyProviderRateClasses eprc (nolock)
						on enaeprc.rate_class_uid = eprc.rate_class_uid
					join dbo.EnergyProviderRateClasses_RateModels eprcrm (nolock)
						on eprc.rate_class_uid = eprcrm.rate_class_uid
					join dbo.RateModels rm (nolock)
						on eprcrm.rate_model_uid = rm.rate_model_uid
					join dbo.Properties p (nolock)
						on	tbl.property_uid = p.property_uid
					join dbo.EnergyAccountTypes eat (nolock)
						on tbl.account_type_uid = eat.account_type_uid
					join dbo.EnergyTypes et (nolock)
						on tbl.energy_type_uid = et.energy_type_uid
					join dbo.MetaTypeGroups mtg (nolock)
						on ena.meta_group_uid = mtg.meta_group_uid
<!--- 						on tbl.meta_group_uid = mtg.meta_group_uid --->
					join dbo.EnergyUnits eu (nolock)
						on tbl.energy_unit_uid = eu.energy_unit_uid
					join dbo.EnergyProviders ep (nolock)
						on ena.energy_provider_uid = ep.energy_provider_uid
					left outer join dbo.EmissionsFactorAliases efa (nolock)
						on tbl.factor_alias_uid_override = efa.factor_alias_uid
					left outer join dbo.EnergyAccounts ea (nolock)
						on tbl.master_energy_account_uid = ea.energy_account_uid


			where	1=1
				and ena.is_active = 1 <!--- Testing active flag--->
					and enaeprc.is_active = 1

			 		<cfif arguments.selectMethod eq 'current'>
				 	and (ea.is_active = 1 or ea.is_active is null)
					and tbl.is_active = 1
					and (

							-- Only retrieve active associations
							ena.is_active = 1
							and ( ena.relationship_end is null or ena.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and ena.relationship_start <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					and (

							-- Only retrieve active associations
							enaeprc.is_active = 1
							and ( enaeprc.relationship_end is null or enaeprc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and enaeprc.relationship_start <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					and eprc.is_active = 1
					and (

							-- Only retrieve active associations
							eprcrm.is_active = 1
							and ( eprcrm.relationship_end is null or eprcrm.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and eprcrm.relationship_start <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					)
					and rm.is_active = 1
					and p.is_active = 1
					and eat.is_active = 1
					and et.is_active = 1
					and mtg.is_active = 1
					and eu.is_active = 1
					and (efa.is_active = 1 or efa.is_active is null)
					</cfif>

					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and tbl.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'account_type_uid')>
					and tbl.account_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.account_type_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_type_uid')>
					and tbl.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'meta_group_uid')>
					and ena.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_group_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_unit_uid')>
					and tbl.energy_unit_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_unit_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'factor_alias_uid_override')>
					and tbl.factor_alias_uid_override in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.factor_alias_uid_override#" list="true" null="false"> )
					</cfif>

<cfif structKeyExists(arguments, 'master_energy_account_uid')>
   <cfif arguments.master_energy_account_uid eq ''>
      and tbl.master_energy_account_uid IS NULL
   <cfelse>
      and tbl.master_energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp"
          value="#arguments.master_energy_account_uid#" list="true" null="false"> )
   </cfif>
</cfif>

		</cfquery>
				
		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyAccountsByProperty"
				hint="This method is used to retrieve an array collection of Energy Account objects for all the Energy Accounts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid" required="true" type="string" hint="Describes the primary key of the property associated to a given energy account."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Accounts and build out the array of components --->
		<cfset local.qResult = getEnergyAccount(property_uid=arguments.property_uid,master_energy_account_uid='')/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyAccount")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>
	<cffunction name="getEnergyAccountAndEnergyType"
				access="public" returntype="query"
				hint="This method is used to retrieve energy accounts and their associated energy types.">

		<!--- Define the arguments for this method --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cfquery name="local.qResult" datasource="#this.datasource#">

			select	distinct
					tbl.energy_account_uid,
					tbl.property_uid,
					tbl.energy_type_uid,

					-- Friendly Names
					tbl.friendly_name as friendlyEnergyAccountName,
					p.friendly_name as friendlyPropertyName,
					et.friendly_name as friendlyEnergyTypeName

			from	dbo.energyAccounts tbl (nolock)
					join dbo.properties p (nolock)
						on tbl.property_uid = p.property_uid
					join dbo.energyTypes et (nolock)
						on tbl.energy_type_uid = et.energy_type_uid

			where	1=1
					and tbl.is_active = 1
					and p.is_active = 1
					and et.is_active = 1

					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and tbl.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_type_uid')>
					and tbl.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" list="true" null="false"> )
					</cfif>

			order
			by		et.friendly_name,
					tbl.friendly_name

		</cfquery>

		<!--- Return the result set --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyProviderRateClassRateModelAssociation"
				hint="This method is used to retrieve an energy provider rate class / rate model association record."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="rate_class_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy provider rate class definition."/>
		<cfargument name="rate_model_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given rate model definition."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			select	tbl.relationship_id,
					tbl.rate_class_uid,
					tbl.rate_model_uid,
					tbl.relationship_start,
					tbl.relationship_end,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.EnergyProviderRateClasses_RateModels tbl
			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					and (

							-- Only retrieve active associations
							tbl.is_active = 1
							and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					<cfif structKeyExists(arguments, 'rate_model_uid')>
					and tbl.rate_model_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.rate_model_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'rate_class_uid')>
					and tbl.rate_class_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.rate_class_uid#" list="true" null="false"> )
					</cfif>

		</cfquery>

		<!--- Return the result set --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyNativeAccountEnergyProviderRateClassAssociation"
				hint="This method is used to retrieve an energy native account / energy provider rate class association record."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="native_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy native account definition."/>
		<cfargument name="rate_class_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy provider rate class definition."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			select	tbl.relationship_id,
					tbl.native_account_uid
					tbl.rate_class_uid,
					tbl.relationship_start,
					tbl.relationship_end,
					tbl.overridden_rate_model_uid,
					tbl.effective_input_set_uid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.EnergyNativeAccounts_EnergyProviderRateClasses tbl
			where	1=1
			and tbl.is_active = 1 <!--- hard code is_active --->

					<cfif arguments.selectMethod eq 'current'>
					and (

							-- Only retrieve active associations
							tbl.is_active = 1
							and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					<cfif structKeyExists(arguments, 'relationship_id')>
					and tbl.relationship_id in ( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.relationship_id#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'native_account_uid')>
					and tbl.native_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.native_account_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'rate_class_uid')>
					and tbl.rate_class_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.rate_class_uid#" list="true" null="false"> )
					</cfif>

		</cfquery>

		<!--- Return the result set --->
		<cfreturn local.qResult>

	</cffunction>

	<!--- Proxy methods --->
	<cffunction name="getEnergyAccountSummary"
				hint="This method is used to retrieve an energy account summary for a given / collection of properties."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
		<cfargument name="energy_unit_uid" required="false" type="string" hint="Describes the primary key of the energy unit associated to a given energy account."/>
		<cfargument name="factor_alias_uid_override" required="false" type="string" hint="Describes the primary key of the energy factor alias used to override a given energy account."/>
		<cfargument name="master_energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			select	ea.energy_account_uid,
					ea.account_type_uid,
					ea.energy_type_uid,
					ea.friendly_name,
					ea.master_energy_account_uid,
					ea.property_uid,
					ea.energy_unit_uid,
					ena.native_account_number,
					ep.energy_provider_uid,
					eunits.friendly_name as energyUnitFriendlyName,
					ep.friendly_name as providerFriendlyName,
					eat.friendly_name as costTypeFriendlyName,
					et.friendly_name as resourceFriendlyName,
					p.friendly_name as propertyFriendlyName,

					case
						when ea.master_energy_account_uid is null then ea.friendly_name
						else mea.friendly_name
					end as masterEnergyAccountFriendlyName,

					min(eu.period_start) as energyUsageAvailableFrom,
					max(eu.period_end) as energyUsageAvailableTo,
					count(ea.energy_account_uid) as numEnergyUsageRecords
,dbo.getEnergyUsageMetaTypeUidById(ena.meta_group_uid,'supplycharges') as supply_meta_type_uid
,dbo.getEnergyUsageMetaTypeUidById(ena.meta_group_uid,'tdcharges') as td_meta_type_uid
,ea.account_status
,ea.status_date
			from	dbo.EnergyAccounts ea (nolock)
					join dbo.EnergyAccountTypes eat (nolock)
						on ea.account_type_uid = eat.account_type_uid
					join dbo.EnergyTypes et (nolock)
						on ea.energy_type_uid = et.energy_type_uid
					join dbo.EnergyUnits eunits (nolock)
						on ea.energy_unit_uid = eunits.energy_unit_uid
					join dbo.Properties p (nolock)
						on ea.property_uid = p.property_uid
					join dbo.EnergyNativeAccounts ena (nolock)
						on ea.energy_account_uid = ena.energy_account_uid
					join dbo.EnergyProviders ep
						--on ena.energy_provider_uid = ep.energy_provider_uid
						--and ea.currentEnergyProviderUid = ep.energy_provider_uid
						on ea.currentEnergyProviderUid = ep.energy_provider_uid
					left outer join dbo.EnergyUsage eu (nolock)
						on eu.energy_account_uid = ea.energy_account_uid
						and eu.usage_type_code = 'actual'
						and eu.is_active = 1
					left outer join dbo.EnergyAccounts mea (nolock)
						on ea.master_energy_account_uid = mea.energy_account_uid
						and mea.is_active = 1

			where	1=1
			and ena.is_active = 1 <!--- hard code is_active --->

					<cfif arguments.selectMethod eq 'current'>
					and ea.is_active = 1
					and eat.is_active = 1
					and et.is_active = 1
					and p.is_active = 1
					and (

						-- Filter on active data while respecting the join relationships
						ena.is_active = 1
						and ( ena.relationship_end is null or ena.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
						and ena.relationship_start <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					)
					</cfif>

					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and ea.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and ea.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'account_type_uid')>
					and ea.account_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.account_type_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_type_uid')>
					and ea.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'meta_group_uid')>
					and ena.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_group_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_unit_uid')>
					and ea.energy_unit_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_unit_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'master_energy_account_uid')>
					and mea.master_energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.master_energy_account_uid#" list="true" null="false"> )
					</cfif>

					--and eu.usage_type_code = 'actual'

			group
			by		ea.energy_account_uid,
					ea.account_type_uid,
					ea.energy_type_uid,
					ea.friendly_name,
					ea.master_energy_account_uid,
					ea.property_uid,
					ea.energy_unit_uid,
					eunits.friendly_name,
					ena.native_account_number,
					ena.meta_group_uid,
					ep.energy_provider_uid,
					ep.friendly_name,
					eat.friendly_name,
					et.friendly_name,
					p.friendly_name,

					case
						when ea.master_energy_account_uid is null then ea.friendly_name
						else mea.friendly_name
					end,
					ea.account_status,
					ea.status_date

			order
			by		et.friendly_name,
					p.friendly_name,
					case
						when ea.master_energy_account_uid is null then ea.friendly_name
						else mea.friendly_name
					end

		</cfquery>

		<!--- Returnt the result --->
		<cfreturn local.qResult/>

	</cffunction>

	<cffunction name="getAvailableEnergyAccountSummaryByEnergyType"
				hint="This method is used to retrieve energy account summaries for the summary on the home page"
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

SELECT      Properties.friendly_name,
			(select COUNT(EnergyAccounts.energy_account_uid)
			FROM         EnergyAccounts INNER JOIN
					  EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid
			WHERE     EnergyAccounts.is_active = 1 AND account_type_uid IN(SELECT account_type_uid FROM EnergyAccountTypes WHERE friendly_name='Utility')
			AND	EnergyAccounts.master_energy_account_uid IS NULL AND  (EnergyAccounts.property_uid = Properties.property_uid ) AND (EnergyTypes.friendly_name = 'Electricity')) as Elec,

			(select COUNT(EnergyAccounts.energy_account_uid)
			FROM         EnergyAccounts INNER JOIN
					  EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid
			WHERE     EnergyAccounts.is_active = 1 AND account_type_uid IN(SELECT account_type_uid FROM EnergyAccountTypes WHERE friendly_name='Supplier')
			AND	EnergyAccounts.master_energy_account_uid IS NULL AND  EnergyAccounts.property_uid = Properties.property_uid AND (EnergyTypes.friendly_name = 'Oil')) as Oil,

			(select COUNT(EnergyAccounts.energy_account_uid)
			FROM        EnergyAccounts INNER JOIN
						EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid
			WHERE EnergyAccounts.is_active = 1 AND account_type_uid IN(SELECT account_type_uid FROM EnergyAccountTypes WHERE friendly_name='Utility')
			AND	EnergyAccounts.master_energy_account_uid IS NULL AND  EnergyAccounts.property_uid = Properties.property_uid AND (EnergyTypes.friendly_name = 'Gas'))  as Gas,

			(select COUNT(EnergyAccounts.energy_account_uid)
			FROM         EnergyAccounts INNER JOIN
					  EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid
			WHERE     EnergyAccounts.is_active = 1 AND account_type_uid IN(SELECT account_type_uid FROM EnergyAccountTypes WHERE friendly_name='Utility')
			AND	EnergyAccounts.master_energy_account_uid IS NULL AND  EnergyAccounts.property_uid = Properties.property_uid  AND (EnergyTypes.friendly_name = 'Steam')) as Steam,

			(select COUNT(EnergyAccounts.energy_account_uid)
			FROM         EnergyAccounts INNER JOIN
					  EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid
			WHERE     EnergyAccounts.is_active = 1 AND account_type_uid IN(SELECT account_type_uid FROM EnergyAccountTypes WHERE friendly_name='Utility')
			AND	EnergyAccounts.master_energy_account_uid IS NULL AND  EnergyAccounts.property_uid = Properties.property_uid  AND (EnergyTypes.friendly_name = 'Water')) as Water,

			(select COUNT(EnergyAccounts.energy_account_uid)
			FROM         EnergyAccounts INNER JOIN
					  EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid
			WHERE  EnergyAccounts.is_active = 1 AND account_type_uid IN(SELECT account_type_uid FROM EnergyAccountTypes WHERE friendly_name='Utility')
			AND	EnergyAccounts.master_energy_account_uid IS NULL AND  EnergyAccounts.property_uid = Properties.property_uid  AND (EnergyTypes.friendly_name IN('Recycled (Waste)','Non-Recyclable (Waste)','Waste'))) as Waste

FROM        Properties
WHERE		Properties.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
AND Properties.friendly_name <> 'Corporate'
ORDER BY
			Properties.friendly_name

		</cfquery>

		<!--- Returnt the result --->
		<cfreturn local.qResult/>

	</cffunction>

	<cffunction name="getCorporateEmissionsSources"
				hint="This method is used to retrieve energy account summaries for the summary on the home page"
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

SELECT      Properties.friendly_name,
CASE WHEN (SELECT COUNT(EnergyAccounts.energy_account_uid) AS Expr1
FROM         EnergyAccounts INNER JOIN
                      EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid INNER JOIN
                      EnergyAccountTypes ON EnergyAccounts.account_type_uid = EnergyAccountTypes.account_type_uid INNER JOIN
                      EmissionsSources ON EnergyTypes.energy_type_uid = EmissionsSources.energy_type_uid
WHERE  EnergyAccounts.is_active=1 AND  (EmissionsSources.scope = 1) AND (EnergyAccountTypes.friendly_name = 'Corporate')
			AND     EnergyAccounts.master_energy_account_uid IS NULL AND  (EnergyAccounts.property_uid = Properties.property_uid )) >=1 THEN 1 ELSE 0 END as Scope1,

CASE WHEN (SELECT COUNT(EnergyAccounts.energy_account_uid) AS Expr1
FROM         EnergyAccounts INNER JOIN
                      EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid INNER JOIN
                      EnergyAccountTypes ON EnergyAccounts.account_type_uid = EnergyAccountTypes.account_type_uid INNER JOIN
                      EmissionsSources ON EnergyTypes.energy_type_uid = EmissionsSources.energy_type_uid
WHERE EnergyAccounts.is_active=1 AND    (EmissionsSources.scope = 2) AND (EnergyAccountTypes.friendly_name = 'Corporate')
			AND     EnergyAccounts.master_energy_account_uid IS NULL AND  (EnergyAccounts.property_uid = Properties.property_uid )) >=1 THEN 1 ELSE 1 END as Scope2,

CASE WHEN 			(SELECT COUNT(EnergyAccounts.energy_account_uid) AS Expr1
FROM         EnergyAccounts INNER JOIN
                      EnergyTypes ON EnergyAccounts.energy_type_uid = EnergyTypes.energy_type_uid INNER JOIN
                      EnergyAccountTypes ON EnergyAccounts.account_type_uid = EnergyAccountTypes.account_type_uid INNER JOIN
                      EmissionsSources ON EnergyTypes.energy_type_uid = EmissionsSources.energy_type_uid
WHERE  EnergyAccounts.is_active=1 AND   (EmissionsSources.scope = 3) AND (EnergyAccountTypes.friendly_name = 'Corporate')
			AND     EnergyAccounts.master_energy_account_uid IS NULL AND  (EnergyAccounts.property_uid = Properties.property_uid )) >=1 THEN 1 ELSE 0 END as Scope3

FROM        Properties
WHERE Properties.friendly_name = 'Corporate'
AND Properties.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
		</cfquery>

		<!--- Returnt the result --->
		<cfreturn local.qResult/>

	</cffunction>

	<cffunction name="getEnergyAccountSummaryForDataAnalysis"
				hint="This method is used to retrieve energy account summaries for data analysis from a collection of properties."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
		<cfargument name="energy_unit_uid" required="false" type="string" hint="Describes the primary key of the energy unit associated to a given energy account."/>
		<cfargument name="master_energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			select	ea.energy_account_uid,
					ea.energy_type_uid,
					ea.property_uid,
					eunits.energy_unit_uid,
					eunits.unit_lookup_code,
					ena.native_account_number,
					ep.energy_provider_uid,

					ea.friendly_name as energyAccountFriendlyName,
					ea.master_energy_account_uid,
					et.friendly_name as energyTypeFriendlyName,
					ep.friendly_name as energyProviderFriendlyName,
					p.friendly_name as propertyFriendlyName,
					eunits.friendly_name as energyUnitFriendlyName,

					min(eu.period_end) as energyUsageAvailableFrom,
					max(eu.period_end) as energyUsageAvailableTo,
					count(eu.energy_usage_uid) as numEnergyUsageRecords,
					ea.account_status,
					ea.status_date

			from	dbo.EnergyAccounts ea (nolock)
					join dbo.EnergyAccountTypes eat (nolock)
						on ea.account_type_uid = eat.account_type_uid
					join dbo.EnergyTypes et (nolock)
						on ea.energy_type_uid = et.energy_type_uid
					join dbo.Properties p (nolock)
						on ea.property_uid = p.property_uid
					join dbo.EnergyNativeAccounts ena (nolock)
						on ea.energy_account_uid = ena.energy_account_uid
					join dbo.EnergyProviders ep (nolock)
						on ena.energy_provider_uid = ep.energy_provider_uid
					join dbo.EnergyUnits eunits (nolock)
						on ISNULL(ea.energy_unit_uid, et.default_energy_unit_uid) = eunits.energy_unit_uid
					left outer join dbo.EnergyUsage eu (nolock)
						on eu.energy_account_uid = ea.energy_account_uid
						and eu.is_active = 1
						and eu.usage_status_code IN (SELECT usage_status_code FROM EnergyUsageStatuses (nolock) WHERE data_is_reportable = 1)
					left outer join dbo.EnergyAccounts mea (nolock)
						on ea.master_energy_account_uid = mea.energy_account_uid
						and mea.is_active = 1

			where	1=1
			and ena.is_active = 1 <!--- Return the result set --->

					<cfif arguments.selectMethod eq 'current'>
					and ea.is_active = 1
					and eat.is_active = 1
					and et.is_active = 1
					and p.is_active = 1
					and ep.is_active = 1
					and (

						-- Filter on active data while respecting the join relationships
						ena.is_active = 1
						and ( ena.relationship_end is null or ena.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
						and ena.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					)
					</cfif>

					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and ea.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and ea.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'account_type_uid')>
					and ea.account_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.account_type_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_type_uid')>
					and ea.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'meta_group_uid')>
					and ena.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_group_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_unit_uid')>
					and ea.energy_unit_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_unit_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'master_energy_account_uid')>
					and mea.master_energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.master_energy_account_uid#" list="true" null="false"> )
					</cfif>

			group
			by		ea.energy_account_uid,
					ea.energy_type_uid,
					eunits.energy_unit_uid,
					eunits.unit_lookup_code,
					ea.property_uid,
					ena.native_account_number,
					ep.energy_provider_uid,

					ea.friendly_name,
					et.friendly_name,
					eunits.friendly_name,
					ep.friendly_name,
					p.friendly_name,
					ea.master_energy_account_uid,
					ea.account_status,
					ea.status_date

			order
			by		p.friendly_name,
					et.friendly_name,
					ep.friendly_name,
					ea.friendly_name
		</cfquery>

		<!--- Returnt the result --->
		<cfreturn local.qResult/>

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Account data. --->
	<cffunction name="saveNewEnergyAccount"
				hint="This method is used to persist a new Energy Account record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyAccount" required="true" type="mce.e2.vo.EnergyAccount" hint="Describes the VO containing the Energy Account record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyAccounts", arguments.EnergyAccount)/>

	</cffunction>

	<cffunction name="saveExistingEnergyAccount"
				hint="This method is used to persist an existing Energy Account record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyAccount" required="true" type="mce.e2.vo.EnergyAccount" hint="Describes the VO containing the Energy Account record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyAccounts", arguments.EnergyAccount)/>

	</cffunction>

	<cffunction name="saveNewEnergyNativeAccount"
				hint="This method is used to persist a new Native Energy Account record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyNativeAccount" required="true" type="struct" hint="Describes the VO containing the energy native account record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyNativeAccounts", arguments.EnergyNativeAccount)/>

	</cffunction>

	<cffunction name="saveExistingEnergyNativeAccount"
				hint="This method is used to persist an existing Native Energy Account record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyNativeAccount" required="true" type="struct" hint="Describes the VO containing the energy native account record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyNativeAccounts", arguments.EnergyNativeAccount)/>

	</cffunction>

	<cffunction name="saveNewEnergyNativeAccountEnergyProviderRateClassAssociation"
				hint="This method is used to persist a new native energy account / energy provider rate class association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AssociationObject" required="true" type="struct" hint="Describes the VO containing the energy native account / energy provider rate class association record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyNativeAccounts_EnergyProviderRateClasses", arguments.AssociationObject)/>

	</cffunction>

	<cffunction name="saveExistingEnergyNativeAccountEnergyProviderRateClassAssociation"
				hint="This method is used to persist an existing native energy account / energy provider rate class association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AssociationObject" required="true" type="struct" hint="Describes the VO containing the energy native account / energy provider rate class association record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyNativeAccounts_EnergyProviderRateClasses", arguments.AssociationObject)/>

	</cffunction>

	<cffunction name="saveNewEnergyProviderRateClassRateModelAssociation"
				hint="This method is used to persist a new energy provider rate class / rate model association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AssociationObject" required="true" type="struct" hint="Describes the VO containing the energy provider rate class / rate model association record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyProviderRateClasses_RateModels", arguments.AssociationObject)/>

	</cffunction>

	<cffunction name="saveExistingEnergyProviderRateClassRateModelAssociation"
				hint="This method is used to persist an existing energy provider rate class / rate model association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AssociationObject" required="true" type="struct" hint="Describes the VO containing the energy provider rate class / rate model association record that will be persisted."/>

		<!--- Persist the Energy Account data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyProviderRateClasses_RateModels", arguments.AssociationObject)/>

	</cffunction>




	<!---
		***************************************
		** Related to "Native Account History"
		***************************************
	--->
	<!--- Used to get data to display in "Native Account History" grid --->
	<cffunction name="getNativeAccountHistory" returntype="query" access="public">
		<cfargument name="energy_account_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				ena.native_account_uid,
				rel.relationship_id,
				rel.relationship_start,
				rel.relationship_end,
				rel.is_active,
				ena.native_account_number,
				prov.energy_provider_uid,
				prov.friendly_name as energyProviderFriendlyName,
				classes.rate_class_uid,
				classes.friendly_name as rateClassFriendlyName,
				groups.meta_group_uid,
				groups.friendly_name as metaGroupFriendlyName,
				rel.overridden_rate_model_uid,
				(SELECT friendly_name
				FROM RateModels models
				WHERE models.rate_model_uid = rel.overridden_rate_model_uid) AS rateModelFriendlyName
			FROM
				EnergyNativeAccounts ena JOIN
				EnergyNativeAccounts_EnergyProviderRateClasses rel
					ON (ena.native_account_uid = rel.native_account_uid) JOIN
				EnergyProviders prov
					ON (prov.energy_provider_uid = ena.energy_provider_uid) JOIN
				EnergyProviderRateClasses classes
					ON (classes.rate_class_uid = rel.rate_class_uid) JOIN
				MetaTypeGroups groups
					ON (groups.meta_group_uid = ena.meta_group_uid)
			WHERE
				energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" list="true">)
				and ena.is_active = 1
			ORDER BY
				rel.relationship_start ASC, <!--- Note that the UI expects the rows to be in this order, be careful about changing --->
				rel.relationship_end ASC	<!--- Note that the UI expects the rows to be in this order, be careful about changing --->
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<!--- Used when a new Native Account record is being inserted, and the dates on existing records therefore adjusted --->
	<cffunction name="adjustNativeAccountDates" returntype="void" access="public">
		<cfargument name="native_account_uid" type="string" required="true">
		<cfargument name="relationship_start" type="any" required="true" hint="Pass empty string or a date">
		<cfargument name="relationship_end" type="any" required="true" hint="Pass empty string or a date">
		<cfargument name="modifying_user" type="string" required="true">

		<cfquery datasource="#this.datasource#">
			UPDATE EnergyNativeAccounts SET
				<cfif arguments.relationship_start neq "">
					relationship_start = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.relationship_start#">,
				</cfif>
				<cfif arguments.relationship_end neq "">
					relationship_end = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.relationship_end#">,
				</cfif>
				modified_date = getDate()
			WHERE
				native_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.native_account_uid#">
		</cfquery>
	</cffunction>

	<!--- Used when a new Native Account "relationship" record is being inserted, and the dates on existing records therefore adjusted --->
	<cffunction name="adjustNativeRelationshipsDates" returntype="void" access="public">
		<cfargument name="relationship_id" type="string" required="true">
		<cfargument name="relationship_start" type="any" required="true">
		<cfargument name="relationship_end" type="any" required="true">
		<cfargument name="modifying_user_uid" type="string" required="true">

		<cfquery datasource="#this.datasource#">
			UPDATE EnergyNativeAccounts_EnergyProviderRateClasses SET
				<cfif arguments.relationship_start neq "">
					relationship_start = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.relationship_start#">,
				</cfif>
				<cfif arguments.relationship_end neq "">
					relationship_end = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.relationship_end#">,
				</cfif>
				modified_date = getDate()
			WHERE
				relationship_id = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.relationship_id#">
		</cfquery>
	</cffunction>


	<cffunction name="insertNativeHistoryRecord" returntype="string">
		<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>

		<!--- Do the actual DB insertion --->
		<cfset var insertedUid = this.dataMgr.insertRecord("EnergyNativeAccounts", arguments.modifiedEnergyAccount)>

		<!--- Return identifier for inserted record --->
		<cfreturn insertedUid>
	</cffunction>

	<cffunction name="insertNativeRelationshipRecord" returntype="numeric">
		<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>

		<!--- Do the actual DB insertion --->
		<cfset var insertedId = this.dataMgr.insertRecord("EnergyNativeAccounts_EnergyProviderRateClasses", arguments.modifiedEnergyAccount)>

		<!--- Return identifier for inserted record --->
		<cfreturn insertedId>
	</cffunction>
	
	<cffunction name="checkForEnergyUsageRecords" returntype="boolean">
		<cfargument name="energy_account_uid" type="string" required="true"/>
		<cfargument name="relationship_start" type="string" required="true"/>
		<cfargument name="relationship_end" type="string" required="true"/>

		<cfset var startDateForNewRecord = DateFormat(arguments.relationship_start, "mm/dd/yyyy")>
		<cfset var endDateForNewRecord = DateFormat(arguments.relationship_end, "mm/dd/yyyy")>
		
		<!---cfset var startDateForNewRecord = DateFormat(arguments.relationship_start, "mm/dd/yyyy")>
		<cfset var endDateForNewRecord = DateFormat(arguments.relationship_end, "mm/dd/yyyy")--->
		
		<!---TODO: check if there are any EnergyUsages for the period of this native account, if so throw exception--->
		
		<cfquery name="q" datasource="#this.datasource#" result="tmpResult">
			SELECT eu.energy_usage_uid
			FROM 
				EnergyUsage eu
			WHERE
				eu.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#">
				and eu.is_active=1
				and ((eu.period_start >= <cfqueryparam value="#startDateForNewRecord#" cfsqltype="cf_sql_date"> and
						eu.period_start <= <cfqueryparam value="#endDateForNewRecord#" cfsqltype="cf_sql_date">) or
					(eu.period_end >= <cfqueryparam value="#startDateForNewRecord#" cfsqltype="cf_sql_date"> and
						eu.period_end <= <cfqueryparam value="#endDateForNewRecord#" cfsqltype="cf_sql_date">) or
					(eu.period_start < <cfqueryparam value="#startDateForNewRecord#" cfsqltype="cf_sql_date"> and
						eu.period_end > <cfqueryparam value="#endDateForNewRecord#" cfsqltype="cf_sql_date">)
				)
		</cfquery>
		
		<cfreturn tmpResult.RecordCount>
	</cffunction>
	
	<cffunction name="deleteNativeAccountHistory" returntype="boolean">
		<cfargument name="native_account_uid" type="string" required="true"/>

		<!---TODO: check if there are any EnergyUsages for the period of this native account, if so throw exception--->
		
		<cfquery datasource="#this.datasource#" result="tmpResult">
			SELECT eu.energy_usage_uid
			FROM 
				EnergyNativeAccounts na, EnergyUsage eu
			WHERE
				eu.energy_account_uid = na.energy_account_uid
				and eu.is_active=1
				and na.native_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.native_account_uid#">
				and (na.relationship_start<=eu.period_start
							or na.relationship_start is NULL or cast(na.relationship_start as time)<>'00:00:00' --needed because the relationship_start in EnergyNativeAccount is set to some CURRENT_DATE instead of NULL or 1970-01-01, not shure why and where
						)
				and (na.relationship_end>=eu.period_end
							or na.relationship_end is NULL
						)
		</cfquery>
		
		<cfif tmpResult.RecordCount neq 0>
			<cfthrow
				type="com.mcenergyinc.BackOffice.Related_data_error"
				errorcode="RelatedDataError"
				message="There are #tmpResult.RecordCount# energy usage records for this native account!"
				detail="There are #tmpResult.RecordCount# energy usage records for this native account! To delete it you must first delete the energy usage records.">			
		</cfif>
		
		<cfquery datasource="#this.datasource#">
			UPDATE EnergyNativeAccounts SET
				is_active = 0,
				modified_date = getDate()
			WHERE
				native_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.native_account_uid#">
		</cfquery>
		
		<cfreturn true>
	</cffunction>
	
	
	<!--- Used to get meta group uid by part of the name, fo example Retail Access group for matching Full Service --->
	<cffunction name="getMetaGroupUidByName" returntype="string" access="public">
		<cfargument name="meta_group_name" type="string" required="true">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT TOP 1 meta_group_uid as mg_uid
			FROM MetaTypeGroups 
			WHERE
				friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_string" value="#arguments.meta_group_name#">
		</cfquery>
		
		<cfreturn q.mg_uid>
	</cffunction>


</cfcomponent>