﻿<cfcomponent displayname="Rate Model Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving rate model data from the application database.">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyRateModelComponent" 
				returntype="mce.e2.vo.RateModel"
				hint="This method is used to retrieve a rate model value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var rateModelComponent = createObject("component", "mce.e2.vo.RateModel")/>

		<!--- Return the rate model component --->
		<cfreturn rateModelComponent/>

	</cffunction>

	<cffunction name="getEmptyRateModelInputValueComponent" 
				returntype="mce.e2.vo.RateModelInputValue"
				hint="This method is used to retrieve a rate model input value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var rateModelInputValueComponent = createObject("component", "mce.e2.vo.RateModelInputValue")/>

		<!--- Return the rate model component --->
		<cfreturn rateModelInputValueComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getAllRateModelsAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of rate model value objects in the application.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate models and build out the array of components --->
		<cfset local.qRateModels = getRateModel(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qRateModels, "mce.e2.vo.RateModel")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getRateModelsAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve a filtered array collection of rate model value objects in the application.">

		<!--- Define the method arguments --->
		<cfargument name="rate_class_uid" type="string" required="false" hint="Describes the primary key of the energy provide rate class used to filter rate models.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate models and build out the array of components --->
		<cfset local.qRateModels = getRateModel(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qRateModels, "mce.e2.vo.RateModel")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getRateModelInputValuesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of rate model input value objects for all the rate model input values for a given rate model in the application.">

		<!--- Initialize the method arguments --->
		<cfargument name="input_set_uid" type="string" required="false" hint="Describes the uid used to identify a rate model input set.">
		<cfargument name="rate_model_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate model record; can also contain a list of comma-delimited rate model primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate models and build out the array of components --->
		<cfset local.qRateModelInputValues = getRateModelInputValue(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qRateModelInputValues, "mce.e2.vo.RateModelInputValue")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getRateModelInputSetsAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of rate model input sets.">

		<!--- Initialize the method arguments --->
		<cfargument name="set_type_code" required="false" type="string" hint="Describes the lookup code used to identify a rate model input set."/>
		<cfargument name="input_set_uid" required="false" type="string" hint="Describes the uid used to identify a rate model input set."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate model input sets and build out the array of components --->
		<cfset local.qRateModelInputSets = getRateModelInputSets(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qRateModelInputSets, "mce.e2.vo.RateModelInputSet")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getRateModelAsComponent"
				returntype="mce.e2.vo.RateModel"
				hint="This method is used to retrieve a rate model record from the application database, and populate its properties in a rate model value object.">

		<!--- Define the arguments for the method --->
		<cfargument name="rate_model_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given rate model record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate models and build out the array of components --->
		<cfset local.qRateModel = getRateModel(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVo(local.qRateModel, "mce.e2.vo.RateModel")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>
	
	<cffunction name="getRateModelInputvalueAsComponent"
				returntype="mce.e2.vo.RateModelInputValue"
				hint="This method is used to retrieve a rate model input value record from the application database, and populate its properties in a rate model value object.">

		<!--- Define the arguments for the method --->
		<cfargument name="relationship_id" type="numeric" required="false" hint="Describes the primary key / unique identifier for a given rate model input value record.">
		<cfargument name="model_input_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate model input set.">
		<cfargument name="input_set_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate model input set definition.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate model input values and build out the array of components --->
		<cfset local.qRateModelInputValue = getRateModelInputValue(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVo(local.qRateModelInputValue, "mce.e2.vo.RateModelInputValue")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>	
	
	<!--- Define the methods used to interact with rate model data in the application database --->
	<cffunction name="getRateModel"
				returntype="query"
				hint="This method is used to retrieve any rate model record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="rate_model_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate model record; can also contain a list of comma-delimited rate model primary keys.">
		<cfargument name="rate_class_uid" type="string" required="false" hint="Describes the primary key of the energy provide rate class used to filter rate models.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the rate models in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	distinct
					rm.rate_model_uid,
					rm.model_lookup_code,
					rm.friendly_name,
					rm.implementation_file,
					rm.defaults_input_set_uid,
					rm.interval_classifier_uid,
					rm.is_active,
					rm.created_by,
					rm.created_date,
					rm.modified_by,
					rm.modified_date
						
			from	dbo.rateModels rm (nolock)

					<!--- Filter on the rate class --->
					<cfif structKeyExists(arguments, 'rate_class_uid')>
					join dbo.EnergyProviderRateClasses_RateModels rcrm
						on rm.rate_model_uid = rcrm.rate_model_uid
					</cfif>
					
			where	1=1

					<cfif arguments.selectMethod eq 'current'>																					
					-- Filter on active data only 
					and rm.is_active = 1
					</cfif>

					<!--- Filter on the rate class --->
					<cfif structKeyExists(arguments, 'rate_class_uid')>
					<cfif arguments.selectMethod eq 'current'>																					
					and (

						-- Filter on active data while respecting the join relationships 				
						rcrm.is_active = 1
						and ( rcrm.relationship_end is null or rcrm.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
						and rcrm.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">											

					)					
					</cfif>
					
					and rcrm.rate_class_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.rate_class_uid#">)
					</cfif>
			
					<cfif structKeyExists(arguments, 'rate_model_uid')>
					and rm.rate_model_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.rate_model_uid#">)
					</cfif>

			order
			by		rm.friendly_name

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>
		
	</cffunction>
	
	<cffunction name="getRateModelInputValue"
				returntype="query"
				hint="This method is used to retrieve any rate model input value record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="relationship_id" type="numeric" required="false" hint="Describes the primary key / unique identifier for a given rate model input value record.">
		<cfargument name="input_set_uid" type="string" required="false" hint="Describes the primary key / unique identifier for a given rate model input value record definition.">
		<cfargument name="model_input_uid" type="string" required="false" hint="Describes the primary key / unique identifier for a given rate model input set.">
		<cfargument name="rate_model_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate model record; can also contain a list of comma-delimited rate model primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the rate models in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	rmi.relationship_id,
					rmi.model_input_uid,
					rmi.input_set_uid,
					rm.friendly_name as inputFriendlyName,
					rmi.factor_lookup_code,
					rm.input_lookup_code,
					rmi.input_value,
					rmi.relationship_start,
					rmi.relationship_end,
					rmi.is_active,
					rmi.created_by,
					rmi.created_date,
					rmi.modified_by,
					rmi.modified_date
						
			from	dbo.RateModelInputSet_RateModelInputs rmi
					join dbo.RateModelInputs rm
						on rmi.model_input_uid = rm.model_input_uid
						
			where	1=1
					<cfif arguments.selectMethod eq 'current'>
					and rmi.is_active = 1
					and rm.is_active = 1
					
					and (

							-- Filter on active data while respecting the join relationships		
							rmi.is_active = 1
							and ( rmi.relationship_end is null or rmi.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and rmi.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
						
						)	
					</cfif>
					<cfif structKeyExists(arguments, 'rate_model_uid')>
					and rm.rate_model_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.rate_model_uid#">)
					</cfif>
			
					<cfif structKeyExists(arguments, 'relationship_id')>
					and rmi.relationship_id in( <cfqueryparam cfsqltype="cf_sql_integer" list="true" value="#arguments.relationship_id#">)
					</cfif>

					<cfif structKeyExists(arguments, 'input_set_uid')>
					and rmi.input_set_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.input_set_uid#">)
					</cfif>

					<cfif structKeyExists(arguments, 'model_input_uid')>
					and rmi.model_input_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.model_input_uid#">)
					</cfif>

			order
			by		rm.friendly_name,
					rm.input_lookup_code,
					rmi.relationship_start,
					rmi.relationship_end

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>
		
	</cffunction>
	
	<cffunction name="getRateModelInputSets"
				hint="This method is used to retrieve single / multiple records from the EnergyContracts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="set_type_code" required="false" type="string" hint="Describes the lookup code used to identify a rate model input set."/>
		<cfargument name="input_set_uid" required="false" type="string" hint="Describes the uid used to identify a rate model input set."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Contract records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		select	rmis.input_set_uid,
				rmis.friendly_name,
				rmis.set_type_code,
				rmis.is_active,
				rmis.created_by,
				rmis.created_date,
				rmis.modified_by,
				rmis.modified_date
		from	dbo.RateModelInputSets rmis
		where	1 = 1
				<cfif structKeyExists(arguments, 'set_type_code') and len(arguments.set_type_code)>
				and	rmis.set_type_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.set_type_code#" list="true" null="false"> )
				</cfif>
				<cfif structKeyExists(arguments, 'input_set_uid') and len(arguments.input_set_uid)>
				and	rmis.input_set_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.input_set_uid#" list="true" null="false"> )
				</cfif>
				<cfif structKeyExists(arguments, 'selectMethod') and len(arguments.selectMethod)>
				and rmis.is_active = 1
				</cfif>
		</cfquery>

		<cfreturn local.qResult>
	</cffunction>

	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewRateModel" 
				access="public" returntype="string"
				hint="This method is used to persist a new rate model record to the application database.">
		<cfargument name="rateModel" type="mce.e2.vo.RateModel" hint="Describes the VO containing the rate model record that will be persisted.">

		<!--- ToDo: [ADL] This method and stored procedure need to be extended to include the recording
					of audit information when a given rate model is created.  For now, we will let
					the defaults populate. --->

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Execute the create rate model stored procedure --->
		<cfstoredproc datasource="#this.datasource#" procedure="dbo.rateModelCalculatorCreate" returncode="No">
			<cfprocparam cfsqltype="cf_sql_varchar" type="in" dbvarname="@model_lookup_code" value="#arguments.rateModel.model_lookup_code#">
			<cfprocparam cfsqltype="cf_sql_varchar" type="in" dbvarname="@friendly_name" value="#arguments.rateModel.friendly_name#">
			<cfprocparam cfsqltype="cf_sql_varchar" type="in" dbvarname="@implementation_file" value="#arguments.rateModel.implementation_file#">
			<cfprocparam cfsqltype="cf_sql_idstamp" type="in" dbvarname="@defaults_input_set_uid" value="#arguments.rateModel.defaults_input_set_uid#" null="#iif(isUniqueIdentifier(arguments.rateModel.defaults_input_set_uid), 'false', 'true')#" >
			<cfprocparam cfsqltype="cf_sql_idstamp" type="inout" dbvarname="@model_uid" variable="local.rate_model_uid" value="#arguments.rateModel.rate_model_uid#" null="#iif(isUniqueIdentifier(arguments.rateModel.rate_model_uid), 'false', 'true')#" >
		</cfstoredproc>

		<!--- Return the primary key --->
		<cfreturn local.rate_model_uid>

	</cffunction>	
	
	<cffunction name="saveExistingRateModel" 
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing rate model record to the application database.">
		<cfargument name="rateModel" type="mce.e2.vo.RateModel" hint="Describes the VO containing the rate model record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Persist the rateModel data to the application database --->
		<cfset local.newUid = this.dataMgr.updateRecord("RateModels", arguments.rateModel)>
		
	</cffunction>	

	<cffunction name="saveExistingRateModelInputSet" 
				access="public" returntype="string"
				hint="This method is used to persist changes for an existing rate model input set.">
		<cfargument name="rateModelInputSet" type="mce.e2.vo.RateModelInputSet" hint="Describes the VO containing the rate model input set record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Persist the rateModelInputSet data to the application database --->
		<cfset local.newUid = this.dataMgr.updateRecord("RateModelInputSets", arguments.rateModelInputSet)>
		
	</cffunction>	

	<cffunction name="saveNewRateModelInputSet" 
				access="public" returntype="string"
				hint="This method is used to persist a new rate model input set record to the application database.">
		<cfargument name="rateModelInputSet" type="mce.e2.vo.RateModelInputSet" hint="Describes the VO containing the rate model input set record that will be persisted.">

		<!--- Persist the rateModelInputSet data to the application database --->
		<cfset local.primaryKey = this.dataMgr.insertRecord("RateModelInputSets", arguments.rateModelInputSet)>

		<!--- Persist the rateModelInputSet data to the application database --->			
		<cfreturn local.primaryKey>

	</cffunction>		

	<cffunction name="saveExistingRateModelInputValue" 
				access="public" returntype="string"
				hint="This method is used to persist changes for an existing rate model input value record to the application database.">
		<cfargument name="rateModelInputValue" type="mce.e2.vo.RateModelInputValue" hint="Describes the VO containing the rate model input value record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Persist the rateModel data to the application database --->
		<cfset local.newUid = this.dataMgr.updateRecord("RateModelInputSet_RateModelInputs", arguments.rateModelInputValue)>
		
	</cffunction>	

	<cffunction name="saveNewRateModelInputValue" 
				access="public" returntype="string"
				hint="This method is used to persist a new rate model input value record to the application database.">
		<cfargument name="rateModelInputValue" type="mce.e2.vo.RateModelInputValue" hint="Describes the VO containing the rate model input value record that will be persisted.">

		<!--- Persist the rateModelInputValue data to the application database --->
		<cfset local.primaryKey = this.dataMgr.insertRecord("RateModelInputSet_RateModelInputs", arguments.rateModelInputValue)>

		<!--- Persist the rateModel data to the application database --->			
		<cfreturn local.primaryKey>

	</cffunction>		

</cfcomponent>