<cfcomponent displayname="Property Collection Type Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving property collection type data from the application database.">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyCollectionTypeComponent" 
				returntype="mce.e2.vo.PropertyCollectionType"
				hint="This method is used to retrieve a property collection type value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var propertyCollectionTypeComponent = createObject("component", "mce.e2.vo.PropertyCollectionType")/>

		<!--- Return the property collection type component --->
		<cfreturn propertyCollectionTypeComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertyCollectionTypesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of property collection value objects that are available to a given user.">

		<!--- Define the arguments for the method --->
		<cfargument name="collection_type_code" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection type record(s); accepts a comma delimited list of type codes.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all properties collection types and build out the array of components --->
		<cfset local.qPropertyCollectionTypes = getPropertyCollectionType(argumentCollection=arguments)>
		<cfset local.pcArray = queryToVoArray(local.qPropertyCollectionTypes, "mce.e2.vo.PropertyCollectionType")>

		<!--- Return the Vo Array --->
		<cfreturn local.pcArray>

	</cffunction>

	<cffunction name="getPropertyCollectionTypeAsComponent"
				returntype="mce.e2.vo.PropertyCollectionType"
				hint="This method is used to retrieve a property collection type record from the application database, and populate its details in a property collection type value object.">

		<!--- Define the arguments for the method --->
		<cfargument name="collection_type_code" type="string" required="true" hint="Describes the primary key / unique identifier of a given property collection type record; accepts a comma delimited list of property collection type codes.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qPropertyCollectionTypes = getPropertyCollectionType(argumentCollection=arguments)>

		<!--- If so, the populate the vo object --->
		<cfset local.returnResult = queryToVo(local.qPropertyCollectionTypes, "mce.e2.vo.PropertyCollectionType")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Define the methods used to interact with the database --->
	<cffunction name="getPropertyCollectionType"
				access="public"
				returnType="query"
				hint="This method is used to retrieve property collection types from the application database.">
	
		<!--- Define the arguments for the method --->
		<cfargument name="collection_type_code" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection type record; accepts a comma delimited list of property collection type codes.">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the property collection types in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	pct.collection_type_code,
					pct.friendly_name,
					pct.is_active,
					pct.created_by,
					pct.created_date,
					pct.modified_by,
					pct.modified_date
					
			from	dbo.propertyCollectionTypes pct
			where	1=1
			
					-- Filter on active data			
					and pct.is_active = 1
			
					<cfif structKeyExists(arguments, "collection_type_code")>
					pct.collection_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#arguments.collection_type_code#"> )
					</cfif>
					
			order
			by		pct.friendly_name

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>	
	
	</cffunction>
	
</cfcomponent>