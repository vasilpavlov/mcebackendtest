<cfcomponent displayname="Property Collection Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving property collection data from the application database.">


	<cffunction name="makeAggregatedPropertyCollectionComponent" returntype="mce.e2.vo.PropertyCollectionPropertyAlertAndToDoCounts">
		<cfargument name="fromPropertyCollections" type="array" required="true">
		<cfargument name="friendly_name" type="string" required="true">

		<cfset var local = StructNew()>

		<cfset local.addedMap = StructNew()>
		<cfset local.allPropertiesCollection = CreateObject("component", "mce.e2.vo.PropertyCollectionPropertyAlertAndToDoCounts")>
		<cfset local.allPropertiesCollection.property_collection_uid = super.createUniqueIdentifierPlaceholder()>
		<cfset local.allPropertiesCollection.friendly_name = arguments.friendly_name>
		<cfset local.allPropertiesCollection.image_filename = "">
		<cfset local.allPropertiesCollection.is_active = 1>
		<cfset local.allPropertiesCollection.propertyItems = ArrayNew(1)>

		<cfloop array="#arguments.fromPropertyCollections#" index="local.item">
			<cfloop array="#local.item.propertyItems#" index="local.propertyItem">
				<cfif not StructKeyExists(local.addedMap, local.propertyItem.property_uid)>
					<cfset local.addedMap[local.propertyItem.property_uid] = true>
					<cfset ArrayAppend(local.allPropertiesCollection.propertyItems, duplicate(local.propertyItem))>
				</cfif>
			</cfloop>
		</cfloop>

		<cfset local.allPropertiesCollection.propertyCount = ArrayLen(local.allPropertiesCollection.propertyItems)>

		<cfreturn local.allPropertiesCollection>
	</cffunction>

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyCollectionComponent"
				returntype="mce.e2.vo.PropertyCollection"
				hint="This method is used to retrieve a property value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var propertyCollectionComponent = createObject("component", "mce.e2.vo.PropertyCollection")/>

		<!--- Return the property collection component --->
		<cfreturn propertyCollectionComponent/>

	</cffunction>

	<cffunction name="getEmptyParticipantPropertyComponent"
				returntype="mce.e2.vo.ParticipantProperty"
				hint="This method is used to retrieve a participant property value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var participantPropertyComponent = createObject("component", "mce.e2.vo.ParticipantProperty")/>

		<!--- Return the property collection component --->
		<cfreturn participantPropertyComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertyCollectionsAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of property collection value objects that are available to a given user.">

		<!--- Initialize the method arguments --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record.">
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection; can also contain a list of comma-delimited property collection primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user group record; can also contain a list of comma-delimited user group primary keys.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user record; can also contain a list of comma-delimited user primary keys.">
		<cfargument name="session_key" type="string" required="false" hint="Describes the primary key / unique identifier of a given user's session record; used to filter collections that are / are not associated to a given user.">
		<cfargument name="selectMethod" type="string" required="true" default="singular-inclusive" hint="Describes the method used to retrieve data (inclusive / exclusive / userGroupAssociations).">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Determine the query to execute based on the selectMethod --->
		<cfswitch expression="#arguments.selectMethod#">

			<cfcase value="userAssociations">

				<!--- Retrieve a comprehensive list of user group associations --->
				<cfset local.qPropertyCollections = getUserAssociations(argumentCollection=arguments)>

			</cfcase>

			<cfcase value="userGroupAssociations">

				<!--- Retrieve a comprehensive list of user group associations --->
				<cfset arguments.selectMethod = "current">
				<cfset local.qPropertyCollections = getUserGroupAssociations(argumentCollection=arguments)>

			</cfcase>

			<cfdefaultcase>

				<!--- Retrieve all properties and build out the array of components --->
				<cfset local.qPropertyCollections = getPropertyCollection(argumentCollection=arguments)>

			</cfdefaultcase>

		</cfswitch>

		<!--- Convert the query to an array of value objects --->
		<cfset local.pcArray = queryToVoArray(local.qPropertyCollections, "mce.e2.vo.PropertyCollection")>

		<!--- Return the Vo Array --->
		<cfreturn local.pcArray>

	</cffunction>

	<cffunction name="getPropertyCollectionPropertyAlertAndToDoCountsAsArrayOfComponents"
				acccess="public" returntype="array"
				hint="This method is used to retrieve an array collection of property collection value objects that are available to a given user.">

		<!--- Initialize the method arguments --->
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also be a comma delimited list of property collection records.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qPropertyCollections = getPropertyCollectionPropertyAlertAndToDoCounts(argumentCollection=arguments)>

		<!--- Convert the query to an array of value objects --->
		<cfset local.pcArray = queryToVoArray(local.qPropertyCollections, "mce.e2.vo.PropertyCollectionPropertyAlertAndToDoCounts")>

		<!--- Return the Vo Array --->
		<cfreturn local.pcArray>

	</cffunction>

	<cffunction name="getPropertyCollectionAsComponent"
				returntype="mce.e2.vo.PropertyCollection"
				hint="This method is used to retrieve a property collection record from the application database, and populate its details in a property collection value object.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property collection record.">
		<cfargument name="byPassSecurity" type="boolean" required="true" default="false" hint="Describes whether the created property collection object should be returned without a property collection / user group association." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get an instance of the security service --->
		<cfset local.securityService = request.beanFactory.getBean("SecurityService")>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qPropertyCollection = getPropertyCollection(argumentCollection=arguments)>

		<!--- Is the primary key of the property collection valid for the current user? --->
		<cfif local.securityService.isPropertyCollectionAllowed(argumentCollection=arguments) or (arguments.byPassSecurity is true)>

			<!--- If so, the populate the vo object --->
			<cfset local.returnResult = queryToVo(local.qPropertyCollection, "mce.e2.vo.PropertyCollection")>

		<cfelse>

			<!--- Otherwise, populate an empty vo object --->
			<cfset local.returnResult = getEmptyPropertyCollectionComponent()>

		</cfif>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getPropertiesInACollectionAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve a collection of properties.">
		<cfargument name="property_collection_uid" type="string" hint="Describes the primary key of the property collection for which properties will be retrieved.">
		<cfargument name="byPassSecurity" type="boolean" required="true" default="false" hint="Describes whether or not the security layer for property collections should be bypassed.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Should security be bypassed --->
		<cfif arguments.byPassSecurity>

			<!--- default the select properties flag to true --->
			<cfset local.selectProperties = true>

		<cfelse>

			<!--- Get an instance of the security service --->
			<cfset local.securityService = request.beanFactory.getBean("SecurityService")>

			<!--- Is the primary key of the property collection valid for the current user? --->
			<cfset local.selectProperties = local.securityService.isPropertyCollectionAllowed(property_collection_uid = arguments.property_collection_uid)>

		</cfif>

		<!--- Should properties be selected? --->
		<cfif local.selectProperties>

			<!--- Execute a query to return the properties for a given collection; then covert the query to an array of vo's '--->
			<cfset local.qPcProperties = getPropertiesInACollection(property_collection_uid=arguments.property_collection_uid)>
			<cfset local.pcArray = queryToVoArray(local.qPcProperties, "mce.e2.vo.Property")>

		<cfelse>

			<!--- Otherwise, populate a blank array --->
			<cfset local.pcArray = arrayNew(1)>

		</cfif>

		<!--- Return the array of properties --->
		<cfreturn local.pcArray>

	</cffunction>

	<cffunction name="getPropertyAlertAndToDoCountsAsArrayOfComponents"
				acccess="public" returntype="array"
				hint="This method is used to retrieve an array collection of property value objects with alert and to do counts.">

		<!--- Initialize the method arguments --->
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also be a comma delimited list of property collection records.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qProperties = getPropertyAlertAndToDoCounts(argumentCollection=arguments)>

		<!--- Convert the query to an array of value objects --->
		<cfset local.pcArray = queryToVoArray(local.qProperties, "mce.e2.vo.PropertyAlertAndToDoCounts")>

		<!--- Return the Vo Array --->
		<cfreturn local.pcArray>

	</cffunction>

	<cffunction name="getParticipantPropertiesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of participant property values objects for a given data review participant.">

		<!--- Initialize the method arguments --->
		<cfargument name="data_review_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="review_participant_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review participant record."/>
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property collection primary keys.">
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="filter_properties_by_review_participant_uid_list" type="string" required="false" hint="Describes the primary key / unique identifier of a given data review participant record; can also contain a list of comma-delimited data review participant primary keys that will be used to filter properties from.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all properties and build out the array of components --->
		<cfset local.qProperties = getParticipantProperties(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qProperties, "mce.e2.vo.ParticipantProperty")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Define the methods used to interact with property collection data in the application database --->
	<cffunction name="getUserGroupAssociations"
				returntype="query"
				hint="This method is used to retrieve a comprehensive collection of user group / property collection associations.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user group record; can also contain a list of comma-delimited user group primary keys.">
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property collection primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			<!--- Run the inclusive select first --->
			select	distinct
					pc.client_company_uid,
					pc.property_collection_uid,
					pc.collection_hierarchyid.ToString() as collection_hierarchyid,
					pc.friendly_name,
					pc.image_filename,
					pc.collection_type_code,

					(
						select	count(*)
						from 	dbo.properties_propertyCollections ppc1
						where 	ppc1.property_collection_uid = pc.property_collection_uid
								and ppc1.is_active = 1
								and ( ppc1.relationship_end is null or ppc1.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and ppc1.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					) as propertyCount,
					1 as isAssociated,
					case

						when ugpc.user_group_uid = ug.user_group_uid then ugpc.relationship_id
						else null
					end relationship_id,

					pc.is_active,
					pc.created_by,
					pc.created_date,
					pc.modified_by,
					pc.modified_date

			from	dbo.propertyCollections pc
					join dbo.UserGroups_PropertyCollections ugpc
						on pc.property_collection_uid = ugpc.property_collection_uid
					join dbo.userGroups ug
						on ugpc.user_group_uid = ug.user_group_uid
						and ug.client_company_uid = pc.client_company_uid
						<cfif isUniqueIdentifier(arguments.user_group_uid)>
						and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
						</cfif>

			where	1=1
					and ugpc.is_active = 1
					<cfif arguments.selectMethod eq 'current'>
					and pc.is_active = 1
					
					and (

							-- Only retrieve active associations
							ugpc.is_active = 1
							and ( ugpc.relationship_end is null or ugpc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and ugpc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)

					and ug.is_active = 1
					</cfif>

					and pc.client_company_uid in(<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)

			union

			select	distinct
					pc.client_company_uid,
					pc.property_collection_uid,
					pc.collection_hierarchyid.ToString() as collection_hierarchyid,
					pc.friendly_name,
					pc.image_filename,
					pc.collection_type_code,

					(
						select	count(*)
						from 	dbo.properties_propertyCollections ppc2
						where 	ppc2.property_collection_uid = pc.property_collection_uid
								and ppc2.is_active = 1
								and ( ppc2.relationship_end is null or ppc2.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and ppc2.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					) as propertyCount,
					0 as isAssociated,
					null as relationship_id,

					pc.is_active,
					pc.created_by,
					pc.created_date,
					pc.modified_by,
					pc.modified_date

			from	dbo.propertyCollections pc
			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					and pc.is_active = 1
					</cfif>

					and pc.client_company_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					and pc.property_collection_uid not in (

						select 	ugpc1.property_collection_uid
						from	dbo.userGroups_PropertyCollections ugpc1
								join dbo.userGroups ug1
									on ugpc1.user_group_uid = ug1.user_group_uid
						where	1=1
								and ug1.is_active = 1 --Usergroup should always be active
								and ugpc1.is_active = 1 --PropertyCollection to UserGroup should always be active
								<cfif arguments.selectMethod eq 'current'>
								and ug1.is_active = 1
								and (

										-- Only retrieve active associations
										ugpc1.is_active = 1
										and ( ugpc1.relationship_end is null or ugpc1.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
										and ugpc1.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

									)

								and ugpc1.is_active = 1
								</cfif>

								and ug1.client_company_uid in(<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
								<cfif structKeyExists(arguments, 'user_group_uid') and isUniqueIdentifier(arguments.user_group_uid)>
								and ug1.user_group_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#">)
								</cfif>
					)

			order
			by		pc.friendly_name,
					pc.modified_date

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getPropertyCollectionPropertyAlertAndToDoCounts"
				returntype="query"
				hint="This method is used to retrieve property collections and their property, alert, and to-do counts.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record.">
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also be a comma delimited list of property collection records.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	distinct
					pc.property_collection_uid,
					pc.friendly_name,
					pc.image_filename,

					(
						select	count(*)
						from 	dbo.properties_propertyCollections ppc1 (nolock)
						where 	property_collection_uid = pc.property_collection_uid
								and ppc1.is_active = 1
								and ( ppc1.relationship_end is null or ppc1.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and ppc1.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					) as propertyCount,

					(
						select	count(*)
						from 	dbo.properties_propertyCollections ppc2 (nolock)
								join dbo.alerts a1 (nolock)
									on ppc2.property_uid = a1.property_uid
								join dbo.alertTypes ats1 (nolock)
									on a1.alert_type_code = ats1.alert_type_code
						where 	ppc2.property_collection_uid = pc.property_collection_uid
								and ppc2.is_active = 1
								and ( ppc2.relationship_end is null or ppc2.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and ppc2.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
								and a1.is_active = 1
								and ats1.is_active = 1

					) as alertCounts,

					(
						select	count(*)
						from 	dbo.properties_propertyCollections ppc3 (nolock)
								join dbo.alerts a2 (nolock)
									on ppc3.property_uid = a2.property_uid
								join dbo.alertTypes ats2 (nolock)
									on a2.alert_type_code = ats2.alert_type_code
						where 	ppc3.property_collection_uid = pc.property_collection_uid
								and ppc3.is_active = 1
								and ( ppc3.relationship_end is null or ppc3.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and ppc3.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
								and a2.is_active = 1
								and ats2.is_active = 1

					) as todoCounts,
					pc.is_active,
					pc.created_by,
					pc.created_date,
					pc.modified_by,
					pc.modified_date

			from	dbo.propertyCollections pc
			where	1=1

					and pc.is_active = 1

					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and pc.property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#">)
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and pc.client_company_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>

			order
			by		pc.friendly_name,
					pc.modified_date

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getPropertyCollection"
				returntype="query"
				hint="This method is used to retrieve any property collection record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property collection primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user group record; can also contain a list of comma-delimited user group primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="singular-inclusive" hint="Describes the type of select sql operation to perform ( inclusive = using [in]; exclusive = using [not in] ) when selecting property collections ).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	distinct
					pc.client_company_uid,
					pc.property_collection_uid,
					pc.collection_hierarchyid.ToString() as collection_hierarchyid,
					pc.friendly_name,
					pc.image_filename,
					pc.collection_type_code,

					(
						select	count(*)
						from 	dbo.properties_propertyCollections ppc1
						where 	ppc1.property_collection_uid = pc.property_collection_uid
								and ppc1.is_active = 1
								and ( ppc1.relationship_end is null or ppc1.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and ppc1.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					) as propertyCount,

			<cfswitch expression="#arguments.selectMethod#">

				<cfcase value="inclusive">
					-- Select the meta columns
					1 as isAssociated,
					case

						when ugpc.user_group_uid = ug.user_group_uid then ugpc.relationship_id
						else null
					end relationship_id,
				</cfcase>

				<cfcase value="exclusive">
					-- Select the meta columns
					0 as isAssociated,
					null as relationship_id,
				</cfcase>

			</cfswitch>

					pc.is_active,
					pc.created_by,
					pc.created_date,
					pc.modified_by,
					pc.modified_date

			<cfswitch expression="#arguments.selectMethod#">

				<cfcase value="singular-inclusive">
			from	dbo.propertyCollections pc
			where	1=1

					and pc.is_active = 1

					<!--- Filter on the company --->
					<cfif structKeyExists(arguments, 'client_company_uid')>
					and pc.client_company_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>

					<!--- Filter on a specific property collection --->
					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and pc.property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#">)
					</cfif>
				</cfcase>

				<cfcase value="singular-exclusive">
			from	dbo.propertyCollections pc
			where	1=1

					and pc.is_active = 1

					<!--- Filter on the company --->
					<cfif structKeyExists(arguments, 'client_company_uid')>
					and pc.client_company_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>

					<!--- Filter on a specific property collection --->
					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and pc.property_collection_uid not in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#">)
					</cfif>
				</cfcase>

				<cfcase value="inclusive">
			from	dbo.propertyCollections pc
					join dbo.userGroups_PropertyCollections ugpc
						on pc.property_collection_uid = ugpc.property_collection_uid
					join dbo.userGroups ug
						on ugpc.user_group_uid = ug.user_group_uid
						and ug.client_company_uid = pc.client_company_uid
						<cfif structKeyExists(arguments, 'user_group_uid') and isUniqueIdentifier(arguments.user_group_uid)>
						and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
						</cfif>
			where	1=1

					and pc.is_active = 1
					and ugpc.is_active = 1
					and (

							-- Only retrieve active associations
							ugpc.is_active = 1
							and ( ugpc.relationship_end is null or ugpc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and ugpc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)

					and ug.is_active = 1

					<!--- Filter on the company --->
					<cfif structKeyExists(arguments, 'client_company_uid')>
					and pc.client_company_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>

					<!--- Filter on the property collection --->
					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and pc.property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#">)
					</cfif>
				</cfcase>

				<cfcase value="exclusive">
				from	dbo.propertyCollections pc
				where	1=1

						and pc.is_active = 1

						and pc.client_company_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
						and pc.property_collection_uid not in (

							select 	ugpc1.property_collection_uid
							from	dbo.userGroups_PropertyCollections ugpc1
									join dbo.userGroups ug1
										on ugpc1.user_group_uid = ug1.user_group_uid
							where	1=1

									and ug1.is_active = 1
									and ugpc1.is_active = 1
									and (

											-- Only retrieve active associations
											ugpc1.is_active = 1
											and ( ugpc1.relationship_end is null or ugpc1.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
											and ugpc1.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

										)

									and ug1.client_company_uid in(<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
									<cfif structKeyExists(arguments, 'user_group_uid') and isUniqueIdentifier(arguments.user_group_uid)>
									and ug1.user_group_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#">)
									</cfif>
						)
				</cfcase>

			</cfswitch>

			order
			by		pc.friendly_name,
					pc.modified_date

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getPortfolioOverview"
				returntype="query"
				hint="This method is used to retrieve a high level overview of properties for a given Property Collection.">

			<!--- Define the arguments for the method --->
			<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property collection primary keys.">
			<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
			<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
			<cfargument name="period_start" type="string" required="true" hint="Describes the energy usage start period.">getParticipantProperties
			<cfargument name="period_end" type="string" required="true" hint="Describes the energy usage end period.">
			<cfargument name="energy_type_uids" type="string" required="false" hint="energy/resource types to pass.">
			
			<!--- Initialize any local variables --->
			<cfset var qResult = ''>
		<cfstoredproc datasource="#this.datasource#" procedure="rsPortfolioDetailCostByProperty">
			<cfprocparam value="#arguments.property_collection_uid#" cfsqltype="cf_SQL_IDSTAMP">
			<cfprocparam value="#arguments.energy_type_uids#" null="true" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#arguments.period_start#" cfsqltype="cf_SQL_DATE">
			<cfprocparam value="#arguments.period_end#" cfsqltype="cf_SQL_DATE">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
			<!--- Return the result set --->
			<cfreturn qResult>
	</cffunction>

	<cffunction name="getPropertiesInACollection"
				returntype="query"
				hint="This method is used to retrieve the property record(s) in a given property collection from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property collection primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	p.property_uid,
					p.friendly_name,
					p.image_filename,
					p.is_active,
					p.created_by,
					p.created_date,
					p.modified_by,
					p.modified_date,
					p.document_set_uid,
					p.currentClientUID as client_company_uid,

					-- Retrieve the count of energy accounts associated to a given property
					(select count(*) from dbo.energyAccounts ea where ea.is_active = 1 and ea.property_uid = p.property_uid) as energyAccountCount,

					-- These columns will be populated in the property vo to color the property collection association
					ppc.property_collection_uid as propertyCollectionUid,
					ppc.relationship_start as relationshipStart,
					ppc.relationship_end as relationshipEnd


			from	dbo.properties p
					join dbo.properties_propertyCollections ppc
						on p.property_uid = ppc.property_uid

			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					-- Filter on active data only
					and p.is_active = 1
					and (

							ppc.is_active = 1
							and ( ppc.relationship_end is null or ppc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and ppc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and ppc.property_collection_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#">)
					</cfif>

			order
			by		ppc.property_collection_uid,
					p.friendly_name

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getParticipantProperties"
				returntype="query"
				hint="This method is used to retrieve any property record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="data_review_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="review_participant_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review participant record."/>
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also contain a list of comma-delimited property collection primary keys.">
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property primary keys.">
		<cfargument name="filter_properties_by_review_participant_uid_list" type="string" required="false" hint="Describes the primary key / unique identifier of a given data review participant record; can also contain a list of comma-delimited data review participant primary keys that will be used to filter properties from.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			-- Initialize any variables required by this query
			declare @lookup_code varchar(250)

			-- Retrieve the lookup code for a given data review
			select	@lookup_code = dr.display_property_meta_type_lookup_code
			from	dbo.DataReviews dr (nolock)
			where	1=1
					and dr.data_review_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.data_review_uid#">
					and dr.is_active = 1

			-- Retrieve the properties
			select	distinct
					p.property_uid,
					p.friendly_name,
					p.image_filename

					,@lookup_code as displayPropertyMetaTypeLookupCode
					,pm.meta_value as displayPropertyMetaValue
					,apsc.approval_status_code

			from	dbo.properties p (nolock)
					join dbo.properties_propertyCollections ppc (nolock)
						on p.property_uid = ppc.property_uid
					join dbo.propertyCollections pc (nolock)
						on ppc.property_collection_uid = pc.property_collection_uid

					-- Retrieve the property meta information
					-- Check to make sure that this is being brought back
					left outer join (

						select	distinct
								pm.meta_value,
								pm.property_uid

						from	dbo.propertyMeta pm (nolock)
								join dbo.propertyMetaTypes pmt (nolock)
									on pm.meta_type_uid = pmt.meta_type_uid
								join dbo.DataReviews dr (nolock)
									on dr.display_property_meta_type_lookup_code = pmt.type_lookup_code
								join dbo.dataReviewDetail drd (nolock)
									on dr.data_review_uid = drd.data_review_uid
									and drd.property_uid = pm.property_uid

						where	1=1
								and dr.data_review_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.data_review_uid#">

								<cfif arguments.selectMethod eq 'current'>
								and drd.is_current_review_detail = 1
								and dr.is_active = 1
								and drd.is_active = 1
								and pmt.is_active = 1
								and (

									pm.is_active = 1
									and ( pm.relationship_end is null or pm.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
									and pm.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

								)
								</cfif>

					) pm on
					pm.property_uid = p.property_uid

					-- Retrieve the data review detail approval status
					-- Check to make sure that this is being brought back
					left outer join (

						select	distinct
								drd.property_uid,
								drd.approval_status_code

						from	dbo.DataReviews dr (nolock)
								join dbo.dataReviewDetail drd (nolock)
									on dr.data_review_uid = drd.data_review_uid

						where	1=1
								and dr.data_review_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.data_review_uid#">

								<cfif arguments.selectMethod eq 'current'>
								and drd.is_current_review_detail = 1
								and dr.is_active = 1
								and drd.is_active = 1
								</cfif>

					) apsc on
					apsc.property_uid = p.property_uid

			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					-- Filter on active data only
					and p.is_active = 1
					and pc.is_active = 1
					and (

							ppc.is_active = 1
							and ( ppc.relationship_end is null or ppc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and ppc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and ppc.property_collection_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#">)
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and p.property_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#">)
					</cfif>

					-- Include any collections that are inherited by this review participant
					and ppc.property_collection_uid in (

						select	drp2.property_collection_uid
						from	dbo.dataReviewParticipants drp2 (nolock)
						where	1=1

								<cfif arguments.selectMethod eq 'current'>
								-- Filter on active data only
								and drp2.is_active = 1
								</cfif>

								-- Retrieve the ownership level for this participant
								and drp2.review_participant_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.review_participant_uid#">
								and drp2.data_review_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.data_review_uid#">

					)

					<!--- Check and see if a list of dependent users was provided --->
					<cfif structKeyExists(arguments, 'filter_properties_by_review_participant_uid_list') and listlen(arguments.filter_properties_by_review_participant_uid_list) gt 0>
					and p.property_uid not in (

						select	distinct
								p3.property_uid
						from	dbo.properties p3 (nolock)
								join dbo.properties_propertyCollections ppc3 (nolock)
									on p3.property_uid = ppc3.property_uid
								join dbo.propertyCollections pc3 (nolock)
									on ppc3.property_collection_uid = pc3.property_collection_uid
								join dbo.dataReviewParticipants drp3 (nolock)
									on pc3.property_collection_uid = drp3.property_collection_uid
								join dbo.clientContacts cc3 (nolock)
									on cc3.client_contact_uid = drp3.client_contact_uid
								join dbo.users u3 (nolock)
									on u3.user_uid = cc3.user_uid

						where	1=1

								and p3.is_active = 1
								and pc3.is_active = 1
								and drp3.is_active = 1
								and cc3.is_active = 1
								and u3.is_active = 1

								and drp3.review_participant_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.filter_properties_by_review_participant_uid_list#"> )

					)
					</cfif>

			order
			by		p.friendly_name

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getPropertyAlertAndToDoCounts"
				returntype="query"
				hint="This method is used to retrieve properties in a given collection accompanied by their alert and to-do counts.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record.">
		<cfargument name="property_collection_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property collection record; can also be a comma delimited list of property collection records.">
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also be a comma delimited list of property records.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	distinct
					p.property_uid,
					p.friendly_name,
					p.image_filename,
					p.document_set_uid,
					(
						select	count(*)
						from 	dbo.alerts a1 (nolock)
								join dbo.alertTypes ats1 (nolock)
									on a1.alert_type_code = ats1.alert_type_code
						where 	p.property_uid = a1.property_uid
								and a1.is_active = 1
								and ats1.is_active = 1

					) as alertCounts,

					(
						select	count(*)
						from 	dbo.alerts a2 (nolock)
								join dbo.alertTypes ats2 (nolock)
									on a2.alert_type_code = ats2.alert_type_code
						where 	p.property_uid = a2.property_uid
								and a2.is_active = 1
								and ats2.is_active = 1

					) as todoCounts,

					(
						select count(*)
						from dbo.energyAccounts ea (nolock)
						where ea.property_uid = p.property_uid
								and ea.is_active = 1
					) as energyAccountCount,

					p.is_active,
					p.created_by,
					p.created_date,
					p.modified_by,
					p.modified_date,
					dbo.getPropertyMetaValue(p.property_uid, 'energyStarRating',<cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">) as energyStarRating,
					dbo.getPropertyMetaStartDate(p.property_uid, 'energyStarRating',<cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">) as energyStarRatingEffectiveDate

			from	dbo.properties p (nolock)
					join dbo.properties_propertyCollections ppc (nolock)
						on p.property_uid = ppc.property_uid
					join dbo.propertyCollections pc (nolock)
						on ppc.property_collection_uid = pc.property_collection_uid
			where	1=1

					and p.is_active = 1
					and pc.is_active = 1
					and (

							ppc.is_active = 1
							and ( ppc.relationship_end is null or ppc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and ppc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					)

					<cfif structKeyExists(arguments, 'property_collection_uid')>
					and pc.property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_collection_uid#">)
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and pc.client_company_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and p.property_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#">)
					</cfif>

			order
			by		p.friendly_name,
					p.modified_date

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getPropertyAndPropertyCollectionAssociation"
				returntype="query"
				hint="This method is used to retrieve the an association record ebtween a property and property collection.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property collection record.">
		<cfargument name="property_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property record.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	p.relationship_id,
					p.property_uid,
					p.property_collection_uid,
					p.relationship_start,
					p.relationship_end,
					p.is_active,
					p.created_by,
					p.created_date,
					p.modified_by,
					p.modified_date

			from	dbo.Properties_PropertyCollections p

			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					and (

						-- Filter on active data only
						p.is_active = 1
						and ( p.relationship_end is null or p.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
						and p.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

					)
					</cfif>

					and p.property_collection_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.property_collection_uid#">
					and p.property_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.property_uid#">

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getUserAndPropertyCollectionAssociation"
				returntype="query"
				hint="This method is used to retrieve the an association record ebtween a property and property collection.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property collection record.">
		<cfargument name="user_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given user record.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	p.relationship_id,
					p.user_uid,
					p.property_collection_uid,
					p.relationship_start,
					p.relationship_end,
					p.is_active,
					p.created_by,
					p.created_date,
					p.modified_by,
					p.modified_date

			from	dbo.PropertyCollections_Users p

			where	1=1
					and p.is_active = 1
					<cfif arguments.selectMethod eq 'current'>
					and (

							-- Filter on active data only
							p.is_active = 1
							and ( p.relationship_end is null or p.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and p.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					and p.property_collection_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.property_collection_uid#">
					and p.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.user_uid#">


		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getPropertyCollectionAssociations"
				returntype="query"
				hint="This method is used to retrieve the property / property collection association record from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property collection record.">
		<cfargument name="property_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given property record; can also contain a list of comma-delimited property collection primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the properties in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	p.relationship_id,
					p.property_uid,
					p.property_collection_uid,
					p.relationship_start,
					p.relationship_end,
					p.is_active,
					p.created_by,
					p.created_date,
					p.modified_by,
					p.modified_date

			from	dbo.properties_propertyCollections p

			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					and (

							-- Filter on active data only
							p.is_active = 1
							and ( p.relationship_end is null or p.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and p.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					and p.property_collection_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.property_collection_uid#">
					and p.property_uid in (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#">)

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to persist a new property collection record to the application database.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" hint="Describes the VO containing the property collection record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Get an instance of the security service --->
		<cfset local.securityService = request.beanFactory.getBean("SecurityService")>

		<!--- Persist the property data to the application database --->
		<cfset this.dataMgr.insertRecord("PropertyCollections", arguments.propertyCollection)>

		<!--- --->

	</cffunction>

	<cffunction name="saveExistingPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing property collection record to the application database.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" hint="Describes the VO containing the property collection record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Persist the property data to the application database --->
		<cfset this.dataMgr.updateRecord("PropertyCollections", arguments.propertyCollection)>

	</cffunction>

	<cffunction name="associateUserToPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to save an association between a user and a property collection.">
		<cfargument name="associationObject" type="struct" required="true" hint="Describes the object / structure defining the association between a property collection and a given user." />

		<!--- Persist the association data to the application database --->
		<cfset this.dataMgr.insertRecord("PropertyCollections_Users", arguments.associationObject)>

	</cffunction>

	<cffunction name="disAssociateUserFromPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to remove an association between a user and a property collection.">
		<cfargument name="associationObject" type="struct" required="true" hint="Describes the object / structure defining the association between a property collection and a given user." />

		<!--- Persist the association data to the application database --->
		<cfset this.dataMgr.updateRecord("PropertyCollections_Users", arguments.associationObject)>

	</cffunction>

	<cffunction name="associatePropertyToPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to save an association between a property and a property collection.">
		<cfargument name="associationObject" type="struct" required="true" hint="Describes the object / structure defining the association between a property collection and a given property." />

		<!--- Persist the association data to the application database --->
		<cfset this.dataMgr.insertRecord("Properties_PropertyCollections", arguments.associationObject)>

	</cffunction>

	<cffunction name="disAssociatePropertyFromPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to remove an existing association between a property and a property collection.">
		<cfargument name="associationObject" type="struct" required="true" hint="Describes the object / structure defining the association between a property collection and a given property." />

		<!--- Persist the association data to the application database --->
		<cfset this.dataMgr.updateRecord("Properties_PropertyCollections", arguments.associationObject)>

	</cffunction>

</cfcomponent>