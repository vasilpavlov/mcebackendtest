<cfcomponent displayname="ClientServiceTypeDelegate"
				hint="This CFC manages all data access interactions for the ClientServiceTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyClientServiceTypeComponent"
				hint="This method is used to retrieve a Client Service Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.ClientServiceType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.ClientServiceType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getClientServiceTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Client Service Type objects for all the Client Service Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="service_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given client service type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Client Service Types and build out the array of components --->
		<cfset local.qResult = getClientServiceType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.ClientServiceType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientServiceTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Client Service Type value object representing a single Client Service Type record."
				output="false"
				returnType="mce.e2.vo.ClientServiceType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="service_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given client service type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Client Service Type and build out the component --->
		<cfset local.qResult = getClientServiceType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.ClientServiceType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientServiceType"
				hint="This method is used to retrieve single / multiple records from the ClientServiceTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="service_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given client service type."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given client service type."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Service Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the ClientServiceTypes table matching the where clause
			select	tbl.service_type_uid,
					tbl.friendly_name,
					tbl.oldid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.ClientServiceTypes tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'service_type_uid')>
					and tbl.service_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.service_type_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>
			order by tbl.friendly_name
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Client Service Type data. --->	
	<cffunction name="saveNewClientServiceType"
				hint="This method is used to persist a new Client Service Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientServiceType" required="true" type="mce.e2.vo.ClientServiceType" hint="Describes the VO containing the Client Service Type record that will be persisted."/>

		<!--- Persist the Client Service Type data to the application database --->
		<cfset this.dataMgr.insertRecord("ClientServiceTypes", arguments.ClientServiceType)/>

	</cffunction>

	<cffunction name="saveExistingClientServiceType"
				hint="This method is used to persist an existing Client Service Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientServiceType" required="true" type="mce.e2.vo.ClientServiceType" hint="Describes the VO containing the Client Service Type record that will be persisted."/>

		<!--- Persist the Client Service Type data to the application database --->
		<cfset this.dataMgr.updateRecord("ClientServiceTypes", arguments.ClientServiceType)/>

	</cffunction>

</cfcomponent>