
<!---

  Template Name:  UserReportDelegate
     Base Table:  UserReports

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Tuesday, June 02, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Tuesday, June 02, 2009 - Template Created.

--->

<cfcomponent displayname="UserReportDelegate"
				hint="This CFC manages all data access interactions for the UserReports table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyUserReportComponent"
				hint="This method is used to retrieve a User Report object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.UserReport"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.UserReport")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getUserReportsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of User Report objects for all the User Reports in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="report_uid" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all User Reports and build out the array of components --->
		<cfset local.qResult = getUserReport(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.UserReport")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUserReportAsComponent"
				hint="This method is used to retrieve a single instance of a / an User Report value object representing a single User Report record."
				output="false"
				returnType="mce.e2.vo.UserReport"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="report_uid" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single User Report and build out the component --->
		<cfset local.qResult = getUserReport(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.UserReport")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUserReport"
				hint="This method is used to retrieve single / multiple records from the UserReports table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="report_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user report."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given report."/>
		<cfargument name="group_name" required="false" type="string" hint="Describes the name of the group a given report is associated to."/>
		<cfargument name="report_lookup_code" required="false" type="string" hint="Describes the internal lookup code for a given report."/>
		<cfargument name="report_generator" required="false" type="string" hint="Describes the type of generator used to render a given report."/>
		<cfargument name="report_name" required="false" type="string" hint="Describes the internal name for a given report."/>
		<cfargument name="report_parameter_ui_class" required="false" type="string" hint="Describes the permission level assigned to a given report."/>
		<cfargument name="permission_needed" required="false" type="string" hint="Describes the permission level assigned to a given report."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple User Report records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the UserReports table matching the where clause
			select	tbl.report_uid,
					tbl.friendly_name,
					tbl.group_name,
					tbl.report_lookup_code,
					tbl.report_generator,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					tbl.report_name,
					tbl.report_parameter_ui_class,
					tbl.permission_needed,
					tbl.image_filename,
					tbl.report_description,
					--temp for sorting of categories
					(CASE WHEN tbl.group_name = 'Account' THEN 0 ELSE CASE WHEN tbl.group_name = 'Property' THEN 1 ELSE 2 END END) as sort_order

			from	dbo.UserReports tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'report_uid')>
					and tbl.report_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.report_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'group_name')>
					and tbl.group_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.group_name#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'report_lookup_code')>
					and tbl.report_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.report_lookup_code#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'report_generator')>
					and tbl.report_generator in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.report_generator#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'report_name')>
					and tbl.report_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.report_name#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'report_parameter_ui_class')>
					and tbl.report_parameter_ui_class in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.report_parameter_ui_class#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'permission_needed')>
					and tbl.permission_needed in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.permission_needed#" null="false" list="true"> )
					</cfif>
					
					and tbl.is_active = 1

			order
			by		sort_order,
					tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Describes the methods used to persist / modify User Report data. --->
	<cffunction name="saveNewUserReport"
				hint="This method is used to persist a new User Report record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="UserReport" required="true" type="mce.e2.vo.UserReport" hint="Describes the VO containing the User Report record that will be persisted."/>

		<!--- Persist the User Report data to the application database --->
		<cfset this.dataMgr.insertRecord("UserReports", arguments.UserReport)/>

	</cffunction>

	<cffunction name="saveExistingUserReport"
				hint="This method is used to persist an existing User Report record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="UserReport" required="true" type="mce.e2.vo.UserReport" hint="Describes the VO containing the User Report record that will be persisted."/>

		<!--- Persist the User Report data to the application database --->
		<cfset this.dataMgr.updateRecord("UserReports", arguments.UserReport)/>

	</cffunction>

</cfcomponent>
