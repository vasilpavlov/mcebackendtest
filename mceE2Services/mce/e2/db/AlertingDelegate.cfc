<cfcomponent extends="mce.e2.db.BaseDatabaseDelegate" output="false">



	<!---
		*****************************************************
		******** RELATED TO RETRIEVING ALERT INSTANCES ******
		*****************************************************
	--->

	<cffunction name="getCurrentAlertsForUser" returntype="query" output="false">
		<cfargument name="user_uid" type="string" required="true">
		<cfargument name="ui_category" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">
		<cfargument name="honorAlertVisibilityDate" type="boolean" required="false" default="true">
		<cfargument name="service_type_uid" type="string" required="false">
		<cfargument name="date_from" type="date" required="false">
		<cfargument name="date_thru" type="date" required="false">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				<!--- alerts --->
				a.alert_uid,
				a.alert_subscription_uid,
				a.alert_visibility_date,
				a.alert_content,
				a.alert_body,
				a.is_active,
				a.alert_location_notes,
				a.alert_context_notes,
				a.alert_subject_notes,
				a.energy_account_uid,
				<!---  alert subscriptions --->
				subs.friendly_name as alertSubscriptionFriendlyName,
				<!--- alert types --->
				types.alert_type_code,
				types.friendly_name AS alertTypeFriendlyName,
				types.ui_priority,
				types.ui_category,
				<!--- alert statuses --->
				statuses.alert_status_code,
				statuses.friendly_name AS alertStatusFriendlyName,
				<!--- associated property --->
				a.property_uid,
				(SELECT friendly_name FROM Properties (nolock) WHERE property_uid = a.property_uid) as propertyFriendlyName,
				reference_key_uid
			FROM
				Alerts a (nolock) JOIN
				AlertSubscriptions subs (nolock) on a.alert_subscription_uid = subs.alert_subscription_uid JOIN
				AlertsUsers au (nolock) ON a.alert_uid = au.alert_uid JOIN
				AlertTypes types (nolock) ON a.alert_type_code = types.alert_type_code JOIN
				AlertStatuses statuses (nolock) ON a.alert_status_code = statuses.alert_status_code

				<!--- If we are filtering by service type, we'll be depending on energy_account at alert level --->
				<!--- Note that this is an inner join, which means that alerts with a NULL energy_account_uid (many) will not be included --->
				<!--- This is fine for current use case (filtering the list of to-do "tasks" in the activity center) but may want an outer join if this argument is used for other cases --->
				<cfif structKeyExists(arguments, "service_type_uid") and len(arguments.service_type_uid)>
					JOIN EnergyAccounts ea (nolock) ON ea.energy_account_uid = a.energy_account_uid
				</cfif>
			WHERE
					au.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#">
				AND types.ui_category = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ui_category#">
				<!--- Simple "active only" or "all" filter --->
				<cfif arguments.is_active_flag>
					AND a.is_active = 1
				</cfif>
				<!--- Allow filter by whether the account (if any) is involved with property/client that gets a particular MCE service --->
				<cfif structKeyExists(arguments, "service_type_uid") and len(arguments.service_type_uid)>
					AND EXISTS (
						SELECT * FROM dbo.getClientServicesForProperty(ea.property_uid, default)
						WHERE service_type_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.service_type_uid#" list="true">)
					)
				</cfif>
				<!--- Allow filter by date --->
				<cfif structKeyExists(arguments, "date_from")>
					AND a.alert_generated_date >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.date_from#">
				</cfif>
				<!--- Allow filter by date --->
				<cfif structKeyExists(arguments, "date_thru")>
					AND a.alert_generated_date <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.date_thru#">
				</cfif>
				<!--- Generally speaking we should not show any alerts, regardless of the user's desires, until that alert's "visibility date" --->
				<cfif honorAlertVisibilityDate eq true>
					AND a.alert_visibility_date <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
				</cfif>

			ORDER BY
				a.alert_generated_date DESC
		</cfquery>

		<cfreturn result>
	</cffunction>

	<cffunction name="getCurrentAlertCategoryCountsForUser" returntype="query" access="public" output="false">
		<cfargument name="user_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				DISTINCT types.ui_category,
				(SELECT COUNT(a.alert_uid)
				 FROM
					 Alerts a (nolock) JOIN AlertsUsers au (nolock) ON au.alert_uid = a.alert_uid JOIN
					 AlertTypes at (nolock) ON a.alert_type_code = at.alert_type_code
				 WHERE
					at.ui_category = types.ui_category
					AND au.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#">
					AND a.alert_visibility_date <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
					AND a.is_active = 1) as alertCount
			FROM
				AlertTypes types (nolock)
			ORDER BY
				types.ui_category
		</cfquery>

		<cfreturn qResult>
	</cffunction>


	<cffunction name="getAlertMeta" returntype="query" access="public" output="false">
		<cfargument name="alert_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				meta_name, meta_value
			FROM
				AlertsMeta
			WHERE
				alert_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#">

			UNION
			SELECT
				'account_notes' as meta_name, notes_text as meta_value
			FROM
				Alerts JOIN
				EnergyAccounts ea on Alerts.energy_account_uid = ea.energy_account_uid
			WHERE
				Alerts.alert_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#">

			ORDER BY
				meta_name		</cfquery>

		<cfreturn qResult>
	</cffunction>


	<cffunction name="setAlertIsActive" returntype="boolean" output="false">
		<cfargument name="user_uid" type="string" required="true">
		<cfargument name="alert_uid" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">
		<cfargument name="alert_status_code" type="string" required="true">

		<cfset var sqlResult = "">

		<cfquery result="sqlResult" datasource="#this.datasource#">
			UPDATE Alerts SET
				<cfif alert_status_code neq "">
				alert_status_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_status_code#">,
				</cfif>
				is_active = <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active_flag#">
			WHERE
				alert_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#">
				AND is_active <> <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active_flag#">
				AND alert_uid IN
					(SELECT alert_uid FROM AlertsUsers where user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#">)
		</cfquery>

		<cfreturn sqlResult.recordCount gt 0>
	</cffunction>

	<cffunction name="snoozeTaskAlert" returntype="boolean" output="false">
		<cfargument name="alert_uid" type="string" required="true">


		<cfset var sqlResult = "">

		<cfquery result="sqlResult" datasource="#this.datasource#">
			UPDATE Alerts SET
				alert_visibility_date = DATEADD(HOUR,12,GETDATE())
			WHERE
				alert_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#">
		</cfquery>

		<cfreturn sqlResult.recordCount gt 0>
	</cffunction>



	<!---
		*****************************************************
		******** RELATED TO RETRIEVING SUBSCRIPTIONS ********
		*****************************************************
	--->

	<cffunction name="getAlertSubscriptionsForProperty" returntype="array" output="false">
		<cfargument name="property_uid_list" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">

		<cfset var qSubscriptions = "">
		<cfset var alert_subscription_uid_list = "">

		<!--- Firstly, we just want a list of subscription uids --->
		<cfquery name="qSubscriptions" datasource="#this.datasource#">
			SELECT
				subs.alert_subscription_uid
			FROM
				AlertSubscriptions subs
			WHERE
				subs.property_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid_list#" list="true">)
				<cfif is_active_flag eq true>
					AND subs.is_active = 1
				</cfif>
		</cfquery>

		<!--- Shortcut if nothing was retrieved --->
		<cfif qSubscriptions.recordCount eq 0>
			<cfreturn ArrayNew(1)>
		</cfif>

		<!--- Put the subscription uids into a list of strings --->
		<cfset alert_subscription_uid_list = ValueList(qSubscriptions.alert_subscription_uid)>

		<!--- Pass to function that gets the corresponding "full" value objects --->
		<cfreturn getAlertSubscriptionsAsArrayOfComponents(alert_subscription_uid_list)>
	</cffunction>


	<cffunction name="getAlertSubscriptionsForAccounts" returntype="array" output="false">
		<cfargument name="energy_account_uid_list" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">

		<cfset var qSubscriptions = "">
		<cfset var alert_subscription_uid_list = "">

		<!--- Firstly, we just want a list of subscription uids --->
		<cfquery name="qSubscriptions" datasource="#this.datasource#">
			SELECT DISTINCT
				subs.alert_subscription_uid
			FROM
				AlertSubscriptions subs JOIN
				AlertSubscriptions_EnergyAccounts sea ON subs.alert_subscription_uid = sea.alert_subscription_uid
			WHERE
				sea.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid_list#" list="true">)
				<cfif is_active_flag eq true>
					AND subs.is_active = 1
				</cfif>
		</cfquery>

		<!--- Shortcut if nothing was retrieved --->
		<cfif qSubscriptions.recordCount eq 0>
			<cfreturn ArrayNew(1)>
		</cfif>

		<!--- Put the subscription uids into a list of strings --->
		<cfset alert_subscription_uid_list = ValueList(qSubscriptions.alert_subscription_uid)>

		<!--- Pass to function that gets the corresponding "full" value objects --->
		<cfreturn getAlertSubscriptionsAsArrayOfComponents(alert_subscription_uid_list)>
	</cffunction>


	<cffunction name="getAlertSubscriptionsForCompany" returntype="array" output="false">
		<cfargument name="client_company_uid" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">

		<cfset var qSubscriptions = "">
		<cfset var alert_subscription_uid_list = "">

		<!--- Firstly, we just want a list of subscription uids --->
		<cfquery name="qSubscriptions" datasource="#this.datasource#">
			SELECT
				subs.alert_subscription_uid
			FROM
				AlertSubscriptions subs
			WHERE
				client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#">
				<cfif is_active_flag eq true>
					AND subs.is_active = 1
				</cfif>
		</cfquery>

		<!--- Shortcut if nothing was retrieved --->
		<cfif qSubscriptions.recordCount eq 0>
			<cfreturn ArrayNew(1)>
		</cfif>

		<!--- Put the subscription uids into a list of strings --->
		<cfset alert_subscription_uid_list = ValueList(qSubscriptions.alert_subscription_uid)>

		<!--- Pass to function that gets the corresponding "full" value objects --->
		<cfreturn getAlertSubscriptionsAsArrayOfComponents(alert_subscription_uid_list)>
	</cffunction>


	<!--- Given a subcription uid (or uids), returns an array of subscription VOs with associated records --->
	<cffunction name="getAlertSubscriptionsAsArrayOfComponents" returntype="array" output="false">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<!--- Get the subscription-level data (one record per subscription) --->
		<cfset var qSubs = getAlertSubscriptions(alert_subscription_uid)>
		<!--- Convert to value objects --->
		<cfset var result = queryToVoArray(qSubs, "mce.e2.vo.AlertSubscription")/>

		<!--- Add associated data (multiple records per subscription) --->
		<cfset addAssociatedRecords(result)>

		<cfreturn result>
	</cffunction>


	<!--- Given an array of subscription VO's, adds associated data (conditions, users, delivery methods, energy accounts) --->
	<cffunction name="addAssociatedRecords" returntype="void" access="private">
		<cfargument name="subscriptions" type="array" required="true">

		<cfset var subscription = "">

		<!--- For each subscription VO, add associated information --->
		<cfloop array="#subscriptions#" index="subscription">
			<cfset subscription.conditions = queryToObjectArray(getSubscriptionConditions(subscription.alert_subscription_uid))>
			<cfset subscription.deliveryMethods = queryToObjectArray(getSubscriptionDeliveryMethods(subscription.alert_subscription_uid))>
			<cfset subscription.accounts = queryToObjectArray(getSubscriptionEnergyAccounts(subscription.alert_subscription_uid))>
			<cfset subscription.users = queryToObjectArray(getSubscriptionUsers(subscription.alert_subscription_uid))>
			<cfif ArrayLen(subscription.users) eq 1>
				<cfset subscription.recipientSummaryString = #subscription.users[1].first_name# & " " & #subscription.users[1].last_name#>
			<cfelse>
				<cfset subscription.recipientSummaryString = "#ArrayLen(subscription.users)# Recipients">
			</cfif>

			<cfif ArrayLen(subscription.deliveryMethods) eq 1>
				<cfset subscription.deliveryMethodSummaryString = subscription.deliveryMethods[1].friendly_name>
			<cfelse>
				<cfset subscription.deliveryMethodSummaryString = "#ArrayLen(subscription.deliveryMethods)# Types">
			</cfif>
			<cfif ArrayLen(subscription.accounts) eq 1>
				<cfset subscription.energyAccountSummaryString = subscription.accounts[1].friendly_name>
			<cfelse>
				<cfset subscription.energyAccountSummaryString = "#ArrayLen(subscription.accounts)# Accounts">
			</cfif>
			<cfset subscription.propertySummaryString = getSubscriptionPropertySummaryString(subscription.alert_subscription_uid)>
			<cfset subscription.energyTypeSummaryString = getEnergyTypeSummaryString(subscription.alert_subscription_uid)>
		</cfloop>
	</cffunction>

	<cffunction name="getEnergyTypeSummaryString" returntype="string" access="public">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT DISTINCT
				et.friendly_name
			FROM
				AlertSubscriptions_EnergyAccounts sea JOIN
				EnergyAccounts ea ON sea.energy_account_uid = ea.energy_account_uid JOIN
				EnergyTypes et on ea.energy_type_uid = et.energy_type_uid
			WHERE
				sea.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
				AND ea.is_active = 1
		</cfquery>

		<cfif qResult.RecordCount eq 1>
			<cfreturn qResult.friendly_name>
		<cfelse>
			<cfreturn "#qResult.RecordCount# Types">
		</cfif>
	</cffunction>


	<cffunction name="getSubscriptionPropertySummaryString" returntype="string" access="public">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT DISTINCT
				p.friendly_name
			FROM
				AlertSubscriptions_EnergyAccounts sea JOIN
				EnergyAccounts ea ON sea.energy_account_uid = ea.energy_account_uid JOIN
				Properties p on ea.property_uid = p.property_uid
			WHERE
				sea.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
				AND ea.is_active = 1
		</cfquery>

		<cfif qResult.RecordCount eq 1>
			<cfreturn qResult.friendly_name>
		<cfelse>
			<cfreturn "#qResult.RecordCount# Properties">
		</cfif>
	</cffunction>

	<cffunction name="getAlertSubscriptions" returntype="query" output="false">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfif alert_subscription_uid eq "">
			<cfthrow
				message="No subscription UIDs to process"
				detail="This function requires at least one alert_subscription_uid to proceed.">
		</cfif>

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				subs.*,
				types.friendly_name as alertTypeFriendlyName,
				dig.friendly_name as alertDigestFriendlyName,
				profiles.friendly_name as emissionsProfileFriendlyName,
				p.friendly_name as propertyFriendlyName
			FROM
				AlertSubscriptions subs JOIN
				AlertTypes types ON types.alert_type_code = subs.alert_type_code JOIN
				AlertDigests dig ON dig.alert_digest_code = subs.alert_digest_code JOIN
				EmissionsProfiles profiles ON profiles.profile_lookup_code = subs.profile_lookup_code JOIN
				Properties p ON p.property_uid = subs.property_uid
			WHERE
				p.is_active = 1 and
				subs.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
			ORDER BY
				p.friendly_name,
				types.friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>


	<cffunction name="getSubscriptionConditions" returntype="query" access="public">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				cond.alert_subscription_uid,
				cond.alert_condition_code,
				cond.numeric_value,
				types.friendly_name,
				types.default_value
			FROM
				AlertSubscriptions_Conditions cond JOIN
				AlertSubscriptions subs ON subs.alert_subscription_uid = cond.alert_subscription_uid JOIN
				AlertConditionTypes types ON (cond.alert_condition_code = types.alert_condition_code AND types.alert_type_code = subs.alert_type_code)
			WHERE
				cond.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
			ORDER BY
				types.friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<cffunction name="getSubscriptionDeliveryMethods" returntype="query" access="public">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				meths.alert_subscription_uid,
				types.alert_delivery_method_code,
				types.friendly_name
			FROM
				AlertSubscriptions_AlertDeliveryMethods meths JOIN
				AlertDeliveryMethods types ON meths.alert_delivery_method_code = types.alert_delivery_method_code
			WHERE
				meths.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
			ORDER BY
				types.friendly_name
			</cfquery>

		<cfreturn qResult>
	</cffunction>

	<cffunction name="getSubscriptionEnergyAccounts" returntype="query" access="public">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				sea.alert_subscription_uid,
				sea.energy_account_uid,
				ea.friendly_name,
				et.friendly_name AS resourceFriendlyName,
                p.friendly_name AS propertyFriendlyName,
				p.property_uid
			FROM
				AlertSubscriptions_EnergyAccounts sea JOIN
				EnergyAccounts ea ON sea.energy_account_uid = ea.energy_account_uid INNER JOIN
				EnergyTypes et ON ea.energy_type_uid = et.energy_type_uid INNER JOIN
				Properties p ON ea.property_uid = p.property_uid
			WHERE
				sea.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
				AND ea.is_active = 1
			ORDER BY
				ea.friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>


	<cffunction name="getSubscriptionUsers" returntype="query" access="public">
		<cfargument name="alert_subscription_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				subu.alert_subscription_uid,
				subu.user_uid,
				u.username,
				u.first_name,
				u.last_name
			FROM
				AlertSubscriptions_Users subu JOIN
				Users u ON u.user_uid = subu.user_uid
			WHERE
				subu.alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" list="true">)
				AND u.is_active = 1
			ORDER BY
				u.username
		</cfquery>

		<cfreturn qResult>
	</cffunction>



	<!---
		*****************************************************
		******** RELATED TO PERSISTING SUBSCRIPTIONS ********
		*****************************************************
	--->

	<cffunction name="saveExistingAlertSubscription" access="public">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cftransaction>
			<cfset this.dataMgr.updateRecord("AlertSubscriptions", arguments.subscription)>
			<cfset saveAssociatedRecords(subscription)>
		</cftransaction>
		<!--- Get the subscription-level data (one record per subscription) --->
		<cfset qSubs = getAlertSubscriptions(subscription.alert_subscription_uid)>
		<!--- Convert to value objects --->
		<cfset result = queryToVoArray(qSubs, "mce.e2.vo.AlertSubscription")/>

		<!--- Add associated data (multiple records per subscription) --->
		<cfset addAssociatedRecords(result)>
		<!-- There's only one element in this array, the new AlertSubscription-->
		<cfreturn result[1]>

	</cffunction>

	<cffunction name="saveNewAlertSubscription" access="public">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cfset var subscription_uid = "">
		<cfset qSubs = "">
		<cfset result = "">
		<cfset subscription.alert_subscription_uid = "">

		<cftransaction>
			<cfset subscription_uid = this.dataMgr.insertRecord("AlertSubscriptions", arguments.subscription)>
			<cfset subscription.alert_subscription_uid = subscription_uid>

			<cfset setValuesForRecords(subscription.conditions, "alert_subscription_uid", subscription_uid)>
			<cfset setValuesForRecords(subscription.deliveryMethods, "alert_subscription_uid", subscription_uid)>
			<cfset setValuesForRecords(subscription.accounts, "alert_subscription_uid", subscription_uid)>
			<cfset setValuesForRecords(subscription.users, "alert_subscription_uid", subscription_uid)>

			<cfset saveAssociatedRecords(subscription)>
		</cftransaction>

		<!--- Get the subscription-level data (one record per subscription) --->
		<cfset qSubs = getAlertSubscriptions(subscription.alert_subscription_uid)>
		<!--- Convert to value objects --->
		<cfset result = queryToVoArray(qSubs, "mce.e2.vo.AlertSubscription")/>

		<!--- Add associated data (multiple records per subscription) --->
		<cfset addAssociatedRecords(result)>
		<!-- There's only one element in this array, the new AlertSubscription-->
		<cfreturn result[1]>
	</cffunction>

	<cffunction name="saveAssociatedRecords">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cfset saveAssociatedSubscriptionConditions(subscription)>
		<cfset saveAssociatedSubscriptionDeliveryMethods(subscription)>
		<cfset saveAssociatedSubscriptionEnergyAccounts(subscription)>
		<cfset saveAssociatedSubscriptionUsers(subscription)>
	</cffunction>


	<cffunction name="saveAssociatedSubscriptionConditions" returntype="void">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cfset var item = "">

		<cfquery datasource="#this.datasource#">
			DELETE FROM AlertSubscriptions_Conditions
			WHERE alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#subscription.alert_subscription_uid#">
		</cfquery>
		<cfloop array="#subscription.conditions#" index="item">
			<cfset this.DataMgr.insertRecord("AlertSubscriptions_Conditions", item)>
		</cfloop>
	</cffunction>


	<cffunction name="saveAssociatedSubscriptionUsers" returntype="void">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cfset var item = "">

		<cfquery datasource="#this.datasource#">
			DELETE FROM AlertSubscriptions_Users
			WHERE alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#subscription.alert_subscription_uid#">
		</cfquery>
		<cfloop array="#subscription.users#" index="item">
			<cfset this.DataMgr.insertRecord("AlertSubscriptions_Users", item)>
		</cfloop>
	</cffunction>

	<!--- New function from Nate as of 6/16/2010--->
	<cffunction name="saveAssociatedSubscriptionEnergyAccounts" returntype="void">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">
	
		<cfset var accountUids = buildVoPropertyListFromArray(subscription.accounts, "energy_account_uid")>
	
		<!--- Delete any records that were associated with this subscription but are no longer in the list of desired accounts --->
		<cfquery datasource="#this.datasource#">
			DELETE FROM AlertSubscriptions_EnergyAccounts
			WHERE alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#subscription.alert_subscription_uid#">
			AND energy_account_uid NOT IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#accountUids#" list="true">)
		</cfquery>
	
		<!--- Insert records for accounts that are now associated with this subscription but were not previously --->
		<cfquery datasource="#this.datasource#">
			INSERT INTO AlertSubscriptions_EnergyAccounts (
				alert_subscription_uid,
				energy_account_uid)
			SELECT
				<cfqueryparam cfsqltype="cf_sql_idstamp" value="#subscription.alert_subscription_uid#">,
				ea.energy_account_uid
			FROM
				EnergyAccounts ea
			WHERE
				ea.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#accountUids#" list="true">)
			AND ea.energy_account_uid NOT IN (
				SELECT energy_account_uid
				FROM AlertSubscriptions_EnergyAccounts
				WHERE alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#subscription.alert_subscription_uid#">
			)
		</cfquery>
	</cffunction>

	<cffunction name="saveAssociatedSubscriptionDeliveryMethods" returntype="void">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cfset var item = "">

		<cfquery datasource="#this.datasource#">
			DELETE FROM AlertSubscriptions_AlertDeliveryMethods
			WHERE alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#subscription.alert_subscription_uid#">
		</cfquery>
		<cfloop array="#subscription.deliveryMethods#" index="item">
			<cfset this.DataMgr.insertRecord("AlertSubscriptions_AlertDeliveryMethods", item)>
		</cfloop>
	</cffunction>


	<cffunction name="saveAlertSubscriptionAssociationsForAccount" returntype="boolean">
		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="subscriptionsToAssociate" type="string" required="true" hint="List of subscription UIDs">
		<cfargument name="subscriptionsToNotAssociate" type="string" required="true" hint="List of subscription UIDs">

		<cfif ListLen(subscriptionsToNotAssociate) gt 0>
			<cfquery datasource="#this.datasource#">
				DELETE FROM AlertSubscriptions_EnergyAccounts
				WHERE energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#">
				AND alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscriptionsToNotAssociate#" list="true">)
			</cfquery>
		</cfif>

		<cfif ListLen(subscriptionsToAssociate) gt 0>
			<cfquery datasource="#this.datasource#">
				DELETE FROM AlertSubscriptions_EnergyAccounts
				WHERE energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#">
				AND alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscriptionsToAssociate#" list="true">)
			</cfquery>
			<cfquery datasource="#this.datasource#">
				INSERT INTO AlertSubscriptions_EnergyAccounts(energy_account_uid, alert_subscription_uid)
				SELECT
					<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#">,
					alert_subscription_uid
				FROM AlertSubscriptions 
				WHERE alert_subscription_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.subscriptionsToAssociate#" list="true">)
			</cfquery>
		</cfif>

		<cfreturn true>
	</cffunction>


	<cffunction name="getAlertTypesAsArrayOfComponents" access="public" returntype="array">
		<cfset var qTypes = getAlertTypes()>
		<cfset var result = queryToVoArray(qTypes, "mce.e2.vo.AlertType")/>
		<cfset var item = "">

		<cfloop array="#result#" index="item">
			<cfset item.conditionsTypesList = getConditionsTypeListForAlertType(item)>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="getAlertTypes" access="public" returntype="query">
		<cfset var qResult = "">

		<cfquery datasource="#this.datasource#" name="qResult">
			SELECT * FROM AlertTypes
			WHERE is_active = 1
			ORDER BY friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>


	<cffunction name="getConditionsTypeListForAlertType" access="private" returntype="query">
		<cfargument name="alertType" type="mce.e2.vo.AlertType" required="true">
		<cfset var qResult = "">

		<cfquery datasource="#this.datasource#" name="qResult">
			SELECT alert_type_code, alert_condition_code, friendly_name, default_value
			FROM AlertConditionTypes
			WHERE alert_type_code = '#alertType.alert_type_code#'
			ORDER BY friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>


	<cffunction name="getAlertDeliveryMethods" access="public" returntype="query">
		<cfset var qResult = "">

		<cfquery datasource="#this.datasource#" name="qResult">
			select alert_delivery_method_code, friendly_name
			from AlertDeliveryMethods
			order by friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<cffunction name="getAlertDigests" access="public" returntype="query">
		<cfset var qResult = "">

		<cfquery datasource="#this.datasource#" name="qResult">
			select alert_digest_code, friendly_name
			from AlertDigests
			order by friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>

</cfcomponent>