<cfcomponent 	extends="BaseDatabaseDelegate"
				output="false"
				hint="Provides access to emissions-related data, used by the emissions visualization portion of E2Track.">


	<cffunction name="getEmissionsAnalysis"
				access="remote"
				returntype="query"
				hint="This method is used to return the emission analysis for a given collection of properties.">

		<!--- Describe the arguments for this method --->
		<cfargument name="property_uid_list" type="string" required="true" hint="The property/properties that you want emissions analysis records for">
		<cfargument name="period_start" type="date" required="true" hint="The start date of the time period that you want emissions records for">
		<cfargument name="period_end" type="date" required="true" hint="The end date of the time period that you want emissions records for">
		<cfargument name="dateRollupMode" type="string" required="true" hint="At what level you want rows to be returned in terms of date. Currently implemented: 'month-actual', 'month-prorated', 'total-actual' and 'total-prorated'">
		<cfargument name="energy_account_uid_list" type="string" required="false" hint="Optional; the specific energy account(s) that you want emissions analysis records for. Omit or provide an empty string for all energy accounts associated with the given property/properties.">
		<cfargument name="profile_lookup_code" type="string" required="false" default="default" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="grouping" type="string" required="false" default="period" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="suppressRecordsWithZeroEmissions" type="boolean" required="false" default="false" hint="If true, records with a calculated emissions value of zero are omitted from the result set">
		<cfargument name="property_collection_uid_list" type="string" required="false" default="" hint="Does not filter which rows are returned; instead, 'tags' each row with one of the property collections in this list, whichever is the first (alphabetically) for the given energy account in each row. ">
		<cfargument name="performConversionsAsNeeded" type="boolean" required="false" default="true">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset var result = "">

		<!--- The rollup mode determines certain characteristics of how the data is returned --->
		<cfswitch expression="#dateRollupMode#">
			<cfcase value="month-prorated">
				<cfset local.monthly = true>	<!--- Return one row for each month per property per account --->
				<cfset local.prorate = true>	<!--- Pro-rate the values for partial months --->
			</cfcase>
			<cfcase value="month-actual">
				<cfset local.monthly = true>	<!--- Return one row for each month per property per account --->
				<cfset local.prorate = false>	<!--- Do not pro-rate values for partial months --->
			</cfcase>
			<cfcase value="total-prorated">
				<cfset local.monthly = false>	<!--- Return one row per property per account --->
				<cfset local.prorate = true>	<!--- Pro-rate the values for partial months --->
			</cfcase>
			<cfcase value="total-actual">
				<cfset local.monthly = false>	<!--- Return one row per property per account --->
				<cfset local.prorate = false>	<!--- Do not pro-rate values for partial months --->
			</cfcase>
			<cfdefaultcase>
				<cfthrow
					message="Unsupported 'dateRollupMode' argument"
					detail="The dateRollupMode argument provided ('#dateRollupMode#') is not supported.">
			</cfdefaultcase>
		</cfswitch>

		<cfswitch expression="#arguments.grouping#">
			<cfcase value="energyAccount">
				<cfset local.includePropertyColumns = true>
				<cfset local.includeEnergyTypeColumns = true>
				<cfset local.includeEnergyAccountColumns = true>
			</cfcase>
			<cfcase value="property">
				<cfset local.includePropertyColumns = true>
				<cfset local.includeEnergyTypeColumns = false>
				<cfset local.includeEnergyAccountColumns = false>
			</cfcase>
			<cfcase value="period">
				<cfset local.includePropertyColumns = false>
				<cfset local.includeEnergyTypeColumns = false>
				<cfset local.includeEnergyAccountColumns = false>
			</cfcase>
			<cfdefaultcase>
				<cfthrow
					message="Unsupported 'grouping' argument"
					detail="The grouping argument provided ('#arguments.grouping#') is not supported..">
			</cfdefaultcase>>
		</cfswitch>



		<cfquery name="result" datasource="#this.datasource#" cachedwithin="#CreateTimeSpan(0,0,1,0)#">
			SELECT
				eu.*,
				p.property_uid,
				p.friendly_name,
				(select top 1
					pc.friendly_name
				 from
				 	dbo.PropertyCollections pc JOIN
				 	dbo.Properties_PropertyCollections ppc ON pc.property_collection_uid = ppc.property_collection_uid
				 WHERE
				 	ppc.property_uid = p.property_uid
				 	AND ppc.property_collection_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#property_collection_uid_list#" list="true">)
				 ORDER BY pc.friendly_name) as [group],
				es.friendly_name as emissionsSourceFriendlyName,
				ea.friendly_name AS energyAccountFriendlyName,
				ep.friendly_name AS energyProviderFriendlyName,
				units.btu_conversion_factor,
				units.friendly_name AS energyUnitFriendlyName,
				types.friendly_name AS energyTypeFriendlyName,
				types.energy_type_uid
			INTO
				##selectedUsage
			FROM
				dbo.EnergyUsage eu JOIN
				dbo.EnergyAccounts ea ON eu.energy_account_uid = ea.energy_account_uid JOIN
				dbo.EnergyNativeAccounts ena ON ena.energy_account_uid = ea.energy_account_uid JOIN
				dbo.EnergyProviders ep ON ena.energy_provider_uid = ep.energy_provider_uid JOIN
				dbo.EnergyUnits units ON units.energy_unit_uid = eu.energy_unit_uid JOIN
				dbo.EnergyTypes types ON types.energy_type_uid = ea.energy_type_uid JOIN
				dbo.EnergyUsageStatuses statuses ON statuses.usage_status_code = eu.usage_status_code JOIN
				dbo.Properties p ON p.property_uid = ea.property_uid,
				dbo.EmissionsSources es
			WHERE
				ea.master_energy_account_uid is null
				AND
				<!--- Filter on energy account uid --->
				p.property_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#property_uid_list#" list="true">)
				AND es.emissions_source_uid = dbo.getEmissionsSourceForEnergyAccount(ea.energy_account_uid, '#profile_lookup_code#')
				<!--- Filter by date... if pro-rating then get all records that overlap with the specified date range; if not then get records that merely start during the specified date range --->
				<cfif local.prorate>
					AND (
						(eu.period_start BETWEEN <cfqueryparam cfsqltype="cf_sql_date" value="#period_start#"> AND <cfqueryparam cfsqltype="cf_sql_date" value="#period_end#">)
						OR
						(eu.period_end BETWEEN <cfqueryparam cfsqltype="cf_sql_date" value="#period_start#"> AND <cfqueryparam cfsqltype="cf_sql_date" value="#period_end#">)
					)
				<cfelse>
					AND (eu.report_date BETWEEN <cfqueryparam cfsqltype="cf_sql_date" value="#period_start#"> AND <cfqueryparam cfsqltype="cf_sql_date" value="#period_end#">)
				</cfif>
				<!--- Optional filter on energy account uid --->
				<cfif isDefined("arguments.energy_account_uid_list") and arguments.energy_account_uid_list neq "">
					AND eu.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energy_account_uid_list#" list="true">)
				</cfif>
				AND ea.is_active = 1
				AND eu.is_active = 1
				AND eu.usage_type_code = 'actual'
				AND statuses.data_is_reportable = 1


			<!--- If we are meant to perform conversions between units, etc (this would presumably always be the case) --->
			<cfif arguments.performConversionsAsNeeded>
				<!--- The first step is to put the data we queried above into a temp table --->
				<!--- Note that we have to escape the pound sign with double-pound-sign so that CF knows we're not talking about a CFML variable --->

				<!--- Next we create an instance of the EnergyUnitConversionType data type and fill it with important columns from the temp table --->
				DECLARE @input EnergyUnitConversionType
				INSERT INTO @input
				SELECT energy_usage_uid, energy_type_uid, energy_unit_uid, recorded_value
				FROM ##selectedUsage

				<!--- Finally we call the ApplyEnergyUnitConversion function and join its output against the original temp table --->
				SELECT
					##selectedUsage.*,
					converted.converted_value,
					converted.converted_energy_unit_friendly_name
				INTO ##convertedUsage
				FROM
					##selectedUsage JOIN
					dbo.ApplyEnergyUnitConversion(@input) converted
						ON (##selectedUsage.energy_usage_uid = converted.energy_usage_uid)
			</cfif>


			SELECT
				<!--- Include some basic property info --->
				<cfif local.includePropertyColumns>
					property_uid,
					friendly_name,
					[group],
				</cfif>
				<!--- Include some basic energy account info --->
				<cfif local.includeEnergyTypeColumns>
					emissionsSourceFriendlyName,
					energy_account_uid,
					energyAccountFriendlyName,
					energyProviderFriendlyName,
					energyUnitFriendlyName,
					energyTypeFriendlyName,
				</cfif>
				<!--- If we are breaking down at the monthly level, include the first day of the month in the SELECT and GROUP BY clauses --->
				<cfif local.monthly>
					dbo.getDateFirstInMonth(report_date) AS sumPeriodStart,
					dbo.getDateLastInMonth(report_date) AS sumPeriodEnd,
				<cfelse>
					<cfqueryparam cfsqltype="cf_sql_date" value="#period_start#"> as sumPeriodStart,
					<cfqueryparam cfsqltype="cf_sql_date" value="#period_end#"> as sumPeriodEnd,
				</cfif>
				<!--- The actual recorded value (consumption) --->
				SUM(recorded_value) AS recorded_value,
				SUM(recorded_value * btu_conversion_factor) AS recordedValueAsBtu,
				SUM(total_cost) AS total_cost,
				<!--- The calculated emissions --->
				CAST(SUM(
					<!--- ...multiply the recorded value... --->
					recorded_value
					<!--- ...times the emissions factor value... --->
					<!--- * dbo.getEmissionsFactorValueForEnergyAccount(eu.energy_account_uid, <cfqueryparam cfsqltype="cf_sql_varchar" value="#profile_lookup_code#">, DEFAULT) --->
					* dbo.getEmissionsFactorValueForEnergyAccount(energy_account_uid, <cfqueryparam cfsqltype="cf_sql_varchar" value="#profile_lookup_code#">, report_date, energy_unit_uid)
					<!--- ...times the pro-rating ratio if applicable... --->
					<!--- <cfif local.prorate>
					* dbo.prorateValueMonthly(eu.recorded_value, eu.period_start, eu.period_end, <cfqueryparam cfsqltype="cf_sql_date" value="#period_start#">, <cfqueryparam cfsqltype="cf_sql_date" value="#period_end#">)
					</cfif> --->
				) AS decimal(19, 6)) AS emissions
				FROM ##convertedUsage
			GROUP BY
				<cfif local.includePropertyColumns>
					property_uid,
					friendly_name,
					[group]
				</cfif>
				<cfif local.includeEnergyAccountColumns>
					<!--- comma ---> <cfif local.includePropertyColumns>,</cfif>
					energy_account_uid,
					energyAccountFriendlyName,
					emissionsSourceFriendlyName,
					energyProviderFriendlyName,
					energyTypeFriendlyName,
					energyUnitFriendlyName
				</cfif>
				<!--- If we are breaking down at the monthly level, include it in the SELECT and GROUP BY clauses --->
				<cfif local.monthly>
					<!--- comma ---> <cfif local.includePropertyColumns or local.includeEnergyAccountColumns>,</cfif>
					dbo.getDateFirstInMonth(report_date),
					dbo.getDateLastInMonth(report_date)
				</cfif>
			ORDER BY
				<cfif local.includePropertyColumns>
					friendly_name,
					property_uid
				</cfif>
				<cfif local.includeEnergyAccountColumns>
					<!--- comma ---> <cfif local.includePropertyColumns>,</cfif>
					energyAccountFriendlyname
				</cfif>
				<cfif local.monthly>
					<!--- comma ---> <cfif local.includePropertyColumns or local.includeEnergyAccountColumns>,</cfif>
					sumPeriodStart
				</cfif>

		</cfquery>


		<!--- If we have been asked to filter out the records with zero emissions --->
		<cfif arguments.suppressRecordsWithZeroEmissions>
			<cfquery name="result" dbtype="query">
				SELECT * FROM result
				WHERE emissions > 0
			</cfquery>
		</cfif>

		<!--- Return the result set --->
		<cfreturn result>

	</cffunction>







	<!--- ==================================================================== --->
	<cffunction name="getEnergyAnalysis" returntype="query">
		<cfargument name="property_uid_list" type="string" required="true" hint="The property/properties that you want emissions analysis records for">
		<cfargument name="period_start" type="date" required="true" hint="The start date of the time period that you want emissions records for">
		<cfargument name="period_end" type="date" required="true" hint="The end date of the time period that you want emissions records for">
		<cfargument name="energy_account_uid_list" type="string" required="false" hint="Optional; the specific energy account(s) that you want emissions analysis records for. Omit or provide an empty string for all energy accounts associated with the given property/properties.">
		<cfargument name="profile_lookup_code" type="string" required="false" default="default" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="grouping" type="string" required="false" default="periodMonth" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="propertyMetaTypeCodeForMetrics" type="string" required="false" default="">
		<cfargument name="energyUsageMetaTypeCodes" type="string" required="false" default="">
		<cfargument name="energyUsageTypeCodes" type="string" required="false" default="" hint="Each code can be prefixed with 'sumMax' or other special prefixes">
		<cfargument name="performConversionsAsNeeded" type="boolean" required="false" default="true">

		<cfset var local = structNew()>
		<cfset local.massagedEnergyUsageMetaCodes = "">
		<cfset local.energyUsageMetaCodesToSumMax = "">

		<cfloop list="#energyUsageMetaTypeCodes#" index="local.thisCode">
			<cfif ListLen(local.thisCode, ":") gt 1>
				<cfswitch expression="#ListFirst(local.thisCode, ':')#">
					<cfcase value="sumMax">
						<cfset local.energyUsageMetaCodesToSumMax = ListAppend(local.energyUsageMetaCodesToSumMax, ListRest(local.thisCode, ":"))>
					</cfcase>
					<cfdefaultcase>
						<cfthrow message="Unknown colon-separated prefix found on energy usage meta type code '#local.thisCode#'">
					</cfdefaultcase>>
				</cfswitch>

				<cfset local.massagedEnergyUsageMetaCodes = ListAppend(local.massagedEnergyUsageMetaCodes, ListRest(local.thisCode, ":"))>
			<cfelse>
				<cfset ListAppend(local.massagedEnergyUsageMetaCodes, local.thisCode)>
			</cfif>
		</cfloop>


		<!--- SQL fragment that we will use below, after the "CTE" portion of the query --->
		<cfsavecontent variable="local.commonColumns">
			<cfoutput>
				usage_type_code,
				usageTypeFriendlyName,
				COUNT(*) as numEnergyUsageRecords,
				CAST(SUM(converted_value) AS decimal(19,6) ) as sumRecordedValue,
				CAST(SUM(recordedValueAsBtu) AS decimal(19,6) ) as sumRecordedValueAsBtu,
				CAST(SUM(emissionsImpact) AS decimal(19,6) ) as sumEmissionsImpact,
				SUM(total_cost) as sumTotalCost,
				<!--- When there is one energy unit, return the name of that unit; otherwise indicate that it is a mix of units --->
				CASE
					WHEN COUNT(DISTINCT converted_energy_unit_friendly_name) = 1 THEN MIN(converted_energy_unit_friendly_name)
					ELSE MIN(converted_energy_unit_friendly_name)
				END AS energyUnitFriendlyName
				<!--- Metric Option (per Sq Ft, etc) --->
				<cfif propertyMetaTypeCodeForMetrics neq "">
					, CAST(SUM( recorded_value / [#propertyMetaTypeCodeForMetrics#] ) AS decimal(19,6) ) AS sumRecordedValuePerMetric
					, CAST(SUM( recordedValueAsBtu / [#propertyMetaTypeCodeForMetrics#] ) AS decimal(19,6) ) AS sumRecordedValueAsBtuPerMetric
					, CAST(SUM( emissionsImpact / [#propertyMetaTypeCodeForMetrics#] ) AS decimal(19,6) ) AS sumEmissionsImpactPerMetric
					, SUM( total_cost / [#propertyMetaTypeCodeForMetrics#] ) AS sumTotalCostPerMetric
				</cfif>
				<!--- Energy Usage Meta Data --->
				<cfloop list="#local.massagedEnergyUsageMetaCodes#" index="local.typeCode">
					, AVG( [EnergyUsageMeta_#local.typeCode#] ) AS [EnergyUsageMeta_#local.typeCode#]
				</cfloop>
			</cfoutput>
		</cfsavecontent>

		<cfquery name="local.result" datasource="#this.datasource#">
			SELECT
				energy_usage_uid,
				energy_unit_uid,
				usage_type_code,
				usageTypeFriendlyName,
				property_uid,
				propertyFriendlyName,
				energy_account_uid,
				energyAccountFriendlyName,
				energy_type_uid,
				energyTypeFriendlyName,
				total_cost,
				recorded_value,
				recordedValueAsBtu,
				<!--- Calculated Emisisons --->
<!--- 				CAST((euv.recorded_value * dbo.getEmissionsFactorValueForEnergyAccount(energy_account_uid, '#profile_lookup_code#', DEFAULT)) AS decimal(19, 6)) AS emissionsImpact, --->
				CAST((euv.recorded_value * dbo.getEmissionsFactorValueForEnergyAccount(energy_account_uid, '#profile_lookup_code#', euv.periodEndFirstInMonth,euv.energy_unit_uid)) AS decimal(19, 6)) AS emissionsImpact,
				<!--- Property Meta Value for Metrics (such as "per sqft") --->
				<cfif propertyMetaTypeCodeForMetrics neq "">
					dbo.getPropertyMetaValue(property_uid, '#propertyMetaTypeCodeForMetrics#', periodEndFirstInMonth) AS [#propertyMetaTypeCodeForMetrics#],
				</cfif>
				<!--- Energy Usage Meta Data --->
				<cfloop list="#local.massagedEnergyUsageMetaCodes#" index="local.typeCode">
					ISNULL(dbo.getEnergyUsageMetaValueById(energy_usage_uid, '#local.typeCode#'), 0) AS [EnergyUsageMeta_#local.typeCode#],
				</cfloop>
				periodEndFirstInMonth,
				energyUnitFriendlyName
			INTO
				##selectedUsage
			FROM
				EnergyUsageView euv
			WHERE
				<!--- Constrain by date --->
				(euv.period_end BETWEEN <cfqueryparam cfsqltype="cf_sql_date" value="#period_start#"> AND <cfqueryparam cfsqltype="cf_sql_date" value="#period_end#">)
				<!--- Constrain by property --->
				<cfif arguments.property_uid_list neq "">
					AND euv.property_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.property_uid_list#" list="true">)
				</cfif>
				<!--- Constrain by energy account --->
				<cfif isDefined("arguments.energy_account_uid_list")>
					AND euv.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energy_account_uid_list#" list="true" null="#yesNoFormat(arguments.energy_account_uid_list eq '')#">)
				</cfif>
				<cfif arguments.energyUsageTypeCodes neq "">
					AND euv.usage_type_code IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energyUsageTypeCodes#" list="true">)
				</cfif>
				AND euv.is_active = 1
				AND euv.data_is_reportable = 1



			<!--- If we are meant to perform conversions between units, etc (this would presumably always be the case) --->
			<cfif arguments.performConversionsAsNeeded>
				<!--- The first step is to put the data we queried above into a temp table --->
				<!--- Note that we have to escape the pound sign with double-pound-sign so that CF knows we're not talking about a CFML variable --->

				<!--- Next we create an instance of the EnergyUnitConversionType data type and fill it with important columns from the temp table --->
				DECLARE @input EnergyUnitConversionType
				INSERT INTO @input
				SELECT energy_usage_uid, energy_type_uid, energy_unit_uid, recorded_value
				FROM ##selectedUsage

				<!--- Finally we call the ApplyEnergyUnitConversion function and join its output against the original temp table --->
				SELECT
					##selectedUsage.*,
					converted.converted_value,
					converted.converted_energy_unit_friendly_name
				INTO ##convertedUsage
				FROM
					##selectedUsage JOIN
					dbo.ApplyEnergyUnitConversion(@input) converted
						ON (##selectedUsage.energy_usage_uid = converted.energy_usage_uid)
			</cfif>


			<!---
				This WITH clause defines a common table expression (CTE), which essentially gives us a temp table, scoped to this query only.
				Very powerful, esp when combined with dynamic query construction at the CF level.
				See the SQL Server 2008 documentation for details.
			--->
			; WITH RawData(
				energy_usage_uid,
				energy_unit_uid,
				usage_type_code,
				usageTypeFriendlyName,
				property_uid,
				propertyFriendlyName,
				energy_account_uid,
				energyAccountFriendlyName,
				energy_type_uid,
				energyTypeFriendlyName,
				total_cost,
				recorded_value,
				recordedValueAsBtu,
				emissionsImpact,
				<!--- Metric Option (per Sq Ft, etc) --->
				<cfif propertyMetaTypeCodeForMetrics neq "">
					[#propertyMetaTypeCodeForMetrics#],
				</cfif>
				<!--- Energy Usage Meta Data --->
				<cfloop list="#local.massagedEnergyUsageMetaCodes#" index="local.typeCode">
					[EnergyUsageMeta_#local.typeCode#],
				</cfloop>
				periodEndFirstInMonth,
				energyUnitFriendlyName,
				converted_value,
				converted_energy_unit_friendly_name
			) AS (
				SELECT * FROM
				##convertedUsage
			)


			<!---
				Now that we have the CTE defined, we can "re-query" it.
			--->
			<cfswitch expression="#arguments.grouping#">
				<cfcase value="property">
					SELECT
						property_uid,
						propertyFriendlyName,
						<cfloop list="#local.energyUsageMetaCodesToSumMax#" index="local.typeCode">
							#snippetForPerAccountSumMax(local.typeCode, "%tbl%.property_uid")#,
						</cfloop>
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						usage_type_code, usageTypeFriendlyName,
						property_uid, propertyFriendlyName
					ORDER BY
						propertyFriendlyName
				</cfcase>
				<cfcase value="energyAccount">
					SELECT
						property_uid, propertyFriendlyName,
						energy_account_uid, energyAccountFriendlyName,
						<cfloop list="#local.energyUsageMetaCodesToSumMax#" index="local.typeCode">
							#snippetForPerAccountSumMax(local.typeCode, "%tbl%.energy_account_uid")#,
						</cfloop>
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						usage_type_code, usageTypeFriendlyName,
						property_uid, propertyFriendlyName,
						energy_account_uid, energyAccountFriendlyName
					ORDER BY
						propertyFriendlyName
				</cfcase>
				<cfcase value="energyType">
					SELECT
						energy_type_uid, energyTypeFriendlyName,
						<cfloop list="#local.energyUsageMetaCodesToSumMax#" index="local.typeCode">
							#snippetForPerAccountSumMax(local.typeCode, "%tbl%.energy_type_uid")#,
						</cfloop>
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						usage_type_code, usageTypeFriendlyName,
						energy_type_uid, energyTypeFriendlyName
					ORDER BY
						energyTypeFriendlyName
				</cfcase>
				<cfcase value="periodMonth">
					SELECT
						periodEndFirstInMonth as periodEnd,
						<cfloop list="#local.energyUsageMetaCodesToSumMax#" index="local.typeCode">
							#snippetForPerAccountSumMax(local.typeCode, "%tbl%.periodEndFirstInMonth")#,
						</cfloop>
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						usage_type_code, usageTypeFriendlyName,
						periodEndFirstInMonth
					ORDER BY
						periodEndFirstInMonth
				</cfcase>
				<cfcase value="periodQuarter">
					SELECT
						dbo.getDateQuarterName(periodEndFirstInMonth) as periodEnd,
						<cfloop list="#local.energyUsageMetaCodesToSumMax#" index="local.typeCode">
							#snippetForPerAccountSumMax(local.typeCode, "dbo.getDateQuarterName(%tbl%.periodEndFirstInMonth)")#,
						</cfloop>
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						usage_type_code, usageTypeFriendlyName,
						dbo.getDateQuarterName(periodEndFirstInMonth)
					ORDER BY
						min(periodEndFirstInMonth)
				</cfcase>
				<cfcase value="periodYear">
					SELECT
						YEAR(periodEndFirstInMonth) as periodEnd,
						<cfloop list="#local.energyUsageMetaCodesToSumMax#" index="local.typeCode">
							#snippetForPerAccountSumMax(local.typeCode, "YEAR(%tbl%.periodEndFirstInMonth)")#,
						</cfloop>
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						usage_type_code, usageTypeFriendlyName,
						YEAR(periodEndFirstInMonth)
					ORDER BY
						YEAR(periodEndFirstInMonth)
				</cfcase>
				<cfdefaultcase>
					<cfthrow
						message="Unknown grouping specfified ('#arguments.grouping#')">
				</cfdefaultcase>
			</cfswitch>


		</cfquery>

		<cfreturn local.result>
	</cffunction>



	<cffunction name="snippetForPerAccountSumMax" returntype="string" access="private">
		<cfargument name="typeCode" type="string" required="true">
		<cfargument name="conditionColumns" type="string" required="true">

		<cfset var result = "">
		<cfset var conditionColumn = "">

		<cfsavecontent variable="result">
			<cfoutput>
				(SELECT
					SUM(per_account.peak_value_per_account)
					FROM (
						SELECT MAX([EnergyUsageMeta_#arguments.typeCode#]) AS peak_value_per_account
						FROM RawData rd2
						WHERE 1=1
						<cfloop list="#arguments.conditionColumns#" index="conditionColumn">
							AND #Replace(conditionColumn, "%tbl%", "rd2")# = #Replace(conditionColumn, "%tbl%", "RawData")#
						</cfloop>
						GROUP BY energy_account_uid
					) AS per_account
				) AS [EnergyUsageMeta_sumMax:#arguments.typeCode#]
			</cfoutput>
		</cfsavecontent>

		<cfreturn result>
	</cffunction>


	<cffunction name="getEmissionsSourcesHierarchyAsArrayOfComponents"
				hint="The method is used to retrieve a complete emissions source energy hierarchy from a given emissions profile as an array of components."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="profile_lookup_code" required="true" type="string" default="default" hint="Describes the energy profile lookup code used to retrieve energy hierarchy information."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all data review approval status information and build out the array of components --->
		<cfset local.qResult = getEmissionsSourcesHierarchy(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EmissionsSourcesHierarchy")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEmissionsSourcesHierarchy"
				access="remote"
				returntype="query"
				hint="Returns a query of emissions sources, with hierarchy columns">

		<!--- Define the arguments for this method --->
		<cfargument name="profile_lookup_code" type="string" required="false" default="default">

		<!--- Initialize the local scope --->
		<cfset var local = {}>

		<cfquery name="local.result" datasource="#this.datasource#">
			select
			    es.emissions_source_uid,
			    es.hierarchy_id.ToString() AS hierarchy_position,
			    es.hierarchy_id.GetAncestor(1).ToString() AS parent_position,
			    es.energy_type_uid,
			    es.friendly_name as emissionsSourceFriendlyName,
			    et.friendly_name as energyTypeFriendlyName

		   from
			    dbo.EmissionsSources es (nolock)
			    left outer join dbo.EnergyTypes et (nolock)
					on es.energy_type_uid = et.energy_type_uid
					and et.is_active = 1,
		    	dbo.EmissionsSources esroot (nolock)
		    	join dbo.EmissionsProfiles ep (nolock)
		    		on ep.root_emissions_source_uid = esroot.emissions_source_uid

		   where 1=1
				and es.is_active = 1
				and esroot.is_active = 1
				and ep.profile_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#profile_lookup_code#">
				and es.hierarchy_id.IsDescendantOf(esroot.hierarchy_id) = 1

		   order by
				es.hierarchy_id.ToString()
		</cfquery>

		<!--- Return the result set --->
		<cfreturn local.result>

	</cffunction>

</cfcomponent>