
<!---

  Template Name:  DataReviewDetailDelegate
     Base Table:  DataReviewDetail	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, May 08, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, May 08, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReviewDetailDelegate"
				hint="This CFC manages all data access interactions for the DataReviewDetail table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyDataReviewDetailComponent"
				hint="This method is used to retrieve a Data Review Detail object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.DataReviewDetail"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.DataReviewDetail")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getDataReviewDetailsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Data Review Detail objects for all the Data Review Details in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="review_detail_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review detail."/>
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Data Review Details and build out the array of components --->
		<cfset local.qResult = getDataReviewDetail(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.DataReviewDetail")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReviewDetailAsComponent"
				hint="This method is used to retrieve a single instance of a / an Data Review Detail value object representing a single Data Review Detail record."
				output="false"
				returnType="mce.e2.vo.DataReviewDetail"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="review_detail_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review detail."/>
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property."/>
		<cfargument name="is_current_review_detail" required="false" type="numeric" hint="Describes whether to retrieve the active / current data review detail record for a given parent data review."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Data Review Detail and build out the component --->
		<cfset local.qResult = getDataReviewDetail(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.DataReviewDetail")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReviewDetail"
				hint="This method is used to retrieve single / multiple records from the DataReviewDetail table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="review_detail_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review detail."/>
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier of an associated data review."/>
		<cfargument name="period_name" required="false" type="string" hint="Describes the external / customer facing name of a given data review detail period."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property associated to a given data review detail."/>
		<cfargument name="is_current_review_detail" required="false" type="numeric" hint="Describes whether to retrieve the active / current data review detail record for a given parent data review."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review Detail records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the DataReviewDetail table matching the where clause
			select	tbl.review_detail_uid,
					tbl.data_review_uid,
					tbl.period_name,
					tbl.is_current_review_detail,
					tbl.detail_period_start,
					tbl.detail_period_end,
					tbl.property_uid,
					tbl.approval_status_date,
					tbl.approval_status_code,
					tbl.approval_status_notes,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.DataReviewDetail tbl (nolock)
			where	1=1
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'is_current_review_detail')>
					and tbl.is_current_review_detail in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_current_review_detail#" null="false" list="false"> ) 					
					</cfif>

					<cfif structKeyExists(arguments, 'review_detail_uid')>
					and tbl.review_detail_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_detail_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'data_review_uid')>
					and tbl.data_review_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.data_review_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'period_name')>
					and tbl.period_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.period_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false"> ) 
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Data Review Detail data. --->	
	<cffunction name="saveNewDataReviewDetail"
				hint="This method is used to persist a new Data Review Detail record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataReviewDetail" required="true" type="mce.e2.vo.DataReviewDetail" hint="Describes the VO containing the Data Review Detail record that will be persisted."/>

		<!--- Persist the Data Review Detail data to the application database --->
		<cfset this.dataMgr.insertRecord("DataReviewDetail", arguments.DataReviewDetail)/>

	</cffunction>

	<cffunction name="saveExistingDataReviewDetail"
				hint="This method is used to persist an existing Data Review Detail record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataReviewDetail" required="true" type="mce.e2.vo.DataReviewDetail" hint="Describes the VO containing the Data Review Detail record that will be persisted."/>

		<!--- Persist the Data Review Detail data to the application database --->
		<cfset this.dataMgr.updateRecord("DataReviewDetail", arguments.DataReviewDetail)/>

	</cffunction>

</cfcomponent>
