<cfcomponent displayname="User Delegate" 
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving user data from the application database.">

	<!--- Define the methods to set the bean / delegates --->
	<cffunction name="setSecurityDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user security information (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.SecurityDelegate" hint="Describes the *.cfc used to manage database interactions related to user security.">
		<cfset this.SecurityDelegate = arguments.bean>
	</cffunction>

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyUserComponent" 
				returntype="mce.e2.vo.User"
				hint="This method is used to retrieve a User object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var UserComponent = createObject("component", "mce.e2.vo.User")/>

		<!--- Return the User component --->
		<cfreturn UserComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getUsersAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of User objects for all the Users in the application.">

		<!--- Define the arguments for this method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user group record; can also contain a list of comma-delimited user group primary keys.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited user primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="singular" hint="Describes the method used to retrieve user group associations; inclusive = associated; exclusive = not associated; userGroupAssocations = all.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Determine the query to execute based on the selectMethod --->
		<cfswitch expression="#arguments.selectMethod#">
		
			<cfcase value="userGroupAssociations">

				<!--- Retrieve a comprehensive list of user group associations --->
				<cfset local.qUser = getUserGroupAssociations(argumentCollection=arguments)>
		
			</cfcase>
			
			<cfdefaultcase>
			
				<!--- Retrieve the users based on the method arguments --->
				<cfset local.qUser = getUser(argumentCollection=arguments)>
		
			</cfdefaultcase>
			
		</cfswitch>

		<!--- Retrieve all Users and build out the array of components --->
		<cfset local.returnResult = queryToVoArray(local.qUser, "mce.e2.vo.User")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getUserAsComponent"
				returntype="mce.e2.vo.User"
				hint="This method is used to retrieve a User record from the application database, and populate its properties in a User object.">

		<!--- Define the arguments for the method --->
		<cfargument name="user_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited User primary keys.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Users and build out the array of components --->
		<cfset local.qUser = getUser(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVo(local.qUser, "mce.e2.vo.User")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getUserCompanyRolesAsComponent"
				access="public" returntype="mce.e2.vo.UserCompanyRoles"
				hint="This method is sued to retrieve extended information about a user (user, company, and associated user roles).">

		<!--- Define the arguments for the method --->
		<cfargument name="user_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited User primary keys.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the user / company properties --->
		<cfset local.qUser = getUserCompanyInformation(user_uid=arguments.user_uid)>
		<cfset local.returnResult = queryToVo(local.qUser, "mce.e2.vo.UserCompanyRoles")>

		<!--- Retrieve the user roles --->
		<cfset local.userRoleList = this.securityDelegate.getCurrentRoles(user_uid=arguments.user_uid)>
		<cfset local.returnResult.roleLookupCodes = listToArray(local.userRoleList)>
		
		<!--- Return the populated vo object --->
		<cfreturn local.returnResult>
		
	</cffunction>

	<!--- Define the methods used to interact with User data in the application database --->
	<cffunction name="getUserGroupAssociations"
				returntype="query"
				hint="This method is used to retrieve all user / user group associations.">
			
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user group record; can also contain a list of comma-delimited user group primary keys.">
		
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the User's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
		
			SELECT  u.client_company_uid,
					u.user_uid, 
					u.first_name,
					u.last_name,
					u.username, 
					u.password_hash_code, 
					u.password_hash_method, 

					1 as isAssociated,
					case
					
						when uug.user_group_uid = ug.user_group_uid then uug.relationship_id
						else null
						
					end relationship_id,
					
					u.is_active,
					u.created_date,
					u.created_by,
					u.modified_date,
					u.modified_by
			FROM 	dbo.Users u
					
					<!--- Only include the group joins if a user group is specified --->
					join dbo.Users_userGroups uug
						on u.user_uid = uug.user_uid
						and uug.is_active = 1
					join dbo.userGroups ug
						on uug.user_group_uid = ug.user_group_uid
						and ug.client_company_uid = u.client_company_uid
						and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
						and ug.is_active = 1
			where	1=1 
			
					-- Filter on active users
					-- and u.is_active = 1

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and u.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
					</cfif>
			
			union
			
			SELECT  u.client_company_uid,
					u.user_uid, 
					u.first_name,
					u.last_name,
					u.username, 
					u.password_hash_code, 
					u.password_hash_method, 

					0 as isAssociated,
					null as relationship_id,
					
					u.is_active,
					u.created_date,
					u.created_by,
					u.modified_date,
					u.modified_by
			FROM 	dbo.Users u						
			where	1=1 
			
					-- Filter on active users
					-- and u.is_active = 1
					and u.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
					and u.user_uid not in ( 
					
						select	uug.user_uid
						from	dbo.users_userGroups uug
								join dbo.userGroups ug
									on uug.user_group_uid = ug.user_group_uid
						where	ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
								and ug.is_active = 1
								and uug.is_active = 1		

					)			

			order
			by		u.last_name,
					u.first_name,
					u.modified_date
												
		</cfquery>				
				
		<!--- Return the query / result set --->		
		<cfreturn qResult>		
				
	</cffunction>	

	<cffunction name="getUser"
				returntype="query"
				hint="This method is used to retrieve data for a given User definition.">
			
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user group record; can also contain a list of comma-delimited user group primary keys.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited User primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="singular" hint="Describes the method used to retrieve user / user group associations; inclusive = associated; exclusive = not associated; all = all.">
		
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the User's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
		
			select  u.client_company_uid,
					u.user_uid, 
					u.first_name,
					u.last_name,
					u.username, 
					u.password_hash_code, 
					u.password_hash_method, 
					u.alert_email,

			<cfswitch expression="#arguments.selectMethod#">	
			
				<cfcase value="inclusive">
					-- Select the meta columns
					1 as isAssociated,
					case
					
						when uug.user_group_uid = ug.user_group_uid then uug.relationship_id
						else null
						
					end relationship_id,
				</cfcase>
			
				<cfcase value="exclusive">
					-- Select the meta columns
					0 as isAssociated,
					null as relationship_id,
				</cfcase>
						
			</cfswitch>		
					
					u.is_active,
					u.created_date,
					u.created_by,
					u.modified_date,
					u.modified_by
					
			<cfswitch expression="#arguments.selectMethod#">

				<cfdefaultcase>
			from 	dbo.Users u
			where	u.is_active = 1

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and u.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>				
				
					<cfif structKeyExists(arguments, 'user_uid')>
					and u.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_uid#">)
					</cfif>				
				</cfdefaultcase>

				<cfcase value="inclusive">
			from 	dbo.Users u
					join dbo.Users_userGroups uug
						on u.user_uid = uug.user_uid
					join dbo.userGroups ug
						on uug.user_group_uid = ug.user_group_uid
						and ug.client_company_uid = u.client_company_uid
						and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
			where	u.is_active = 1
					and ug.is_active = 1
					and uug.is_active = 1
						
					<cfif structKeyExists(arguments, 'user_uid')>
					and u.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_uid#"> )
					</cfif>	
				</cfcase>

				<cfcase value="exclusive">
			from 	dbo.Users u
			where	u.is_active = 1

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and u.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>				
				
					and u.user_uid not in ( 
					
						-- Exclude the groups already associated to the current user
						select	uug1.user_uid
						from	dbo.Users_UserGroups uug1
								join dbo.UserGroups ug
									on uug1.user_group_uid = ug.user_group_uid
						where	ug.is_active = 1 
								and uug1.is_active = 1 
								and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )

					)
				</cfcase>					
				
			</cfswitch>											
									
			order
			by		u.last_name,
					u.first_name,
					u.modified_date
												
		</cfquery>				
				
		<!--- Return the query / result set --->		
		<cfreturn qResult>		
				
	</cffunction>	

	<cffunction name="getUserCompanyInformation"
				returntype="query"
				hint="This method is used to retrieve user / company data for a given user definition.">
			
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited User primary keys.">
		
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the User's in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
		
			select  u.client_company_uid,
					u.user_uid, 
					u.first_name,
					u.last_name,
					u.username, 
					u.is_active,
					u.created_by,
					u.created_date,
					u.modified_by,
					u.modified_date,
					
					cc.friendly_name as companyFriendlyName,
					cc.image_filename
					
			from 	dbo.Users u (nolock)
					join dbo.ClientCompanies cc (nolock)
						on u.client_company_uid = cc.client_company_uid

			where	u.is_active = 1
					and cc.is_active = 1

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and u.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>				
				
					<cfif structKeyExists(arguments, 'user_uid')>
					and u.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_uid#">)
					</cfif>				
									
			order
			by		u.last_name,
					u.first_name,
					u.modified_date
												
		</cfquery>				
				
		<!--- Return the query / result set --->		
		<cfreturn qResult>		
				
	</cffunction>	


	<cffunction name="isUserNameUnique"
				returnType="boolean"
				access="public"
				hint="This method is used to validate the uniqueness of a given user name in the application database.">
		
		<!--- Define the arguments for this method --->
		<cfargument name="username" type="string" required="true" hint="Describes the username that is being validated as unique.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; used to validate the user that owns the specified user name.">
				
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Default the return result --->
		<cfset var returnResult = false>

		<!--- Build out the query to validate the uniqueness of a username from the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
		
			select	1
			from	dbo.Users
			where	1=1 
			
					-- Only filter on active records
					and is_active = 1
			
					<!--- Only retrieve records where the user has this user name --->
					and username = <cfqueryparam cfsqltype="cf_sql_varchar" list="false" value="#arguments.username#">
					
					<!--- Check to see if any other users have this username --->
					<cfif structKeyExists(arguments, 'user_uid') and len(arguments['user_uid'])>
					and user_uid <> <cfqueryparam cfsqltype="cf_sql_varchar" list="false" value="#arguments.user_uid#">
					</cfif>
				
		</cfquery>
		
		<!--- Was a username found? --->
		<cfif qResult.recordCount eq 0>
		
			<!--- If not, then certify it as unique --->
			<cfset returnResult = true>
		
		</cfif>
		
		<!--- Return the return result --->		
		<cfreturn returnResult>		
										
	</cffunction>

	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewUser" 
				access="public" returntype="void"
				hint="This method is used to persist a new User record to the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="User" type="mce.e2.vo.User" hint="Describes the VO containing the User record that will be persisted.">

		<!--- Persist the User data to the application database --->
		<cfset this.dataMgr.insertRecord("Users", arguments.User)>
				
	</cffunction>	
	
	<cffunction name="saveExistingUser" 
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing User record to the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="User" type="mce.e2.vo.User" hint="Describes the VO containing the User record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Persist the User data to the application database --->
		<cfset this.dataMgr.updateRecord("Users", arguments.User)>
				
	</cffunction>	
	
</cfcomponent>