
<!---

  Template Name:  StoredDocumentDelegate
     Base Table:  StoredDocuments	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 16, 2009 - Template Created.		

--->
		
<cfcomponent displayname="StoredDocumentDelegate"
				hint="This CFC manages all data access interactions for the StoredDocuments table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyStoredDocumentComponent"
				hint="This method is used to retrieve a Stored Document object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.StoredDocument"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.StoredDocument")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getStoredDocumentsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Stored Document objects for all the Stored Documents in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="stored_document_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given stored document."/>
		<cfargument name="document_repository_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the repository where a given document will be stored."/>
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing the document set associated to a given stored document."/>
		<cfargument name="permission_needed" required="false" type="string" hint="Describes the permission level assigned to a given report."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Stored Documents and build out the array of components --->
		<cfset local.qResult = getStoredDocument(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.StoredDocument")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getStoredDocumentAsComponent"
				hint="This method is used to retrieve a single instance of a / an Stored Document value object representing a single Stored Document record."
				output="false"
				returnType="mce.e2.vo.StoredDocument"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="stored_document_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given stored document."/>
		<cfargument name="document_repository_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the repository where a given document will be stored."/>
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing the document set associated to a given stored document."/>
		<cfargument name="original_filename" required="false" type="string" hint="Describes the original filename of a given document."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Stored Document and build out the component --->
		<cfset local.qResult = getStoredDocument(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.StoredDocument")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getStoredDocument"
				hint="This method is used to retrieve single / multiple records from the StoredDocuments table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="stored_document_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given stored document."/>
		<cfargument name="document_repository_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the repository where a given document will be stored."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given document."/>
		<cfargument name="original_filename" required="false" type="string" hint="Describes the original filename of a given document."/>
		<cfargument name="document_keywords" required="false" type="string" hint="Describes the keywords associated to a given document."/>
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing the document set associated to a given stored document."/>
		<cfargument name="content_as_filename" required="false" type="string" hint="Describes the content file name for a given docuemtn."/>
		<cfargument name="folder_name" required="false" type="string" hint="Describes the name of the folder where a given document has been stored."/>
		<cfargument name="preferred_filename" required="false" type="string" hint="Describes the preferred filename for a given document."/>
		<cfargument name="mime_type" required="false" type="string" hint="Describes the mime type associated to a given document."/>
		<cfargument name="permission_needed" required="false" type="string" hint="Describes the permission level assigned to a given report."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Stored Document records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the StoredDocuments table matching the where clause
			select	tbl.stored_document_uid,
					tbl.document_repository_uid,
					tbl.friendly_name,
					tbl.original_filename,
					tbl.document_keywords,
					tbl.document_set_uid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					tbl.content_as_filename,
					tbl.storage_date,
					tbl.folder_name,
					tbl.preferred_filename,
					tbl.mime_type,
					tbl.permission_needed

			from	dbo.StoredDocuments tbl (nolock)
			where	1=1
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'stored_document_uid') and len(arguments.stored_document_uid)>
					and tbl.stored_document_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.stored_document_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'document_repository_uid') and len(arguments.document_repository_uid)>
					and tbl.document_repository_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.document_repository_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'original_filename')>
					and tbl.original_filename in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.original_filename#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'document_set_uid') and len(arguments.document_set_uid)>
					and tbl.document_set_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.document_set_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'content_as_filename')>
					and tbl.content_as_filename in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.content_as_filename#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'folder_name')>
					and tbl.folder_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.folder_name#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'preferred_filename')>
					and tbl.preferred_filename in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.preferred_filename#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'mime_type')>
					and tbl.mime_type in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.mime_type#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'permission_needed') and len(arguments.permission_needed)>
					and tbl.permission_needed in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.permission_needed#" null="false" list="true"> ) 
					</cfif>
					

			order
			by		tbl.folder_name,
					tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!--- Get document and document set data by context. --->
	<cffunction name="getStoredDocumentsByContext"
				hint="This method is used to retrieve single / multiple records from the StoredDocumentSet table based on their context such as clients, properties, energy contracts, and client contracts."
				output="false"
				returnType="Query" 
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="energy_contract_uid" required="false" type="string"/>
		<cfargument name="client_contract_uid" required="false" type="string"/>
		<cfargument name="include_properties" required="false" type="boolean" default="true"/>
		<cfargument name="filter_by_active" required="false" type="boolean" default="true"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Pull the appropriate document sets. --->
		<cfstoredproc datasource="#this.datasource#" procedure="DocumentSetsByContext">
			<cfprocparam value="#arguments.client_company_uid#" cfsqltype="CF_SQL_IDSTAMP" null="#iif(isUniqueIdentifier(arguments.client_company_uid), 'false', 'true')#">
			<cfprocparam value="#arguments.property_uid#" cfsqltype="CF_SQL_IDSTAMP" null="#iif(isUniqueIdentifier(arguments.property_uid), 'false', 'true')#">
			<cfprocparam value="#arguments.energy_contract_uid#" cfsqltype="CF_SQL_IDSTAMP" null="#iif(isUniqueIdentifier(arguments.energy_contract_uid), 'false', 'true')#">
			<cfprocparam value="#arguments.client_contract_uid#" cfsqltype="CF_SQL_IDSTAMP" null="#iif(isUniqueIdentifier(arguments.client_contract_uid), 'false', 'true')#">
			<cfprocparam value="#arguments.include_properties#" cfsqltype="CF_SQL_BIT">
			<cfprocparam value="#arguments.filter_by_active#" cfsqltype="CF_SQL_BIT">
			<cfprocresult name="local.qResult">
		</cfstoredproc>
		
		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Stored Document data. --->	
	<cffunction name="saveNewStoredDocument"
				hint="This method is used to persist a new Stored Document record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="StoredDocument" required="true" type="mce.e2.vo.StoredDocument" hint="Describes the VO containing the Stored Document record that will be persisted."/>

		<!--- Persist the Stored Document data to the application database --->
		<cfset this.dataMgr.insertRecord("StoredDocuments", arguments.StoredDocument)/>

	</cffunction>

	<cffunction name="saveExistingStoredDocument"
				hint="This method is used to persist an existing Stored Document record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="StoredDocument" required="true" type="mce.e2.vo.StoredDocument" hint="Describes the VO containing the Stored Document record that will be persisted."/>

		<!--- Persist the Stored Document data to the application database --->
		<cfset this.dataMgr.updateRecord("StoredDocuments", arguments.StoredDocument)/>

	</cffunction>

	<cffunction name="deactivateStoredDocument"
				hint="This method is used to deactivate single / multiple records in the StoredDocuments table."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="stored_document_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given stored document."/>
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier for the document set associated to a given stored document."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the user deactivating the stored document."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date / time that the document was deactivated."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- deactivate the specified document --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			update	tbl
			set		tbl.is_active = 0,
					modified_by = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" null="false" list="false">,
					modified_date = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="false">
			from	dbo.storedDocuments tbl 
			where	1=1

					<cfif structKeyExists(arguments, 'stored_document_uid')>
					and tbl.stored_document_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.stored_document_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'document_set_uid')>
					and tbl.document_set_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.document_set_uid#" null="false" list="true"> ) 
					</cfif>

		</cfquery>

	</cffunction>

</cfcomponent>
