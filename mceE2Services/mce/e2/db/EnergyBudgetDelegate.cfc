
<!---

  Template Name:  EnergyBudget
     Base Table:  EnergyBudgets	

abe@twintechs.com

       Author:  Jack Freudenheim
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyBudgetDelegate"
				hint="This CFC manages all data access interactions for the EnergyBudgets table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyBudgetComponent"
				hint="This method is used to retrieve a Energy Budget object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyBudget"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyBudget")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyBudgetsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Budget objects the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given Energy Budget."/>
		<cfargument name="default_energy_unit_uid" required="false" type="string" hint="Describes the default energy unit associated to a given Energy Budget."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Budgets and build out the array of components --->
		<cfset local.qResult = getEnergyBudget(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyBudget")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyBudgetAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Budget value object representing a single Energy Budget record."
				output="false"
				returnType="mce.e2.vo.EnergyBudget"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_budget_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given Energy Budget."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Budget and build out the component --->
		<cfset local.qResult = getEnergyBudget(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyBudget")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>
	
	<cffunction name="getAssociatedAccountsForEnergyBudget"
				hint="This method is used to retrieve multiple records from the EnergyBudgets_EnergyAccounts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_budget_uid" required="true" type="string" hint="Describes the primary key / unique identifierfor a given energy budget."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Budget records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyBudgets_EnergyAccounts table matching the where clause
			select	tbl.relationship_id,
					tbl.energy_account_uid,
					tbl.is_active,
					tbl.comment_text

			from	dbo.EnergyBudgets_EnergyAccounts tbl
			where	1=1
					and tbl.is_active = 1
					<cfif structKeyExists(arguments, 'energy_budget_uid')>
					and tbl.energy_budget_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energy_budget_uid#" list="true" null="false"> ) 
					</cfif>

			order by
					tbl.energy_account_uid

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>
	

	<cffunction name="getEnergyBudgetSummary"
				hint="This method is used to retrieve multiple records from the EnergyBudgets table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property" required="true" type="mce.e2.vo.Property" hint="Describes the primary key / unique identifierfor a given property."/>
		<cfargument name="clientCompany" required="true" type="mce.e2.vo.ClientCompany" hint="Describes the primary key / unique identifierfor a given client company."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Budget records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyBudgets table matching the where clause
			select distinct	tbl.energy_budget_uid,
					tbl.friendly_name,
					tbl.budget_start,
					tbl.is_locked

			from	dbo.EnergyBudgets tbl (nolock) join
					dbo.EnergyBudgets_EnergyAccounts ebea on tbl.energy_budget_uid = ebea.energy_budget_uid join
					dbo.EnergyAccounts ea on ebea.energy_account_uid = ea.energy_account_uid
			where	1=1
					and tbl.is_active = 1
					and ebea.is_active = 1
					
					and budget_type_code in('Budget','Reforecast')
					
					<!--- 
<cfif structKeyExists(arguments, 'clientCompany')>
					--and tbl.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.clientCompany.client_company_uid#" list="true" null="false"> ) 
					</cfif>
 --->

					<cfif structKeyExists(arguments, 'property')>
					and ea.property_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.property.property_uid#" list="true" null="false"> ) 
					</cfif>

			order by
					tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<cffunction name="getEnergyAccountEnergyBudgets"
				hint="This method is used to retrieve multiple records from the EnergyBudgets table."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifierfor a given energy account."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Budget records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyBudgets table matching the where clause
			select distinct	tbl.energy_budget_uid,
					tbl.friendly_name

			from	dbo.EnergyBudgets tbl (nolock) 
				join dbo.EnergyBudgets_EnergyAccounts ebea 
						on tbl.energy_budget_uid = ebea.energy_budget_uid
			where	1=1
					and tbl.is_active = 1
					
					and budget_type_code in('Budget','Reforecast')
					
					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and ebea.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" list="true" null="false"> ) 
					</cfif>

			order by
					tbl.friendly_name

		</cfquery>

<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyBudget")/>
		<!--- Return the result set based on the query parameters --->
		<cfreturn local.returnResult>		

	</cffunction>
	
	<cffunction name="getEnergyBudget"
				hint="This method is used to retrieve single / multiple records from the EnergyBudgets table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_budget_uid" required="true" type="string" hint="Describes the primary key / unique identifierfor a given Energy Budget."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Budget records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyBudgets table matching the where clause
			select	tbl.energy_budget_uid,
					tbl.budget_type_code,
					tbl.friendly_name,
					tbl.budget_start,
					tbl.budget_end,
					tbl.client_company_uid,
					tbl.permission_required_to_edit,
					tbl.comment_text,
					tbl.is_locked,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.EnergyBudgets tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'energy_budget_uid')>
					and tbl.energy_budget_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_budget_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Budget data. --->	
	<cffunction name="saveNewEnergyBudget"
				hint="This method is used to persist a new Energy Budget record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energyBudget" required="true" type="mce.e2.vo.EnergyBudget" hint="Describes the VO containing the Energy Budget record that will be persisted."/>

		
		<cfset var newUid = "">
		<cfset var infoUid = "">		
		<cfset var i = 0>
		<cfset var energyEntryInfo = "">
		<cfset var metaFields = structNew()>
		<cfset var energyAccount = "">		
		<cfset var rel_rec = "">
		<cfset var energyBudgetDetail = structNew()>
		<cfset var noMeta = "false">

		<!--- This is a multi-step process (master and detail records), so open a transaction in case anything goes wrong --->
		<cftransaction action="begin">
			<!--- First, perist the main EnergyBudget record and get its ID --->
			<cfset newUid = this.dataMgr.insertRecord("EnergyBudgets", arguments.energyBudget)>


			<cfset energyBudgetDetail.energy_budget_uid = newUid>
			<cfset energyBudgetDetail.created_by = arguments.energyBudget.created_by>
			<cfset energyBudgetDetail.created_date = arguments.energyBudget.created_date>
			<cfset energyBudgetDetail.modified_by = arguments.energyBudget.modified_by>
			<cfset energyBudgetDetail.modified_date = arguments.energyBudget.modified_date>
			<cfset energyBudgetDetail.is_active = 1>
			
			<!--- Now persist a related EnergyUsage record for each --->
			<cfloop from="1" to="#ArrayLen(energyBudget.energyUsage)#" index="i">
				<cfset energyEntryInfo = energyBudget.energyUsage[i]>
				<cfset metaFields[0] = structNew()>
				<cfset metaFields[1] = structNew()>
						<cfif energyEntryInfo.energyTypeFriendlyName eq "Water">
			<cfset noMeta = "true">
			<cfelseif energyEntryInfo.energyTypeFriendlyName eq "Steam">
				<cfset noMeta = "true">
			<cfelse>
				<cfset noMeta = "false">
		</cfif>
				<cfset energyEntryInfo.metaFields = metaFields>
				
				<cfset energyEntryInfo.created_by = arguments.energyBudget.created_by>
				<cfset energyEntryInfo.created_date = arguments.energyBudget.created_date>
				<cfset energyEntryInfo.modified_by = arguments.energyBudget.modified_by>
				<cfset energyEntryInfo.modified_date = arguments.energyBudget.modified_date>
				<cfset energyEntryInfo.is_active = 1>
				<cfset infoUid = this.dataMgr.insertRecord("EnergyUsage", energyEntryInfo)>
				
				<cfif noMeta eq "false">
				<cfset energyEntryInfo.metaFields = metaFields>
				<cfset energyEntryInfo.metaFields[0].energy_usage_uid = infoUid>
				<cfset energyEntryInfo.metaFields[0].type_lookup_code = "tdcharges">
				<cfset energyEntryInfo.metaFields[0].meta_value = energyEntryInfo.total_utility_cost>
				<cfset energyEntryInfo.metaFields[0].meta_type_uid = energyEntryInfo.td_meta_type_uid>
				<cfset energyEntryInfo.metaFields[1].energy_usage_uid = infoUid>
				<cfset energyEntryInfo.metaFields[1].type_lookup_code = "supplycharges">
				<cfset energyEntryInfo.metaFields[1].meta_value = energyEntryInfo.total_supply_cost>
				<cfset energyEntryInfo.metaFields[1].meta_type_uid = energyEntryInfo.supply_meta_type_uid>
				
				<cfset this.dataMgr.insertRecord("EnergyUsageMeta", energyEntryInfo.metaFields[0])>
				<cfset this.dataMgr.insertRecord("EnergyUsageMeta", energyEntryInfo.metaFields[1])>
				</cfif>
				<cfset energyBudgetDetail.energy_usage_uid = infoUid>
				<cfset this.dataMgr.insertRecord("EnergyBudgetDetail", energyBudgetDetail)>
			</cfloop>


			<!--- Now persist an EnergyBudgets_EnergyAccounts record for each account in the budget --->
			<cfloop from="1" to="#ArrayLen(energyBudget.energyAccounts)#" index="i">
				<cfset energyAccount = energyBudget.energyAccounts[i]>
				<cfset rel_rec = structNew()>
				<cfset rel_rec.energy_budget_uid = newUid>
				<cfset rel_rec.energy_account_uid = energyAccount.energyAccountSummary.energy_account_uid>
				<cfset rel_rec.comment_text = energyAccount.comments>
				<cfset rel_rec.created_by = arguments.energyBudget.created_by>
				<cfset rel_rec.created_date = arguments.energyBudget.created_date>
				<cfset rel_rec.modified_by = arguments.energyBudget.modified_by>
				<cfset rel_rec.modified_date = arguments.energyBudget.modified_date>
				<cfset rel_rec.is_active = 1>
				<cfset this.dataMgr.insertRecord("EnergyBudgets_EnergyAccounts", rel_rec)>
			</cfloop>

			<!--- Now we can commit the transaction --->
			<cftransaction action="commit"/>
		</cftransaction>

		

	</cffunction>


	<cffunction name="checkForAccountPeriodConflicts"
				hint="Check if any of the accounts in this budget already exist with the same budget type for the same periods, and if so, throw an error."	
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energyBudget" required="true" type="mce.e2.vo.EnergyBudget" hint="Describes the VO containing the Energy Budget record that will be persisted."/>
		<cfset var mylist= "">
		<cfset var accountList = "">
		<cfset var energyAccount = "">
		<cfset var local = structnew()>
		
		<cfloop from="1" to="#ArrayLen(energyBudget.energyAccounts)#" index="i">
		   <cfset energyAccount = energyBudget.energyAccounts[i]>
		   <cfset accountList = ListAppend(accountList, energyAccount.energyAccountSummary.energy_account_uid)>
		 </cfloop>		
		<cfif accountList eq "">
			<cfreturn>
		</cfif>
			<cfquery name="local.qResult" datasource="#this.datasource#">	
				select distinct 
					ea.friendly_name 
				from
					EnergyBudgets_EnergyAccounts ebea 
					join EnergyBudgets eb on ebea.energy_budget_uid = eb.energy_budget_uid 
					join EnergyAccounts ea on ebea.energy_account_uid = ea.energy_account_uid
				where 
					1 = 1
					and eb.is_active = 1
					and ebea.is_active = 1
					<cfif structKeyExists(energyBudget, 'budget_type_code')>
						and eb.budget_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyBudget.budget_type_code#" list="false" null="false">
					</cfif>
					<cfif structKeyExists(energyBudget, 'energy_budget_uid') and (Len(energyBudget.energy_budget_uid) is not 0)>
						and eb.energy_budget_uid <> <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyBudget.energy_budget_uid#" list="false" null="false"> 
					</cfif>
					and ebea.energy_account_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" value="#accountList#" list="true" null="false">) 
					and (eb.budget_start between <cfqueryparam cfsqltype="cf_sql_date" value="#energyBudget.budget_start#" list="false" null="false"> 
							and <cfqueryparam cfsqltype="cf_sql_date" value="#energyBudget.budget_end#" list="false" null="false"> 
						OR eb.budget_end between <cfqueryparam cfsqltype="cf_sql_date" value="#energyBudget.budget_start#" list="false" null="false"> 
							and <cfqueryparam cfsqltype="cf_sql_date" value="#energyBudget.budget_end#" list="false" null="false"> )
			</cfquery>

		<cfif local.qResult.recordCount gt 0>
			<cfset mylist = ValueList(local.qResult.friendly_name)>
				<cfif local.qResult.recordCount eq 1>
					<cfset local.myDetail = "This budget can not be saved, because the following account is already in a budget for this time period: " & mylist>
				<cfelse>
					<cfset local.myDetail = "This budget can not be saved, because the following accounts are already in budgets for this time period: " & mylist>
				</cfif>
				<cfthrow message="Account/Period Conflict"
							 detail=#local.myDetail# >
		</cfif>
	</cffunction>

	<cffunction name="saveExistingEnergyBudget"
				hint="This method is used to persist an existing Energy Budget record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energyBudget" required="true" type="mce.e2.vo.EnergyBudget" hint="Describes the VO containing the Energy Budget record that will be persisted."/>

		
		<cfset var budgetUid = "">
		<cfset var infoUid = "">		
		<cfset var i = 0>
		<cfset var energyEntryInfo = "">
		<cfset var metaFields = structNew()>
		<cfset var energyAccount = "">		
		<cfset var rel_rec = "">
		<cfset var energyBudgetDetail = structNew()>
		<cfset var noMeta = "false">

				
		<!--- This is a multi-step process (master and detail records), so open a transaction in case anything goes wrong --->
		<cftransaction action="begin">
			<!--- First, update the main EnergyBudget record --->
			<cfset this.dataMgr.updateRecord("EnergyBudgets", arguments.energyBudget)>
			<cfset budgetUid = arguments.energyBudget.energy_budget_uid>

			<cfset energyBudgetDetail.energy_budget_uid = budgetUid>
			<!-- set create date to the master budget's modified properties-->
			<cfset energyBudgetDetail.created_by = arguments.energyBudget.modified_by>
			<cfset energyBudgetDetail.created_date = arguments.energyBudget.modified_date>
			<cfset energyBudgetDetail.modified_by = arguments.energyBudget.modified_by>
			<cfset energyBudgetDetail.modified_date = arguments.energyBudget.modified_date>
			<cfset energyBudgetDetail.is_active = 1>
			
			<!--- Now persist a related EnergyUsage record for each --->
			<cfloop from="1" to="#ArrayLen(energyBudget.energyUsage)#" index="i">
				<cfset energyEntryInfo = energyBudget.energyUsage[i]>
				<cfset metaFields[0] = structNew()>
				<cfset metaFields[1] = structNew()>
			<cfif energyEntryInfo.energyTypeFriendlyName eq "Water">
					<cfset noMeta = "true">
				<cfelseif energyEntryInfo.energyTypeFriendlyName eq "Steam">
					<cfset noMeta = "true">
			<cfelse>
					<cfset noMeta = "false">
			</cfif>
			
				<cfset energyEntryInfo.metaFields = metaFields>
				<cfif NOT structKeyExists(energyEntryInfo, 'energy_usage_uid')>
					<cfset energyEntryInfo.created_by = arguments.energyBudget.created_by>
					<cfset energyEntryInfo.created_date = createOdbcDateTime(now())>
					<cfset energyEntryInfo.modified_by = arguments.energyBudget.modified_by>
					<cfset energyEntryInfo.modified_date = arguments.energyBudget.modified_date>
					<cfset energyEntryInfo.is_active = 1>
					<cfset infoUid = this.dataMgr.insertRecord("EnergyUsage", energyEntryInfo)>

					<cfif noMeta eq "false">
						<cfif NOT structKeyExists(energyEntryInfo,'td_usage_meta_uid')>
							<cfset energyEntryInfo.metaFields[0].energy_usage_uid = infoUid>
							<cfset energyEntryInfo.metaFields[0].type_lookup_code = "tdcharges">
							<cfset energyEntryInfo.metaFields[0].meta_value = energyEntryInfo.total_utility_cost>
							<cfset energyEntryInfo.metaFields[0].meta_type_uid = energyEntryInfo.td_meta_type_uid>
							<cfset this.dataMgr.insertRecord("EnergyUsageMeta", energyEntryInfo.metaFields[0])>
						<cfelse>
							<cfset energyEntryInfo.metaFields[0].usage_meta_uid = energyEntryInfo.td_usage_meta_uid>
							<cfset energyEntryInfo.metaFields[0].energy_usage_uid = infoUid>
							<cfset energyEntryInfo.metaFields[0].type_lookup_code = "tdcharges">
							<cfset energyEntryInfo.metaFields[0].meta_value = energyEntryInfo.total_utility_cost>
							<cfset energyEntryInfo.metaFields[0].meta_type_uid = energyEntryInfo.td_meta_type_uid>
							 <cfset this.dataMgr.updateRecord("EnergyUsageMeta", energyEntryInfo.metaFields[0])> 

					</cfif>
						<cfif NOT structKeyExists(energyEntryInfo,'supply_usage_meta_uid')>
							<cfset energyEntryInfo.metaFields[1].energy_usage_uid = infoUid>
							<cfset energyEntryInfo.metaFields[1].type_lookup_code = "supplycharges">
							<cfset energyEntryInfo.metaFields[1].meta_value = energyEntryInfo.total_supply_cost>
							<cfset energyEntryInfo.metaFields[1].meta_type_uid = energyEntryInfo.supply_meta_type_uid>
							 <cfset this.dataMgr.insertRecord("EnergyUsageMeta", energyEntryInfo.metaFields[1])> 
						<cfelse>
							<cfset energyEntryInfo.metaFields[1].usage_meta_uid = energyEntryInfo.supply_usage_meta_uid>
							<cfset energyEntryInfo.metaFields[1].energy_usage_uid = infoUid>
							<cfset energyEntryInfo.metaFields[1].type_lookup_code = "supplycharges">
							<cfset energyEntryInfo.metaFields[1].meta_value = energyEntryInfo.total_supply_cost>
							<cfset energyEntryInfo.metaFields[1].meta_type_uid = energyEntryInfo.supply_meta_type_uid>
							 <cfset this.dataMgr.updateRecord("EnergyUsageMeta", energyEntryInfo.metaFields[1])> 
						</cfif>
					</cfif>
					<cfset energyBudgetDetail.energy_usage_uid = infoUid>
					<cfset this.dataMgr.insertRecord("EnergyBudgetDetail", energyBudgetDetail)>
				<cfelse>
					<cfset energyEntryInfo.modified_by = arguments.energyBudget.modified_by>
					<cfset energyEntryInfo.modified_date = arguments.energyBudget.modified_date>
					<cfset infoUid = this.dataMgr.updateRecord("EnergyUsage", energyEntryInfo)>

					<cfif noMeta eq "false">
						<cfif structKeyExists(energyEntryInfo,'td_usage_meta_uid')>
							<cfif energyEntryInfo.td_usage_meta_uid eq "0">
								<cfset energyEntryInfo.metaFields[0].energy_usage_uid = infoUid>
								<cfset energyEntryInfo.metaFields[0].type_lookup_code = "tdcharges">
								<cfset energyEntryInfo.metaFields[0].meta_value = energyEntryInfo.total_utility_cost>
								<cfset energyEntryInfo.metaFields[0].meta_type_uid = energyEntryInfo.td_meta_type_uid>
								<cfset this.dataMgr.insertRecord("EnergyUsageMeta", energyEntryInfo.metaFields[0])>
							<cfelse>
								<cfset energyEntryInfo.metaFields[0].usage_meta_uid = energyEntryInfo.td_usage_meta_uid>
								<cfset energyEntryInfo.metaFields[0].energy_usage_uid = infoUid>
								<cfset energyEntryInfo.metaFields[0].type_lookup_code = "tdcharges">
								<cfset energyEntryInfo.metaFields[0].meta_value = energyEntryInfo.total_utility_cost>
								<cfset energyEntryInfo.metaFields[0].meta_type_uid = energyEntryInfo.td_meta_type_uid>
								<cfset this.dataMgr.updateRecord("EnergyUsageMeta", energyEntryInfo.metaFields[0])>
							</cfif>
						</cfif>
						<cfif structKeyExists(energyEntryInfo,'supply_usage_meta_uid')>
							<cfif energyEntryInfo.supply_usage_meta_uid eq "0">
								<cfset energyEntryInfo.metaFields[1].energy_usage_uid = infoUid>
								<cfset energyEntryInfo.metaFields[1].type_lookup_code = "supplycharges">
								<cfset energyEntryInfo.metaFields[1].meta_value = energyEntryInfo.total_supply_cost>
								<cfset energyEntryInfo.metaFields[1].meta_type_uid = energyEntryInfo.supply_meta_type_uid>
								<cfset this.dataMgr.insertRecord("EnergyUsageMeta", energyEntryInfo.metaFields[1])>
							<cfelse>
								<cfset energyEntryInfo.metaFields[1].usage_meta_uid = energyEntryInfo.supply_usage_meta_uid>
								<cfset energyEntryInfo.metaFields[1].energy_usage_uid = infoUid>
								<cfset energyEntryInfo.metaFields[1].type_lookup_code = "supplycharges">
								<cfset energyEntryInfo.metaFields[1].meta_value = energyEntryInfo.total_supply_cost>
								<cfset energyEntryInfo.metaFields[1].meta_type_uid = energyEntryInfo.supply_meta_type_uid>
								<cfset this.dataMgr.updateRecord("EnergyUsageMeta", energyEntryInfo.metaFields[1])> 
							</cfif>
						</cfif>
					</cfif>
					<!-- if the energy usage is made inactive, then do the same for this record-->
					<cfif energyEntryInfo.is_active eq 0>
						<cfset energyBudgetDetail.is_active = 0>
							<cfquery name="local.qResult" datasource="#this.datasource#">
							    UPDATE 
							    	energyBudgetDetail 
							    SET 
							    	is_active = 0
							    WHERE
							    	energy_budget_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#infoUid#" list="false" null="false"> 
							    	and energy_usage_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyEntryInfo.energy_usage_uid#" list="false" null="false"> 
							</cfquery>
						
					</cfif>
				</cfif>
			</cfloop>

			<!--- Now persist an EnergyBudgets_EnergyAccounts record for each account in the budget --->
			<cfloop from="1" to="#ArrayLen(energyBudget.energyAccounts)#" index="i">
				<cfset energyAccount = energyBudget.energyAccounts[i]>
					<cfset rel_rec = structNew()>
					<cfset rel_rec.energy_budget_uid = budgetUid>
					<cfset rel_rec.energy_account_uid = energyAccount.energyAccountSummary.energy_account_uid>
					<cfset rel_rec.comment_text = energyAccount.comments>
					<cfset rel_rec.modified_by = arguments.energyBudget.modified_by>
					<cfset rel_rec.modified_date = arguments.energyBudget.modified_date>
				<cfif NOT structKeyExists(energyAccount, 'relationship_id')>
					<!-- there's a chance here that this account HAD been in the budget but was taken out,
							in which case there's a record already in the EnergyBudgets_EnergyAccounts table
							and adding a new one here would cause a conflict. So before inserting, 
							do a SQL delete just in case.
					-->
					<cfquery name="local.qEnergyBudgets_EnergyAccounts" datasource="#this.datasource#">
					    DELETE FROM
					    	EnergyBudgets_EnergyAccounts 
					    WHERE
					    	energy_budget_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#budgetUid#" list="false" null="false"> 
					    	and energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rel_rec.energy_account_uid#" list="false" null="false"> 
					</cfquery>					
					<cfset rel_rec.is_active = 1>
					<cfset rel_rec.created_by = arguments.energyBudget.modified_by>
					<cfset rel_rec.created_date = arguments.energyBudget.modified_date>
					<cfset this.dataMgr.insertRecord("EnergyBudgets_EnergyAccounts", rel_rec)>
				<cfelse>
					<cfset rel_rec.relationship_id = energyAccount.relationship_id>
				 	<cfset rel_rec.is_active = energyAccount.energyAccountSummary.is_active>
					<cfset this.dataMgr.updateRecord("EnergyBudgets_EnergyAccounts", rel_rec)>
				</cfif>
			</cfloop>

			<!--- Now we can commit the transaction --->
			<cftransaction action="commit"/>
		</cftransaction>

	</cffunction>

</cfcomponent>
