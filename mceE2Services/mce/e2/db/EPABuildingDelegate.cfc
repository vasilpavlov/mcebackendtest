	
<cfcomponent displayname="EPABuildingDelegate"
				hint="This CFC manages all data access interactions for the EPABuilding table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEPABuildingComponent"
				hint="This method is used to retrieve a Property Address object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EPABuilding"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EPABuilding")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEPABuildingsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Property Address objects for all the Property Addresses in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="pmBldgID" required="false" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>
	
		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Property Addresses and build out the array of components --->
		<cfset local.qResult = getEPABuilding(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EPABuilding")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEPABuildingAsComponent"
				hint="This method is used to retrieve a single instance of a / an Property Address value object representing a single Property Address record."
				output="false"
				returnType="mce.e2.vo.EPABuilding"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="pmBldgID" required="false" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Property Address and build out the component --->
		<cfset local.qResult = getEPABuilding(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EPABuilding")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEPABuilding"
				hint="This method is used to retrieve single / multiple records from the EPABuilding table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="pmBldgID" required="false" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>
		
		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Property Address records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EPABuilding table matching the where clause
			select	tbl.*
			from	dbo.EPABuildings tbl (nolock)
			where	1=1 
					and tbl.is_active = 1
					and tbl.property_uid is null
			order
			by		tbl.pmCustID,
					tbl.pmBldgID
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Property Address data. --->	
	<cffunction name="saveNewEPABuilding"
				hint="This method is used to persist a new Property Address record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EPABuilding" required="true" type="mce.e2.vo.EPABuilding" hint="Describes the VO containing the Property Address record that will be persisted."/>

		<!--- Persist the Property Address data to the application database --->
		<cfset this.dataMgr.insertRecord("EPABuildings", arguments.EPABuilding)/>

	</cffunction>

	<cffunction name="saveExistingEPABuilding"
				hint="This method is used to persist an existing Property Address record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EPABuilding" required="true" type="mce.e2.vo.EPABuilding" hint="Describes the VO containing the Property Address record that will be persisted."/>

		<!--- Persist the Property Address data to the application database --->
		<cfset this.dataMgr.updateRecord("EPABuildings", arguments.EPABuilding)/>

	</cffunction>

</cfcomponent>
