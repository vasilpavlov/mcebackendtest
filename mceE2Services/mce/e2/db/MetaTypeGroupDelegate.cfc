
<!---

  Template Name:  MetaTypeGroup
     Base Table:  MetaTypeGroups	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="MetaTypeGroupDelegate"
				hint="This CFC manages all data access interactions for the MetaTypeGroups table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyMetaTypeGroupComponent"
				hint="This method is used to retrieve a Meta Type Group object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.MetaTypeGroup"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.MetaTypeGroup")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getMetaTypeGroupsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Meta Type Group objects for all the Meta Type Groups in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key for a given meta type group."/>
		<cfargument name="rate_class_uid" required="false" type="string" hint="Describes the primary key for a given energy provider rate class (used to filter meta type groups)."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Meta Type Groups and build out the array of components --->
		<cfset local.qResult = getMetaTypeGroup(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.MetaTypeGroup")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getMetaTypeGroupAsComponent"
				hint="This method is used to retrieve a single instance of a / an Meta Type Group value object representing a single Meta Type Group record."
				output="false"
				returnType="mce.e2.vo.MetaTypeGroup"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="meta_group_uid" required="true" type="string" hint="Describes the primary key for a given meta type group."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Meta Type Group and build out the component --->
		<cfset local.qResult = getMetaTypeGroup(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.MetaTypeGroup")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getMetaTypeGroup"
				hint="This method is used to retrieve single / multiple records from the MetaTypeGroups table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the primary key for a given meta type group."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given meta type group."/>
		<cfargument name="group_lookup_code" required="false" type="string" hint="Describes the internal identifier describing a given meta type group."/>
		<cfargument name="rate_class_uid" required="false" type="string" hint="Describes the primary key for a given energy provider rate class (used to filter meta type groups)."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Meta Type Group records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the MetaTypeGroups table matching the where clause
			select	tbl.meta_group_uid,
					tbl.friendly_name,
					tbl.group_lookup_code,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.MetaTypeGroups tbl (nolock)
		
					<!--- Filter on rate classes --->
					<cfif structKeyExists(arguments, 'rate_class_uid')>
					join dbo.MetaTypeGroups_EnergyProviderRateClasses mtgeprc
						on tbl.meta_group_uid = mtgeprc.meta_group_uid
					join dbo.EnergyProviderRateClasses eprc
						on mtgeprc.rate_class_uid = eprc.rate_class_uid
					</cfif>
		
			where	1=1
					and tbl.is_active = 1

					<!--- Filter on rate classes --->
					<cfif structKeyExists(arguments, 'rate_class_uid')>
					and mtgeprc.is_active = 1
					and eprc.is_active = 1
					and eprc.rate_class_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.rate_class_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'meta_group_uid')>
					and tbl.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_group_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'group_lookup_code')>
					and tbl.group_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.group_lookup_code#" list="true" null="false"> ) 
					</cfif>
					
			order
			by		tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Meta Type Group data. --->	
	<cffunction name="saveNewMetaTypeGroup"
				hint="This method is used to persist a new Meta Type Group record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="MetaTypeGroup" required="true" type="mce.e2.vo.MetaTypeGroup" hint="Describes the VO containing the Meta Type Group record that will be persisted."/>

		<!--- Persist the Meta Type Group data to the application database --->
		<cfset this.dataMgr.insertRecord("MetaTypeGroups", arguments.MetaTypeGroup)/>

	</cffunction>

	<cffunction name="saveExistingMetaTypeGroup"
				hint="This method is used to persist an existing Meta Type Group record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="MetaTypeGroup" required="true" type="mce.e2.vo.MetaTypeGroup" hint="Describes the VO containing the Meta Type Group record that will be persisted."/>

		<!--- Persist the Meta Type Group data to the application database --->
		<cfset this.dataMgr.updateRecord("MetaTypeGroups", arguments.MetaTypeGroup)/>

	</cffunction>

</cfcomponent>
