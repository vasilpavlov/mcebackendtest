<cfcomponent displayname="ClientServicesDelegate"
				hint="This CFC manages all data access interactions for the Clients_ClientServices table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyClientServicesComponent"
				hint="This method is used to retrieve a Client Service object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.ClientServices"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.ClientServices")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getClientServicesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Client Service objects for all the Client Services in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client record; can also contain a list of comma-delimited Client primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Client Services and build out the array of components --->
		<cfset local.qResult = getClientServices(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.ClientServices")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientServicesAsComponent"
				hint="This method is used to retrieve a single instance of a / an Client Service value object representing a single Client Service record."
				output="false"
				returnType="mce.e2.vo.ClientServices"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client record; can also contain a list of comma-delimited Client primary keys.">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Client Service and build out the component --->
		<cfset local.qResult = getClientServices(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.ClientServices")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientServices"
				hint="This method is used to retrieve single / multiple records from the Clients_ClientServices table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Service records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the Clients_ClientServices table matching the where clause
			SELECT tbl.relationship_id,
			 tbl.client_company_uid,
			 tbl.client_service_uid,
			 tbl.service_bill_per_code,
			 tbl.service_status,
			 tbl.service_bill_rate,
			 tbl.comments_text,
			 tbl.relationship_start,
			 tbl.relationship_end,
			 tbl.client_contract_uid,
			 tbl.property_uid,
			 tbl.is_active,
			 tbl.created_by,
			 tbl.created_date,
			 tbl.modified_by,
			 tbl.modified_date,
			 cst.friendly_name as serviceTypeFriendlyName,
			 csbpt.friendly_name as serviceBillPerCodeFriendlyName,
			 csst.friendly_name as serviceStatusTypeFriendlyName
			 <cfif structKeyExists(arguments, 'property_uid')>
			 ,p.friendly_name as propertyFriendlyName
			 </cfif>


			FROM dbo.Clients_ClientServices AS tbl (nolock)
					<cfif structKeyExists(arguments, 'property_uid')>
					join dbo.Properties p (nolock)
						on tbl.property_uid = p.property_uid
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid') and not structKeyExists(arguments, 'property_uid')>
					join dbo.ClientCompanies c (nolock)
						on c.client_company_uid = tbl.client_company_uid
					</cfif>

					join dbo.ClientServiceTypes cst (nolock)
						on tbl.client_service_uid = cst.service_type_uid

					join dbo.ClientServiceStatusTypes csst (nolock)
						on tbl.service_status = csst.service_status_code
						
					join dbo.ClientServiceBillPerTypes csbpt (nolock)
						on tbl.service_bill_per_code = csbpt.service_bill_per_code

			where	1=1

					<cfif structKeyExists(arguments, 'property_uid')>
						<cfif arguments.selectMethod eq 'current'>
							and p.is_active = 1
							and (
									-- Only retrieve active associations
									tbl.is_active = 1
									and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
<!---
commenting out: we want to see future services
									and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
 --->
								)
						</cfif>
						and p.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false"> )
					</cfif>
					<cfif structKeyExists(arguments, 'client_company_uid') and not structKeyExists(arguments, 'property_uid')>
					<cfif arguments.selectMethod eq 'current'>
					and c.is_active = 1
					and (
							-- Only retrieve active associations
							tbl.is_active = 1
							and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
<!---
commenting out: we want to see future services
							and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
 --->
						)
					</cfif>
					and tbl.property_uid is null
					and c.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false"> )
					</cfif>


			order
			by		<cfif structKeyExists(arguments, 'property_uid')>
					p.friendly_name,
					</cfif>
					<cfif not structKeyExists(arguments, 'property_uid') and structKeyExists(arguments, 'client_company_uid')>
					c.friendly_name,
					</cfif>
					tbl.relationship_start,
					tbl.client_service_uid

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Describes the methods used to persist / modify Client Service data. --->
	<cffunction name="saveNewClientServices"
				hint="This method is used to persist a new Client Service record to the application database."
				output="false"
				returnType="numeric"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientServices" required="true" type="mce.e2.vo.ClientServices" hint="Describes the VO containing the Client Service record that will be persisted."/>

		<!--- Handle optional fields --->
		<cfif not StructKeyExists(arguments.ClientServices, "relationship_end")>
			<cfset arguments.ClientServices.relationship_end = "">
		</cfif>


		<!--- Persist the Client Service data to the application database --->
		<cfreturn this.dataMgr.insertRecord("Clients_ClientServices", arguments.ClientServices)/>

	</cffunction>

	<cffunction name="saveExistingClientServices"
				hint="This method is used to persist an existing Client Service record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientServices" required="true" type="mce.e2.vo.ClientServices" hint="Describes the VO containing the Client Service record that will be persisted."/>

		<!--- Handle optional fields --->
		<cfif not StructKeyExists(arguments.ClientServices, "relationship_end")>
			<cfset arguments.ClientServices.relationship_end = "">
		</cfif>

		<!--- Persist the Client Service data to the application database --->
		<cfset this.dataMgr.updateRecord("Clients_ClientServices", arguments.ClientServices)/>

	</cffunction>

</cfcomponent>
