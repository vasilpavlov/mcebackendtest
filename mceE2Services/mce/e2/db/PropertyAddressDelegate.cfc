
<!---

  Template Name:  PropertyAddress
     Base Table:  PropertyAddresses	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Monday, March 30, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Monday, March 30, 2009 - Template Created.		

--->
		
<cfcomponent displayname="PropertyAddressDelegate"
				hint="This CFC manages all data access interactions for the PropertyAddresses table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyAddressComponent"
				hint="This method is used to retrieve a Property Address object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.PropertyAddress"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.PropertyAddress")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertyAddressesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Property Address objects for all the Property Addresses in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_address_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="address_type_code" required="false" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Property Addresses and build out the array of components --->
		<cfset local.qResult = getPropertyAddress(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.PropertyAddress")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getPropertyAddressAsComponent"
				hint="This method is used to retrieve a single instance of a / an Property Address value object representing a single Property Address record."
				output="false"
				returnType="mce.e2.vo.PropertyAddress"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_address_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>
		<cfargument name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="address_type_code" required="true" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Property Address and build out the component --->
		<cfset local.qResult = getPropertyAddress(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.PropertyAddress")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getPropertyAddress"
				hint="This method is used to retrieve single / multiple records from the PropertyAddresses table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_address_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="address_type_code" required="false" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Property Address records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the PropertyAddresses table matching the where clause
			select	tbl.property_address_uid,
					tbl.property_uid,
					tbl.address_type_code,
					tbl.relationship_start,
					tbl.relationship_end,
					tbl.address_line_1,
					tbl.address_line_2,
					tbl.city,
					tbl.state,
					tbl.country,
					tbl.postal_code,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					
					att.friendly_name as addressTypeFriendlyName,
					p.friendly_name as propertyFriendlyName

			from	dbo.PropertyAddresses tbl (nolock)
					join dbo.AddressTypes att (nolock) 
						on tbl.address_type_code = att.address_type_code
					join dbo.Properties p (nolock)
						on tbl.property_uid = p.property_uid

			where	1=1 
			
					<cfif arguments.selectMethod eq 'current'>												
					and att.is_active = 1
					and	p.is_active = 1
					and (

							-- Only retrieve active associations
							tbl.is_active = 1
							and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
					
						)
					</cfif>
					
					<cfif structKeyExists(arguments, 'property_address_uid')>
					and tbl.property_address_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_address_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'address_type_code')>
					and tbl.address_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.address_type_code#" null="false" list="true"> ) 
					</cfif>

			order
			by		tbl.property_uid,
					tbl.state,
					tbl.city,
					tbl.address_line_1

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Property Address data. --->	
	<cffunction name="saveNewPropertyAddress"
				hint="This method is used to persist a new Property Address record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyAddress" required="true" type="mce.e2.vo.PropertyAddress" hint="Describes the VO containing the Property Address record that will be persisted."/>

		<!--- Persist the Property Address data to the application database --->
		<cfset this.dataMgr.insertRecord("PropertyAddresses", arguments.PropertyAddress)/>

	</cffunction>

	<cffunction name="saveExistingPropertyAddress"
				hint="This method is used to persist an existing Property Address record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyAddress" required="true" type="mce.e2.vo.PropertyAddress" hint="Describes the VO containing the Property Address record that will be persisted."/>

		<!--- Persist the Property Address data to the application database --->
		<cfset this.dataMgr.updateRecord("PropertyAddresses", arguments.PropertyAddress)/>

	</cffunction>

</cfcomponent>
