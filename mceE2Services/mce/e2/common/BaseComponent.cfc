<cfcomponent displayname="Base Component"
			 output="false"
			 hint="This component is the base component for all other *.cfc's">

	<cffunction name="createUniqueIdentifierPlaceholder"
				hint="This method is used to create a new Microsoft UUID/GUID value with all zeroes."
				returntype="string"
				access="public"
				output="false">

		<!--- Return the guid value --->
		<cfreturn "00000000-0000-0000-0000-000000000000">

	</cffunction>

	<cffunction name="createUniqueIdentifier"
				hint="This method is used to create a new Microsoft UUID/GUID value."
				returntype="string"
				access="public"
				output="false">

		<!--- Return the guid value --->
		<cfreturn insert("-", CreateUUID(), 23)>

	</cffunction>

	<cffunction name="isUniqueIdentifier"
				hint="This method is used validate that a guid/unique identifier is Microsoft friendly."
				returntype="boolean"
				access="public"
				output="false">

		<!--- Define the guid argument - the unique identifier to be validated --->
		<cfargument name="guid" type="string" required="true"
					hint="defines the unique identifier to be validated as Microsoft friendly.">

		<!--- Initialize the local scope for all variables within this method --->
		<cfset var local = structnew()>

		<cfscript>

			// Initialize the validationResult variable (used to store the validation results)
			local.validationResult = true;

			// Is the length of the GUID 36 Characters?
			if( len(arguments.guid) neq 36 ){

				// No; this is not a Microsoft GUID.  Exit the function as validation failed.
				local.validationResult = false;

			// Validate that the specified GUID only contains numbers from 0-9, letters from
			// A-F, and that the GUID format required is followed.  All GUID values must follow
			// the Microsoft GUID format of 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'.

			// Were any invalid characters found in the specified guid value?
			} else if( refindnocase("^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$", arguments.guid) eq 0 ){

				// Yes; there were values in the specified GUID that were not present in the RegEx
				// used to validate Microsoft compliant GUID values.  Exit the function as validation
				// failed.

				local.validationResult = false;

			}

		</cfscript>

		<!--- Return the validation result --->
		<cfreturn local.validationResult>

	</cffunction>

	<cffunction name="queryRowToStruct"
				hint="This method is used to convert a query row to a structure."
				returntype="any"
				access="public"
				output="false">

		<!--- Default the query argument (identifies the query to transform ) --->
		<cfargument	name="query"
					hint="defines the query to transform"
					required="true"
					type="query">

		<!--- Default the row number (identifies the query row to transform ) --->
		<cfargument	name="row"
					hint="defines the query row to transform"
					required="true"
					type="numeric"
					default="1">

		<!--- Initialize the local scope for all variables within this method --->
		<cfset var local = structnew()>

		<cfscript>

			// Default the looping variable
			local.rowIndex = 1;

			// Place each of the query column in a list
			local.queryColumns = listtoArray(arguments.query.columnList);

			// Default the looplength variable
			local.loopLength = arraylen(local.queryColumns);

			// Initialize the return object
			local.queryStruct = structnew();

			// Loop over each of the columns in the query and build the structure
			for( local.rowIndex = 1; local.rowIndex lte local.loopLength; local.rowIndex = local.rowIndex + 1 ){

				// Build the structure using each column as a key/value pair
				local.queryStruct[local.queryColumns[local.rowIndex]] = arguments.query[local.queryColumns[local.rowIndex]][arguments.row];

			}

		</cfscript>

		<!--- Return the structure --->
		<cfreturn local.queryStruct>

	</cffunction>

	<cffunction name="removeMilisecondsFromDate"
				returnType="string"
				access="public"
				hint="This method is used to remove the miliseconds from a given ODBC date.">

		<!--- Define the method arguments --->
		<cfargument name="date" type="date" required="true" hint="Describes the date from which miliseconds will be removed.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Reformat the date; remove miliseconds --->
		<cfset local.newDate = dateFormat(arguments.date, 'YYYY-MM-DD') & ' ' & timeFormat(arguments.date, 'HH:MM:SS')>

		<!--- Return the date / time --->
		<cfreturn createOdbcDateTime(local.newDate)>

	</cffunction>


	<cffunction name="stringExtract" returntype="string" hint="Returns a substring of a string, based on a regular expression">
		<cfargument name="str" type="string" required="true" hint="The string to search">
		<cfargument name="pattern" type="string" required="true" hint="Regular expression to search for">
		<cfargument name="subexpressionIndex" type="numeric" default="1" required="false" hint="The subexpression index that indicates the portion of the match that you want returned. It only makes sense to use this argument if your regular expression contains set(s) of parentheses. Defaults to 1, which means that you want the whole match returned. See CF's ReFind function for details.">

		<cfset var found = REFind(pattern, str, 1, true)>

		<cfif found.pos[1] gt 0>
			<cfreturn Mid(str, found.pos[subexpressionIndex], found.len[subexpressionIndex])>
		</cfif>

		<cfreturn "">
	</cffunction>

</cfcomponent>