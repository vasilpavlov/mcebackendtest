
<!---

  Template Name:  SubclassesRateModelsMap
     Base Table:  SubclassesRateModelsMap
     


       Author:  Ilian Mihaylov
 Date Created:  Thu, May 30, 2013
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thu, May 30, 2013 - Template Created.		

--->
		
<cfcomponent displayname="SubclassesRateModelsMap"
				hint="This CFC manages all object/bean/value object instances for the SubclassesRateModelsMap table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.SubclassesRateModelsMap"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="id" required="true" type="numeric" hint="Describes the primary key "/>
	<cfproperty name="rate_class" required="true" type="string" hint="Describes the rate classes part."/>
	<cfproperty name="subclass" required="true" type="string" hint="Describes the rate subclasses part.."/>
	<cfproperty name="rate_model_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given rate model."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given rate class"/>
	<cfproperty name="modelname" required="true" type="string" hint="Describes the external / customer facing name for a given rate model"/>
	<cfproperty name="provider_type_uid" required="true" type="string" hint="Describes the primary key of the associated energy providers type ."/>
	<cfproperty name="rate_class_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given rate class."/>
	<cfproperty name="native_rate_class_code" required="true" type="string" hint="Describes the internal identifier describing the rate class code for a given rate class."/>
	<cfproperty name="energy_provider_uid" required="true" type="string" hint="Describes the primary key of the associated energy provider."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="id" hint="Describes the primary key for the current object / table.">


	<!--- Default any required properties. --->
	<cfset this.primaryKey = "id"/>
	<cfset this.id = 0/>
	
</cfcomponent>
