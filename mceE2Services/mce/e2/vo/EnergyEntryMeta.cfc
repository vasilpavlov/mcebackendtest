<cfcomponent output="false"
				alias="mce.e2.vo.EnergyEntryMeta"
				style="rpc"
				extends="BaseValueObject">

	<!--- ToDo: [ADL] Modify this component (vo.EnergyEntryMeta.cfc) to follow the formatting / commenting
				standards that the other service components are using. --->

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="usage_meta_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given energy usage record."/>
	<cfproperty name="entry_status" type="string" hint="Describes the energy entry status for a given meta value / property.">
	<cfproperty name="friendly_name" type="string" hint="Describes the external / customer facing friendly name for a given energy entry meta type.">
	<cfproperty name="meta_type_uid" type="string" hint="Describes the primary key / unique identifier for a given energy entry meta type.">
	<cfproperty name="meta_value" type="string" hint="Describes the value for a given energy entry meta property.">
	<cfproperty name="meta_value_before_edit" type="string" hint="Describes the value for a given energy entry meta property, before editing.">
	<cfproperty name="type_lookup_code" type="string" hint="Describes the lookup code for a given energy entry meta  type.">
	<cfproperty name="data_type" type="string" required="true" hint="Describes the data type for a given energy usage meta type.">
	<cfproperty name="meta_group_uid" type="string" hint="Describes the primary key / unique identifier for a given energy entry meta type group.">

	<!--- Computed fields which should come back to server on updates; these should also exist in corresonding actionscript class --->
	<cfproperty name="priorValues" type="array">
	
	<cfproperty name="primaryKey" required="true" type="string" default="usage_meta_uid" hint="Describes the primary key for the current object / table.">

	<!--- New Properties added as part of the UBS Specific requirements --->
	<cfproperty name="energy_unit_uid" type="string" required="false" hint="Describes the primary key / unique identifier for a given energy unit record."/>
	<cfproperty name="energyUnitFriendlyName" type="string" required="true" hint="Describes the friendly name for the associated energy unit record.">
	<cfproperty name="conversionEnergyUnitFriendlyName" type="string" required="true" hint="Describes the friendly name for the associated energy unit that will be used during energy conversion activities.">
	<cfproperty name="conversion_factor" type="numeric" required="true" hint="Describes the factor / multiple to be applied when converting energy units.">
	<cfproperty name="is_cost_component" type="boolean" required="true" hint="Describes whether a given meta type is an energy cost component.">
	<cfproperty name="is_usage_component" type="boolean" required="true" hint="Describes whether a given meta type is an energy usage component.">
	<cfproperty name="is_system_calculated" type="boolean" required="true" hint="Describes whether a given meta type should be system-calculated.">

	<!--- Additional fields to assist UI, but which do not need to come back to server on updates; these should exist in actionscript class, but should be marked [Transient] there so the Flex app doesn't bother sending them back to the server --->
	<cfproperty name="is_required" type="boolean">
	<cfproperty name="consider_touched" type="boolean">
	<cfproperty name="variance_enabled" type="boolean">
	<cfproperty name="tolerance_error" type="numeric">
	<cfproperty name="tolerance_warning" type="numeric">
	<cfproperty name="type_group_lookup_code" type="string" required="false" hint="represents the meta type lookup code to group values for">
	<cfproperty name="unit_conversion_enabled" type="boolean">
	<cfproperty name="hasPermissionToEdit" type="boolean">
	<cfproperty name="metaChoices" type="array">
	<cfproperty name="numMetaChoices" type="numeric">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "usage_meta_uid"/>
	<cfset this.usage_meta_uid = ""/>
	
</cfcomponent>