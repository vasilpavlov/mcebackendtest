
<!---

  Template Name:  EnergyAccountSummaryForDataAnalysis
     Base Table:  EnergyAccounts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Sunday, March 22, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Monday, May 11, 2009 - Template Created.

--->

<cfcomponent displayname="EnergyAccountSummaryForDataAnalysis"
				hint="This CFC manages all object/bean/value object instances for energy account summaries used specifically for data analysis."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyAccountSummaryForDataAnalysis"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated energy type definition."/>
	<cfproperty name="energy_unit_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated energy type definition."/>
	<cfproperty name="native_account_number" required="true" type="string" hint="Describes the external / native account number for a given energy account.">
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated property definition.">
	<cfproperty name="master_energy_accountuid" required="false" type="string"/>
	<cfproperty name="account_status" required="true" type="string" hint="Energy Account Status"/>
	<cfproperty name="status_date" type="date" required="true"  hint="Describes the date account status was changed."/>
	
	<!--- Declare the system calculated / defined properties --->
	<cfproperty name="energyAccountFriendlyName" required="true" type="string" hint="Describes the customer facing / external name for a given energy account."/>
	<cfproperty name="propertyFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated property."/>
	<cfproperty name="energyTypeFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy account type."/>
	<cfproperty name="energyProviderFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy type."/>
	<cfproperty name="energyUnitFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy unit."/>
	<cfproperty name="energyUsageAvailableFrom" required="true" type="date" hint="Describes the start date of the associated energy usage period."/>
	<cfproperty name="energyUsageAvailableTo" required="true" type="date" hint="Describes the end date of the associated energy usage period."/>
	<cfproperty name="numEnergyUsageRecords" required="true" type="date" hint="Describes the end date of the associated energy usage period."/>
	<cfproperty name="budgetAvailableFrom" required="true" type="date" hint="Describes the start date of the associated budget period."/>
	<cfproperty name="budgetAvailableTo" required="true" type="date" hint="Describes the end date of the associated budget period."/>

	<!--- Default properties --->
	<cfset this.budgetAvailableFrom = '2008/01/01 00:00:00'>
	<cfset this.budgetAvailableTo = now()>

</cfcomponent>