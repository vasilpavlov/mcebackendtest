
<!---

  Template Name:  AddressType
     Base Table:  AddressTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 01, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 01, 2009 - Template Created.		

--->
		
<cfcomponent displayname="AddressType"
				hint="This CFC manages all object/bean/value object instances for the AddressTypes table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.AddressType"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="address_type_code" required="true" type="string" hint="Describes the address type code for a given address."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the friendly / customer facing name for a given address type."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="address_type_code" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "address_type_code"/>
	<cfset this.address_type_code = ""/>

</cfcomponent>
