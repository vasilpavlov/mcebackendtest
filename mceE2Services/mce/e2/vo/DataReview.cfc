
<!---

  Template Name:  DataReview
     Base Table:  dataReviews	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, April 28, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, April 28, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReview"
				hint="This CFC manages all object/bean/value object instances for the dataReviews table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataReview"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="data_review_uid" required="true" type="string" hint="Describes the primary key / unique identifier fora  given data review record."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the friendly name for a given emissions data review."/>
	<cfproperty name="review_period_start" required="true" type="date" hint="Describes the start date for a given review period."/>
	<cfproperty name="review_period_end" required="true" type="date" hint="Describes the end date for a given review period."/>
	<cfproperty name="display_period_start" required="true" type="date" hint="Describes the start date for a given review display period."/>
	<cfproperty name="display_period_end" required="true" type="date" hint="Describes the end date for a given review display period."/>
	<cfproperty name="display_property_meta_type_lookup_code" required="true" type="string" hint="Describes the lookup code for the associated property meta type."/>
	<cfproperty name="review_namespace_code" required="true" type="string" hint="Describes the date review namespace lookup code."/>
	<cfproperty name="detail_records_length_datepart" required="true" type="string" hint="Describes the date rage lookup code for data review detail records (ex. week, quarter, year)."/>
	<cfproperty name="detail_records_length" required="true" type="numeric" hint="Describes the length / limit of detail records."/>
	<cfproperty name="first_comparison_offset_months_start" required="true" type="numeric" hint="Describes the month offset for the first data review / comparison window. "/>
	<cfproperty name="first_comparison_offset_months_length" required="true" type="numeric" hint="Describes the length of the first comparison from the related / specified offset."/>
	<cfproperty name="second_comparison_offset_months_start" required="true" type="numeric" hint="Describes the month offset for the second data review / comparison window. "/>
	<cfproperty name="second_comparison_offset_months_length" required="true" type="numeric" hint="Describes the length of the second comparison from the related / specified offset."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>
	<cfproperty name="profile_lookup_code" required="false" type="string" hint="Describes the energy profile lookup code used to retrieve energy hierarchy information."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="data_review_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "data_review_uid"/>
	<cfset this.data_review_uid = ""/>

</cfcomponent>
