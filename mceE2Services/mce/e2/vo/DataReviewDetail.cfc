
<!---

  Template Name:  DataReviewDetail
     Base Table:  DataReviewDetail	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, May 08, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, May 08, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReviewDetail"
				hint="This CFC manages all object/bean/value object instances for the DataReviewDetail table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataReviewDetail"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="review_detail_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review detail."/>
	<cfproperty name="data_review_uid" required="true" type="string" hint="Describes the primary key / unique identifier of an associated data review."/>
	<cfproperty name="period_name" required="true" type="string" hint="Describes the external / customer facing name of a given data review detail period."/>
	<cfproperty name="detail_period_start" required="true" type="date" hint="Describes the start date for a given data review detail period."/>
	<cfproperty name="detail_period_end" required="true" type="date" hint="Describes the end date for a given data review detail period."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the property associated to a given data review detail."/>
	<cfproperty name="approval_status_date" required="true" type="date" hint="Describes the date for a given approval state."/>
	<cfproperty name="approval_status_code" required="true" type="string" hint="Describes the lookup code for a given approval status."/>
	<cfproperty name="approval_status_notes" required="true" type="string" hint="Describes any notes associated to an approval status."/>
	<cfproperty name="is_current_review_detail" required="true" type="boolean" hint="Describes whether a given data review detail record is the active / primary review record for a given parent data review."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="review_detail_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "review_detail_uid"/>
	<cfset this.review_detail_uid = ""/>

</cfcomponent>
