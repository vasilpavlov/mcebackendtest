		
<cfcomponent displayname="EPABuilding"
				hint="This CFC manages all object/bean/value object instances for the EPABuildings table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EPABuilding"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="pmCustID" required="true" type="string" hint="Describes the customer portfolio manager id."/>
	<cfproperty name="custOrgName" required="true" type="string" hint="Describes the customer organization name."/>
	<cfproperty name="bldgName" required="true" type="string" hint="Describes the customer organization name."/>
	<cfproperty name="pmBldgID" required="true" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
	<cfproperty name="bldgAddress" required="true" type="string" hint="Describes the first address line for a given address."/>
	<cfproperty name="bldgCity" required="true" type="string" hint="Describes the city associated to a given address."/>
	<cfproperty name="bldgState" required="true" type="string" hint="Describes the state / province associated to a given address."/>
	<cfproperty name="bldgZipCode" required="true" type="string" hint="Describes the country associated to a given address."/>
	<cfproperty name="bldgYearBuilt" required="true" type="string" hint="Describes the postal code / zip code associated to a given address."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="pmBldgID" hint="Describes the primary key for the current object / table.">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "pmBldgID"/>
	<cfset this.pmBldgID = ""/>

</cfcomponent>
