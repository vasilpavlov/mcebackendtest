
<!---

  Template Name:  EnergyUsageforReviewDetail
     Base Table:  Energy Usage, Data Review	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, April 28, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, April 28, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyUsageforReviewDetail"
				hint="This CFC manages all object/bean/value object instances for energy usage review detail data, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyUsageforReviewDetail"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_usage_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy usage record."/>
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy account record."/>
	<cfproperty name="user_comments" required="false" type="string" hint="Describes the user comments as provided by review users for a given energy usage record."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given property record."/>
	<cfproperty name="period_start" required="false" type="date" hint="Describes the start date for a given energy usage record.">
	<cfproperty name="period_end" required="false" type="date" hint="Describes the end date for a given energy usage record.">

	<!--- Derrived properties / Friendly Names --->
	<cfproperty name="propertyFriendlyName" required="true" type="string" hint="Describes the friendly name for a given property record."/>
	<cfproperty name="energyTypeFriendlyName" required="true" type="string" hint="Describes the friendly name for a given energy type record."/>
	<cfproperty name="energyAccountFriendlyName" required="true" type="string" hint="Describes the friendly name for a given energy account record."/>
	<cfproperty name="dataReviewPeriodName" required="true" type="string" hint="Describes the period name for a given data review detail record."/>
	<cfproperty name="displayPropertyMetaTypeLookupCode" required="true" type="string" hint="Describes the review data detail meta property lookup code associated to a given data review."/>
	<cfproperty name="displayPropertyMetaValue" required="true" type="string" hint="Describes the review data detail meta property value associated to a given property."/>
	<cfproperty name="conversionEnergyUnitFriendlyName" required="true" type="string" hint="Describes the energy unit that the source energy unit type will be converted to."/>
	<cfproperty name="conversion_factor" required="true" type="numeric" hint="Describes the conversion factor to be applied when converting a given energy unit from one type to another."/>
	
</cfcomponent>