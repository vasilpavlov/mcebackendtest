	
<cfcomponent displayname="ClientServiceStatusType"
				hint="This CFC manages all object/bean/value object instances for the ClientServiceStatusTypes table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.ClientServiceStatusType"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="service_status_code" required="true" type="string" hint="Describes the primary key / unique identifierfor a given client service type."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given client service type."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="service_status_code" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>
	
	<!--- Default any required properties. --->
	<cfset this.primaryKey = "service_status_code"/>
	<cfset this.service_status_code = ""/>

</cfcomponent>
