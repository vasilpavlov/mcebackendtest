
<!---

  Template Name:  PropertyClientContactAssociation
     Base Table:  Properties_ClientContacts	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 02, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 02, 2009 - Template Created.		

--->
		
<cfcomponent displayname="PropertyClientContactAssociation"
				hint="This CFC manages all object/bean/value object instances for the Properties_ClientContacts table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.PropertyClientContactAssociation"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="relationship_id" required="true" type="numeric" hint="Describes the primary key / unique identifier for a given property / client contact association."/>
	<cfproperty name="external_identifier" required="true" type="numeric" hint="Describes the external identifier for a given client contact."/>
	<cfproperty name="client_company_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given client company."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given / associated property."/>
	<cfproperty name="client_contact_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given / associated client contact."/>
	<cfproperty name="contact_role_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given / associated contact role."/>
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the start date enforcing when the relationship between a given property / client contact / contact role is enforced."/>
	<cfproperty name="relationship_end" required="true" type="date" hint="Describes the end date enforcing when the relationship between a given property / client contact / contact role will no longer be enforced."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="propertyFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for an associated property."/>
	<cfproperty name="contactRoleFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for a given property associated client contact role."/>
	<cfproperty name="clientCompanyFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for a given property associated client contact role."/>
	<cfproperty name="first_name" required="false" type="string" hint="Describes the first name of the client contact associated to a given property."/>
	<cfproperty name="last_name" required="false" type="string" hint="Describes the last name of the client contact associated to a given property."/>
	<cfproperty name="email" required="true" type="string" hint="Describes the email address of the client contact."/>
	<cfproperty name="phone" required="true" type="string" hint="Describes the phone number of the client contact."/>
	<cfproperty name="comments" required="true" type="string" hint="Describes the comments for the client contact."/>
	<cfproperty name="primaryKey" required="true" type="string" default="relationship_id" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "relationship_id"/>
	<cfset this.relationship_id = 0/>

</cfcomponent>
