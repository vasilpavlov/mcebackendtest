
<cfcomponent displayname="EnergyContractsMeta"
				hint="This CFC manages Energy Contract Meta info"
				output="false" 
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyContractsMeta" 
				style="rpc">


	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_contract_meta_uid"  required="true"  type="string" hint="x"/>
	<cfproperty name="friendly_name" type="string" hint="Describes the external / customer facing friendly name for a given energy contract meta type."/>
	<cfproperty name="meta_type_uid"  required="false"  type="string" hint="x"/>
	<cfproperty name="meta_value"  required="true"  type="string" hint="x"/>
	<cfproperty name="type_lookup_code" type="string" hint="Describes the lookup code for a given energy contract meta  type."/>
	<cfproperty name="meta_group_uid" type="string" hint="Describes the primary key / unique identifier for a given energy contract meta type group."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>
	<cfproperty name="energyContractFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated Energy Contract">
	<cfproperty name="energy_contract_uid"  required="false"  type="string" hint="x"/>
	<cfproperty name="primaryKey" required="true" type="string" default="energy_contract_meta_uid" hint="Describes the primary key for the current object / table.">
		
	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Additional fields to assist UI, but which do not need to come back to server on updates; these should exist in actionscript class, but should be marked [Transient] there so the Flex app doesn't bother sending them back to the server --->
	<cfproperty name="metaChoices" type="array">
	<cfproperty name="numMetaChoices" type="numeric">
	<cfproperty name="data_type" type="int" hint="Describes the data type for a given energy usage meta type.">
	<cfproperty name="meta_value_before_edit" type="string" hint="Describes the value for a given energy entry meta property, before editing.">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_contract_meta_uid"/>
	<cfset this.energy_contract_meta_uid = ""/>

</cfcomponent>