<cfcomponent displayname="ClientNote"
				hint="This CFC manages all object/bean/value object instances for the ClientNotes table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.ClientNote"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="client_note_uid" required="true" type="string" hint="This column represents the primary key / unique identifier for each note."/>
	<cfproperty name="client_company_uid" required="true" type="string" hint="Describes the primary key / unique identifier for an associated client company."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for an associated property."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / client facing name for a given note."/>
	<cfproperty name="note_text" required="true" type="string" hint="Describes the contents of the note for a record."/>
	<cfproperty name="reminder_date" required="true" type="date" hint="Describes the reminder date for a note."/>
	<cfproperty name="completed_date" required="true" type="date" hint="Describes the date that a record was completed."/>
	<cfproperty name="completed_user_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the user who completed this note."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="client_note_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the computed columns for this component --->

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "client_note_uid"/>
	<cfset this.client_note_uid = ""/>

</cfcomponent>