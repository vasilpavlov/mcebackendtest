<cfcomponent displayName="Rate Model Input Set Value Object"
			 output="false"
			 alias="mce.e2.vo.RateModelInputSet" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given rate model input set.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="input_set_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate model input set definition.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the user-facing name for a particular rate model input set.">
	<cfproperty name="set_type_code" type="string" required="true" hint="Describes the code used to group rate model input sets.">

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="input_set_uid" hint="Describes the property that acts as the primary key for a given rate model input set.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.primaryKey = "">
	<cfset this.primaryKey = "input_set_uid">

</cfcomponent>
