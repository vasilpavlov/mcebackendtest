
<!---

  Template Name:  EnergyType
     Base Table:  EnergyTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyBudgetType"
				hint="This CFC manages all object/bean/value object instances for the EnergyBudgetTypes table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyBudgetType"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="budget_type_code" required="true" type="string" hint="Describes the primary key / unique identifierfor a given energy budget type."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given energy budget type."/>
	<cfproperty name="permission_required_to_assign" required="true" type="string" hint="Describes the permission required to assign for a given energy budget type."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given budget type record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given budget type record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given budget type record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given budget type record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given budget type record was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_type_uid"/>
	<cfset this.energy_type_uid = ""/>

</cfcomponent>
