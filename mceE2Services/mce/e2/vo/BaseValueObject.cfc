<cfcomponent displayName="Base Value Object"
			 output="false"
			 hint="This component is the base value component for all value objects.">


	<cffunction name="compareProperties"
				returntype="boolean"
				hint="This method is used to compare properties between the current value object and another value object.  This method will return true if properties match; false if properties do not match.">

		<!--- Define the arguments for this method --->
		<cfargument name="objectToCompare" type="any" required="true" hint="Describes the struct / vo whose properties are being compared to the current /active vo / component.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the output variable --->
		<cfset local.output = true>

		<!--- Loop over the properties in the present component --->
		<cfloop collection="#this#" item="local.thisKey">

			<!--- Was the property found in the target object? --->
			<cfif not structKeyExists(arguments.objectToCompare, local.thisKey)>

				<!--- If not, set the output value and exit the function --->
				<cfset local.output = false>
				<cfbreak>

			</cfif>

		</cfloop>

		<!--- Return the output variable --->
		<cfreturn local.output>

	</cffunction>

</cfcomponent>