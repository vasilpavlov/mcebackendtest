<cfcomponent displayName="Property Collection Property, Alert, and ToDo Count Value Object"
			 output="false"
			 alias="mce.e2.vo.PropertyCollectionPropertyAlertAndToDoCounts" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the properties of a given property collection as well as its property, alert, and to do count values.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given property collection.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given property collection.">
	<cfproperty name="image_filename" type="string" required="true" hint="Describes the image file name for a given property collection.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="propertyCount" type="numeric" required="true" default="false" hint="Describes the total number of properties associated to the specified proprty collection."/>
	<cfproperty name="alertCount" type="numeric" required="true" default="false" hint="Describes the total number of standard alerts associated to the properties associated to the specified property collection."/>
	<cfproperty name="todoCount" type="numeric" required="true" default="false" hint="Describes the total number of to do alerts associated to the properties associated to the specified property collection."/>
	<cfproperty name="propertyItems" type="array" required="true" default="false" hint="list of property objects"/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

</cfcomponent>