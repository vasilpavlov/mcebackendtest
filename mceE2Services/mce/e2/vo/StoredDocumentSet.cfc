
<!---

  Template Name:  StoredDocumentSet
     Base Table:  StoredDocumentSet	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 16, 2009 - Template Created.		

--->
		
<cfcomponent displayname="StoredDocumentSet"
				hint="This CFC manages all object/bean/value object instances for the StoredDocumentSet table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.StoredDocumentSet"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="document_set_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given document set."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the customer facing / friendly name of the document set."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="document_set_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define any associated objects for a given value object. --->
	<cfproperty name="StoredDocuments" required="true" type="array" default="false" hint="Describes the collection of stored documents associated to a given document set.">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "document_set_uid"/>
	<cfset this.document_set_uid = ""/>

</cfcomponent>
