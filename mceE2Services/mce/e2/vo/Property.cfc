<cfcomponent displayName="Property"
			 output="false"
			 alias="mce.e2.vo.Property"
			 style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given property.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="property_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given property.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given property.">
	<cfproperty name="image_filename" type="string" required="true" hint="Describes the image file name associated to a given property.">
	<cfproperty name="oldId" type="numeric" required="false" hint="Describes the external / legacy application primary key for this data (used to migrate data from v1 to v2).">
	<cfproperty name="document_set_uid" type="string" required="false" hint="Describes primary key / unique identifier for the document set assigned to this property."/>
	<cfproperty name="pmBldgID" type="string" required="false" hint="this represents the EPA Portfolio Manager Building ID.">
	
	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="propertyCollectionUid" type="string" required="false" hint="Describes the primary key of the property collection to which a given property is associated to.">
	<cfproperty name="energyAccountCount" type="numeric" required="false" hint="Describes the count of energy accounts associated to a given property.">
	<cfproperty name="relationshipStart" type="date" required="false" hint="Describes the start date of a given property based on its relationship to a property collection.">
	<cfproperty name="relationshipEnd" type="date" required="false" hint="Describes the end date of a given property based on its relationship to a property collection.">

	<!--- Define any associations that can exist for a given property --->
	<cfproperty name="metaDataArray" type="array" required="false" hint="Describes the meta data associated to a given property.">
	<cfproperty name="energyAccountArray" type="array" required="false" hint="Describes the array of energy accounts that are associated to a given property.">
	<cfproperty name="clientCompanyFriendlyName" type="string" required="false" hint="this represents the client company friendly_name for the property.">
	<cfproperty name="propertyAddressString" type="string" required="false" hint="this represents the property address formatted as a string.">
	
	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="property_uid" hint="Describes the property that acts as the primary key for a given property.">
	<cfproperty name="client_company_uid" type="string" required="false" hint="Describes the primary key for a given Client Company Association">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.property_uid = "">
	<cfset this.primaryKey = "property_uid">

</cfcomponent>