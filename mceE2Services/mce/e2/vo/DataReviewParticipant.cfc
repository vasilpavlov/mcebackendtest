
<!---

  Template Name:  DataReviewParticiant
     Base Table:  dataReviewParticipants	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 22, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 22, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReviewParticipant"
				hint="This CFC manages all object/bean/value object instances for the dataReviewParticipants table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataReviewParticipant"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="review_participant_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data participant review record."/>
	<cfproperty name="parent_participant_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data participant's parent record."/>
	<cfproperty name="data_review_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
	<cfproperty name="client_contact_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given client contact record."/>
	<cfproperty name="property_collection_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given property collection record."/>
	<cfproperty name="review_role_code" required="true" type="string" hint="Describes the lookup code representing the associated review role."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="data_review_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Derrived properties --->
	<cfproperty name="reviewRoleFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated review role."/>
	<cfproperty name="reviewRoleOwnershipLevel" required="true" type="numeric" hint="Describes the ownership level associated to a given review role."/>
	<cfproperty name="participantFirstName" required="true" type="string" hint="Describes the first name of the data review participant."/>
	<cfproperty name="participantLastName" required="true" type="string" hint="Describes the last name of the data review participant."/>

	<!--- Associated objects --->
	<cfproperty name="propertyArray" required="true" type="array" hint="Describes the collection of properties associated to a given data review.">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "review_participant_uid"/>
	<cfset this.review_participant_uid = ""/>

</cfcomponent>
