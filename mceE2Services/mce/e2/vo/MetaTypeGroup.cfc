
<!---

  Template Name:  MetaTypeGroup
     Base Table:  MetaTypeGroups	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="MetaTypeGroup"
				hint="This CFC manages all object/bean/value object instances for the MetaTypeGroups table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.MetaTypeGroup"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="meta_group_uid" required="true" type="string" hint="Describes the primary key for a given meta type group."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given meta type group."/>
	<cfproperty name="group_lookup_code" required="true" type="string" hint="Describes the internal identifier describing a given meta type group."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="meta_group_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "meta_group_uid"/>
	<cfset this.meta_group_uid = ""/>

</cfcomponent>
