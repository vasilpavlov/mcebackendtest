
<!---

  Template Name:  EmissionsFactorAlias
     Base Table:  EmissionsFactorAliases	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Sunday, March 22, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Sunday, March 22, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EmissionsFactorAlias"
				hint="This CFC manages all object/bean/value object instances for the EmissionsFactorAliases table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EmissionsFactorAlias"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="factor_alias_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given emission factor alias."/>
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the energy type associated to a given emission factor alias."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given emissions factor alias."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="factor_alias_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "factor_alias_uid"/>
	<cfset this.factor_alias_uid = ""/>

</cfcomponent>
