<cfcomponent displayName="User Value Object"
			 output="false"
			 alias="mce.e2.vo.User" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given user.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="client_company_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given client company to which each user group is associated to.">
	<cfproperty name="user_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given user.">
	<cfproperty name="first_name" type="string" required="true" hint="Describes the first name of a given user.">
	<cfproperty name="last_name" type="string" required="true" hint="Describes the last name of a given user.">
	<cfproperty name="username" type="string" required="true" hint="Describes the login / username for a given user.">
	<cfproperty name="alert_email" type="string" required="true" hint="Describes the email address used for alerting for a given user.">
	<cfproperty name="password_hash_method" type="numeric" required="true" default="1" hint="Describes the password hash method used to encrypt a given user's password.">
	<cfproperty name="relationship_id" type="numeric" required="false" hint="Describes the primary key of the association record between a user and another / associated object (ex. userGroup, propertyCollection, etc)."/>
	<cfproperty name="energy_provider_uid" type="string" required="false" hint="If the user represents a provider for data entry purposes (vendor entry scenario), this is the provider."/>

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="user_uid" hint="Describes the property that acts as the primary key for a given user.">

	<!--- Set any defaults --->
	<cfset this.user_uid = "">
	<cfset this.primaryKey = "user_uid">
	<cfset this.password_hash_method = 1>

	<!--- Define which fields are required --->
	<cfset this.requiredProperties = structNew()>

	<!--- Specify which properties are required / will be validated --->
	<cfset this.requiredProperties.client_company_uid = "">
	<cfset this.requiredProperties.first_name = "">
	<cfset this.requiredProperties.last_name = "">
	<cfset this.requiredProperties.username = "">

</cfcomponent>