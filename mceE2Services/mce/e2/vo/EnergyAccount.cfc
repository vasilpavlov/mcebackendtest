
<!---

  Template Name:  EnergyAccount
     Base Table:  EnergyAccounts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Sunday, March 22, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Sunday, March 22, 2009 - Template Created.

--->

<cfcomponent displayname="EnergyAccount"
				hint="This CFC manages all object/bean/value object instances for the EnergyAccounts table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyAccount"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
	<cfproperty name="native_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for an associated energy native account definition."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the customer facing / external name for a given energy account."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
	<cfproperty name="account_type_uid" required="true" type="string" hint="Describes the primary key of the enery account type associated to a given energy account."/>
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key of the enery type associated to a given energy account."/>
	<cfproperty name="meta_group_uid" required="true" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>
	<cfproperty name="energy_unit_uid" required="true" type="string" hint="Describes the primary key of the energy unit associated to a given energy account."/>
	<cfproperty name="factor_alias_uid_override" required="true" type="string" hint="Describes the primary key of the energy factor alias used to override a given energy account."/>
	<cfproperty name="master_energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>
	<cfproperty name="notes_text" required="true" type="string" hint="Simple account-level notes (in the db as varchar(max))"/>
	<cfproperty name="account_status" required="true" type="string" hint="Energy Account Status"/>
	<cfproperty name="status_date" type="date" required="true"  hint="Describes the date account status was changed."/>

	<!--- Define the secondary / associated properties. --->
	<cfproperty name="native_account_number" required="true" type="string" hint="Describes the account number associated to the energy native account tied to a given energy account."/>
	<cfproperty name="energy_provider_uid" required="true" type="string" hint="Describes the primary key of the energy provider associated to a given energy account."/>
	<cfproperty name="rate_class_uid" required="true" type="string" hint="Describes the primary key of the rate class associated to a given energy account."/>
	<cfproperty name="rate_model_uid" required="true" type="string" hint="Describes the primary key of the rate model associated to a given energy account."/>
	<cfproperty name="is_delivery_based" required="true" type="string" hint="Whether the associated energy type is delivery based"/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="energy_account_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the friendly names --->
	<cfproperty name="friendlyEnergyAccountName" required="true" type="string" hint="Describes the customer facing / external name for a given energy account."/>
	<cfproperty name="friendlyMasterEnergyAccountName" required="true" type="string" hint="Describes the customer facing / external name for a given master / parent energy account."/>
	<cfproperty name="friendlyPropertyName" required="true" type="string" hint="Describes the customer facing / external name for a given property."/>
	<cfproperty name="friendlyEnergyAccountTypeName" required="true" type="string" hint="Describes the customer facing / external name for a given energy account type."/>
	<cfproperty name="friendlyEnergyTypeName" required="true" type="string" hint="Describes the customer facing / external name for a given energy type."/>
	<cfproperty name="friendlyMetaTypeGroupName" required="true" type="string" hint="Describes the customer facing / external name for a given meta type group."/>
	<cfproperty name="friendlyEnergyUnitName" required="true" type="string" hint="Describes the customer facing / external name for a given energy unit."/>
	<cfproperty name="friendlyEnergyProviderName" required="true" type="string" hint="Describes the customer facing / external name for a given energy provider."/>
	<cfproperty name="friendlyRateClassName" required="true" type="string" hint="Describes the customer facing / external name for a given rate class."/>
	<cfproperty name="friendlyFactorAliasName" required="true" type="string" hint="Describes the customer facing / external name for a given rate factor alias."/>
	<cfproperty name="friendlyRateModelName" required="true" type="string" hint="Describes the customer facing / external name for a given rate model."/>

	<!--- Define the association id values --->
	<cfproperty name="energyNativeAccountEnergyProviderRateClass_relId" required="false" type="numeric" hint="Describes the primary key of the energy native account / energy provider rate class association.">
	<cfproperty name="energyProviderRateClassRateModels_relId" required="false" type="numeric" hint="Describes the primary key of the energy provider rate class / rate model association.">

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_account_uid"/>
	<cfset this.energy_account_uid = ""/>

</cfcomponent>
