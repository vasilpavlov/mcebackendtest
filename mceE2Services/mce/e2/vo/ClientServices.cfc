<cfcomponent displayname="ClientServices" 
				hint="This CFC manages all object/bean/value object instances for the Clients_ClientServices tables, and is used to pass object instances through the application service layer." 
				output="false" 
				extends="BaseValueObject" 
				alias="mce.e2.vo.ClientServices" 
				style="rpc" >

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="relationship_id"  required="true"  type="numeric" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="client_company_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="client_service_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="service_bill_per_code" required="true" type="string" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="service_bill_rate" required="true" type="string" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="relationship_end" required="false" type="date" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="client_contract_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="service_status" required="true" type="string" hint="Describes the primary key / unique identifier for a given client service definition." />
	<cfproperty name="comments_text" required="false" type="string" hint="Free-text comments" />
	
	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey"  required="true"  type="string"  default="relationship_id"  hint="Describes the primary key for the current object / table." >
	<cfproperty name="isAssociated"  required="true"  type="boolean"  default="false"  hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)." />
	<cfproperty name="serviceTypeFriendlyName" required="false" type="string" hint="Describes the friendly name for a given client service." />
	<cfproperty name="serviceBillPerCodeFriendlyName" required="false" type="string" hint="Describes the friendly name for a given service_bill_per_code." />
	<cfproperty name="serviceStatusTypeFriendlyName" required="false" type="string" hint="Describes the friendly name for a given service_status_code." />
	
	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active"  required="true"  type="boolean"  hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)." />
	<cfproperty name="created_by"  required="true"  type="string"  hint="Describes the system user that created a  given rate model record." />
	<cfproperty name="created_date"  required="true"  type="date"  hint="Describes the date that a given rate model record was first created." />
	<cfproperty name="modified_by"  required="true"  type="string"  hint="Describes the system user that last modified a given rate model record." />
	<cfproperty name="modified_date"  required="true"  type="date"  hint="Describes the date that a given rate model record was last modified." />

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "relationship_id"/>
	<cfset this.relationship_id = 0/>

</cfcomponent>