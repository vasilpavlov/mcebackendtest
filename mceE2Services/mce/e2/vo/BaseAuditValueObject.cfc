<cfcomponent displayName="Base Audit Value Object"
			 output="false"
			 extends="BaseValueObject"
			 hint="This component is the base audit value object / component for all value objects.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

</cfcomponent>