
<!---

  Template Name:  Alert
     Base Table:  Alerts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, April 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, April 16, 2009 - Template Created.

--->

<cfcomponent displayname="Alert"
				hint="This CFC manages all object/bean/value object instances for the Alerts table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="alert_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given alert."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key of the property associated to a given alert."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name of a given alert."/>
	<cfproperty name="alert_type_code" required="true" type="string" hint="Describes the type code / internal identifier for a given alert."/>
	<cfproperty name="alert_status_code" required="true" type="string" hint="Describes the internal identifier / status code for a given alert."/>
	<cfproperty name="alert_generated_date" required="true" type="date" hint="Describes the last date that an alert was generated."/>
	<cfproperty name="alert_due_date" required="true" type="date" hint="Describes the optional date when a given alert is due."/>
	<cfproperty name="alert_visibility_date" required="true" type="date" hint="Describes the date when a given alert should be visible."/>
	<cfproperty name="foreign_key_uid" required="true" type="string" hint="Describes the foreign key for a given alert."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>
	<cfproperty name="energy_account_uid" required="true" type="string" hint="This is optional and will not always be populated, depending on the alert type"/>
	<cfproperty name="alert_location_notes" required="true" type="string" hint="This is optional and will not always be populated, depending on the alert type"/>
	<cfproperty name="alert_subject_notes" required="true" type="string" hint="This is optional and will not always be populated, depending on the alert type"/>
	<cfproperty name="alert_context_notes" required="true" type="string" hint="This is optional and will not always be populated, depending on the alert type"/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="alert_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>
	<cfproperty name="alertTypeFriendlyName" required="true" type="string" hint="Describes the customer facing name for a given alert type.">
	<cfproperty name="alertTypeGroupFriendlyName" required="true" type="string" hint="Describes the customer facing name for a given alert group type.">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "alert_uid"/>
	<cfset this.alert_uid = ""/>

</cfcomponent>
