
<!---

  Template Name:  EnergyType
     Base Table:  EnergyTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyBudget"
				hint="This CFC manages all object/bean/value object instances for the EnergyBudgets table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyBudget"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_budget_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given energy budget record."/>
	<cfproperty name="budget_type_code" required="true" type="string" hint="Describes the primary key / unique identifier of the budget type for a given energy budget record."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given energy budget."/>
	<cfproperty name="budget_start" required="true" type="date" hint="Describes the start date for a given budget."/>
	<cfproperty name="budget_end" required="true" type="date" hint="Describes the end date for a given budget."/>
	<cfproperty name="client_company_uid" required="true" type="string" hint="Describes the primary key of the related / associated client company."/>
	<cfproperty name="permission_required_to_edit" required="true" type="string" hint="Describes the permission required to edit for a given energy budget."/>
	<cfproperty name="comment_text" type="string" required="false" hint="Describes any budget comments recorded.">
	<cfproperty name="is_locked" type="boolean" required="false" hint="Describes if the budget is locked">

	<!--- Computed fields which should come back to server on updates; these should also exist in corresonding actionscript class --->

	<cfproperty name="associatedEnergyAccounts" type="array" hint="Describes the collection of associated energy accounts, retrieved from db.">
	<cfproperty name="energyAccounts" type="array" hint="Describes the collection of energy accounts associated to a given energy budget - for saving">
	<cfproperty name="energyUsage" type="array" hint="Describes the collection of energy info objects associated to a given energy budget.">


	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given budget type record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given budget type record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given budget type record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given budget type record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given budget type record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="energy_budget_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_budget_uid"/>
	<cfset this.energy_budget_uid = ""/>

</cfcomponent>
