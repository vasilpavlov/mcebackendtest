
<!---

  Template Name:  AlertSubscription
     Base Table:  AlertSubscriptions

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, July 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, July 16, 2009 - Template Created.

--->

<cfcomponent	displayname="AlertSubscription"
				hint="This CFC manages all object/bean/value object instances for the AlertSubscriptions table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.AlertSubscription"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="alert_subscription_uid" required="true" type="string"/>
	<cfproperty name="alert_type_code" required="true" type="string" hint="Describes the type code / internal identifier for a given alert."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name of a given alert."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="last_polled" required="true" type="date" hint="The last time the subscription was processed by the alerting system."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<cfproperty name="alert_digest_code" required="true" type="string" hint=""/>
	<cfproperty name="profile_lookup_code" required="true" type="string" hint=""/>
	<cfproperty name="alertTypeCfcName" type="string">

	<cfproperty name="alertTypeFriendlyName" required="true" type="string" hint=""/>
	<cfproperty name="alertDigestFriendlyName" required="true" type="string" hint=""/>
	<cfproperty name="emissionsProfileFriendlyName" required="true" type="string" hint=""/>
	<cfproperty name="propertyFriendlyName" required="true" type="string" hint=""/>
	<cfproperty name="client_company_uid" required="true" type="string" hint=""/>
	<cfproperty name="property_uid" required="true" type="string" hint=""/>

	<cfproperty name="users" required="true" type="array"/>
	<cfproperty name="accounts" required="true" type="array" hint="Describes the primary key of the property associated to a given alert."/>
	<cfproperty name="conditions" required="true" type="array"/>
	<cfproperty name="deliveryMethods" required="true" type="array"/>	
	
	<cfproperty name="recipientSummaryString" required="true" type="string"/>		
	<cfproperty name="propertySummaryString" required="true" type="string"/>		
	<cfproperty name="energyAccountSummaryString" required="true" type="string"/>			
	<cfproperty name="energyTypeSummaryString" required="true" type="string"/>
	<cfproperty name="deliveryMethodSummaryString" required="true" type="string"/>	


	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="alert_subscription_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "alert_subscription_uid"/>
	<cfset this.alert_subscription_uid = ""/>
	<cfset this.profile_lookup_code = "default">

</cfcomponent>
