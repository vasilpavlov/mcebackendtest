
<!---

  Template Name:  DataReviewDetailHistory
     Base Table:  DataReviewDetailHistory	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, April 28, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, April 28, 2009 - Template Created.		

--->
		
<cfcomponent displayname="Data Review Detail History"
				hint="This CFC manages all object/bean/value object instances for the DataReviewDetailHistory table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataReviewDetailHistory"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="detail_history_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review detail history record."/>
	<cfproperty name="review_detail_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the data review detail record being audited."/>
	<cfproperty name="review_participant_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the review participant conducting the approval audit for a given review detail record."/>
	<cfproperty name="participantFriendlyName" required="true" type="string" hint="Describes the friendly name of the review participant conducting the approval audit for a given review detail record."/>
	<cfproperty name="period_name" required="true" type="string" hint="Describes the period name for a given approval record."/>
	<cfproperty name="approval_status_date" required="true" type="date" hint="Describes the date for a given approval state."/>
	<cfproperty name="approval_status_code" required="true" type="string" hint="Describes the lookup code for a given approval status."/>
	<cfproperty name="approval_status_notes" required="true" type="string" hint="Describes any notes associated to an approval status."/>
	<cfproperty name="approvalStatusFriendlyName" required="true" type="string" hint="Describes the approval status code friendly name."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="detail_history_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "detail_history_uid"/>
	<cfset this.detail_history_uid = ""/>

</cfcomponent>
