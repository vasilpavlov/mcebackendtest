<cfcomponent extends="mce.e2.db.BaseDatabaseDelegate" output="false">

	<!---
		Returns an array of WalkthroughPropertyVo value objects
		The "queryToVoArray" function is available because we extend BaseDatabaseDelegate.
		See also "queryToVo" function when you only want to return one object.
	--->
	<cffunction name="getPropertiesAsVoArray" returntype="array">
		<cfset var q = getProperties()>
		<cfreturn queryToVoArray(q, "mce.e2.walkthrough.WalkthroughPropertyVo")>
	</cffunction>

	<!---
		Returns a list of properties, as a native CF query object. This query is
		used by the above function, but could also be used by other parts of the
		app that desire the data in query recordset form.
		This implementation of this function uses DataMgr to write the SQL on the fly
		for us. You are also welcome to write your own SQL (see comments below in this file)
	--->
	<cffunction name="getProperties" returntype="query">
		<cfset var q = this.dataMgr.getRecords(tableName="properties", fieldlist="property_uid,friendly_name", orderBy="friendly_name")>
		<cfreturn q>
	</cffunction>


	<!---
		Persists a property value object by updating the database.
		This implementation of the function uses DataMgr to write the SQL on the fly for us.
		You are welcome to write your own SQL to do this instead, or to use a stored procedure.
		That said, you are encouraged to use the DataMgr method when it is straightforward to do so,
		in an effort to cut down on the amount of hand-coded SQL throughout the application.
		In particular, for writes (update, insert, etc), it is often much less work to use DataMgr.
	 --->
	<cffunction name="savePropertyVo" returntype="void">
		<cfargument name="propertyVo" type="mce.e2.walkthrough.WalkthroughPropertyVo" required="true">
		<cfset this.dataMgr.updateRecord(tableName="properties", data=propertyVo)>
	</cffunction>



	<!---
		Most of the time, the dataMgr functionality makes things easier for you. But sometimes
		it is just easier to write your own queries. You are free to code your SQL by hand whenever
		it would be easier, more flexible, or better for the project in some other way

		Here is the above "getProperties" function, implemented as hand-coded SQL.

	<cffunction name="getProperties" returntype="query">
		<cfset var q = "">

		<cfquery name="q" datasource="#this.datasource#">
			SELECT property_uid, friendly_name
			FROM properties
			ORDER BY friendly_name
		</cfquery>

		<cfreturn q>
	</cffunction>

	You can also use stored procedures when it makes sense. The following version of the function
	delegates the logic to a stored procedure. Of course, for this simplistic example it probably
	makes no sense to do so. But for a complicated update that affects many tables, it may make
	sense to use a stored procedure.

	<cffunction name="getProperties" returntype="query">
		<cfset var q = "">

		<cfstoredproc procedure="myProcToGetProperties" datasource="#this.datasource#">
			<cfprocresult name="q">
		</cfstoredproc>

		<cfreturn q>
	</cffunction>

	--->

</cfcomponent>