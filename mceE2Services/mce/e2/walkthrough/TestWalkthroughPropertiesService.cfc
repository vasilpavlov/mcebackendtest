<cfcomponent extends="mxunit.framework.TestCase">

	<cffunction name="testGetPropertiesList">
		<!--- You can refer to request.beanFactory, which is an instance of ColdSpring, to get beans from the registry --->
		<cfset var service = request.beanFactory.getBean("WalkthroughPropertiesService")>
		<cfset var properties = service.getPropertiesList()>
		<cfset assertTrue( ArrayLen(properties) gt 0,
			"Expected to get at least one property from the database.")>
	</cffunction>

	<cffunction name="testGetSpecificProperties">
		<cfset var delegate = request.beanFactory.getBean("WalkthroughPropertiesDelegate")>
		<cfset var qProperties = delegate.getProperties()>
		<cfset var testPropName = '1001 6th Avenue'>
		<cfset var queryWithinProperties = "">
		
		<cfset assertTrue( qProperties.recordCount gt 0,
			"Expected to get at least one property from the database.")>

		<!--- One strategy for examining data more closely for unit testing purposes is to use a "query of queries" --->
		<cfquery name="queryWithinProperties" dbtype="query">
			SELECT * FROM qProperties
			WHERE friendly_name = '#testPropName#'
		</cfquery>

		<cfset assertEquals(queryWithinProperties.recordCount, 1,
			"Expected to have one and only one record for #testPropName#")>
	</cffunction>

</cfcomponent>