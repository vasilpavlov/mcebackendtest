<!---
	README:
	This file is part of the code set for the "developer walkthrough" document.
	All of the "walkthrough" classes are collected together in "walkthrough" folders,
	but this type of file would normally be found with others of its type.

	PURPOSE:
	This is a "Value Object" (VO) to represent the notion of a property.
	Its purpose in life is to match up with a corresponding VO on the Flex side.

	REMOTE CLASS ALIAS:
	The alias used here MUST be exactly the same as the alias used in the VO on the CF side.
	The convention is to use the qualified component name of the CF component as the alias.

	OTHER RULES AND NOTES:
	See the comments in the Flex "WalkthroughPropertyVo" class, which go into greater detail
	about purpose of value objects and what your responsibilities are.
--->
<cfcomponent output="false" alias="mce.e2.walkthrough.WalkthroughPropertyVo" style="rpc" extends="mce.e2.vo.BaseValueObject">
	<cfproperty name="property_uid" type="string">
	<cfproperty name="friendly_name" type="string">
</cfcomponent>