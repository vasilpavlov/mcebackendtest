<cfcomponent>
	<cfproperty name="RSip" type="string" hint="Description of the setting here">
	<cfproperty name="RSport" type="string" hint="Description of the setting here">


	<!--- Constructor - the arguments get defined in a coldspring xml file and passed to us as part of the bean loading process --->
	<cffunction name="init" output="false" hint="Constructor">
		<cfargument name="RSip">
		<cfargument name="RSport">
		<cfargument name="StoredDocumentServer">
		<cfargument name="PropertyImagesAbsolutePath">
		<cfargument name="PropertyImagesUrlPath">

		<cfargument name="RateEngineServicesWsdlUrl">
		<cfargument name="AlertingServicesWsdlUrl">

		<cfset this.reporting.RSip = arguments.RSip>
		<cfset this.reporting.RSport = arguments.RSport>
		<cfset this.reporting.RSFolder = arguments.RSFolder>
		<cfset this.reporting.StoredDocumentServer = arguments.StoredDocumentServer>
		<cfset this.propertyImages.PropertyImagesAbsolutePath = arguments.PropertyImagesAbsolutePath>
		<cfset this.propertyImages.PropertyImagesUrlPath = arguments.PropertyImagesUrlPath>

		<cfset this.rateEngineBridge.RateEngineServicesWsdlUrl = arguments.RateEngineServicesWsdlUrl>
		<cfset this.alertingBridge.AlertingServicesWsdlUrl = arguments.AlertingServicesWsdlUrl>

		<cfreturn this>
	</cffunction>

</cfcomponent>