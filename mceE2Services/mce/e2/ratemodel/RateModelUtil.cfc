<!--- Static functions related to rate models etc --->
<cfcomponent output="false">


	<!--- Merges the values from a rateModelResponse into the metaFields in an entryInfo --->
	<cffunction name="mergeEnergyUsageMetasFromRateModel" access="public" returntype="void">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo">
		<cfargument name="rateModelResponse" required="true">

		<!--- Local variables --->
		<cfset var rowMap = StructNew()>
		<cfset var i = 0>
		<cfset var thisCode = "">
		<cfset var valueFromModel = "">
		<!--- Shorthand variable for accessing the "output" query which is part of the rate model response we were handed --->
		<cfset var qOutput = arguments.rateModelResponse.result.output>

		<!--- Make a quick index to make it easy to find the query row number for a given lookup code --->
		<cfloop from="1" to="#qOutput.recordCount#" index="i">
			<cfif qOutput.meta_type_lookup_code[i] neq "">
				<cfset thisCode = qOutput.meta_type_lookup_code[i]>
				<cfset rowMap[thisCode] = i>
			</cfif>
		</cfloop>

		<!--- Now loop through the metas on this entryInfo --->
		<cfloop from="1" to="#ArrayLen(arguments.entryInfo.metaFields)#" index="i">
			<!--- If this meta's code matches up with one of the outputs from the rate model (via our quick index) --->
			<cfif StructKeyExists(rowMap, arguments.entryInfo.metaFields[i].type_lookup_code)>
				<!--- Copy the value from the model output to the meta --->
				<cfset outputRowNum = rowMap[arguments.entryInfo.metaFields[i].type_lookup_code]>
				<cfset valueFromModel = qOutput.value[outputRowNum]>
				<cfif valueFromModel neq "">
					<cfset arguments.entryInfo.metaFields[i].meta_value = valueFromModel>
				</cfif>
			</cfif>
		</cfloop>
	</cffunction>

</cfcomponent>