<cfcomponent displayname="Rate Factor Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of rate factor information.">

	<!--- ToDo:  [ADL].  Add logic to populate the audit properties for a given rate factor or rate factor 
				 value based on the session information available during the request.  This will prevent 
				 the audit data from having to be populated by the flex client and passed over the wire. --->

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setRateFactorDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage rate factor data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.RateFactorDelegate" hint="Describes the *.cfc used to manage database interactions related to rate factor inforamtion.">
		<cfset this.rateFactorDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyRateFactorVo" 
				access="public" returntype="mce.e2.vo.RateFactor"
				hint="This method is used to retrieve / return an empty rate factor information value object.">
		<cfreturn this.rateFactorDelegate.getEmptyRateFactorComponent()>
	</cffunction>

	<cffunction name="getEmptyRateFactorValueVo" 
				access="public" returntype="mce.e2.vo.RateFactorValue"
				hint="This method is used to retrieve / return an empty rate factor values value object.">
		<cfreturn this.rateFactorDelegate.getEmptyRateFactorValueComponent()>
	</cffunction>

	<cffunction name="getAllRateFactors" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of rate factor value objects.">
		<cfreturn this.rateFactorDelegate.getRateFactorsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getRateFactorsForRateModel" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of rate factor value objects.">
		<cfargument name="rate_model_uid" type="String" required="true" hint="Describes the rate model for which rate factors are to be retrieved." /> 
		<cfreturn this.rateFactorDelegate.getRateFactorsAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>
	
	<cffunction name="getRateFactor" 
				access="public" returntype="mce.e2.vo.RateFactor" 
				hint="This method is used to return a populated rate factor vo (value object)."> 
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be retrieved." /> 
		<cfreturn this.rateFactorDelegate.getRateFactorAsComponent(rate_factor_uid=arguments.rateFactor.rate_factor_uid)>
	</cffunction>

	<cffunction name="getRateFactorValues" 
				access="public" returntype="array" 
				hint="This method is used to return an array of rate factor value vo's' (value object)."> 
		<cfargument name="rate_factor_uid" type="string" required="true" hint="Describes the primary key of the rate factor who's value data is to be retrieved." /> 
		<cfargument name="relationship_start" type="date" required="false" hint="Describes the relation start date that will be used to fiter rate factor value records.">
		<cfargument name="relationship_end" type="date" required="false" hint="Describes the relation end date that will be used to fiter rate factor value records.">
		<cfargument name="rowCount" type="numeric" required="true" default="50" hint="Describes the total number of rows to retrieve when executing this query.">
		<cfargument name="selectMethod" type="string" required="false" default="all" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfreturn this.rateFactorDelegate.getRateFactorValuesAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getRateFactorValue" 
				access="public" returntype="mce.e2.vo.RateFactorValue" 
				hint="This method is used to return a rate factor value vo (value object) for a given rate factor value."> 
		<cfargument name="rateFactorValue" type="mce.e2.vo.RateFactorValue" required="true" hint="Describes the rate factor value VO object containing the properties of the rate factor value record being retrieved." /> 
		<cfargument name="selectMethod" type="string" required="false" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfreturn this.rateFactorDelegate.getRateFactorValueAsComponent(
					rate_factor_uid=arguments.rateFactorValue.rate_factor_uid,
					factor_value_uid=arguments.rateFactorValue.factor_value_uid,
					selectMethod=arguments.selectMethod)>
	</cffunction>

	<cffunction name="getRateFactorUsageData" 
				access="public" returntype="query" 
				hint="This method is used to return a rate factor usage data for a given rate factor definition."> 
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" required="true" hint="Describes the rate factor detail VO object containing the properties of the rate factor who's usage data is to be retrieved." /> 
		<cfreturn this.rateFactorDelegate.getRateFactorUsageData(rate_factor_uid=arguments.rateFactor.rate_factor_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewRateFactor" 
				access="public" returntype="string" 
				hint="This method is used to persist a new rate factor record to the application database, and return the primary key of the new rate factor object."> 
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be retrieved." /> 
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.rateFactor = setAuditProperties(duplicate(arguments.rateFactor), "create")>
		
		<!--- Set the primary key for the rate factor if it's not already set --->
		<cfset local.rateFactor = setPrimaryKey(local.rateFactor)>
								
		<!--- Save the new rate factor --->
		<cfset this.rateFactorDelegate.saveNewRateFactor(rateFactor=duplicate(local.rateFactor))>

		<!--- Return the primary key --->
		<cfreturn local.rateFactor.rate_factor_uid>

	</cffunction>
	
	<cffunction name="saveAndReturnNewRateFactor" 
				access="public" returntype="mce.e2.vo.RateFactor" 
				hint="This method is used to persist a new rate factor record to the application database, and return an instance of the new rate factor vo (value object)."> 
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be retrieved." /> 
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
				
		<!--- Save the new rate factor --->
		<cfset local.rate_factor_uid = saveNewRateFactor(rateFactor=arguments.rateFactor)>

		<!--- Set the primary key for the new rateFactor --->
		<cfset arguments.rateFactor.rate_factor_uid = local.rate_factor_uid>

		<!--- Retrieve the new rateFactor --->
		<cfreturn getRateFactor(arguments.rateFactor)>

	</cffunction>	
	
	<cffunction name="saveExistingRateFactor" 
				access="public" returntype="string" 
				hint="This method is used to save existing rate factor information to the application database, and return the primary key of the modified rate factor object."> 
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.rateFactor = setAuditProperties(arguments.rateFactor, "modify")>
		
		<!--- Save the modified rate factor --->
		<cfset this.rateFactorDelegate.saveExistingRateFactor(rateFactor=local.rateFactor)/>

		<!--- Return the modified rate factor --->
		<cfreturn local.rateFactor.rate_factor_uid>	

	</cffunction>

	<cffunction name="saveAndReturnExistingRateFactor" 
				access="public" returntype="mce.e2.vo.RateFactor" 
				hint="This method is used to save existing rate factor information to the application database, and return an instance of the modified rate factor vo (value object)."> 
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
				
		<!--- Save the modified rate factor --->
		<cfset saveExistingRateFactor(rateFactor=arguments.rateFactor)/>

		<!--- Retrieve the modified rateFactor --->
		<cfreturn getRateFactor(arguments.rateFactor)>

	</cffunction>

	<cffunction name="saveNewRateFactorValue" 
				access="public" returntype="string" 
				hint="This method is used to persist a new rate factor value record to the application database, and return the primary key of the modified rate factor object."> 
		<cfargument name="rateFactorValue" type="mce.e2.vo.RateFactorValue" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.rateFactorValue = setAuditProperties(arguments.rateFactorValue, "create")>
		
		<!--- Set the primary key for the rate factor value if it's not already set --->
		<cfset local.rateFactorValue = setPrimaryKey(local.rateFactorValue)>		
		
		<!--- Save the created rate factor value --->
		<cfset this.rateFactorDelegate.saveNewRateFactorValue(rateFactorValue=local.rateFactorValue)/>

		<!--- Retrieve the primary key of the rate factor value --->
		<cfreturn local.rateFactorValue.factor_value_uid>

	</cffunction>
	
	<cffunction name="saveAndReturnNewRateFactorValue" 
				access="public" returntype="mce.e2.vo.RateFactorValue" 
				hint="This method is used to persist a new rate factor value record to the application database, and return a complete instance of the rate factor value vo (value object)."> 
		<cfargument name="rateFactorValue" type="mce.e2.vo.RateFactorValue" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
				
		<cfif not structKeyExists(arguments.rateFactorValue, "relationship_end")>
			<cfset arguments.rateFactorValue.relationship_end = "">
		</cfif>
		
		<!--- Save the created rate factor value --->
		<cfset local.factor_value_uid = saveNewRateFactorValue(rateFactorValue=arguments.rateFactorValue)/>

		<!--- Set the primary key --->
		<cfset arguments.rateFactorValue.factor_value_uid =local.factor_value_uid>

		<!--- Retrieve the new rateFactor --->
		<cfreturn getRateFactorValue(arguments.rateFactorValue, "all")>

	</cffunction>	
	
	<cffunction name="saveExistingRateFactorValue" 
				access="public" returntype="string" 
				hint="This method is used to save existing rate factor value to the application database."> 
		<cfargument name="rateFactorValue" type="mce.e2.vo.RateFactorValue" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.rateFactorValue = setAuditProperties(arguments.rateFactorValue, "modify")>
		
		<!--- Save the modified rate factor value --->
		<cfset this.rateFactorDelegate.saveExistingRateFactorValue(rateFactorValue=local.rateFactorValue)/>

		<!--- Return the modified rate factor value primary key --->
		<cfreturn local.rateFactorValue.factor_value_uid>	

	</cffunction>

	<cffunction name="saveAndReturnExistingRateFactorValue" 
				access="public" returntype="mce.e2.vo.RateFactorValue" 
				hint="This method is used to save existing rate factor value to the application database, and return a complete instance of the rate factor value vo (value object)."> 
		<cfargument name="rateFactorValue" type="mce.e2.vo.RateFactorValue" required="true" hint="Describes the rate factor VO object containing the properties of the rate factor to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<cfif not structKeyExists(arguments.rateFactorValue, "relationship_end")>
			<cfset arguments.rateFactorValue.relationship_end = "">
		</cfif>
		
		<!--- Set the modified by / date audit properties --->
		<cfset saveExistingRateFactorValue(arguments.rateFactorValue)>
		
		<!--- Return the modified rate factor value --->
		<cfreturn getRateFactorValue(arguments.rateFactorValue, "all")>

	</cffunction>
	
	<cffunction name="saveTaxFactors" 
				access="public" returntype="boolean" 
				hint="This method is used to save tax factors for given postal codes, it adds or updates the tax factors. The delegate method throws exception if some of the given factor lookup codes is missing."> 
		<cfargument name="postalCode" type="string" required="true" hint="One or a list of postal codes devided with comma" /> 
		<cfargument name="commodityFactor" type="string" required="true" hint="Lookup code for commodity tax factor" /> 
		<cfargument name="deliveryFactor" type="string" required="true" hint="Lookup code for delivery tax factor" /> 
		<cfargument name="salesFactor" type="string" required="true" hint="Lookup code for sales tax factor" /> 

		<!--- Save the modified or new tax factors --->
		<cfset this.rateFactorDelegate.saveTaxFactors(postalCode, commodityFactor, deliveryFactor, salesFactor)/>

		<cfreturn true>
	</cffunction>
	
	<cffunction name="deleteTaxFactors" 
				access="public" returntype="boolean" 
				hint="This method is used to save tax factors for given postal codes, it adds or updates the tax factors. The delegate method throws exception if some of the given factor lookup codes is missing."> 
		<cfargument name="postalCode" type="string" required="true" hint="One or a list of postal codes devided with comma" /> 
		
		<!--- Save the modified or new tax factors --->
		<cfset this.rateFactorDelegate.deleteTaxFactors(postalCode)/>

		<cfreturn true>
	</cffunction>
	
	<cffunction name="getAllTaxFactors" 
				access="public" returntype="array" 
				hint="This method is used to get all tax factors for postal codes (cities)"> 
		
		<!--- Retrieve array of all tax rate factors --->
		<cfreturn this.rateFactorDelegate.getAllTaxFactors()>

	</cffunction>

</cfcomponent>