<cfcomponent displayname="Energy Contracts Meta Data Service"
			 extends="BaseDataService" output="true"
			 hint="The component is used to manage all business logic tied / associated with the management of Energy Contracts Meta Data information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyContractsMetaDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Energy Contracts Meta Data data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyContractsMetaDelegate" hint="Describes the *.cfc used to manage database interactions related to Energy Contracts Meta Data information.">
		<cfset this.EnergyContractsMetaDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyContractsMetaVo" 
				access="public" returntype="mce.e2.vo.EnergyContractsMeta"
				hint="This method is used to retrieve / return an empty EnergyContractsMeta information value object.">
		<cfreturn this.EnergyContractsMetaDelegate.getEmptyEnergyContractsMetaComponent()>
	</cffunction>

	<cffunction name="getEnergyContractsMetaData" 
				access="public" returntype="array" 
				hint="This method is used to return a collection of associated energy contract meta data."> 
		<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" hint="Describes the property VO object used to retrieve associated meta data." />
		<cfset aEnergyContractsMetaData = this.EnergyContractsMetaDelegate.getEnergyContractsMetaDataAsArrayOfComponents(energy_contract_uid=arguments.EnergyContracts.energy_contract_uid)> 
		<cfset this.EnergyContractsMetaDelegate.applyChoicesToEnergyContractsMeta(aEnergyContractsMetaData)>
		<cfreturn aEnergyContractsMetaData>
	</cffunction>
	<cffunction name="getEnergyContractsMeta" 
				access="public" returntype="mce.e2.vo.EnergyContractsMeta" 
				hint="This method is used to return a populated EnergyContractsMeta vo (value object)."> 
		<cfargument name="EnergyContractsMeta" type="mce.e2.vo.EnergyContractsMeta" required="true" hint="Describes the EnergyContractsMeta collection VO object containing the properties of the EnergyContractsMeta collection to be retrieved." /> 
		<cfreturn this.EnergyContractsMetaDelegate.getEnergyContractsMetaDataAsComponent(energy_contract_meta_uid=arguments.EnergyContractsMeta.energy_contract_meta_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyContractsMeta" 
				access="public" returntype="string" output="true" 
				hint="This method is used to persist a new EnergyContractsMeta record to the application database."> 
		<cfargument name="EnergyContractsMeta" type="mce.e2.vo.EnergyContractsMeta" required="true" hint="Describes the EnergyContractsMeta collection VO object containing the details of the EnergyContractsMeta collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.EnergyContractsMeta = setAuditProperties(arguments.EnergyContractsMeta, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.EnergyContractsMeta = setPrimaryKey(local.EnergyContractsMeta)>
		
		<!--- Save the modified EnergyContractsMeta --->
		<cfset this.EnergyContractsMetaDelegate.saveNewEnergyContractsMeta(EnergyContractsMeta=local.EnergyContractsMeta)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.EnergyContractsMeta.energy_contract_meta_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyContractsMeta" 
				access="public" returntype="mce.e2.vo.EnergyContractsMeta" 
				hint="This method is used to persist a new EnergyContractsMeta record to the application database."> 
		<cfargument name="EnergyContractsMeta" type="mce.e2.vo.EnergyContractsMeta" required="true" hint="Describes the EnergyContractsMeta collection VO object containing the details of the EnergyContractsMeta collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Determine if this record already exists. --->		
		<cfset local.existingEnergyContractsMeta = this.EnergyContractsMetaDelegate.getEnergyContractsMetaData(energy_contract_uid = arguments.EnergyContractsMeta.energy_contract_uid, meta_type_uid = arguments.EnergyContractsMeta.meta_type_uid)>

		<cfif local.existingEnergyContractsMeta.energy_contract_meta_uid eq "">
		
			<!--- Capture the primary key and create the object --->
			<cfset local.primaryKey = saveNewEnergyContractsMeta(EnergyContractsMeta=arguments.EnergyContractsMeta)>

			<!--- Set the primary key --->
			<cfset arguments.EnergyContractsMeta.energy_contract_meta_uid = local.primaryKey>
			
			<!--- Return the new object --->
			<cfreturn this.EnergyContractsMetaDelegate.getEnergyContractsMetaDataAsComponent(energy_contract_meta_uid = arguments.EnergyContractsMeta.energy_contract_meta_uid)>

		<cfelse>

			<cfreturn arguments.EnergyContractsMeta>

		</cfif>

	</cffunction>

	<cffunction name="saveExistingEnergyContractsMeta" 
				access="public" returntype="mce.e2.vo.EnergyContractsMeta" 
				hint="This method is used to save an existing EnergyContractsMeta information to the application database."> 
		<cfargument name="EnergyContractsMeta" type="mce.e2.vo.EnergyContractsMeta" required="true" hint="Describes the EnergyContractsMeta collection VO object containing the details of the EnergyContractsMeta collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.EnergyContractsMeta = setAuditProperties(arguments.EnergyContractsMeta, "modify")>
		
		<!--- Save the modified EnergyContractsMeta --->
		<cfset this.EnergyContractsMetaDelegate.saveExistingEnergyContractsMeta(EnergyContractsMeta=local.EnergyContractsMeta)/>

		<!--- Return the modified EnergyContractsMeta --->
		<cfreturn local.EnergyContractsMeta>	

	</cffunction>
	
</cfcomponent>