<cfcomponent displayname="Rate Model Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of rate model information.">

	<!--- ToDo:  [ADL].  Add logic to populate the audit properties for a given rate model or related rate model
				 data based on the session information available during the request.  This will prevent
				 the audit data from having to be populated by the flex client and passed over the wire. --->

	<!--- Bean getter / setter / dependencies --->

	<cffunction name="setRateModelDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage rate model data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.RateModelDelegate" hint="Describes the *.cfc used to manage database interactions related to rate model information.">
		<cfset this.rateModelDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyRateModelVo"
				access="public" returntype="mce.e2.vo.RateModel"
				hint="This method is used to retrieve / return an empty rate model information value object.">
		<cfreturn this.rateModelDelegate.getEmptyRateModelComponent()>
	</cffunction>

	<cffunction name="getEmptyRateModelInputValueVo"
				access="public" returntype="mce.e2.vo.RateModelInputValue"
				hint="This method is used to retrieve / return an empty rate model input value object.">
		<cfreturn this.rateModelDelegate.getEmptyRateModelInputValueComponent()>
	</cffunction>

	<cffunction name="getAvailableRateModels"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of rate model value objects filtered by a given energy provider rate class.">
		<cfargument name="EnergyProviderRateClass" type="mce.e2.vo.EnergyProviderRateClass" required="true" hint="Describes the energyProviderRateClass collection VO object used to filter available rate models." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Intiailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.rate_class_uid = arguments.EnergyProviderRateClass.rate_class_uid>

		<!--- Retrieve the collection of available rate models --->
		<cfreturn this.rateModelDelegate.getRateModelsAsArrayOfComponents(argumentCollection=local.args)>

	</cffunction>

	<cffunction name="getAllRateModels"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of rate model value objects.">
		<cfreturn this.rateModelDelegate.getAllRateModelsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getRateModel"
				access="public" returntype="mce.e2.vo.RateModel"
				hint="This method is used to return a populated rate model vo (value object).">
		<cfargument name="rateModel" type="mce.e2.vo.RateModel" required="true" hint="Describes the rate model VO object containing the properties of the rate model to be retrieved." />
		<cfreturn this.rateModelDelegate.getRateModelAsComponent(rate_model_uid=arguments.rateModel.rate_model_uid)>
	</cffunction>

	<cffunction name="getRateModelInputValues"
				access="public" returntype="array"
				hint="This method is used to return a populated array of rate model input values vo's (value objects).">
		<cfargument name="rateModel" type="mce.e2.vo.RateModel" required="true" hint="Describes the rate model VO object containing the properties of the rate model whose input values will be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<cfset var args = StructNew()>
		<!--- We want to use this rate model's input set as the main criteria that we pass to the delegate function --->
		<cfset args.input_set_uid = arguments.rateModel.defaults_input_set_uid>
		<cfset args.selectMethod = arguments.selectMethod>
		<cfset args.relationship_filter_date = arguments.relationship_filter_date>

		<cfreturn this.rateModelDelegate.getRateModelInputValuesAsArrayOfComponents(argumentCollection=args)>
	</cffunction>

	<cffunction name="getRateModelInputSetValues"
				access="public" returntype="array"
				hint="This method is used to return a populated array of rate model input values vo's (value objects).">
		<cfargument name="input_set_uid" required="false" type="string" hint="Describes the uid used to identify a rate model input set."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.rateModelDelegate.getRateModelInputValuesAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getRateModelInputValue"
				access="public" returntype="mce.e2.vo.RateModelInputValue"
				hint="This method is used to return a populated rate model input value vo (value object).">
		<cfargument name="rateModelInputValue" type="mce.e2.vo.RateModelInputValue" required="true" hint="Describes the rate model input value VO object containing the properties of the rate model input value to be retrieved." />
		<cfreturn this.rateModelDelegate.getRateModelInputValueAsComponent(
					relationship_id=arguments.rateModelInputValue.relationship_id,
					model_input_uid=arguments.rateModelInputValue.model_input_uid,
					input_set_uid=arguments.rateModelInputValue.input_set_uid)>
	</cffunction>

	<cffunction name="getRateModelInputSets"
				hint="This method is used to return a populated array of rate model input sets."
				output="false"
				returnType="array"
				access="public">
		<cfargument name="set_type_code" required="false" type="string" hint="Describes the lookup code used to identify a rate model input set."/>
		<cfargument name="input_set_uid" required="false" type="string" hint="Describes the uid used to identify a rate model input set."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfreturn this.rateModelDelegate.getRateModelInputSetsAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewRateModel"
				access="public" returntype="string"
				hint="This method is used to persist a new rate model record to the application database.  The method returns the primary key of the rate model that was crated.">
		<cfargument name="rateModel" type="mce.e2.vo.RateModel" required="true" hint="Describes the rate model VO object containing the properties of the rate model to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.rateModel = setAuditProperties(arguments.rateModel, "create")>

		<!--- Set the primary key for the rate model if it's not already set --->
		<cfset local.rateModel = setPrimaryKey(local.rateModel)>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.rateModel.rate_model_uid>

		<!--- Create the rateModel --->
		<cfset this.rateModelDelegate.saveNewRateModel(rateModel=local.rateModel)>

		<!--- Return the new rateModel primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewRateModel"
				access="public" returntype="mce.e2.vo.RateModel"
				hint="This method is used to persist a new rate model record to the application database.  The method returns the primary key of the rate model that was crated.">
		<cfargument name="rateModel" type="mce.e2.vo.RateModel" required="true" hint="Describes the rate model VO object containing the properties of the rate model to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create the rateModel --->
		<cfset local.primaryKey = saveNewRateModel(rateModel=arguments.rateModel)>

		<!--- Set the primary key for the rate model --->
		<cfset arguments.rateModel.rate_model_uid = local.primaryKey>

		<!--- Retrieve the new rate model --->
		<cfset local.rateModel = getRateModel(rateModel=arguments.rateModel)>

		<!--- Return the new rateModel --->
		<cfreturn local.rateModel>

	</cffunction>

	<cffunction name="saveExistingRateModel"
				access="public" returntype="void"
				hint="This method is used to save existing rate model information to the application database.">
		<cfargument name="rateModel" type="mce.e2.vo.RateModel" required="true" hint="Describes the rate model VO object containing the properties of the rate model to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.rateModel = setAuditProperties(arguments.rateModel, "modify")>

		<!--- Modify the rateModel --->
		<cfset this.rateModelDelegate.saveExistingRateModel(rateModel=local.rateModel)/>

	</cffunction>

	<cffunction name="saveNewRateModelInputSet"
				access="public" returntype="string"
				hint="This method is used to persist a new rate model value set record to the application database.  The method returns the primary key of the rate model input set that was crated.">
		<cfargument name="rateModelInputSet" type="mce.e2.vo.RateModelInputSet" required="true" hint="Describes the rate model input value VO object containing the properties of the rate model input set to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.rateModelInputSet = setAuditProperties(arguments.rateModelInputSet, "create")>

		<!--- Create the rateModelInputSet --->
		<cfset local.primaryKey = this.rateModelDelegate.saveNewRateModelInputSet(rateModelInputSet=local.rateModelInputSet)>

		<!--- Return the new rateModelInputSet primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveExistingRateModelInputSet"
				access="public" returntype="void"
				hint="This method is used to save existing rate model input set information to the application database.">
		<cfargument name="rateModelInputSet" type="mce.e2.vo.RateModelInputSet" required="true" hint="Describes the rate model input VO object containing the properties of the rate model set to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.rateModelInputSet = setAuditProperties(arguments.rateModelInputSet, "modify")>

		<!--- Modify the rateModelInputValue --->
		<cfset this.rateModelDelegate.saveExistingRateModelInputSet(rateModelInputSet=local.rateModelInputSet)/>

	</cffunction>

	<cffunction name="saveExistingRateModelInputValue"
				access="public" returntype="void"
				hint="This method is used to save existing rate model input value information to the application database.">
		<cfargument name="rateModelInputValue" type="mce.e2.vo.RateModelInputValue" required="true" hint="Describes the rate model input VO object containing the properties of the rate model input to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.rateModelInputValue = setAuditProperties(arguments.rateModelInputValue, "modify")>

		<!--- Modify the rateModelInputValue --->
		<cfset this.rateModelDelegate.saveExistingRateModelInputValue(rateModelInputValue=local.rateModelInputValue)/>

	</cffunction>

	<cffunction name="saveandReturnNewRateModelInputValue"
				access="public" returntype="mce.e2.vo.RateModelInputValue"
				hint="This method is used to save a new rate model input value information to the application database.">
		<cfargument name="rateModelInputValue" type="mce.e2.vo.RateModelInputValue" required="true" hint="Describes the rate model value input VO object containing the properties of the rate model input to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create the rateModelInputValue --->
		<cfset local.primaryKey = saveNewRateModelInputValue(rateModelInputValue=arguments.rateModelInputValue)>

		<!--- Set the primary key for the rateModelInputValue --->
		<cfset arguments.rateModelInputValue.relationship_id = local.primaryKey>

		<!--- Retrieve the new rrateModelInputValue --->
		<cfset local.rateModelInputValue = getRateModelInputValue(rateModelInputValue=arguments.rateModelInputValue)>

		<!--- Return the new rateModelInputValue  --->
		<cfreturn local.rateModelInputValue>

	</cffunction>

	<cffunction name="saveNewRateModelInputValue"
				access="public" returntype="string"
				hint="This method is used to persist a new rate model value input record to the application database.  The method returns the primary key of the rate model input value that was crated.">
		<cfargument name="rateModelInputValue" type="mce.e2.vo.RateModelInputValue" required="true" hint="Describes the rate model input value VO object containing the properties of the rate model input value to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.rateModelInputValue = setAuditProperties(arguments.rateModelInputValue, "create")>

		<!--- Set the primary key for the rate model if it's not already set --->
		<!---cfset local.rateModelInputValue = setPrimaryKey(local.rateModelInputValue)--->

		<!--- Capture the primary key --->
		<!---cfset local.primaryKey = local.rateModelInputValue.model_input_uid --->

		<!--- Create the rateModelInputValue --->
		<cfset local.primaryKey = this.rateModelDelegate.saveNewRateModelInputValue(rateModelInputValue=local.rateModelInputValue)>

		<!--- Return the new rateModelInputValue primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
</cfcomponent>