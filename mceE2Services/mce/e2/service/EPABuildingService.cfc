<cfcomponent displayname="EPABuilding Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of EPABuilding information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEPABuildingDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage EPABuilding data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EPABuildingDelegate" hint="Describes the *.cfc used to manage database interactions related to EPABuilding information.">
		<cfset this.EPABuildingDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEPABuildingVo" 
				access="public" returntype="mce.e2.vo.EPABuilding"
				hint="This method is used to retrieve / return an empty EPABuilding information value object.">
		<cfreturn this.EPABuildingDelegate.getEmptyEPABuildingComponent()>
	</cffunction>

	<cffunction name="getEPABuildings" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of EPABuildings.">
		<cfreturn this.EPABuildingDelegate.getEPABuildingsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEPABuilding" 
				access="public" returntype="mce.e2.vo.EPABuilding" 
				hint="This method is used to return a populated EPABuilding vo (value object)."> 
		<cfargument name="EPABuilding" type="mce.e2.vo.EPABuilding" required="true" hint="Describes the EPABuilding VO object containing the properties of the EPABuilding to be retrieved." /> 
		<cfreturn this.EPABuilding.getEPABuildingAsComponent(pmBldgID=arguments.EPABuilding.pmBldgID)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEPABuilding" 
				access="public" returntype="string" 
				hint="This method is used to persist a new EPABuilding record to the application database."> 
		<cfargument name="EPABuilding" type="mce.e2.vo.EPABuilding" required="true" hint="Describes the EPABuilding VO object containing the details of the EPABuilding to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.EPABuilding = setAuditProperties(arguments.EPABuilding, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.EPABuilding = setPrimaryKey(local.EPABuilding)>
		
		<!--- Save the modified EPABuilding --->
		<cfset this.EPABuildingDelegate.saveNewEPABuilding(EPABuilding=local.EPABuilding)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.EPABuilding.pmBldgID>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEPABuilding" 
				access="public" returntype="mce.e2.vo.EPABuilding" 
				hint="This method is used to persist a new EPABuilding record to the application database."> 
		<cfargument name="EPABuilding" type="mce.e2.vo.EPABuilding" required="true" hint="Describes the EPABuilding VO object containing the details of the EPABuilding to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEPABuilding(EPABuilding=arguments.EPABuilding)>

		<!--- Set the primary key --->
		<cfset arguments.EPABuilding.pmBldgID = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.EPABuilding>

	</cffunction>

	<cffunction name="saveExistingEPABuilding" 
				access="public" returntype="mce.e2.vo.EPABuilding" 
				hint="This method is used to save an existing EPABuilding information to the application database."> 
		<cfargument name="EPABuilding" type="mce.e2.vo.EPABuilding" required="true" hint="Describes the EPABuilding VO object containing the details of the EPABuilding to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.EPABuilding = setAuditProperties(arguments.EPABuilding, "modify")>
		
		<!--- Save the modified EPABuilding --->
		<cfset this.EPABuildingDelegate.saveExistingEPABuilding(EPABuilding=local.EPABuilding)/>

		<!--- Return the modified EPABuilding --->
		<cfreturn local.EPABuilding>	

	</cffunction>
	
</cfcomponent>