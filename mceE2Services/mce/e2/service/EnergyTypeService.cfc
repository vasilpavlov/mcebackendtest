<cfcomponent displayname="Energy Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to energy type information.">
		<cfset this.EnergyTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyTypeVo" 
				access="public" returntype="mce.e2.vo.EnergyType"
				hint="This method is used to retrieve / return an empty energyType information value object.">
		<cfreturn this.energyTypeDelegate.getEmptyEnergyTypeComponent()>
	</cffunction>

	<cffunction name="getAvailableEnergyTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy types supported by the application.">
		<cfreturn this.energyTypeDelegate.getEnergyTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy types.">
		<cfreturn this.energyTypeDelegate.getEnergyTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyType" 
				access="public" returntype="mce.e2.vo.EnergyType" 
				hint="This method is used to return a populated EnergyType vo (value object)."> 
		<cfargument name="EnergyType" type="mce.e2.vo.EnergyType" required="true" hint="Describes the energyType collection VO object containing the properties of the energyType collection to be retrieved." /> 
		<cfreturn this.energyTypeDelegate.getEnergyTypeAsComponent(energy_type_uid=arguments.EnergyType.energy_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new energyType record to the application database."> 
		<cfargument name="EnergyType" type="mce.e2.vo.EnergyType" required="true" hint="Describes the energyType collection VO object containing the details of the energyType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyType = setAuditProperties(arguments.energyType, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyType = setPrimaryKey(local.energyType)>
		
		<!--- Save the modified energyType --->
		<cfset this.EnergyTypeDelegate.saveNewEnergyType(energyType=local.energyType)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyType.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyType" 
				access="public" returntype="mce.e2.vo.EnergyType" 
				hint="This method is used to persist a new energyType record to the application database."> 
		<cfargument name="EnergyType" type="mce.e2.vo.EnergyType" required="true" hint="Describes the energyType collection VO object containing the details of the energyType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyType(energyType=arguments.energyType)>

		<!--- Set the primary key --->
		<cfset arguments.energyType.energy_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.energyType>

	</cffunction>

	<cffunction name="saveExistingEnergyType" 
				access="public" returntype="mce.e2.vo.EnergyType" 
				hint="This method is used to save an existing energyType information to the application database."> 
		<cfargument name="energyType" type="mce.e2.vo.EnergyType" required="true" hint="Describes the energyType collection VO object containing the details of the energyType collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.energyType = setAuditProperties(arguments.energyType, "modify")>
		
		<!--- Save the modified energyType --->
		<cfset this.EnergyTypeDelegate.saveExistingEnergyType(energyType=local.energyType)/>

		<!--- Return the modified energyType --->
		<cfreturn local.energyType>	

	</cffunction>
	
</cfcomponent>