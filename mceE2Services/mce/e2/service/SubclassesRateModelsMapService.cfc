<cfcomponent displayname="Energy Provider Rate Class Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy provider rate class information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setSubclassesRateModelsMapDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy provider rate class data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.SubclassesRateModelsMapDelegate" hint="Describes the *.cfc used to manage database interactions related to energy provider rate class data information.">
		<cfset this.subclassesRateModelsMapDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptySubclassesRateModelsMapVo" 
				access="public" returntype="mce.e2.vo.SubclassesRateModelsMap"
				hint="This method is used to retrieve / return an empty SubclassesRateModelsMap information value object.">
		<cfreturn this.subclassesRateModelsMapDelegate.getEmptySubclassesRateModelsMapComponent()>
	</cffunction>


	<cffunction name="getAvailableSubclassesRateModelsMaps" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy provider rate classes filtered by a given energy provider.">
		<cfargument name="EnergyProvider" type="mce.e2.vo.EnergyProvider" required="true" hint="Describes the energyProvider collection VO object containing the properties of the energyProvider collection to be retrieved." /> 
		<cfargument name="RateClass" type="string" required="false" hint="Describes the energyProvider collection VO object containing the properties of the energyProvider collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Intiailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.energy_provider_uid = arguments.EnergyProvider.energy_provider_uid>
		<cfif structKeyExists(arguments, 'RateClass')>
		<cfset local.args.rate_class = arguments.RateClass>
		</cfif>


		<!--- Retrieve the collection of availabel energy providers --->	
		<cfreturn this.subclassesRateModelsMapDelegate.getSubclassesRateModelsMapsAsArrayOfComponents(argumentCollection=local.args)>

	</cffunction>

	<cffunction name="getSubclassesRateModelsMaps" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy provider rate classes.">
		<cfreturn this.subclassesRateModelsMapDelegate.getSubclassesRateModelsMapsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getSubclassesRateModelsMap" 
				access="public" returntype="mce.e2.vo.SubclassesRateModelsMap" 
				hint="This method is used to return a populated SubclassesRateModelsMap vo (value object)."> 
		<cfargument name="SubclassesRateModelsMap" type="mce.e2.vo.SubclassesRateModelsMap" required="true" hint="Describes the SubclassesRateModelsMap collection VO object containing the properties of the SubclassesRateModelsMap collection to be retrieved." /> 
		<cfreturn this.subclassesRateModelsMapDelegate.getSubclassesRateModelsMapAsComponent(rate_class_uid=arguments.SubclassesRateModelsMap.rate_class_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewSubclassesRateModelsMap" 
				access="public" returntype="string" 
				hint="This method is used to persist a new SubclassesRateModelsMap record to the application database."> 
		<cfargument name="SubclassesRateModelsMap" type="mce.e2.vo.SubclassesRateModelsMap" required="true" hint="Describes the SubclassesRateModelsMap collection VO object containing the details of the SubclassesRateModelsMap collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.subclassesRateModelsMap = setAuditProperties(arguments.SubclassesRateModelsMap, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.subclassesRateModelsMap = setPrimaryKey(local.subclassesRateModelsMap)>
		
		<!--- Save the modified SubclassesRateModelsMap --->
		<cfset this.subclassesRateModelsMapDelegate.saveNewSubclassesRateModelsMap(SubclassesRateModelsMap=local.subclassesRateModelsMap)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.subclassesRateModelsMap.id>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewSubclassesRateModelsMap" 
				access="public" returntype="mce.e2.vo.SubclassesRateModelsMap" 
				hint="This method is used to persist a new SubclassesRateModelsMap record to the application database."> 
		<cfargument name="SubclassesRateModelsMap" type="mce.e2.vo.SubclassesRateModelsMap" required="true" hint="Describes the SubclassesRateModelsMap collection VO object containing the details of the SubclassesRateModelsMap collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewSubclassesRateModelsMap(SubclassesRateModelsMap=arguments.SubclassesRateModelsMap)>

		<!--- Set the primary key --->
		<cfset arguments.SubclassesRateModelsMap.rate_class_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.SubclassesRateModelsMap>

	</cffunction>

	<cffunction name="saveExistingSubclassesRateModelsMap" 
				access="public" returntype="mce.e2.vo.SubclassesRateModelsMap" 
				hint="This method is used to save an existing SubclassesRateModelsMap information to the application database."> 
		<cfargument name="SubclassesRateModelsMap" type="mce.e2.vo.SubclassesRateModelsMap" required="true" hint="Describes the SubclassesRateModelsMap collection VO object containing the details of the SubclassesRateModelsMap collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.subclassesRateModelsMap = setAuditProperties(arguments.SubclassesRateModelsMap, "modify")>
		
		<!--- Save the modified SubclassesRateModelsMap --->
		<cfset this.subclassesRateModelsMapDelegate.saveExistingSubclassesRateModelsMap(SubclassesRateModelsMap=local.subclassesRateModelsMap)/>

		<!--- Return the modified SubclassesRateModelsMap --->
		<cfreturn local.subclassesRateModelsMap>	

	</cffunction>
	
</cfcomponent>