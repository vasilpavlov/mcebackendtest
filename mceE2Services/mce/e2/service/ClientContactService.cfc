<cfcomponent displayname="Client Contact Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of client contact information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setClientContactDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client contact data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientContactDelegate" hint="Describes the *.cfc used to manage database interactions related to client contact information.">
		<cfset this.ClientContactDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setClientContactRoleDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client contact role data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientContactRoleDelegate" hint="Describes the *.cfc used to manage database interactions related to client contact role information.">
		<cfset this.ClientContactRoleDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyClientContactVo" 
				access="public" returntype="mce.e2.vo.ClientContact"
				hint="This method is used to retrieve / return an empty clientContact information value object.">
		<cfreturn this.ClientContactDelegate.getEmptyClientContactComponent()>
	</cffunction>

	<cffunction name="getClientContacts" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client contacts.">
		<cfreturn this.ClientContactDelegate.getClientContactsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientContactsByUser" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client contacts associated to a given user.">
		<cfargument name="User" type="mce.e2.vo.User" required="true" hint="Describes the user VO object used to retrieve client contacts." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.ClientContactDelegate.getClientContactsAsArrayOfComponents(
					user_uid=arguments.User.user_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>	
	
	<cffunction name="getClientContactsByProperty" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client contacts associated to a given property.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve client contacts." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.ClientContactDelegate.getClientContactsAsArrayOfComponents(
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>	

	<cffunction name="getClientContactsByClientCompany" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client contacts associated to a given client company.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object used to retrieve client contacts." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.ClientContactDelegate.getClientContactsAsArrayOfComponents(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>
	
	<cffunction name="getContactsByContext"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of contacts associated to companies or properties.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object used to retrieve client contacts." /> 
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve client contacts." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.ClientContactDelegate.getContactsByContext(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>

	<cffunction name="getClientContactAssociations" 
				access="public" returntype="query"
				hint="Retrieves all of the active client and property associations for a given contact.">
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given / associated client contact."/>
		
		<!--- Retrieve the property / client contact associations --->
		<cfreturn this.ClientContactDelegate.getClientContactAssociations(client_contact_uid=arguments.client_contact_uid)>
	</cffunction>

	<cffunction name="getClientContact" 
				access="public" returntype="mce.e2.vo.ClientContact" 
				hint="This method is used to return a populated Client Contact vo (value object)."> 
		<cfargument name="ClientContact" type="mce.e2.vo.ClientContact" required="true" hint="Describes the ClientContact VO object containing the properties of the ClientContact to be retrieved." /> 
		<cfreturn this.ClientContact.getClientContactAsComponent(client_contact_uid=arguments.ClientContact.client_contact_uid)>
	</cffunction>
	
	<cffunction name="getAvailableClientContactRoles" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client contact roles.">
		<cfreturn this.ClientContactRoleDelegate.getClientContactRolesAsArrayOfComponents()>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewClientContact" 
				access="public" returntype="string" 
				hint="This method is used to persist a new ClientContact record to the application database."> 
		<cfargument name="ClientContact" type="mce.e2.vo.ClientContact" required="true" hint="Describes the ClientContact VO object containing the details of the ClientContact to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.ClientContact = setAuditProperties(arguments.ClientContact, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.ClientContact = setPrimaryKey(local.ClientContact)>
		
		<!--- Save the modified ClientContact --->
		<cfset this.ClientContactDelegate.saveNewClientContact(ClientContact=local.ClientContact)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.ClientContact.client_contact_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewClientContact" 
				access="public" returntype="mce.e2.vo.ClientContact" 
				hint="This method is used to persist a new ClientContact record to the application database."> 
		<cfargument name="ClientContact" type="mce.e2.vo.ClientContact" required="true" hint="Describes the ClientContact VO object containing the details of the ClientContact to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewClientContact(ClientContact=arguments.ClientContact)>

		<!--- Set the primary key --->
		<cfset arguments.ClientContact.client_contact_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.ClientContact>

	</cffunction>

	<cffunction name="saveExistingClientContact" 
				access="public" returntype="mce.e2.vo.ClientContact" 
				hint="This method is used to save an existing ClientContact information to the application database."> 
		<cfargument name="ClientContact" type="mce.e2.vo.ClientContact" required="true" hint="Describes the ClientContact VO object containing the details of the ClientContact to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Deactivate all associations if this contact is inactive. --->
		<cfif arguments.ClientContact.is_active eq false>
			<cfset local.assocObject = structnew()>
			<cfset local.assocObject = setAuditProperties(local.assocObject, "modify")>
			<cfset this.ClientContactDelegate.inactivateExistingClientContactAssociations(client_contact_uid=arguments.ClientContact.client_contact_uid, modified_date=local.assocObject.modified_date, modified_by=local.assocObject.modified_by)>
		</cfif>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.ClientContact = setAuditProperties(arguments.ClientContact, "modify")>
		
		<!--- Save the modified ClientContact --->
		<cfset this.ClientContactDelegate.saveExistingClientContact(ClientContact=local.ClientContact)/>

		<!--- Return the modified ClientContact --->
		<cfreturn local.ClientContact>	

	</cffunction>

	<cffunction name="searchClientContacts" 
				access="public" returntype="query"
				hint="This method is used to search client contacts.">
		<cfargument name="first_name" type="string" required="false" hint="Describes the first name of the client contact.">
		<cfargument name="last_name" type="string" required="false" hint="Describes the last name of the client contact.">
		<cfreturn this.ClientContactDelegate.getClientContact(
					first_name=arguments.first_name,
					last_name=arguments.last_name,
					selectMethod="current")>
	</cffunction>	

</cfcomponent>