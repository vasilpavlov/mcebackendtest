<cfcomponent displayname="Energy Provider Rate Class Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy provider rate class information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyProviderRateClassDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy provider rate class data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyProviderRateClassDelegate" hint="Describes the *.cfc used to manage database interactions related to energy provider rate class data information.">
		<cfset this.EnergyProviderRateClassDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyProviderRateClassVo" 
				access="public" returntype="mce.e2.vo.EnergyProviderRateClass"
				hint="This method is used to retrieve / return an empty energyProviderRateClass information value object.">
		<cfreturn this.energyProviderRateClassDelegate.getEmptyEnergyProviderRateClassComponent()>
	</cffunction>

	<cffunction name="getAvailableEnergyProviderRateClasses" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy provider rate classes filtered by a given energy provider.">
		<cfargument name="EnergyProvider" type="mce.e2.vo.EnergyProvider" required="true" hint="Describes the energyProvider collection VO object containing the properties of the energyProvider collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Intiailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.energy_provider_uid = arguments.EnergyProvider.energy_provider_uid>

		<!--- Retrieve the collection of availabel energy providers --->	
		<cfreturn this.energyProviderRateClassDelegate.getEnergyProviderRateClassesAsArrayOfComponents(argumentCollection=local.args)>

	</cffunction>

	<cffunction name="getEnergyProviderRateClasses" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy provider rate classes.">
		<cfreturn this.energyProviderRateClassDelegate.getEnergyProviderRateClassesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyProviderRateClass" 
				access="public" returntype="mce.e2.vo.EnergyProviderRateClass" 
				hint="This method is used to return a populated EnergyProviderRateClass vo (value object)."> 
		<cfargument name="EnergyProviderRateClass" type="mce.e2.vo.EnergyProviderRateClass" required="true" hint="Describes the energyProviderRateClass collection VO object containing the properties of the energyProviderRateClass collection to be retrieved." /> 
		
		<cfreturn this.energyProviderRateClassDelegate.getEnergyProviderRateClassAsComponent(rate_class_uid=arguments.EnergyProviderRateClass.rate_class_uid, energy_provider_uid=arguments.EnergyProviderRateClass.energy_provider_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyProviderRateClass" 
				access="public" returntype="string" 
				hint="This method is used to persist a new energyProviderRateClass record to the application database."> 
		<cfargument name="EnergyProviderRateClass" type="mce.e2.vo.EnergyProviderRateClass" required="true" hint="Describes the energyProviderRateClass collection VO object containing the details of the energyProviderRateClass collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyProviderRateClass = setAuditProperties(arguments.energyProviderRateClass, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyProviderRateClass = setPrimaryKey(local.energyProviderRateClass)>
		
		<!--- Save the modified energyProviderRateClass --->
		<cfset this.EnergyProviderRateClassDelegate.saveNewEnergyProviderRateClass(energyProviderRateClass=local.energyProviderRateClass)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyProviderRateClass.rate_class_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyProviderRateClass" 
				access="public" returntype="mce.e2.vo.EnergyProviderRateClass" 
				hint="This method is used to persist a new energyProviderRateClass record to the application database."> 
		<cfargument name="EnergyProviderRateClass" type="mce.e2.vo.EnergyProviderRateClass" required="true" hint="Describes the energyProviderRateClass collection VO object containing the details of the energyProviderRateClass collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyProviderRateClass(energyProviderRateClass=arguments.energyProviderRateClass)>

		<!--- Set the primary key --->
		<cfset arguments.energyProviderRateClass.rate_class_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.energyProviderRateClass>

	</cffunction>

	<cffunction name="saveExistingEnergyProviderRateClass" 
				access="public" returntype="mce.e2.vo.EnergyProviderRateClass" 
				hint="This method is used to save an existing energyProviderRateClass information to the application database."> 
		<cfargument name="energyProviderRateClass" type="mce.e2.vo.EnergyProviderRateClass" required="true" hint="Describes the energyProviderRateClass collection VO object containing the details of the energyProviderRateClass collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.energyProviderRateClass = setAuditProperties(arguments.energyProviderRateClass, "modify")>
		
		<!--- Save the modified energyProviderRateClass --->
		<cfset this.EnergyProviderRateClassDelegate.saveExistingEnergyProviderRateClass(energyProviderRateClass=local.energyProviderRateClass)/>

		<!--- Return the modified energyProviderRateClass --->
		<cfreturn local.energyProviderRateClass>	

	</cffunction>
	
</cfcomponent>