<cfcomponent displayname="Data Review Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Data Review information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setDataReviewDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Data Review data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.DataReviewDelegate" hint="Describes the *.cfc used to manage database interactions related to Data Review information.">
		<cfset this.DataReviewDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setDataReviewDetailDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage data review detail history data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.DataReviewDetailDelegate" hint="Describes the *.cfc used to manage database interactions related to data review detail information.">
		<cfset this.DataReviewDetailDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setDataReviewDetailHistoryDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage data review detail history data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.DataReviewDetailHistoryDelegate" hint="Describes the *.cfc used to manage database interactions related to data review detail history information.">
		<cfset this.DataReviewDetailHistoryDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyCollectionDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage property collection data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyCollectionDelegate" hint="Describes the *.cfc used to manage database interactions related to property collection information.">
		<cfset this.PropertyCollectionDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setEnergyUsageDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy usage data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyUsageDelegate" hint="Describes the *.cfc used to manage database interactions related to energy usage information.">
		<cfset this.EnergyUsageDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyDataReviewVo" 
				access="public" returntype="mce.e2.vo.DataReview"
				hint="This method is used to retrieve / return an empty DataReview information value object.">
		<cfreturn this.DataReviewDelegate.getEmptyDataReviewComponent()>
	</cffunction>

	<cffunction name="getEmptyDataReviewDetailVo" 
				access="public" returntype="mce.e2.vo.DataReviewDetail"
				hint="This method is used to retrieve / return an empty DataReviewDetail information value object.">
		<cfreturn this.DataReviewDetailDelegate.getEmptyDataReviewDetailComponent()>
	</cffunction>

	<cffunction name="getEmptyDataReviewDetailHistoryVo" 
				access="public" returntype="mce.e2.vo.DataReviewDetailHistory"
				hint="This method is used to retrieve / return an empty DataReviewDetailHistory information value object.">
		<cfreturn this.DataReviewDetailHistoryDelegate.getEmptyDataReviewDetailHistoryComponent()>
	</cffunction>
	
	<cffunction name="getEmptyDataReviewApprovalStatusVo" 
				access="public" returntype="mce.e2.vo.DataReviewApprovalStatus"
				hint="This method is used to retrieve / return an empty DataReviewApprovalStatus information value object.">
		<cfreturn this.DataReviewDelegate.getEmptyDataReviewApprovalStatusComponent()>
	</cffunction>	
	
	<cffunction name="getAvailableDataReviews" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Data Reviews supported by the application.">

		<!--- Define the arguments for this method --->
		<cfargument name="user" type="any" required="false" hint="Describes the user VO object containing the properties of the user for which user reports will be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Evaluate the user properties, and get permissions for the current / specified users --->
		<cfif structKeyExists(arguments, 'user') and (isStruct(arguments.user) or isInstanceOf(arguments.user, 'mce.e2.vo.User'))>

			<!--- If so, then grab the primary key, and append it to the arguments structure --->
			<cfset local.args.user_uid = arguments.user.user_uid>		

		<cfelse>		

			<!--- Create a reference to the current session --->
			<cfset local.securityService = request.beanFactory.getBean("securityService")>
	
			<!---  Create a reference to the current session --->
			<cfset local.session = local.securityService.getSession()>

			<!--- Grab the user_uid associated to the current session --->
			<cfset local.args.user_uid = local.session.user_uid>

		</cfif>

		<!---  Retrieve the data reviews associated to the current user --->
		<cfreturn this.DataReviewDelegate.getDataReviewsAsArrayOfComponents(argumentCollection = local.args)>

	</cffunction>

	<cffunction name="getDataReviews" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Data Reviews.">
		<cfreturn this.DataReviewDelegate.getDataReviewAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getDataReview" 
				access="public" returntype="mce.e2.vo.DataReview" 
				hint="This method is used to return a populated DataReview vo (value object)."> 
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object containing the properties of the DataReview collection to be retrieved." /> 
		<cfreturn this.DataReviewDelegate.getDataReviewAsComponent(data_review_uid=arguments.DataReview.data_review_uid)>
	</cffunction>

	<cffunction name="getVisibleDataReviewParticipants" 
					access="public" returntype="array" 
					hint="This method is used to return a collection of populated DataReviewParticipant vo's that the current user has rights to view."> 

		<!--- Initialize the method arguments --->
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object defining the data review for which participant data will be retrieved."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create a reference to the current session --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>

		<!---  Create a reference to the current session --->
		<cfset local.session = local.securityService.getSession()>

		<!--- Initialize the participant array --->
		<cfset local.participantArray = arrayNew(1)>

		<!--- Retrieve the current participant --->
		<cfset local.currentParticipant = this.DataReviewDelegate.getDataReviewParticipantAsComponent(
				data_review_uid=arguments.DataReview.data_review_uid,
				user_uid=local.session.user_uid)>
	
		<!--- Retrieve the properties for the current user. --->
		<cfset local.currentParticipantProperties = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
				review_participant_uid = local.currentParticipant["review_participant_uid"],
				data_review_uid = local.currentParticipant["data_review_uid"])>	
	
		<!--- First, switch over the different roles --->
		<cfswitch expression = "#local.currentParticipant.review_role_code#">

			<!--- Render a single user --->
			<cfcase value="EM">
				
				<!--- Create the participant propertyArray property, and seed it the with properties for this participant --->
				<cfset local.currentParticipant["propertyArray"] = duplicate(local.currentParticipantProperties)>

				<!--- Appent the current user to the participant array --->
				<cfset arrayAppend(local.participantArray, duplicate(local.currentParticipant))>

			</cfcase>

			<!--- Render only the users one level under a given user --->
			<cfcase value="CRM">

				<!--- Initialize the property_uid list --->
				<cfset local.property_uid_list = buildVoPropertyListFromArray(local.currentParticipantProperties, "property_uid")>
			
				<!--- Use this user's properties to retrieve all EM's that have associated properties. --->
				<cfset local.participantEMArray = this.dataReviewDelegate.getDataReviewParticipantsAsArrayOfComponents(
					data_review_uid = local.currentParticipant["data_review_uid"],
					client_company_uid = local.session.client_company_uid,
					review_role_code = 'EM',
					property_uid = local.property_uid_list)>

				<!--- Retrieve a list of the users that are children of the current parent participant --->
				<cfset local.review_participant_uid_list = buildVoPropertyListFromArray(local.participantEMArray, "review_participant_uid")>

				<!--- Retrieve the properties for the current user. --->
				<cfset local.currentParticipant["propertyArray"] = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
						review_participant_uid = local.currentParticipant["review_participant_uid"],
						data_review_uid = local.currentParticipant["data_review_uid"],
						filter_properties_by_review_participant_uid_list = local.review_participant_uid_list)>	
								
				<!--- Add the current participant to the output array --->				
				<cfset arrayAppend(local.participantArray, duplicate(local.currentParticipant))>				
								
				<!--- loop over the participant array and populate the properties for each participant --->
				<cfloop from="1" to="#arrayLen(local.participantEMArray)#" index="local.arrayIndex">
		
					<!--- Set the parent participant key to denote the parent / child relationship --->
					<cfset local.participantEMArray[local.arrayIndex]["parent_participant_uid"] = local.currentParticipant["review_participant_uid"]>
		
					<!--- Get the participant properties as a structure of component arrays --->
					<cfset local.participantProperties = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
							review_participant_uid = local.participantEMArray[local.arrayIndex]["review_participant_uid"],
							data_review_uid = local.participantEMArray[local.arrayIndex]["data_review_uid"],
							property_uid = local.property_uid_list)>
			
					<!--- If so, then duplicate the properties array to the current element of the participantArray --->
					<cfset local.participantEMArray[local.arrayIndex]["propertyArray"] = duplicate(local.participantProperties)>
		
					<!--- Append each rendered EM to the participant / output array --->
					<cfset arrayAppend(local.participantArray, duplicate(local.participantEMArray[local.arrayIndex]))>

				</cfloop>	
	
			</cfcase>

			<!--- Render only the users two levels under a given user --->			
			<cfcase value="RM">
			
				<!--- Initialize the property_uid list changed from buildVoPropertyListFromArray(local.participantProperties)--->
				<cfset local.property_uid_list = buildVoPropertyListFromArray(local.currentParticipantProperties, "property_uid")>
						
				<!--- Use this user's properties to retrieve all CRM's that have associated properties. --->
				<cfset local.participantCRMArray = this.dataReviewDelegate.getDataReviewParticipantsAsArrayOfComponents(
					data_review_uid = local.currentParticipant["data_review_uid"],
					client_company_uid = local.session.client_company_uid,
					review_role_code = 'CRM',
					property_uid = local.property_uid_list)>
			
				<!--- Retrieve a list of the users that are children of the current parent participant --->
				<cfset local.review_participant_uid_list = buildVoPropertyListFromArray(local.participantCRMArray, "review_participant_uid")>

				<!--- Retrieve the properties for the current user. --->
				<cfset local.currentParticipant["propertyArray"] = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
						review_participant_uid = local.currentParticipant["review_participant_uid"],
						data_review_uid = local.currentParticipant["data_review_uid"],
						filter_properties_by_review_participant_uid_list = local.review_participant_uid_list)>	

				<!--- Appent the current user to the participant array --->
				<cfset arrayAppend(local.participantArray, duplicate(local.currentParticipant))>			
			
				<!--- loop over the participant CRM array and populate the properties for each participant --->
				<cfloop from="1" to="#arrayLen(local.participantCRMArray)#" index="local.arrayIndex">
					
					<!--- Set the parent participant key to denote the parent / child relationship --->
					<cfset local.participantCRMArray[local.arrayIndex]["parent_participant_uid"] = local.currentParticipant["review_participant_uid"]>
		
					<!--- Get the participant properties as a structure of component arrays --->
					<cfset local.participantProperties = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
							review_participant_uid = local.participantCRMArray[local.arrayIndex]["review_participant_uid"],
							data_review_uid = local.participantCRMArray[local.arrayIndex]["data_review_uid"],
							property_uid = local.property_uid_list)>
											
					<!--- If so, then duplicate the properties array to the current element of the participantArray --->
					<cfset local.participantCRMArray[local.arrayIndex]["propertyArray"] = duplicate(local.participantProperties)>
					
					<!--- Once a CRM has been defined, append it to the participantArray --->
					<cfset arrayAppend(local.participantArray, duplicate(local.participantCRMArray[local.arrayIndex]))>	
						
					<!--- Now that a CRM has been processed, we need to find / identify it's EMs using the properties assigned to the CRM 
					<cfset local.property_uid_list = buildVoPropertyListFromArray(local.participantProperties, "property_uid")>

					<!--- Use this user's properties to retrieve all CRM's that have associated properties. --->
					<cfset local.participantEMArray = this.dataReviewDelegate.getDataReviewParticipantsAsArrayOfComponents(
						data_review_uid = local.currentParticipant["data_review_uid"],
						client_company_uid = local.session.client_company_uid,
						review_role_code = 'EM',
						property_uid = local.property_uid_list)>
											
					<!--- loop over the participant EM array and populate the properties for each participant --->
					<cfloop from="1" to="#arrayLen(local.participantEMArray)#" index="local.emArrayIndex">

						<!--- Set the parent participant key to denote the parent / child relationship --->
						<cfset local.participantEMArray[local.arrayIndex]["parent_participant_uid"] = local.participantCRMArray[local.arrayIndex]["review_participant_uid"]>
			
						<!--- Get the participant properties as a structure of component arrays --->
						<cfset local.participantProperties = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
								review_participant_uid = local.participantEMArray[local.emArrayIndex]["review_participant_uid"],
								data_review_uid = local.participantEMArray[local.emArrayIndex]["data_review_uid"],
								property_uid = local.property_uid_list)>
				
						<!--- If so, then duplicate the properties array to the current element of the participantArray --->
						<cfset local.participantEMArray[local.emArrayIndex]["propertyArray"] = duplicate(local.participantProperties)>
						
						<!--- Once a CRM has been defined, append it to the participantArray --->
						<cfset arrayAppend(local.participantArray, duplicate(local.participantEMArray[local.emArrayIndex]))>	

					</cfloop>
					--->
					
				</cfloop>
			
			</cfcase>			
			
		</cfswitch>
	
		<!--- Return the array contents --->
		<cfreturn local.participantArray>		
				
	</cffunction>			
				
	<cffunction name="getDataReviewParticipants" 
				access="public" returntype="array" 
				hint="This method is used to return a populated DataReviewParticipant vo (value object)."> 

		<!--- Initialize the method arguments --->
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object defining the data review for which participant data will be retrieved."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the participant array --->
		<cfset local.participantArray = this.DataReviewDelegate.getDataReviewParticipantsAsArrayOfComponents(
				data_review_uid=arguments.DataReview.data_review_uid)>
	
		<!--- loop over the participant array and populate the properties for each participant --->
		<cfloop from="1" to="#arrayLen(local.participantArray)#" index="local.arrayIndex">

			<!--- Get the participant properties as a structure of component arrays --->
			<cfset local.participantProperties = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
					review_participant_uid = local.participantArray[local.arrayIndex]["review_participant_uid"],
					data_review_uid = local.participantArray[local.arrayIndex]["data_review_uid"])>
	
			<!--- If so, then duplicate the properties array to the current element of the participantArray --->
			<cfset local.participantArray[local.arrayIndex]["propertyArray"] = duplicate(local.participantProperties)>

		</cfloop>	

		<!--- Return the participant array --->
		<cfreturn local.participantArray>

	</cffunction>		
	
	<cffunction name="getDataReviewParticipant" 
				access="public" returntype="mce.e2.vo.DataReviewParticipant" 
				hint="This method is used to return a populated DataReviewParticipant vo (value object)."> 

		<!--- Initialize the method arguments --->
		<cfargument name="DataReviewParticipant" type="mce.e2.vo.DataReviewParticipant" required="true" hint="Describes the DataReviewParticipant collection VO object containing the properties of the DataReviewParticipant collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create the base data review object --->
		<cfset local.participant = this.DataReviewDelegate.getDataReviewParticipantAsComponent(review_participant_uid=arguments.DataReviewParticipant.review_participant_uid)>

		<!--- Retrieve the properties associated to the participant --->
		<cfset local.properties = this.propertyCollectionDelegate.getParticipantPropertiesAsArrayOfComponents(
				review_participant_uid = local.participant.review_participant_uid,
				data_review_uid = local.participant.data_review_uid)>

		<!--- Place the propertyArray in the participant object --->
		<cfset local.participant.propertyArray = duplicate(local.properties)>

		<!--- Return the data review object --->
		<cfreturn local.participant>

	</cffunction>	
		
	<cffunction name="getDataReviewDetailUsageSummaries" 
				access="public" returntype="array" 
				hint="This method is used to retrieve data review usage summary data."> 

		<!--- Initialize the method arguments --->
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object containing the properties of the DataReview object used to retrieve the review detail usage summaries." /> 
		<cfargument name="propertyArray" type="array" required="true" hint="Describes the colleciton of properties used to retrieve the review detail usage summaries." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!---  Build out the property_uid list --->
		<cfset local.property_uid_list = buildVoPropertyListFromArray(arguments.propertyArray, 'property_uid')>

		<!--- Retrive the data review detail usage summary data --->
		<cfset local.usageSummary = this.DataReviewDelegate.getDataReviewDetailUsageSummaryAsArrayOfComponents(
				data_review_uid = arguments.dataReview.data_review_uid,
				profile_lookup_code = arguments.dataReview.profile_lookup_code,
				property_uid = local.property_uid_list,
				display_period_start = arguments.dataReview.display_period_start,
				display_period_end = arguments.dataReview.display_period_end)>

		<!--- Return the data review object --->
		<cfreturn local.usageSummary>

	</cffunction>			
		
	<cffunction name="getCurrentDataReviewDetail" 
				access="public" returntype="mce.e2.vo.DataReviewDetail" 
				hint="This method is used to retrieve the current / active data review detail record for a given parent data review."> 

		<!--- Initialize the method arguments --->
		<cfargument name="DataReviewDetail" type="any" required="true" hint="Describes the DataReview collection VO object containing the properties of the DataReview object used to retrieve the data review detail history." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the flag used to retrieve only the current review detail record --->
		<cfset arguments.DataReviewDetail.is_current_review_detail = 1>
		
		<!--- Retrieve the data review detail component --->
		<cfset local.dataReviewDetail = this.DataReviewDetailDelegate.getDataReviewDetailAsComponent(
				argumentCollection = arguments.dataReviewDetail)>

		<!--- Return the data review detail component --->
		<cfreturn local.dataReviewDetail>

	</cffunction>				
		
	<cffunction name="getDataReviewDetail" 
				access="public" returntype="mce.e2.vo.DataReviewDetail" 
				hint="This method is used to retrieve data review detail record."> 

		<!--- Initialize the method arguments --->
		<cfargument name="DataReviewDetail" type="mce.e2.vo.DataReviewDetail" required="true" hint="Describes the DataReview collection VO object containing the properties of the DataReview object used to retrieve the data review detail history." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Retrieve the data review detail component --->
		<cfset local.dataReviewDetail = this.DataReviewDetailDelegate.getDataReviewDetailAsComponent(
				review_detail_uid = arguments.dataReviewDetail.review_detail_uid)>

		<!--- Return the data review detail component --->
		<cfreturn local.dataReviewDetail>

	</cffunction>			
						
	<cffunction name="getDataReviewDetailHistory" 
				access="public" returntype="array" 
				hint="This method is used to retrieve data review detail history."> 

		<!--- Initialize the method arguments --->
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object containing the properties of the DataReview object used to retrieve the data review detail history." /> 
		<cfargument name="includePrior" type="boolean" required="true" default="false" hint="Describes how to return data review detail recordsets; when false, include only the reviw history for the specified data review object instance.  When true, include review history records from other review objects that share the same property." /> 
		<cfargument name="property" type="any" required="false" hint="Describes the property by which data review detail history data will be filtered." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the arguments scope --->
		<cfset local.args = structNew()>
		
		<!--- Populate required arguments --->
		<cfset local.args.data_review_uid = arguments.DataReview.data_review_uid>
		<cfset local.args.includePrior = arguments.includePrior>
		
		<!--- Was a filter property specified? --->
		<cfif structKeyExists(arguments, 'property')>
		
			<!--- If so, then set / seed the property value --->
			<cfset local.args.property_uid = arguments.property.property_uid>
		
		</cfif>
		
		<!--- Retrieve the data review detail history array --->
		<cfset local.history = this.DataReviewDetailHistoryDelegate.getDataReviewDetailHistoryAsArrayOfComponents(
				argumentCollection = local.args)>

		<!--- Return the history array --->
		<cfreturn local.history>

	</cffunction>			
		
	<cffunction name="getDataReviewApprovalStatuses" 
				access="public" returntype="array" 
				hint="This method is used to retrieve data review approval status data.">

		<!--- Initialize the method arguments --->
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object containing the properties of the DataReview object used to retrieve approval status data." /> 
		<cfargument name="User" type="any" required="false" hint="Describes the user for which data review approval statuses are being pulled (if no user is specified, the default / logged in user will be processed)." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the arguments scope --->
		<cfset local.args = structNew()>
		<cfset local.args.data_review_uid = arguments.DataReview.data_review_uid>
	
		<!--- Was a user specified / passed in? --->
		<cfif not structKeyExists(arguments, 'User') or 
			  ( structKeyExists(arguments, 'User') and not isInstanceOf(arguments.User, 'mce.e2.vo.User'))>
		
			<!--- Retrieve the user's session --->
			<cfset local.session = request.beanFactory.getBean("securityService").getSession()>
			
			<!--- Initialize the user object, and seed the user's primary key --->
			<cfset local.args.user_uid = local.session.user_uid>			
		
		</cfif>

		<!--- Retrieve the data review approval statuses array --->
		<cfset local.approvalStatuses = this.DataReviewDelegate.getDataReviewApprovalStatusesAsArrayOfComponents(
				argumentCollection = local.args)>

		<!--- Return the approval statuses array --->
		<cfreturn local.approvalStatuses>

	</cffunction>			

	<cffunction name="getEnergyUsageforReviewDetail" 
				access="public" returntype="array" 
				hint="This method is used to retrieve energy usage for review detail data.">

		<!--- Initialize the method arguments --->
		<cfargument name="accountArray" type="array" required="true" hint="Describes a collection of energy accounts for which energy usage data will be retrieved."/>
		<cfargument name="display_period_start" required="true" type="date" hint="Describes the start date used to define the period for which energy usage will be calculated."/>
		<cfargument name="display_period_end" required="true" type="date" hint="Describes the end date used to define the period for which energy usage will be calculated."/>
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<!--- Build a list of the accounts to process --->
		<cfset local.account_uid_list = buildVoPropertyListFromArray(arguments.accountArray, 'energy_account_uid')>

		<!--- Retrieve the data review approval statuses array --->
		<cfset local.energyUsage = this.DataReviewDelegate.getEnergyUsageforReviewDetailAsArrayOfComponents(
				energy_account_uid = local.account_uid_list,
				display_period_start = arguments.display_period_start,
				display_period_end = arguments.display_period_end)>

		<!--- Return the energy usage array --->
		<cfreturn local.energyUsage>

	</cffunction>			

	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewDataReview" 
				access="public" returntype="string" 
				hint="This method is used to persist a new DataReview record to the application database."> 
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object containing the details of the DataReview collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.DataReview = setAuditProperties(arguments.DataReview, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.DataReview = setPrimaryKey(local.DataReview)>
		
		<!--- Save the modified DataReview --->
		<cfset this.DataReviewDelegate.saveNewDataReview(DataReview=local.DataReview)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.DataReview.data_review_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewDataReview" 
				access="public" returntype="mce.e2.vo.DataReview" 
				hint="This method is used to persist a new DataReview record to the application database."> 
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object containing the details of the DataReview collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewDataReview(DataReview=arguments.DataReview)>

		<!--- Set the primary key --->
		<cfset arguments.DataReview.data_review_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.DataReview>

	</cffunction>

	<cffunction name="saveExistingDataReview" 
				access="public" returntype="mce.e2.vo.DataReview" 
				hint="This method is used to save an existing DataReview information to the application database."> 
		<cfargument name="DataReview" type="mce.e2.vo.DataReview" required="true" hint="Describes the DataReview collection VO object containing the details of the DataReview collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.DataReview = setAuditProperties(arguments.DataReview, "modify")>
		
		<!--- Save the modified DataReview --->
		<cfset this.DataReviewDelegate.saveExistingDataReview(DataReview=local.DataReview)/>

		<!--- Return the modified DataReview --->
		<cfreturn local.DataReview>	

	</cffunction>
	
	<cffunction name="saveNewDataReviewDetail" 
				access="public" returntype="string" 
				hint="This method is used to persist a new DataReviewDetail record to the application database."> 
		<cfargument name="DataReviewDetail" type="mce.e2.vo.DataReviewDetail" required="true" hint="Describes the DataReviewDetail VO object containing the details of the DataReviewDetail to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.DataReviewDetail = setAuditProperties(arguments.DataReviewDetail, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.DataReviewDetail = setPrimaryKey(local.DataReviewDetail)>
		
		<!--- Save the modified DataReview --->
		<cfset this.DataReviewDetailDelegate.saveNewDataReviewDetail(DataReviewDetail=local.DataReviewDetail)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.DataReviewDetail.review_detail_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewDataReviewDetail" 
				access="public" returntype="mce.e2.vo.DataReviewDetail" 
				hint="This method is used to persist a new DataReviewDetail record to the application database."> 
		<cfargument name="DataReviewDetail" type="mce.e2.vo.DataReviewDetail" required="true" hint="Describes the DataReviewDetail VO object containing the details of the DataReviewDetail record to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewDataReviewDetail(DataReviewDetail=arguments.DataReviewDetail)>

		<!--- Set the primary key --->
		<cfset arguments.DataReviewDetailDelegate.review_detail_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.DataReviewDetail>

	</cffunction>

	<cffunction name="saveExistingDataReviewDetail" 
				access="public" returntype="mce.e2.vo.DataReviewDetail" 
				hint="This method is used to save an existing DataReviewDetail information to the application database."> 
		<cfargument name="DataReviewDetail" type="mce.e2.vo.DataReviewDetail" required="true" hint="Describes the DataReviewDetail VO object containing the details of the DataReviewDetail record to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.DataReviewDetail = setAuditProperties(arguments.DataReviewDetail, "modify")>
		
		<!--- Save the modified DataReviewDetail --->
		<cfset this.DataReviewDetailDelegate.saveExistingDataReviewDetail(DataReviewDetail=local.DataReviewDetail)/>

		<!--- Return the modified DataReviewDetail --->
		<cfreturn local.DataReviewDetail>	

	</cffunction>
		
	<cffunction name="changeDataReviewDetailStatus"
				access="public" returntype="void"
				hint="This method is used to save / update the review detail status properties.">
		<cfargument name="DataReviewDetailStatus" type="mce.e2.vo.DataReviewDetailStatus" required="true" hint="Describes the status changes that will be made to a given review detail record.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create an instance of the security service and retrieve the current session --->
		<cfset local.securityService = request.beanFactory.getBean("SecurityService")>				 	
		<cfset local.session = local.securityService.getSession()>
		
		<!--- Initialize the local arguments structure --->
		<cfset local.args = structNew()>
		
		<!--- Validate if a review_detail_uid is specified? --->
		<cfif structKeyExists(arguments.dataReviewDetailStatus, 'review_detail_uid')>

			<!--- If so, then set use this value to retrieve the associated record --->
			<cfset local.args.review_detail_uid = arguments.dataReviewDetailStatus.review_detail_uid>

			<!--- Initialize the data review object --->
			<cfset local.dataReviewDetail = this.dataReviewDetailDelegate.getDataReviewDetailAsComponent(
					argumentCollection = local.args)>

		<cfelse>

			<!--- Validate if a data_review_uid is specified? --->		
			<cfif structKeyExists(arguments.dataReviewDetailStatus, 'data_review_uid')>
				<cfset local.args.data_review_uid = arguments.dataReviewDetailStatus.data_review_uid>
			</cfif>
		
			<!--- Validate if a property_uid is specified? --->		
			<cfif structKeyExists(arguments.dataReviewDetailStatus, 'property_uid')>
				<cfset local.args.property_uid = arguments.dataReviewDetailStatus.property_uid>
			</cfif>
			
			<!--- Initialize the data review object --->
			<cfset local.dataReviewDetail = getCurrentDataReviewDetail(
					dataReviewDetail = local.args)>
		
		</cfif>		
				
		<!--- Set the modified by / date audit properties --->
		<cfset local.dataReviewDetail.approval_status_date = createOdbcDateTime(now())>		
		<cfset local.dataReviewDetail.approval_status_code = arguments.dataReviewDetailStatus.approval_status_code>		
		<cfset local.dataReviewDetail.approval_status_notes = arguments.dataReviewDetailStatus.approval_status_notes>		
		<cfset local.DataReviewDetail = setAuditProperties(local.dataReviewDetail, "modify")>
			
		<!--- Save the modified DataReview --->
		<cfset saveExistingDataReviewDetail(
				DataReviewDetail=local.DataReviewDetail)/>

		<!--- Retrieve the modified data review record --->	
		<cfset local.dataReviewDetail = getDataReviewDetail(local.dataReviewDetail)/>		
		
		<!--- Instantiate the data review history component --->
		<cfset local.dataReviewDetailHistory = getEmptyDataReviewDetailHistoryVo()>

		<!--- Audit the data review detail record --->
		<cfset local.dataReviewDetailHistory.detail_history_uid = createUniqueIdentifier()>
		<cfset local.dataReviewDetailHistory.review_detail_uid = local.dataReviewDetail.review_detail_uid>
		<cfset local.dataReviewDetailHistory.approval_status_code = local.dataReviewDetail.approval_status_code>
		<cfset local.dataReviewDetailHistory.approval_status_date = local.dataReviewDetail.approval_status_date>
		<cfset local.dataReviewDetailHistory.approval_status_notes = local.dataReviewDetail.approval_status_notes>
		<cfset local.dataReviewDetailHistory.is_active = local.dataReviewDetail.is_active>
		<cfset local.dataReviewDetailHistory.created_by = local.dataReviewDetail.created_by>
		<cfset local.dataReviewDetailHistory.created_date = local.dataReviewDetail.created_date>
		<cfset local.dataReviewDetailHistory.modified_by = local.dataReviewDetail.modified_by>
		<cfset local.dataReviewDetailHistory.modified_date = local.dataReviewDetail.modified_date>

		<!--- Populate the current user as the review participant --->
		<cfset local.dataReviewDetailHistory.review_participant_uid = this.dataReviewDelegate.getDataReviewParticipant(
				data_review_uid = local.dataReviewDetail.data_review_uid,
				user_uid = local.session.user_uid).review_participant_uid>
		
		<!--- Now save the data review object as a data review detail history record --->
		<cfset this.DataReviewDetailHistoryDelegate.saveNewDataReviewDetailHistory(
				dataReviewDetailHistory = local.dataReviewDetailHistory)/>

	</cffunction>
	
</cfcomponent>