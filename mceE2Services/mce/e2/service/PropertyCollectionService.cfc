<cfcomponent displayname="Property Collection Service"
			 extends="BaseDataService" output="true"
			 hint="The component is used to manage all business logic tied / associated with the management of property collection information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setPropertyCollectionDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property collection data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyCollectionDelegate" hint="Describes the *.cfc used to manage database interactions related to property collection information.">
		<cfset this.propertyCollectionDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyDelegate" hint="Describes the *.cfc used to manage database interactions related to property information.">
		<cfset this.propertyDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setSecurityDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to security relationships (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.SecurityDelegate" hint="Describes the *.cfc used to manage security relationship information.">
		<cfset this.securityDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyPropertyCollectionVo"
				access="public" returntype="mce.e2.vo.PropertyCollection"
				hint="This method is used to retrieve / return an empty property collection information value object.">
		<cfreturn this.propertyCollectionDelegate.getEmptyPropertyCollectionComponent()>
	</cffunction>

	<cffunction name="getClientCompanyPropertyCollections"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property collections associated to a given user.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the company used to retrieve property collections." />
			<cfreturn this.propertyCollectionDelegate.getPropertyCollectionsAsArrayOfComponents(
						client_company_uid=arguments.clientCompany.client_company_uid)>
	</cffunction>

	<cffunction name="getAssociatedPropertyCollections"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property collections associated to a given user.">

			<!--- Initialize the local scope --->
			<cfset var local = structNew()>

			<!--- Retrieve the user's session --->
			<cfset local.session = request.beanFactory.getBean("securityService").getSession()>

			<!--- Define the arguments structure --->
			<cfset local.args = structNew()>
			<cfset local.args.client_company_uid = local.session.client_company_uid>
			<cfset local.args.selectMethod = 'singular-inclusive'>

			<!--- Does the user have any property collections associated? --->
			<cfif listlen(local.session.propertyCollectionsList) gt 0>

				<!--- If so, then add them to the arguments structure --->
				<cfset local.args.property_collection_uid = local.session.propertyCollectionsList>

			</cfif>

			<!--- Retrieve associated collections by specifying the user --->
			<cfset local.returnResult = this.propertyCollectionDelegate.getPropertyCollectionsAsArrayOfComponents(
						argumentCollection=local.args)>

			<!--- Return the array --->
			<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getUnAssociatedPropertyCollections"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property collections within a company that are not associated to a given user.">

			<!--- Initialize the local scope --->
			<cfset var local = structNew()>

			<!--- Retrieve the user's session --->
			<cfset local.session = request.beanFactory.getBean("securityService").getSession()>

			<!--- Define the arguments structure --->
			<cfset local.args = structNew()>
			<cfset local.args.client_company_uid = local.session.client_company_uid>
			<cfset local.args.selectMethod = 'singular-exclusive'>

			<!--- Does the user have any property collections associated? --->
			<cfif listlen(local.session.propertyCollectionsList) gt 0>

				<!--- If so, then add them to the arguments structure --->
				<cfset local.args.property_collection_uid = local.session.propertyCollectionsList>

			</cfif>

			<!--- Retrieve unassociated collections by specifying the user --->
			<cfset local.returnResult = this.propertyCollectionDelegate.getPropertyCollectionsAsArrayOfComponents(
						argumentCollection=local.args)>

			<!--- Return the array --->
			<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getPropertyCollection"
				access="public" returntype="mce.e2.vo.PropertyCollection"
				hint="This method is used to return a populated property collection vo (value object).">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the properties of the property collection to be retrieved." />
		<cfargument name="byPassSecurity" type="boolean" required="true" default="false" hint="Describes whether the created property collection object should be returned without a property collection / user group association." />
		<cfreturn this.propertyCollectionDelegate.getPropertyCollectionAsComponent(
					property_collection_uid=arguments.propertyCollection.property_collection_uid,
					byPassSecurity=arguments.byPassSecurity)>
	</cffunction>

	<cffunction name="getPortfolioOverview"
				access="public" returntype="query"
				hint="This method is used to return a populated property collection vo (value object).">
			<cfargument name="property_collection_uid" type="string" required="true" hint="Describes the property collection key." />
			<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
			<cfargument name="period_start" type="string" required="true" hint="Describes the energy usage start period.">
			<cfargument name="period_end" type="string" required="true" hint="Describes the energy usage end period.">
			<cfargument name="energy_type_uids" type="string" required="false" hint="energy/resource types to pass.">
			
		<cfreturn this.propertyCollectionDelegate.getPortfolioOverview(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getPropertiesInACollection"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of properties associated to a given property collection.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection whose associated properties are being retrieved." />
		<cfargument name="byPassSecurity" type="boolean" required="true" default="false" hint="Describes whether or not the security layer for property collections should be bypassed.">
		<cfreturn this.propertyCollectionDelegate.getPropertiesInACollectionAsArrayOfComponents(
					property_collection_uid=arguments.propertyCollection.property_collection_uid,
					byPassSecurity=arguments.byPassSecurity)>
	</cffunction>

	<cffunction name="getPropertyCollectionPropertyAlertAndToDoCounts"
				access="public" returntype="array"
				hint="This method is used to return a collection of property collections associated to a specified user, based on what has been configured in the application.">
		<cfargument name="user_uid" type="string" required="true" hint="Describes the user VO object containing the properties of the user for which property collections will be retrieved." />
		<cfargument name="addAllPropertiesPseudoCollection" type="boolean" required="false" default="true">
		<cfargument name="allPropertiesPseudoCollectionName" type="string" required="false" default="All Properties">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset local.hasAllPropertiesCollectionAlready = false>

		<!--- Get the property collections associated to a given user --->
		<cfset local.propertyCollectionList = this.securityDelegate.getValidPropertyCollections(user_uid=arguments.user_uid)>

		<!--- Throw a hard error if we don't have any data to work with --->
		<cfif listLen(local.propertyCollectionList) eq 0>
			<cfthrow
				message="No property collections found."
				detail="No property collections available for user #user_uid#">
		</cfif>

		<!--- Get the counts associated to the property collections for a given user --->
		<cfset local.result = this.propertyCollectionDelegate.getPropertyCollectionPropertyAlertAndToDoCountsAsArrayOfComponents(
				property_collection_uid=local.propertyCollectionList)>


		<!--- Populate "propertyItems" array for each property collection --->
		<cfloop array="#local.result#" index="local.item">
			<cfset local.propertyCollectionArray = [local.item]>
			<cfset local.item.propertyItems = getPropertyAlertAndToDoCounts(local.propertyCollectionArray)>

			<cfif local.item.friendly_name eq arguments.allPropertiesPseudoCollectionName>
				<cfset local.hasAllPropertiesCollectionAlready = true>
			</cfif>
		</cfloop>

		<cfif arguments.addAllPropertiesPseudoCollection eq true>
			<cfif (not local.hasAllPropertiesCollectionAlready) and (ArrayLen(local.result) gt 1)>
				<cfset ArrayPrepend(local.result, this.propertyCollectionDelegate.makeAggregatedPropertyCollectionComponent(local.result, arguments.allPropertiesPseudoCollectionName))>
			</cfif>
		</cfif>

		<!--- Return the array containing the property collections --->
		<cfreturn local.result>
	</cffunction>

	<cffunction name="getPropertyAlertAndToDoCounts"
				access="public" returntype="array"
				hint="This method is used to return a collection of properties associated to a specified property collection, based on what has been configured in the application.">
		<cfargument name="propertyCollectionArray" type="array" required="true" hint="Describes the array collection containing the property collections for which associated properties will be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the property collections associated to a given user --->
		<cfset local.propertyCollectionList = buildVoPropertyListFromArray(
				voArray=arguments.propertyCollectionArray,
				propertyName="property_collection_uid")>

		<!--- Get the counts associated to the property collections for a given user --->
		<cfset local.result = this.propertyCollectionDelegate.getPropertyAlertAndToDoCountsAsArrayOfComponents(
				property_collection_uid=local.propertyCollectionList)>

		<!--- Return the array containing the property collections --->
		<cfreturn local.result>

	</cffunction>

	<!--- Author methods used to retrieve property collection data when the "propertySpecifics" option is selected --->
	<cffunction name="getPropertyCollectionsAndAssociatedProperties"
				access="public" returntype="array"
				hint="This method is used to retrieve all the property collections associated to a given user.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of property collections associated to the current user --->
		<cfset local.returnResult = getAssociatedPropertyCollections()>

		<!--- Loop over the property collections --->
		<cfloop from="1" to="#arraylen(local.returnResult)#" index="local.propertyCollectionIndex">

			<!--- Create an instance of the current property collection --->
			<cfset local.thisPropertyCollection = local.returnResult[local.propertyCollectionIndex]>

			<!--- Retrieve the properties in a given property collection --->
			<cfset local.propertyImageArray = getPropertiesInACollection(local.thisPropertyCollection)>

			<!--- Append the property array to each property collection --->
			<cfset local.returnResult[local.propertyCollectionIndex].propertiesArray = duplicate(local.propertyImageArray)>

		</cfloop>

		<!--- Return the property collection --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getPropertyCollectionAndAssociatedProperties"
				access="public" returntype="mce.e2.vo.PropertyCollection"
				hint="This method is used to retrieve a specific property collection and its properties.">

		<!--- Define the arguments for this method --->
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection whose associated properties are being retrieved." />
		<cfargument name="byPassSecurity" type="boolean" required="true" default="false" hint="Describes whether or not the security layer for property collections should be bypassed.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the specified property collection --->
		<cfset local.returnResult = getPropertyCollection(argumentCollection=arguments)>

		<!--- Retrieve the properties in a given property collection --->
		<cfset local.propertyImageArray = getPropertiesInACollection(argumentCollection=arguments)>

		<!--- Append the property array to each property collection --->
		<cfset local.returnResult.propertiesArray = duplicate(local.propertyImageArray)>

		<!--- Return the property collection --->
		<cfreturn local.returnResult>

	</cffunction>


	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewPropertyCollection"
				access="public" returntype="string"
				hint="This method is used to persist a new property collection record to the application database.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.propertyCollection = setAuditProperties(arguments.propertyCollection, "create")>

		<!--- Set the primary key for the property collection --->
		<cfset local.propertyCollection = setPrimaryKey(local.propertyCollection)>

		<!--- Save the property collection --->
		<cfset this.propertyCollectionDelegate.saveNewPropertyCollection(local.propertyCollection)>

		<!--- Associate the primary key to the active / current user --->
		<cfset associateUserToPropertyCollection(local.propertyCollection)>

		<!--- Save the modified property collection --->
		<cfset local.primaryKey = local.propertyCollection.property_collection_uid>

		<!--- Return the primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewPropertyCollection"
				access="public" returntype="mce.e2.vo.PropertyCollection"
				hint="This method is used to persist a new property collection record to the application database.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.primaryKey = saveNewPropertyCollection(arguments.propertyCollection)>

		<!--- Set the primary key for the property collection --->
		<cfset arguments.propertyCollection.property_collection_uid = local.primaryKey>

		<!--- Return the propertyCollection objcet --->
		<cfreturn getPropertyCollection(arguments.propertyCollection,true)>

	</cffunction>

	<cffunction name="saveExistingPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to save an existing property collection information to the application database.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.propertyCollection = setAuditProperties(arguments.propertyCollection, "modify")>

		<!--- Save the modified property collection --->
		<cfset this.PropertyCollectionDelegate.saveExistingPropertyCollection(propertyCollection=local.propertyCollection)/>

	</cffunction>

	<cffunction name="associateUserToPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to associate a user to a given property collection; if no user is specified the user from the active session is used to create the association.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection that a user will be associated to." />
		<cfargument name="user" type="any" required="false" hint="Describes the user VO object containing the details of the user being associated to a given property collection." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Determine if the user object was passed to this method --->
		<cfif not structKeyExists(arguments, 'user') or ( structKeyExists(arguments, 'user') and not isInstanceOf(arguments.user, 'mce.e2.vo.User'))>

			<!--- Create an instance of the bean factory --->
			<cfset local.sessionService = request.beanFactory.getBean("securityService")>

			<!--- Get the active session --->
			<cfset local.session = local.sessionService.getSession()>

			<!--- If no active user is found, then throw an error --->
			<cfif not isUniqueIdentifier(local.session.user_uid)>

				<!--- Log the error and throw an exception --->
				<cfset request.e2.logger.error("No valid User_UID property found in the active session.", "The active session does not have a valid user_uid value; a valid value must be available in order to create an association between a property collection and user.")>

			</cfif>

			<!--- Mockup a user object, and populate it with a valid user_uid --->
			<cfset arguments.user = structNew()>
			<cfset arguments.user.user_uid = local.session.user_uid>

		</cfif>

		<!--- Build out the association structure that will be used to create the property / property collection assocation --->
		<cfset local.assocObject = structNew()>

		<!--- Define the properties of the association object --->
		<cfset local.assocObject.property_collection_uid = arguments.propertyCollection.property_collection_uid>
		<cfset local.assocObject.user_uid = arguments.user.user_uid>
		<cfset local.assocObject.relationship_start = createOdbcDateTime(now())>
		<cfset local.assocObject.relationship_end = ''>

		<!--- Set the created by / date audit properties --->
		<cfset local.userPropertyCollectionAssociation = setAuditProperties(local.assocObject, "create")>

		<!--- Create the association --->
		<cfset this.propertyCollectionDelegate.associateUserToPropertyCollection(
				associationObject=local.userPropertyCollectionAssociation)>

	</cffunction>

	<cffunction name="disAssociateUserFromPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to remove the association between a user and a given property collection; if no user is specified the user from the active session is used to remove the association.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection associated to a given user." />
		<cfargument name="user" type="any" required="false" hint="Describes the user VO object containing the details of the user whose association is being removed from a property collection." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Determine if the user object was passed to this method --->
		<cfif not structKeyExists(arguments, 'user') or ( structKeyExists(arguments, 'user') and not isInstanceOf(arguments.user, 'mce.e2.vo.User'))>

			<!--- Create an instance of the bean factory --->
			<cfset local.sessionService = request.beanFactory.getBean("securityService")>

			<!--- Get the active session --->
			<cfset local.session = local.sessionService.getSession()>

			<!--- If no active user is found, then throw an error --->
			<cfif not isUniqueIdentifier(local.session.user_uid)>

				<!--- Log the error and throw an exception --->
				<cfset request.e2.logger.error("No valid User_UID property found in the active session.", "The active session does not have a valid user_uid value; a valid value must be available in order to create an association between a property collection and user.")>

			</cfif>

			<!--- Mockup a user object, and populate it with a valid user_uid --->
			<cfset arguments.user = structNew()>
			<cfset arguments.user.user_uid = local.session.user_uid>

		</cfif>

		<!--- Build out the association structure that will be used to create the property / property collection assocation --->
		<cfset local.assocObject = structNew()>

		<!--- Define the properties of the association object --->
		<cfset local.assocObject.property_collection_uid = arguments.propertyCollection.property_collection_uid>
		<cfset local.assocObject.user_uid = arguments.user.user_uid>

		<!--- Retrieve the association query --->
		<cfset local.qAssocObject = this.propertyCollectionDelegate.getUserAndPropertyCollectionAssociation(
				argumentCollection=local.assocObject)>

		<!--- Build out the association object --->
		<cfset local.assocObject = queryRowToStruct(local.qAssocObject, 1)>

		<!--- Deactivate the association between the user / property collection --->
		<cfset local.assocObject = deActivate(local.assocObject)>

		<!--- Set the created by / date audit properties --->
		<cfset local.userPropertyCollectionAssociation = setAuditProperties(local.assocObject, "modify")>

		<!--- Create the association --->
		<cfset this.propertyCollectionDelegate.disAssociateUserFromPropertyCollection(
				associationObject=local.userPropertyCollectionAssociation)>

	</cffunction>

	<cffunction name="associatePropertyToPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to associate a property to a given property collection.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection that a property will be associated to." />
		<cfargument name="property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the details of the property being associated to a property collection." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Build out the association structure that will be used to create the property / property collection assocation --->
		<cfset local.assocObject = structNew()>

		<!--- Define the properties of the association object --->
		<cfset local.assocObject.property_collection_uid = arguments.propertyCollection.property_collection_uid>
		<cfset local.assocObject.property_uid = arguments.property.property_uid>

		<!--- Test to see if an association between a property / property collection already exists --->
		<cfset local.assocRecord = this.propertyCollectionDelegate.getPropertyAndPropertyCollectionAssociation(
				argumentCollection=local.assocObject)>

		<!--- Was an active association found? --->
		<cfif local.assocRecord.recordCount eq 0>

			<!--- Define the additional properties of the association --->
			<cfset local.assocObject.relationship_start = createOdbcDateTime(now())>
			<cfset local.assocObject.relationship_end = ''>

			<!--- Set the created by / date audit properties --->
			<cfset local.propertyPropertyCollectionAssociation = setAuditProperties(local.assocObject, "create")>

			<!--- Create the association --->
			<cfset this.propertyCollectionDelegate.associatePropertyToPropertyCollection(
					associationObject=local.propertyPropertyCollectionAssociation)>

		<cfelse>

			<!--- If so, then throw an error --->
			<cfinvoke
				component="#request.e2.logger#"
				method="error"
				message="An instance of the property [#arguments.property.friendly_name#] is already associated to the property collection [#arguments.propertyCollection.friendly_name#]."
				detail="Multiple instances of the same property cannot be associated to a given property collection.  Please either modify the existing property, or remove it so that the new one can be added.">

		</cfif>

	</cffunction>

	<cffunction name="disAssociatePropertyFromPropertyCollection"
				access="public" returntype="void"
				hint="This method is used to remove the association between a property and a given property collection.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection associatd to a given property" />
		<cfargument name="property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the details of the property whose association is being removed from a property collection." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Build out the association structure that will be used to create the property / property collection assocation --->
		<cfset local.assocObject = structNew()>

		<!--- Define the properties of the association object --->
		<cfset local.assocObject.property_collection_uid = arguments.propertyCollection.property_collection_uid>
		<cfset local.assocObject.property_uid = arguments.property.property_uid>

		<!--- Get the association object --->
		<cfset local.assocObject = getPropertyAndPropertyCollectionAssociation(
				propertyCollection=arguments.propertyCollection,
				property=arguments.property)>

		<!--- Set the created by / date audit properties --->
		<cfset local.propertyPropertyCollectionAssociation = setAuditProperties(local.assocObject, "modify")>

		<!--- Deactivate the association between the user / property collection --->
		<cfset local.assocObject = deActivate(local.assocObject)>

		<!--- Modify the association --->
		<cfset this.propertyCollectionDelegate.disAssociatePropertyFromPropertyCollection(
				associationObject=local.propertyPropertyCollectionAssociation)>

	</cffunction>

	<cffunction name="getPropertyAndPropertyCollectionAssociation"
				access="public" returntype="struct"
				hint="This method is used to retrieve the association between a property and a given property collection.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection associatd to a given property" />
		<cfargument name="property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the details of the property whose association is being removed from a property collection." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create a reference to the association object --->
		<cfset local.qAssocObject = this.propertyCollectionDelegate.getPropertyAndPropertyCollectionAssociation(
				property_collection_uid=arguments.propertyCollection.property_collection_uid,
				property_uid = arguments.property.property_uid)>

		<!--- Build out the association object --->
		<cfset local.assocObject = queryRowToStruct(local.qAssocObject, 1)>

		<!--- Return the association object --->
		<cfreturn local.assocObject>

	</cffunction>

	<cffunction name="getUserAndPropertyCollectionAssociation"
				access="public" returntype="struct"
				hint="This method is used to treive the association between a property and a given user.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the property collection VO object containing the details of the property collection associatd to a given property" />
		<cfargument name="user" type="mce.e2.vo.User" required="false" hint="Describes the user VO object containing the details of the user whose association is being removed from a property collection." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create a reference to the association object --->
		<cfset local.qAssocObject = this.propertyCollectionDelegate.getUserAndPropertyCollectionAssociation(
				property_collection_uid=arguments.propertyCollection.property_collection_uid,
				user_uid = arguments.user.user_uid)>

		<!--- Build out the association object --->
		<cfset local.assocObject = queryRowToStruct(local.qAssocObject, 1)>

		<!--- Return the association object --->
		<cfreturn local.assocObject>

	</cffunction>

</cfcomponent>