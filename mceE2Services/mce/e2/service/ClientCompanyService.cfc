<cfcomponent displayname="Client Company Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of client company information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setClientCompanyDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage client company data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientCompanyDelegate" hint="Describes the *.cfc used to manage database interactions related to client company information.">
		<cfset this.clientCompanyDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setClientContactDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage client contact data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientContactDelegate" hint="Describes the *.cfc used to manage database interactions related to client contact information.">
		<cfset this.clientContactDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setCompanyRoleDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage company role data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.CompanyRoleDelegate" hint="Describes the *.cfc used to manage database interactions related to company role information.">
		<cfset this.companyRoleDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setUserDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage user data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserDelegate" hint="Describes the *.cfc used to manage database interactions related to user information.">
		<cfset this.userDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setUserGroupDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage user group data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserGroupDelegate" hint="Describes the *.cfc used to manage database interactions related to user group information.">
		<cfset this.userGroupDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setCompanyAddressDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage company address data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.CompanyAddressDelegate" hint="Describes the *.cfc used to manage database interactions related to company address information.">
		<cfset this.companyAddressDelegate = arguments.bean>
	</cffunction>
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyClientCompanyVo"
				access="public" returntype="mce.e2.vo.ClientCompany"
				hint="This method is used to retrieve / return an empty client company information value object.">
		<cfreturn this.clientCompanyDelegate.getEmptyClientCompanyComponent()>
	</cffunction>

	<cffunction name="getEmptyCompanyRoleVo"
				access="public" returntype="mce.e2.vo.CompanyRole"
				hint="This method is used to retrieve / return an empty company role information value object.">
		<cfreturn this.companyRoleDelegate.getEmptyCompanyRoleComponent()>
	</cffunction>

	<cffunction name="getClientCompanies"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client company value objects.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<cfreturn this.clientCompanyDelegate.getClientCompaniesAsArrayOfComponents(
					relationship_filter_date=arguments.relationship_filter_date,
					selectMethod=arguments.selectMethod)>
	</cffunction>

	<cffunction name="getClientCompaniesAsQuery"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of client company value objects.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<cfreturn this.clientCompanyDelegate.getClientCompany(
					relationship_filter_date=arguments.relationship_filter_date,
					includeRoleInformation=true,
					selectMethod=arguments.selectMethod, getDescendants=true)>
	</cffunction>

	<cffunction name="getCompanyHierarchy"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of client company value objects.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<cfreturn this.clientCompanyDelegate.getClientCompany(client_company_uid=arguments.clientCompany.client_company_uid,
					relationship_filter_date=arguments.relationship_filter_date,
					selectMethod=arguments.selectMethod,getDescendants=true)>
	</cffunction>

	<cffunction name="getCompanyProperties"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client company Property value objects.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<cfreturn this.clientCompanyDelegate.getClientCompanyPropertiesAsArrayOfComponents(client_company_uid=arguments.clientCompany.client_company_uid,
					relationship_filter_date=arguments.relationship_filter_date,
					selectMethod=arguments.selectMethod)>
	</cffunction>

	<cffunction name="getChildCompaniesAndProperties"
				access="public" returntype="Query"
				hint="This method is used to retrieve / return a collection of client companies and properties.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<cfreturn this.clientCompanyDelegate.getChildCompaniesAndProperties(client_company_uid=arguments.clientCompany.client_company_uid,
					relationship_filter_date=arguments.relationship_filter_date,
					selectMethod=arguments.selectMethod)>
	</cffunction>

	<cffunction name="getCompanyUsers"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client company User value objects.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<cfreturn this.clientCompanyDelegate.getClientCompanyUsersAsArrayOfComponents(client_company_uid=arguments.clientCompany.client_company_uid,
					relationship_filter_date=arguments.relationship_filter_date,
					selectMethod=arguments.selectMethod)>
	</cffunction>

	<cffunction name="getClientCompaniesByProperty"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client company value objects.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="false" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfset var client_company_uid = "">
		<cfif structKeyExists(arguments, "clientCompany")>
			<cfset client_company_uid = arguments.clientCompany.client_company_uid>
		</cfif>
		<cfreturn this.clientCompanyDelegate.getClientCompaniesAsArrayOfComponents(
				client_company_uid=client_company_uid,
				property_uid=arguments.Property.property_uid,
				relationship_filter_date=arguments.relationship_filter_date,
				selectMethod=arguments.selectMethod,
				includeRoleInformation=true)>
	</cffunction>

	<cffunction name="getClientCompany"
				access="public" returntype="mce.e2.vo.ClientCompany"
				hint="This method is used to return a populated client company vo (value object).">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfargument name="getDescendants" type="boolean" required="true" default="false" hint="Whether to include companies in the hierarchy of the provided client company uid">
		<cfreturn this.clientCompanyDelegate.getClientCompanyAsComponent(client_company_uid=arguments.clientCompany.client_company_uid,getDescendants=arguments.getDescendants)>
	</cffunction>

	<cffunction name="getCompanyTypes"
				access="public" returntype="mce.e2.vo.ClientCompany"
				hint="This method is used to return a populated client company vo (value object).">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />
		<cfreturn this.clientCompanyDelegate.getClientCompanyAsComponent(client_company_uid=arguments.clientCompany.client_company_uid)>
	</cffunction>

	<!--- Create methods to retrieve company-related collections --->
	<cffunction name="getUsers"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user value objects for a given client company.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company to which users are associated to.">
		<cfreturn this.userDelegate.getUsersAsArrayOfComponents(client_company_uid=arguments.clientCompany.client_company_uid)>
	</cffunction>

	<cffunction name="getUserGroups"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user group value objects.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company to which users are associated to.">
		<cfreturn this.userGroupDelegate.getUserGroupsAsArrayOfComponents(
					client_company_uid=arguments.clientCompany.client_company_uid)>
	</cffunction>



	<cffunction name="getClientCompaniesAndContactsByProperty"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client contacts organized by company.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the client companies by property --->
		<cfset local.clientCompanies = getClientCompaniesByProperty(
				argumentCollection=arguments)>

		<!--- Create a list of the client company primary keys --->
		<cfset local.clientCompanyUidList = buildVoPropertyListFromArray(local.clientCompanies, 'client_company_uid')>

		<!--- Get the client contacts by property and company --->
		<cfset local.clientContacts = this.clientContactDelegate.getClientContactsAsArrayOfComponents(
				property_uid=arguments.Property.property_uid,
				client_company_uid=local.clientCompanyUidList,
				relationship_filter_date=arguments.relationship_filter_date,
				selectMethod=arguments.selectMethod)>

		<!--- Initialize the output array --->
		<cfset local.returnResult = arrayNew(1)>

		<!--- Loop over the client company array, and add the client companies to the return result --->
		<cfloop from="1" to="#arrayLen(local.clientCompanies)#" index="local.clientCompanyIndex">

			<!--- Create a reference to the current / active client company --->
			<cfset local.thisClientCompany = local.clientCompanies[local.clientCompanyIndex]>

			<!--- Add the client company object to the return result --->
			<cfset arrayAppend(local.returnResult, local.thisClientCompany)>

			<!--- Loop over the client contacts, and append them to the return result in order of company --->
			<cfloop from="1" to="#arrayLen(local.clientContacts)#" index="local.clientContactIndex">

				<!--- Create a reference to the current / active client contact --->
				<cfset local.thisClientContact = local.clientContacts[local.clientContactIndex]>

				<!--- Is the present contact in the active company?  If so, then add it to the return result --->
				<cfif local.thisClientCompany.client_company_uid eq local.thisClientContact.client_company_uid>

					<!--- Append the client contact to the return result array --->
					<cfset arrayAppend(local.returnResult, local.thisClientContact)>

				<cfelse>

					<!--- Otherwise, break out of the loop and move onto the next client company --->
					<cfbreak>

				</cfif>

			</cfloop>

		</cfloop>

		<!--- Return the result --->
		<cfreturn local.returnResult>

	</cffunction>

		<cffunction name="getUsersInCompaniesForProperty"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Users for a given property.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the client companies by property --->
		<cfset local.clientCompanies = getClientCompaniesByProperty(
				argumentCollection=arguments)>

		<!--- Create a list of the client company primary keys --->
		<cfset local.clientCompanyUidList = buildVoPropertyListFromArray(local.clientCompanies, 'client_company_uid')>

		<!--- Sanity check --->
		<cfif local.clientCompanyUidList eq "">
			<cfthrow message="No companies appear to be associated with the property that was provided, so no users can be retrieved.">
		</cfif>

		<!--- Get the client contacts by property and company --->
		<cfset local.users = this.clientCompanyDelegate.getClientCompanyUsersAsArrayOfComponents(
				client_company_uid=local.clientCompanyUidList)>

		<!--- Return the result --->
		<cfreturn local.users>

	</cffunction>


	<cffunction name="getAllCompanyRoles"
				access="public" returntype="Array"
				hint="This method is used to return an array of all client roles vo (value object) not limited by company.">
		<cfreturn this.companyRoleDelegate.getCompanyRolesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAvailableCompanyRoles"
				access="public" returntype="Array"
				hint="This method is used to return an array of all client roles vo (value object) for a given company.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company for which company roles will be retrieved.">
		<cfreturn this.companyRoleDelegate.getCompanyRolesAsArrayOfComponents(
					client_company_uid=arguments.clientCompany.client_company_uid)>
	</cffunction>

	<cffunction name="getCompanyAddresses"
				access="public" returntype="array"
				hint="This method is used to return a collection of associated company addresses.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object used to retrieve associated addresses." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.companyAddressDelegate.getCompanyAddressesAsArrayOfComponents(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>

	<cffunction name="getNotesByContext"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of notes associated to companies or properties.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object used to retrieve client notes." />
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve client notes." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="showReminders" type="boolean" required="true" default="true" hint="Describes whether or not reminders should be included.">
		<cfargument name="showNonReminders" type="boolean" required="true" default="false" hint="Describes whether or not non-reminders should be included.">
		<cfargument name="dueIn" type="string" required="true" default="7" hint="Describes which reminders to get.">
		<cfargument name="includeDescendants" type="boolean" required="true" default="true" hint="Describes whether or not to include children.">
		<cfreturn this.ClientCompanyDelegate.getNotesByContext(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date,
					showReminders=arguments.showReminders,
					showNonReminders=arguments.showNonReminders,
					dueIn=arguments.dueIn,
					includeDescendants=arguments.includeDescendants)>
	</cffunction>


	<cffunction name="getRootClientForCompany"
				access="public" returntype="query"
				hint="This method is used to return a collection of associated company addresses.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object used to retrieve associated addresses." />
		<cfreturn this.clientCompanyDelegate.getRootClientForCompany(
					clientCompany=arguments.ClientCompany)>
	</cffunction>
	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewClientCompany"
				access="public" returntype="string"
				hint="This method is used to persist a new client company record to the application database.  The method returns the primary key of the client company that was crated.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.clientCompany = setAuditProperties(arguments.clientCompany, "create")>

		<!--- Set the primary key for the client company if it's not already set --->
		<cfset local.clientCompany = setPrimaryKey(local.clientCompany)>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.clientCompany.client_company_uid>

		<!--- Create the clientCompany --->
		<cfset this.clientCompanyDelegate.saveNewClientCompany(clientCompany=local.clientCompany)>

		<!--- Return the new clientCompany primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewClientCompany"
				access="public" returntype="mce.e2.vo.ClientCompany"
				hint="This method is used to persist a new client company record to the application database.  The method returns the primary key of the client company that was crated.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create the clientCompany --->
		<cfset local.primaryKey = saveNewClientCompany(clientCompany=arguments.clientCompany)>

		<!--- Set the primary key for the client company --->
		<cfset arguments.clientCompany.client_company_uid = local.primaryKey>

		<!--- Retrieve the new client company --->
		<cfset local.clientCompany = getClientCompany(clientCompany=arguments.clientCompany)>

		<!--- Return the new clientCompany --->
		<cfreturn local.clientCompany>

	</cffunction>

	<cffunction name="saveExistingClientCompany"
				access="public" returntype="void"
				hint="This method is used to save existing client company information to the application database.">
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the properties of the client company to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.clientCompany = setAuditProperties(arguments.clientCompany, "modify")>

		<!--- Modify the clientCompany --->
		<cfset this.clientCompanyDelegate.saveExistingClientCompany(clientCompany=local.clientCompany)/>

	</cffunction>

	<cffunction name="saveNewCompanyAddress"
				access="public" returntype="string"
				hint="This method is used to persist a new company address record to the application database.">
		<cfargument name="CompanyAddress" type="mce.e2.vo.CompanyAddress" required="true" hint="Describes the company address VO object containing the details of the company address to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.companyAddress = setAuditProperties(arguments.companyAddress, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.companyAddress = setPrimaryKey(local.companyAddress)>

		<!--- Save the modified company address --->
		<cfset this.CompanyAddressDelegate.saveNewCompanyAddress(companyAddress=local.companyAddress)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.companyAddress.company_address_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewCompanyAddress"
				access="public" returntype="mce.e2.vo.CompanyAddress"
				hint="This method is used to persist a new company address record to the application database.">
		<cfargument name="CompanyAddress" type="mce.e2.vo.CompanyAddress" required="true" hint="Describes the company address VO object containing the details of the company address to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewCompanyAddress(companyAddress=arguments.companyAddress)>

		<!--- Set the primary key --->
		<cfset arguments.companyAddress.company_address_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.companyAddress>

	</cffunction>

	<cffunction name="saveExistingCompanyAddress"
				access="public" returntype="mce.e2.vo.CompanyAddress"
				hint="This method is used to save an existing company address information to the application database.">
		<cfargument name="CompanyAddress" type="mce.e2.vo.CompanyAddress" required="true" hint="Describes the company address VO object containing the company address definition to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.companyAddress = setAuditProperties(arguments.CompanyAddress, "modify")>

		<!--- if they blanked out the end-date, it will not be saved, so do it explicitly here--->
		<cfif not structKeyExists(local.companyAddress, 'relationship_end')>
				<cfset local.companyAddress.relationship_end = ''>
		</cfif>

		<!--- Save the modified property --->
		<cfset this.CompanyAddressDelegate.saveExistingCompanyAddress(companyAddress=arguments.companyAddress)/>

		<!--- Return the modified company address --->
		<cfreturn local.companyAddress>

	</cffunction>

	<cffunction name="changeCompanyFiling"
				access="public" returntype="void"
				hint="This method is used to change the hierarchy for a given Client Company">
			<cfargument name="parent_company_uid" type="String" required="true" hint="Describes the Parent client company primary key/uid">
			<cfargument name="child_company_uid" type="String" required="true" hint="Describes the Child client company primary key/uid">
			<cfargument name="company_type_code" type="String" required="true" hint="Describes the company type code to file the child as">

			<cfset this.clientCompanyDelegate.changeCompanyFiling(argumentCollection=arguments)/>
	</cffunction>

	<cffunction name="changeCompanyFriendlyName"
				access="public" returntype="void"
				hint="This method is used to change the hierarchy for a given Client Company">
			<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company value object"/>

			<cfset this.clientCompanyDelegate.changeCompanyFriendlyName(clientCompany=arguments.clientCompany)/>
	</cffunction>

	<cffunction name="associateClientContactToClientCompany"
				access="public"
				returntype="void"
				hint="This method is used to process changes to a collection of client contacts to a specified client company.">

		<!--- Define the arguments for this method --->
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true">
		<cfargument name="clientContact" type="mce.e2.vo.ClientContact" required="true"/>
		<cfargument name="clientContactRole" type="mce.e2.vo.ClientContactRole" required="true"/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>
		<cfset local.assocObject.client_contact_uid = arguments.clientContact.client_contact_uid>
		<cfset local.assocObject.client_company_uid = arguments.clientCompany.client_company_uid>
		<cfset local.assocObject.contact_role_uid = arguments.clientContactRole.contact_role_uid>
		<cfset local.assocObject.relationship_start = now()>
		<cfset local.assocObject = setAuditProperties(local.assocObject, "create")>

		<cfset this.clientCompanyDelegate.associateClientContactToClientCompany(assocObject = local.assocObject)>

	</cffunction>

	<cffunction name="disAssociateClientContactFromClientCompany"
				access="public"
				returntype="void"
				hint="This method is used to remove a client contact from a specified client company.">

		<!--- Define the arguments for this method --->
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="true">
		<cfargument name="clientContact" type="mce.e2.vo.ClientContact" required="true"/>
		<cfargument name="clientContactRole" type="mce.e2.vo.ClientContactRole" required="true"/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the association object --->
		<!---<cfset local.assocObject = this.clientContactDelegate.getClientContactAsComponent(
						client_contact_uid = arguments.clientContact.client_contact_uid,
						client_company_uid = arguments.clientCompany.client_company_uid,
						contact_role_uid = arguments.clientContactRole.contact_role_uid)>--->
		<cfset local.assocObject.relationship_id = this.clientContactDelegate.getClientContactCompanyRelationshipId(
						client_contact_uid = arguments.clientContact.client_contact_uid,
						client_company_uid = arguments.clientCompany.client_company_uid,
						contact_role_uid = arguments.clientContactRole.contact_role_uid)>
		<cfset local.assocObject.client_contact_uid = arguments.clientContact.client_contact_uid>
		<cfset local.assocObject.client_company_uid = arguments.clientCompany.client_company_uid>
		<cfset local.assocObject.contact_role_uid = arguments.clientContactRole.contact_role_uid>
		<cfset local.assocObject.is_active = 0>
		<cfset local.assocObject.relationship_end = now()>
		<cfset local.assocObject = setAuditProperties(local.assocObject, "modify")>

		<cfset this.clientCompanyDelegate.disAssociateClientContactFromClientCompany(assocObject = local.assocObject)>

	</cffunction>

	<cffunction name="saveNewClientNote"
				access="public" returntype="string"
				hint="This method is used to persist a new client note record to the application database.  The method returns the primary key of the client note that was crated.">
		<cfargument name="clientNote" type="mce.e2.vo.ClientNote" required="true" hint="Describes the client note VO object containing the properties of the client note to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.clientNote = setAuditProperties(arguments.clientNote, "create")>

		<!--- Set the primary key for the client note if it's not already set --->
		<cfset local.clientNote = setPrimaryKey(local.clientNote)>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.clientNote.client_note_uid>

		<!--- Create the clientNote --->
		<cfset this.clientCompanyDelegate.saveNewClientNote(clientNote=local.clientNote)>

		<!--- Return the new clientNote primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveExistingClientNote"
				access="public" returntype="void"
				hint="This method is used to save existing client note information to the application database.">
		<cfargument name="clientNote" type="mce.e2.vo.ClientNote" required="true" hint="Describes the client note VO object containing the properties of the client note to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.clientNote = setAuditProperties(arguments.clientNote, "modify")>
		<cfif NOT StructKeyExists(local.clientNote,'reminder_date')>
			<cfset local.clientNote.reminder_date = ''>
		</cfif>
		<cfif NOT StructKeyExists(local.clientNote,'completed_date')>
			<cfset local.clientNote.completed_date = ''>
		</cfif>
		<cfif NOT StructKeyExists(local.clientNote,'completed_user_uid')>
			<cfset local.clientNote.completed_user_uid = ''>
		</cfif>

		<!--- Modify the clientCompany --->
		<cfset this.clientCompanyDelegate.saveExistingClientNote(clientNote=local.clientNote)/>

	</cffunction>

</cfcomponent>