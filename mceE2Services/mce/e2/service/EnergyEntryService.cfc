<cfcomponent displayName="Energy Entry Service"
			 extends="BaseDataService" output="false"
			 hint="This component is used to manage all service interactions with energy entry data.">

	<!--- Constructor (Autowired by Coldspring IOC/DI framework) --->
	<cffunction name="init">
		<cfargument name="energyEntryDelegate" type="mce.e2.db.EnergyEntryDelegate" required="true">
		<cfargument name="energyUsageDelegate" type="mce.e2.db.EnergyUsageDelegate" required="true">
		<cfargument name="rateModelBridge" type="mce.e2.ratemodel.RateModelBridge" required="true">

		<cfset super.init()>
		<cfset this.energyEntryDelegate = arguments.energyEntryDelegate>
		<cfset this.energyUsageDelegate = arguments.energyUsageDelegate>
		<cfset this.rateModelBridge = arguments.rateModelBridge>

		<cfreturn this>
	</cffunction>


	<!--- Used by the UI to show the data entry form for energy usage for which there is no record yet --->
	<cffunction name="getEnergyUsageVo"
				access="public" returntype="mce.e2.vo.EnergyEntryInfo"
				hint="This method is used to retrieve an energy usage value object.">
		<cfargument name="energy_usage_uid" type="string" required="true">
		<cfargument name="priorPeriods" type="array" required="false">
		<cfargument name="priorPeriodDefaultOffsets" type="string" required="false" default="12">
		<cfargument name="usageTypeCodeForDefaultPriorPeriods" type="string" required="false" default="actual">

		<cfset arguments.userPermissions = getPermissions()>

		<cfreturn this.energyEntryDelegate.getEnergyUsageRecordAsComponent(argumentCollection=arguments)>
	</cffunction>

	<!--- Used by the UI to show the data entry form for energy usage for which there is no record yet --->
	<cffunction name="getEmptyEnergyUsageVo"
				access="public" returntype="mce.e2.vo.EnergyEntryInfo"
				hint="This method is used to retrieve an empty energy usage value object.">
		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		<cfargument name="priorPeriods" type="array" required="false">
		<cfargument name="priorPeriodDefaultOffsets" type="string" required="false" default="12">
		<cfargument name="usageTypeCodeForDefaultPriorPeriods" type="string" required="false" default="actual">

		<cfset arguments.userPermissions = getPermissions()>
		
		<cfreturn this.energyEntryDelegate.getEmptyEnergyUsageRecordAsComponent(argumentCollection=arguments)>
	</cffunction>

	<!--- Used by the UI when the user saves an energy usage record --->
	<cffunction name="saveNewEnergyUsageEntry"
				access="public" returnType="string"
				hint="This method is used to save / create a new energy entry usage entry.">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" hint="Describes the energy entry usage entry that will be save / modified.">
		<cfargument name="allowRateModelRun" type="boolean" required="false" default="true">
		<cfargument name="swallowRateModelErrors" type="boolean" required="false" default="true">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.entryInfo = setAuditProperties(arguments.entryInfo, "create")>
		<cfset local.user_uid = getUserUid()>

		<!--- Run the rate model engine and merge values from the engine's response into the metaFields in this entryInfo --->
		<cfif arguments.allowRateModelRun>
			<cfset mergeEnergyUsageMetasFromRateModel(local.entryInfo, arguments.swallowRateModelErrors)>
		</cfif>

		<!--- Looks at certain meta values, adds up the sums, and places into total_cost and recorded_value at EnergyInfo level --->
		<cfset calculateTotalCostAndRecordedValue(local.entryInfo)>

		<!--- Update the application database --->
		<cfreturn this.energyEntryDelegate.saveNewEnergyUsageEntry(local.entryInfo, local.user_uid)>
	</cffunction>

	<!--- Used by the UI when the user saves an energy usage record --->
	<cffunction name="saveExistingEnergyUsageEntry"
				access="public" returntype="mce.e2.vo.EnergyEntryInfo"
				hint="This method is used to save / modify an existing energy entry usage entry.">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" hint="Describes the energy entry usage entry that will be save / modified.">
		<cfargument name="addHistory" required="false" type="boolean" default="true" hint="Allow internal users to change the status without affecting the history."/>
		<cfargument name="allowRateModelRun" type="boolean" required="false" default="true">
		<cfargument name="swallowRateModelErrors" type="boolean" required="false" default="true">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.entryInfo = setAuditProperties(arguments.entryInfo, "modify")>
		<cfset local.user_uid = getUserUid()>

		<!--- Run the rate model engine and merge values from the engine's response into the metaFields in this entryInfo --->
		<cfif arguments.allowRateModelRun>
			<cfset mergeEnergyUsageMetasFromRateModel(local.entryInfo, arguments.swallowRateModelErrors)>
		</cfif>

		<!--- Looks at certain meta values, adds up the sums, and places into total_cost and recorded_value at EnergyInfo level --->
		<cfset calculateTotalCostAndRecordedValue(local.entryInfo)>
		
		<!--- Update the application database --->
		<cfset this.energyEntryDelegate.saveExistingEnergyUsageEntry(local.entryInfo, arguments.addHistory, local.user_uid)>

		<!--- Return the modified value object --->
		<cfreturn local.entryInfo>
	</cffunction>

	<!--- Used by the UI when the user deletes an energy usage record --->
	<cffunction name="deleteExistingEnergyUsageEntry"
				access="public" returntype="void"
				hint="This method is used to save / modify an existing energy entry usage entry.">
		<cfargument name="energy_usage_uid" type="string" hint="Describes the energy entry usage entry that will be deleted.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.user_uid = getUserUid()>

		<!--- Update the application database --->
		<cfset this.energyEntryDelegate.deleteExistingEnergyUsageEntry(arguments.energy_usage_uid, local.user_uid)>

	</cffunction>
	
	<!--- Used by the UI to show the data entry form for energy usage for which there is no record yet --->
	<cffunction name="getPotentialDuplicateRecords"
				access="public" returntype="array"
				hint="This method is used to determine whether an energy usage record already exists for a given time period.">
		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		<cfargument name="usage_type_code" type="string" required="false" default="actual">

		<cfset var potentialDuplicates = ArrayNew(1)>
		<cfset var qPotentialDuplicates = this.energyEntryDelegate.getPotentialDuplicateRecords(argumentCollection=arguments)>

		<cfif qPotentialDuplicates.recordCount gt 0>
			<cfset potentialDuplicates = this.energyUsageDelegate.getEnergyUsageAsArrayOfComponents(energy_usage_uid=ValueList(qPotentialDuplicates.energy_usage_uid))>
		</cfif>

		<cfreturn potentialDuplicates>
	</cffunction>
	
	
	<cffunction name="isOkayToSave" access="public" returntype="struct">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" hint="Describes the energy entry usage entry that will be save / modified.">

		<!--- Start off assuming that all is okay --->
		<cfset var result = {status=true, message="", fatal=false}>
		<cfset var qOtherRecords = "">
		<cfset var priorMonth = "">
		<cfset var isNewRecord = entryInfo.energy_usage_uid eq "">

		<!--- See if there are any entries with the same report date --->
		<cfset qOtherRecords = this.energyEntryDelegate.getPotentialDuplicateRecords(
			energy_account_uid=entryInfo.energy_account_uid, 
			report_date=entryInfo.report_date, 
			not_energy_usage_uid=entryInfo.energy_usage_uid)>
		<!--- If we found any with same report date, send that back --->	
		<cfif qOtherRecords.recordCount gt 0>
			<cfset result = {status=false, message="There is already another energy entry record on file with that File Under date. Are you sure you want to continue?", fatal=false}>
			<cfreturn result>
		</cfif>
		
		<!--- Now we will do another set of checks, as long as the account is not delivery-based (like oil) --->
		<cfif not entryInfo.is_delivery_based>
		
			<!--- See whether there are any usage records for this account for the PRIOR report_date month (aka "File Under" date in UI) --->
			<cfset qOtherRecords = this.energyEntryDelegate.getPotentialDuplicateRecords(
				energy_account_uid=entryInfo.energy_account_uid, 
				report_date=dateAdd("m", -1, entryInfo.report_date), 
				not_energy_usage_uid=entryInfo.energy_usage_uid)>

			<!--- If there are NOT any for the prior month, then that may indicate a "missing" record --->
			<cfif qOtherRecords.recordCount eq 0>
			
				<!--- Find out if there are any records at all for this account (we won't want to give a "false alarm" about "missing record" if this is the first record) --->
				<cfset qOtherRecords = this.energyEntryDelegate.getPotentialDuplicateRecords(
					energy_account_uid=entryInfo.energy_account_uid)>

				<!--- If there is at least one record for this account --->									
				<cfif qOtherRecords.recordCount gt 0>
					<!--- So, we are not delivery-based, and there are other records, but not one for the prior month. Warn the user. --->
					<cfset result = {status=false, message="The month prior to this entry's File Under date appears to be missing. Are you sure you want to continue?", fatal=false}>
					<cfreturn result>
				</cfif>
			</cfif>	
		</cfif>
			
		<cfreturn result>
	</cffunction>


	<!--- Private: Merges the values from the response into the metaFields in the entryInfo --->
	<cffunction name="mergeEnergyUsageMetasFromRateModel" access="private" returntype="void">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo" required="true">
		<cfargument name="swallowRateModelErrors" type="boolean" required="true">

		<!--- Local variables --->
		<cfset var rateModelResponse = "">

		<cftry>
			<!--- Call the rate model --->
			<cfset rateModelResponse = this.rateModelBridge.executeRateModelForEnergyEntryInfo(arguments.entryInfo)>

			<!--- Merge the values from the response into the metaFields in the entryInfo --->
			<cfinvoke
				component="mce.e2.ratemodel.RateModelUtil"
				method="mergeEnergyUsageMetasFromRateModel"
				entryInfo="#arguments.entryInfo#"
				rateModelResponse="#rateModelResponse#">

			<cfcatch type="any">
				<cfif not swallowRateModelErrors>
					<cfrethrow>
				</cfif>
			</cfcatch>
		</cftry>

	</cffunction>


	<!--- Private: Looks at certain meta values, adds up the sums, and places into total_cost and recorded_value at EnergyInfo level --->
	<!--- The reliance on magic hard-coded lookup codes seems suspect to Nate. Seems that at least we could rely on the "is_cost_component" column which is at same level? --->
	<cffunction name="calculateTotalCostAndRecordedValue" returntype="void" access="private">
		<cfargument name="entryInfo" type="mce.e2.vo.EnergyEntryInfo">

		<cfset var meta = "">
		<cfset var i = 0>
		<cfset var recordedValue = 0>
		<cfset var totalCost = 0>

		<cfloop from="1" to="#ArrayLen(entryInfo.metaFields)#" index="i">
			<cfset meta = entryInfo.metaFields[i]>
			<cfif meta.type_lookup_code eq "recorded_value">
				<cfset recordedValue = meta.meta_value>
			</cfif>
			<cfif meta.type_lookup_code eq "total_cost">
				<cfset totalCost = meta.meta_value>
			</cfif>
		</cfloop>
		
		<cfset entryInfo.recorded_value = recordedValue>
		<cfset entryInfo.total_cost = totalCost>
	</cffunction>

	<cffunction name="getEnergyEntryTax"
				access="public" returntype="array"
				hint="This method is used to retrieve Sales Taxes.">
		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		
		<cfreturn this.energyEntryDelegate.getEnergyEntryTax(energy_account_uid, period_start, period_end)>
	</cffunction>
	
	<cffunction name="getEnergyEntryTripCode"
				access="public" returntype="numeric"
				hint="This method is used to retrieve Trip code for the property of this account.">
		<cfargument name="property_uid" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		
		<cfreturn this.energyEntryDelegate.getEnergyEntryTripCode(property_uid, period_start)>
	</cffunction>
	
</cfcomponent>