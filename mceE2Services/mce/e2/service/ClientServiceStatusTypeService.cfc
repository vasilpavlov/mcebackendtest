<cfcomponent displayname="Client Service Status Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of client service status type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setClientServiceStatusTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client service type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientServiceStatusTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to client service type information.">
		<cfset this.ClientServiceStatusTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyClientServiceStatusTypeVo" 
				access="public" returntype="mce.e2.vo.ClientServiceStatusType"
				hint="This method is used to retrieve / return an empty ClientServiceStatusTypeDelegate information value object.">
		<cfreturn this.ClientServiceStatusTypeDelegate.getEmptyClientServiceStatusTypeComponent()>
	</cffunction>

	<cffunction name="getAvailableClientServiceStatusTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service types supported by the application.">
		<cfreturn this.ClientServiceStatusTypeDelegate.getClientServiceStatusTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientServiceStatusTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service types.">
		<cfreturn this.ClientServiceStatusTypeDelegate.getClientServiceStatusTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientServiceStatusType" 
				access="public" returntype="mce.e2.vo.ClientServiceStatusType" 
				hint="This method is used to return a populated ClientServiceStatusType vo (value object)."> 
		<cfargument name="ClientServiceStatusType" type="mce.e2.vo.ClientServiceStatusType" required="true" hint="Describes the ClientServiceStatusTypeDelegate collection VO object containing the properties of the ClientServiceStatusTypeDelegate collection to be retrieved." /> 
		<cfreturn this.ClientServiceStatusTypeDelegate.getClientServiceStatusTypeAsComponent(service_status_code=arguments.ClientServiceStatusType.service_status_code)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewClientServiceStatusType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new ClientServiceStatusTypeDelegate record to the application database."> 
		<cfargument name="ClientServiceStatusType" type="mce.e2.vo.ClientServiceStatusType" required="true" hint="Describes the ClientServiceStatusTypeDelegate collection VO object containing the details of the ClientServiceStatusTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.ClientServiceStatusTypeDelegate = setAuditProperties(arguments.ClientServiceStatusTypeDelegate, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.ClientServiceStatusTypeDelegate = setPrimaryKey(local.ClientServiceStatusTypeDelegate)>
		
		<!--- Save the modified ClientServiceStatusTypeDelegate --->
		<cfset this.ClientServiceStatusTypeDelegate.saveNewClientServiceStatusType(ClientServiceStatusTypeDelegate=local.ClientServiceStatusTypeDelegate)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.ClientServiceStatusTypeDelegate.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewClientServiceStatusType" 
				access="public" returntype="mce.e2.vo.ClientServiceStatusType" 
				hint="This method is used to persist a new ClientServiceStatusTypeDelegate record to the application database."> 
		<cfargument name="ClientServiceStatusType" type="mce.e2.vo.ClientServiceStatusType" required="true" hint="Describes the ClientServiceStatusTypeDelegate collection VO object containing the details of the ClientServiceStatusTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewClientServiceStatusType(ClientServiceStatusTypeDelegate=arguments.ClientServiceStatusTypeDelegate)>

		<!--- Set the primary key --->
		<cfset arguments.ClientServiceStatusTypeDelegate.service_status_code = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.ClientServiceStatusTypeDelegate>

	</cffunction>

	<cffunction name="saveExistingClientServiceStatusType" 
				access="public" returntype="mce.e2.vo.ClientServiceStatusType" 
				hint="This method is used to save an existing ClientServiceStatusTypeDelegate information to the application database."> 
		<cfargument name="ClientServiceStatusTypeDelegate" type="mce.e2.vo.ClientServiceStatusType" required="true" hint="Describes the ClientServiceStatusTypeDelegate collection VO object containing the details of the ClientServiceStatusTypeDelegate collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.ClientServiceStatusTypeDelegate = setAuditProperties(arguments.ClientServiceStatusTypeDelegate, "modify")>
		
		<!--- Save the modified ClientServiceStatusTypeDelegate --->
		<cfset this.ClientServiceStatusTypeDelegate.saveExistingClientServiceStatusType(ClientServiceStatusTypeDelegate=local.ClientServiceStatusTypeDelegate)/>

		<!--- Return the modified ClientServiceStatusTypeDelegate --->
		<cfreturn local.ClientServiceStatusTypeDelegate>	

	</cffunction>
	
</cfcomponent>