<cfcomponent displayname="User Role Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of userRole role information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setUserRoleDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user role data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserRoleDelegate" hint="Describes the *.cfc used to manage database interactions related to user role information.">
		<cfset this.userRoleDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyUserRoleVo" 
				access="public" returntype="mce.e2.vo.UserRole"
				hint="This method is used to retrieve / return an empty user role information value object.">
		<cfreturn this.userRoleDelegate.getEmptyUserRoleComponent()>
	</cffunction>

	<cffunction name="getApplicationUserRoles" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user role value objects.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="false" hint="Describes the user group VO object containing the properties of the user group for which user roles will be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>
		
		<!--- Was the userGroup argument passed? --->
		<cfif structKeyExists(arguments, 'userGroup')>
		
			<!--- If so, then grab the primary key, and append it to the arguments structure --->
			<cfset local.args.user_group_uid = arguments.UserGroup.user_group_uid>
		
		</cfif>
		
		<!--- Return a collection of user roles based on the arguments provided --->
		<cfreturn this.userRoleDelegate.getUserRolesAsArrayOfComponents(argumentCollection=local.args)>
	
	</cffunction>

	<cffunction name="getUserRole" 
				access="public" returntype="mce.e2.vo.UserRole" 
				hint="This method is used to return a populated user role vo (value object)."> 
		<cfargument name="userRole" type="mce.e2.vo.UserRole" required="true" hint="Describes the user role VO object containing the properties of the user role to be retrieved." /> 
		<cfreturn this.userRoleDelegate.getUserRoleAsComponent(user_role_uid=arguments.userRole.user_role_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewUserRole" 
				access="public" returntype="string" 
				hint="This method is used to persist a new user role record to the application database.  The method returns the primary key of the user role that was crated."> 
		<cfargument name="userRole" type="mce.e2.vo.UserRole" required="true" hint="Describes the user role VO object containing the properties of the user role to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.userRole = setAuditProperties(arguments.userRole, "create")>

		<!--- Set the primary key for the user role if it's not already set --->
		<cfset local.userRole = setPrimaryKey(local.userRole)>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.userRole.user_role_uid>

		<!--- Create the userRole --->		
		<cfset this.userRoleDelegate.saveNewUserRole(userRole=local.userRole)>

		<!--- Return the new userRole primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewUserRole" 
				access="public" returntype="mce.e2.vo.UserRole" 
				hint="This method is used to persist a new user role record to the application database.  The method returns the primary key of the user role that was crated."> 
		<cfargument name="userRole" type="mce.e2.vo.UserRole" required="true" hint="Describes the user role VO object containing the properties of the user role to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Create the userRole --->		
		<cfset local.primaryKey = saveNewUserRole(userRole=arguments.userRole)>
	
		<!--- Set the primary key for the user role --->
		<cfset arguments.userRole.user_role_uid = local.primaryKey>

		<!--- Retrieve the new user role --->
		<cfset local.userRole = getUserRole(userRole=arguments.userRole)>

		<!--- Return the new userRole --->
		<cfreturn local.userRole>

	</cffunction>
	
	<cffunction name="saveExistingUserRole" 
				access="public" returntype="void" 
				hint="This method is used to save existing user role information to the application database."> 
		<cfargument name="userRole" type="mce.e2.vo.UserRole" required="true" hint="Describes the user role VO object containing the properties of the user role to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.userRole = setAuditProperties(arguments.userRole, "modify")>

		<!--- Modify the userRole --->		
		<cfset this.userRoleDelegate.saveExistingUserRole(userRole=local.userRole)/>
				
	</cffunction>

</cfcomponent>