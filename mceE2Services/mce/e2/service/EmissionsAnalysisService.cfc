<cfcomponent extends="BaseDataService" output="false">

	<cffunction name="getEmissionsAnalysis"
				access="remote"
				returntype="query"
				hint="Returns emissions analysis for a given collection of properties and energy accounts.">

		<!--- Define the arguments for this method --->
		<cfargument name="property_uid_list" type="string" required="true" hint="The property/properties that you want emissions analysis records for">
		<cfargument name="period_start" type="date" required="true" hint="The start date of the time period that you want emissions records for">
		<cfargument name="period_end" type="date" required="true" hint="The end date of the time period that you want emissions records for">
		<cfargument name="dateRollupMode" type="string" required="true" hint="At what level you want rows to be returned in terms of date. Currently implemented: 'month-actual', 'month-prorated', 'total-actual' and 'total-prorated'">
		<cfargument name="energy_account_uid_list" type="string" required="false" hint="Optional; the specific energy account(s) that you want emissions analysis records for. Omit or provide an empty string for all energy accounts associated with the given property/properties.">
		<cfargument name="profile_lookup_code" type="string" required="false" default="default" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="grouping" type="string" required="false" default="period" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="property_collection_uid_list" type="string" required="false">

		<cfif not IsDefined("arguments.property_collection_uid_list")>
			<cfset arguments.property_collection_uid_list = this.securityService.getSession().propertyCollectionsList>
		</cfif>

		<!--- Retrieve the emissions analysis --->
		<cfreturn this.emissionsAnalysisDelegate.getEmissionsAnalysis(
					argumentCollection=arguments)>

	</cffunction>

	<cffunction name="getEnergyAnalysis"
				access="remote"
				returntype="query"
				hint="Returns emissions analysis for a given collection of properties and energy accounts.">

		<!--- Define the arguments for this method --->
		<cfargument name="property_uid_list" type="string" required="true" hint="The property/properties that you want emissions analysis records for">
		<cfargument name="period_start" type="date" required="true" hint="The start date of the time period that you want emissions records for">
		<cfargument name="period_end" type="date" required="true" hint="The end date of the time period that you want emissions records for">
		<cfargument name="dateRollupMode" type="string" required="true" hint="At what level you want rows to be returned in terms of date. Currently implemented: 'month-actual', 'month-prorated', 'total-actual' and 'total-prorated'">
		<cfargument name="energy_account_uid_list" type="string" required="false" hint="Optional; the specific energy account(s) that you want emissions analysis records for. Omit or provide an empty string for all energy accounts associated with the given property/properties.">
		<cfargument name="profile_lookup_code" type="string" required="false" default="default" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="grouping" type="string" required="false" default="period" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="propertyMetaTypeCodeForMetrics" type="string" required="false" default="">
		<cfargument name="energyUsageMetaTypeCodes" type="string" required="false" default="">

		<!--- Retrieve the energy analysis --->
		<cfreturn this.emissionsAnalysisDelegate.getEnergyAnalysis(
					argumentCollection=arguments)>

	</cffunction>

	<cffunction name="getEmissionsSourcesHierarchy"
				access="remote"
				returntype="array"
				hint="Returns a query of emissions sources, with hierarchy columns.">

		<!--- Define the arguments for this method --->
		<cfargument name="profile_lookup_code" type="string" required="true" default="default" hint="Describes the emissions profile ">

		<!--- Retrieve the hierarchy elements, filtering on the profile lookup code --->
		<cfreturn this.emissionsAnalysisDelegate.getEmissionsSourcesHierarchyAsArrayOfComponents(
					argumentCollection=arguments)>

	</cffunction>

	<!--- Managed beans --->
	<cffunction name="setEmissionsAnalysisDelegate"
				access="public" returntype="void"
				hint="Called automatically via IOC / dependency injection (Coldspring).">
		<cfargument name="bean" type="mce.e2.db.EmissionsAnalysisDelegate">
		<cfset this.emissionsAnalysisDelegate = arguments.bean>
	</cffunction>

</cfcomponent>