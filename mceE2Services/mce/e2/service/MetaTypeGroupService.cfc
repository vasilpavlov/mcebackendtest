<cfcomponent displayname="Meta Type Group Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of meta type group information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setMetaTypeGroupDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage meta type group data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.MetaTypeGroupDelegate" hint="Describes the *.cfc used to manage database interactions related to meta type group information.">
		<cfset this.MetaTypeGroupDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyMetaTypeGroupVo" 
				access="public" returntype="mce.e2.vo.MetaTypeGroup"
				hint="This method is used to retrieve / return an empty metaTypeGroup information value object.">
		<cfreturn this.metaTypeGroupDelegate.getEmptyMetaTypeGroupComponent()>
	</cffunction>

	<cffunction name="getAvailableMetaTypeGroups" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of meta type groups filtered by a given energy provider rate class.">

		<!--- Define the arguments for this method --->
		<cfargument name="EnergyProviderRateClass" type="mce.e2.vo.EnergyProviderRateClass" required="true" hint="Describes the energy provider rate class used to retrieve / filter meta type groups.">

		<!--- Intialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Intialize the arguments structure --->
		<cfset local.args = structNew()>
		
		<!--- Initialize the arguments used to retrieve meta type group information --->
		<cfset local.args.rate_class_uid = arguments.EnergyProviderRateClass.rate_class_uid>
		
		<!--- Retrieve the collection of meta type group data --->
		<cfset local.result = this.metaTypeGroupDelegate.getMetaTypeGroupsAsArrayOfComponents(argumentCollection=local.args)>
		
		<!--- Return the meta type group records --->
		<cfreturn local.result>

	</cffunction>

	<cffunction name="getMetaTypeGroups" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of meta type groups.">
		<cfreturn this.metaTypeGroupDelegate.getMetaTypeGroupsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getMetaTypeGroup" 
				access="public" returntype="mce.e2.vo.MetaTypeGroup" 
				hint="This method is used to return a populated MetaTypeGroup vo (value object)."> 
		<cfargument name="MetaTypeGroup" type="mce.e2.vo.MetaTypeGroup" required="true" hint="Describes the metaTypeGroup collection VO object containing the properties of the metaTypeGroup collection to be retrieved." /> 
		<cfreturn this.metaTypeGroupDelegate.getMetaTypeGroupAsComponent(metaTypeGroup_uid=arguments.MetaTypeGroup.metaTypeGroup_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewMetaTypeGroup" 
				access="public" returntype="string" 
				hint="This method is used to persist a new metaTypeGroup record to the application database."> 
		<cfargument name="MetaTypeGroup" type="mce.e2.vo.MetaTypeGroup" required="true" hint="Describes the metaTypeGroup collection VO object containing the details of the metaTypeGroup collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.metaTypeGroup = setAuditProperties(arguments.metaTypeGroup, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.metaTypeGroup = setPrimaryKey(local.metaTypeGroup)>
		
		<!--- Save the modified metaTypeGroup --->
		<cfset this.MetaTypeGroupDelegate.saveNewMetaTypeGroup(metaTypeGroup=local.metaTypeGroup)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.metaTypeGroup.meta_group_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewMetaTypeGroup" 
				access="public" returntype="mce.e2.vo.MetaTypeGroup" 
				hint="This method is used to persist a new metaTypeGroup record to the application database."> 
		<cfargument name="MetaTypeGroup" type="mce.e2.vo.MetaTypeGroup" required="true" hint="Describes the metaTypeGroup collection VO object containing the details of the metaTypeGroup collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewMetaTypeGroup(metaTypeGroup=arguments.metaTypeGroup)>

		<!--- Set the primary key --->
		<cfset arguments.metaTypeGroup.meta_group_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.metaTypeGroup>

	</cffunction>

	<cffunction name="saveExistingMetaTypeGroup" 
				access="public" returntype="mce.e2.vo.MetaTypeGroup" 
				hint="This method is used to save an existing metaTypeGroup information to the application database."> 
		<cfargument name="metaTypeGroup" type="mce.e2.vo.MetaTypeGroup" required="true" hint="Describes the metaTypeGroup collection VO object containing the details of the metaTypeGroup collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.metaTypeGroup = setAuditProperties(arguments.metaTypeGroup, "modify")>
		
		<!--- Save the modified metaTypeGroup --->
		<cfset this.MetaTypeGroupDelegate.saveExistingMetaTypeGroup(metaTypeGroup=local.metaTypeGroup)/>

		<!--- Return the modified metaTypeGroup --->
		<cfreturn local.metaTypeGroup>	

	</cffunction>
	
</cfcomponent>