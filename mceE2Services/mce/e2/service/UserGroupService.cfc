<cfcomponent displayname="User Group Service"
			 extends="BaseDataService" output="true"
			 hint="The component is used to manage all business logic tied / associated with the management of userGroup group information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setUserGroupDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user group data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserGroupDelegate" hint="Describes the *.cfc used to manage database interactions related to user group information.">
		<cfset this.userGroupDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setUserDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserDelegate" hint="Describes the *.cfc used to manage database interactions related to user information.">
		<cfset this.userDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setUserRoleDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user role data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserRoleDelegate" hint="Describes the *.cfc used to manage database interactions related to user role information.">
		<cfset this.userRoleDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyCollectionDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage property collection data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyCollectionDelegate" hint="Describes the *.cfc used to manage database interactions related to property collection information.">
		<cfset this.propertyCollectionDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyUserGroupVo" 
				access="public" returntype="mce.e2.vo.UserGroup"
				hint="This method is used to retrieve / return an empty user group information value object.">
		<cfreturn this.userGroupDelegate.getEmptyUserGroupComponent()>
	</cffunction>

	<cffunction name="getUserGroups" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user group value objects.">

		<!--- Define the arguments for this method --->
		<cfargument name="clientCompany" type="mce.e2.vo.ClientCompany" required="false" hint="Describes the client company VO object containing the properties of the company for which user groups will be retrieved." /> 
		<cfargument name="user" type="mce.e2.vo.User" required="false" hint="Describes the user VO object containing the properties of the user for which user groups will be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the local arguments structure --->
		<cfset local.args = structNew()>
		
		<!--- Was a client company specified? --->
		<cfif structKeyExists(arguments, 'clientCompany')>
		
			<!--- If so, set the arguments based on the client company --->
			<cfset local.args.client_company_uid = arguments.clientCompany.client_company_uid>
			
		</cfif>
		
		<!--- Was a user specified? --->
		<cfif structKeyExists(arguments, 'user')>
		
			<!--- If so, set the arguments based on the client company --->
			<cfset local.args.user_uid = arguments.user.user_uid>
			<cfset local.args.client_company_uid = arguments.user.client_company_uid>
		
		</cfif>

		<!--- Retrieve the user groups --->
		<cfreturn this.userGroupDelegate.getUserGroupsAsArrayOfComponents(
					argumentCollection=local.args)>

	</cffunction>

	<cffunction name="getUserGroup" 
				access="public" returntype="mce.e2.vo.UserGroup" 
				hint="This method is used to return a populated user group vo (value object)."> 
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group VO object containing the properties of the user group to be retrieved." /> 
		<cfreturn this.userGroupDelegate.getUserGroupAsComponent(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid)>
	</cffunction>

	<!--- Create methods to retrieve userGroup-related collections --->	
	<cffunction name="getAssociatedUsers" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user value objects associated / not associated for a given user group.  Associated users will be flagged with an isAssociated value of 1; unassociated users will be flagged with an isAssociated value of 0.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which users are associated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of users --->
		<cfset local.result = this.userDelegate.getUsersAsArrayOfComponents(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid,
					selectMethod="inclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>

	<cffunction name="getUnAssociatedUsers" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user value objects not associated to a given user group.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which users are not associated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of users --->
		<cfset local.result = this.userDelegate.getUsersAsArrayOfComponents(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid,
					selectMethod="exclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>
	
	<cffunction name="getUserAssociations" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user value objects that are either associated / not associated to a specified group.  The objects in the array collection will return an isAssociated value of true if an association between the user ans user group exists.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group for which all user associations are described.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of users --->
		<cfset local.result = this.userDelegate.getUsersAsArrayOfComponents(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid,
					selectMethod="userGroupAssociations")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>	

	<cffunction name="getAssociatedUserRoles" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user role value objects for a given user group.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which user roles are associated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of user roles --->
		<cfset local.result = this.userRoleDelegate.getUserRolesAsArrayOfComponents(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid,
					selectMethod="inclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>

	<cffunction name="getUnAssociatedUserRoles" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user role value objects not associated to a given user group.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which user roles are not associated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of user roles --->
		<cfset local.result = this.userRoleDelegate.getUserRolesAsArrayOfComponents(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid,
					selectMethod="exclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>
	
	<cffunction name="getUserRoleAssociations" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user role value objects that are either associated / not associated to a specified group.  The objects in the array collection will return an isAssociated value of true if an association between the user role and user group exists.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group for which all user role associations are described.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Is the group being passed in a new or existing group? --->
		<cfif isUniqueIdentifier(arguments.userGroup.user_group_uid)>

			<!--- Retrieve the collection of user roles --->
			<cfset local.result = this.userRoleDelegate.getUserRolesAsArrayOfComponents(
						client_company_uid=arguments.userGroup.client_company_uid,
						user_group_uid=arguments.userGroup.user_group_uid,
						selectMethod="userGroupAssociations")>
		
		<cfelse>
	
			<!--- Retrieve the collection of user roles --->
			<cfset local.result = this.userRoleDelegate.getUserRolesAsArrayOfComponents(
						client_company_uid=arguments.userGroup.client_company_uid,
						selectMethod="singular")>
	
		</cfif>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>	

	<cffunction name="getAssociatedPropertyCollections" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property collection value objects for a given user group.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which property collections are associated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of property collections --->
		<cfset local.result = this.propertyCollectionDelegate.getPropertyCollectionsAsArrayOfComponents(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid,
					selectMethod="inclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>

	<cffunction name="getUnAssociatedPropertyCollections" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property collection value objects not associated to a given user group.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which property collections are not associated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the collection of property collections --->
		<cfset local.result = this.propertyCollectionDelegate.getPropertyCollectionsAsArrayOfComponents(
					client_company_uid=arguments.userGroup.client_company_uid,
					user_group_uid=arguments.userGroup.user_group_uid,
					selectMethod="exclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>
	
	<cffunction name="getPropertyCollectionAssociations" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property collection value objects that are either associated / not associated to a specified group.  The objects in the array collection will return an isAssociated value of true if an association between the property collection and user group exists.">
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group for which all property collection associations are described.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Is the group being passed in a new or existing group? --->
		<cfif isUniqueIdentifier(arguments.userGroup.user_group_uid)>

			<!--- Retrieve the collection of property collections --->
			<cfset local.result = this.propertyCollectionDelegate.getPropertyCollectionsAsArrayOfComponents(
						client_company_uid=arguments.userGroup.client_company_uid,
						user_group_uid=arguments.userGroup.user_group_uid,
						selectMethod="userGroupAssociations")>

		<cfelse>
	
			<!--- Retrieve the collection of property collections --->
			<cfset local.result = this.propertyCollectionDelegate.getPropertyCollectionsAsArrayOfComponents(
						client_company_uid=arguments.userGroup.client_company_uid,
						selectMethod="singular-inclusive")>
	
		</cfif>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>	

	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewUserGroup" 
				access="public" returntype="string" 
				hint="This method is used to persist a new user group record to the application database.  The method returns the primary key of the user group that was crated."> 
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group VO object containing the properties of the user group to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.userGroup = setAuditProperties(arguments.userGroup, "create")>

		<!--- Set the primary key for the user group if it's not already set --->
		<cfset local.userGroup = setPrimaryKey(local.userGroup)>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.userGroup.user_group_uid>

		<!--- Create the userGroup --->		
		<cfset this.userGroupDelegate.saveNewUserGroup(userGroup=local.userGroup)>

		<!--- Return the new userGroup primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewUserGroup" 
				access="public" returntype="mce.e2.vo.UserGroup" 
				hint="This method is used to persist a new user group record to the application database.  The method returns the primary key of the user group that was crated."> 
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group VO object containing the properties of the user group to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Create the userGroup --->		
		<cfset local.primaryKey = saveNewUserGroup(userGroup=arguments.userGroup)>
	
		<!--- Set the primary key for the user group --->
		<cfset arguments.userGroup.user_group_uid = local.primaryKey>

		<!--- Retrieve the new user group --->
		<cfset local.userGroup = getUserGroup(userGroup=arguments.userGroup)>

		<!--- Return the new userGroup --->
		<cfreturn local.userGroup>

	</cffunction>
	
	<cffunction name="saveExistingUserGroup" 
				access="public" returntype="void" 
				hint="This method is used to save existing user group information to the application database."> 
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group VO object containing the properties of the user group to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.userGroup = setAuditProperties(arguments.userGroup, "modify")>

		<!--- Modify the userGroup --->		
		<cfset this.userGroupDelegate.saveExistingUserGroup(userGroup=local.userGroup)/>
				
	</cffunction>

	<!--- Author all the methods that will manage associations between user groups and other objects --->	
	<cffunction name="associateUserToUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to add / associate a given user to a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which users are being associated.">
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user being associated to a user group." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>
		
		<!--- Add the user / user group association --->
		<cfset local.assocObject.user_uid = arguments.user.user_uid>
		<cfset local.assocObject.user_group_uid = arguments.userGroup.user_group_uid>

		<!--- Set the modified by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(local.assocObject, "create")>

		<!--- Create the user / user group association --->		
		<cfset this.userGroupDelegate.saveUserToUserGroup(assocObject=local.assocObject)>

	</cffunction>

	<cffunction name="disAssociateUserFromUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to remove / disassociate a given user from a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which users are being associated.">
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user being removed from a user group." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>
		
		<!--- Add the user / user group association --->
		<cfset local.assocObject.user_uid = arguments.user.user_uid>
		<cfset local.assocObject.user_group_uid = arguments.userGroup.user_group_uid>

		<!--- Since this will accept relationship data from users / usergroups, we need to interrogate both objects for the relationship_id --->
		<cfif structKeyExists(arguments.user, 'relationship_id')>
			
			<!--- Grab the relationship id from the user object --->
			<cfset local.assocObject.relationship_id = arguments.user.relationship_id>

		<!--- Otherwise, check the userGroup object for the relationship id --->
		<cfelseif structKeyExists(arguments.userGroup, 'relationship_id')>

			<!--- Grab the relationship id from the user group object --->
			<cfset local.assocObject.relationship_id = arguments.userGroup.relationship_id>		
		</cfif>

		<!--- Deactivate the association --->
		<cfset local.assocObject.is_active = 0>

		<!--- Set the modified by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(local.assocObject, "modify")>

		<!--- Create the user / user group association --->		
		<cfset this.userGroupDelegate.removeUserFromUserGroup(assocObject=local.assocObject)>

	</cffunction>

	<cffunction name="processUserAssociations"
				access="public"
				returntype="void"
				hint="This method is used to process changes to a collection of user associations to a specified user group.">
				
		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which users are being associated.">
		<cfargument name="userArray" type="array" required="true" hint="Describes the array collection of user VO objects containing the users whose association to the specified user group is changing." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the associated users for this user group --->
		<cfset local.associatedUsers = getAssociatedUsers(arguments.userGroup)>				
		
		<!--- Determine the associations to process --->		
		<cfset local.associationsToProcess = reviewAssociations(
				associatedObjects=local.associatedUsers,
				associationsToProcess=arguments.userArray,
				primaryKey="user_uid")>		
				
		<!--- Loop over the save array, and process the user / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.addArray)#" index="local.arrayIndex">
		
			<!--- Process each new user / group association --->
			<cfset associateUserToUserGroup(
					userGroup=arguments.userGroup,
					user=local.associationsToProcess.addArray[local.arrayIndex])>
		
		</cfloop>		
				
		<!--- Loop over the save array, and process the user / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.removeArray)#" index="local.arrayIndex">
		
			<!--- Process each existing user / group association --->
			<cfset disAssociateUserFromUserGroup(
					userGroup=arguments.userGroup,
					user=local.associationsToProcess.removeArray[local.arrayIndex])>
		
		</cfloop>		

	</cffunction>			

	<cffunction name="associateUserRoleToUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to add / associate a given user role to a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which user roles are being associated.">
		<cfargument name="userRole" type="mce.e2.vo.UserRole" required="true" hint="Describes the user VO object containing the properties of the user role being added to a given user group." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>
		
		<!--- Add the user role / user group association --->
		<cfset local.assocObject.user_group_uid = arguments.userGroup.user_group_uid>
		<cfset local.assocObject.user_role_uid = arguments.userRole.user_role_uid>
		<cfset local.assocObject.is_role_granted = arguments.userRole.isRoleGranted>

		<!--- Set the created by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(local.assocObject, "create")>

		<!--- Create the user role / user group association --->		
		<cfset this.userGroupDelegate.saveUserRoleToUserGroup(assocObject=local.assocObject)>

	</cffunction>

	<cffunction name="modifyUserRoleToUserGroupAssociation"
				access="public"
				returnType="void"
				hint="This method is used to modify a given user role to a user group association.">

		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which user roles are being associated.">
		<cfargument name="userRole" type="mce.e2.vo.UserRole" required="true" hint="Describes the user VO object containing the properties of the user role being added to a given user group." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>
		
		<!--- Define the properties of the user role / user group association --->
		<cfset local.assocObject.user_group_uid = arguments.userGroup.user_group_uid>
		<cfset local.assocObject.user_role_uid = arguments.userRole.user_role_uid>
		<cfset local.assocObject.is_role_granted = arguments.userRole.isRoleGranted>
		<cfset local.assocObject.relationship_id = arguments.userRole.relationship_id>
		<cfset local.assocObject.is_active = arguments.userRole.isAssociated><!-- JF Addition not sure this is right 3/15/2010-->

		<!--- Set the created by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(local.assocObject, "modify")>

		<!--- Modify the user role / user group association --->		
		<cfset this.userGroupDelegate.modifyUserRoleToUserGroupAssociation(assocObject=local.assocObject)>

	</cffunction>

	<cffunction name="disAssociateUserRoleFromUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to remove a given user role from a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which user roles are being associated.">
		<cfargument name="userRole" type="mce.e2.vo.UserRole" required="true" hint="Describes the user VO object containing the properties of the user role being added to a given user group." /> 
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>	
	
		<!--- Add the user / user group association --->
		<cfset local.assocObject.user_role_uid = arguments.userRole.user_role_uid>
		<cfset local.assocObject.user_group_uid = arguments.userGroup.user_group_uid>
		<cfset local.assocObject.relationship_id = arguments.userRole.relationship_id>
		
		<!--- Deactivate the association --->
		<cfset local.assocObject.is_active = 0>

		<!--- Set the modified by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(local.assocObject, "modify")>

		<!--- Remove the user / user group association --->		
		<cfset this.userGroupDelegate.removeUserRoleFromUserGroup(assocObject=local.assocObject)>
			
	</cffunction>

	<cffunction name="processUserRoleAssociations"
				access="public"
				returntype="void"
				hint="This method is used to process changes to a collection of user role associations to a specified user group.">
				
		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which user roles are being associated.">
		<cfargument name="userRoleArray" type="array" required="true" hint="Describes the array collection of user role VO objects containing the user roles whose association to the specified user group is changing." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the associated user roles for this user group --->
		<cfset local.associatedUserRoles = getAssociatedUserRoles(arguments.userGroup)>				
		
		<!--- Determine the associations to process --->		
		<cfset local.associationsToProcess = reviewAssociations(
				associatedObjects=local.associatedUserRoles,
				associationsToProcess=arguments.userRoleArray,
				primaryKey="user_role_uid",
				compareProperty="isRoleGranted")>		
				
		<!--- Loop over the save array, and process the user role / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.addArray)#" index="local.arrayIndex">
		
			<!--- Process each new role / group association --->
			<cfset associateUserRoleToUserGroup(
					userGroup=arguments.userGroup,
					userRole=local.associationsToProcess.addArray[local.arrayIndex])>
		
		</cfloop>		

		<!--- Loop over the modify array, and process the user role / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.modifyArray)#" index="local.arrayIndex">
		
			<!--- Process each existing role / group association --->
			<cfset modifyUserRoleToUserGroupAssociation(
					userGroup=arguments.userGroup,
					userRole=local.associationsToProcess.modifyArray[local.arrayIndex])>
		
		</cfloop>
				
		<!--- Loop over the save array, and process the user role / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.removeArray)#" index="local.arrayIndex">
		
			<!--- Process each existing user / group association --->
			<cfset disAssociateUserRoleFromUserGroup(
					userGroup=arguments.userGroup,
					userRole=local.associationsToProcess.removeArray[local.arrayIndex])>
		
		</cfloop>		

	</cffunction>			

	<cffunction name="associatePropertyCollectionToUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to add a given property collection to a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which property collections are being associated.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the propertycollection VO object containing the properties of the property collection being added to a given user group." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>
		
		<!--- Add the user group / property collection association --->
		<cfset local.assocObject.user_group_uid = arguments.userGroup.user_group_uid>
		<cfset local.assocObject.relationship_start = now()>
		<cfset local.assocObject.property_collection_uid = arguments.propertyCollection.property_collection_uid>

		<!--- Set the created by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(local.assocObject, "create")>

		<!--- Create the user group / property collection association --->		
		<cfset this.userGroupDelegate.savePropertyCollectionToUserGroup(assocObject=local.assocObject)>

	</cffunction>

	<cffunction name="disAssociatePropertyCollectionFromUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to remove a given property collection from a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which property collections are being associated.">
		<cfargument name="propertyCollection" type="mce.e2.vo.PropertyCollection" required="true" hint="Describes the propertyCollection VO object containing the properties of the property collection being added to a given user group." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the association object --->
		<cfset local.assocObject = structNew()>
		
		<!--- Add the user group / property collection association --->
		<cfset local.assocObject.user_group_uid = arguments.userGroup.user_group_uid>
		<cfset local.assocObject.property_collection_uid = arguments.propertyCollection.property_collection_uid>
		<cfset local.assocObject.relationship_id = arguments.propertyCollection.relationship_id>

		<!--- Deactivate the association --->
		<cfset local.assocObject.is_active = 0>

		<!--- Set the created by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(local.assocObject, "modify")>

		<!--- Create the user group / property collection association --->		
		<cfset this.userGroupDelegate.removePropertyCollectionFromUserGroup(assocObject=local.assocObject)>

	</cffunction>
	
	<cffunction name="processPropertyCollectionAssociations"
				access="public"
				returntype="void"
				hint="This method is used to process changes to a collection of property collection associations to a specified user group.">
				
		<!--- Define the arguments for this method --->
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which user roles are being associated.">
		<cfargument name="propertyCollectionArray" type="array" required="true" hint="Describes the array collection of property collection VO objects containing the property collections whose association to the specified user group is changing." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the associated property collections for this user group --->
		<cfset local.associatedPropertyCollections = getAssociatedPropertyCollections(arguments.userGroup)>				

		<!--- Determine the associations to process --->		
		<cfset local.associationsToProcess = reviewAssociations(
				associatedObjects=local.associatedPropertyCollections,
				associationsToProcess=arguments.propertyCollectionArray,
				primaryKey="property_collection_uid")>		
		
		<!--- Loop over the save array, and process the property collection / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.addArray)#" index="local.arrayIndex">
		
			<!--- Process each new role / group association --->
			<cfset associatePropertyCollectionToUserGroup(
					userGroup=arguments.userGroup,
					propertyCollection=local.associationsToProcess.addArray[local.arrayIndex])>
		
		</cfloop>		
				
		<!--- Loop over the save array, and process the property collection / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.removeArray)#" index="local.arrayIndex">
		
			<!--- Process each existing user / group association --->
			<cfset disAssociatePropertyCollectionFromUserGroup(
					userGroup=arguments.userGroup,
					propertyCollection=local.associationsToProcess.removeArray[local.arrayIndex])>
		
		</cfloop>		

	</cffunction>	

</cfcomponent>