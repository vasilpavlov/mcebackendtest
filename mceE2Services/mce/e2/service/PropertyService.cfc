<cfcomponent displayname="Property Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of property information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setPropertyDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyDelegate" hint="Describes the *.cfc used to manage database interactions related to property information.">
		<cfset this.propertyDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setEnergyAccountDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage energy account data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyAccountDelegate" hint="Describes the *.cfc used to manage database interactions related to energy account information.">
		<cfset this.energyAccountDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyAddressDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property address data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyAddressDelegate" hint="Describes the *.cfc used to manage database interactions related to property address information.">
		<cfset this.propertyAddressDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyMetaDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property meta data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyMetaDelegate" hint="Describes the *.cfc used to manage database interactions related to property meta information.">
		<cfset this.propertyMetaDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setClientCompanyDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage client company data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientCompanyDelegate" hint="Describes the *.cfc used to manage database interactions related to property information.">
		<cfset this.clientCompanyDelegate = arguments.bean>
	</cffunction>
	<cffunction name="setClientContactDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage client contact data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientContactDelegate" hint="Describes the *.cfc used to manage database interactions related to client contact information.">
		<cfset this.clientContactDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setAlertDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage alert data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.AlertDelegate" hint="Describes the *.cfc used to manage database interactions related to alert information.">
		<cfset this.alertDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyClientCompanyAssociationDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property / client company associations (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyClientCompanyAssociationDelegate" hint="Describes the *.cfc used to manage database interactions related to property / client company association information.">
		<cfset this.propertyClientCompanyAssociationDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyClientContactAssociationDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property / client contact associations (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyClientContactAssociationDelegate" hint="Describes the *.cfc used to manage database interactions related to property / client contact association information.">
		<cfset this.propertyClientContactAssociationDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyPropertyVo"
				access="public" returntype="mce.e2.vo.Property"
				hint="This method is used to retrieve / return an empty property information value object.">
		<cfreturn this.propertyDelegate.getEmptyPropertyComponent()>
	</cffunction>

	<cffunction name="getEmptyPropertyAddressVo"
				access="public" returntype="mce.e2.vo.PropertyAddress"
				hint="This method is used to retrieve / return an empty property address information value object.">
		<cfreturn this.propertyAddressDelegate.getEmptyPropertyAddressComponent()>
	</cffunction>

	<cffunction name="getEmptyPropertyMetaVo"
				access="public" returntype="mce.e2.vo.PropertyMeta"
				hint="This method is used to retrieve / return an empty property meta information value object.">
		<cfreturn this.propertyMetaDelegate.getEmptyPropertyMetaComponent()>
	</cffunction>

	<cffunction name="getEmptyPropertyClientCompanyAssociationVo"
				access="public" returntype="mce.e2.vo.PropertyClientCompanyAssociation"
				hint="This method is used to retrieve / return an empty property / client company association information value object.">
		<cfreturn this.propertyClientCompanyAssociationDelegate.getEmptyPropertyClientCompanyAssociationComponent()>
	</cffunction>

	<cffunction name="getEmptyPropertyClientContactAssociationVo"
				access="public" returntype="mce.e2.vo.PropertyClientContactAssociation"
				hint="This method is used to retrieve / return an empty property / client contact association information value object.">
		<cfreturn this.propertyClientContactAssociationDelegate.getEmptyPropertyClientContactAssociationComponent()>
	</cffunction>

	<!--- Author the select methods to retrieve property and related data --->
	<cffunction name="getProperties"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of properties.">

		<cfargument name="includeActivityStatus" type="boolean" required="false" default="true">

		<cfset var local = structNew()>
		<cfif includeActivityStatus>
			<cfset local.args.metaValuesFor = "intClientTypeID">
		</cfif>
		
		<cfreturn this.propertyDelegate.getPropertiesAsQuery(argumentCollection=local.args)>
	</cffunction>

	<cffunction name="getAssociatedProperties"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of associated properties.">
		<cfreturn this.propertyDelegate.getAssociatedPropertiesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getPropertiesByEnergyAccount"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of properties.">
		<cfargument name="energy_account_uid" type="string" required="true" hint="Provides a way to get the property for an energy account">
		<cfreturn this.propertyDelegate.getPropertiesAsArrayOfComponents(energy_account_uid=arguments.energy_account_uid)>
	</cffunction>

	<cffunction name="getProperty"
				access="public" returntype="mce.e2.vo.Property"
				hint="This method is used to return a populated property vo (value object).">
		<cfargument name="Property" type="any" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />
		<cfreturn this.propertyDelegate.getPropertyAsComponent(property_uid=arguments.Property.property_uid)>
	</cffunction>

	<cffunction name="getPropertyMeters"
				access="public" returntype="query"
				hint="This method is used to return a query of meters.">
		<cfargument name="property_uid" type="string" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />
		<cfreturn this.propertyDelegate.getPropertyMeters(property_uid=arguments.property_uid)>
	</cffunction>

	<cffunction name="getPropertyDetails"
				access="public" returntype="mce.e2.vo.Property"
				hint="This method is used to return a populated property vo (value object) with associated energy accounts and meta data.">

		<!--- Define the arguments for this method --->
		<cfargument name="Property" type="any" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the details for the specified property --->
		<cfset local.returnResult = getProperty(arguments.property)>

		<!--- Get the meta data associated to the current property --->
		<cfset local.returnResult.metaDataArray = getPropertyMetaData(arguments.property)>

		<!--- Get the energy accounts associated to the current property --->
		<cfset local.returnResult.energyAccountArray = getPropertyEnergyAccounts(arguments.property)>

		<!--- Return the property object with the associated details --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getPropertyAddresses"
				access="public" returntype="array"
				hint="This method is used to return a collection of associated property addresses.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve associated addresses." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.propertyAddressDelegate.getPropertyAddressesAsArrayOfComponents(
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>

	<cffunction name="getPropertyMetaData"
				access="public" returntype="array"
				hint="This method is used to return a collection of associated property meta data.">
		<cfargument name="Property" type="any" required="true" hint="Describes the property VO object used to retrieve associated meta data." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		
		<cfset aPropertyMetaData = this.propertyMetaDelegate.getPropertyMetaDataAsArrayOfComponents(
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date,
					userPermissions = getPermissions())>

		<cfset this.propertyMetaDelegate.applyChoicesToPropertyMeta(aPropertyMetaData)>
		<cfreturn aPropertyMetaData>
	</cffunction>

	<cffunction name="getPropertyEnergyAccounts"
				access="public" returntype="array"
				hint="This method is used to return a collection of associated energy accounts.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve associated meta data." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.energyAccountDelegate.getEnergyAccountsAndEnergyTypesAsArrayOfComponents(
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>

	<cffunction name="getAccountsForAllCompaniesForProperty"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of accounts for all client_companies associated with a given property.">
	<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />
	<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
	<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset local.args = structNew()>

		<!--- Get the client companies by property --->
		<cfset local.clientCompanies = this.clientCompanyDelegate.getClientCompaniesAsArrayOfComponents(property_uid=arguments.Property.property_uid,
			relationship_filter_date=arguments.relationship_filter_date,
			selectMethod=arguments.selectMethod)>

		<!--- Create a list of the client company primary keys --->
		<cfset local.args.client_company_uid = buildVoPropertyListFromArray(local.clientCompanies, 'client_company_uid')>

		<cfset local.properties = this.propertyDelegate.getPropertiesAsArrayOfComponents(
				argumentCollection=local.args)>


		<!-- now need to call getEnergyAccountSummaryForDataAnalysis -->
		<!--- Build out the property_uid list --->
		<cfset local.args.property_uid = buildVoPropertyListFromArray(voArray=local.properties, propertyName='property_uid')>

		<cfset local.energyAccounts = this.energyAccountDelegate.getEnergyAccountSummaryForDataAnalysis(property_uid = local.args.property_uid,
			selectMethod=arguments.selectMethod, relationship_filter_date = arguments.relationship_filter_date)>
		<!--- Return the result --->
		<cfreturn local.energyAccounts>

	</cffunction>

	<cffunction name="getAccountsForAllPropertiesForClientCompany"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of accounts for all properties associated with a given company.">
	<cfargument name="clientCompanyVo" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object containing the property definition to be retrieved." />
	<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
	<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset local.args = structNew()>


		<!--- Create a list of the client company primary keys --->
		<cfset local.args.client_company_uid = arguments.clientCompanyVo.client_company_uid>

		<cfset local.properties = this.propertyDelegate.getPropertiesAsArrayOfComponents(
				argumentCollection=local.args)>


		<!-- now need to call getEnergyAccountSummaryForDataAnalysis -->
		<!--- Build out the property_uid list --->
		<cfset local.args.property_uid = buildVoPropertyListFromArray(voArray=local.properties, propertyName='property_uid')>

		<cfset local.energyAccounts = this.energyAccountDelegate.getEnergyAccountSummaryForDataAnalysis(property_uid = local.args.property_uid,
			selectMethod=arguments.selectMethod, relationship_filter_date = arguments.relationship_filter_date)>
		<!--- Return the result --->
		<cfreturn local.energyAccounts>

	</cffunction>


	<cffunction name="getPropertiesForClientCompany"
				access="public" returntype="array"
				hint="This method is used to return a collection of associated properties.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company  VO object used to retrieve associated meta data." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.propertyDelegate.getPropertiesAsArrayOfComponents(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>
	<cffunction name="getPropertyClientContactAssociations"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property / client contact association value objects.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company to which properties and client contacts are associated.">
		<cfargument name="selectMethod" type="string" required="false" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="false" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Default the selectMethod argument --->
		<cfif len(arguments.selectMethod) eq 0 >
			<cfset arguments.selectMethod = "current">
		</cfif>

		<!--- Default the relationship_filter_date argument --->
		<cfif len(arguments.relationship_filter_date) eq 0 >
			<cfset arguments.relationship_filter_date = now()>
		</cfif>

		<!--- Retrieve the property / client contact associations --->
		<cfreturn this.propertyClientContactAssociationDelegate.getPropertyClientContactAssociationsAsArrayOfComponents(
				property_uid=arguments.Property.property_uid,
				client_company_uid=arguments.ClientCompany.client_company_uid,
				relationship_filter_date=arguments.relationship_filter_date,
				selectMethod=arguments.selectMethod)>

	</cffunction>

	<cffunction name="getPropertyClientCompanyAssociations"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of property / client company association value objects.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the property definition to be retrieved." />
		<cfargument name="selectMethod" type="string" required="false" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="false" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Default the selectMethod argument --->
		<cfif len(arguments.selectMethod) eq 0 >
			<cfset arguments.selectMethod = "current">
		</cfif>

		<!--- Default the relationship_filter_date argument --->
		<cfif len(arguments.relationship_filter_date) eq 0 >
			<cfset arguments.relationship_filter_date = now()>
		</cfif>

		<!--- Retrieve the property / client company associations --->
		<cfreturn this.propertyClientCompanyAssociationDelegate.getPropertyClientCompanyAssociationsAsArrayOfComponents(
				property_uid=arguments.Property.property_uid,
				relationship_filter_date=arguments.relationship_filter_date,
				selectMethod=arguments.selectMethod)>

	</cffunction>

	<cffunction name="getPropertyAlerts"
				access="public" returnType="array"
				hint="This method is used to retrieve property alerts based grouped by type group code and property.">

		<!--- Define the arguments associated to this method --->
		<cfargument name="propertyArray" type="array" required="true" hint="Describes the array of properties that will be used to retrieve related alerts." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- build the property_uid list used to retrieve properties --->
		<cfset local.propertyList = buildVoPropertyListFromArray(
				voArray=arguments.propertyArray,
				propertyName="property_uid")>

		<!--- TODO: Re-implement as an alert summary VO --->
		<!--- Retrieve the alerts associated to a given collection of properties--->
		<!---<cfreturn this.alertDelegate.getAlertsAsArrayOfComponents(
					property_uid=local.propertyList)>--->
		<cfreturn arrayNew(1)>

	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewProperty"
				access="public" returntype="string"
				hint="This method is used to persist a new property record to the application database.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the details of the property to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.property = setAuditProperties(arguments.property, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.property = setPrimaryKey(local.property)>

		<!--- Save the modified property --->
		<cfset this.PropertyDelegate.saveNewProperty(property=local.property)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.property.property_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewProperty"
				access="public" returntype="mce.e2.vo.Property"
				hint="This method is used to persist a new property record to the application database.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the details of the property to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewProperty(property=arguments.property)>

		<!--- Set the primary key --->
		<cfset arguments.property.property_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.property>

	</cffunction>

	<cffunction name="saveExistingProperty"
				access="public" returntype="mce.e2.vo.Property"
				hint="This method is used to save an existing property information to the application database.">
		<cfargument name="property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the property definition to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.property = setAuditProperties(arguments.property, "modify")>

		<!--- Save the modified property --->
		<cfset this.PropertyDelegate.saveExistingProperty(property=local.property)/>

		<!--- Return the modified property --->
		<cfreturn local.property>

	</cffunction>

	<cffunction name="saveNewPropertyAddress"
				access="public" returntype="string"
				hint="This method is used to persist a new property address record to the application database.">
		<cfargument name="PropertyAddress" type="mce.e2.vo.PropertyAddress" required="true" hint="Describes the property address VO object containing the details of the property address to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.propertyAddress = setAuditProperties(arguments.propertyAddress, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.propertyAddress = setPrimaryKey(local.propertyAddress)>

		<!--- Save the modified property address --->
		<cfset this.PropertyAddressDelegate.saveNewPropertyAddress(propertyAddress=local.propertyAddress)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.propertyAddress.property_address_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewPropertyAddress"
				access="public" returntype="mce.e2.vo.PropertyAddress"
				hint="This method is used to persist a new property address record to the application database.">
		<cfargument name="PropertyAddress" type="mce.e2.vo.PropertyAddress" required="true" hint="Describes the property address VO object containing the details of the property address to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewPropertyAddress(propertyAddress=arguments.propertyAddress)>

		<!--- Set the primary key --->
		<cfset arguments.propertyAddress.property_address_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.propertyAddress>

	</cffunction>

	<cffunction name="saveExistingPropertyAddress"
				access="public" returntype="mce.e2.vo.PropertyAddress"
				hint="This method is used to save an existing property address information to the application database.">
		<cfargument name="PropertyAddress" type="mce.e2.vo.PropertyAddress" required="true" hint="Describes the property address VO object containing the property address definition to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.propertyAddress = setAuditProperties(arguments.PropertyAddress, "modify")>

		<cfif not structKeyExists(local.propertyAddress, 'relationship_end')>
			<cfset local.propertyAddress.relationship_end = ''>
		</cfif>
		<!--- Save the modified property --->
		<cfset this.PropertyAddressDelegate.saveExistingPropertyAddress(propertyAddress=arguments.propertyAddress)/>

		<!--- Return the modified property address --->
		<cfreturn local.propertyAddress>

	</cffunction>

	<cffunction name="saveNewPropertyClientCompanyAssociation"
				access="public" returntype="numeric"
				hint="This method is used to persist a new property / client company association record to the application database.">
		<cfargument name="PropertyClientCompanyAssociation" type="mce.e2.vo.PropertyClientCompanyAssociation" required="true" hint="Describes the property / client company VO object containing the details of the association to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(arguments.PropertyClientCompanyAssociation, "create")>

		<!--- Save the modified property address --->
		<cfset local.primaryKey = this.propertyClientCompanyAssociationDelegate.saveNewPropertyClientCompanyAssociation(
				PropertyClientCompanyAssociation=local.assocObject)/>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewPropertyClientCompanyAssociation"
				access="public" returntype="mce.e2.vo.PropertyClientCompanyAssociation"
				hint="This method is used to persist a new property / client company association record to the application database.">
		<cfargument name="PropertyClientCompanyAssociation" type="mce.e2.vo.PropertyClientCompanyAssociation" required="true" hint="Describes the property / client company VO object containing the details of the association to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewPropertyClientCompanyAssociation(
				PropertyClientCompanyAssociation=arguments.PropertyClientCompanyAssociation)/>

		<!--- Set the primary key --->
		<cfset arguments.PropertyClientCompanyAssociation.relationship_id = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.PropertyClientCompanyAssociation>

	</cffunction>

	<cffunction name="saveExistingPropertyClientCompanyAssociation"
				access="public" returntype="mce.e2.vo.PropertyClientCompanyAssociation"
				hint="This method is used to save existing property / client company association information to the application database.">
		<cfargument name="PropertyClientCompanyAssociation" type="mce.e2.vo.PropertyClientCompanyAssociation" required="true" hint="Describes the existing property / client company VO object containing the details of the association to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>


		<!--- Set the modified by / date audit properties --->
		<cfset local.PropertyClientCompanyAssociation = setAuditProperties(arguments.PropertyClientCompanyAssociation, "modify")>

		<!-- Deal with dropped date field if it's null'--->
		<cfif (not structKeyExists(local.PropertyClientCompanyAssociation, 'relationship_end'))>
			<cfset local.PropertyClientCompanyAssociation.relationship_end = ''>
		</cfif>
		<!--- Save the modified association --->
		<cfset this.propertyClientCompanyAssociationDelegate.saveExistingPropertyClientCompanyAssociation(
				PropertyClientCompanyAssociation=local.PropertyClientCompanyAssociation)/>

		<!--- Return the modified property address --->
		<cfreturn local.PropertyClientCompanyAssociation>

	</cffunction>

	<cffunction name="saveNewPropertyClientContactAssociation"
				access="public" returntype="numeric"
				hint="This method is used to persist a new property / client contact association record to the application database.">
		<cfargument name="PropertyClientContactAssociation" type="mce.e2.vo.PropertyClientContactAssociation" required="true" hint="Describes the property / client company VO object containing the details of the association to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.assocObject = setAuditProperties(arguments.PropertyClientContactAssociation, "create")>

		<!--- Check and see if a client contact exists with the same client contact uid --->
		<cfset local.clientContactRecord = this.ClientContactDelegate.getClientContact(
				client_contact_uid=local.assocObject.client_contact_uid)>

		<!--- Was a client contact record found? --->
		<cfif local.clientContactRecord.recordCount eq 0>

			<!--- Create a reference to the client contact service --->
			<cfset local.clientContactService = request.beanFactory.getBean("ClientContactService")>

			<!--- If not, then create a new client contact record --->
			<cfset local.clientContact = local.clientContactService.getEmptyClientContactVo()>
			<cfset local.clientContact = setAuditProperties(local.clientContact, "create")>

			<!--- Add the first / last name --->
			<cfset local.clientContact.first_name = local.assocObject.first_name>
			<cfset local.clientContact.last_name = local.assocObject.last_name>
			<cfset local.clientContact.external_identifier = local.assocObject.external_identifier>
			<cfset local.clientContact.client_company_uid = local.assocObject.client_company_uid>

			<!--- Save the client contact --->
			<cfset local.newClientContact = local.clientContactService.saveAndReturnNewClientContact(local.clientContact)>

			<!--- Add the new client contact record to the association object --->
			<cfset local.assocObject.client_contact_uid = local.newClientContact.client_contact_uid>

		<cfelse>

			<!--- If so, then set the existing client contact id for the association --->
			<cfset local.assocObject.client_contact_uid = local.clientContactRecord.client_contact_uid>

		</cfif>

		<!--- Save the modified property / client contact association --->
		<cfset local.primaryKey = this.PropertyClientContactAssociationDelegate.saveNewPropertyClientContactAssociation(
				PropertyClientContactAssociation=local.assocObject)/>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewPropertyClientContactAssociation"
				access="public" returntype="mce.e2.vo.PropertyClientContactAssociation"
				hint="This method is used to persist a new property / client contact association record to the application database.">
		<cfargument name="PropertyClientContactAssociation" type="mce.e2.vo.PropertyClientContactAssociation" required="true" hint="Describes the property / client company VO object containing the details of the association to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewPropertyClientContactAssociation(
				PropertyClientContactAssociation=arguments.PropertyClientContactAssociation)/>

		<!--- Set the primary key --->
		<cfset arguments.PropertyClientContactAssociation.relationship_id = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.PropertyClientContactAssociation>

	</cffunction>

	<cffunction name="saveExistingPropertyClientContactAssociation"
				access="public" returntype="mce.e2.vo.PropertyClientContactAssociation"
				hint="This method is used to save existing property / client contact association information to the application database.">
		<cfargument name="PropertyClientContactAssociation" type="mce.e2.vo.PropertyClientContactAssociation" required="true" hint="Describes the existing property / client company VO object containing the details of the association to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.PropertyClientContactAssociation = setAuditProperties(arguments.PropertyClientContactAssociation, "modify")>

		<!--- Get existing properties needed for the update. --->
		<cfset local.assocObject = this.clientContactDelegate.getClientContactAsComponent(
						client_contact_uid = arguments.PropertyClientContactAssociation.client_contact_uid,
						property_uid = arguments.PropertyClientContactAssociation.property_uid,
						contact_role_uid = arguments.PropertyClientContactAssociation.contact_role_uid)>
		<cfset local.PropertyClientContactAssociation.relationship_id = local.assocObject.relationship_id>

		<!--- Save the modified association --->
		<cfset this.PropertyClientContactAssociationDelegate.saveExistingPropertyClientContactAssociation(
				PropertyClientContactAssociation=local.PropertyClientContactAssociation)/>

		<!--- Return the modified property address --->
		<cfreturn local.PropertyClientContactAssociation>

	</cffunction>

	<cffunction name="saveAndReturnNewPropertyWithCompanyAddressAssociation"
				access="public" returntype="mce.e2.vo.Property"
				hint="This method is used to persist a new property, property address, and property / client company association.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object containing the details of the property to be saved." />
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object being associated to a given property." />
		<cfargument name="CompanyRole" type="mce.e2.vo.CompanyRole" required="true" hint="Describes the company role VO object being associated to a given property." />
		<cfargument name="PropertyAddress" type="mce.e2.vo.PropertyAddress" required="true" hint="Describes the property address VO object containing the details of the property address to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Step 1: Save the property --->
		<cfset local.property = saveAndReturnNewProperty(arguments.property)>

		<!--- Step 2: Save the property address --->
		<cfset arguments.propertyAddress.property_uid = local.property.property_uid>
		<cfset local.propertyAddress = saveAndReturnNewPropertyAddress(arguments.propertyAddress)>

		<!--- Step 3:  Save the property / client company association --->
		<cfset local.assocObject = getEmptyPropertyClientCompanyAssociationVo()>

		<!--- Populate the association object's properties --->
		<cfset local.assocObject.property_uid = local.property.property_uid>
		<cfset local.assocObject.client_company_uid = arguments.clientCompany.client_company_uid>
		<cfset local.assocObject.company_role_uid = arguments.companyRole.company_role_uid>
		<cfset local.assocObject.relationship_start = createOdbcDateTime(now())>

		<!--- Save the association --->
		<cfset saveNewPropertyClientCompanyAssociation(
				PropertyClientCompanyAssociation=local.assocObject)>

		<!--- Return the newly created property --->
		<cfreturn local.property>

	</cffunction>

	<cffunction name="setAppSettingsBean" hint="Called by the IOC framework">
		<cfargument name="bean" type="mce.e2.config.AppSettingsBean">
		<cfset this.appSettingsBean = bean>
	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="uploadPropertyImage"
				access="public" returntype="string"
				hint="This method is used to upload a property image.">

		<!--- Define the arguments for this method --->
		<cfargument name="property_uid" type="string" required="true" hint="Describes the primary key / unique identifier of the property.">
		<cfargument name="file_content" type="string" required="true" hint="Describes the file content of the file being added to a given document repository.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get a reference to this document. --->
		<cfset local.property = this.PropertyDelegate.getPropertyAsComponent(property_uid=arguments.property_uid)>

		<!--- Get the folder where property images are stored. --->
		<cfset local.SessionBeanFactory = request.beanFactory.getBean("SessionBeanFactory")>
		<cfset local.session = local.SessionBeanFactory.getSession()>
		<cfset local.file_path = local.session.propertyImages.PropertyImagesAbsolutePath>

		<!--- Recieve the uploaded file --->
		<cffile action="upload"
				nameconflict="overwrite"
				filefield="file_content"
				destination="#local.file_path#"
				result="local.cffile">

		<!--- Determine the filename that should be used. --->
		<cfset local.property.image_filename = left(REReplaceNoCase(local.property.friendly_name,"\W ","","All"), 50) & " - " & local.property.property_uid & "." & local.cffile.serverFileExt />

		<cffile action="rename"
				source="#local.file_path##local.cffile.serverFile#"
				destination="#local.file_path##local.property.image_filename#">

		<!--- Save the updated property --->
		<cfset local.property = saveExistingProperty(local.property)>

		<cfreturn local.property.image_filename>
	</cffunction>

</cfcomponent>