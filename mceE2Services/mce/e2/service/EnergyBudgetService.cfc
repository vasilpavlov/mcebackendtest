<cfcomponent displayname="Energy Budget Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Energy Budget information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyBudgetDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Energy Budget data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyBudgetDelegate" hint="Describes the *.cfc used to manage database interactions related to Energy Budget information.">
		<cfset this.energyBudgetDelegate = arguments.bean>
	</cffunction>

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyEntryDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Energy Entry data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyEntryDelegate" hint="Describes the *.cfc used to manage database interactions related to Energy Budget information.">
		<cfset this.energyEntryDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyBudgetVo" 
				access="public" returntype="mce.e2.vo.EnergyBudget"
				hint="This method is used to retrieve / return an empty EnergyBudget information value object.">
		<cfreturn this.energyBudgetDelegate.getEmptyEnergyBudgetComponent()>
	</cffunction>


	<cffunction name="getEnergyBudgetSummary" 
				access="public" returntype="any"
				hint="This method is used to retrieve / return a collection of Energy Budgets supported by the application.">
				<cfargument name="property" required="true" type="mce.e2.vo.Property" hint="Describes the primary key / unique identifierfor a given property."/>
				<cfargument name="clientCompany" required="true" type="mce.e2.vo.ClientCompany" hint="Describes the primary key / unique identifierfor a given client company."/>
		<cfreturn this.energyBudgetDelegate.getEnergyBudgetSummary(property=arguments.property, clientCompany = arguments.clientCompany)>
	</cffunction>

	<cffunction name="getAvailableEnergyBudgets" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Energy Budgets supported by the application.">
		<cfreturn this.energyBudgetDelegate.getEnergyBudgetsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyAccountEnergyBudgets" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Energy Budgets for a given energy account.">
		<cfargument name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifierfor a given energy account."/>
		
		<cfreturn this.energyBudgetDelegate.getEnergyAccountEnergyBudgets(energy_account_uid=arguments.energy_account_uid)>
	</cffunction>
	
	<cffunction name="getEnergyBudgets" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Energy Budgets.">
		<cfreturn this.energyBudgetDelegate.getEnergyBudgetAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyBudget" 
				access="public" returntype="mce.e2.vo.EnergyBudget" 
				hint="This method is used to return a populated EnergyBudget vo (value object)."> 
		<cfargument name="energyBudget" type="mce.e2.vo.EnergyBudget" required="true" hint="Describes the EnergyBudget collection VO object containing the properties of the EnergyBudget collection to be retrieved." /> 
		<cfset local = structNew()>
		<cfset local.energyBudget = this.energyBudgetDelegate.getEnergyBudgetAsComponent(energy_budget_uid=arguments.energyBudget.energy_budget_uid)>
		<cfset local.energyBudget.energyUsage = this.energyEntryDelegate.getEnergyUsageRecordsForBudget(energy_budget_uid=arguments.energyBudget.energy_budget_uid)>
		<cfset local.energyBudget.associatedEnergyAccounts = this.energyBudgetDelegate.getAssociatedAccountsForEnergyBudget(energy_budget_uid=arguments.energyBudget.energy_budget_uid)>
		
		<cfreturn local.energyBudget>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyBudget" 
				access="public" returntype="string" 
				hint="This method is used to persist a new EnergyBudget record to the application database."> 
		<cfargument name="energyBudget" type="mce.e2.vo.EnergyBudget" required="true" hint="Describes the EnergyBudget object containing the details of the EnergyBudget to be saved" /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyBudget = setAuditProperties(arguments.energyBudget, "create")>
		<!-- Check if any of the accounts in this budget already exist 
			with the same budget type for the same periods, and if so, throw an error and 
			don't save. -->
		<cfset this.energyBudgetDelegate.checkForAccountPeriodConflicts(energyBudget=local.energyBudget)>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyBudget = setPrimaryKey(local.energyBudget)>

		<!--- Save the modified EnergyBudget --->
		<cfset this.energyBudgetDelegate.saveNewEnergyBudget(energyBudget=local.energyBudget)/>
	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyBudget" 
				access="public" returntype="mce.e2.vo.EnergyBudget" 
				hint="This method is used to persist a new EnergyBudget record to the application database."> 
		<cfargument name="energyBudget" type="mce.e2.vo.EnergyBudget" required="true" hint="Describes the EnergyBudget collection VO object containing the details of the EnergyBudget collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyBudget(energyBudget=arguments.energyBudget)>

		<!--- Set the primary key --->
		<cfset arguments.energyBudget.energy_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.energyBudget>

	</cffunction>

	<cffunction name="saveExistingEnergyBudget" 
				access="public" returntype="mce.e2.vo.EnergyBudget" 
				hint="This method is used to save an existing EnergyBudget to the application database."> 
		<cfargument name="energyBudget" type="mce.e2.vo.energyBudget" required="true" hint="Describes the EnergyBudget VO object containing the details of the EnergyBudget to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.energyBudget = setAuditProperties(arguments.energyBudget, "modify")>
		
		<!--- Save the modified EnergyBudget --->
		<cfset this.energyBudgetDelegate.saveExistingEnergyBudget(energyBudget=local.energyBudget)/>

		<!--- Return the modified EnergyBudget --->
		<cfreturn local.energyBudget>	

	</cffunction>
	
</cfcomponent>0