<cfcomponent displayname="Company Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of company type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setCompanyTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage company type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.CompanyTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to company type information.">
		<cfset this.CompanyTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyCompanyTypeVo" 
				access="public" returntype="mce.e2.vo.CompanyType"
				hint="This method is used to retrieve / return an empty CompanyTypeDelegate information value object.">
		<cfreturn this.CompanyTypeDelegate.getEmptyCompanyTypeComponent()>
	</cffunction>

	<cffunction name="getAvailableCompanyTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of company types supported by the application.">
		<cfreturn this.CompanyTypeDelegate.getCompanyTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getCompanyTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of company types.">
		<cfreturn this.CompanyTypeDelegate.getCompanyTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getCompanyType" 
				access="public" returntype="mce.e2.vo.CompanyType" 
				hint="This method is used to return a populated CompanyType vo (value object)."> 
		<cfargument name="CompanyType" type="mce.e2.vo.CompanyType" required="true" hint="Describes the CompanyTypeDelegate collection VO object containing the properties of the CompanyTypeDelegate collection to be retrieved." /> 
		<cfreturn this.CompanyTypeDelegate.getCompanyTypeAsComponent(company_type_code=arguments.CompanyType.company_type_code)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewCompanyType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new CompanyTypeDelegate record to the application database."> 
		<cfargument name="CompanyType" type="mce.e2.vo.CompanyType" required="true" hint="Describes the CompanyTypeDelegate collection VO object containing the details of the CompanyTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.CompanyTypeDelegate = setAuditProperties(arguments.CompanyTypeDelegate, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.CompanyTypeDelegate = setPrimaryKey(local.CompanyTypeDelegate)>
		
		<!--- Save the modified CompanyTypeDelegate --->
		<cfset this.CompanyTypeDelegate.saveNewCompanyType(CompanyTypeDelegate=local.CompanyTypeDelegate)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.CompanyTypeDelegate.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewCompanyType" 
				access="public" returntype="mce.e2.vo.CompanyType" 
				hint="This method is used to persist a new CompanyTypeDelegate record to the application database."> 
		<cfargument name="CompanyType" type="mce.e2.vo.CompanyType" required="true" hint="Describes the CompanyTypeDelegate collection VO object containing the details of the CompanyTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewCompanyType(CompanyTypeDelegate=arguments.CompanyTypeDelegate)>

		<!--- Set the primary key --->
		<cfset arguments.CompanyTypeDelegate.company_type_code = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.CompanyTypeDelegate>

	</cffunction>

	<cffunction name="saveExistingCompanyType" 
				access="public" returntype="mce.e2.vo.CompanyType" 
				hint="This method is used to save an existing CompanyTypeDelegate information to the application database."> 
		<cfargument name="CompanyTypeDelegate" type="mce.e2.vo.CompanyType" required="true" hint="Describes the CompanyTypeDelegate collection VO object containing the details of the CompanyTypeDelegate collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.CompanyTypeDelegate = setAuditProperties(arguments.CompanyTypeDelegate, "modify")>
		
		<!--- Save the modified CompanyTypeDelegate --->
		<cfset this.CompanyTypeDelegate.saveExistingCompanyType(CompanyTypeDelegate=local.CompanyTypeDelegate)/>

		<!--- Return the modified CompanyTypeDelegate --->
		<cfreturn local.CompanyTypeDelegate>	

	</cffunction>
	
</cfcomponent>