<cfcomponent displayname="Client Service Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of client service type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setClientServiceTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client service type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientServiceTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to client service type information.">
		<cfset this.ClientServiceTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyClientServiceTypeVo" 
				access="public" returntype="mce.e2.vo.ClientServiceType"
				hint="This method is used to retrieve / return an empty ClientServiceTypeDelegate information value object.">
		<cfreturn this.ClientServiceTypeDelegate.getEmptyClientServiceTypeComponent()>
	</cffunction>

	<cffunction name="getAvailableClientServiceTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service types supported by the application.">
		<cfreturn this.ClientServiceTypeDelegate.getClientServiceTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientServiceTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service types.">
		<cfreturn this.ClientServiceTypeDelegate.getClientServiceTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientServiceType" 
				access="public" returntype="mce.e2.vo.ClientServiceType" 
				hint="This method is used to return a populated ClientServiceType vo (value object)."> 
		<cfargument name="ClientServiceType" type="mce.e2.vo.ClientServiceType" required="true" hint="Describes the ClientServiceTypeDelegate collection VO object containing the properties of the ClientServiceTypeDelegate collection to be retrieved." /> 
		<cfreturn this.ClientServiceTypeDelegate.getClientServiceTypeAsComponent(service_type_uid=arguments.ClientServiceType.service_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewClientServiceType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new ClientServiceTypeDelegate record to the application database."> 
		<cfargument name="ClientServiceType" type="mce.e2.vo.ClientServiceType" required="true" hint="Describes the ClientServiceTypeDelegate collection VO object containing the details of the ClientServiceTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.ClientServiceTypeDelegate = setAuditProperties(arguments.ClientServiceTypeDelegate, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.ClientServiceTypeDelegate = setPrimaryKey(local.ClientServiceTypeDelegate)>
		
		<!--- Save the modified ClientServiceTypeDelegate --->
		<cfset this.ClientServiceTypeDelegate.saveNewClientServiceType(ClientServiceTypeDelegate=local.ClientServiceTypeDelegate)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.ClientServiceTypeDelegate.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewClientServiceType" 
				access="public" returntype="mce.e2.vo.ClientServiceType" 
				hint="This method is used to persist a new ClientServiceTypeDelegate record to the application database."> 
		<cfargument name="ClientServiceType" type="mce.e2.vo.ClientServiceType" required="true" hint="Describes the ClientServiceTypeDelegate collection VO object containing the details of the ClientServiceTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewClientServiceType(ClientServiceTypeDelegate=arguments.ClientServiceTypeDelegate)>

		<!--- Set the primary key --->
		<cfset arguments.ClientServiceTypeDelegate.service_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.ClientServiceTypeDelegate>

	</cffunction>

	<cffunction name="saveExistingClientServiceType" 
				access="public" returntype="mce.e2.vo.ClientServiceType" 
				hint="This method is used to save an existing ClientServiceTypeDelegate information to the application database."> 
		<cfargument name="ClientServiceTypeDelegate" type="mce.e2.vo.ClientServiceType" required="true" hint="Describes the ClientServiceTypeDelegate collection VO object containing the details of the ClientServiceTypeDelegate collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.ClientServiceTypeDelegate = setAuditProperties(arguments.ClientServiceTypeDelegate, "modify")>
		
		<!--- Save the modified ClientServiceTypeDelegate --->
		<cfset this.ClientServiceTypeDelegate.saveExistingClientServiceType(ClientServiceTypeDelegate=local.ClientServiceTypeDelegate)/>

		<!--- Return the modified ClientServiceTypeDelegate --->
		<cfreturn local.ClientServiceTypeDelegate>	

	</cffunction>
	
</cfcomponent>