<cfcomponent name="Test User Group Service"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the userGroupService component.">

	<!--- ToDo: [ADL] Add unit tests to test / validate adding groups to user groups. --->

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Create an instance of the baseClientCompany testing component (for company seed data)
			this.companyQAComponent = createObject('component', 'mce.e2.testing.cfunit.tests.TestClientCompanyService').init();
				
			// Get an instance of the Client Company delegate
			this.companyDelegate = request.beanFactory.getBean("ClientCompanyDelegate");
				
			// Get an instance of the user group service
			this.service = request.beanFactory.getBean("UserGroupService");
			this.delegate = request.beanFactory.getBean("UserGroupDelegate");
	
			// Set the seed company
			this.seedCompanies = this.companyDelegate.queryToVoArray(this.companyQAComponent.newClientCompanies, 'mce.e2.vo.ClientCompany');
			this.seedCompany = this.seedCompanies[1];
				
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyUserGroupComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty user group component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the user group delegate --->
		<cfset local.userGroup = this.service.getEmptyUserGroupVo()>

		<!--- Assert that the userGroup object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.userGroup, "mce.e2.vo.UserGroup"), true, "local.userGroup objectType is not of type mce.e2.vo.UserGroup.")>
			
	</cffunction>

	<cffunction name="getUserGroups"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all user groups from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the created count --->
		<cfset local.createdCount = 0>
								
		<!--- Build out the array of user group objects --->
		<cfset local.newUserGroups = this.delegate.queryToVoArray(this.newUserGroups, 'mce.e2.vo.UserGroup')/>
					
		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset local.testUserGroupPrimaryKey = this.service.saveNewUserGroup(local.newUserGroups[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserGroupPrimaryKey), "The value returned by this.service.saveNewUserGroup() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newUserGroups), "The total number of records processed does not match the total number of records iterated over when creating user group records.")/>

		<!--- Get the user groups associated to the company --->
		<cfset local.userGroupCollection = this.service.getUserGroups(clientCompany=this.seedCompany)>

		<!--- Validate that the total records created equals the records processed --->
		<cfset assertTrue(isArray(local.userGroupCollection), "The return result from the method this.service.getUserGroups is not of type array.")/>

		<!--- Compare the user group properties and excersize retrieving single instances of user groups --->
		<cfset compareUserGroupProperties(local.userGroupCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getUserGroup"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single user group from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the created count --->
		<cfset local.createdCount = 0>
								
		<!--- Build out the array of user group objects --->
		<cfset local.newUserGroups = this.delegate.queryToVoArray(this.newUserGroups, 'mce.e2.vo.UserGroup')/>
					
		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset local.testUserGroupPrimaryKey = this.service.saveNewUserGroup(local.newUserGroups[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserGroupPrimaryKey), "The value returned by this.service.saveNewUserGroup() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newUserGroups), "The total number of records processed does not match the total number of records iterated over when creating user group records.")/>
				
		<!--- Initialize the user group delegate --->
		<cfset local.userGroupCollection = this.service.getUserGroups(clientCompany=this.seedCompany)>
				
		<!--- Loop through the user group collection, and retrieve each user group individually --->
		<cfloop from="1" to="#arraylen(local.userGroupCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the userGroup to test --->
			<cfset local.userGroup = local.userGroupCollection[local.arrayIndex]>

			<!--- Retrrive a single instance of a user group from the service --->
			<cfset local.testUserGroup = this.service.getUserGroup(local.userGroup)>

			<!--- Assert that the userGroup array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testUserGroup, "mce.e2.vo.UserGroup"), true, "local.testUserGroup objectType is not of type mce.e2.vo.UserGroup.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of user group data without specifying primary keys --->
	<cffunction name="saveAndValidateNewUserGroupsWithoutPrimaryKeys"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of user groups using the saveNewUserGroup() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user group objects --->
		<cfset local.newUserGroups = this.delegate.queryToVoArray(this.newUserGroupsWithoutPks, 'mce.e2.vo.UserGroup')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdUserGroups = arrayNew(1)/>
		
		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset local.testUserGroupPrimaryKey = this.service.saveNewUserGroup(local.newUserGroups[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserGroupPrimaryKey), "The value returned by this.service.saveNewUserGroup() was not a unique identifier.")/>
	
			<!--- Add the primary key to the user group --->
			<cfset local.testUserGroup = local.newUserGroups[local.arrayIndex]>	
			<cfset local.testUserGroup.user_group_uid = local.testUserGroupPrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdUserGroups, duplicate(local.testUserGroup))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdUserGroups), arrayLen(local.newUserGroups), "The total number of records processed does not match the total number of records iterated over when creating user group records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserGroupProperties(sourceObject=local.createdUserGroups, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()> 			
							
	</cffunction>

	<!--- Test the creation of user group data --->
	<cffunction name="saveAndValidateNewUserGroups"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of user groups using the saveNewUserGroup() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user group objects --->
		<cfset local.newUserGroups = this.delegate.queryToVoArray(this.newUserGroups, 'mce.e2.vo.UserGroup')/>
										
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>
				
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset local.testUserGroupPrimaryKey = this.service.saveNewUserGroup(local.newUserGroups[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserGroupPrimaryKey), "The value returned by this.service.saveNewUserGroup() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newUserGroups), "The total number of records processed does not match the total number of records iterated over when creating user group records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserGroupProperties(sourceObject=local.newUserGroups, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewUserGroups"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of user groups using the saveAndReturnNewUserGroup() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test user groups array --->			
		<cfset local.testUserGroups = arrayNew(1)>			
						
		<!--- Build out the array of user group objects --->
		<cfset local.newUserGroups = this.delegate.queryToVoArray(this.newUserGroupsWithoutPks, 'mce.e2.vo.UserGroup')/>
		
		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset local.testUserGroup = this.service.saveAndReturnNewUserGroup(local.newUserGroups[local.arrayIndex])/>
			
			<!--- Assert that the userGroup object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testUserGroup, "mce.e2.vo.UserGroup"), true, "local.userGroup objectType is not of type mce.e2.vo.UserGroup.")>
						
			<!--- Append the user group to the test array --->			
			<cfset arrayAppend(local.testUserGroups, local.testUserGroup)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testUserGroups), arrayLen(local.newUserGroups), "The total number of records processed does not match the total number of records iterated over when creating user group records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserGroupProperties(sourceObject=local.testUserGroups, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingUserGroups"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of user groups.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user group objects --->
		<cfset local.newUserGroups = this.delegate.queryToVoArray(this.newUserGroups, 'mce.e2.vo.UserGroup')/>
		<cfset local.modifiedUserGroups = this.delegate.queryToVoArray(this.modifiedUserGroups, 'mce.e2.vo.UserGroup')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset this.service.saveNewUserGroup(local.newUserGroups[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newUserGroups), "The total number of records processed does not match the total number of records iterated over when creating user group records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserGroupProperties(sourceObject=local.newUserGroups, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset this.service.saveExistingUserGroup(local.modifiedUserGroups[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedUserGroups), "The total number of records processed does not match the total number of records iterated over when modifying user group records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserGroupProperties(sourceObject=local.modifiedUserGroups, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user group test data --->	
		<cfset purgeUserGroupTestData()> 			
							
	</cffunction>	

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeUserGroupTestData"
				access="private"
				hint="This method is used to purge / delete any user group data related to the test data being used when user group unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="userGroups" type="query" hint="This argument is used to identify the user group data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the user group data --->					
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.users_userGroups
			where	user_group_uid in (

						select	user_group_uid
						from	dbo.userGroups
						where	friendly_name in (			
			
									<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedUserGroups.friendly_name)#">
			
								)
			
						union
						
						select	user_group_uid
						from	dbo.userGroups
						where	friendly_name in (			
			
									<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newUserGroups.friendly_name)#">
			
								)
			
					)
										
		</cfquery>			
												
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.userGroups
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newUserGroups.friendly_name)#">

					)
					
		</cfquery>			
			
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.userGroups
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedUserGroups.friendly_name)#">

					)
					
		</cfquery>				
			
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.propertyCollections_Users
			where	property_collection_uid in (
			
						select	property_collection_uid
						from	dbo.propertyCollections
						where	client_company_uid in (
						
									select	client_company_uid
									from	dbo.clientCompanies
									where	friendly_name in (
							
										<!--- Create a value list of the client company friendly names --->	
										<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
										
									)
								
								)
					
					)
		
		</cfquery>				
			
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.propertyCollections
			where	client_company_uid in (
			
						select	client_company_uid
						from	dbo.clientCompanies
						where	friendly_name in (
				
							<!--- Create a value list of the client company friendly names --->	
							<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
							
						)
					
					)
		
		</cfquery>				

		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.propertyCollections_Users
			where	user_uid in (
			
						select	user_uid
						from	dbo.users
						where	client_company_uid in (
						
									select	client_company_uid
									from	dbo.clientCompanies
									where	friendly_name in (
							
										<!--- Create a value list of the client company friendly names --->	
										<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
										
									)
								
								)
					
					)
		
		</cfquery>	

		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.userSessions
			where	user_uid in (
			
						select	user_uid
						from	dbo.users
						where	client_company_uid in (
						
									select	client_company_uid
									from	dbo.clientCompanies
									where	friendly_name in (
							
										<!--- Create a value list of the client company friendly names --->	
										<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
										
									)
								
								)
								
					)
		
		</cfquery>	
		
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.users
			where	client_company_uid in (
			
						select	client_company_uid
						from	dbo.clientCompanies
						where	friendly_name in (
				
							<!--- Create a value list of the client company friendly names --->	
							<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
							
						)
					
					)
		
		</cfquery>				
			
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.clientCompanies
			where	friendly_name in (
			
						<!--- Create a value list of the client company friendly names --->	
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
						
					)
		
		</cfquery>							
						
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the user group service and delegate tests.">
		
		<cfscript>
		
			//Build out the user group test data (for create / modify scenarios)
			createUserGroupTestData();
			
			// Purge any related user group data
			purgeUserGroupTestData();
		
		</cfscript>		
				
	</cffunction>
	
	<cffunction name="CreateSeedClientCompany"
				access="private"
				hint="This method is used to setup test data for the PropertyCollection service and delegate tests.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>		
				
		<cfscript>
			
			// Save the client company
			this.companyDelegate.saveNewClientCompany(this.seedCompany);

		</cfscript>		
				
	</cffunction>	
	
	<cffunction name="createUserGroupTestData"
				access="private"
				hint="This method is used to create the test data for testing user group records (create and update actions).">

		<cfscript>			
						
			// Initialize a query containing user group data to be tested with (the query should contain all of the columns of the base table)
			this.newUserGroups = queryNew('client_company_uid,user_group_uid,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newUserGroups,4);
		
			// Set the cell values for this instance of a user group
			querySetCell(this.newUserGroups,'client_company_uid', this.seedCompany.client_company_uid, 1);
			querySetCell(this.newUserGroups,'user_group_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newUserGroups,'friendly_name', 'UR_001', 1);
			querySetCell(this.newUserGroups,'is_active', 1, 1);
			querySetCell(this.newUserGroups,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newUserGroups,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newUserGroups,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newUserGroups,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newUserGroups,'client_company_uid', this.seedCompany.client_company_uid, 2);
			querySetCell(this.newUserGroups,'user_group_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newUserGroups,'friendly_name', 'UR_002', 2);
			querySetCell(this.newUserGroups,'is_active', 1, 2);
			querySetCell(this.newUserGroups,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newUserGroups,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newUserGroups,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newUserGroups,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newUserGroups,'client_company_uid', this.seedCompany.client_company_uid, 3);
			querySetCell(this.newUserGroups,'user_group_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newUserGroups,'friendly_name', 'UR_003', 3);
			querySetCell(this.newUserGroups,'is_active', 1, 3);
			querySetCell(this.newUserGroups,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newUserGroups,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newUserGroups,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newUserGroups,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newUserGroups,'client_company_uid', this.seedCompany.client_company_uid, 4);
			querySetCell(this.newUserGroups,'user_group_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newUserGroups,'friendly_name', 'UR_004', 4);
			querySetCell(this.newUserGroups,'is_active', 1, 4);
			querySetCell(this.newUserGroups,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newUserGroups,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newUserGroups,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newUserGroups,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of userGroups without primary keys
			this.newUserGroupsWithoutPks = duplicate(this.newUserGroups);
			
			// Remote the primary keys from the user group test data 
			querySetCell(this.newUserGroupsWithoutPks,'user_group_uid', '', 1);
			querySetCell(this.newUserGroupsWithoutPks,'user_group_uid', '', 2);
			querySetCell(this.newUserGroupsWithoutPks,'user_group_uid', '', 3);
			querySetCell(this.newUserGroupsWithoutPks,'user_group_uid', '', 4);
					
			// Duplicate the new user groups to create the baseline modified user groups
			this.modifiedUserGroups = duplicate(this.newUserGroups);

			// Update the friendly names for the modified user groups
			querySetCell(this.modifiedUserGroups,'friendly_name', 'URM_001', 1);
			querySetCell(this.modifiedUserGroups,'friendly_name', 'URM_002', 2);
			querySetCell(this.modifiedUserGroups,'friendly_name', 'URM_003', 3);			
			querySetCell(this.modifiedUserGroups,'friendly_name', 'URM_004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedUserGroups,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedUserGroups,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedUserGroups,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedUserGroups,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareUserGroupProperties"
				access="private"
				hint="This method is used to compare the properties for a given user group vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of user group vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the user group from the application database --->
				<cfset local.retrievedUserGroup = this.delegate.getUserGroupAsComponent(arguments.sourceObject[local.arrayIndex].rate_factor_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the user group from the application database --->
				<cfset local.retrievedUserGroup = this.service.getUserGroup(userGroup=arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared user group VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedUserGroup, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved userGroup VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedUserGroup[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedUserGroup[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>

</cfcomponent>