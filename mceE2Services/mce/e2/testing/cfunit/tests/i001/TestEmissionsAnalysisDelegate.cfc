<cfcomponent extends="mxunit.framework.TestCase" output="true">

	<cffunction name="getEmissionsSourcesHierarchy" output="true">
		<cfset var delegate = request.beanFactory.getBean("EmissionsAnalysisDelegate")>
		<cfset var sources = delegate.getEmissionsSourcesHierarchy()>

		<cfset assertTrue(sources.recordCount gt 0,
			"Expected to retrieve at least one emissions source")>
		<cfset assertTrue(sources.friendly_name[1] eq "All Emissions Sources",
			"Expected the first emissions source record to be for 'All Emissions Sources'")>
	</cffunction>

	<cffunction name="getEmissionsAnalysis" output="true">
		<cfset var delegate = request.beanFactory.getBean("EmissionsAnalysisDelegate")>
		<cfset var analysis = "">

		<cfinvoke
			component="#delegate#"
			method="getEmissionsAnalysis"
			property_uid_list="7A859DFD-9613-4568-B46E-60F9EBBF9B9E"
			period_start="1/1/2008"
			period_end="3/31/2008"
			dateRollupMode="month-prorated"
			returnvariable="analysis">

		<cfset assertTrue(analysis.recordCount gt 0,
			"Expected to retrieve at least one analysis record")>

		<cfdump var="#analysis#">
	</cffunction>



</cfcomponent>