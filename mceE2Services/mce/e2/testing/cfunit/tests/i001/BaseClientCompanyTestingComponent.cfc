<cfcomponent name="Base Client Company Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the clientCompany delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeClientCompanyTestData"
				access="private"
				hint="This method is used to purge / delete any client company data related to the test data being used when client company unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="clientCompanies" type="query" hint="This argument is used to identify the client company data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the client company data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.clientCompanies
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newClientCompanies.friendly_name)#">

					)
		
			delete
			from	dbo.clientCompanies
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedClientCompanies.friendly_name)#">

					)		
		
		</cfquery>
			
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the client company service and delegate tests.">
		
		<cfscript>
		
			//Build out the client company test data (for create / modify scenarios)
			createClientCompanyTestData();

		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createClientCompanyTestData"
				access="private"
				hint="This method is used to create the test data for testing client company records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing client company data to be tested with (the query should contain all of the columns of the base table)
			this.newClientCompanies = queryNew('client_company_uid,friendly_name,company_hierarchyId,oldId,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newClientCompanies,4);
		
			// Set the cell values for this instance of a client company
			querySetCell(this.newClientCompanies,'client_company_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newClientCompanies,'friendly_name', 'CC 001', 1);
			querySetCell(this.newClientCompanies,'company_hierarchyId', '/', 1);
			querySetCell(this.newClientCompanies,'oldId', 1, 1);
			querySetCell(this.newClientCompanies,'is_active', 1, 1);
			querySetCell(this.newClientCompanies,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newClientCompanies,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newClientCompanies,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newClientCompanies,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newClientCompanies,'client_company_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newClientCompanies,'friendly_name', 'CC 002', 2);
			querySetCell(this.newClientCompanies,'company_hierarchyId', '/', 2);
			querySetCell(this.newClientCompanies,'oldId', 2, 2);
			querySetCell(this.newClientCompanies,'is_active', 1, 2);
			querySetCell(this.newClientCompanies,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newClientCompanies,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newClientCompanies,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newClientCompanies,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newClientCompanies,'client_company_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newClientCompanies,'friendly_name', 'CC 003', 3);
			querySetCell(this.newClientCompanies,'company_hierarchyId', '/', 3);
			querySetCell(this.newClientCompanies,'oldId', 3, 3);
			querySetCell(this.newClientCompanies,'is_active', 1, 3);
			querySetCell(this.newClientCompanies,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newClientCompanies,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newClientCompanies,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newClientCompanies,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newClientCompanies,'client_company_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newClientCompanies,'friendly_name', 'CC 004', 4);
			querySetCell(this.newClientCompanies,'company_hierarchyId', '/', 4);
			querySetCell(this.newClientCompanies,'oldId', 4, 4);
			querySetCell(this.newClientCompanies,'is_active', 1, 4);
			querySetCell(this.newClientCompanies,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newClientCompanies,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newClientCompanies,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newClientCompanies,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of clientCompanies without primary keys
			this.newClientCompaniesWithoutPks = duplicate(this.newClientCompanies);
			
			// Remote the primary keys from the client company test data 
			querySetCell(this.newClientCompaniesWithoutPks,'client_company_uid', '', 1);
			querySetCell(this.newClientCompaniesWithoutPks,'client_company_uid', '', 2);
			querySetCell(this.newClientCompaniesWithoutPks,'client_company_uid', '', 3);
			querySetCell(this.newClientCompaniesWithoutPks,'client_company_uid', '', 4);
					
			// Duplicate the new client companies to create the baseline modified client companies
			this.modifiedClientCompanies = duplicate(this.newClientCompanies);

			// Update the friendly names for the modified client companies
			querySetCell(this.modifiedClientCompanies,'friendly_name', 'CCM 001', 1);
			querySetCell(this.modifiedClientCompanies,'friendly_name', 'CCM 002', 2);
			querySetCell(this.modifiedClientCompanies,'friendly_name', 'CCM 003', 3);			
			querySetCell(this.modifiedClientCompanies,'friendly_name', 'CCM 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedClientCompanies,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedClientCompanies,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedClientCompanies,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedClientCompanies,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareClientCompanyProperties"
				access="private"
				hint="This method is used to compare the properties for a given client company vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of client company vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the client company from the application database --->
				<cfset local.retrievedClientCompany = this.delegate.getClientCompanyAsComponent(arguments.sourceObject[local.arrayIndex].company_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the client company from the application database --->
				<cfset local.retrievedClientCompany = this.service.getClientCompany(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared client company VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedClientCompany, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved clientCompany VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedClientCompany[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedClientCompany[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>