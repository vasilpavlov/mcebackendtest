<cfcomponent name="Test Client Company Service"
			 extends="baseClientCompanyTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the clientCompanyService component.">

	<!--- ToDo: [ADL] Add a unit test for getUsers() associated to a specific company. --->

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the client company service
			this.service = request.beanFactory.getBean("ClientCompanyService");
			this.delegate = request.beanFactory.getBean("ClientCompanyDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyClientCompanyComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty client company component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the client company delegate --->
		<cfset local.clientCompany = this.service.getEmptyClientCompanyVo()>

		<!--- Assert that the clientCompany object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.clientCompany, "mce.e2.vo.ClientCompany"), true, "local.clientCompany objectType is not of type mce.e2.vo.ClientCompany.")>
			
	</cffunction>

	<cffunction name="getClientCompanies"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all client companies from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the client company delegate --->
		<cfset local.clientCompanyCollection = this.service.getClientCompanies()>
				
		<!--- Assert that the clientCompany array collection is an array --->
		<cfset assertTrue(isArray(local.clientCompanyCollection), "The value returned by this.service.getClientCompanies() was not an array.")>

		<!--- Compare the client company properties and excersize retrieving single instances of client companies --->
		<cfset compareClientCompanyProperties(local.clientCompanyCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getClientCompany"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single client company from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the client company delegate --->
		<cfset local.clientCompanyCollection = this.service.getClientCompanies()>
				
		<!--- Loop through the client company collection, and retrieve each client company individually --->
		<cfloop from="1" to="#arraylen(local.clientCompanyCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the clientCompany to test --->
			<cfset local.clientCompany = local.clientCompanyCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a client company from the service --->
				<cfset local.testClientCompany = this.service.getClientCompany(local.clientCompany)>

			<!--- Assert that the clientCompany array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testClientCompany, "mce.e2.vo.ClientCompany"), true, "local.testClientCompany objectType is not of type mce.e2.vo.ClientCompany.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of client company data without specifying primary keys --->
	<cffunction name="saveAndValidateNewClientCompaniesWithoutPrimaryKeys"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of client companies using the saveNewClientCompany() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of client company objects --->
		<cfset local.newClientCompanies = this.delegate.queryToVoArray(this.newClientCompaniesWithoutPks, 'mce.e2.vo.ClientCompany')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdClientCompanies = arrayNew(1)/>
		
		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newClientCompanies)#" index="local.arrayIndex">	

			<!--- Save the client company to the application database --->
			<cfset local.testClientCompanyPrimaryKey = this.service.saveNewClientCompany(local.newClientCompanies[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testClientCompanyPrimaryKey), "The value returned by this.service.saveNewClientCompany() was not a unique identifier.")/>
	
			<!--- Add the primary key to the client company --->
			<cfset local.testClientCompany = local.newClientCompanies[local.arrayIndex]>	
			<cfset local.testClientCompany.client_company_uid = local.testClientCompanyPrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdClientCompanies, duplicate(local.testClientCompany))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdClientCompanies), arrayLen(local.newClientCompanies), "The total number of records processed does not match the total number of records iterated over when creating client company records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareClientCompanyProperties(sourceObject=local.createdClientCompanies, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()> 			
							
	</cffunction>

	<!--- Test the creation of client company data --->
	<cffunction name="saveAndValidateNewClientCompanies"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of client companies using the saveNewClientCompany() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of client company objects --->
		<cfset local.newClientCompanies = this.delegate.queryToVoArray(this.newClientCompanies, 'mce.e2.vo.ClientCompany')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newClientCompanies)#" index="local.arrayIndex">	

			<!--- Save the client company to the application database --->
			<cfset local.testClientCompanyPrimaryKey = this.service.saveNewClientCompany(local.newClientCompanies[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testClientCompanyPrimaryKey), "The value returned by this.service.saveNewClientCompany() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newClientCompanies), "The total number of records processed does not match the total number of records iterated over when creating client company records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareClientCompanyProperties(sourceObject=local.newClientCompanies, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewClientCompanies"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of client companies using the saveAndReturnNewClientCompany() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test client companies array --->			
		<cfset local.testClientCompanies = arrayNew(1)>			
						
		<!--- Build out the array of client company objects --->
		<cfset local.newClientCompanies = this.delegate.queryToVoArray(this.newClientCompaniesWithoutPks, 'mce.e2.vo.ClientCompany')/>
		
		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newClientCompanies)#" index="local.arrayIndex">	

			<!--- Save the client company to the application database --->
			<cfset local.testClientCompany = this.service.saveAndReturnNewClientCompany(local.newClientCompanies[local.arrayIndex])/>
			
			<!--- Assert that the clientCompany object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testClientCompany, "mce.e2.vo.ClientCompany"), true, "local.clientCompany objectType is not of type mce.e2.vo.ClientCompany.")>
						
			<!--- Append the client company to the test array --->			
			<cfset arrayAppend(local.testClientCompanies, local.testClientCompany)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testClientCompanies), arrayLen(local.newClientCompanies), "The total number of records processed does not match the total number of records iterated over when creating client company records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareClientCompanyProperties(sourceObject=local.testClientCompanies, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingClientCompanies"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of client companies.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of client company objects --->
		<cfset local.newClientCompanies = this.delegate.queryToVoArray(this.newClientCompanies, 'mce.e2.vo.ClientCompany')/>
		<cfset local.modifiedClientCompanies = this.delegate.queryToVoArray(this.modifiedClientCompanies, 'mce.e2.vo.ClientCompany')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newClientCompanies)#" index="local.arrayIndex">	

			<!--- Save the client company to the application database --->
			<cfset this.service.saveNewClientCompany(local.newClientCompanies[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newClientCompanies), "The total number of records processed does not match the total number of records iterated over when creating client company records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareClientCompanyProperties(sourceObject=local.newClientCompanies, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedClientCompanies)#" index="local.arrayIndex">	

			<!--- Save the client company to the application database --->
			<cfset this.service.saveExistingClientCompany(local.modifiedClientCompanies[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedClientCompanies), "The total number of records processed does not match the total number of records iterated over when modifying client company records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareClientCompanyProperties(sourceObject=local.modifiedClientCompanies, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the client company test data --->	
		<cfset purgeClientCompanyTestData()> 			
							
	</cffunction>	

</cfcomponent>