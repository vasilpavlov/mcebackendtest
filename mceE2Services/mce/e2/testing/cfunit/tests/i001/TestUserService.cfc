<cfcomponent name="Test User Service"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the userService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Step 1:  Instantiate any components
			// Create an instance of the baseClientCompany testing component (for company seed data)
			this.companyQAComponent = createObject('component', 'mce.e2.testing.cfunit.tests.TestClientCompanyService').init();
				
			// Get an instance of the Client Company delegate
			this.companyDelegate = request.beanFactory.getBean("ClientCompanyDelegate");
			this.securityService = request.beanFactory.getBean("securityService");
			
			// Get an instance of the user service
			this.service = request.beanFactory.getBean("UserService");
			this.delegate = request.beanFactory.getBean("UserDelegate");			
			
			// Step 2:  Define the test variables / configuraiton properties
			// Define the seed company array index
			this.seedCompanyArrayIndex = 1;
				
			// Define the total number of test users to generate
			this.testDataRowCount = 4;	
	
			// Step 3:  Initialize the test component
			// Set the seed company
			this.seedCompany = getSeedCompany();
				
			// Setup the test data for the unit tests
			createTestData();
				
		</cfscript>		
				
	</cffunction>

	<cffunction name="createTestData"
				access="private"
				hint="This method is used to create the test data for testing user records (create and update actions).">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cfscript>			
						
			// Initialize a query containing user data to be tested with (the query should contain all of the columns of the base table)
			this.newUsers = queryNew('client_company_uid,user_uid,username,password,password_hash_code,password_hash_method,first_name,last_name,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newUsers,this.testDataRowCount);
		
			// Initialize the query data 
			for( local.rowIndex=1; local.rowIndex lte this.testDataRowCount; local.rowIndex = local.rowIndex + 1){
		
				// Set the cell values for this instance of a user group
				querySetCell(this.newUsers,'client_company_uid', this.seedCompany.client_company_uid, local.rowIndex);
				querySetCell(this.newUsers,'user_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.newUsers,'username', 'username#local.rowIndex#', local.rowIndex);
				querySetCell(this.newUsers,'password', 'password', local.rowIndex);
				querySetCell(this.newUsers,'password_hash_code', request.beanFactory.getBean("PasswordEncrypter").encryptString(this.newUsers.password[local.rowIndex]), local.rowIndex);
				querySetCell(this.newUsers,'password_hash_method', 1, local.rowIndex);
				querySetCell(this.newUsers,'first_name', 'firstName#local.rowIndex#', local.rowIndex);
				querySetCell(this.newUsers,'last_name', 'lastName#local.rowIndex#', local.rowIndex);												
				querySetCell(this.newUsers,'is_active', 1, local.rowIndex);
				querySetCell(this.newUsers,'created_by', 'MX Unit (created)', local.rowIndex);
				querySetCell(this.newUsers,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.newUsers,'modified_by', 'MX Unit (created)', local.rowIndex);
				querySetCell(this.newUsers,'modified_date', createOdbcDateTime(now()), local.rowIndex);

			}
	
			// Build out the test data set of users without primary keys
			this.newUsersWithoutPks = duplicate(this.newUsers);
			
			// Initialize the query data to create a result set of users without primary keys
			for( local.rowIndex=1; local.rowIndex lte this.testDataRowCount; local.rowIndex = local.rowIndex + 1){

				// Remote the primary keys from the user test data 
				querySetCell(this.newUsersWithoutPks,'user_uid', '', local.rowIndex);
		
			}			
			
	
			// Build out the test data set of users with duplicate user names
			this.newUsersWithDuplicateUserNames = duplicate(this.newUsers);
			
			// Initialize the query data to create a result set of users with duplicate user names
			for( local.rowIndex=1; local.rowIndex lte this.testDataRowCount; local.rowIndex = local.rowIndex + 1){

				// Set all users to have duplicate user names
				querySetCell(this.newUsersWithDuplicateUserNames,'username', 'username1', local.rowIndex);
		
			}			
					
			// Duplicate the new user to create the baseline modified user
			this.modifiedUsers = duplicate(this.newUsers);

			// Initialize the query data to create a result set of users without primary keys
			for( local.rowIndex=1; local.rowIndex lte this.testDataRowCount; local.rowIndex = local.rowIndex + 1){

				// Update the friendly names for the modified user
				querySetCell(this.modifiedUsers,'first_name', 'firstNameMD#local.rowIndex#', local.rowIndex);		
				querySetCell(this.modifiedUsers,'last_name', 'lastNameMD#local.rowIndex#', local.rowIndex);
				querySetCell(this.modifiedUsers,'modified_by', 'MX Unit (modified)', local.rowIndex);

			}	
			
			// Duplicate the modified user to create the baseline modified users with duplicate user names
			this.modifiedUsersWithDuplicateUserNames = duplicate(this.modifiedUsers);

			// Initialize the query data to create a result set of users with duplicate user names
			for( local.rowIndex=1; local.rowIndex lte this.testDataRowCount; local.rowIndex = local.rowIndex + 1){

				// Set all users to have duplicate user names
				querySetCell(this.modifiedUsersWithDuplicateUserNames,'username', 'username1', local.rowIndex);
		
			}	
					
		</cfscript>
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyUserComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty user component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the user delegate --->
		<cfset local.component = this.service.getEmptyUserVo()>

		<!--- Assert that the test component is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.component, "mce.e2.vo.User"), true, "local.component objectType is not of type mce.e2.vo.User.")>
			
	</cffunction>

	<cffunction name="getUsers"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all users from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Create the seed data used to execute this test case --->
		<cfset createUserSeedData()>

		<!--- Get the user associated to the company --->
		<cfset local.userCollection = this.service.getUsers(clientCompany=this.seedCompany)>

		<!--- Validate that the total records created equals the records processed --->
		<cfset assertTrue(isArray(local.userCollection), "The return result from the method this.service.getUsers is not of type array.")/>

		<!--- Compare the user properties and excersize retrieving single instances of user --->
		<cfset compareUserProperties(local.userCollection,'get','service')>
			
		<!--- Purge the test data --->	
		<cfset purgeTestData()>		
				
	</cffunction>

	<cffunction name="getUser"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of individual users from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Create the seed data used to execute this test case --->
		<cfset createUserSeedData()>

		<!--- Initialize the user delegate --->
		<cfset local.userCollection = this.service.getUsers(clientCompany=this.seedCompany)>
				
		<!--- Loop through the user collection, and retrieve each user individually --->
		<cfloop from="1" to="#arraylen(local.userCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the user to test --->
			<cfset local.user = local.userCollection[local.arrayIndex]>

			<!--- Retrrive a single instance of a user from the service --->
			<cfset local.testUser = this.service.getUser(local.user)>

			<!--- Assert that the user array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testUser, "mce.e2.vo.User"), true, "local.testUser objectType is not of type mce.e2.vo.User.")>
	
		</cfloop>
			
		<!--- Purge the test data --->	
		<cfset purgeTestData()>		
				
	</cffunction>

	<cffunction name="saveAndValidateNewUsersWithoutPrimaryKeys"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of users without pre-defined primary keys using the saveNewUser() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.newUsersWithoutPks, 'mce.e2.vo.User')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdUsers = arrayNew(1)/>
		
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUsers)#" index="local.arrayIndex">	

			<!--- Save the user to the application database --->
			<cfset local.testUserPrimaryKey = this.service.saveNewUser(
					user=local.newUsers[local.arrayIndex], password=local.newUsers[local.arrayIndex]["password"])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserPrimaryKey), "The value returned by this.service.saveNewUser() was not a unique identifier.")/>
	
			<!--- Add the primary key to the user --->
			<cfset local.testUser = local.newUsers[local.arrayIndex]>	
			<cfset local.testUser.user_uid = local.testUserPrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdUsers, duplicate(local.testUser))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdUsers), arrayLen(local.newUsers), "The total number of records processed does not match the total number of records iterated over when creating users.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserProperties(sourceObject=local.createdUsers, compareAction="create", compareType="service", ignorePropertyList="password,password_hash,created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>
							
	</cffunction>

	<cffunction name="saveAndValidateNewUsersWithDuplicateUserNames"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of users with duplicate user names using the saveNewUser() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.newUsersWithDuplicateUserNames, 'mce.e2.vo.User')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdUsers = arrayNew(1)/>
		
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUsers)#" index="local.arrayIndex">	

			<cftry>

				<!--- Save the user to the application database --->
				<cfset local.testUserPrimaryKey = this.service.saveNewUser(
						user=local.newUsers[local.arrayIndex], password=local.newUsers[local.arrayIndex]["password"])/>
	
				<!--- Validate that the returned value is a unique identifier --->
				<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserPrimaryKey), "The value returned by this.service.saveNewUser() was not a unique identifier.")/>
		
				<!--- Add the primary key to the user --->
				<cfset local.testUser = local.newUsers[local.arrayIndex]>	
				<cfset local.testUser.user_uid = local.testUserPrimaryKey>
		
				<!--- Increment the created count by one --->
				<cfset arrayAppend(local.createdUsers, duplicate(local.testUser))/>
			
				<cfcatch></cfcatch>
				
			</cftry>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdUsers), 1, "The total number of records processed does not match the total number of records iterated over with unique usernames when creating user records.")/>
		
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>
							
	</cffunction>

	<cffunction name="saveAndValidateNewUsers"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of users with pre-defined primary keys using the saveNewUser() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.newUsers, 'mce.e2.vo.User')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdUsers = arrayNew(1)/>
		
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUsers)#" index="local.arrayIndex">	

			<!--- Save the user to the application database --->
			<cfset local.testUserPrimaryKey = this.service.saveNewUser(
					user=local.newUsers[local.arrayIndex], password=local.newUsers[local.arrayIndex]["password"])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserPrimaryKey), "The value returned by this.service.saveNewUser() was not a unique identifier.")/>
	
			<!--- Add the primary key to the user --->
			<cfset local.testUser = local.newUsers[local.arrayIndex]>	
			<cfset local.testUser.user_uid = local.testUserPrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdUsers, duplicate(local.testUser))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdUsers), arrayLen(local.newUsers), "The total number of records processed does not match the total number of records iterated over when creating users.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserProperties(sourceObject=local.createdUsers, compareAction="create", compareType="service", ignorePropertyList="password,password_hash,created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>
							
	</cffunction>

	<cffunction name="saveAndReturnAndValidateNewUsers"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of users using the saveAndReturnNewUsers() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.newUsers, 'mce.e2.vo.User')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdUsers = arrayNew(1)/>
		
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUsers)#" index="local.arrayIndex">	

			<!--- Save the user to the application database --->
			<cfset local.testUser = this.service.saveAndReturnNewUser(
					user=local.newUsers[local.arrayIndex], password=local.newUsers[local.arrayIndex]["password"])/>

			<!--- Assert that the user object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testUser, "mce.e2.vo.User"), true, "local.testUser objectType is not of type mce.e2.vo.User.")>
		
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdUsers, duplicate(local.testUser))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdUsers), arrayLen(local.newUsers), "The total number of records processed does not match the total number of records iterated over when creating users.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserProperties(sourceObject=local.createdUsers, compareAction="create", compareType="service", ignorePropertyList="password,password_hash,created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>
							
	</cffunction>
			
	<cffunction name="saveAndValidateExistingUsers"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of user.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.newUsers, 'mce.e2.vo.User')/>
		<cfset local.modifiedUsers = this.delegate.queryToVoArray(this.modifiedUsers, 'mce.e2.vo.User')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>				
		
		<!--- Create the seed company --->
		<cfset createUserSeedData()>
			
		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedUsers)#" index="local.arrayIndex">	

			<!--- Save the user to the application database --->
			<cfset this.service.saveExistingUser(
					user=local.modifiedUsers[local.arrayIndex], password=local.modifiedUsers[local.arrayIndex]["password"])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedUsers), "The total number of records processed does not match the total number of records iterated over when modifying user records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserProperties(sourceObject=local.modifiedUsers, compareAction="modify", compareType="service", ignorePropertyList="password,created_date,modified_date,created_by,modified_by")>

	</cffunction>	
	
	<cffunction name="saveAndValidateExistingUsersWithDuplicateUserNames"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of user with a duplicate / existing user name.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.newUsers, 'mce.e2.vo.User')/>
		<cfset local.modifiedUsers = this.delegate.queryToVoArray(this.modifiedUsersWithDuplicateUserNames, 'mce.e2.vo.User')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>				
		
		<!--- Create the seed company --->
		<cfset createUserSeedData()>
			
		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedUsers)#" index="local.arrayIndex">	

			<cftry>

				<!--- Save the user to the application database --->
				<cfset this.service.saveExistingUser(
						user=local.modifiedUsers[local.arrayIndex], password=local.modifiedUsers[local.arrayIndex]["password"])/>
				
				<!--- Increment the modified count by one --->
				<cfset local.modifiedCount = local.modifiedCount + 1/>

				<cfcatch></cfcatch>
			
			</cftry>

		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, 1, "The total number of records processed does not match the total number of records iterated over with unique usernames when modifying user records.")/>
				
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>
								
	</cffunction>		
				
	<!--- Private methods / for internal use only --->
	<cffunction name="purgeTestData"
				access="private"
				hint="This method is used to purge / delete any test data being used when user unit tests are exersized.">
								
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the user data --->					
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.users
			where	client_company_uid in (

					select	client_company_uid			
					from	dbo.clientCompanies
					where	friendly_name in (
					
								<!--- Create a value list of the client company friendly names --->	
								<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
								
							)
			
			
					)
										
		</cfquery>			

		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.propertyCollections
			where	client_company_uid in (

					select	client_company_uid			
					from	dbo.clientCompanies
					where	friendly_name in (
					
								<!--- Create a value list of the client company friendly names --->	
								<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
								
							)
			
			
					)
										
		</cfquery>
						
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.clientCompanies
			where	friendly_name in (
			
						<!--- Create a value list of the client company friendly names --->	
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
						
					)
		
		</cfquery>							
						
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.users
			where	username in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newUsers.username)#">

					)
					
		</cfquery>						
			
	</cffunction>		
		
	<cffunction name="GetSeedCompany"
				access="private"
				returnType="mce.e2.vo.ClientCompany"
				hint="This method is used to retrieve the seed company used for data dependent testing.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>		
				
		<cfscript>
		
			// Convert the client company query to an array of Vo objects
			local.clientCompanyCollection = this.companyDelegate.queryToVoArray(this.companyQAComponent.newClientCompanies, 'mce.e2.vo.ClientCompany');
			
		</cfscript>		
			
		<!--- Return the seed company --->	
		<cfreturn local.clientCompanyCollection[this.seedCompanyArrayIndex]>		
				
	</cffunction>	
	
	<cffunction name="CreateSeedClientCompany"
				access="private"
				hint="This method is used to setup test data for the PropertyCollection service and delegate tests.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>		
				
		<cfscript>
		
			// Convert the client company query to an array of Vo objects
			local.clientCompany = getSeedCompany();
			
			// Save the client company
			this.companyDelegate.saveNewClientCompany(local.clientCompany);

		</cfscript>		
				
	</cffunction>	

	<cffunction name="createUserSeedData"
				access="private"
				output="false" returnType="void"
				hint="This method is used to create user seed data that can be leveraged by other methods.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the created count --->
		<cfset local.createdCount = 0>
								
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.newUsers, 'mce.e2.vo.User')/>
					
		<!--- Purge / remote the user test data --->	
		<cfset purgeTestData()>				
		
		<!--- Create the seed company --->
		<cfset CreateSeedClientCompany()>		
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUsers)#" index="local.arrayIndex">	

			<!--- Save the user to the application database --->
			<cfset local.testPrimaryKey = this.service.saveNewUser(
					user=local.newUsers[local.arrayIndex],password=local.newUsers[local.arrayIndex].password )/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testPrimaryKey), "The value returned by this.service.saveNewUser() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newUsers), "The total number of records processed does not match the total number of records iterated over when creating user records.")/>

		<!--- Get the user  associated to the company --->
		<cfset local.userCollection = this.service.getUsers(clientCompany=this.seedCompany)>

		<!--- Validate that the total records created equals the records processed --->
		<cfset assertTrue(isArray(local.userCollection), "The return result from the method this.service.getUsers is not of type array.")/>

		<!--- Compare the user properties and excersize retrieving single instances of user  --->
		<cfset compareUserProperties(local.userCollection,'get','service')>
				
	</cffunction>

	<cffunction name="compareUserProperties"
				access="private"
				hint="This method is used to compare the properties for a given user Vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of user vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the user leveraging the delegate --->
				<cfset local.retrievedUser = this.delegate.getUserAsComponent(arguments.sourceObject[local.arrayIndex].user_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the user leveraging the service --->
				<cfset local.retrievedUser = this.service.getUser(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only perform the comparison if the property being evaluated is a simple object --->
				<cfif isSimpleValue(arguments.sourceObject[local.arrayIndex][local.thisKey])>

					<!--- Only compare / evaluate properties that are not in the ignore list --->
					<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>
	
						<!--- Define the error message to be rendered when comparing property values --->
						<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared user VO's [testing #arguments.compareAction#].">		
					
						<!--- Validate that the total records created equals the records processed --->
						<cfset assertTrue(structKeyExists(local.retrievedUser, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved user VO component [#arguments.compareAction#].")/>
			
						<!--- Process different comparison logic for date fields --->
						<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
						
							<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
							<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
							
							<!--- Compare the retrieved property against the source property, and see if their values are the same --->
							<cfset assertEquals(trim(createOdbcDateTime(local.retrievedUser[local.thisKey])), trim(local.newDate), local.errorMessage)/>
										
						<cfelse>
			
							<!--- Compare the retrieved property against the source property, and see if their values are the same --->
							<cfset assertEquals(trim(local.retrievedUser[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
												
						</cfif>
	
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>

	<!--- Define any private / internal methods --->
	<cffunction name="createSession"
				access="public"
				returnType="mce.e2.security.sessionBean"
				hint="This method is used to create a session and spoof a login for a given user.">		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>					
	
		<!--- Build out the array of user objects --->
		<cfset local.user = this.delegate.queryRowToStruct(this.newUsers,1)/>

		<!--- Create the session for the current user --->
		<cfset local.session = this.securityService.attemptLogin(local.user.username, local.user.password)>
				
		<!--- Return the session --->		
		<cfreturn local.session>		
				
	</cffunction>

</cfcomponent>