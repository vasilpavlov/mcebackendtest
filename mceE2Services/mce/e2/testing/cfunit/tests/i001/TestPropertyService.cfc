<cfcomponent name="Test Property Service"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the propertyService component.">

	<!--- ToDo: [ADL] Extend the rate model testing service to test rate model input value
				methods (creating, updating, and retrieval).  To support these tests, the
				basePropertyTestingComponent will need to be extended to include creating
				custom data, data validation, etc. --->

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the property service
			this.service = request.beanFactory.getBean("PropertyService");
			this.delegate = request.beanFactory.getBean("PropertyDelegate");
	
			//Build out the property test data (for create / modify scenarios)
			createPropertyTestData();

		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyPropertyComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty property component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the property delegate --->
		<cfset local.property = this.service.getEmptyPropertyVo()>

		<!--- Assert that the property object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.property, "mce.e2.vo.Property"), true, "local.property objectType is not of type mce.e2.vo.Property.")>
			
	</cffunction>

	<cffunction name="getProperties"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all properties from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the property delegate --->
		<cfset local.propertyCollection = this.service.getProperties()>
				
		<!--- Assert that the property array collection is an array --->
		<cfset assertTrue(isArray(local.propertyCollection), "The value returned by this.service.getProperties() was not an array.")>

		<!--- Compare the property properties and excersize retrieving single instances of properties --->
		<cfset comparePropertyProperties(local.propertyCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getProperty"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single property from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the property delegate --->
		<cfset local.propertyCollection = this.service.getProperties()>
				
		<!--- Loop through the property collection, and retrieve each property individually --->
		<cfloop from="1" to="#arraylen(local.propertyCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the property to test --->
			<cfset local.property = local.propertyCollection[local.arrayIndex]>

			<!--- Retrrive a single instance of a property from the service --->
			<cfset local.testProperty = this.service.getProperty(local.property)>

			<!--- Assert that the property array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testProperty, "mce.e2.vo.Property"), true, "local.testProperty objectType is not of type mce.e2.vo.Property.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of property data without specifying primary keys --->
	<cffunction name="saveAndValidateNewPropertiesWithoutPrimaryKeys"
				access="public" returnType="void" output="true"
				hint="This method is used to test / validate the creation of properties using the saveNewProperty() method without specifying a primary key.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of property objects --->
		<cfset local.newProperties = this.delegate.queryToVoArray(this.newPropertiesWithoutPKs, 'mce.e2.vo.Property')/>	
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.newPropertyCollection = arrayNew(1)/>
		
		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newProperties)#" index="local.arrayIndex">	

			<!--- Save the property to the application database --->
			<cfset local.testPropertyPrimaryKey = this.service.saveNewProperty(local.newProperties[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testPropertyPrimaryKey), "The value returned by this.service.saveNewProperty() was not a unique identifier.")/>

			<!--- Build out the primary key and attach it to the property --->
			<cfset local.testProperty = local.newProperties[local.arrayIndex]>
			<cfset local.testProperty.property_uid = local.testPropertyPrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.newPropertyCollection, duplicate(local.testProperty))>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.newPropertyCollection), arrayLen(local.newProperties), "The total number of records processed does not match the total number of records iterated over when creating property records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyProperties(sourceObject=local.newProperties, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()> 			
							
	</cffunction>

	<!--- Test the creation / modification of new / existing property values --->
	<cffunction name="saveAndValidateNewProperties"
				access="public" returnType="void" output="true"
				hint="This method is used to test / validate the creation of properties using the saveNewProperty() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of property objects --->
		<cfset local.newProperties = this.delegate.queryToVoArray(this.newProperties, 'mce.e2.vo.Property')/>	
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.newPropertyCollection = arrayNew(1)/>
		
		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newProperties)#" index="local.arrayIndex">	

			<!--- Save the property to the application database --->
			<cfset local.testPropertyPrimaryKey = this.service.saveNewProperty(local.newProperties[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testPropertyPrimaryKey), "The value returned by this.service.saveNewProperty() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.newPropertyCollection, local.testPropertyPrimaryKey)>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.newPropertyCollection), arrayLen(local.newProperties), "The total number of records processed does not match the total number of records iterated over when creating property records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyProperties(sourceObject=local.newProperties, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewProperties"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of properties using the saveAndReturnNewProperty() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test properties array --->			
		<cfset local.testProperties = arrayNew(1)>			
						
		<!--- Build out the array of property objects --->
		<cfset local.newProperties = this.delegate.queryToVoArray(this.newPropertiesWithoutPks, 'mce.e2.vo.Property')/>
		
		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newProperties)#" index="local.arrayIndex">	

			<!--- Save the property to the application database --->
			<cfset local.testProperty = this.service.saveAndReturnNewProperty(local.newProperties[local.arrayIndex])/>
			
			<!--- Assert that the property object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testProperty, "mce.e2.vo.Property"), true, "local.property objectType is not of type mce.e2.vo.Property.")>
						
			<!--- Append the property to the test array --->			
			<cfset arrayAppend(local.testProperties, local.testProperty)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testProperties), arrayLen(local.newProperties), "The total number of records processed does not match the total number of records iterated over when creating property records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyProperties(sourceObject=local.testProperties, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingProperties"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of properties.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of property objects --->
		<cfset local.newProperties = this.delegate.queryToVoArray(this.newProperties, 'mce.e2.vo.Property')/>
		<cfset local.modifiedProperties = this.delegate.queryToVoArray(this.modifiedProperties, 'mce.e2.vo.Property')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdProperties = arrayNew(1)/>
		<cfset local.modifiedProperties = arrayNew(1)/>
				
		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newProperties)#" index="local.arrayIndex">	

			<!--- Save the property to the application database --->
			<cfset local.primaryKey = this.service.saveNewProperty(local.newProperties[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdProperties, local.primaryKey)>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdProperties), arrayLen(local.newProperties), "The total number of records processed does not match the total number of records iterated over when creating property records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyProperties(sourceObject=local.newProperties, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedProperties)#" index="local.arrayIndex">	

			<!--- Save the property to the application database --->
			<cfset local.primaryKey = this.service.saveExistingProperty(local.modifiedProperties[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset arrayAppend(local.modifiedProperties, local.primaryKey)>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(arrayLen(local.modifiedProperties), arrayLen(local.modifiedProperties), "The total number of records processed does not match the total number of records iterated over when modifying property records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyProperties(sourceObject=local.modifiedProperties, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the property test data --->	
		<cfset purgePropertyTestData()> 			
							
	</cffunction>	

	<cffunction name="purgePropertyTestData"
				access="public"
				hint="This method is used to purge / delete any property data related to the test data being used when property unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="properties" type="query" hint="This argument is used to identify the property data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Create a query to purge data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.Properties_PropertyCollections
			where	property_uid in (
			
						select	property_uid
						from	dbo.properties
						where	friendly_name in (
						
									<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newProperties.friendly_name)#">									
						
								)
			
					)

		</cfquery>
		
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.properties
			where	friendly_name in (
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newProperties.friendly_name)#">									
			
					)
			
		</cfquery>
		
	</cffunction>			

	<!--- Private methods / for internal use only --->
	<cffunction name="createPropertyTestData"
				access="private"
				hint="This method is used to create the test data for testing property records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing property data to be tested with (the query should contain all of the columns of the base table)
			this.newProperties = queryNew('property_uid,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	

			// Set the rowLimit
			this.rowLimit = 5;
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newProperties,this.rowLimit);
		
			// Loop and create the test data
			for( local.rowIndex = 1; local.rowIndex lte this.rowLimit; local.rowIndex = local.rowIndex + 1 ){
			
				// Set the cell values for this instance of a property
				querySetCell(this.newProperties,'property_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.newProperties,'friendly_name', 'PROP 00#local.rowIndex#', local.rowIndex);
				querySetCell(this.newProperties,'is_active', 1, local.rowIndex);
				querySetCell(this.newProperties,'created_by', 'MX Unit (created)', local.rowIndex);
				querySetCell(this.newProperties,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.newProperties,'modified_by', 'MX Unit (created)', local.rowIndex);
				querySetCell(this.newProperties,'modified_date', createOdbcDateTime(now()), local.rowIndex);
							
			}
		
			// Build out the test data set of properties without primary keys
			this.newPropertiesWithoutPks = duplicate(this.newProperties);

			// Loop and create the test data
			for( local.rowIndex = 1; local.rowIndex lte this.rowLimit; local.rowIndex = local.rowIndex + 1 ){
			
				// Set the cell values for this instance of a property
				querySetCell(this.newPropertiesWithoutPks,'property_uid', '', local.rowIndex);
							
			}
					
			// Duplicate the new properties to create the baseline modified properties
			this.modifiedProperties = duplicate(this.newProperties);

			// Loop and create the test data
			for( local.rowIndex = 1; local.rowIndex lte this.rowLimit; local.rowIndex = local.rowIndex + 1 ){
			
				// Set the cell values for this instance of a property
				querySetCell(this.modifiedProperties,'friendly_name', 'MODPROP 00#local.rowIndex#', local.rowIndex);
				querySetCell(this.modifiedProperties,'modified_by', 'MX Unit (modified)', local.rowIndex);
							
			}
				
		</cfscript>	
				
	</cffunction>

	<!--- ToDo: [ADL] This method (createPropertyInputValueTestData) must be tailored
				to create a valid propertyInputValue test data set.  This is being put on
				hold due to the nature / relationships involved with the data. --->
	
	<cffunction name="comparePropertyProperties"
				access="private"
				hint="This method is used to compare the properties for a given property vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of property vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the property from the application database --->
				<cfset local.retrievedProperty = this.delegate.getPropertyAsComponent(arguments.sourceObject[local.arrayIndex].property_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the property from the application database --->
				<cfset local.retrievedProperty = this.service.getProperty(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared rateFactor VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedProperty, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved rateFactor VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedProperty[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedProperty[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>	

</cfcomponent>