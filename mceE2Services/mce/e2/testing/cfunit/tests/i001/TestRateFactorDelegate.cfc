<cfcomponent name="Test Rate Factor Delegate"
			 extends="baseRateFactorTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the rateFactorDelegate component.">

	<!--- Kick off the constructor; only execute the function if the beanFactory exists --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
				
			// Get an instance of the rate factor delegate
			this.delegate = request.beanFactory.getBean("RateFactorDelegate");
			
			// Setup the test data for the unit tests
			setupTestData();
		
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyRateFactorComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty rate factor component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the rate factor delegate --->
		<cfset local.rateFactor = this.delegate.getEmptyRateFactorComponent()>

		<!--- Assert that the rateFactor object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.rateFactor, "mce.e2.vo.RateFactor"), true, "local.rateFactor objectType is not of type mce.e2.vo.RateFactor.")>
			
	</cffunction>

	<cffunction name="getEmptyRateFactorValueComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty rate factor value component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the rate factor delegate --->
		<cfset local.rateFactor = this.delegate.getEmptyRateFactorValueComponent()>

		<!--- Assert that the rateFactor object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.rateFactor, "mce.e2.vo.RateFactorValue"), true, "local.rateFactor objectType is not of type mce.e2.vo.RateFactorValue.")>
			
	</cffunction>

	<!--- Test Retrieval of collections of value objects --->
	<cffunction name="getRateFactorsAsArrayOfComponents"
				access="public" returntype="void"
				hint="This method is used to test retrieving a collection of rateFactors from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Build out the array of rate factor value objects --->
		<cfset local.newRateFactors = this.delegate.queryToVoArray(this.newRateFactors, 'mce.e2.vo.RateFactor')/>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
			
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateFactors)#" index="local.arrayIndex">	

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveNewRateFactor(local.newRateFactors[local.arrayIndex])/>
			
		</cfloop>

		<!--- Step 2: Retreive the rateFactor data --->
		<cfset local.rateFactorCollection = this.delegate.getRateFactorsAsArrayOfComponents()>
		
		<!--- Step 3: Validate that the rateFactorCollection is an array --->
		<cfset assertTrue(isArray(local.rateFactorCollection), "The value returned by getRateFactorsAsArrayOfComponents() was not an array.")/>
		<cfset assertTrue(arrayLen(local.rateFactorCollection) gte arrayLen(local.newRateFactors), "The length of the array returned by getRateFactorsAsArrayOfComponents() was less than the total number of records created during this test.")/>
				
		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
								
	</cffunction>	
		
	<cffunction name="getRateFactorValuesAsArrayOfComponents"
				access="public" returntype="void"
				hint="This method is used to test retrieving a collection of rate factor values from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		<cfset local.createdCount = 0/>
		
		<!--- Build out the array of rate factor value objects --->
		<cfset local.newRateFactors = this.delegate.queryToVoArray(this.newRateFactors, 'mce.e2.vo.RateFactor')/>
		<cfset local.newRateFactorValues = this.delegate.queryToVoArray(this.newRateFactorValues, 'mce.e2.vo.RateFactorValue')/>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
			
		<!--- Step 1:  Create the data --->
		<cfset this.delegate.saveNewRateFactor(local.newRateFactors[1])/>

		<!--- Step 2:  Create the rate factor value data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateFactorValues)#" index="local.arrayIndex">	

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveNewRateFactorValue(local.newRateFactorValues[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Step 3:  Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newRateFactorValues), "The total number of records processed does not match the total number of records iterated over when creating rate factor value records.")/>
		
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateFactorValueProperties(sourceObject=local.newRateFactorValues, compareAction="create")>

		<!--- Step 2: Retreive the rateFactor data --->
		<cfset local.rateFactorCollection = this.delegate.getRateFactorValuesAsArrayOfComponents(rate_factor_uid=local.newRateFactors[1].rate_factor_uid)>
		
		<!--- Step 3: Validate that the rateFactorCollection is an array --->
		<cfset assertTrue(isArray(local.rateFactorCollection), "The value returned by getRateFactorsAsArrayOfComponents() was not an array.")/>
		<cfset assertTrue(arrayLen(local.rateFactorCollection) gte arrayLen(local.newRateFactors), "The length of the array returned by getRateFactorsAsArrayOfComponents() was less than the total number of records created during this test.")/>
				
		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
								
	</cffunction>				

	<!--- Test data creation / modification / retrieval actions involving the application database --->	
	<cffunction name="saveAndValidateNewRateFactors"
				access="public" returntype="void"
				hint="This method is used to create new rate factors in the application database, save them, and then validate that they were saved using the component-defined rate factor data.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Build out the array of rate factor value objects --->
		<cfset local.newRateFactors = this.delegate.queryToVoArray(this.newRateFactors, 'mce.e2.vo.RateFactor')/>
	
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
	
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateFactors)#" index="local.arrayIndex">	

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveNewRateFactor(local.newRateFactors[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newRateFactors), "The total number of records processed does not match the total number of records iterated over when creating rate factor records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateFactorProperties(sourceObject=local.newRateFactors, compareAction="create")>
								
		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>

	</cffunction>

	<cffunction name="saveAndValidateExistingRateFactors"
				access="public" returntype="void"
				hint="This method is used to create new rate factors in the application database, save them, modify them, and then validate that they were modified.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Build out the array of rate factor value objects --->
		<cfset local.newRateFactors = this.delegate.queryToVoArray(this.newRateFactors, 'mce.e2.vo.RateFactor')/>
		<cfset local.modifiedRateFactors = this.delegate.queryToVoArray(this.modifiedRateFactors, 'mce.e2.vo.RateFactor')/>
	
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
	
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateFactors)#" index="local.arrayIndex">	

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveNewRateFactor(local.newRateFactors[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newRateFactors), "The total number of records processed does not match the total number of records iterated over when creating rate factor records.")/>
				
		<!--- Step 2:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedRateFactors)#" index="local.arrayIndex">	

			<!--- Set the modified date for each rate factor record being processed --->
			<cfset local.modifiedRateFactors[local.arrayIndex]["modified_date"] = createOdbcDateTime(now())/>

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveExistingRateFactor(local.modifiedRateFactors[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>

		<!--- Step 3:  Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedRateFactors), "The total number of records processed does not match the total number of records iterated over when modifying rate factor records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateFactorProperties(sourceObject=local.modifiedRateFactors, compareAction="modify")>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>

	</cffunction>

	<cffunction name="saveAndValidateNewRateFactorValues"
				access="public" returntype="void"
				hint="This method is used to create a new rate factor in the application database and then create an additional set of associated rate factor value records.  These records will then be validated and confirmed that they were created successfully.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Build out the array of rate factor value objects --->
		<cfset local.newRateFactors = this.delegate.queryToVoArray(this.newRateFactors, 'mce.e2.vo.RateFactor')/>
		<cfset local.newRateFactorValues = this.delegate.queryToVoArray(this.newRateFactorValues, 'mce.e2.vo.RateFactorValue')/>
	
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
	
		<!--- Step 1:  Create the data --->
		<cfset this.delegate.saveNewRateFactor(local.newRateFactors[1])/>
																		
		<!--- Step 2:  Create the rate factor value data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateFactorValues)#" index="local.arrayIndex">	

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveNewRateFactorValue(local.newRateFactorValues[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Step 3:  Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newRateFactorValues), "The total number of records processed does not match the total number of records iterated over when creating rate factor value records.")/>
		
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateFactorValueProperties(sourceObject=local.newRateFactorValues, compareAction="create")>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>

	</cffunction>

	<cffunction name="saveAndValidateExistingRateFactorValues"
				access="public" returntype="void"
				hint="This method is used to create a new rate factor in the application database and then create an additional set of associated rate factor value records.  The rate factor value records will then be modified, and validated as such.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Build out the array of rate factor value objects --->
		<cfset local.newRateFactors = this.delegate.queryToVoArray(this.newRateFactors, 'mce.e2.vo.RateFactor')/>
		<cfset local.newRateFactorValues = this.delegate.queryToVoArray(this.newRateFactorValues, 'mce.e2.vo.RateFactorValue')/>
		<cfset local.modifiedRateFactorValues = this.delegate.queryToVoArray(this.modifiedRateFactorValues, 'mce.e2.vo.RateFactorValue')/>		
	
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>
	
		<!--- Step 1:  Create the data --->
		<cfset this.delegate.saveNewRateFactor(local.newRateFactors[1])/>
																		
		<!--- Step 2:  Create the rate factor value data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateFactorValues)#" index="local.arrayIndex">	

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveNewRateFactorValue(local.newRateFactorValues[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Step 3:  Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newRateFactorValues), "The total number of records processed does not match the total number of records iterated over when creating rate factor value records.")/>
		
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateFactorValueProperties(sourceObject=local.newRateFactorValues, compareAction="create")>

		<!--- Step 5:  Modify the rate factor value data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedRateFactorValues)#" index="local.arrayIndex">	

			<!--- Save the rate factor to the application database --->
			<cfset this.delegate.saveExistingRateFactorValue(local.modifiedRateFactorValues[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Step 6:  Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedRateFactorValues), "The total number of records processed does not match the total number of records iterated over when modifying rate factor value records.")/>
		
		<!--- Step 7:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateFactorValueProperties(sourceObject=local.modifiedRateFactorValues, compareAction="modify")>		

		<!--- Clean the test data from the application database --->
		<cfset purgeRateFactorTestData()>

	</cffunction>
						
</cfcomponent>