<cfcomponent extends="mxunit.framework.TestCase" output="true">

	<cffunction name="getEmptyEnergyUsageRecordAsComponent" output="true">
		<cfset var delegate = request.beanFactory.getBean("EnergyEntryDelegate")>
		<cfset var priorPeriods = [{period_start='3/18/2008',period_end='4/16/2008'}]>
		<cfset var entryInfo = delegate.getEmptyEnergyUsageRecordAsComponent(
				energy_account_uid="a351f597-fd71-48ae-81e1-dc8ce76f3a3b",
				period_start="04/16/2008",
				period_end="5/15/2008",
				priorPeriods=priorPeriods)>

		<cfset assertEquals(entryInfo.propertyFriendlyName, "1600 Harvester Rd ##1 West",
			"Returned data did not include the expected property name.")>
	</cffunction>

	<cffunction name="getEnergyUsageRecordAsComponent" output="true">
		<cfset var delegate = request.beanFactory.getBean("EnergyEntryDelegate")>
		<cfset var entryInfo = delegate.getEnergyUsageRecordAsComponent(
				energy_usage_uid="10AEE937-EDC8-4E3F-9B20-005C7901C93C")>

		<cfset assertEquals(entryInfo.propertyFriendlyName, "1600 Harvester Rd ##1 West",
			"Returned data did not include the expected property name.")>
	</cffunction>

	<cffunction name="getEnergyUsageRecord" output="true">
		<cfset var delegate = request.beanFactory.getBean("EnergyEntryDelegate")>
		<cfset var q = delegate.getEnergyUsageRecord("10AEE937-EDC8-4E3F-9B20-005C7901C93C")>

		<cfset assertEquals(q.recordCount, 1,
			"Returned data should have exactly one row.")>
		<cfset assertEquals(q.propertyFriendlyName, "1600 Harvester Rd ##1 West",
			"Returned data did not include the expected property name.")>
	</cffunction>

</cfcomponent>