<cfcomponent name="Base Rate Factor Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the rateFactorDelegate and rateFactorService unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeRateFactorTestData"
				access="private"
				hint="This method is used to purge / delete any ratefactor data related to the test data being used when rate factor unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="rateFactors" type="query" hint="This argument is used to identify the rate factor data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Create a query to purge rate factor value data --->
		<cfquery name="local.purgeRateFactorValuesData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.rateFactorValues
			where	rate_factor_uid in ( 
		
						<!--- Create a value list of the factor lookup codes that are in use by this component --->	
						select	rate_factor_uid
						from	dbo.rateFactors
						where	factor_lookup_code in (
						
							<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newRateFactors.factor_lookup_code)#">
			
						)
			
					)
		
		</cfquery>
				
		<!--- Create a query to purge rate factor data --->
		<cfquery name="local.purgeRateFactorValuesData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.rateFactors
			where	factor_lookup_code in ( 
		
						<!--- Create a value list of the factor lookup codes that are in use by this component --->	
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newRateFactors.factor_lookup_code)#">
			
					)
		
		</cfquery>
				
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the rateFactor service and delegate tests.">
		
		<cfscript>
		
			//Build out the rate factor test data (for create / modify scenarios)
			createRateFactorTestData();
			createRateFactorValueTestData();
			
			// Purge any related rate factor data
			purgeRateFactorTestData();
		
		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createRateFactorTestData"
				access="private"
				hint="This method is used to create the rate factor test data for testing rate factor records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing rate factor data to be tested with (the query should contain all of the columns of the base table)
			this.newRateFactors = queryNew('rate_factor_uid,factor_lookup_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newRateFactors,4);
		
			// Set the cell values for this instance of a rate factor
			querySetCell(this.newRateFactors,'rate_factor_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newRateFactors,'factor_lookup_code', 'rf_001', 1);
			querySetCell(this.newRateFactors,'friendly_name', 'RF 001', 1);
			querySetCell(this.newRateFactors,'is_active', 1, 1);
			querySetCell(this.newRateFactors,'created_by', '[Unidentified User]', 1);
			querySetCell(this.newRateFactors,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newRateFactors,'modified_by', '[Unidentified User]', 1);
			querySetCell(this.newRateFactors,'modified_date', createOdbcDateTime(now()), 1);
					
			// Set the cell values for this instance of a rate factor
			querySetCell(this.newRateFactors,'rate_factor_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newRateFactors,'factor_lookup_code', 'rf_002', 2);
			querySetCell(this.newRateFactors,'friendly_name', 'RF 002', 2);
			querySetCell(this.newRateFactors,'is_active', 1, 2);
			querySetCell(this.newRateFactors,'created_by', '[Unidentified User]', 2);
			querySetCell(this.newRateFactors,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newRateFactors,'modified_by', '[Unidentified User]', 2);
			querySetCell(this.newRateFactors,'modified_date', createOdbcDateTime(now()), 2);
		
			// Set the cell values for this instance of a rate factor
			querySetCell(this.newRateFactors,'rate_factor_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newRateFactors,'factor_lookup_code', 'rf_003', 3);
			querySetCell(this.newRateFactors,'friendly_name', 'RF 003', 3);
			querySetCell(this.newRateFactors,'is_active', 1, 3);
			querySetCell(this.newRateFactors,'created_by', '[Unidentified User]', 3);
			querySetCell(this.newRateFactors,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newRateFactors,'modified_by', '[Unidentified User]', 3);
			querySetCell(this.newRateFactors,'modified_date', createOdbcDateTime(now()), 3);
		
			// Set the cell values for this instance of a rate factor
			querySetCell(this.newRateFactors,'rate_factor_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newRateFactors,'factor_lookup_code', 'rf_004', 4);
			querySetCell(this.newRateFactors,'friendly_name', 'RF 004', 4);
			querySetCell(this.newRateFactors,'is_active', 1, 4);
			querySetCell(this.newRateFactors,'created_by', '[Unidentified User]', 4);
			querySetCell(this.newRateFactors,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newRateFactors,'modified_by', '[Unidentified User]', 4);
			querySetCell(this.newRateFactors,'modified_date', createOdbcDateTime(now()), 4);

			// Duplicate the new rate factors and remove the primary keys
			this.newRateFactorsWithoutPks = duplicate(this.newRateFactors);
			
			// Update the friendly names for the modified rate factors
			querySetCell(this.newRateFactorsWithoutPks,'rate_factor_uid', '', 1);
			querySetCell(this.newRateFactorsWithoutPks,'rate_factor_uid', '', 2);
			querySetCell(this.newRateFactorsWithoutPks,'rate_factor_uid', '', 3);			
			querySetCell(this.newRateFactorsWithoutPks,'rate_factor_uid', '', 4);			

			// Duplicate the new rate factors to create the baseline modified rate factors
			this.modifiedRateFactors = duplicate(this.newRateFactors);

			// Update the friendly names for the modified rate factors
			querySetCell(this.modifiedRateFactors,'friendly_name', 'RFM 001', 1);
			querySetCell(this.modifiedRateFactors,'friendly_name', 'RFM 002', 2);
			querySetCell(this.modifiedRateFactors,'friendly_name', 'RFM 003', 3);			
			querySetCell(this.modifiedRateFactors,'friendly_name', 'RFM 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedRateFactors,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedRateFactors,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedRateFactors,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedRateFactors,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>
	
	<cffunction name="createRateFactorValueTestData"
				access="private"
				hint="This method is used to create the rate factor value test data for testing rate factor value records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing rate factor data to be tested with (the query should contain all of the columns of the base table)
			this.newRateFactorValues = queryNew('rate_factor_uid,factor_value_uid,factor_value,relationship_start,relationship_end,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newRateFactorValues,4);
				
			// Set the cell values for this instance of a rate factor value
			querySetCell(this.newRateFactorValues,'rate_factor_uid', this.newRateFactors.rate_factor_uid[1], 1);
			querySetCell(this.newRateFactorValues,'factor_value_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newRateFactorValues,'factor_value', 1, 1);
			querySetCell(this.newRateFactorValues,'relationship_start', '2009-01-01 12:00:00', 1);
			querySetCell(this.newRateFactorValues,'relationship_end', '2009-01-31 12:00:00', 1);		
			querySetCell(this.newRateFactorValues,'is_active', 1, 1);
			querySetCell(this.newRateFactorValues,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newRateFactorValues,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newRateFactorValues,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newRateFactorValues,'modified_date', createOdbcDateTime(now()), 1);

			// Set the cell values for this instance of a rate factor value
			querySetCell(this.newRateFactorValues,'rate_factor_uid', this.newRateFactors.rate_factor_uid[1], 2);
			querySetCell(this.newRateFactorValues,'factor_value_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newRateFactorValues,'factor_value', 2, 2);
			querySetCell(this.newRateFactorValues,'relationship_start', '2009-02-01 12:00:00', 2);
			querySetCell(this.newRateFactorValues,'relationship_end', '2009-02-28 12:00:00', 2);		
			querySetCell(this.newRateFactorValues,'is_active', 1, 2);
			querySetCell(this.newRateFactorValues,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newRateFactorValues,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newRateFactorValues,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newRateFactorValues,'modified_date', createOdbcDateTime(now()), 2);

			// Set the cell values for this instance of a rate factor value
			querySetCell(this.newRateFactorValues,'rate_factor_uid', this.newRateFactors.rate_factor_uid[1], 3);
			querySetCell(this.newRateFactorValues,'factor_value_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newRateFactorValues,'factor_value', 3, 3);
			querySetCell(this.newRateFactorValues,'relationship_start', '2009-03-01 12:00:00', 3);
			querySetCell(this.newRateFactorValues,'relationship_end', '2009-03-31 12:00:00', 3);		
			querySetCell(this.newRateFactorValues,'is_active', 1, 3);
			querySetCell(this.newRateFactorValues,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newRateFactorValues,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newRateFactorValues,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newRateFactorValues,'modified_date', createOdbcDateTime(now()), 3);

			// Set the cell values for this instance of a rate factor value
			querySetCell(this.newRateFactorValues,'rate_factor_uid', this.newRateFactors.rate_factor_uid[1], 4);
			querySetCell(this.newRateFactorValues,'factor_value_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newRateFactorValues,'factor_value', 4, 4);
			querySetCell(this.newRateFactorValues,'relationship_start', '2009-04-01 12:00:00', 4);
			querySetCell(this.newRateFactorValues,'relationship_end', '2009-04-30 12:00:00', 4);		
			querySetCell(this.newRateFactorValues,'is_active', 1, 4);
			querySetCell(this.newRateFactorValues,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newRateFactorValues,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newRateFactorValues,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newRateFactorValues,'modified_date', createOdbcDateTime(now()), 4);
	
			// Duplicate the new rate factor values to create the baseline modified rate factor values
			this.modifiedRateFactorValues = duplicate(this.newRateFactorValues);

			// Update the friendly names for the modified rate factors
			querySetCell(this.modifiedRateFactorValues,'factor_value', 2, 1);
			querySetCell(this.modifiedRateFactorValues,'factor_value', 4, 2);
			querySetCell(this.modifiedRateFactorValues,'factor_value', 6, 3);			
			querySetCell(this.modifiedRateFactorValues,'factor_value', 6, 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedRateFactorValues,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedRateFactorValues,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedRateFactorValues,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedRateFactors,'modified_by', 'MX Unit (modified)', 4);	
	
		</cfscript>
				
	</cffunction>	

	<cffunction name="compareRateFactorProperties"
				access="private"
				hint="This method is used to compare the properties for a given rate factor vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of rate factor vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the rate factor from the application database --->
				<cfset local.retrievedRateFactor = this.delegate.getRateFactorAsComponent(arguments.sourceObject[local.arrayIndex].rate_factor_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the rate factor from the application database --->
				<cfset local.retrievedRateFactor = this.service.getRateFactor(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared rateFactor VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedRateFactor, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved rateFactor VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedRateFactor[local.thisKey])), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedRateFactor[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
	<cffunction name="compareRateFactorValueProperties"
				access="private"
				hint="This method is used to compare the properties for a given rate factor value vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of rate factor vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the rate factor from the application database --->
				<cfset 	local.retrievedRateFactorValue = this.delegate.getRateFactorValueAsComponent(
						rate_factor_uid=arguments.sourceObject[local.arrayIndex].rate_factor_uid,
						factor_value_uid=arguments.sourceObject[local.arrayIndex].factor_value_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the rate factor from the application database --->
				<cfset 	local.retrievedRateFactorValue = this.service.getRateFactorValue(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>
			
			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">
		
				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>		
		
					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared rateFactorValue VO's [#arguments.compareAction#].">
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedRateFactorValue, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved rateFactorValue VO component.")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date' or local.thisKey eq 'relationship_start' or local.thisKey eq 'relationship_end'>
					
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedRateFactorValue[local.thisKey])), trim(createOdbcDateTime(arguments.sourceObject[local.arrayIndex][local.thisKey])), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedRateFactorValue[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>