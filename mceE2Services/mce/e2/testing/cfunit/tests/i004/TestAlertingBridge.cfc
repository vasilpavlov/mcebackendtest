<cfcomponent name="Test Alerting Remote Services"
			 extends="mxunit.framework.TestCase"
			 output="true"
			 hint="This component is used to test the methods related to AlertingService for the UI (not the alert sending framework).">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>



	<cffunction name="init" access="private">
		<cfset this.AlertingBridge = request.beanFactory.getBean("AlertingBridge")>
	</cffunction>


	<cffunction name="checkAlertRelevancy">
		<cfset var alertUids = "EFD2A1DB-E443-62B9-D204-1CAE1CFC5205">
		<cfset var noLongerRelevant = this.AlertingBridge.checkRelevancy(alertUids)>
	</cffunction>


</cfcomponent>