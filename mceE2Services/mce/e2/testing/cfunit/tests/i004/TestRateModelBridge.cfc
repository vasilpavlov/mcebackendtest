<cfcomponent name="Test Alerting Remote Services"
			 extends="mxunit.framework.TestCase"
			 output="true"
			 hint="This component is used to test the methods related to AlertingService for the UI (not the alert sending framework).">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>



	<cffunction name="init" access="private">

		<cfset var accountName = "494202305000015 (Con Edison)">
		<cfset var periodStart = "1/30/2009">

		<cfset var utils = CreateObject("component", "mceRateModelEngine.testbed.RateModelTestUtils")>
		<cfset var accountUid = utils.getEnergyAccountUid(accountName)>

		<cfset this.RateModelBridge = request.beanFactory.getBean("RateModelBridge")>
		<cfset this.EnergyEntryService = request.beanFactory.getBean("EnergyEntryService")>

		<cfset this.energyUsageUid = utils.getUsageRecords(accountUid, periodStart).energy_usage_uid>
	</cffunction>


	<cffunction name="getSession" access="private"
				output="false" returnType="any"
				hint="This method is used to test the retrieval of a session.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('mgalle','ets')>

		<!--- Retrieve the session --->
		<cfset local.result = local.securityService.getSession()>

		<cfreturn local.result>
	</cffunction>


	<cffunction name="executeRateModelForEnergyEntryInfo">
		<cfset var session = getSession()>

		<cfset var entryInfo = this.EnergyEntryService.getEnergyUsageVo(energy_usage_uid=this.energyUsageUid)>
		<cfset var rateModelResponse = this.RateModelBridge.executeRateModelForEnergyEntryInfo(entryInfo)>

		<cfdump var="#rateModelResponse#">
	</cffunction>


</cfcomponent>