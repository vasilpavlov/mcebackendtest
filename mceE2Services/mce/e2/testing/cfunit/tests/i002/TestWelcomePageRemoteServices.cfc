<cfcomponent name="Test Welcome Page Remote Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to the welcome page.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getPropertyCollectionPropertyAlertAndToDoCounts" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property collection roll-up / summary data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("propertyCollectionService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.user')>
		<cfset local.obj.user_uid = 'EEFAA50B-93DE-DD11-B9FC-001A6B6DA3A2'>	
		<cfset local.result = local.service.getPropertyCollectionPropertyAlertAndToDoCounts(local.obj)>	
					
	</cffunction>

	<cffunction name="getPropertyAlertAndToDoCounts" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property roll-up / summary data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("propertyCollectionService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.propertyCollection')>
		<cfset local.obj.property_collection_uid = '81EE1964-0215-DE11-9950-0019D2AF28A8'>
		
		<!--- Build out the property collection array --->
		<cfset local.pcArray = arrayNew(1)>
		<cfset arrayAppend(local.pcArray, local.obj)>
		
		<!--- Get / retrieve the property alert and toDo counts --->		
		<cfset local.result = local.service.getPropertyAlertAndToDoCounts(local.pcArray)>	
			
	</cffunction>

	<cffunction name="getCurrentUserCompanyRoleInformation" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user information.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("SecurityService")>
		<cfset local.userService = request.beanFactory.getBean("UserService")>
		
		<!--- Attempt the login / create a valid session --->
		<cfset local.service.attemptLogin("nate", "nate")>
		
		<!--- Get / retrieve the property alert and toDo counts --->		
		<cfset local.result = local.userService.getCurrentUserCompanyRoleInformation()>	
			
	</cffunction>

	<cffunction name="getCurrentUserCompanyContacts" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of client contacts associated to the company of a given user.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
				
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("SecurityService")>
		<cfset local.userService = request.beanFactory.getBean("UserService")>
		
		<!--- Attempt the login / create a valid session --->
		<cfset local.service.attemptLogin("nate", "nate")>
		
		<!--- Get / retrieve the property alert and toDo counts --->		
		<cfset local.result = local.userService.getCurrentUserCompanyContacts()>	
				
	</cffunction>

	<cffunction name="getUserCompanyRoleInformation" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user information.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("UserService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.User')>
		<cfset local.obj.user_uid = 'EEFAA50B-93DE-DD11-B9FC-001A6B6DA3A2'>
		
		<!--- Get / retrieve the property alert and toDo counts --->		
		<cfset local.result = local.service.getUserCompanyRoleInformation(local.obj)>
								
	</cffunction>

	<cffunction name="getUserCompanyContacts" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of client contacts associated to the company of a given user.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("UserService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.User')>
		<cfset local.obj.user_uid = 'EEFAA50B-93DE-DD11-B9FC-001A6B6DA3A2'>
			
		<!--- Get / retrieve the property alert and toDo counts --->		
		<cfset local.result = local.service.getUserCompanyContacts(local.obj)>	
			
	</cffunction>

	<cffunction name="getEnergyUsageDateRange"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of energy account / energy usage date ranges.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("EnergyUsageService")>
		<cfset local.obj = createObject('component','mce.e2.vo.EnergyAccount')>
		<cfset local.obj.energy_account_uid = 'BE052EC4-1F0F-4E54-92AE-00240E7EE443'>
		
		<!--- Initialize the array --->
		<cfset local.eaArray = arrayNew(1)>
		<cfset arrayAppend(local.eaArray,local.obj)>
		
		<!--- Retrieve the date range properties --->
		<cfset local.result = local.service.getEnergyUsageDateRange(local.eaArray)>	
		
	</cffunction>

	<cffunction name="getAlerts"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of alerts associated to a collection of properties.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("PropertyService")>
		<cfset local.obj = createObject('component','mce.e2.vo.Property')>
		<cfset local.obj.property_uid = ''>
		
		<!--- Initialize the array --->
		<cfset local.eaArray = arrayNew(1)>
		<cfset arrayAppend(local.obj)>
		
		<!--- Retrieve the date range properties --->
		<cfset local.result = local.service.getPropertyAlerts(
				propertyArray=local.eaArray)>				
				
	</cffunction>

</cfcomponent>