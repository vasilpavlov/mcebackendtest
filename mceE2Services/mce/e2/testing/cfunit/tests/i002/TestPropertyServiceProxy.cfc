<cfcomponent name="Test Property Service Proxy Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to property services.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getPropertyAddresses" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property address data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("propertyService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
		<cfset local.obj.property_uid = 'BB45C5C2-A1CB-414F-AA50-007901A01264'>	
		<cfset local.result = local.service.getPropertyAddresses(local.obj)>	
			
	</cffunction>

	<cffunction name="getPropertyMetaData" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property meta data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("propertyService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
		<cfset local.obj.property_uid = 'BB45C5C2-A1CB-414F-AA50-007901A01264'>	
		<cfset local.result = local.service.getPropertyMetaData(local.obj)>	
			
	</cffunction>

	<cffunction name="getClientCompaniesByProperty" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of client companies by property.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("clientCompanyService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>	
		<cfset local.result = local.service.getClientCompaniesByProperty(local.obj)>	
			
	</cffunction>

	<cffunction name="getPropertyClientCompanyAssociations" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property / client company associations.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("propertyService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>	
		<cfset local.result = local.service.getPropertyClientCompanyAssociations(local.obj)>	

	</cffunction>

	<cffunction name="getClientContactsByProperty" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of client contacts by property.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("ClientContactService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>	
		<cfset local.result = local.service.getClientContactsByProperty(local.obj)>		
			
	</cffunction>

	<cffunction name="getPropertyClientContactAssociations" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property / client contact associations.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("propertyService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>	
		<cfset local.result = local.service.getPropertyClientContactAssociations(local.obj)>	

	</cffunction>


	<cffunction name="getClientCompaniesAndContactsByProperty" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of client companies and contacts by property.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("ClientCompanyService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>	
		<cfset local.result = local.service.getClientCompaniesAndContactsByProperty(local.obj)>		
			
	</cffunction>

	<cffunction name="getAvailableCompanyRoles" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all available company roles.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("ClientCompanyService")>
		<cfset local.result = local.service.getAvailableCompanyRoles()>		
			
	</cffunction>

	<cffunction name="getAvailableClientContactRoles" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all available client contact roles.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("ClientContactService")>
		<cfset local.result = local.service.getAvailableClientContactRoles()>		

	</cffunction>

	<cffunction name="getAddressTypes" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all available address types.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("AddressTypeService")>
		<cfset local.result = local.service.getAddressTypes()>	
			
	</cffunction>

	<cffunction name="getClientCompanies" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all available client companies.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("ClientCompanyService")>
		<cfset local.result = local.service.getClientCompanies()>	
			
	</cffunction>

	<cffunction name="getPropertyMetaTypes" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all available property meta types.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("PropertyMetaTypeService")>
		<cfset local.result = local.service.getPropertyMetaTypes()>	
			
	</cffunction>

	<cffunction name="saveAndReturnNewPropertyMeta" 
				output="false" returnType="void"
				hint="This method is used to test creating a new property meta value.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Create the service --->
		<cfset local.service = request.beanFactory.getBean("PropertyMetaService")>
		<cfset local.obj = local.service.getEmptyPropertyMetaVo()>
		
		<!--- Set the object properties --->
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.obj.meta_type_uid = '9B1AFE3D-EB72-4024-A7D3-0BF3E3C35F0B'>
		<cfset local.obj.meta_value = 'Abraham001'>
		<cfset local.obj.relationship_start = '2009-01-01 00:00:00'>
		
		<!--- Save the object --->
		<cfset local.newObj = local.service.saveAndReturnNewPropertyMeta(local.obj)>
		
	</cffunction>

	<cffunction name="saveExistingPropertyMeta" 
				output="false" returnType="void"
				hint="This method is used to test saving an existing property meta value.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Create the service --->
		<cfset local.service = request.beanFactory.getBean("PropertyMetaService")>
		<cfset local.obj = local.service.getEmptyPropertyMetaVo()>
		
		<!--- Set the object properties --->
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.obj.meta_type_uid = '9B1AFE3D-EB72-4024-A7D3-0BF3E3C35F0B'>
		<cfset local.obj.meta_value = 'Abraham998'>
		<cfset local.obj.relationship_start = '2009-01-01 00:00:00'>
		
		<!--- Save the object --->
		<cfset local.newObj = local.service.saveAndReturnNewPropertyMeta(local.obj)>
		
		<!--- Modify the meta value --->
		<cfset local.newObj.meta_value = 'Abraham999'>
		
		<!--- Modify the object --->
		<cfset local.modifiedObj = local.service.saveExistingPropertyMeta(local.newObj)>
		
	</cffunction>

	<cffunction name="saveAndReturnNewPropertyAddress" 
				output="false" returnType="void"
				hint="This method is used to test creating a new property address.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Create the service --->
		<cfset local.service = request.beanFactory.getBean("PropertyService")>
		<cfset local.obj = local.service.getEmptyPropertyAddressVo()>
		
		<!--- Set the object properties --->
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.obj.address_type_code = '21E383EE-D99F-446D-9822-97A6187E559E'>
		<cfset local.obj.relationship_start = '2009-01-01 00:00:00'>
		<cfset local.obj.address_line_1 = '23 76th Street'>
		<cfset local.obj.address_line_2 = ''>
		<cfset local.obj.city = 'North Bergen'>
		<cfset local.obj.state = 'Nj'>
		<cfset local.obj.country = 'USA'>
		<cfset local.obj.postal_code = '07047'>
		
		<!--- Save the object --->
		<cfset local.newObj = local.service.saveAndReturnNewPropertyAddress(local.obj)>

	</cffunction>

	<cffunction name="saveExistingPropertyAddress" 
				output="false" returnType="void"
				hint="This method is used to test saving an existing property address.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Create the service --->
		<cfset local.service = request.beanFactory.getBean("PropertyService")>
		<cfset local.obj = local.service.getEmptyPropertyAddressVo()>
		
		<!--- Set the object properties --->
		<cfset local.obj.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.obj.address_type_code = '21E383EE-D99F-446D-9822-97A6187E559E'>
		<cfset local.obj.relationship_start = '2009-01-01 00:00:00'>
		<cfset local.obj.address_line_1 = '23 76th Street'>
		<cfset local.obj.address_line_2 = ''>
		<cfset local.obj.city = 'North Bergen'>
		<cfset local.obj.state = 'Nj'>
		<cfset local.obj.country = 'USA'>
		<cfset local.obj.postal_code = '07047'>
		
		<!--- Save the object --->
		<cfset local.newObj = local.service.saveAndReturnNewPropertyAddress(local.obj)>
		
		<!--- Modify the address properties --->
		<cfset local.newObj.address_line_2 = '76th Street'>		
		<cfset local.newObj.address_line_2 = 'Apt. 23'>
		
		<!--- Modify the object --->
		<cfset local.modifiedObj = local.service.saveExistingPropertyAddress(local.newObj)>
		
	</cffunction>

	<cffunction name="saveAndReturnNewPropertyWithCompanyAddressAssociation" 
				output="false" returnType="void"
				hint="This method is used to test saving a property, property / company association, and property address.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Create the necessary services --->
		<cfset local.propertyService = request.beanFactory.getBean("PropertyService")>
		<cfset local.clientCompanyService = request.beanFactory.getBean("ClientCompanyService")>
		
		<!--- Get the vo's for each object --->
		<cfset local.property = local.propertyService.getEmptyPropertyVo()>
		<cfset local.propertyAddress = local.propertyService.getEmptyPropertyAddressVo()>
		<cfset local.clientCompany = local.clientCompanyService.getEmptyClientCompanyVo()>
		<cfset local.companyRole = local.clientCompanyService.getEmptyCompanyRoleVo()>
		
		<!--- Configure the property --->
		<cfset local.property.friendly_name = 'Test 001'>
		
		<!--- Configure the property address --->
		<cfset local.propertyAddress.address_type_code = '21E383EE-D99F-446D-9822-97A6187E559E'>
		<cfset local.propertyAddress.relationship_start = '2009-01-01 00:00:00'>
		<cfset local.propertyAddress.address_line_1 = '23 76th Street'>
		<cfset local.propertyAddress.address_line_2 = ''>
		<cfset local.propertyAddress.city = 'North Bergen'>
		<cfset local.propertyAddress.state = 'Nj'>
		<cfset local.propertyAddress.country = 'USA'>
		<cfset local.propertyAddress.postal_code = '07047'>
		
		<!-- Set the company and company role --->
		<cfset local.clientCompany.client_company_uid = 'CAEAD04C-5807-DE11-BBF3-001279657128'>
		<cfset local.companyRole.company_role_uid = '35DBA58C-5E2B-4AA0-8C54-4FB115772DC9'>
		
		<!--- Save the association, and receive the property in return --->
		<cfset local.obj = local.propertyService.saveAndReturnNewPropertyWithCompanyAddressAssociation(
				property=local.property,
				propertyAddress=local.propertyAddress,
				clientCompany=local.clientCompany,
				companyRole=local.companyRole)>
				
	</cffunction>

	<cffunction name="saveAndReturnNewPropertyClientCompanyAssociation"
				output="false" returnType="void"
				hint="This method is used to test saving a new property / client company association.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Create the necessary services --->
		<cfset local.propertyService = request.beanFactory.getBean("PropertyService")>
		
		<!--- Get an instance of the association object --->
		<cfset local.assocObject = local.propertyService.getEmptyPropertyClientCompanyAssociationVo()>

		<!--- Set the properties --->
		<cfset local.assocObject.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.assocObject.client_company_uid = 'CAEAD04C-5807-DE11-BBF3-001279657128'>
		<cfset local.assocObject.company_role_uid = '35DBA58C-5E2B-4AA0-8C54-4FB115772DC9'>
		<cfset local.assocObject.relationship_start = '2009-01-01 00:00:00'>
		
		<!--- Save the association object --->
		<cfset local.newAssocObject = local.propertyService.saveAndReturnNewPropertyClientCompanyAssociation(local.assocObject)>
		
	</cffunction>

	<cffunction name="saveExistingPropertyClientCompanyAssociation"
				output="false" returnType="void"
				hint="This method is used to test saving an existing property / client company association.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
			
		<!--- Create the necessary services --->
		<cfset local.propertyService = request.beanFactory.getBean("PropertyService")>
		
		<!--- Get an instance of the association object --->
		<cfset local.assocObject = local.propertyService.getEmptyPropertyClientCompanyAssociationVo()>
		
		<!--- Set the properties --->
		<cfset local.assocObject.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.assocObject.client_company_uid = 'CAEAD04C-5807-DE11-BBF3-001279657128'>
		<cfset local.assocObject.company_role_uid = '35DBA58C-5E2B-4AA0-8C54-4FB115772DC9'>
		<cfset local.assocObject.relationship_start = '2009-01-01 00:00:00'>
		
		<!--- Save the association object --->
		<cfset local.newAssocObject = local.propertyService.saveAndReturnNewPropertyClientCompanyAssociation(local.assocObject)>
		
		<!--- Now, modify the company role association --->
		<cfset local.newAssocObject.company_role_uid = '4AD59C10-76DE-42DC-BD80-88AE03175646'>
		
		<!--- Save the modified association object --->
		<cfset local.modifiedAssocObject = local.propertyService.saveExistingPropertyClientCompanyAssociation(local.newAssocObject)>

	</cffunction>

	<cffunction name="saveAndReturnNewPropertyClientContactAssociation"
				output="false" returnType="void"
				hint="This method is used to test saving a new property / client contact association.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Create the necessary services --->
		<cfset local.propertyService = request.beanFactory.getBean("PropertyService")>
		
		<!--- Get an instance of the association object --->
		<cfset local.assocObject = local.propertyService.getEmptyPropertyClientContactAssociationVo()>
		
		<!--- Set the properties --->
		<cfset local.assocObject.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.assocObject.external_identifier = 1>
		<cfset local.assocObject.client_company_uid = 'CAEAD04C-5807-DE11-BBF3-001279657128'>
		<cfset local.assocObject.contact_role_uid = 'F76E28CD-9AFE-4361-B0BB-AA0303EB2715'>
		<cfset local.assocObject.relationship_start = '2009-01-01 00:00:00'>
		
		<!--- Save the association object --->
		<cfset local.newAssocObject = local.propertyService.saveAndReturnNewPropertyClientContactAssociation(local.assocObject)>

	</cffunction>

	<cffunction name="saveExistingPropertyClientContactAssociation"
				output="false" returnType="void"
				hint="This method is used to test saving an existing property / client contact association.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
			
		<!--- Create the necessary services --->
		<cfset local.propertyService = request.beanFactory.getBean("PropertyService")>
		
		<!--- Get an instance of the association object --->
		<cfset local.assocObject = local.propertyService.getEmptyPropertyClientContactAssociationVo()>
		
		<!--- Set the properties --->
		<cfset local.assocObject.property_uid = '8578612E-81BF-4732-AC85-9CE1DED48AEF'>
		<cfset local.assocObject.external_identifier = 1>		
		<cfset local.assocObject.client_company_uid = 'CAEAD04C-5807-DE11-BBF3-001279657128'>
		<cfset local.assocObject.contact_role_uid = 'F76E28CD-9AFE-4361-B0BB-AA0303EB2715'>
		<cfset local.assocObject.relationship_start = '2009-01-01 00:00:00'>
		
		<!--- Save the association object --->
		<cfset local.newAssocObject = local.propertyService.saveAndReturnNewPropertyClientContactAssociation(local.assocObject)>
		
		<!--- Now, modify the company role association --->
		<cfset local.newAssocObject.contact_role_uid = '11C34FA6-348E-4EB6-8F15-D3BCB610FE70'>
		
		<!--- Save the modified association object --->
		<cfset local.modifiedAssocObject = local.propertyService.saveExistingPropertyClientContactAssociation(local.newAssocObject)>

	</cffunction>
	
</cfcomponent>