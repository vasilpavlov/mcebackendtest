<cfcomponent name="Test Welcome Page Remote Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to the welcome page.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="saveStoredDocumentSet" 
				output="false" returnType="void"
				hint="This method is used to test the ability to save a stored document set.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("storedDocumentService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.storedDocumentSet')>
		<cfset local.obj.friendly_name = 'My document set'>	
		<cfset local.result = local.service.saveAndReturnNewStoredDocumentSet(local.obj)>	
					
	</cffunction>

	<cffunction name="associateNewStoredDocumentSetToEnergyUsage" 
				output="false" returnType="void"
				hint="This method is used associate a stored document set to a given energy usage.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the service --->
		<cfset local.service = request.beanFactory.getBean("storedDocumentService")>

		<!--- Build out the storage document set object --->
		<cfset local.obj = createObject('component', 'mce.e2.vo.storedDocumentSet')>
		<cfset local.obj.friendly_name = 'My Test Document Set'>	

		<!--- Build out the energy usage object --->
		<cfset local.energyUsage = createObject('component', 'mce.e2.vo.energyUsage')>
		<cfset local.energyUsage.energy_usage_uid = 'E4A47C5F-8CED-483E-9C3A-0000874E6EB0'>

		<!--- Associate the objects --->
		<cfset local.result = local.service.associateNewStoredDocumentSetToEnergyUsage(
				storedDocumentSet=local.obj,
				energyUsage=local.energyUsage)>	
									
	</cffunction>
	
	<cffunction name="getEnergyUsage"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of energy usage data, including document counts.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the service --->
		<cfset local.service = request.beanFactory.getBean("energyUsageService")>
	
		<!--- Build out the energy usage object --->
		<cfset local.energyUsage = createObject('component', 'mce.e2.vo.energyUsage')>
		<cfset local.energyUsage.energy_usage_uid = 'E4A47C5F-8CED-483E-9C3A-0000874E6EB0'>

		<!--- Retrieve the energy usage object --->
		<cfset local.result = local.service.getEnergyUsage(local.energyUsage)>

	</cffunction>

	<cffunction name="createStoredDocument"
				access="public" returnType="void"
				hint="This method is used to test creating stored document records over an http connection.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
				
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("storedDocumentService")>
				
		<!--- Perform an http post to test the create stored document method --->		
		<cfhttp url="http://localhost/mceE2Services/remote/remoteStoredDocumentService.cfc?method=createStoredDocument"
				method="post"
				result="local.result">	
				
			<!--- Specify the http parameters --->	
			<cfhttpparam name="document_set_uid" type="formfield" value="AF95A1E7-DCA7-58D3-7030-3091DA515066">	
			<cfhttpparam name="repository_lookup_code" type="formfield" value="usage_entry_backup_docs">	
			<cfhttpparam name="original_filename" type="formfield" value="Stored Document Workflow Requirements.pdf">	
			<cfhttpparam name="preferred_filename" type="formfield" value="Requirements.pdf">	
			<cfhttpparam name="folder_name" type="formfield" value="testFolder">	
			<cfhttpparam name="file_content" type="file" file="C:\dev\mce\mceE2Services\mce\e2\testing\cfunit\tests\i002\Stored Document Workflow Requirements.pdf">	
				
		</cfhttp>	
									
	</cffunction>

	<cffunction name="getStoredDocumentSet"
				access="public" returnType="void"
				hint="This method is used to test retrieving a stored document set and its associated files.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
				
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("storedDocumentService")>
				
		<!--- Define the document set --->
		<cfset local.ds = createObject("component", "mce.e2.vo.storedDocumentSet")>
		<cfset local.ds.document_set_uid = 'EF7C8A76-9865-B189-B5F8-517EEC315DA0'>
		
		<!--- Retrieve the document set --->
		<cfset local.result = local.service.getStoredDocumentSet(local.ds)>		

	</cffunction>

	<cffunction name="removeStoredDocument"
				access="public" returnType="void"
				hint="This method is used to test removing a stored document.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
				
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("storedDocumentService")>
				
		<!--- Define the document --->
		<cfset local.ds = createObject("component", "mce.e2.vo.storedDocument")>
		<cfset local.ds.document_set_uid = 'AF95A1E7-DCA7-58D3-7030-3091DA515066'>
		<cfset local.ds.stored_document_uid = 'B2A12CA5-C1AF-92EA-3BA8-E9BFA4C8E602'>
		
		<!--- Remove the stored document --->
		<cfset local.result = local.service.removeStoredDocument(local.ds)>		

	</cffunction>
	
	<cffunction name="getStoredDocumentContent"
				access="public" returnType="void"
				hint="This method is used to test downloading stored document content over an http connection.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
				
		<!--- Perform an http post to test the create stored document method --->		
		<cfhttp url="http://localhost/mceE2Services/remote/remoteStoredDocumentService.cfc?method=getStoredDocumentContent"
				method="post"
				result="local.result">	
				
			<!--- Specify the http parameters --->	
			<cfhttpparam name="stored_document_uid" type="formfield" value="B2A12CA5-C1AF-92EA-3BA8-E9BFA4C8E602">	
			<cfhttpparam name="original_filename" type="formfield" value="Stored Document Workflow Requirements.pdf">	
				
		</cfhttp>		

	</cffunction>
	
</cfcomponent>