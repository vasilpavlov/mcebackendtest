<cfcomponent name="Test Data Playground Remote Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to the data playground remote services.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getEnergyAccountSummariesForDataAnalysis" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of energy account summary data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("energyAccountService")>
		<cfset local.propertyArray = arrayNew(1)>
		
		<!--- Set / append the first property --->
		<cfset local.firstProperty = structNew()>
		<cfset local.firstProperty.property_uid = '9051c22f-f3ee-425d-a476-dd058f253742'>
		<cfset arrayAppend(local.propertyArray, local.firstProperty)>
		
		<!--- Set / append the second property --->
		<cfset local.secondProperty = structNew()>
		<cfset local.secondProperty.property_uid = '3010aa1c-765e-40b6-a2b7-cb70e8af5d6e'>
		<cfset arrayAppend(local.propertyArray, local.secondProperty)>
		
		<!--- Set / append the third property --->
		<cfset local.thirdProperty = structNew()>
		<cfset local.thirdProperty.property_uid = '862e6d92-f86e-417d-9eb1-33c83adc4c0c'>
		<cfset arrayAppend(local.propertyArray, local.thirdProperty)>
		
		<!--- Retrieve the energy account summaries --->
		<cfset local.result = local.service.getEnergyAccountSummariesForDataAnalysis(
				propertyArray=local.propertyArray)>	
					
	</cffunction>
	
	<cffunction name="getAvailableMetricsDivisors" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available metrics advisors.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataPlaygroundService")>

		<!--- Initailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.namespace_lookup_code = 'playground.default'>
		<cfset local.args.as_of_date = '2009-01-01'>
		<cfset local.args.propertyArray = arrayNew(1)>
		
		<!--- Set / append the first property --->
		<cfset local.firstProperty = structNew()>
		<cfset local.firstProperty.property_uid = '9051c22f-f3ee-425d-a476-dd058f253742'>
		<cfset arrayAppend(local.args.propertyArray, local.firstProperty)>
		
		<!--- Set / append the second property --->
		<cfset local.secondProperty = structNew()>
		<cfset local.secondProperty.property_uid = '3010aa1c-765e-40b6-a2b7-cb70e8af5d6e'>
		<cfset arrayAppend(local.args.propertyArray, local.secondProperty)>
		
		<!--- Set / append the third property --->
		<cfset local.thirdProperty = structNew()>
		<cfset local.thirdProperty.property_uid = '862e6d92-f86e-417d-9eb1-33c83adc4c0c'>
		<cfset arrayAppend(local.args.propertyArray, local.thirdProperty)>

		<!--- Get the results --->
		<cfset local.result = local.service.getAvailableMetricsDivisors(
				argumentCollection=local.args)>
				
	</cffunction>			
				
	<cffunction name="getEmissionsAnalysis" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available metrics advisors.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("emissionsAnalysisService")>

		<!--- Initailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.property_uid_list = '862e6d92-f86e-417d-9eb1-33c83adc4c0c,9051c22f-f3ee-425d-a476-dd058f253742,3010aa1c-765e-40b6-a2b7-cb70e8af5d6e'>
		<cfset local.args.period_start = '2007-01-01'>
		<cfset local.args.period_end = '2009-01-01'>
		<cfset local.args.dateRollupMode = 'month-actual'>
		<cfset local.args.energy_account_uid_list = '8C65CFFA-006D-4EFD-9EF2-4A279BDB5786,8034DC64-6208-45AC-831A-6B78776BBF7F,3D40FA61-B41F-454E-BBD6-FE5D3E7291D7'>
		
		<!--- Get the result --->
		<cfset local.result = local.service.getEmissionsAnalysis(
				argumentCollection = local.args)>
								
	</cffunction>				
	
	<cffunction name="getPropertyMetaDataAnalysis" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property meta data analysis.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataPlaygroundService")>

		<!--- Initailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.period_start = '2007-01-01'>
		<cfset local.args.period_end = '2007-01-01'>
		<cfset local.args.dateRollupMode = 'monthly'>

		<!--- Define the property arrays --->
		<cfset local.args.energyAccountArray = arrayNew(1)>
		<cfset local.args.metaTypeArray = arrayNew(1)>
		
		<!--- Set / append the first property --->
		<cfset local.firstProperty = structNew()>
		<cfset local.firstProperty.energy_account_uid = '8C65CFFA-006D-4EFD-9EF2-4A279BDB5786'>
		<cfset arrayAppend(local.args.energyAccountArray, local.firstProperty)>
		
		<!--- Set / append the second property --->
		<cfset local.secondProperty = structNew()>
		<cfset local.secondProperty.energy_account_uid = '8034DC64-6208-45AC-831A-6B78776BBF7F'>
		<cfset arrayAppend(local.args.energyAccountArray, local.secondProperty)>
		
		<!--- Set / append the third property --->
		<cfset local.thirdProperty = structNew()>
		<cfset local.thirdProperty.energy_account_uid = '3D40FA61-B41F-454E-BBD6-FE5D3E7291D7'>
		<cfset arrayAppend(local.args.energyAccountArray, local.thirdProperty)>

		<!--- Set / append the first property --->
		<cfset local.firstProperty = structNew()>
		<cfset local.firstProperty.type_lookup_code = 'AdminAddressID'>
		<cfset arrayAppend(local.args.metaTypeArray, local.firstProperty)>
		
		<!--- Set / append the second property --->
		<cfset local.secondProperty = structNew()>
		<cfset local.secondProperty.type_lookup_code = 'BillingAddressID'>
		<cfset arrayAppend(local.args.metaTypeArray, local.secondProperty)>
		
		<!--- Set / append the third property --->
		<cfset local.thirdProperty = structNew()>
		<cfset local.thirdProperty.type_lookup_code = 'BuildingClass'>
		<cfset arrayAppend(local.args.metaTypeArray, local.thirdProperty)>
		
		<!--- Get the results --->
		<cfset local.result = local.service.getPropertyMetaDataAnalysis(
				argumentCollection=local.args)>
				
	</cffunction>				
				
	<cffunction name="getEnergyMetaDataAnalysis" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of energy meta data analysis.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataPlaygroundService")>

		<!--- Initailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.period_start = '2007-01-01'>
		<cfset local.args.period_end = '2007-01-01'>
		<cfset local.args.dateRollupMode = 'monthly'>

		<!--- Define the property arrays --->
		<cfset local.args.energyAccountArray = arrayNew(1)>
		<cfset local.args.metaTypeArray = arrayNew(1)>
		
		<!--- Set / append the first property --->
		<cfset local.firstProperty = structNew()>
		<cfset local.firstProperty.energy_account_uid = '8C65CFFA-006D-4EFD-9EF2-4A279BDB5786'>
		<cfset arrayAppend(local.args.energyAccountArray, local.firstProperty)>
		
		<!--- Set / append the second property --->
		<cfset local.secondProperty = structNew()>
		<cfset local.secondProperty.energy_account_uid = '8034DC64-6208-45AC-831A-6B78776BBF7F'>
		<cfset arrayAppend(local.args.energyAccountArray, local.secondProperty)>
		
		<!--- Set / append the third property --->
		<cfset local.thirdProperty = structNew()>
		<cfset local.thirdProperty.energy_account_uid = '3D40FA61-B41F-454E-BBD6-FE5D3E7291D7'>
		<cfset arrayAppend(local.args.energyAccountArray, local.thirdProperty)>

		<!--- Set / append the first property --->
		<cfset local.firstProperty = structNew()>
		<cfset local.firstProperty.type_lookup_code = 'AccountNumber'>
		<cfset arrayAppend(local.args.metaTypeArray, local.firstProperty)>
		
		<!--- Set / append the second property --->
		<cfset local.secondProperty = structNew()>
		<cfset local.secondProperty.type_lookup_code = 'YearNo'>
		<cfset arrayAppend(local.args.metaTypeArray, local.secondProperty)>
		
		<!--- Set / append the third property --->
		<cfset local.thirdProperty = structNew()>
		<cfset local.thirdProperty.type_lookup_code = 'LDC_Supply_Tax'>
		<cfset arrayAppend(local.args.metaTypeArray, local.thirdProperty)>
		
		<!--- Get the results --->
		<cfset local.result = local.service.getEnergyMetaDataAnalysis(
				argumentCollection=local.args)>
				
	</cffunction>							
				
	<cffunction name="getRateFactorAnalysis" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of rate factor data analysis.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataPlaygroundService")>

		<!--- Initailize the arguments structure --->
		<cfset local.args = structNew()>
		<cfset local.args.period_start = '2007-01-01'>
		<cfset local.args.period_end = '2007-01-01'>
		<cfset local.args.dateRollupMode = 'monthly'>

		<!--- Define the property arrays --->
		<cfset local.args.rateFactorArray = arrayNew(1)>
		
		<!--- Set / append the first property --->
		<cfset local.firstProperty = structNew()>
		<cfset local.firstProperty.factor_lookup_code = 'ny.electric.coned.demandrate.zone-j'>
		<cfset arrayAppend(local.args.rateFactorArray, local.firstProperty)>
		
		<!--- Set / append the second property --->
		<cfset local.secondProperty = structNew()>
		<cfset local.secondProperty.factor_lookup_code = 'mce.ny.electric.coned.day-ahead.zone-j'>
		<cfset arrayAppend(local.args.rateFactorArray, local.secondProperty)>
		
		<!--- Set / append the third property --->
		<cfset local.thirdProperty = structNew()>
		<cfset local.thirdProperty.factor_lookup_code = 'ny.electric.coned.lossfactor.class-m'>
		<cfset arrayAppend(local.args.rateFactorArray, local.thirdProperty)>				
				
		<!--- Get the results --->
		<cfset local.result = local.service.getRateFactorDataAnalysis(
				argumentCollection=local.args)>		
				
	</cffunction>		
				
</cfcomponent>