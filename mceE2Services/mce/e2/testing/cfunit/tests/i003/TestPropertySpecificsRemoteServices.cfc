<cfcomponent name="Test Property Specifics Remote Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to the property specifics display.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getPropertyCollectionsAndAssociatedProperties" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of property collections and related properties.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
		
		<!--- Instantiate the property collection service --->
		<cfset local.propertyCollectionService = request.beanFactory.getBean("propertyCollectionService")>
					
		<!--- Retrieve the associated property collections and properties --->			
		<cfset local.result = local.propertyCollectionService.getPropertyCollectionsAndAssociatedProperties()>				
					
	</cffunction>

	<cffunction name="getPropertyCollectionAndAssociatedPropertiesWhileRespectingSecurity" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a specific property collection and related properties while respecting user security.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
		
		<!--- Instantiate the property collection service --->
		<cfset local.propertyCollectionService = request.beanFactory.getBean("propertyCollectionService")>
		
		<!--- Initialize the arguments that will be tested --->
		<cfset local.propertyCollection = createObject('component', 'mce.e2.vo.propertyCollection')>			
		<cfset local.propertyCollection.property_collection_uid = 'bd0c446a-b0dc-47d1-a340-3aa1f32960d4'>			
					
		<!--- Retrieve the associated property collections and properties --->			
		<cfset local.result = local.propertyCollectionService.getPropertyCollectionAndAssociatedProperties(
				propertyCollection = local.propertyCollection)>				
					
	</cffunction>

	<cffunction name="getPropertyCollectionAndAssociatedPropertiesWhileNotRespectingSecurity" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a specific property collection and related properties while not respecting user security.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the property collection service --->
		<cfset local.propertyCollectionService = request.beanFactory.getBean("propertyCollectionService")>
		
		<!--- Initialize the arguments that will be tested --->
		<cfset local.propertyCollection = createObject('component', 'mce.e2.vo.propertyCollection')>			
		<cfset local.propertyCollection.property_collection_uid = 'bd0c446a-b0dc-47d1-a340-3aa1f32960d4'>			
					
		<!--- Retrieve the associated property collections and properties --->			
		<cfset local.result = local.propertyCollectionService.getPropertyCollectionAndAssociatedProperties(
				propertyCollection = local.propertyCollection
				byPassSecurity = true)>				
					
	</cffunction>

	<cffunction name="getPropertyDetails"
				output="false" returntype="void"
				hint="This method is used to test the retrieval of property details.">

		<!--- Initialize local variables --->
		<cfset var local = structnew()>
				
		<!--- Instantiate the property collection service --->
		<cfset local.propertyService = request.beanFactory.getBean("propertyService")>
		
		<!--- Initialize the arguments that will be tested --->
		<cfset local.property = createObject('component', 'mce.e2.vo.property')>			
		<cfset local.property.property_uid = '4c6925f1-c42b-4a94-a839-82398b117255'>			
					
		<!--- Retrieve the associated property collections and properties --->			
		<cfset local.result = local.propertyService.getPropertyDetails(
				property = local.property)>	
				
	</cffunction>

	<cffunction name="getAssociatedProperties"
				output="false" returntype="void"
				hint="This method is used to test the retrieval of all associated properties across associated property collections.">

		<!--- Initialize local variables --->
		<cfset var local = structnew()>
	
		<!--- Instantiate the property collection service --->
		<cfset local.propertyService = request.beanFactory.getBean("propertyService")>
		
		<!--- Initialize the arguments that will be tested --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
					
		<!--- Retrieve the associated property collections and properties --->			
		<cfset local.result = local.propertyService.getAssociatedProperties()>						

	</cffunction>
					
</cfcomponent>