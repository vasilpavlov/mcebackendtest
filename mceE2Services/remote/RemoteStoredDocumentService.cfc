<!---

  Copyright (c) 2005, Chris Scott, David Ross, Kurt Wiersma, Sean Corfield
  All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

 $Id: RemoteProxyBean.cfc,v 1.8 2008/03/07 02:25:04 pjf Exp $
 $Log: RemoteProxyBean.cfc,v $
 Revision 1.8  2008/03/07 02:25:04  pjf
 Var'ed and scope variables caught by a var scope checker

 Revision 1.7  2007/09/11 11:41:52  scottc
 Fixed error setting bean factory in the proper scope, moved initialization into setup method in RemoteProxyBean

 Revision 1.6  2007/01/01 17:41:36  scottc
 added support for <alias name="fromName" alias="toName"/> tag

 Revision 1.5  2006/06/25 13:22:43  rossd
 removing debug code

 Revision 1.4  2006/04/04 03:51:27  simb
 removed duplicate local var bfUtils

 Revision 1.3  2006/01/28 21:44:13  scottc
 Another slight tweek, everything refers to beanFactory, not context

 Revision 1.2  2006/01/28 21:39:57  scottc
 Shoot, the RemoteProxyBean was looking for an applicationContext instead of a bean factory. Updated to look for a beanFactory, but I need to test!

 Revision 1.1  2006/01/13 15:00:12  scottc
 CSP-38 - First pass at RemoteProxyBean, creating remote services for CS managed seriveces through AOP


--->

<cfcomponent name="RemoteStoredDocumentService"
			displayname="RemoteStoredDocumentService:RemoteProxyBean"
			hint="Abstract Base Class for Aop Based Remote Proxy Beans"
			output="false">

	<cfset variables.proxyId = CreateUUId() />
	<cfset variables.beanFactoryName = "beanFactory" />
	<cfset variables.beanFactoryScope = "" />
	<cfset variables.constructed = false />
	<cfset setup() />

	<cffunction name="setup" access="public" returntype="void">
		<cfset var bfUtils = 0 />
		<cfset var bf = 0 />
		<cfset var error = false />
		<cfset var remoteFactory = "" />

		<!--- I want to make sure that the proxy id really exists --->
		<cfif not StructKeyExists(variables, "proxyId")>
			<cfset variables.proxyId = CreateUUId() />
		</cfif>

		<cflock name="RemoteProxyBean.#variables.proxyId#.Setup" type="readonly" timeout="5">
			<cfif not StructKeyExists(variables, "constructed") or not variables.constructed>

				<!--- it looks like there is an issue with setting up the variables scope in a static initializer
					  with remote methods, so we will make sure things are set up --->
				<cfif not StructKeyExists(variables, "constructed")>
					<cfset variables.beanFactoryName = "beanFactory" />
					<cfset variables.beanFactoryScope = "" />
					<cfset variables.constructed = false />
				</cfif>
				<!--- make sure scope is setup (could have been set to '', meaning application, default) --->
				<cfif not len(variables.beanFactoryScope)>
					<cfset variables.beanFactoryScope = 'application' />
				</cfif>
					<cfset bfUtils = createObject("component","coldspring.beans.util.BeanFactoryUtils").init()/>
				<cftry>
					<cfif not len(variables.beanFactoryName)>
						<cfset bf = bfUtils.getDefaultFactory(variables.beanFactoryScope) />
					<cfelse>
						<cfset bf = bfUtils.getNamedFactory(variables.beanFactoryScope, variables.beanFactoryName) />
					</cfif>
					<cfset remoteFactory = bf.getBean("&RemoteStoredDocumentService") />
					<cfset variables.target = bf.getBean("RemoteStoredDocumentService") />
					<cfset variables.adviceChains = remoteFactory.getProxyAdviceChains() />
					<cfset variables.constructed = true />
					<cfcatch>
						<cfset error = true />
					</cfcatch>
				</cftry>
			</cfif>
		</cflock>

		<cfif error>
			<cfthrow type="coldspring.remoting.ApplicationContextError"
					 message="Sorry, a ColdSpring BeanFactory named #variables.beanFactoryName# was not found in #variables.beanFactoryScope# scope. Please make sure your bean factory is properly loaded. Perhapse your main application is not running?" />
		</cfif>

	</cffunction>

	<cffunction name="callMethod" access="private" returntype="any">
		<cfargument name="methodName" type="string" required="true" />
		<cfargument name="args" type="struct" required="true" />
		<cfset var adviceChain = 0 />
		<cfset var methodInvocation = 0 />
		<cfset var rtn = 0 />
		<cfset var method = 0 />

		<!--- make sure setup is called --->
		<cfif not StructKeyExists(variables, "constructed") or not variables.constructed>
			<cfset setup() />
		</cfif>

		<!--- if an advice chain was created for this method, retrieve a methodInvocation chain from it and proceed --->
		<cfif StructKeyExists(variables.adviceChains, arguments.methodName)>
			<cfset method = CreateObject('component','coldspring.aop.Method').init(variables.target, arguments.methodName, arguments.args) />
			<cfset adviceChain = variables.adviceChains[arguments.methodName] />
			<cfset methodInvocation = adviceChain.getMethodInvocation(method, arguments.args, variables.target) />
			<cfreturn methodInvocation.proceed() />
		<cfelse>
			<!--- if there's no advice chains to execute, just call the method --->
			<cfinvoke component="#variables.target#"
					  method="#arguments.methodName#"
					  argumentcollection="#arguments.args#"
					  returnvariable="rtn">
			</cfinvoke>
			<cfif isDefined('rtn')>
				<cfreturn rtn />
			</cfif>
		</cfif>

	</cffunction>

	<cffunction name="setStoredDocumentDelegate" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.db.StoredDocumentDelegate" /> 
<cfset var rtn = callMethod('setStoredDocumentDelegate', arguments) />
</cffunction>

<cffunction name="getEmptyStoredDocumentSetVo" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getEmptyStoredDocumentSetVo', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveNewStoredDocument" access="remote" returntype="any" > 
<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" /> 
<cfset var rtn = callMethod('saveNewStoredDocument', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveExistingStoredDocument" access="remote" returntype="any" > 
<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" /> 
<cfset var rtn = callMethod('saveExistingStoredDocument', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="removeStoredDocument" access="remote" returntype="any" > 
<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" /> 
<cfset var rtn = callMethod('removeStoredDocument', arguments) />
</cffunction>

<cffunction name="getStoredDocument" access="remote" returntype="any" > 
<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" /> 
<cfset var rtn = callMethod('getStoredDocument', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getDocumentRepositoryProperties" access="remote" returntype="any" > 
<cfargument name="repository_lookup_code" type="string" required="false" /> 
<cfargument name="document_repository_uid" type="string" required="false" /> 
<cfset var rtn = callMethod('getDocumentRepositoryProperties', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setEnergyUsageDelegate" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.db.EnergyUsageDelegate" /> 
<cfset var rtn = callMethod('setEnergyUsageDelegate', arguments) />
</cffunction>

<cffunction name="getStoredDocuments" access="remote" returntype="any" > 
<cfargument name="stored_document_uid" type="string" required="false" /> 
<cfargument name="document_repository_uid" type="string" required="false" /> 
<cfargument name="document_set_uid" type="string" required="false" /> 
<cfargument name="permission_needed" type="string" required="false" /> 
<cfargument name="property_uid" type="string" required="false" /> 
<cfargument name="client_company_uid" type="string" required="false" /> 
<cfset var rtn = callMethod('getStoredDocuments', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveNewStoredDocumentSet" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfset var rtn = callMethod('saveNewStoredDocumentSet', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveExistingStoredDocumentSet" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfset var rtn = callMethod('saveExistingStoredDocumentSet', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getStoredDocumentSets" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getStoredDocumentSets', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveAndReturnNewStoredDocument" access="remote" returntype="any" > 
<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" /> 
<cfset var rtn = callMethod('saveAndReturnNewStoredDocument', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="validateDocumentFolders" access="remote" returntype="any" > 
<cfargument name="directoryPath" type="string" required="true" /> 
<cfset var rtn = callMethod('validateDocumentFolders', arguments) />
</cffunction>

<cffunction name="guessContentType" access="remote" returntype="any" > 
<cfargument name="filename" type="string" required="true" /> 
<cfargument name="defaultType" type="string" required="true" /> 
<cfset var rtn = callMethod('guessContentType', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setStoredDocumentSetDelegate" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.db.StoredDocumentSetDelegate" /> 
<cfset var rtn = callMethod('setStoredDocumentSetDelegate', arguments) />
</cffunction>

<cffunction name="setEnergyContractsDelegate" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.db.EnergyContractsDelegate" /> 
<cfset var rtn = callMethod('setEnergyContractsDelegate', arguments) />
</cffunction>

<cffunction name="saveAndReturnNewStoredDocumentSet" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfset var rtn = callMethod('saveAndReturnNewStoredDocumentSet', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setClientCompanyDelegate" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.db.ClientCompanyDelegate" /> 
<cfset var rtn = callMethod('setClientCompanyDelegate', arguments) />
</cffunction>

<cffunction name="associateNewStoredDocumentSetToClientCompany" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" /> 
<cfset var rtn = callMethod('associateNewStoredDocumentSetToClientCompany', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="createStoredDocument" access="remote" returntype="any" > 
<cfargument name="document_set_uid" type="string" required="true" /> 
<cfargument name="repository_lookup_code" type="string" required="true" /> 
<cfargument name="friendly_name" type="string" required="true" /> 
<cfargument name="original_filename" type="string" required="true" /> 
<cfargument name="document_keywords" type="string" required="false" default="" /> 
<cfargument name="preferred_filename" type="string" required="true" /> 
<cfargument name="permission_needed" type="string" required="true" /> 
<cfargument name="folder_name" type="string" required="true" /> 
<cfargument name="file_content" type="string" required="true" /> 
<cfset var rtn = callMethod('createStoredDocument', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setPropertyDelegate" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.db.PropertyDelegate" /> 
<cfset var rtn = callMethod('setPropertyDelegate', arguments) />
</cffunction>

<cffunction name="getStoredDocumentSet" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfset var rtn = callMethod('getStoredDocumentSet', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getStoredDocumentsByContext" access="remote" returntype="any" > 
<cfargument name="client_company_uid" type="string" required="false" /> 
<cfargument name="property_uid" type="string" required="false" /> 
<cfargument name="energy_contract_uid" type="string" required="false" /> 
<cfargument name="client_contract_uid" type="string" required="false" /> 
<cfargument name="include_properties" type="boolean" required="false" default="true" /> 
<cfargument name="filter_by_active" type="boolean" required="false" default="true" /> 
<cfset var rtn = callMethod('getStoredDocumentsByContext', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getStoredDocumentContent" access="remote" returntype="any" > 
<cfargument name="stored_document_uid" type="string" required="true" /> 
<cfargument name="original_filename" type="string" required="true" /> 
<cfargument name="content_disposition" type="string" required="true" default="attachment" /> 
<cfset var rtn = callMethod('getStoredDocumentContent', arguments) />
</cffunction>

<cffunction name="getEmptyStoredDocumentVo" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getEmptyStoredDocumentVo', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="associateNewStoredDocumentSetToEnergyContract" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" /> 
<cfset var rtn = callMethod('associateNewStoredDocumentSetToEnergyContract', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="associateNewStoredDocumentSetToProperty" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfargument name="Property" type="mce.e2.vo.Property" required="true" /> 
<cfset var rtn = callMethod('associateNewStoredDocumentSetToProperty', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="associateNewStoredDocumentSetToEnergyUsage" access="remote" returntype="any" > 
<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" /> 
<cfargument name="EnergyUsage" type="mce.e2.vo.EnergyUsage" required="true" /> 
<cfset var rtn = callMethod('associateNewStoredDocumentSetToEnergyUsage', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="buildVoPropertyListFromArray" access="remote" returntype="any" > 
<cfargument name="voArray" type="array" required="true" /> 
<cfargument name="propertyName" type="string" required="true" /> 
<cfset var rtn = callMethod('buildVoPropertyListFromArray', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getUserUid" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getUserUid', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getPermissions" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getPermissions', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="validateVo" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfset var rtn = callMethod('validateVo', arguments) />
</cffunction>

<cffunction name="deActivate" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfset var rtn = callMethod('deActivate', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="enforcePermission" access="remote" returntype="any" > 
<cfargument name="permissionCode" type="string" required="true" /> 
<cfset var rtn = callMethod('enforcePermission', arguments) />
</cffunction>

<cffunction name="setAuditProperties" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfargument name="updateType" type="string" required="true" default="create" /> 
<cfset var rtn = callMethod('setAuditProperties', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="reviewAssociations" access="remote" returntype="any" > 
<cfargument name="associatedObjects" type="array" required="true" /> 
<cfargument name="associationsToProcess" type="array" required="true" /> 
<cfargument name="primaryKey" type="string" required="true" /> 
<cfargument name="compareProperty" type="string" required="false" /> 
<cfset var rtn = callMethod('reviewAssociations', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="removeNullPropertiesFromVo" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfset var rtn = callMethod('removeNullPropertiesFromVo', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setSecurityService" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.service.SecurityService" /> 
<cfset var rtn = callMethod('setSecurityService', arguments) />
</cffunction>

<cffunction name="getAuditLabel" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getAuditLabel', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="hasPermission" access="remote" returntype="any" > 
<cfargument name="permissionCode" type="string" required="true" /> 
<cfset var rtn = callMethod('hasPermission', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setPrimaryKey" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfargument name="primaryKey" type="string" required="false" /> 
<cfset var rtn = callMethod('setPrimaryKey', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="queryRowToStruct" access="remote" returntype="any" output="false" > 
<cfargument name="query" type="query" required="true" /> 
<cfargument name="row" type="numeric" required="true" default="1" /> 
<cfset var rtn = callMethod('queryRowToStruct', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="createUniqueIdentifierPlaceholder" access="remote" returntype="any" output="false" > 
<cfset var rtn = callMethod('createUniqueIdentifierPlaceholder', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="stringExtract" access="remote" returntype="any" > 
<cfargument name="str" type="string" required="true" /> 
<cfargument name="pattern" type="string" required="true" /> 
<cfargument name="subexpressionIndex" type="numeric" required="false" default="1" /> 
<cfset var rtn = callMethod('stringExtract', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="removeMilisecondsFromDate" access="remote" returntype="any" > 
<cfargument name="date" type="date" required="true" /> 
<cfset var rtn = callMethod('removeMilisecondsFromDate', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="createUniqueIdentifier" access="remote" returntype="any" output="false" > 
<cfset var rtn = callMethod('createUniqueIdentifier', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="isUniqueIdentifier" access="remote" returntype="any" output="false" > 
<cfargument name="guid" type="string" required="true" /> 
<cfset var rtn = callMethod('isUniqueIdentifier', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>



</cfcomponent>
