
<!--- Initialize the local scope --->
<cfset local = structNew()>

<!--- Initialize the services that we need --->
<cfset local.propertyCollectionService = request.beanFactory.getBean("propertyCollectionService")>
<cfset local.propertyService = request.beanFactory.getBean("propertyService")>
<cfset local.securityService = request.beanFactory.getBean("securityService")>

<cfset local.session = local.securityService.attemptLogin('nate','nate')>

<cfset local.propertyCollection = local.propertyCollectionService.getEmptyPropertyCollectionVo()>
<cfset local.propertyCollection.property_collection_uid = 'BAE35A71-0EBA-81B9-789C-C2D173B0CA62'>
<cfset local.propertyCollection = local.propertyCollectionService.getPropertyCollection(local.propertyCollection)>

<cfset local.propertyCollectionService.disassociateUserFromPropertyCollection(
		local.propertyCollection)>

<cfdump var="#local.propertyCollections#">