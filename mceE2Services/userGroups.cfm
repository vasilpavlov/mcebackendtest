
<!--- Initialize the local scope --->
<cfset local = structNew()>

<!--- Initialize the services that we need --->
<cfset local.userGroupService = request.beanFactory.getBean("userGroupService")>
<cfset local.securityService = request.beanFactory.getBean("securityService")>

<!--- Log into the user's session --->
<cfset local.session = local.securityService.attemptLogin('username001','username001')>

<!--- Retrieve the test user group --->
<cfset local.userGroup = local.userGroupService.getEmptyUserGroupVo()>
<cfset local.userGroup.company_uid = local.session.client_company_uid>
<cfset local.userGroup.user_group_uid = 'DA97D847-ABE8-74CD-1165-AC3EEA91FB48'>
<cfset local.userGroup = local.userGroupService.getUserGroup(local.userGroup)>

<!--- Get the associated users --->
<table>
<tr><td colspan="3">Property Collections</td></tr>
<tr>
<td valign="top">
GetAssociations
<cfset local.pcArray = local.userGroupService.getPropertyCollectionAssociations(local.userGroup)>
<cfdump var="#local.pcArray#">
</td>
<td valign="top">
GetAssociated
<cfset local.output = local.userGroupService.getAssociatedPropertyCollections(local.userGroup)>
<cfdump var="#local.output#">
</td>
<td valign="top">
GetUnAssociated
<cfset local.output = local.userGroupService.getUnAssociatedPropertyCollections(local.userGroup)>
<cfdump var="#local.output#">
</td>
</tr>
</table>
<table>
<tr><td colspan="3">Users</td></tr>
<tr>
<td valign="top">
GetAssociations
<cfset local.pcArray = local.userGroupService.getUserAssociations(local.userGroup)>
<cfdump var="#local.pcArray#">
</td>
<td valign="top">
GetAssociated
<cfset local.output = local.userGroupService.getAssociatedUsers(local.userGroup)>
<cfdump var="#local.output#">
</td>
<td valign="top">
GetUnAssociated
<cfset local.output = local.userGroupService.getUnAssociatedUsers(local.userGroup)>
<cfdump var="#local.output#">
</td>
</tr>
</table>
<table>
<tr><td colspan="3">User Roles</td></tr>
<tr>
<td valign="top">
GetAssociations
<cfset local.pcArray = local.userGroupService.getUserRoleAssociations(local.userGroup)>
<cfdump var="#local.pcArray#">
</td>
<td valign="top">
GetAssociated
<cfset local.output = local.userGroupService.getAssociatedUserRoles(local.userGroup)>
<cfdump var="#local.output#">
</td>
<td valign="top">
GetUnAssociated
<cfset local.output = local.userGroupService.getUnAssociatedUserRoles(local.userGroup)>
<cfdump var="#local.output#">
</td>
</tr>
</table>