
<!--- Initialize the local scope --->
<cfset local = structNew()>

<!--- Initialize the services that we need --->
<cfset local.userService = request.beanFactory.getBean("userService")>
<cfset local.securityService = request.beanFactory.getBean("securityService")>

<!--- Log into the user's session --->
<cfset local.session = local.securityService.attemptLogin('username001','password')>

<!--- Retrieve the test user group --->
<cfset local.user = local.userService.getEmptyUserVo()>
<cfset local.user.user_uid = local.session.user_uid>
<cfset local.user.client_company_uid = local.session.client_company_uid>

<table>
<tr>
<td valign="top">
getAssociations
<cfdump var="#local.userService.getUserGroupAssociations(local.user)#">
</td>
<td valign="top">
getAssociated
<cfdump var="#local.userService.getAssociatedUserGroups(local.user)#">
</td>
<td valign="top">
getUnAssociated
<cfdump var="#local.userService.getUnAssociatedUserGroups(local.user)#">
</td>
</tr>
</table>


