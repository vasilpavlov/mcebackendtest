
<!--- Initialize the local scope --->
<cfset local = structNew()>

<!--- Initialize the services that we need --->
<cfset local.securityService = request.beanFactory.getBean("securityService")>

<!--- Get the session --->
<cfset local.session = local.securityService.getSession()>

<cfdump var="#local.session#">

<cfset local.session = local.securityService.attemptLogin('nate','nate')>

<cfdump var="#local.session#">

<cfset local.session = local.securityService.getSession()>

<cfdump var="#local.session#">
