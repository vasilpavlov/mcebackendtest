
<!--- Switch / case over what data to seed 
<cfhttp url="http://192.168.1.10:80/ReportServer_sqldev?/SQL2008 Reports/Account Level Detail Report&rs:Command=Render&rs:Format=pdf&energy_account_uid=2abab453%2D7e00%2D4654%2Da923%2D6a0ee0c387d1&period_start=12%2F01%2F2006&period_end=06%2F01%2F2009"  username="mce\Administrator" password="mce" result="report" getasbinary="YES"/> 

<cfdump var="#report#">
--->

<!--- Test vendor entry history --->
<cfset service = application.beanFactory.getBean("EnergyUsageService") />

<!--- Existing --->
<cfset usages = service.getEnergyUsages(engeryUsageUid="e4a47c5f-8ced-483e-9c3a-0000874e6eb0")>
<cfset usages[1].usage_status_code = "estimated" />
<cfset service.saveExistingEnergyUsage(usages[1], true)>
<cfset service.saveExistingEnergyUsage(usages[1], false)>

<!--- New --->
<cfset usage = createObject("component", "mce.e2.vo.EnergyUsage")>
<cfset usage.energy_account_uid = "A6097E69-5B82-4120-8CB3-41DC6F9061F9">
<cfset usage.period_start = now()>
<cfset usage.period_end = now()>
<cfset usage.energy_unit_uid = "F2266770-62F9-43F9-BC7A-CC8FF167EFC4">
<cfset usage.classification_code = 0>
<cfset usage.recorded_value = 45654>
<cfset usage.is_system_calculated = 0>
<cfset usage.is_active = 1>
