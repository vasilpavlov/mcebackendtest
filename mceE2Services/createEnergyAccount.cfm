<!--- Initialize local variables --->
<cfset local = structNew()>	

<!--- Create the service --->	
<cfset local.accountService = request.beanFactory.getBean("energyAccountService")>

<!--- Get the energy account --->
<!---
<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyAccount')>
<cfset local.obj.energy_account_uid = 'D747ADD1-2769-4F56-BA5D-001DF2EFC64B'>	
<cfset local.obj = local.accountService.getEnergyAccount(local.obj)>
<cfdump var="#local.obj#">
<cfabort>
--->

<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyAccount')>

<!--- modify the account --->
<cfset local.obj.account_type_uid ='251c341b-0b7b-4ee4-9b78-ff562364f82e'>
<cfset local.obj.energy_provider_uid ='72f22801-c631-4743-b173-00f66cc1aca5'>
<cfset local.obj.energy_type_uid ='0b6c61c8-92d3-40d8-9a15-520e1df91a29'>
<cfset local.obj.friendly_name ='Abraham Lloyd'>
<cfset local.obj.meta_group_uid ='51ddc0c1-594d-4e37-a1c5-9c5623928c2b'>
<cfset local.obj.native_account_number ='abrahamlloyd'>
<cfset local.obj.property_uid ='a892074d-848f-44a6-a9a6-20d269248fbd'>
<cfset local.obj.rate_class_uid ='c3c2675c-5919-de11-9951-0019d2af28a8'>
<cfset local.obj.rate_model_uid ='0e5cc250-6a46-4bc4-a5c8-5d0d608267d4'>
<cfset local.obj.energy_unit_uid ='EFD0DE19-7820-43A4-830B-FA26A4EE1EAC'>
<cfset local.obj.factor_alias_uid_override ='D4027A3C-5105-DE11-B5EA-001C265BAF9C'>
<cfdump var="#local.obj#">

<!--- Save the account --->
<cfset local.obj = local.accountService.SaveAndReturnNewEnergyAccount(local.obj)>
<cfset local.obj = local.accountService.getEnergyAccount(local.obj)>
<cfdump var="#local.obj#">

<cfset local.obj.friendly_name = 'Modified ADL'>
<cfset local.accountService.SaveExistingEnergyAccount(local.obj)>
<cfset local.obj = local.accountService.getEnergyAccount(local.obj)>
<cfdump var="#local.obj#">

